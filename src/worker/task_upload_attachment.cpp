/*
 * Copyright (C) 2014-2025 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

#include <QThread>
#include <QFile>

#include "src/datovka_shared/log/log.h"
#include "src/global.h"
#include "src/isds/services/message_interface_vodz.h"
#include "src/isds/session/isds_session.h"
#include "src/isds/session/isds_sessions.h"
#include "src/worker/emitter.h"
#include "src/worker/task_upload_attachment.h"

TaskUploadAttachment::TaskUploadAttachment(const AcntId &acntId,
    const QString &fileName, const QString &fileMimeType,
    const QString &filePath, bool useMtomXop)
    : m_result(DL_ERR),
    m_dmAtt(),
    m_lastError(),
    m_acntId(acntId),
    m_fileName(fileName),
    m_fileMimeType(fileMimeType),
    m_filePath(filePath),
    m_useMtomXop(useMtomXop)
{
}

void TaskUploadAttachment::run(void)
{
	/* ### Worker task begin. ### */

	logDebugLv0NL("%s", "------------UPLOAD ATTACHMENT TASK------------");
	logDebugLv0NL("Starting in thread '%p'", QThread::currentThreadId());

	m_result = uploadAttachment(m_acntId, m_fileName, m_fileMimeType,
	    m_filePath, m_useMtomXop, m_dmAtt, m_lastError);

	logDebugLv0NL("Finished in thread '%p'", QThread::currentThreadId());
	logDebugLv0NL("%s", "-----------------------------------------------");

	/* ### Worker task end. ### */
}

/*!
 * @brief Translate communication error onto task error.
 */
static
enum TaskUploadAttachment::Result error2result(enum Isds::Type::Error error)
{
	switch (error) {
	case Isds::Type::ERR_SUCCESS:
		return TaskUploadAttachment::DL_SUCCESS;
		break;
	case Isds::Type::ERR_ISDS:
		return TaskUploadAttachment::DL_ISDS_ERROR;
		break;
	case Isds::Type::ERR_SOAP:
	case Isds::Type::ERR_XML:
		return TaskUploadAttachment::DL_XML_ERROR;
		break;
	default:
		return TaskUploadAttachment::DL_ERR;
		break;
	}
}

enum TaskUploadAttachment::Result TaskUploadAttachment::uploadAttachment(
    const AcntId &acntId, const QString &fileName, const QString &fileMimeType,
    const QString &filePath, bool useMtomXop, Isds::DmAtt &dmAtt,
    QString &lastError)
{
	if (Q_UNLIKELY((!acntId.isValid()) || (filePath.isEmpty()) ||
	        (GlobInstcs::isdsSessionsPtr == Q_NULLPTR) ||
	        (!GlobInstcs::isdsSessionsPtr->holdsSession(acntId)))) {
		Q_ASSERT(0);
		return DL_ERR;
	}

	Isds::Session *session = GlobInstcs::isdsSessionsPtr->session(acntId);
	if (Q_UNLIKELY(session == Q_NULLPTR)) {
		Q_ASSERT(0);
		return DL_ERR;
	}

	logDebugLv1NL("Sending from account '%s'",
	    acntId.username().toUtf8().constData());


	enum Isds::Type::Error error = Isds::Type::ERR_ERROR;

	if (useMtomXop) {
		error = Isds::uploadAttachmentMtomXop(*session, fileName,
		    fileMimeType, filePath, dmAtt, Q_NULLPTR, &lastError);
	} else {
		Isds::DmFile dmFile;
		QFile f(filePath);
		if (Q_UNLIKELY(!f.open(QIODevice::ReadOnly))) {
			return DL_ERR;
		}
		dmFile.setBinaryContent(f.readAll());
		dmFile.setFileDescr(fileName);
		dmFile.setMimeType(fileMimeType);
		f.close();
		error = Isds::uploadAttachmentBase64(*session, dmFile, dmAtt,
		    Q_NULLPTR, &lastError);
	}

	if (Q_UNLIKELY(error != Isds::Type::ERR_SUCCESS)) {
		return error2result(error);
	}

	/* Attachment has been sent */
	logDebugLv1NL("Attachment has been sent to ISDS with ID %s",
	    dmAtt.dmAttID().toUtf8().constData());

	return DL_SUCCESS;
}
