/*
 * Copyright (C) 2014-2025 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

#pragma once

#include <QString>

#include "src/datovka_shared/identifiers/account_id.h"
#include "src/datovka_shared/isds/message_interface_vodz.h"
#include "src/worker/task.h"

/*!
 * @brief Task describing uploading of a big message attachment.
 */
class TaskUploadAttachment : public Task {
public:
	/*!
	 * @brief Return state describing what happened.
	 */
	enum Result {
		DL_SUCCESS, /*!< Operation was successful. */
		DL_ISDS_ERROR, /*!< Error communicating with ISDS. */
		DL_XML_ERROR, /*!< Error parsing XML. */
		DL_DB_INS_ERR, /*!< Error inserting into database. */
		DL_ERR /*!< Other error. */
	};

	/*!
	 * @brief Constructor.
	 *
	 * @param[in] acntId Account id.
	 * @param[in] fileName File name.
	 * @param[in] fileMimeType File mime type.
	 * @param[in] filePath File path.
	 * @param[in] useMtomXop True if send request as MTOM/XOP.
	 */
	explicit TaskUploadAttachment(const AcntId &acntId,
	    const QString &fileName, const QString &fileMimeType,
	    const QString &filePath, bool useMtomXop);

	/*!
	 * @brief Performs actual upload big attachment.
	 */
	virtual
	void run(void) Q_DECL_OVERRIDE;

	enum Result m_result; /*!< Return state. */
	Isds::DmAtt m_dmAtt; /*!< Attachment identification. */
	QString m_lastError; /*!< Last ISDS error message. */

private:
	/*!
	 * Disable copy and assignment.
	 */
	TaskUploadAttachment(const TaskUploadAttachment &);
	TaskUploadAttachment &operator=(const TaskUploadAttachment &);

	/*!
	 * @brief Upload attachment.
	 *
	 * @param[in] acntId Account id.
	 * @param[in] fileName File name.
	 * @param[in] fileMimeType File mime type.
	 * @param[in] filePath File path.
	 * @param[in] useMtomXop True if send request as MTOM/XOP.
	 * @param[out] dmAtt Attachment identification.
	 * @param[out] lastError Last ISDS error message.
	 * @return Error state.
	 */
	static
	enum Result uploadAttachment(const AcntId &acntId,
	    const QString &fileName, const QString &fileMimeType,
	    const QString &filePath, bool useMtomXop,
	    Isds::DmAtt &dmAtt, QString &lastError);

	const AcntId m_acntId; /*!< Account id. */
	const QString m_fileName; /*!< File name. */
	const QString m_fileMimeType; /*!< File mime type. */
	const QString m_filePath; /*!< File path. */
	const bool m_useMtomXop; /*!< True if send request as MTOM/XOP. */
};
