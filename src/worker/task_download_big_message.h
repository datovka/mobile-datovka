/*
 * Copyright (C) 2014-2025 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

#pragma once

#include <QString>

#include "src/datovka_shared/identifiers/account_id.h"
#include "src/messages.h"
#include "src/worker/task.h"

/*!
 * @brief Task describing big message download.
 */
class TaskDownloadBigMessage : public Task {
public:
	/*!
	 * @brief Return state describing what happened.
	 */
	enum Result {
		DL_SUCCESS, /*!< Operation was successful. */
		DL_ISDS_ERROR, /*!< Error communicating with ISDS. */
		DL_XML_ERROR, /*!< Error parsing XML. */
		DL_DB_INS_ERR, /*!< Error inserting into database. */
		DL_ERR /*!< Other error. */
	};

	/*!
	 * @brief Constructor.
	 *
	 * @param[in] acntId Account id.
	 * @param[in] dmId Message identifier.
	 * @param[in] msgDirect Received or sent message.
	 * @param[in] useMtomXop True if received reply as MTOM/XOP.
	 */
	explicit TaskDownloadBigMessage(const AcntId &acntId, qint64 dmId,
	    enum Messages::MessageType msgDirect, bool useMtomXop);

	/*!
	 * @brief Performs actual message download.
	 */
	virtual
	void run(void) Q_DECL_OVERRIDE;

	enum Result m_result; /*!< Return state. */
	QString m_lastError; /*!< Last error description. */

private:
	/*!
	 * Disable copy and assignment.
	 */
	TaskDownloadBigMessage(const TaskDownloadBigMessage &);
	TaskDownloadBigMessage &operator=(const TaskDownloadBigMessage &);

	/*!
	 * @brief Download whole message (envelope, attachments, raw).
	 *
	 * @param[in] acntId Account id.
	 * @param[in] dmId Message identifier.
	 * @param[in] msgDirect Received or sent message.
	 * @param[out] zfoData ZFO file content.
	 * @param[in] useMtomXop True if received reply as MTOM/XOP.
	 * @param[out] lastError Last error description.
	 * @return Error state.
	 */
	static
	enum Result downloadMessage(const AcntId &acntId, qint64 dmId,
	    enum Messages::MessageType msgDirect, bool useMtomXop,
	    QString &lastError);

	const AcntId m_acntId; /*!< Account id. */
	const qint64 m_dmId; /*!< Message ID. */
	enum Messages::MessageType m_msgDirect; /*!< Sent or received message. */
	bool m_useMtomXop; /*!< True if received reply as MTOM/XOP. */
};
