/*
 * Copyright (C) 2014-2025 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

#include <QFile>
#include <QThread>

#include "src/datovka_shared/log/log.h"
#include "src/datovka_shared/isds/message_interface2.h"
#include "src/global.h"
#include "src/io/filesystem.h"
#include "src/isds/io/downloader.h"
#include "src/isds/services/message_interface.h"
#include "src/isds/services/message_interface_offline.h" /* Isds::toMessage() */
#include "src/isds/services/message_interface_vodz.h"
#include "src/isds/session/isds_session.h"
#include "src/isds/session/isds_sessions.h"
#include "src/messages.h"
#include "src/net/db_wrapper.h"
#include "src/worker/emitter.h"
#include "src/worker/task_download_big_message.h"

TaskDownloadBigMessage::TaskDownloadBigMessage(const AcntId &acntId,
    qint64 dmId, enum Messages::MessageType msgDirect, bool useMtomXop)
    : m_result(DL_ERR),
    m_lastError(),
    m_acntId(acntId),
    m_dmId(dmId),
    m_msgDirect(msgDirect),
    m_useMtomXop(useMtomXop)
{
}

void TaskDownloadBigMessage::run(void)
{
	if (Q_UNLIKELY((Messages::TYPE_RECEIVED != m_msgDirect) &&
	        (Messages::TYPE_SENT != m_msgDirect))) {
		Q_ASSERT(0);
		return;
	}

	/* ### Worker task begin. ### */

	logDebugLv0NL("%s", "-----------DOWNLOAD BIG MESSAGE TASK-----------");
	logDebugLv0NL("Starting in thread '%p'", QThread::currentThreadId());

	m_result = downloadMessage(m_acntId, m_dmId, m_msgDirect, m_useMtomXop,
	    m_lastError);

	if (GlobInstcs::msgProcEmitterPtr != Q_NULLPTR) {
		emit GlobInstcs::msgProcEmitterPtr->downloadMessageFinishedSignal(
		    m_acntId, m_dmId,
		    TaskDownloadBigMessage::DL_SUCCESS == m_result,
		    m_lastError);
	}

	logDebugLv0NL("Finished in thread '%p'", QThread::currentThreadId());
	logDebugLv0NL("%s", "-----------------------------------------------");

	/* ### Worker task end. ### */
}

/*!
 * @brief Translate communication error onto task error.
 */
static
enum TaskDownloadBigMessage::Result error2result(enum Isds::Type::Error error)
{
	switch (error) {
	case Isds::Type::ERR_SUCCESS:
		return TaskDownloadBigMessage::DL_SUCCESS;
		break;
	case Isds::Type::ERR_ISDS:
		return TaskDownloadBigMessage::DL_ISDS_ERROR;
		break;
	case Isds::Type::ERR_SOAP:
	case Isds::Type::ERR_XML:
		return TaskDownloadBigMessage::DL_XML_ERROR;
		break;
	default:
		return TaskDownloadBigMessage::DL_ERR;
		break;
	}
}

enum TaskDownloadBigMessage::Result TaskDownloadBigMessage::downloadMessage(
    const AcntId &acntId, qint64 dmId, enum Messages::MessageType msgDirect,
    bool useMtomXop, QString &lastError)
{
	if (Q_UNLIKELY((!acntId.isValid()) ||
	        (GlobInstcs::isdsSessionsPtr == Q_NULLPTR) ||
	        (!GlobInstcs::isdsSessionsPtr->holdsSession(acntId)))) {
		Q_ASSERT(0);
		return DL_ERR;
	}

	Isds::Session *session = GlobInstcs::isdsSessionsPtr->session(acntId);
	if (Q_UNLIKELY(session == Q_NULLPTR)) {
		Q_ASSERT(0);
		return DL_ERR;
	}

	logDebugLv1NL("Sending from account '%s'",
	    acntId.username().toUtf8().constData());

	Isds::Message message;

	if (useMtomXop) {

		const QString path = joinDirs(createZfoTargetDir(acntId.username(),
		    acntId.testing(), dmId), getMsgZfoName(dmId, msgDirect));

		Isds::Downloader m_downloader;
		if (Messages::TYPE_RECEIVED == msgDirect) {
			m_downloader.getSignedReceivedBigMessage(*session,
			    dmId, path);
		} else {
			m_downloader.getSignedSentBigMessage(*session,
			    dmId, path);
		}

		/* Open file and read its content */
		QFile file(path);
		if (!file.open(QIODevice::ReadOnly)) {
			return DL_XML_ERROR;
		}

		/* Parse zfo content and detect valid ZFO format */
		message = Isds::toMessage(file.readAll());
		file.close();

	} else {

		enum Isds::Type::Error error = Isds::Type::ERR_ERROR;
		if (Messages::TYPE_RECEIVED == msgDirect) {
			error = Isds::getSignedReceivedBigMessageBase64(*session,
			    dmId, message, Q_NULLPTR, &lastError);
		} else {
			error = Isds::getSignedSentBigMessageBase64(*session,
			    dmId, message, Q_NULLPTR, &lastError);
		}
		if (Q_UNLIKELY(error != Isds::Type::ERR_SUCCESS)) {
			return error2result(error);
		}
	}

	if (Q_UNLIKELY(message.isNull())) {
		logErrorNL("SOAP: %s", lastError.toUtf8().constData());
		return DL_XML_ERROR;
	}

	if (message.documents().isEmpty()) {
		logErrorNL("SOAP: %s", "Message attachment missing.");
		return DL_ERR;
	}

	enum MessageDb::MessageType msgType = MessageDb::TYPE_RECEIVED;
	switch (msgDirect) {
	case Messages::TYPE_RECEIVED:
		msgType = MessageDb::TYPE_RECEIVED;
		break;
	case Messages::TYPE_SENT:
		msgType = MessageDb::TYPE_SENT;
		break;
	default:
		/*
		 * Any other translates to sent, but forces the application to
		 * crash in debugging mode.
		 */
		Q_ASSERT(0);
		msgType = MessageDb::TYPE_SENT;
		break;
	}

	/* Save ZFO message to the storage if not using MtomXop downloader. */
	if (!useMtomXop) {
		if (!message.raw().isEmpty()) {
			if (Q_UNLIKELY(!writeZfoToLocalStorage(
			        acntId.username(), acntId.testing(),
			        dmId, getMsgZfoName(dmId, msgDirect), message.raw()))) {
				logErrorNL("STORE: %s", "Cannot store ZFO message.");
				return DL_DB_INS_ERR;
			}
		} else {
			logErrorNL("SOAP: %s", "Message ZFO missing.");
			return DL_DB_INS_ERR;
		}
	}

	/* Store data into db */
	if (!DbWrapper::insertCompleteMessageToDb(acntId, message, msgType,
	        true, lastError)) {
		logErrorNL("DB: Insert error: %s",
		    lastError.toUtf8().constData());
		return DL_DB_INS_ERR;
	}

	enum Isds::Type::Error error = Isds::Type::ERR_ERROR;

	/*
	 * Following functions don't have to check return values
	 * because they are complementary actions for message download
	 * (not required for success complete message download).
	 */
	/* Send SOAP request to download message author info 2 */
	Isds::DmMessageAuthor msgAuthor;
	error = Isds::getMessageAuthorInfo2(*session, dmId, msgAuthor,
	    Q_NULLPTR, &lastError);
	if (Q_UNLIKELY(error == Isds::Type::ERR_SUCCESS)) {
		DbWrapper::updateAuthorInfo2(acntId, dmId, msgAuthor, lastError);
	}

	/* Send SOAP request mark message as read (only for received message) */
	if (Messages::TYPE_RECEIVED == msgDirect) {
		Isds::markMessageAsDownloaded(*session, dmId, Q_NULLPTR,
		    &lastError);
	}

	/* Clear message content. */
	message = Isds::Message();

	Isds::getSignedDeliveryInfo(*session, dmId, message,
	    Q_NULLPTR, &lastError);
	/* Store events from delivery info into database. */
	if (!message.envelope().dmEvents().isEmpty()) {
		DbWrapper::insertMesasgeDeliveryInfoToDb(acntId,
		    message.envelope().dmEvents(), dmId, lastError);
	}

	/* Save ZFO message to the storage. */
	if (!message.raw().isEmpty()) {
		if (Q_UNLIKELY(!writeZfoToLocalStorage(acntId.username(), acntId.testing(),
		        dmId, getDelInfoZfoName(dmId), message.raw()))) {
			logWarningNL("STORE: %s", "Delivery info ZFO missing.");
		}
	} else {
		logWarningNL("SOAP: %s", "Delivery info ZFO missing.");
	}

	logDebugLv1NL("%s", "Complete message has been downloaded");

	return DL_SUCCESS;
}
