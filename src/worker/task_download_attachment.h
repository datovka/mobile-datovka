/*
 * Copyright (C) 2014-2025 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

#pragma once

#include <QString>

#include "src/datovka_shared/identifiers/account_id.h"
#include "src/worker/task.h"

/*!
 * @brief Task describing downloading attachment of a big message.
 */
class TaskDownloadAttachment : public Task {
public:
	/*!
	 * @brief Return state describing what happened.
	 */
	enum Result {
		DL_SUCCESS, /*!< Operation was successful. */
		DL_ISDS_ERROR, /*!< Error communicating with ISDS. */
		DL_XML_ERROR, /*!< Error parsing XML. */
		DL_DB_INS_ERR, /*!< Error inserting into database. */
		DL_ERR /*!< Other error. */
	};

	/*!
	 * @brief Constructor.
	 *
	 * @param[in] acntId Account id.
	 * @param[in] dmId Message id.
	 * @param[in] attId Attachment id.
	 * @param[in] useMtomXop True if received reply as MTOM/XOP.
	 */
	explicit TaskDownloadAttachment(const AcntId &acntId,
	    qint64 dmId, qint64 attId, bool useMtomXop);

	/*!
	 * @brief Performs actual download.
	 */
	virtual
	void run(void) Q_DECL_OVERRIDE;

	enum Result m_result; /*!< Return state. */
	QString m_lastError; /*!< Last ISDS error message. */

private:
	/*!
	 * Disable copy and assignment.
	 */
	TaskDownloadAttachment(const TaskDownloadAttachment &);
	TaskDownloadAttachment &operator=(const TaskDownloadAttachment &);

	/*!
	 * @brief Download attachment.
	 *
	 * @param[in] acntId Account id.
	 * @param[in] dmId Message id.
	 * @param[in] attId Attachment id.
	 * @param[in] useMtomXop True if received reply as MTOM/XOP.
	 * @param[out] lastError Last ISDS error message.
	 * @return Error state.
	 */
	static
	enum Result downloadAttachment(const AcntId &acntId,
	    qint64 dmId, qint64 attId, bool useMtomXop, QString &lastError);

	const AcntId m_acntId; /*!< Account id. */
	const qint64 m_dmId; /*!< Message id. */
	const qint64 m_attId; /*!< Attachment id. */
	const bool m_useMtomXop; /*!< True if received reply as MTOM/XOP. */
};
