/*
 * Copyright (C) 2014-2025 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

#include <QThread>

#include "src/datovka_shared/log/log.h"
#include "src/global.h"
#include "src/io/filesystem.h"
#include "src/isds/io/downloader.h"
#include "src/isds/services/message_interface_vodz.h"
#include "src/isds/session/isds_session.h"
#include "src/isds/session/isds_sessions.h"
#include "src/worker/task_download_attachment.h"

TaskDownloadAttachment::TaskDownloadAttachment(const AcntId &acntId,
    qint64 dmId, qint64 attId, bool useMtomXop)
    : m_result(DL_ERR),
    m_lastError(),
    m_acntId(acntId),
    m_dmId(dmId),
    m_attId(attId),
    m_useMtomXop(useMtomXop)
{
}

void TaskDownloadAttachment::run(void)
{
	/* ### Worker task begin. ### */

	logDebugLv0NL("%s", "-----------DOWNLOAD ATTACHMENT TASK------------");
	logDebugLv0NL("Starting in thread '%p'", QThread::currentThreadId());

	m_result = downloadAttachment(m_acntId, m_dmId, m_attId, m_useMtomXop,
	    m_lastError);

	logDebugLv0NL("Finished in thread '%p'", QThread::currentThreadId());
	logDebugLv0NL("%s", "-----------------------------------------------");

	/* ### Worker task end. ### */
}

/*!
 * @brief Translate communication error onto task error.
 */
static
enum TaskDownloadAttachment::Result error2result(enum Isds::Type::Error error)
{
	switch (error) {
	case Isds::Type::ERR_SUCCESS:
		return TaskDownloadAttachment::DL_SUCCESS;
		break;
	case Isds::Type::ERR_ISDS:
		return TaskDownloadAttachment::DL_ISDS_ERROR;
		break;
	case Isds::Type::ERR_SOAP:
	case Isds::Type::ERR_XML:
		return TaskDownloadAttachment::DL_XML_ERROR;
		break;
	default:
		return TaskDownloadAttachment::DL_ERR;
		break;
	}
}

enum TaskDownloadAttachment::Result TaskDownloadAttachment::downloadAttachment(
    const AcntId &acntId, qint64 dmId, qint64 attId, bool useMtomXop,
    QString &lastError)
{
	if (Q_UNLIKELY((!acntId.isValid()) ||
	        (GlobInstcs::isdsSessionsPtr == Q_NULLPTR) ||
	        (!GlobInstcs::isdsSessionsPtr->holdsSession(acntId)))) {
		Q_ASSERT(0);
		return DL_ERR;
	}

	Isds::Session *session = GlobInstcs::isdsSessionsPtr->session(acntId);
	if (Q_UNLIKELY(session == Q_NULLPTR)) {
		Q_ASSERT(0);
		return DL_ERR;
	}

	const QString path = createZfoTargetDir(acntId.username(),
	    acntId.testing(), dmId);

	logDebugLv1NL("Sending from account '%s'",
	    acntId.username().toUtf8().constData());

	enum Isds::Type::Error error = Isds::Type::ERR_ERROR;

	if (useMtomXop) {
		Isds::Downloader m_downloader;
		if (m_downloader.downloadAttachment(*session,
		        dmId, attId, path)) {
			error = Isds::Type::ERR_SUCCESS;
		}
	} else {
		Isds::DmFile dmFile;
		error = Isds::downloadAttachmentBase64(*session,
		    dmId, attId, dmFile, Q_NULLPTR, &lastError);
	}

	if (Q_UNLIKELY(error != Isds::Type::ERR_SUCCESS)) {
		return error2result(error);
	}

	return DL_SUCCESS;
}
