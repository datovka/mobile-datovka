/*
 * Copyright (C) 2014-2025 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

#include <QDir>
#include <QMutexLocker>
#include <QSqlDatabase>
#include <QSqlError>
#include <QSqlQuery>
#include <QSqlRecord>
#include <QVariant>

#include "src/datovka_shared/log/log.h"
#include "src/io/filesystem.h"
#include "src/sqlite/zfo_db.h"
#include "src/sqlite/zfo_db_tables.h"

bool ZfoDb::openDb(const QString &fileName, bool storeToDisk)
{
	QString dirName(dfltDbAndConfigLoc());

	if (Q_UNLIKELY(storeToDisk && (dirName.isEmpty() || fileName.isEmpty()))) {
		Q_ASSERT(0);
		return false;
	}

	SQLiteDb::OpenFlags flags = SQLiteDb::CREATE_MISSING;
	flags |= storeToDisk ? SQLiteDb::NO_OPTIONS : SQLiteDb::FORCE_IN_MEMORY;

	return SQLiteDb::openDb(
	    dirName + QDir::separator() + QDir::toNativeSeparators(fileName),
	    flags);
}

QByteArray ZfoDb::getZfoContentFromDb(qint64 msgId, bool isTestAccount)
{
	QMutexLocker locker(&m_lock);
	QSqlQuery query(m_db);

	QString queryStr = "SELECT data FROM message_zfos WHERE "
	    "dmID = :msgId AND testEnv = :isTestAccount";
	if (Q_UNLIKELY(!query.prepare(queryStr))) {
		logErrorNL("Cannot prepare SQL query: %s",
		    query.lastError().text().toUtf8().constData());
		goto fail;
	}
	query.bindValue(":msgId", msgId);
	query.bindValue(":isTestAccount", isTestAccount);

	if (query.exec() && query.isActive() &&
	    query.first() && query.isValid()) {
		return query.value(0).toByteArray();
	}
fail:
	return QByteArray();
}

int ZfoDb::getZfoSizeFromDb(qint64 msgId, bool isTestAccount)
{
	QMutexLocker locker(&m_lock);
	QSqlQuery query(m_db);

	QString queryStr = "SELECT size FROM message_zfos WHERE "
	    "dmID = :msgId AND testEnv = :isTestAccount";
	if (Q_UNLIKELY(!query.prepare(queryStr))) {
		logErrorNL("Cannot prepare SQL query: %s",
		    query.lastError().text().toUtf8().constData());
		goto fail;
	}
	query.bindValue(":msgId", msgId);
	query.bindValue(":isTestAccount", isTestAccount);

	if (query.exec() && query.isActive() &&
	    query.first() && query.isValid()) {
		return query.value(0).toInt();
	}
fail:
	return 0;
}

QList<class SQLiteTbl *> ZfoDb::listOfTables(void) const
{
	QList<class SQLiteTbl *> tables;
	tables.append(&msgZfoTbl);
	tables.append(&zfoSizeCntTbl);
	return tables;
}
