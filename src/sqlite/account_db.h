/*
 * Copyright (C) 2014-2025 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

#pragma once

#include <QList>
#include <QString>

#include "src/datovka_shared/identifiers/account_id.h"
#include "src/datovka_shared/io/sqlite/db_single.h"
#include "src/datovka_shared/isds/box_interface.h"
#include "src/datovka_shared/isds/box_interface2.h"
#include "src/net/db_wrapper.h"

class QDateTime; /* Forward declaration. */

/*!
 * @brief Encapsulates account database.
 */
class AccountDb : public QObject, public SQLiteDbSingle {
	Q_OBJECT

public:
	/* Use parent class constructor. */
	using SQLiteDbSingle::SQLiteDbSingle;

	/* Make some inherited methods public. */
	using SQLiteDbSingle::closeDb;

	/*!
	 * @brief Return data-box type.
	 *
	 * @param[in] userName Account user name.
	 * @param[in] defaultValue Default value.
	 * @return Databox type.
	 */
	enum Isds::Type::DbType dbType(const QString &userName,
	    enum Isds::Type::DbType defaultValue = Isds::Type::BT_NULL) const;

	/*!
	 * @brief Return data box identifier.
	 *
	 * @param[in] userName Account user name.
	 * @param[in] defaultValue Default value.
	 * @return Databox id string.
	 */
	QString dbId(const QString &userName,
	    const QString &defaultValue = QString()) const;

	/*!
	 * @brief Delete account info from db.
	 *
	 * @param[in] userName Account user name.
	 * @return True on success.
	 */
	bool deleteAccountInfoFromDb(const QString &userName) const;

	/*!
	 * @brief Delete user info from db.
	 *
	 * @param[in] userName Account user name.
	 * @return True on success.
	 */
	bool deleteUserInfoFromDb(const QString &userName) const;

	/*!
	 * @brief Delete long term storage info from db.
	 *
	 * @param[in] dbID Data box id.
	 * @return True on success.
	 */
	bool deleteDTInfoFromDb(const QString &dbID) const;

	/*!
	 * @brief Insert account info into db.
	 *
	 * @param[in] userName Account user name.
	 * @param[in] dbOwnerInfo Account (owner) info structure.
	 * @return True on success.
	 */
	bool insertAccountInfoIntoDb(const QString &userName,
	    const Isds::DbOwnerInfoExt2 &dbOwnerInfo);

	/*!
	 * @brief Insert user info into db.
	 *
	 * @param[in] userName Account user name.
	 * @param[in] dbUserInfo User info structure.
	 * @return True on success.
	 */
	bool insertUserInfoIntoDb(const QString &userName,
	    const Isds::DbUserInfoExt2 &dbUserInfo);

	/*!
	 * @brief Insert long term storage info into db.
	 *
	 * @param[in] dbID Data box id.
	 * @param[in] dtInfo Long term storage info structure.
	 * @return True on success.
	 */
	bool insertDTInfoIntoDb(const QString &dbID,
	    const Isds::DTInfoOutput &dtInfo);

	/*!
	 * @brief Update pwd expiration in db.
	 *
	 * @note Emits dataUpdated().
	 *
	 * @param[in] userName Account user name.
	 * @param[in] testing True if testing account.
	 * @param[in] date New expiration date.
	 * @return True on success.
	 */
	bool updatePwdExpirInDb(const QString &userName, bool testing,
	    const QDateTime &date);

	/*!
	 * @brief Get databox/owner info from database.
	 *
	 * @param[in] userName Account user name.
	 * @return DbOwnerInfo structure - account info.
	 */
	Isds::DbOwnerInfoExt2 getOwnerInfo(const QString &userName) const;

	/*!
	 * @brief Get databox/user info from database.
	 *
	 * @param[in] userName Account user name.
	 * @return DbUserInfo structure - user info.
	 */
	Isds::DbUserInfoExt2 getUserInfo(const QString &userName) const;

	/*!
	 * @brief Get long term storage info from database.
	 *
	 * @param[in] dbID Data box id.
	 * @return Long term storage info structure.
	 */
	Isds::DTInfoOutput getDTInfo(const QString &dbID) const;

	/*!
	 * @brief Checks whether password expires in given period for all accounts.
	 *
	 * @param[in] nonPwdUserNames List of user names without password login.
	 * @param[in] days Amount of days to check the expiration.
	 * @return List of expired usernames with password expiration date.
	 */
	QStringList getPasswordExpirationList(const QStringList &nonPwdUserNames,
	    int days) const;

	/*!
	 * @brief Return last sync datetime.
	 *
	 * @param[in] userName Account user name.
	 * @param[in] testing True if testing account.
	 * @return Last sync datetime string.
	 */
	QString lastSyncTime(const QString &userName, bool testing) const;

	/*!
	 * @brief Update last sync datetime.
	 *
	 * @note Emits dataUpdated().
	 *
	 * @param[in] userName Account user name.
	 * @param[in] testing True if testing account.
	 * @param[in] lastSyncTime Last sync datetime.
	 * @return True on success.
	 */
	bool updateLastSyncTime(const QString &userName, bool testing,
	    const QDateTime &lastSyncTime);

	/*!
	 * @brief Return databox password expiration datetime.
	 *
	 * @param[in] userName Account user name.
	 * @return Databox password expiration datetime or null.
	 */
	QString getPwdExpirDateTime(const QString &userName) const;

Q_SIGNALS:
	/*!
	 * @brief Emitted when data updated.
	 *
	 * @param[in] acntId Account identifier.
	 */
	void dataUpdated(const AcntId &acntId);

protected:
	/*!
	 * @brief Returns list of tables.
	 *
	 * @return List of pointers to tables.
	 */
	virtual
	QList<class SQLiteTbl *> listOfTables(void) const Q_DECL_OVERRIDE;
};
