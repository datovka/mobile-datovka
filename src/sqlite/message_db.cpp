/*
 * Copyright (C) 2014-2025 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

#include <QFileInfo>
#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonValue>
#include <QMutexLocker>
#include <QPair>
#include <QStringBuilder>
#include <QSqlDatabase>
#include <QSqlError>
#include <QSqlQuery>
#include <QSqlRecord>

#include "src/auxiliaries/email_helper.h"
#include "src/datovka_shared/compat_qt/variant.h" /* nullVariantWhenIsNull */
#include "src/datovka_shared/identifiers/account_id.h"
#include "src/datovka_shared/isds/json_conversion.h"
#include "src/datovka_shared/isds/message_interface2.h"
#include "src/datovka_shared/isds/to_text_conversion.h"
#include "src/datovka_shared/isds/type_conversion.h"
#include "src/datovka_shared/log/log.h"
#include "src/datovka_shared/utility/date_time.h"
#include "src/isds/conversion/isds_conversion.h"
#include "src/models/databoxmodel.h"
#include "src/models/messagemodel.h"
#include "src/sqlite/dbs.h"
#include "src/sqlite/message_db.h"
#include "src/sqlite/message_db_tables.h"

MessageDb::MessageDb(const QString &connectionName)
    : SQLiteDb(connectionName),
    m_newDbFormat(true)
{
}

bool MessageDb::copyDb(const QString &newFileName)
{
	return SQLiteDb::copyDb(newFileName, SQLiteDb::CREATE_MISSING);
}

bool MessageDb::reopenDb(const QString &newFileName)
{
	return SQLiteDb::reopenDb(newFileName, SQLiteDb::CREATE_MISSING);
}

bool MessageDb::moveDb(const QString &newFileName)
{
	return SQLiteDb::moveDb(newFileName, SQLiteDb::CREATE_MISSING);
}

/*!
 * @brief Delete all events related to a message.
 *
 * @param[in] query SQL query to work with.
 * @param[in] msgId Message ID.
 * @param[in] newDbFormat True if is opened database with new format.
 * @return True on success.
 */
static
bool deleteEvents(QSqlQuery &query, qint64 msgId, bool newDbFormat)
{
	QString queryStr = "DELETE FROM events WHERE message_id = :dmID";
	if (Q_UNLIKELY(!newDbFormat)) {
		queryStr = "DELETE FROM events WHERE dmID = :dmID";
	}
	if (Q_UNLIKELY(!query.prepare(queryStr))) {
		logErrorNL("Cannot prepare SQL query: %s",
		    query.lastError().text().toUtf8().constData());
		return false;
	}
	query.bindValue(":dmID", msgId);
	if (Q_UNLIKELY(!query.exec())) {
		logErrorNL("Cannot execute SQL query: %s",
		    query.lastError().text().toUtf8().constData());
		return false;
	}
	return true;
}

bool MessageDb::deleteMessageFromDb(qint64 msgId, bool transaction)
{
	QMutexLocker locker(&m_lock);
	QSqlQuery query(m_db);
	QString queryStr;

	if (transaction) {
		transaction = beginTransaction();
		if (Q_UNLIKELY(!transaction)) {
			logErrorNL("%s", "Cannot begin transaction.");
			goto fail;
		}
	}

	if (Q_UNLIKELY(!deleteEvents(query, msgId, m_newDbFormat))) {
		goto fail;
	}

	queryStr = "DELETE FROM messages WHERE dmID = :dmID";
	if (Q_UNLIKELY(!query.prepare(queryStr))) {
		logErrorNL("Cannot prepare SQL query: %s",
		    query.lastError().text().toUtf8().constData());
		goto fail;
	}
	query.bindValue(":dmID", msgId);
	if (Q_UNLIKELY(!query.exec())) {
		logErrorNL("Cannot execute SQL query: %s",
		    query.lastError().text().toUtf8().constData());
		goto fail;
	}

	if (m_newDbFormat) {
		queryStr = "DELETE FROM supplementary_message_data "
		    "WHERE message_id = :dmID";
		if (Q_UNLIKELY(!query.prepare(queryStr))) {
			logErrorNL("Cannot prepare SQL query: %s",
			    query.lastError().text().toUtf8().constData());
			goto fail;
		}
		query.bindValue(":dmID", msgId);
		if (Q_UNLIKELY(!query.exec())) {
			logErrorNL("Cannot execute SQL query: %s",
			    query.lastError().text().toUtf8().constData());
			goto fail;
		}
		queryStr = "DELETE FROM events WHERE message_id = :dmID";
		if (Q_UNLIKELY(!query.prepare(queryStr))) {
			logErrorNL("Cannot prepare SQL query: %s",
			    query.lastError().text().toUtf8().constData());
			goto fail;
		}
		query.bindValue(":dmID", msgId);
		if (Q_UNLIKELY(!query.exec())) {
			logErrorNL("Cannot execute SQL query: %s",
			    query.lastError().text().toUtf8().constData());
			goto fail;
		}
	} else {
		queryStr = "DELETE FROM events WHERE dmID = :dmID";
		if (Q_UNLIKELY(!query.prepare(queryStr))) {
			logErrorNL("Cannot prepare SQL query: %s",
			    query.lastError().text().toUtf8().constData());
			goto fail;
		}
		query.bindValue(":dmID", msgId);
		if (Q_UNLIKELY(!query.exec())) {
			logErrorNL("Cannot execute SQL query: %s",
			    query.lastError().text().toUtf8().constData());
			goto fail;
		}
	}

	if (transaction) {
		commitTransaction();
	}
	return true;

fail:
	if (transaction) {
		rollbackTransaction();
	}
	return false;
}

void MessageDb::getContactsFromDb(DataboxListModel *dbModel, const QString &dbId)
{
	if (Q_UNLIKELY(dbModel == Q_NULLPTR)) {
		Q_ASSERT(0);
		return;
	}

	QMutexLocker locker(&m_lock);
	QSqlQuery query(m_db);

	QString queryStr = "SELECT "
	    "dbIDRecipient, dmRecipient, dmRecipientAddress FROM messages "
	    "WHERE (dmRecipientAddress IS NOT NULL) "
	    " UNION SELECT "
	    "dbIDSender, dmSender, dmSenderAddress FROM messages "
	    "WHERE (dmSenderAddress IS NOT NULL) "
	    "ORDER BY dmRecipient, dmSender DESC";
	if (Q_UNLIKELY(!query.prepare(queryStr))) {
		logErrorNL("Cannot prepare SQL query: %s",
		    query.lastError().text().toUtf8().constData());
		return;
	}
	if (query.exec() && query.isActive()) {
		dbModel->setQuery(query, dbId, false);
	}
}

int MessageDb::getDbSizeInBytes(void)
{
	QFileInfo fi(m_db.databaseName());
	return fi.size();
}

QSet<qint64> MessageDb::getAllMessageIDsFromDb(void)
{
	QMutexLocker locker(&m_lock);
	QSqlQuery query(m_db);
	QSet<qint64> msgIDList;

	QString queryStr = "SELECT dmID FROM messages";
	if (Q_UNLIKELY(!query.prepare(queryStr))) {
		logErrorNL("Cannot prepare SQL query: %s",
		    query.lastError().text().toUtf8().constData());
		return QSet<qint64>();
	}
	if (query.exec() && query.isActive()) {
		query.first();
		while (query.isValid()) {
			msgIDList.insert(query.value(0).toLongLong());
			query.next();
		}
	} else {
		return QSet<qint64>();
	}

	return msgIDList;
}

short MessageDb::getMessageStatusFromDb(qint64 msgId, bool &hasFiles) const
{
	QMutexLocker locker(&m_lock);
	QSqlQuery query(m_db);
	int ret = -1;

	QString queryStr = "SELECT dmMessageStatus, _origin "
	    "FROM messages WHERE dmID = :dmID";
	if (Q_UNLIKELY(!m_newDbFormat)) {
		queryStr = "SELECT dmMessageStatus, _dmAttachDownloaded "
		    "FROM messages WHERE dmID = :dmID";
	}
	if (Q_UNLIKELY(!query.prepare(queryStr))) {
		logErrorNL("Cannot prepare SQL query: %s",
		    query.lastError().text().toUtf8().constData());
		return false;
	}
	query.bindValue(":dmID", msgId);
	if (query.exec() && query.isActive()) {
		query.first();
		if (query.isValid()) {
			ret = query.value(0).toInt();
			hasFiles = (query.value(1).toString()
			    == "tReturnedMessage");
		}
	}
	return ret;
}

void MessageDb::setMessageListModelFromDb(MessageListModel *msgModel,
    const AcntId &acntId, enum MessageType messageType)
{
	if (Q_UNLIKELY(msgModel == Q_NULLPTR)) {
		Q_ASSERT(0);
		return;
	}

	QMutexLocker locker(&m_lock);
	QSqlQuery query(m_db);

	QString queryStr = "SELECT m.dmID, m.dmSender, m.dmRecipient, "
	    "m.dmAnnotation, m.dmDeliveryTime, m.dmAcceptanceTime, "
	    "s.read_locally, m._origin, s.message_type "
	    "FROM messages AS m LEFT JOIN supplementary_message_data AS s "
	    "ON (m.dmID = s.message_id) "
	    "WHERE s.message_type = :_dmMessageType ORDER BY dmID DESC";
	if (Q_UNLIKELY(!m_newDbFormat)) {
		queryStr = "SELECT dmID, dmSender, dmRecipient, "
		    "dmAnnotation, dmDeliveryTime, dmAcceptanceTime, "
		    "_dmReadLocally, _dmAttachDownloaded, _dmMessageType "
		    "FROM messages WHERE "
		    "_dmMessageType = :_dmMessageType ORDER BY dmID DESC";
	}
	if (Q_UNLIKELY(!query.prepare(queryStr))) {
		logErrorNL("Cannot prepare SQL query: %s",
		    query.lastError().text().toUtf8().constData());
		return;
	}
	query.bindValue(":_dmMessageType", messageType);
	if (query.exec() && query.isActive()) {
		msgModel->setQuery(acntId.username(), acntId.testing(),
		    query, false);
	}
}

int MessageDb::getNewMessageCountFromDb(enum MessageType messageType)
{
	QMutexLocker locker(&m_lock);
	QSqlQuery query(m_db);

	QString queryStr = "SELECT COUNT(*) FROM supplementary_message_data "
	    "WHERE message_type = :_dmMessageType "
	    "AND read_locally = :_dmReadLocally";
	if (Q_UNLIKELY(!m_newDbFormat)) {
		queryStr = "SELECT COUNT(*) FROM messages WHERE "
		    "_dmMessageType = :_dmMessageType "
		    "AND _dmReadLocally = :_dmReadLocally";
	}
	if (Q_UNLIKELY(!query.prepare(queryStr))) {
		logErrorNL("Cannot prepare SQL query: %s",
		    query.lastError().text().toUtf8().constData());
		return 0;
	}
	query.bindValue(":_dmMessageType", messageType);
	query.bindValue(":_dmReadLocally", false);
	if (query.exec() && query.isActive() &&
	    query.first() && query.isValid()) {
		return query.value(0).toInt();
	}
	return 0;
}

int MessageDb::getMessageCountFromDb(enum MessageType messageType)
{
	QMutexLocker locker(&m_lock);
	QSqlQuery query(m_db);

	QString queryStr = "SELECT COUNT(*) FROM supplementary_message_data "
	    "WHERE message_type = :_dmMessageType";
	if (Q_UNLIKELY(!m_newDbFormat)) {
		queryStr = "SELECT COUNT(*) FROM messages WHERE "
		   "_dmMessageType = :_dmMessageType";
	}
	if (Q_UNLIKELY(!query.prepare(queryStr))) {
		logErrorNL("Cannot prepare SQL query: %s",
		    query.lastError().text().toUtf8().constData());
		return 0;
	}
	query.bindValue(":_dmMessageType", messageType);
	if (query.exec() && query.isActive() &&
	    query.first() && query.isValid()) {
		return query.value(0).toInt();
	}
	return 0;
}

Isds::Envelope MessageDb::getMessageEnvelopeFromDb(qint64 dmId)
{
	QMutexLocker locker(&m_lock);
	QSqlQuery query(m_db);
	Isds::Envelope envelope;
	QList<Isds::Event> events;

	QString queryStr = "SELECT dmAnnotation, "
	    "dbIDSender, dmSender, dmSenderAddress, dmSenderType, "
	    "dmSenderOrgUnit, dmSenderOrgUnitNum, dmSenderRefNumber, "
	    "dmSenderIdent, dbIDRecipient, dmRecipient, dmRecipientAddress, "
	    "dmRecipientOrgUnit, dmRecipientOrgUnitNum, dmAmbiguousRecipient, "
	    "dmRecipientRefNumber, dmRecipientIdent, dmLegalTitleLaw, "
	    "dmLegalTitleYear, dmLegalTitleSect, dmLegalTitlePar, "
	    "dmLegalTitlePoint, dmToHands, dmPersonalDelivery, "
	    "dmAllowSubstDelivery, dmQTimestamp, dmDeliveryTime, "
	    "dmAcceptanceTime, dmMessageStatus, dmAttachmentSize, _dmType "
	    "FROM messages WHERE dmID = :dmID";
	if (Q_UNLIKELY(!m_newDbFormat)) {
		queryStr = "SELECT dmAnnotation, "
		    "dbIDSender, dmSender, dmSenderAddress, dmSenderType, "
		    "dmSenderOrgUnit, dmSenderOrgUnitNum, dmSenderRefNumber, "
		    "dmSenderIdent, dbIDRecipient, dmRecipient, dmRecipientAddress, "
		    "dmRecipientOrgUnit, dmRecipientOrgUnitNum, dmAmbiguousRecipient, "
		    "dmRecipientRefNumber, dmRecipientIdent, dmLegalTitleLaw, "
		    "dmLegalTitleYear, dmLegalTitleSect, dmLegalTitlePar, "
		    "dmLegalTitlePoint, dmToHands, dmPersonalDelivery, "
		    "dmAllowSubstDelivery, dmQTimestamp, dmDeliveryTime, "
		    "dmAcceptanceTime, dmMessageStatus, dmAttachmentSize, dmType "
		    "FROM messages WHERE dmID = :dmID";
	}
	if (Q_UNLIKELY(!query.prepare(queryStr))) {
		return Isds::Envelope();
	}
	query.bindValue(":dmID", dmId);
	if (query.exec() && query.isActive() &&
	    query.first() && query.isValid()) {
		envelope.setDmId(dmId);
		envelope.setDmID(QString::number(dmId));
		envelope.setDmAnnotation(query.value(0).toString());
		envelope.setDbIDSender(query.value(1).toString());
		envelope.setDmSender(query.value(2).toString());
		envelope.setDmSenderAddress(query.value(3).toString());
		envelope.setDmSenderType(
		    Isds::intVariant2DbType(query.value(4)));
		envelope.setDmSenderOrgUnit(query.value(5).toString());
		envelope.setDmSenderOrgUnitNumStr(query.value(6).toString());
		envelope.setDmSenderRefNumber(query.value(7).toString());
		envelope.setDmSenderIdent(query.value(8).toString());
		envelope.setDbIDRecipient(query.value(9).toString());
		envelope.setDmRecipient(query.value(10).toString());
		envelope.setDmRecipientAddress(query.value(11).toString());
		envelope.setDmRecipientOrgUnit(query.value(12).toString());
		envelope.setDmRecipientOrgUnitNumStr(
		    query.value(13).toString());
		envelope.setDmAmbiguousRecipient(
		    Isds::variant2NilBool(query.value(14)));
		envelope.setDmRecipientRefNumber(query.value(15).toString());
		envelope.setDmRecipientIdent(query.value(16).toString());
		envelope.setDmLegalTitleLawStr(query.value(17).toString());
		envelope.setDmLegalTitleYearStr(query.value(18).toString());
		envelope.setDmLegalTitleSect(query.value(19).toString());
		envelope.setDmLegalTitlePar(query.value(20).toString());
		envelope.setDmLegalTitlePoint(query.value(21).toString());
		envelope.setDmToHands(query.value(22).toString());
		envelope.setDmPersonalDelivery(
		     Isds::variant2NilBool(query.value(23)));
		envelope.setDmAllowSubstDelivery(
		    Isds::variant2NilBool(query.value(24)));
		envelope.setDmQTimestamp(query.value(25).toByteArray());
		envelope.setDmDeliveryTime(
		    dateTimeFromDbFormat(query.value(26).toString()));
		envelope.setDmAcceptanceTime(
		    dateTimeFromDbFormat(query.value(27).toString()));
		envelope.setDmMessageStatus(
		    Isds::variant2DmState(query.value(28)));
		envelope.setDmAttachmentSize(query.value(29).toLongLong());
		envelope.setDmType(Isds::variant2Char(query.value(30)));
	}

	queryStr = "SELECT dmEventTime, dmEventDescr"
	    " FROM events WHERE message_id = :dmID"
	    " ORDER BY dmEventTime ASC";
	if (Q_UNLIKELY(!m_newDbFormat)) {
		queryStr = "SELECT dmEventTime, dmEventDescr"
		    " FROM events WHERE dmID = :dmID"
		    " ORDER BY dmEventTime ASC";
	}
	if (Q_UNLIKELY(!query.prepare(queryStr))) {
		return Isds::Envelope();
	}
	query.bindValue(":dmID", dmId);
	if (query.exec() && query.isActive()) {
		query.first();
		while (query.isValid()) {
			Isds::Event event;
			event.setTime(dateTimeFromDbFormat(query.value(0).toString()));
			event.setDescr(query.value(1).toString());
			events.append(event);
			query.next();
		}
	} else {
		return Isds::Envelope();
	}

	envelope.setDmEvents(events);

	return envelope;
}

bool MessageDb::getMessageEmailDataFromDb(qint64 dmId, QString &body,
    QString &subject)
{
	QMutexLocker locker(&m_lock);
	QSqlQuery query(m_db);
	QString queryStr;

	queryStr = "SELECT dmSender, dmRecipient, dmAcceptanceTime, "
	    "dmAnnotation FROM messages WHERE dmID = :dmID";
	if (Q_UNLIKELY(!query.prepare(queryStr))) {
		return false;
	}
	query.bindValue(":dmID", dmId);
	if (query.exec() && query.isActive() &&
	    query.first() && query.isValid()) {
		body = generateEmailBodyText(dmId, query.value(0).toString(),
		    query.value(1).toString(), dateTimeStrFromDbFormat(
		    query.value(2).toString(), DATETIME_QML_FORMAT));
		subject = query.value(3).toString();
		return true;
	}
	return false;
}

/*!
 * @brief Insert or update event.
 *
 * @param[in] query SQL query to work with.
 * @param[in] msgId Message ID.
 * @param[in] event Event structure.
 * @param[in] newDbFormat True if is opened database with new format.
 * @return True on success.
 */
static
bool insertEvent(QSqlQuery &query, qint64 msgId, const Isds::Event &event,
    bool newDbFormat)
{
	QString queryStr = "INSERT INTO events "
	    "(message_id, dmEventTime, dmEventDescr) "
	    "VALUES (:dmID, :dmEventTime, :dmEventDescr)";
	if (Q_UNLIKELY(!newDbFormat)) {
		queryStr = "INSERT INTO events "
		    "(dmID, dmEventTime, dmEventDescr) "
		    "VALUES (:dmID, :dmEventTime, :dmEventDescr)";
	}
	if (Q_UNLIKELY(!query.prepare(queryStr))) {
		logErrorNL("Cannot prepare SQL query: %s",
		    query.lastError().text().toUtf8().constData());
		return false;
	}
	query.bindValue(":dmID", msgId);
	query.bindValue(":dmEventTime", nullVariantWhenIsNull(
	    dateTimeToDbFormatStr(event.time())));
	query.bindValue(":dmEventDescr", nullVariantWhenIsNull(event.descr()));
	if (Q_UNLIKELY(!query.exec())) {
		logErrorNL("Cannot execute SQL query: %s",
		    query.lastError().text().toUtf8().constData());
		return false;
	}

	return true;
}

bool MessageDb::setMessageEvents(qint64 msgId, const QList<Isds::Event> &events,
    bool transaction)
{
	QMutexLocker locker(&m_lock);
	QSqlQuery query(m_db);

	if (transaction) {
		transaction = beginTransaction();
		if (Q_UNLIKELY(!transaction)) {
			logErrorNL("%s", "Cannot begin transaction.");
			goto fail;
		}
	}

	if (Q_UNLIKELY(!deleteEvents(query, msgId, m_newDbFormat))) {
		goto fail;
	}

	foreach (const Isds::Event &event, events) {
		if (Q_UNLIKELY(!insertEvent(query, msgId, event,
		        m_newDbFormat))) {
			goto fail;
		}
	}

	if (transaction) {
		commitTransaction();
	}
	return true;

fail:
	if (transaction) {
		rollbackTransaction();
	}
	return false;
}

bool MessageDb::insertOrUpdateMessageEnvelopeInDb(qint64 msgId,
    enum MessageType messageType, const Isds::Envelope &envelope, bool verified)
{
	QMutexLocker locker(&m_lock);
	QSqlQuery query(m_db);
	qint64 dmID = -1;

	QString queryStr = "SELECT dmID FROM messages WHERE dmID = :dmID";
	if (Q_UNLIKELY(!query.prepare(queryStr))) {
		logErrorNL("Cannot prepare SQL query: %s",
		    query.lastError().text().toUtf8().constData());
		goto fail;
	}
	query.bindValue(":dmID", msgId);
	if (query.exec() && query.isActive()) {
		query.first();
		if (query.isValid()) {
			dmID = query.value(0).toLongLong();
		}
	} else {
		logErrorNL("Cannot execute SQL query: %s",
		    query.lastError().text().toUtf8().constData());
		goto fail;
	}

	// update message envelope
	if (-1 != dmID) {
		return updateMessageEnvelopeInDb(envelope, verified);
	}

	queryStr = "INSERT INTO messages (dmID, "
	    "_origin, dbIDSender, dmSender, dmSenderAddress, dmSenderType, "
	    "dmRecipient, dmRecipientAddress, dmAmbiguousRecipient, "
	    "dmSenderOrgUnit, dmSenderOrgUnitNum, dbIDRecipient, "
	    "dmRecipientOrgUnit, dmRecipientOrgUnitNum, dmToHands, dmAnnotation, "
	    "dmRecipientRefNumber, dmSenderRefNumber, dmRecipientIdent, "
	    "dmSenderIdent, dmLegalTitleLaw, dmLegalTitleYear, "
	    "dmLegalTitleSect, dmLegalTitlePar, dmLegalTitlePoint, "
	    "dmPersonalDelivery, dmAllowSubstDelivery, dmQTimestamp, "
	    "dmDeliveryTime, dmAcceptanceTime, dmMessageStatus, "
	    "dmAttachmentSize, _dmType"
	    ") VALUES ("
	    ":dmID, :_origin, :dbIDSender, :dmSender, "
	    ":dmSenderAddress, :dmSenderType, :dmRecipient, "
	    ":dmRecipientAddress, :dmAmbiguousRecipient, :dmSenderOrgUnit, "
	    ":dmSenderOrgUnitNum, :dbIDRecipient, :dmRecipientOrgUnit, "
	    ":dmRecipientOrgUnitNum, :dmToHands, :dmAnnotation, "
	    ":dmRecipientRefNumber, :dmSenderRefNumber, :dmRecipientIdent, "
	    ":dmSenderIdent, :dmLegalTitleLaw, :dmLegalTitleYear, "
	    ":dmLegalTitleSect, :dmLegalTitlePar, :dmLegalTitlePoint,"
	    ":dmPersonalDelivery, :dmAllowSubstDelivery, :dmQTimestamp, "
	    ":dmDeliveryTime, :dmAcceptanceTime, :dmMessageStatus, "
	    ":dmAttachmentSize, :dmType"
	    ")";
	if (Q_UNLIKELY(!m_newDbFormat)) {
		queryStr = "INSERT INTO messages ("
		    "dmID, dbIDSender, dmSender, dmSenderAddress, dmSenderType, "
		    "dmRecipient, dmRecipientAddress, dmAmbiguousRecipient, "
		    "dmSenderOrgUnit, dmSenderOrgUnitNum, dbIDRecipient, "
		    "dmRecipientOrgUnit, dmRecipientOrgUnitNum, dmToHands, dmAnnotation, "
		    "dmRecipientRefNumber, dmSenderRefNumber, dmRecipientIdent, "
		    "dmSenderIdent, dmLegalTitleLaw, dmLegalTitleYear, "
		    "dmLegalTitleSect, dmLegalTitlePar, dmLegalTitlePoint, "
		    "dmPersonalDelivery, dmAllowSubstDelivery, dmQTimestamp, "
		    "dmDeliveryTime, dmAcceptanceTime, dmMessageStatus, "
		    "dmAttachmentSize, dmType, _dmMessageType, _dmDownloadDate, "
		    "_dmCustomData, _dmAttachDownloaded, _dmReadLocally"
		    ") VALUES ("
		    ":dmID, :dbIDSender, :dmSender, "
		    ":dmSenderAddress, :dmSenderType, :dmRecipient, "
		    ":dmRecipientAddress, :dmAmbiguousRecipient, :dmSenderOrgUnit, "
		    ":dmSenderOrgUnitNum, :dbIDRecipient, :dmRecipientOrgUnit, "
		    ":dmRecipientOrgUnitNum, :dmToHands, :dmAnnotation, "
		    ":dmRecipientRefNumber, :dmSenderRefNumber, :dmRecipientIdent, "
		    ":dmSenderIdent, :dmLegalTitleLaw, :dmLegalTitleYear, "
		    ":dmLegalTitleSect, :dmLegalTitlePar, :dmLegalTitlePoint,"
		    ":dmPersonalDelivery, :dmAllowSubstDelivery, :dmQTimestamp, "
		    ":dmDeliveryTime, :dmAcceptanceTime, :dmMessageStatus, "
		    ":dmAttachmentSize, :dmType, :_dmMessageType, :_dmDownloadDate, "
		    ":_dmCustomData, :_dmAttachDownloaded, :_dmReadLocally"
		    ")";
	}
	if (Q_UNLIKELY(!query.prepare(queryStr))) {
		logErrorNL("Cannot prepare SQL query: %s",
		    query.lastError().text().toUtf8().constData());
		goto fail;
	}
	query.bindValue(":dmID", msgId);
	if (m_newDbFormat) {
		query.bindValue(":_origin",
		    (verified) ? "tReturnedMessage": "tRecord");
	}
	query.bindValue(":dbIDSender", nullVariantWhenIsNull(envelope.dbIDSender()));
	query.bindValue(":dmSender", nullVariantWhenIsNull(envelope.dmSender()));
	query.bindValue(":dmSenderAddress", nullVariantWhenIsNull(envelope.dmSenderAddress()));
	query.bindValue(":dmSenderType", Isds::dbType2IntVariant(envelope.dmSenderType()));
	query.bindValue(":dmRecipient", envelope.dmRecipient());
	query.bindValue(":dmRecipientAddress", envelope.dmRecipientAddress());
	query.bindValue(":dmAmbiguousRecipient", Isds::nilBool2Variant(envelope.dmAmbiguousRecipient()));
	query.bindValue(":dmSenderOrgUnit", nullVariantWhenIsNull(envelope.dmSenderOrgUnit()));
	query.bindValue(":dmSenderOrgUnitNum", nullVariantWhenIsNull(envelope.dmSenderOrgUnitNumStr()));
	query.bindValue(":dbIDRecipient", nullVariantWhenIsNull(envelope.dbIDRecipient()));
	query.bindValue(":dmRecipientOrgUnit", nullVariantWhenIsNull(envelope.dmRecipientOrgUnit()));
	query.bindValue(":dmRecipientOrgUnitNum", nullVariantWhenIsNull(envelope.dmRecipientOrgUnitNumStr()));
	query.bindValue(":dmToHands", nullVariantWhenIsNull(envelope.dmToHands()));
	query.bindValue(":dmAnnotation", nullVariantWhenIsNull(envelope.dmAnnotation()));
	query.bindValue(":dmRecipientRefNumber", nullVariantWhenIsNull(envelope.dmRecipientRefNumber()));
	query.bindValue(":dmSenderRefNumber", nullVariantWhenIsNull(envelope.dmSenderRefNumber()));
	query.bindValue(":dmRecipientIdent", nullVariantWhenIsNull(envelope.dmRecipientIdent()));
	query.bindValue(":dmSenderIdent", nullVariantWhenIsNull(envelope.dmSenderIdent()));
	query.bindValue(":dmLegalTitleLaw", nullVariantWhenIsNull(envelope.dmLegalTitleLawStr()));
	query.bindValue(":dmLegalTitleYear", nullVariantWhenIsNull(envelope.dmLegalTitleYearStr()));
	query.bindValue(":dmLegalTitleSect", nullVariantWhenIsNull(envelope.dmLegalTitleSect()));
	query.bindValue(":dmLegalTitlePar", nullVariantWhenIsNull(envelope.dmLegalTitlePar()));
	query.bindValue(":dmLegalTitlePoint", nullVariantWhenIsNull(envelope.dmLegalTitlePoint()));
	query.bindValue(":dmPersonalDelivery", Isds::nilBool2Variant(envelope.dmPersonalDelivery()));
	query.bindValue(":dmAllowSubstDelivery", Isds::nilBool2Variant(envelope.dmAllowSubstDelivery()));
	query.bindValue(":dmQTimestamp", nullVariantWhenIsNull(QString()));
	query.bindValue(":dmDeliveryTime", nullVariantWhenIsNull(dateTimeToDbFormatStr(envelope.dmDeliveryTime())));
	query.bindValue(":dmAcceptanceTime", nullVariantWhenIsNull(dateTimeToDbFormatStr(envelope.dmAcceptanceTime())));
	query.bindValue(":dmMessageStatus", Isds::dmState2Variant(envelope.dmMessageStatus()));
	query.bindValue(":dmAttachmentSize", Isds::nonNegativeLong2Variant(envelope.dmAttachmentSize()));
	query.bindValue(":dmType", (!envelope.dmType().isNull()) ? envelope.dmType() : QVariant());
	if (Q_UNLIKELY(!m_newDbFormat)) {
		query.bindValue(":_dmMessageType", messageType);
		query.bindValue(":_dmDownloadDate", nullVariantWhenIsNull(QDateTime::currentDateTime().toString(DATETIME_DB_FORMAT)));
		query.bindValue(":_dmCustomData", nullVariantWhenIsNull(QString()));
		query.bindValue(":_dmAttachDownloaded", false);
		query.bindValue(":_dmReadLocally", false);
	}
	if (Q_UNLIKELY(!query.exec())) {
		logErrorNL("Cannot execute SQL query: %s",
		    query.lastError().text().toUtf8().constData());
		goto fail;
	}

	if (m_newDbFormat) {
		queryStr = "INSERT INTO supplementary_message_data ("
		    "message_id, message_type, read_locally, download_date,"
		    "custom_data, dmVODZ, attsNum) VALUES "
		    "(:dmID, :_dmMessageType, :_dmReadLocally, "
		    ":_dmDownloadDate, :_dmCustomData, :_dmVODZ, :_attsNum)";
		if (Q_UNLIKELY(!query.prepare(queryStr))) {
			logErrorNL("Cannot prepare SQL query: %s.",
			    query.lastError().text().toUtf8().constData());
			goto fail;
		}
		query.bindValue(":dmID", msgId);
		query.bindValue(":_dmMessageType", messageType);
		query.bindValue(":_dmReadLocally", false);
		query.bindValue(":_dmDownloadDate", nullVariantWhenIsNull(QDateTime::currentDateTime().toString(DATETIME_DB_FORMAT)));
		query.bindValue(":_dmCustomData", nullVariantWhenIsNull(QString()));
		query.bindValue(":_dmVODZ", Isds::nilBool2Variant(envelope.dmVODZ()));
		query.bindValue(":_attsNum", Isds::nonNegativeLong2Variant(envelope.attsNum()));
		if (Q_UNLIKELY(!query.exec())) {
			logErrorNL("Cannot execute SQL query: %s",
			    query.lastError().text().toUtf8().constData());
			goto fail;
		}
	}

	return true;
fail:
	return false;
}

bool MessageDb::markMessageLocallyRead(qint64 msgId, bool read)
{
	QMutexLocker locker(&m_lock);
	QSqlQuery query(m_db);

	QString queryStr =
	    "UPDATE supplementary_message_data SET "
	    "read_locally = :_dmReadLocally WHERE message_id = :dmID";
	if (Q_UNLIKELY(!m_newDbFormat)) {
		queryStr = "UPDATE messages SET "
		    "_dmReadLocally = :_dmReadLocally WHERE dmID = :dmID";
	}
	if (Q_UNLIKELY(!query.prepare(queryStr))) {
		logErrorNL("Cannot prepare SQL query: %s",
		    query.lastError().text().toUtf8().constData());
		return false;
	}
	query.bindValue(":dmID", msgId);
	query.bindValue(":_dmReadLocally", read);
	if (Q_UNLIKELY(!query.exec())) {
		logErrorNL("Cannot execute SQL query: %s",
		    query.lastError().text().toUtf8().constData());
		return false;
	}
	return true;
}

bool MessageDb::markMessagesLocallyRead(enum MessageType messageType, bool read)
{
	QMutexLocker locker(&m_lock);
	QSqlQuery query(m_db);

	QString queryStr = "UPDATE supplementary_message_data SET "
	    "read_locally = :_dmReadLocally "
	    "WHERE message_type = :_dmMessageType";
	if (Q_UNLIKELY(!m_newDbFormat)) {
		queryStr = "UPDATE messages SET "
		    "_dmReadLocally = :_dmReadLocally "
		    "WHERE _dmMessageType = :_dmMessageType";
	}
	if (Q_UNLIKELY(!query.prepare(queryStr))) {
		logErrorNL("Cannot prepare SQL query: %s",
		    query.lastError().text().toUtf8().constData());
		return false;
	}
	query.bindValue(":_dmReadLocally", read);
	query.bindValue(":_dmMessageType", messageType);
	if (Q_UNLIKELY(!query.exec())) {
		logErrorNL("Cannot execute SQL query: %s",
		    query.lastError().text().toUtf8().constData());
		return false;
	}
	return true;
}

/*!
 * @brief Detect if a database file is in the new format.
 *
 * @param[in] fileName Database file name.
 * @return True if database file is in the new format.
 */
static
bool isNewDatabase(const QString &fileName)
{
	QFileInfo fi(fileName);
	QString baseName(fi.baseName());

	const QRegularExpression re("^[a-zA-Z0-9]+___[0-1]$");
	return re.match(baseName).hasMatch();
}

bool MessageDb::openDb(const QString &fileName, bool storeToDisk)
{
	m_newDbFormat = isNewDatabase(fileName);

	SQLiteDb::OpenFlags flags = SQLiteDb::CREATE_MISSING;
	flags |= storeToDisk ? SQLiteDb::NO_OPTIONS : SQLiteDb::FORCE_IN_MEMORY;

	return SQLiteDb::openDb(fileName, flags);
}

bool MessageDb::openDbNoNewTables(const QString &fileName, bool storeToDisk)
{
	m_newDbFormat = isNewDatabase(fileName);

	SQLiteDb::OpenFlags flags =
	    (storeToDisk ? SQLiteDb::NO_OPTIONS : SQLiteDb::FORCE_IN_MEMORY);
	return SQLiteDb::openDb(fileName, flags);
}

int MessageDb::searchMessagesAndSetModelFromDb(MessageListModel *msgModel,
    const AcntId &acntId, const QString &phrase,
    enum MessageType messageType, const QList<qint64> &msgIds)
{
	if (Q_UNLIKELY(msgModel == Q_NULLPTR)) {
		Q_ASSERT(0);
		return 0;
	}

	QMutexLocker locker(&m_lock);
	QSqlQuery query(m_db);
	QString queryStr;
	int resultCnt = 0;

	if (!msgIds.isEmpty()) {
		/*
		 * Append messages into model where attachment name
		 * contains search phrase.
		 */
		queryStr = "SELECT "
		    "m.dmID, m.dmSender, m.dmRecipient, m.dmAnnotation, "
		    "m.dmDeliveryTime, m.dmAcceptanceTime, s.read_locally, "
		    "m._origin, s.message_type "
		    "FROM messages AS m LEFT JOIN supplementary_message_data AS s "
		    "ON (m.dmID = s.message_id) "
		    "WHERE s.message_type = :_dmMessageType "
		    "AND m.dmID IN (";
		if (Q_UNLIKELY(!m_newDbFormat)) {
			queryStr = "SELECT dmID, dmSender, dmRecipient, "
			    "dmAnnotation, dmDeliveryTime, dmAcceptanceTime, "
			    "_dmReadLocally, _dmAttachDownloaded, _dmMessageType "
			    "FROM messages WHERE _dmMessageType = :_dmMessageType "
			    "AND dmID IN (";
		}
		for (int i = 0; i < msgIds.count(); ++i) {
			queryStr += QString::number(msgIds.at(i));
			if (i != (msgIds.count() - 1)) {
				queryStr += ", ";
			}
		}
		queryStr += ") ORDER BY dmID DESC";
		if (Q_UNLIKELY(!query.prepare(queryStr))) {
			logErrorNL("Cannot prepare SQL query: %s",
			    query.lastError().text().toUtf8().constData());
			return resultCnt;
		}
		query.bindValue(":_dmMessageType", messageType);
		if (query.exec() && query.isActive()) {
			resultCnt += msgModel->setQuery(acntId.username(),
			    acntId.testing(), query, true);
		}
	}

	/* Search in message envelopes and append resutls into model */
	queryStr = "SELECT DISTINCT m.dmID, m.dmSender, m.dmRecipient, "
	    "m.dmAnnotation, m.dmDeliveryTime, m.dmAcceptanceTime, "
	    "s.read_locally, m._origin, s.message_type "
	    "FROM messages AS m LEFT JOIN supplementary_message_data AS s "
	    "ON (m.dmID = s.message_id) "
	    "WHERE s.message_type = :_dmMessageType AND "
	    "("
	    "m.dmID LIKE '%'||:_phrase||'%' OR "
	    "m.dmSender LIKE '%'||:_phrase||'%' OR "
	    "m.dmRecipient LIKE '%'||:_phrase||'%' OR "
	    "m.dmAnnotation LIKE '%'||:_phrase||'%' OR "
	    "m.dmRecipientRefNumber LIKE '%'||:_phrase||'%' OR "
	    "m.dmSenderRefNumber LIKE '%'||:_phrase||'%' OR "
	    "m.dmRecipientIdent LIKE '%'||:_phrase||'%' OR "
	    "m.dmSenderIdent LIKE '%'||:_phrase||'%' OR "
	    "m.dmToHands LIKE '%'||:_phrase||'%' OR "
	    "m.dmDeliveryTime LIKE '%'||:_phrase||'%') "
	    "ORDER BY m.dmID DESC";
	if (Q_UNLIKELY(!m_newDbFormat)) {
		queryStr = "SELECT DISTINCT dmID, dmSender, dmRecipient, "
		    "dmAnnotation, dmDeliveryTime, dmAcceptanceTime, "
		    "_dmReadLocally, _dmAttachDownloaded, _dmMessageType "
		    "FROM messages WHERE _dmMessageType = :_dmMessageType AND "
		    "(dmID LIKE '%'||:_phrase||'%' OR "
		    "dmSender LIKE '%'||:_phrase||'%' OR "
		    "dmRecipient LIKE '%'||:_phrase||'%' OR "
		    "dmAnnotation LIKE '%'||:_phrase||'%' OR "
		    "dmRecipientRefNumber LIKE '%'||:_phrase||'%' OR "
		    "dmSenderRefNumber LIKE '%'||:_phrase||'%' OR "
		    "dmRecipientIdent LIKE '%'||:_phrase||'%' OR "
		    "dmSenderIdent LIKE '%'||:_phrase||'%' OR "
		    "dmToHands LIKE '%'||:_phrase||'%' OR "
		    "dmDeliveryTime LIKE '%'||:_phrase||'%') "
		    "ORDER BY dmID DESC";
	}
	if (Q_UNLIKELY(!query.prepare(queryStr))) {
		logErrorNL("Cannot prepare SQL query: %s",
		    query.lastError().text().toUtf8().constData());
		return resultCnt;
	}
	query.bindValue(":_phrase", phrase);
	query.bindValue(":_dmMessageType", messageType);
	if (query.exec() && query.isActive()) {
		resultCnt += msgModel->setQuery(acntId.username(),
		    acntId.testing(), query, true);
	} else {
		/*
		 * SQL query can return empty search result. It is not error.
		 * Show log information like warning.
		*/
		logWarningNL(
		    "Cannot execute SQL query or empty search result: %s",
		    query.lastError().text().toUtf8().constData());
	}

	return resultCnt;
}

bool MessageDb::setAttachmentDownloaded(qint64 msgId, bool downloaded)
{
	QMutexLocker locker(&m_lock);
	QSqlQuery query(m_db);

	QString queryStr = "UPDATE messages SET "
	    "_origin = :_origin WHERE dmID = :dmID";
	if (Q_UNLIKELY(!m_newDbFormat)) {
		queryStr = "UPDATE messages SET _dmAttachDownloaded = "
		    ":_dmAttachDownloaded WHERE dmID = :dmID";
	}
	if (Q_UNLIKELY(!query.prepare(queryStr))) {
		logErrorNL("Cannot prepare SQL query: %s",
		    query.lastError().text().toUtf8().constData());
		return false;
	}
	query.bindValue(":dmID", msgId);
	if (m_newDbFormat) {
		query.bindValue(":_origin",
		    (downloaded) ? "tReturnedMessage" : "tRecord");
	} else {
		query.bindValue(":_dmAttachDownloaded", downloaded);
	}
	if (Q_UNLIKELY(!query.exec())) {
		logErrorNL("Cannot execute SQL query: %s",
		    query.lastError().text().toUtf8().constData());
		return false;
	}
	return true;
}

bool MessageDb::setAttachmentsDownloaded(bool downloaded)
{
	QMutexLocker locker(&m_lock);
	QSqlQuery query(m_db);

	QString queryStr = "UPDATE messages SET _origin = :_origin";
	if (Q_UNLIKELY(!m_newDbFormat)) {
		queryStr = "UPDATE messages SET "
		    "_dmAttachDownloaded = :_dmAttachDownloaded";
	}
	if (Q_UNLIKELY(!query.prepare(queryStr))) {
		logErrorNL("Cannot prepare SQL query: %s",
		    query.lastError().text().toUtf8().constData());
		return false;
	}
	if (m_newDbFormat) {
		query.bindValue(":_origin",
		    (downloaded) ? "tReturnedMessage" : "tRecord");
	} else {
		query.bindValue(":_dmAttachDownloaded", downloaded);
	}
	if (Q_UNLIKELY(!query.exec())) {
		logErrorNL("Cannot execute SQL query: %s",
		    query.lastError().text().toUtf8().constData());
		return false;
	}
	return true;
}

bool MessageDb::updateMessageAuthorInfo2(qint64 dmId,
    const Isds::DmMessageAuthor &msgAuthor)
{
	QMutexLocker locker(&m_lock);
	QSqlQuery query(m_db);

	QString json;
	{
		QJsonDocument document;
		{
			QJsonObject object;
			{
				QJsonValue jsonVal;
				if (Q_UNLIKELY(!Isds::Json::toJsonVal(msgAuthor, jsonVal))) {
					return false;
				}
				object.insert("message_author2", jsonVal);
			}
			document.setObject(object);
		}
		json = document.toJson(QJsonDocument::Compact);
	}

	QString queryStr = "UPDATE supplementary_message_data SET "
	    "custom_data = :custom_data WHERE message_id = :dmId";
	if (Q_UNLIKELY(!query.prepare(queryStr))) {
		logErrorNL("Cannot prepare SQL query: %s.",
		    query.lastError().text().toUtf8().constData());
		return false;
	}
	query.bindValue(":dmId", dmId);
	query.bindValue(":custom_data", nullVariantWhenIsNull(json));
	if (Q_UNLIKELY(!query.exec())) {
		logErrorNL("Cannot execute SQL query: %s.",
		    query.lastError().text().toUtf8().constData());
		return false;
	}
	return true;
}

bool MessageDb::updateMessageEnvelopeInDb(const Isds::Envelope &envelope,
    bool verified)
{
	QMutexLocker locker(&m_lock);
	QSqlQuery query(m_db);

	QString queryStr = "UPDATE messages SET "
	    "dbIDSender = :dbIDSender, dmSender = :dmSender, "
	    "dmSenderAddress = :dmSenderAddress, "
	    "dmSenderType = :dmSenderType, "
	    "dmRecipient = :dmRecipient, "
	    "dmRecipientAddress = :dmRecipientAddress, "
	    "dmAmbiguousRecipient = :dmAmbiguousRecipient, "
	    "dmSenderOrgUnit = :dmSenderOrgUnit, "
	    "dmSenderOrgUnitNum = :dmSenderOrgUnitNum, "
	    "dbIDRecipient = :dbIDRecipient, "
	    "dmRecipientOrgUnit = :dmRecipientOrgUnit, "
	    "dmRecipientOrgUnitNum = :dmRecipientOrgUnitNum, "
	    "dmToHands = :dmToHands, dmAnnotation = :dmAnnotation, "
	    "dmRecipientRefNumber = :dmRecipientRefNumber, "
	    "dmSenderRefNumber = :dmSenderRefNumber, "
	    "dmRecipientIdent = :dmRecipientIdent, "
	    "dmSenderIdent = :dmSenderIdent, "
	    "dmLegalTitleLaw = :dmLegalTitleLaw, "
	    "dmLegalTitleYear = :dmLegalTitleYear, "
	    "dmLegalTitleSect = :dmLegalTitleSect, "
	    "dmLegalTitlePar = :dmLegalTitlePar, "
	    "dmLegalTitlePoint = :dmLegalTitlePoint, "
	    "dmPersonalDelivery = :dmPersonalDelivery, "
	    "dmAllowSubstDelivery = :dmAllowSubstDelivery, "
	    "dmQTimestamp = :dmQTimestamp, "
	    "dmDeliveryTime = :dmDeliveryTime, "
	    "dmAcceptanceTime = :dmAcceptanceTime, "
	    "dmMessageStatus = :dmMessageStatus, "
	    "dmAttachmentSize = :dmAttachmentSize, "
	    "_dmType = :dmType "
	    "WHERE dmID = :dmID";
	if (Q_UNLIKELY(!m_newDbFormat)) {
		queryStr = "UPDATE messages SET "
		    "dbIDSender = :dbIDSender, dmSender = :dmSender, "
		    "dmSenderAddress = :dmSenderAddress, "
		    "dmSenderType = :dmSenderType, "
		    "dmRecipient = :dmRecipient, "
		    "dmRecipientAddress = :dmRecipientAddress, "
		    "dmAmbiguousRecipient = :dmAmbiguousRecipient, "
		    "dmSenderOrgUnit = :dmSenderOrgUnit, "
		    "dmSenderOrgUnitNum = :dmSenderOrgUnitNum, "
		    "dbIDRecipient = :dbIDRecipient, "
		    "dmRecipientOrgUnit = :dmRecipientOrgUnit, "
		    "dmRecipientOrgUnitNum = :dmRecipientOrgUnitNum, "
		    "dmToHands = :dmToHands, dmAnnotation = :dmAnnotation, "
		    "dmRecipientRefNumber = :dmRecipientRefNumber, "
		    "dmSenderRefNumber = :dmSenderRefNumber, "
		    "dmRecipientIdent = :dmRecipientIdent, "
		    "dmSenderIdent = :dmSenderIdent, "
		    "dmLegalTitleLaw = :dmLegalTitleLaw, "
		    "dmLegalTitleYear = :dmLegalTitleYear, "
		    "dmLegalTitleSect = :dmLegalTitleSect, "
		    "dmLegalTitlePar = :dmLegalTitlePar, "
		    "dmLegalTitlePoint = :dmLegalTitlePoint, "
		    "dmPersonalDelivery = :dmPersonalDelivery, "
		    "dmAllowSubstDelivery = :dmAllowSubstDelivery, "
		    "dmQTimestamp = :dmQTimestamp, "
		    "dmDeliveryTime = :dmDeliveryTime, "
		    "dmAcceptanceTime = :dmAcceptanceTime, "
		    "dmMessageStatus = :dmMessageStatus, "
		    "dmAttachmentSize = :dmAttachmentSize, "
		    "dmType = :dmType "
		    "WHERE dmID = :dmID";
	}
	if (Q_UNLIKELY(!query.prepare(queryStr))) {
		logErrorNL("Cannot prepare SQL query: %s",
		    query.lastError().text().toUtf8().constData());
		goto fail;
	}
	query.bindValue(":dmID", envelope.dmId());
	query.bindValue(":dbIDSender", nullVariantWhenIsNull(envelope.dbIDSender()));
	query.bindValue(":dmSender", nullVariantWhenIsNull(envelope.dmSender()));
	query.bindValue(":dmSenderAddress", nullVariantWhenIsNull(envelope.dmSenderAddress()));
	query.bindValue(":dmSenderType", Isds::dbType2IntVariant(envelope.dmSenderType()));
	query.bindValue(":dmRecipient", nullVariantWhenIsNull(envelope.dmRecipient()));
	query.bindValue(":dmRecipientAddress", nullVariantWhenIsNull(envelope.dmRecipientAddress()));
	query.bindValue(":dmAmbiguousRecipient", Isds::nilBool2Variant(envelope.dmAmbiguousRecipient()));
	query.bindValue(":dmSenderOrgUnit", nullVariantWhenIsNull(envelope.dmSenderOrgUnit()));
	query.bindValue(":dmSenderOrgUnitNum", nullVariantWhenIsNull(envelope.dmSenderOrgUnitNumStr()));
	query.bindValue(":dbIDRecipient", nullVariantWhenIsNull(envelope.dbIDRecipient()));
	query.bindValue(":dmRecipientOrgUnit", nullVariantWhenIsNull(envelope.dmRecipientOrgUnit()));
	query.bindValue(":dmRecipientOrgUnitNum", nullVariantWhenIsNull(envelope.dmRecipientOrgUnitNumStr()));
	query.bindValue(":dmToHands", nullVariantWhenIsNull(envelope.dmToHands()));
	query.bindValue(":dmAnnotation", nullVariantWhenIsNull(envelope.dmAnnotation()));
	query.bindValue(":dmRecipientRefNumber", nullVariantWhenIsNull(envelope.dmRecipientRefNumber()));
	query.bindValue(":dmSenderRefNumber", nullVariantWhenIsNull(envelope.dmSenderRefNumber()));
	query.bindValue(":dmRecipientIdent", nullVariantWhenIsNull(envelope.dmRecipientIdent()));
	query.bindValue(":dmSenderIdent", nullVariantWhenIsNull(envelope.dmSenderIdent()));
	query.bindValue(":dmLegalTitleLaw", nullVariantWhenIsNull(envelope.dmLegalTitleLawStr()));
	query.bindValue(":dmLegalTitleYear", nullVariantWhenIsNull(envelope.dmLegalTitleYearStr()));
	query.bindValue(":dmLegalTitleSect", nullVariantWhenIsNull(envelope.dmLegalTitleSect()));
	query.bindValue(":dmLegalTitlePar", nullVariantWhenIsNull(envelope.dmLegalTitlePar()));
	query.bindValue(":dmLegalTitlePoint", nullVariantWhenIsNull(envelope.dmLegalTitlePoint()));
	query.bindValue(":dmPersonalDelivery", Isds::nilBool2Variant(envelope.dmPersonalDelivery()));
	query.bindValue(":dmAllowSubstDelivery", Isds::nilBool2Variant(envelope.dmAllowSubstDelivery()));
	query.bindValue(":dmQTimestamp", nullVariantWhenIsNull(QString()));
	query.bindValue(":dmDeliveryTime", nullVariantWhenIsNull(dateTimeToDbFormatStr(envelope.dmDeliveryTime())));
	query.bindValue(":dmAcceptanceTime", nullVariantWhenIsNull(dateTimeToDbFormatStr(envelope.dmAcceptanceTime())));
	query.bindValue(":dmMessageStatus", Isds::dmState2Variant(envelope.dmMessageStatus()));
	query.bindValue(":dmAttachmentSize", Isds::nonNegativeLong2Variant(envelope.dmAttachmentSize()));
	query.bindValue(":dmType", (!envelope.dmType().isNull()) ? envelope.dmType() : QVariant());

	if (Q_UNLIKELY(!query.exec())) {
		logErrorNL("Cannot execute SQL query: %s",
		    query.lastError().text().toUtf8().constData());
		goto fail;
	}

	if (m_newDbFormat && verified) {

		queryStr = "UPDATE messages SET is_verified = :verified, "
		    "_origin = :_origin WHERE dmID = :dmID";

		if (Q_UNLIKELY(!query.prepare(queryStr))) {
			logErrorNL("Cannot prepare SQL query: %s",
			    query.lastError().text().toUtf8().constData());
			goto fail;
		}

		query.bindValue(":verified", 1);
		query.bindValue(":_origin", "tReturnedMessage");
		query.bindValue(":dmID", envelope.dmId());

		if (Q_UNLIKELY(!query.exec())) {
			logErrorNL("Cannot execute SQL query: %s",
			    query.lastError().text().toUtf8().constData());
			goto fail;
		}
	}

	if (m_newDbFormat) {
		queryStr = "UPDATE supplementary_message_data SET "
		    "attsNum = :attsNum WHERE message_id = :dmID";
		if (Q_UNLIKELY(!query.prepare(queryStr))) {
			logErrorNL("Cannot prepare SQL query: %s.",
			    query.lastError().text().toUtf8().constData());
			goto fail;
		}
		query.bindValue(":attsNum",
		    Isds::nonNegativeLong2Variant(envelope.attsNum()));
		query.bindValue(":dmID", envelope.dmId());
		if (Q_UNLIKELY(!query.exec())) {
			logErrorNL("Cannot execute SQL query: %s",
			    query.lastError().text().toUtf8().constData());
			goto fail;
		}
	}

	return true;
fail:
	return false;
}

/*!
 * @brief Convert custom data to JSON format.
 *
 * @param[in] query SQL query to work with.
 * @param[in] msgId Message id.
 * @param[in] data Custom data.
 * @return True on success.
 */
static
bool convertCustomData(QSqlQuery &query, qint64 msgId, const QString &data)
{
	QString sType;
	QString sName;
	QStringList split = data.split(QLatin1Char(','));
	if (split.count() == 2) {
		sType =  split.at(0);
		sName =  split.at(1);
	} else {
		sType =  split.at(0);
	}
	enum Isds::Type::SenderType senderType =
	    IsdsConversion::descrToSenderType(sType);

	QJsonObject authorObject;
	authorObject.insert("userType", (senderType != Isds::Type::ST_NULL) ?
	    Isds::senderType2Str(senderType) : QJsonValue(QJsonValue::Null));
	authorObject.insert("authorName", sName.isEmpty() ?
	    QJsonValue(QJsonValue::Null) : sName);
	QJsonObject object;
	object.insert("message_author", authorObject);

	QJsonDocument document;
	document.setObject(object);
	QString json = document.toJson(QJsonDocument::Compact);

	QString queryStr = "UPDATE supplementary_message_data "
	    "SET custom_data = :_dmCustomData WHERE message_id = :dmID";

	if (Q_UNLIKELY(!query.prepare(queryStr))) {
		logErrorNL("Cannot prepare SQL query: %s",
		    query.lastError().text().toUtf8().constData());
		return false;
	}
	query.bindValue(":dmID", msgId);
	query.bindValue(":_dmCustomData", nullVariantWhenIsNull(json));

	if (Q_UNLIKELY(!query.exec())) {
		logErrorNL("Cannot execute SQL query: %s",
		    query.lastError().text().toUtf8().constData());
		return false;
	}
	return true;
}

bool MessageDb::converMsgDbIntoNewFormat(void)
{
	logInfoNL("Convert database to new format: %s",
	    m_db.databaseName().toUtf8().constData());

	QMutexLocker locker(&m_lock);
	QSqlQuery query(m_db);
	QSqlQuery query2(m_db);
	QString queryStr;
	bool transaction = false;

	/* Rename message table.*/
	queryStr = "ALTER TABLE messages RENAME TO _temp_messages_old";
	if (Q_UNLIKELY(!query.prepare(queryStr))) {
		logErrorNL("Cannot prepare SQL query: %s",
		    query.lastError().text().toUtf8().constData());
		goto fail;
	}
	if (Q_UNLIKELY(!query.exec())) {
		logErrorNL("Cannot execute SQL query: %s",
		    query.lastError().text().toUtf8().constData());
		goto fail;
	}

	/* Create new messages and supplementary_message_data tables. */
	if (!msgsTbl.createEmpty(m_db)) {
		goto fail;
	}
	if (!smsgdtTbl.createEmpty(m_db)) {
		goto fail;
	}

	/* Transform old massage table into new message table. */
	queryStr = "INSERT INTO messages ("
	    "dmID, _origin, dbIDSender, dmSender, dmSenderAddress, "
	    "dmSenderType, dmRecipient, dmRecipientAddress, "
	    "dmAmbiguousRecipient, dmSenderOrgUnit, dmSenderOrgUnitNum, "
	    "dbIDRecipient, dmRecipientOrgUnit, dmRecipientOrgUnitNum, "
	    "dmToHands, dmAnnotation, dmRecipientRefNumber, dmSenderRefNumber, "
	    "dmRecipientIdent, dmSenderIdent, dmLegalTitleLaw, "
	    "dmLegalTitleYear, dmLegalTitleSect, dmLegalTitlePar, "
	    "dmLegalTitlePoint, dmPersonalDelivery, dmAllowSubstDelivery, "
	    "dmQTimestamp, dmDeliveryTime, dmAcceptanceTime, dmMessageStatus, "
	    "dmAttachmentSize, _dmType"
	    ") SELECT "
	    "dmID, "
	    "(CASE _dmAttachDownloaded WHEN 1 THEN 'tReturnedMessage' ELSE 'tRecord' END),"
	    "dbIDSender, dmSender, dmSenderAddress, "
	    "dmSenderType, dmRecipient, dmRecipientAddress, dmAmbiguousRecipient, "
	    "dmSenderOrgUnit, dmSenderOrgUnitNum, dbIDRecipient, "
	    "dmRecipientOrgUnit, dmRecipientOrgUnitNum, dmToHands, dmAnnotation, "
	    "dmRecipientRefNumber, dmSenderRefNumber, dmRecipientIdent, "
	    "dmSenderIdent, dmLegalTitleLaw, dmLegalTitleYear, "
	    "dmLegalTitleSect, dmLegalTitlePar, dmLegalTitlePoint, "
	    "dmPersonalDelivery, dmAllowSubstDelivery, dmQTimestamp, "
	    "dmDeliveryTime, dmAcceptanceTime, dmMessageStatus, "
	    "dmAttachmentSize, dmType "
	    "FROM _temp_messages_old";
	if (Q_UNLIKELY(!query.prepare(queryStr))) {
		logErrorNL("Cannot prepare SQL query: %s",
		    query.lastError().text().toUtf8().constData());
		goto fail;
	}
	if (Q_UNLIKELY(!query.exec())) {
		logErrorNL("Cannot execute SQL query: %s",
		    query.lastError().text().toUtf8().constData());
		goto fail;
	}

	/* Transform old massages table into supplementary_message_data table. */
	queryStr = "INSERT INTO supplementary_message_data "
	    "(message_id, message_type, read_locally, download_date, "
	    "custom_data) SELECT dmID, _dmMessageType, "
	   "_dmReadLocally, _dmDownloadDate, _dmCustomData "
	    "FROM _temp_messages_old";
	if (Q_UNLIKELY(!query.prepare(queryStr))) {
		logErrorNL("Cannot prepare SQL query: %s",
		    query.lastError().text().toUtf8().constData());
		goto fail;
	}
	if (Q_UNLIKELY(!query.exec())) {
		logErrorNL("Cannot execute SQL query: %s",
		    query.lastError().text().toUtf8().constData());
		goto fail;
	}

	/* Drop old massages table. */
	queryStr = "DROP TABLE _temp_messages_old";
	if (Q_UNLIKELY(!query.prepare(queryStr))) {
		logErrorNL("Cannot prepare SQL query: %s",
		    query.lastError().text().toUtf8().constData());
		goto fail;
	}
	if (Q_UNLIKELY(!query.exec())) {
		logErrorNL("Cannot execute SQL query: %s",
		    query.lastError().text().toUtf8().constData());
		goto fail;
	}

	/* Convert custom data to JSON format like desktop. */
	transaction = beginTransaction();
	if (Q_UNLIKELY(!transaction)) {
		logErrorNL("%s", "Cannot begin transaction.");
		goto fail;
	}
	queryStr = "SELECT message_id, custom_data "
	    "FROM supplementary_message_data";
	if (Q_UNLIKELY(!query.prepare(queryStr))) {
		logErrorNL("Cannot prepare SQL query: %s",
		    query.lastError().text().toUtf8().constData());
		goto fail;
	}
	if (query.exec() && query.isActive()) {
		query.first();
		while (query.isValid()) {
			if (query.value(1).toString().isEmpty()) {
				query.next();
				continue;
			}
			if (Q_UNLIKELY(!convertCustomData(query2,
			        query.value(0).toLongLong(),
			        query.value(1).toString()))) {
				logWarningNL("%s", "Error to convert custom data.");
			}
			query.next();
		}
	} else {
		logErrorNL("Cannot execute SQL query: %s",
		    query.lastError().text().toUtf8().constData());
		goto fail;
	}
	if (transaction) {
		commitTransaction();
	}

	/* Rename events table.*/
	queryStr = "ALTER TABLE events RENAME TO _temp_events_old";
	if (Q_UNLIKELY(!query.prepare(queryStr))) {
		logErrorNL("Cannot prepare SQL query: %s",
		    query.lastError().text().toUtf8().constData());
		goto fail;
	}
	if (Q_UNLIKELY(!query.exec())) {
		logErrorNL("Cannot execute SQL query: %s",
		    query.lastError().text().toUtf8().constData());
		goto fail;
	}

	/* Create new event table. */
	if (!evntsTbl.createEmpty(m_db)) {
		goto fail;
	}

	/* Transform old events table into new events table. */
	queryStr = "INSERT INTO events "
	    "(id, message_id, dmEventTime, dmEventDescr) "
	    "SELECT id, dmID, dmEventTime, dmEventDescr "
	    "FROM _temp_events_old";
	if (Q_UNLIKELY(!query.prepare(queryStr))) {
		logErrorNL("Cannot prepare SQL query: %s",
		    query.lastError().text().toUtf8().constData());
		goto fail;
	}
	if (Q_UNLIKELY(!query.exec())) {
		logErrorNL("Cannot execute SQL query: %s",
		    query.lastError().text().toUtf8().constData());
		goto fail;
	}

	/* Drop old events table. */
	queryStr = "DROP TABLE _temp_events_old";
	if (Q_UNLIKELY(!query.prepare(queryStr))) {
		logErrorNL("Cannot prepare SQL query: %s",
		    query.lastError().text().toUtf8().constData());
		goto fail;
	}
	if (Q_UNLIKELY(!query.exec())) {
		logErrorNL("Cannot execute SQL query: %s",
		    query.lastError().text().toUtf8().constData());
		goto fail;
	}

	logInfoNL("%s", "Convert: Done");

	return true;
fail:
	if (transaction) {
		rollbackTransaction();
	}
	return false;
}

void MessageDb::addNewTables(void)
{
	/* Create another tables. */
	flsTbl.createEmpty(m_db);
	hshsTbl.createEmpty(m_db);
	prcstTbl.createEmpty(m_db);
	rwmsgdtTbl.createEmpty(m_db);
	rwdlvrinfdtTbl.createEmpty(m_db);
	crtdtTbl.createEmpty(m_db);
	msgcrtdtTbl.createEmpty(m_db);
}

QString MessageDb::constructDbFileNameOld(const QString &userName)
{
	return userName + "_msg.db";
}

QString MessageDb::constructDbFileNameNew(const QString &userName,
   bool testing)
{
	return QString(userName + "___%1.db").arg(testing ? "1" : "0");
}

QString MessageDb::constructDbBackupFileName(const QString &userName,
   bool testing, const QDate &backupDate)
{
	return QString("%1___%2_bak%3.db").arg(userName)
	    .arg(testing ? "1" : "0").arg(backupDate.toString("yyyyMM"));
}

bool MessageDb::checkMsgDbIntegrity(void)
{
	QMutexLocker locker(&m_lock);
	QSqlQuery query(m_db);

	/* Integrity check. */
	QString queryStr = "PRAGMA integrity_check";
	if (Q_UNLIKELY(!query.prepare(queryStr))) {
		logErrorNL("Cannot prepare SQL query: %s",
		    query.lastError().text().toUtf8().constData());
		return false;
	}
	if (query.exec() && query.isActive()) {
		query.first();
		if (Q_UNLIKELY(!query.isValid())) {
			return false;
		}
		if (query.record().count() > 0) {
			if (query.record().value(0).toString() != "ok") {
				logErrorNL("Integrity check failed. Test returns: %s.",
				    query.record().value(0).toString().toUtf8().constData());
				return false;
			}
		}
	} else {
		logErrorNL("Cannot execute SQL query: %s.",
		    query.lastError().text().toUtf8().constData());
		return false;
	}

	return true;
}

bool MessageDb::runCompleteTestMsgDbIntegrity(void)
{
	QMutexLocker locker(&m_lock);
	QSqlQuery query(m_db);
	QString queryStr;

	/* Integrity check. */
	if (Q_UNLIKELY(!checkMsgDbIntegrity())) {
		goto fail;
	}

	/* Vacuum database test. */
	queryStr = "VACUUM";
	if (Q_UNLIKELY(!query.prepare(queryStr))) {
		logErrorNL("Cannot prepare SQL query: %s",
		    query.lastError().text().toUtf8().constData());
		goto fail;
	}
	if (Q_UNLIKELY(!query.exec())) {
		logErrorNL("Cannot execute SQL query: %s",
		    query.lastError().text().toUtf8().constData());
		goto fail;
	}

	/* Create temporary table test. */
	queryStr = "CREATE TABLE _tmp_table_ (id INTEGER PRIMARY KEY)";
	if (Q_UNLIKELY(!query.prepare(queryStr))) {
		logErrorNL("Cannot prepare SQL query: %s",
		    query.lastError().text().toUtf8().constData());
		goto fail;
	}
	if (Q_UNLIKELY(!query.exec())) {
		logErrorNL("Cannot execute SQL query: %s",
		    query.lastError().text().toUtf8().constData());
		goto fail;
	}

	/* Drop temporary table. */
	queryStr = "DROP TABLE _tmp_table_";
	if (Q_UNLIKELY(!query.prepare(queryStr))) {
		logErrorNL("Cannot prepare SQL query: %s",
		    query.lastError().text().toUtf8().constData());
		goto fail;
	}
	if (Q_UNLIKELY(!query.exec())) {
		logErrorNL("Cannot execute SQL query: %s",
		    query.lastError().text().toUtf8().constData());
		goto fail;
	}

	/* Clean-up database. */
	queryStr = "VACUUM";
	if (Q_UNLIKELY(!query.prepare(queryStr))) {
		logErrorNL("Cannot prepare SQL query: %s",
		    query.lastError().text().toUtf8().constData());
		goto fail;
	}
	if (Q_UNLIKELY(!query.exec())) {
		logErrorNL("Cannot execute SQL query: %s",
		    query.lastError().text().toUtf8().constData());
		goto fail;
	}

	return true;

fail:
	logErrorNL("Integrity test failed. Database file '%s' is probably damaged.",
	    m_db.databaseName().toUtf8().constData());
	return false;
}

bool MessageDb::isVodz(qint64 dmId, bool *ok)
{
	QMutexLocker locker(&m_lock);

	QSqlQuery query(m_db);

	QString queryStr = "SELECT dmVODZ "
	    "FROM supplementary_message_data WHERE message_id = :dmId";
	if (Q_UNLIKELY(!query.prepare(queryStr))) {
		logErrorNL("Cannot prepare SQL query: %s",
		    query.lastError().text().toUtf8().constData());
		goto fail;
	}
	query.bindValue(":dmId", dmId);
	if (query.exec() && query.isActive()) {
		query.first();
		if (query.isValid()) {
			return query.value(0).toBool();
		}
	} else {
		logErrorNL("Cannot execute SQL query: %s",
		    query.lastError().text().toUtf8().constData());
	}

fail:
	if (ok != Q_NULLPTR) {
		*ok = false;
	}
	return false;
}

QByteArray MessageDb::messageAuthorJsonStr(qint64 dmId) const
{
	QMutexLocker locker(&m_lock);

	QSqlQuery query(m_db);

	QString queryStr = "SELECT custom_data "
	    "FROM supplementary_message_data WHERE message_id = :dmID";
	if (Q_UNLIKELY(!m_newDbFormat)) {
		queryStr = "SELECT _dmCustomData "
		    "FROM messages WHERE dmID = :dmID";
	}
	if (Q_UNLIKELY(!query.prepare(queryStr))) {
		goto fail;
	}
	query.bindValue(":dmID", dmId);
	if (query.exec() && query.isActive()) {
		query.first();
		if (query.isValid()) {
			return query.value(0).toByteArray();
		}
	} else {
		logErrorNL("Cannot execute SQL query: %s",
		    query.lastError().text().toUtf8().constData());
	}

fail:
	return QByteArray();
}

QList<class SQLiteTbl *> MessageDb::listOfTables(void) const
{
	QList<class SQLiteTbl *> tables;
	tables.append(&msgsTbl);
	tables.append(&flsTbl);
	tables.append(&hshsTbl);
	tables.append(&evntsTbl);
	tables.append(&prcstTbl);
	tables.append(&rwmsgdtTbl);
	tables.append(&rwdlvrinfdtTbl);
	tables.append(&smsgdtTbl);
	tables.append(&crtdtTbl);
	tables.append(&msgcrtdtTbl);

	return tables;
}

/*!
 * @brief This method ensures that the supplementary_message_data table
 *     contains the dmVODZ and attsNum column.
 *
 * @param[in] mDb Message database.
 * @param[in] db Database reference.
 * @return True on success.
 *
 * TODO -- This method may be removed in some future version
 *     of the programme.
 */
static
bool ensureDmVODZAndAttsNumColumnInSupMsgData(MessageDb &mDb,
    const QSqlDatabase &db)
{
	QSqlQuery query(db);
	QString queryStr;
	QString createTableSql;
	bool transaction = false;

	queryStr = "SELECT sql FROM sqlite_master "
	    "WHERE (type = :type) and (name = :name)";
	if (Q_UNLIKELY(!query.prepare(queryStr))) {
		logErrorNL("Cannot prepare SQL query: %s.",
		    query.lastError().text().toUtf8().constData());
		goto fail;
	}
	query.bindValue(":type", QString("table"));
	query.bindValue(":name", QString("supplementary_message_data"));
	if (query.exec() && query.isActive() &&
	    query.first() && query.isValid()) {
		createTableSql = query.value(0).toString();
	} else {
		logErrorNL(
		    "Cannot execute SQL query and/or read SQL data: %s.",
		    query.lastError().text().toUtf8().constData());
		goto fail;
	}

	if (Q_UNLIKELY(createTableSql.isEmpty())) {
		goto fail;
	}

	if (createTableSql.contains("dmVODZ", Qt::CaseSensitive)) {
		return true;
	}

	/*
	 * Table does not contain 'dmVODZ' or 'attNum' column as they are
	 * added in one pass.
	 */

	transaction = mDb.beginTransaction();
	if (Q_UNLIKELY(!transaction)) {
		logErrorNL("%s", "Cannot begin transaction.");
		goto fail;
	}

	/* Rename existing table. */
	queryStr = "ALTER TABLE supplementary_message_data "
	    "RENAME TO _supplementary_message_data";
	if (Q_UNLIKELY(!query.prepare(queryStr))) {
		logErrorNL("Cannot prepare SQL query: %s.",
		    query.lastError().text().toUtf8().constData());
		goto fail;
	}
	if (Q_UNLIKELY(!query.exec())) {
		logErrorNL(
		    "Cannot execute SQL query and/or read SQL data: %s.",
		    query.lastError().text().toUtf8().constData());
		goto fail;
	}

	if (Q_UNLIKELY(!smsgdtTbl.createEmpty(db))) {
		goto fail;
	}

	/* Copy table content. */
	queryStr = "INSERT OR REPLACE INTO supplementary_message_data (message_id, message_type, read_locally, download_date, custom_data) "
	    "SELECT message_id, message_type, read_locally, download_date, custom_data FROM _supplementary_message_data";
	if (Q_UNLIKELY(!query.prepare(queryStr))) {
		logErrorNL("Cannot prepare SQL query: %s.",
		    query.lastError().text().toUtf8().constData());
		goto fail;
	}
	if (Q_UNLIKELY(!query.exec())) {
		logErrorNL(
		    "Cannot execute SQL query and/or read SQL data: %s.",
		    query.lastError().text().toUtf8().constData());
		goto fail;
	}

	/* Delete old table. */
	queryStr = "DROP TABLE _supplementary_message_data";
	if (Q_UNLIKELY(!query.prepare(queryStr))) {
		logErrorNL("Cannot prepare SQL query: %s.",
		    query.lastError().text().toUtf8().constData());
		goto fail;
	}
	if (Q_UNLIKELY(!query.exec())) {
		logErrorNL(
		    "Cannot execute SQL query and/or read SQL data: %s.",
		    query.lastError().text().toUtf8().constData());
		goto fail;
	}

	if (transaction) {
		mDb.commitTransaction();
	}

	return true;
fail:
	if (transaction) {
		mDb.rollbackTransaction();
	}
	return false;
}

bool MessageDb::assureConsistency(void)
{
	QMutexLocker locker(&m_lock);

	logInfoNL("Assuring dmVODZ, attsNum columns in supplementary message table in database '%s'.",
	    fileName().toUtf8().constData());
	bool ret = ensureDmVODZAndAttsNumColumnInSupMsgData(*this, m_db);
	if (Q_UNLIKELY(!ret)) {
		logErrorNL(
		    "Couldn't assure dmVODZ, attsNum columns in supplementary message table in database '%s'.",
		    fileName().toUtf8().constData());
	}
	return ret;
}
