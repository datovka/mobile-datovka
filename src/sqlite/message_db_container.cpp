/*
 * Copyright (C) 2014-2025 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

#include <QDir>
#include <QFile>

#include "src/sqlite/message_db_container.h"
#include "src/sqlite/message_db.h"

MsgDbContainer::MsgDbContainer(const QString &connectionPrefix)
    : QMap<AcntId, MessageDb *>(),
    m_connectionPrefix(connectionPrefix)
{
}

MsgDbContainer::~MsgDbContainer(void)
{
	QMap<AcntId, MessageDb *>::iterator i;

	for (i = this->begin(); i != this->end(); ++i) {
		delete i.value();
	}
}

MessageDb *MsgDbContainer::accessOpenedMessageDb(const AcntId &acntId) const
{
	if (Q_UNLIKELY(!acntId.isValid())) {
		Q_ASSERT(0);
		return Q_NULLPTR;
	}

	/* Already opened. */
	if (this->find(acntId) != this->end()) {
		return (*this)[acntId];
	}

	return Q_NULLPTR;
}

MessageDb *MsgDbContainer::accessMessageDb(const QString &locDir,
    const AcntId &acntId, bool storeLocally)
{
	if (Q_UNLIKELY(!acntId.isValid())) {
		Q_ASSERT(0);
		return Q_NULLPTR;
	}

	MessageDb *db = accessOpenedMessageDb(acntId);
	if (Q_NULLPTR != db) {
		return db;
	}

	QString dbFileName;
	/* Test whether any message database exists. */
	const enum DatabaseType msgType = msgDbExists(locDir, acntId);
	switch (msgType) {
	case BOTH_MSG_DB:
	case NEW_MSG_DB:
		/* Open and use the new message database. */
		dbFileName = MessageDb::constructDbFileNameNew(acntId.username(),
		    acntId.testing());
		break;
	case OLD_MSG_DB:
		/* Open and use the old message database. */
		dbFileName = MessageDb::constructDbFileNameOld(acntId.username());
		break;
	case NO_MSG_DB:
		/* Create empty new message database with new format. */
		dbFileName = MessageDb::constructDbFileNameNew(acntId.username(),
		    acntId.testing());
		break;
	default:
		break;
	}

	{
		QString connectionName = acntId.username() + "_MSGDB";
		if (!m_connectionPrefix.isEmpty()) {
			connectionName = m_connectionPrefix + "_" + connectionName;
		}

		db = new (::std::nothrow) MessageDb(connectionName);
		if (Q_UNLIKELY(Q_NULLPTR == db)) {
			Q_ASSERT(0);
			return Q_NULLPTR;
		}
	}

	const QString location = locDir + QDir::separator() +
	    QDir::toNativeSeparators(dbFileName);

	bool openRet = false;
	if (OLD_MSG_DB == msgType) {
		/* Open old database without new tables creation. */
		openRet = db->openDbNoNewTables(location, storeLocally);
	} else {
		openRet = db->openDb(location, storeLocally);
	}
	if (Q_UNLIKELY(!openRet)) {
		delete db;
		return Q_NULLPTR;
	}

	this->insert(acntId, db);

	return db;
}

bool MsgDbContainer::deleteDb(MessageDb *db)
{
	if (Q_UNLIKELY(Q_NULLPTR == db)) {
		Q_ASSERT(0);
		return false;
	}

	/* Find entry. */
	QMap<AcntId, MessageDb *>::iterator it = this->begin();
	while ((it != this->end()) && (it.value() != db)) {
		++it;
	}
	/* Must exist. */
	if (Q_UNLIKELY(this->end() == it)) {
		Q_ASSERT(0);
		return false;
	}

	/* Remove from container. */
	this->erase(it);

	/* Get file name. */
	const QString fileName = db->fileName();

	/* Close database. */
	delete db;

	if (fileName == SQLiteDb::memoryLocation) {
		return true;
	}

	/* Delete file. */
	if (Q_UNLIKELY(!QFile::remove(fileName))) {
		return false;
	}

	return true;
}

bool MsgDbContainer::closeAndRemoveDb(MessageDb *db)
{
	if (Q_UNLIKELY(Q_NULLPTR == db)) {
		Q_ASSERT(0);
		return false;
	}

	/* Find entry. */
	QMap<AcntId, MessageDb *>::iterator it = this->begin();
	while ((it != this->end()) && (it.value() != db)) {
		++it;
	}
	/* Must exist. */
	if (Q_UNLIKELY(this->end() == it)) {
		Q_ASSERT(0);
		return false;
	}

	/* Remove from container. */
	this->erase(it);

	/* Close database. */
	delete db;

	return true;
}

MessageDb *MsgDbContainer::accessMessageDbNew(const QString &locDir,
    const AcntId &acntId, bool storeLocally, bool createTables)
{
	if (Q_UNLIKELY(!acntId.isValid())) {
		Q_ASSERT(0);
		return Q_NULLPTR;
	}

	QString dbFileName(MessageDb::constructDbFileNameNew(acntId.username(),
	    acntId.testing()));

	MessageDb *db = new (::std::nothrow) MessageDb(dbFileName);
	if (Q_UNLIKELY(Q_NULLPTR == db)) {
		Q_ASSERT(0);
		return Q_NULLPTR;
	}

	/* Open new database without new tables creation. */
	QString location = locDir + QDir::separator() + dbFileName;
	bool openRet;
	if (createTables) {
		openRet = db->openDb(location, storeLocally);
	} else {
		openRet = db->openDbNoNewTables(location, storeLocally);
	}
	if (Q_UNLIKELY(!openRet)) {
		delete db;
		return Q_NULLPTR;
	}

	return db;
}

enum MsgDbContainer::DatabaseType MsgDbContainer::msgDbExists(
    const QString &locDir, const AcntId &acntId)
{
	enum DatabaseType msgType = NO_MSG_DB;

	if (Q_UNLIKELY(locDir.isEmpty())) {
		Q_ASSERT(0);
		return msgType;
	}

	if (Q_UNLIKELY(!acntId.isValid())) {
		Q_ASSERT(0);
		return msgType;
	}

	/* Test for new database file. Must be first! */
	QString newDb = locDir + QDir::separator() +
	    MessageDb::constructDbFileNameNew(acntId.username(),
	        acntId.testing());
	if (QFile::exists(newDb)) {
		return NEW_MSG_DB;
	}

	/* Then test for old database file. */
	QString oldDb = locDir + QDir::separator() +
	    MessageDb::constructDbFileNameOld(acntId.username());
	if (QFile::exists(oldDb)) {
		return OLD_MSG_DB;
	}

	return msgType;
}
