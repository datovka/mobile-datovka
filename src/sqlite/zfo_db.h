/*
 * Copyright (C) 2014-2025 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

#pragma once

#include <QList>
#include <QString>

#include "src/datovka_shared/io/sqlite/db_single.h"

/* Zfo db filename */
#define ZFO_DB_NAME "zfo.db"

/*!
 * @brief Encapsulates zfo database.
 */
class ZfoDb : public SQLiteDbSingle {

public:
	/* Use parent class constructor. */
	using SQLiteDbSingle::SQLiteDbSingle;

	/* Make some inherited methods public. */
	using SQLiteDbSingle::closeDb;

	/*!
	 * @brief Get Zfo content.
	 *
	 * @param[in] msgId String with message id.
	 * @param[in] isTestAccount True if account is in the ISDS testing environment.
	 * @return Zfo content in base64.
	 */
	QByteArray getZfoContentFromDb(qint64 msgId, bool isTestAccount);

	/*!
	 * @brief Get Zfo size in bytes.
	 *
	 * @param[in] msgId String with message id.
	 * @param[in] isTestAccount True if account is in the ISDS testing environment.
	 * @return Size of zfo file in bytes.
	 */
	int getZfoSizeFromDb(qint64 msgId, bool isTestAccount);

	/*!
	 * @brief Open database file.
	 *
	 * @param[in] fileName File name.
	 * @param[in] storeToDisk Whether to create db in memory.
	 * @return True on success.
	 */
	bool openDb(const QString &fileName, bool storeToDisk);

protected:
	/*!
	 * @brief Returns list of tables.
	 *
	 * @return List of pointers to tables.
	 */
	virtual
	QList<class SQLiteTbl *> listOfTables(void) const Q_DECL_OVERRIDE;
};
