/*
 * Copyright (C) 2014-2025 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

#pragma once

#include <QMap>
#include <QString>

#include "src/datovka_shared/identifiers/account_id.h"

class MessageDb; /* Forward declaration. */

/*!
 * @brief Database container.
 */
class MsgDbContainer : private QMap<AcntId, MessageDb *> {
public:
	/*!
	 * @brief Used to distinguish between message database formats.
	 */
	enum DatabaseType {
		NO_MSG_DB = 0, /*!< No message db present. */
		OLD_MSG_DB, /*!< Old message db. */
		NEW_MSG_DB, /*!< New message db. */
		BOTH_MSG_DB /*!< Old and new message db. */
	};

	/*!
	 * @brief Constructor.
	 *
	 * @param[in] connectionPrefix Database connection prefix.
	 */
	explicit MsgDbContainer(const QString &connectionPrefix);

	/*!
	 * @brief Destructor.
	 */
	~MsgDbContainer(void);

	/*!
	 * @brief Access message database that has already been opened for a given account.
	 *
	 * @param[in] acntId Account identifier.
	 * @return Pointer to database, Q_NULLPTR if database does not exist.
	 */
	MessageDb *accessOpenedMessageDb(const AcntId &acntId) const;

	/*!
	 * @brief Access/create+open message database related to item.
	 *
	 * @param[in] locDir Directory where to search for the file.
	 * @param[in] acntId Account identifier.
	 * @param[in] storeLocally True if it should be stored in local storage.
	 * @return Pointer to database, null pointer on error.
	 */
	MessageDb *accessMessageDb(const QString &locDir,
	    const AcntId &acntId, bool storeLocally);

	/*!
	 * @brief Delete all files related to database.
	 *
	 * @param db Deleted database.
	 * @return True on success.
	 */
	bool deleteDb(MessageDb *db);

	/*!
	 * @brief Close database and remove it for container.
	 *
	 * @param db Database.
	 * @return True on success.
	 */
	bool closeAndRemoveDb(MessageDb *db);

	/*!
	 * @brief Access/create+open new message database related to item.
	 *
	 * @param[in] locDir Directory where to search for the file.
	 * @param[in] acntId Account identifier.
	 * @param[in] storeLocally True if it should be stored in local storage.
	 * @param[in] createTables True if create tables.
	 * @return Pointer to database, null pointer on error.
	 */
	static
	MessageDb *accessMessageDbNew(const QString &locDir,
	    const AcntId &acntId, bool storeLocally, bool createTables);

	/*!
	 * @brief Check whether any message database file exists in the app sandbox.
	 *
	 * @param[in] locDir Directory where to search for the file.
	 * @param[in] acntId Account identifier.
	 * @return Message database type if any database exists.
	 */
	static
	enum DatabaseType msgDbExists(const QString &locDir,
	    const AcntId &acntId);

private:
	const QString m_connectionPrefix; /*!< Database connection prefix. */
};
