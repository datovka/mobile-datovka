/*
 * Copyright (C) 2014-2025 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

#include <QFile>
#include <QFileInfo>
#include <QSettings>

#include "src/datovka_shared/html/html_export.h"
#include "src/datovka_shared/isds/box_interface.h"
#include "src/datovka_shared/isds/type_conversion.h" /* Isds::dbType2Str */
#include "src/datovka_shared/log/log.h"
#include "src/global.h"
#include "src/io/filesystem.h"
#include "src/models/accountmodel.h"
#include "src/qml_identifiers/qml_account_id.h"
#include "src/settings/account.h"
#include "src/settings/account_logos.h"
#include "src/settings/accounts.h"
#include "src/settings/prefs_specific.h"
#include "src/setwrapper.h"
#include "src/sqlite/account_db.h"
#include "src/sqlite/file_db_container.h"
#include "src/sqlite/message_db_container.h"
#include "src/wrap_accounts.h"

Accounts::Accounts(QObject *parent)
    : QObject(parent)
{
}

QString Accounts::dbType(const QmlAcntId *qAcntId)
{
	if ((GlobInstcs::accountDbPtr != Q_NULLPTR) && (qAcntId != Q_NULLPTR)) {
		return Isds::dbType2Str(
		    GlobInstcs::accountDbPtr->dbType(qAcntId->username()));
	} else {
		Q_ASSERT(0);
		return QString();
	}
}

QString Accounts::dbId(const QmlAcntId *qAcntId)
{
	if ((GlobInstcs::accountDbPtr != Q_NULLPTR) && (qAcntId != Q_NULLPTR)) {
		return GlobInstcs::accountDbPtr->dbId(qAcntId->username());
	} else {
		Q_ASSERT(0);
		return QString();
	}
}

bool Accounts::isOvm(const QmlAcntId *qAcntId)
{
	if ((GlobInstcs::accountDbPtr != Q_NULLPTR) && (qAcntId != Q_NULLPTR)) {
		enum Isds::Type::DbType dbType =
		    GlobInstcs::accountDbPtr->dbType(qAcntId->username());
		return (Isds::Type::BT_NULL < dbType) && (dbType < Isds::Type::BT_PO);
	} else {
		Q_ASSERT(0);
		return false;
	}
}

/*!
 * @brief Load message counters of the account.
 *
 * @param[in,out] accountModel Model whose counters should be updated.
 * @param[in] acntId Account identifier.
 */
static
void loadOneAccountCounters(AccountListModel &accountModel,
    const AcntId &acntId)
{
	if (Q_UNLIKELY((GlobInstcs::prefsPtr == Q_NULLPTR) ||
	        (GlobInstcs::messageDbsPtr == Q_NULLPTR) ||
	        (GlobInstcs::acntMapPtr == Q_NULLPTR))) {
		Q_ASSERT(0);
		return;
	}

	if (Q_UNLIKELY(!acntId.isValid())) {
		return;
	}

	MessageDb *msgDb = GlobInstcs::messageDbsPtr->accessMessageDb(
	    GlobalSettingsQmlWrapper::dbPath(), acntId,
	    PrefsSpecific::dataOnDisk(
	        *GlobInstcs::prefsPtr, GlobInstcs::acntMapPtr->acntData(acntId)));
	if (Q_UNLIKELY(msgDb == Q_NULLPTR)) {
		logWarningNL("Cannot open message database for '%s'.",
		    acntId.username().toUtf8().constData());
		return;
	}

	accountModel.updateCounters(acntId,
	    msgDb->getNewMessageCountFromDb(MessageDb::TYPE_RECEIVED),
	    msgDb->getMessageCountFromDb(MessageDb::TYPE_RECEIVED),
	    msgDb->getNewMessageCountFromDb(MessageDb::TYPE_SENT),
	    msgDb->getMessageCountFromDb(MessageDb::TYPE_SENT));
}

void Accounts::updateOneAccountCounters(AccountListModel *accountModel,
    const QmlAcntId *qAcntId)
{
	if (Q_UNLIKELY((GlobInstcs::logPtr == Q_NULLPTR) ||
	        (qAcntId == Q_NULLPTR))) {
		return;
	}

	debugFuncCall();

	if (Q_UNLIKELY(accountModel == Q_NULLPTR)) {
		/* accountModel can be NULL when app is shutdown */
		logWarningNL("%s", "Cannot access account model.");
		return;
	}

	loadOneAccountCounters(*accountModel, *qAcntId);
}

QString Accounts::fillAccountInfo(const QmlAcntId *qAcntId)
{
	debugFuncCall();

	if ((GlobInstcs::accountDbPtr != Q_NULLPTR) && (qAcntId != Q_NULLPTR)) {
		return Html::Export::htmlDataboxOwnerInfoApp(
		    GlobInstcs::accountDbPtr->getOwnerInfo(qAcntId->username()))
		    + Html::Export::htmlDataboxUserInfoApp(
		    GlobInstcs::accountDbPtr->getUserInfo(qAcntId->username()))
		    + Html::Export::htmlPwdExpirDateInfoApp(
		    GlobInstcs::accountDbPtr->getPwdExpirDateTime(
		    qAcntId->username()));
	} else {
		Q_ASSERT(0);
		return QString();
	}
}

void Accounts::getAccountData(const QmlAcntId *qAcntId)
{
	debugFuncCall();

	if (Q_UNLIKELY((GlobInstcs::acntMapPtr == Q_NULLPTR) ||
	        (qAcntId == Q_NULLPTR))) {
		Q_ASSERT(0);
		return;
	}

	if (!GlobInstcs::acntMapPtr->acntIds().contains(*qAcntId)) {
		return;
	}

	const AcntData aData = GlobInstcs::acntMapPtr->acntData(*qAcntId);

	emit sendAccountData(aData.accountName(), aData.userName(),
	    aData.loginMethod(), aData.password(), aData.mepToken(),
	    aData.isTestAccount(), aData.rememberPwd(),
	    PrefsSpecific::dataOnDisk(*GlobInstcs::prefsPtr, aData),
	    aData.syncWithAll(), aData.p12File());
}

int Accounts::actDTType(const QmlAcntId *qAcntId)
{
	if (Q_UNLIKELY((qAcntId == Q_NULLPTR) ||
		GlobInstcs::accountDbPtr == Q_NULLPTR)) {
		Q_ASSERT(0);
		return -1;
	}

	QString dbID = dbId(qAcntId);

	Isds::DTInfoOutput dtInfo(GlobInstcs::accountDbPtr->getDTInfo(dbID));

	if (dtInfo.isNull()) {
		Q_ASSERT(0);
		return -1;
	}

	return dtInfo.actDTType();
}

bool Accounts::isDTCapacityFull(const QmlAcntId *qAcntId)
{
	if (Q_UNLIKELY((qAcntId == Q_NULLPTR) ||
		GlobInstcs::accountDbPtr == Q_NULLPTR)) {
		Q_ASSERT(0);
		return false;
	}

	QString dbID = dbId(qAcntId);

	Isds::DTInfoOutput dtInfo(GlobInstcs::accountDbPtr->getDTInfo(dbID));

	if (dtInfo.isNull()) {
		Q_ASSERT(0);
		return false;
	}

	return (dtInfo.actDTCapacity()-dtInfo.actDTCapUsed() == 0);
}

QString Accounts::createAccount(
    enum AcntData::LoginMethod loginMethod, const QString &acntName,
    const QString &userName, const QString &pwd, const QString &mepToken,
    bool isTestAccount, bool rememberPwd, bool storeToDisk, bool syncWithAll,
    const QString &certPath)
{
	debugFuncCall();

	AcntData acntData;
	QString errTxt;
	QString newUserName = userName.trimmed();

	if (Q_UNLIKELY(GlobInstcs::acntMapPtr == Q_NULLPTR)) {
		logErrorNL("%s", "Cannot access account data.");
		Q_ASSERT(0);
		return tr("Cannot access data-box data.");
	}

	if (AcntData::LIM_UNAME_MEP == loginMethod) {
		if (newUserName.isEmpty() || acntName.isEmpty()
		        || mepToken.isEmpty()) {
			return tr("Username, communication code or data-box name has not been specified. These fields must be filled in.");
		}
	} else if (newUserName.isEmpty() || acntName.isEmpty()
	        || pwd.isEmpty()) {
		return tr("Username, password or data-box name has not been specified. These fields must be filled in.");
	}

	acntData.setAccountName(acntName.trimmed());
	acntData.setUserName(newUserName);
	acntData.setPassword(pwd);
	acntData.setMepToken(mepToken.trimmed());
	acntData.setLoginMethod(loginMethod);
	acntData.setTestAccount(isTestAccount);
	acntData.setRememberPwd(rememberPwd);
	acntData.setP12File(certPath);
	acntData.setSyncWithAll(syncWithAll);

	if (-2 == GlobInstcs::acntMapPtr->addAccount(acntData)) {
		return tr("Account with username '%1' already exists.").arg(newUserName);
	}

	PrefsSpecific::setDataOnDisk(*GlobInstcs::prefsPtr, acntData,
	    storeToDisk);

	return QString();
}

bool Accounts::updateAccount(AccountListModel *accountModel,
    enum AcntData::LoginMethod loginMethod, const QString &acntName,
    const QString &userName, const QString &pwd, const QString &mepToken,
    bool isTestAccount, bool rememberPwd, bool storeToDisk, bool syncWithAll,
    const QString &certPath)
{
	debugFuncCall();

	if (Q_UNLIKELY((GlobInstcs::prefsPtr == Q_NULLPTR) ||
	        (GlobInstcs::messageDbsPtr == Q_NULLPTR) ||
	        (GlobInstcs::fileDbsPtr == Q_NULLPTR) ||
	        (GlobInstcs::acntMapPtr == Q_NULLPTR))) {
		Q_ASSERT(0);
		return false;
	}

	if (Q_UNLIKELY(accountModel == Q_NULLPTR)) {
		logErrorNL("%s", "Cannot access account model.");
		Q_ASSERT(0);
		return false;
	}

	if (acntName.isEmpty()) {
		logErrorNL("%s", "Data-box name has not been specified.");
		return false;
	}

	AcntId acntId;
	acntId.setUsername(userName.trimmed());
	acntId.setTesting(isTestAccount);

	if (Q_UNLIKELY(!GlobInstcs::acntMapPtr->acntIds().contains(acntId))) {
		return false;
	}

	AcntData aData = GlobInstcs::acntMapPtr->acntData(acntId);

	/* update account map */
	aData.setLoginMethod(loginMethod);
	aData.setAccountName(acntName.trimmed());
	aData.setPassword(pwd);
	aData.setMepToken(mepToken.trimmed());
	aData.setTestAccount(isTestAccount);
	aData.setRememberPwd(rememberPwd);

	const bool oldStoreToDisk = PrefsSpecific::dataOnDisk(
	    *GlobInstcs::prefsPtr, aData);
	/* Reopen database if storeToDisk changed. */
	if (oldStoreToDisk != storeToDisk) {
		MessageDb *mDb = GlobInstcs::messageDbsPtr->accessMessageDb(
		    GlobalSettingsQmlWrapper::dbPath(), acntId, oldStoreToDisk);
		if (mDb != Q_NULLPTR) {
			QString mDbFileName(mDb->fileName());
			/* Reopen database. */
			GlobInstcs::messageDbsPtr->deleteDb(mDb);
			GlobInstcs::messageDbsPtr->accessMessageDb(
			    GlobalSettingsQmlWrapper::dbPath(),
			    acntId, storeToDisk);
			if (oldStoreToDisk) {
				/* Delete databases from storage. */
				QFile::remove(mDbFileName);
			}
		}
		FileDb *fDb = GlobInstcs::fileDbsPtr->accessFileDb(
		    GlobalSettingsQmlWrapper::dbPath(), acntId.username(),
		    oldStoreToDisk);
		if (fDb != Q_NULLPTR) {
			QString fDbFileName(fDb->fileName());
			/* Reopen database. */
			GlobInstcs::fileDbsPtr->deleteDb(fDb);
			GlobInstcs::fileDbsPtr->accessFileDb(
			    GlobalSettingsQmlWrapper::dbPath(),
			    acntId.username(), storeToDisk);
			if (oldStoreToDisk) {
				/* Delete databases from storage. */
				QFile::remove(fDbFileName);
			}
		}

		/* Inform the model that message counters have changed. */
		accountModel->updateCounters(acntId, 0, 0, 0, 0);
	}

	aData.setSyncWithAll(syncWithAll);
	aData.setP12File(certPath);
	PrefsSpecific::setDataOnDisk(*GlobInstcs::prefsPtr, aData,
	    storeToDisk);

	/* Model data have changed. */
	GlobInstcs::acntMapPtr->updateAccount(aData);

	/* Delete isds session context */
	emit removeIsdsCtx(acntId);

	return true;
}

bool Accounts::removeAccount(const QmlAcntId *qAcntId)
{
	debugFuncCall();

	if (Q_UNLIKELY((GlobInstcs::accountDbPtr == Q_NULLPTR) ||
	        (GlobInstcs::messageDbsPtr == Q_NULLPTR) ||
	        (GlobInstcs::fileDbsPtr == Q_NULLPTR) ||
	        (GlobInstcs::acntMapPtr == Q_NULLPTR) ||
	        (GlobInstcs::acntLogos == Q_NULLPTR) ||
	        (qAcntId == Q_NULLPTR))) {
		Q_ASSERT(0);
		return false;
	}

	if (Q_UNLIKELY(!GlobInstcs::acntMapPtr->acntIds().contains(*qAcntId))) {
		return false;
	}

	const bool dataOnDisk = PrefsSpecific::dataOnDisk(*GlobInstcs::prefsPtr,
	    GlobInstcs::acntMapPtr->acntData(*qAcntId));

	/* Delete file database. */
	FileDb *fDb = GlobInstcs::fileDbsPtr->accessFileDb(
	    GlobalSettingsQmlWrapper::dbPath(), qAcntId->username(), dataOnDisk);
	if (Q_UNLIKELY(fDb == Q_NULLPTR)) {
		logErrorNL("%s", "Cannot access file database.");
		return false;
	}
	if (Q_UNLIKELY(!GlobInstcs::fileDbsPtr->deleteDb(fDb))) {
		logErrorNL("%s", "Could not delete file database.");
		return false;
	}

	/* Delete message database. */
	MessageDb *msgDb = GlobInstcs::messageDbsPtr->accessMessageDb(
	    GlobalSettingsQmlWrapper::dbPath(), *qAcntId, dataOnDisk);
	if (Q_UNLIKELY(msgDb == Q_NULLPTR)) {
		logErrorNL("%s", "Cannot access message database.");
		return false;
	}
	if (Q_UNLIKELY(!GlobInstcs::messageDbsPtr->deleteDb(msgDb))) {
		logErrorNL("%s", "Could not delete message database.");
		return false;
	}

	/* Delete account zfo files. */
	if (Q_UNLIKELY(!deleteAllZfoFiles(qAcntId->username(),
	        qAcntId->testing()))) {
		logErrorNL("%s", "Could not delete account zfo files.");
		return false;
	}

	/* Delete account info from account database. */
	if (Q_UNLIKELY(!GlobInstcs::accountDbPtr->
	        deleteAccountInfoFromDb(qAcntId->username()))) {
		logErrorNL("%s", "Could not delete account info.");
		return false;
	}

	/* Delete user info from account database. */
	if (Q_UNLIKELY(!GlobInstcs::accountDbPtr->
	        deleteUserInfoFromDb(qAcntId->username()))) {
		logErrorNL("%s", "Could not delete user info.");
		return false;
	}

	/* Delete account logo file. */
	GlobInstcs::acntLogos->removeDataboxLogo(*qAcntId);

	/* Delete isds session context. */
	emit removeIsdsCtx(*qAcntId);

	/* Delete account from settings. */
	GlobInstcs::acntMapPtr->removeAccount(*qAcntId);

	return true;
}

void Accounts::loadModelCounters(AccountListModel *accountModel)
{
	debugFuncCall();

	if (Q_UNLIKELY(accountModel == Q_NULLPTR)) {
		logErrorNL("%s", "Cannot access account model.");
		Q_ASSERT(0);
		return;
	}

	for (int row = 0; row < accountModel->rowCount(); ++row) {
		const AcntId acntId = accountModel->acntId(accountModel->index(row, 0));
		if (Q_UNLIKELY(!acntId.isValid())) {
			Q_ASSERT(0);
			continue;
		}
		loadOneAccountCounters(*accountModel, acntId);
	}
}

QString Accounts::prepareChangeUserName(
    enum AcntData::LoginMethod loginMethod, const QString &acntName,
    const QString &newUserName, const QString &pwd, const QString &mepToken,
    bool isTestAccount, bool rememberPwd, bool storeToDisk, bool syncWithAll,
    const QString &certPath)
{
	debugFuncCall();

	return createAccount(loginMethod, acntName,
	    newUserName, pwd, mepToken, isTestAccount, rememberPwd, storeToDisk,
	    syncWithAll, certPath);
}

/*!
 * @brief Check whether the box identifier matches the username of a known account.
 *
 * @param[in] dbId Data box ID.
 * @param[in] username User name of already existing account.
 * @return True if username corresponds to data box id, false on any error.
 */
static
bool boxMatchesUser(const QString &dbId, const AcntId &acntId)
{
	if (Q_UNLIKELY(dbId.isEmpty() || (!acntId.isValid()))) {
		Q_ASSERT(0);
		return false;
	}

	return dbId == GlobInstcs::accountDbPtr->dbId(acntId.username());
}

/*!
 * @brief Change file database name to new username.
 *
 * @param[in]  oldUserName Original user name of account.
 * @param[in]  newUserName New user name identifying account.
 * @param[in]  storeToDisk True if database store to local storage.
 * @param[out] errTxt Error description.
 * @return True on success.
 */
static
bool changeFileDbName(const QString &oldUserName,
    const QString &newUserName, bool storeToDisk, QString &errTxt)
{
	debugFuncCall();

	FileDb *fDb = GlobInstcs::fileDbsPtr->accessFileDb(
	    GlobalSettingsQmlWrapper::dbPath(), oldUserName, storeToDisk);
	if (Q_UNLIKELY(fDb == Q_NULLPTR)) {
		errTxt = Accounts::tr("Cannot access file database for the username '%1'.")
		    .arg(oldUserName);
		return false;
	}

	/* Rename file database */
	QString currentDbFileName = fDb->fileName();
	currentDbFileName.replace(oldUserName, newUserName);
	if (!fDb->moveDb(currentDbFileName)) {
		errTxt = Accounts::tr("Cannot change file database to username '%1'.")
		    .arg(newUserName);
		return false;
	}

	return true;
}

/*!
 * @brief Change message database name to new user name.
 *
 * @param[in] oldUserName Original user name of account.
 * @param[in] newUserName New user name identifying account.
 * @param[in] storeToDisk True if database store to local storage.
 * @param[out] errTxt Error description.
 * @return True if success.
 */
static
bool changeMessageDbName(const QString &oldUserName,
    const QString &newUserName, bool testing, bool storeToDisk, QString &errTxt)
{
	debugFuncCall();

	MessageDb *msgDb = GlobInstcs::messageDbsPtr->accessMessageDb(
	    GlobalSettingsQmlWrapper::dbPath(), AcntId(oldUserName, testing),
	    storeToDisk);
	if (Q_UNLIKELY(msgDb == Q_NULLPTR)) {
		errTxt = Accounts::tr("Cannot access message database for username '%1'.")
		    .arg(oldUserName);
		return false;
	}

	/* Rename message database */
	QString currentDbFileName = msgDb->fileName();
	currentDbFileName.replace(oldUserName, newUserName);
	if (!msgDb->moveDb(currentDbFileName)) {
		errTxt = Accounts::tr("Cannot change message database to username '%1'.")
		    .arg(newUserName);
		return false;
	}

	return true;
}

QString Accounts::changeAccountUserName(
    const QString &newUserName, const QString &oldUserName,
    bool storeToDisk, const QString &newDbId)
{
	debugFuncCall();

	QString errTxt;
	const AcntId acntId =
	    GlobInstcs::acntMapPtr->acntIdFromUsername(oldUserName);
	const AcntId newAcntId =
	    GlobInstcs::acntMapPtr->acntIdFromUsername(newUserName);

	if (Q_UNLIKELY((GlobInstcs::acntMapPtr == Q_NULLPTR) ||
	        (GlobInstcs::fileDbsPtr == Q_NULLPTR) ||
	        (GlobInstcs::messageDbsPtr == Q_NULLPTR))) {
		Q_ASSERT(0);
		errTxt = tr("Internal error.");
		goto fail;
	}

	/* Check whether new box id corresponds with the old data-box id. */
	if (!boxMatchesUser(newDbId, acntId)) {
		errTxt = tr("Data-box identifier related to the new username '%1' doesn't correspond with the data-box identifier related to the old username '%2'.").arg(newUserName).arg(oldUserName);
		goto fail;
	}

	/* Change file database name or exit on error. */
	if (!changeFileDbName(oldUserName, newUserName, storeToDisk, errTxt)) {
		errTxt = tr("Cannot change the file database to match the new username '%1'.").arg(newDbId);
		goto fail;
	}

	/* Change message database name or rollback on error. */
	if (!changeMessageDbName(oldUserName, newUserName, acntId.testing(),
	        storeToDisk, errTxt)) {
		errTxt = tr("Cannot change the message database to match the new username '%1'.").arg(newDbId);
		goto rollbackFilesChanges;
	}

	/* Delete old account info and user info from account database. */
	GlobInstcs::accountDbPtr->deleteAccountInfoFromDb(oldUserName);
	GlobInstcs::accountDbPtr->deleteUserInfoFromDb(oldUserName);

	/* Delete original/old account from model and settings. */
	GlobInstcs::acntMapPtr->removeAccount(acntId);
	/* Delete ISDS session context of account  */
	emit removeIsdsCtx(acntId);

	logInfoNL("Username has been changed to '%s'.",
	     newUserName.toUtf8().constData());

	return QString();

rollbackFilesChanges:

	/* Change back file database name. */
	if (changeFileDbName(newUserName, oldUserName, storeToDisk, errTxt)) {
		logErrorNL("Cannot restore file database name '%s'.",
		    oldUserName.toUtf8().constData());
	}

fail:
	/* Remove new account. */
	GlobInstcs::acntMapPtr->removeAccount(newAcntId);
	/* Delete ISDS session context of account  */
	emit removeIsdsCtx(newAcntId);

	/* Delete new account info and user info from account database. */
	GlobInstcs::accountDbPtr->deleteAccountInfoFromDb(newUserName);
	GlobInstcs::accountDbPtr->deleteUserInfoFromDb(newUserName);

	return errTxt;
}

QString Accounts::getPasswordExpirationInfo(void)
{
	debugSlotCall();

	int days = GlobalSettingsQmlWrapper::pwdExpirationDays();
	if (days <= 0)  {
		return QString();
	}

	/* Get list of user name without password login method. */
	QStringList nonPwdUserNames;
	foreach (const AcntData &aData, GlobInstcs::acntMapPtr->values()) {
		if (aData.loginMethod() == AcntData::LIM_UNAME_MEP ||
		        aData.loginMethod() == AcntData::LIM_UNAME_CRT) {
			nonPwdUserNames.append(aData.userName());
		}
	}

	/* Skip if all user accounts don't contain login with password. */
	if (GlobInstcs::acntMapPtr->count() == nonPwdUserNames.count()) {
		return QString();
	}

	/* Return all expired user names where login method uses password. */
	QStringList exprList =
	    GlobInstcs::accountDbPtr->getPasswordExpirationList(nonPwdUserNames,
	    days);
	if (exprList.isEmpty()) {
		return QString();
	}

	QString detailText;
	foreach (const QString &expItem, exprList) {
		detailText.append(expItem);
		detailText.append("\n");
	}

	return detailText.trimmed();
}

void Accounts::updateLastSyncTime(const QmlAcntId *qAcntId)
{
	if (Q_UNLIKELY((qAcntId == Q_NULLPTR) ||
		GlobInstcs::accountDbPtr == Q_NULLPTR)) {
		Q_ASSERT(0);
		return;
	}

	GlobInstcs::accountDbPtr->updateLastSyncTime(qAcntId->username(),
	    qAcntId->testing(), QDateTime::currentDateTime());

	/* Account model is redrawn automatically. */
}

QString Accounts::databoxLogo(const QmlAcntId *qAcntId)
{
	if (Q_UNLIKELY((GlobInstcs::acntLogos == Q_NULLPTR)
	        || (qAcntId == Q_NULLPTR))) {
		Q_ASSERT(0);
		return QString();
	}
	return GlobInstcs::acntLogos->databoxLogoLocation(*qAcntId);
}

bool Accounts::setDataboxLogo(const QmlAcntId *qAcntId,
    const QString &srcFilePath)
{
	if (Q_UNLIKELY((GlobInstcs::acntLogos == Q_NULLPTR)
	        || (qAcntId == Q_NULLPTR))) {
		Q_ASSERT(0);
		return false;
	}

	if (Q_UNLIKELY(srcFilePath.isEmpty())) {
		return false;
	}

	QString suffix;
#if defined(Q_OS_ANDROID)
	/*
	 * Android file provider returns the path with file index instead of
	 * the file name. File name and its suffix must be derived from
	 * file info explicitly.
	 */
	QFileInfo fn(QFileInfo(srcFilePath).fileName());
	suffix = fn.suffix().toLower();
#else
	suffix = QFileInfo(srcFilePath).suffix().toLower();
#endif

	if (suffix != "png" && suffix != "jpg" && suffix != "jpeg" &&
	        suffix != "bmp" && suffix != "svg" && suffix != "gif") {
	        logErrorNL("File '%s' is not valid image format.",
		    srcFilePath.toUtf8().constData());
		return false;
	}

	/*
	 * Explicitly reset logo before setting a new logo.
	 * This enforces the QML engine to reload the image in the account model.
	 */
	{
		if (Q_UNLIKELY(!AccountLogos::fileContainsImage(srcFilePath))) {
			return false;
		}
		/* Only reset the image if replacement is valid. */
		GlobInstcs::acntLogos->removeDataboxLogo(*qAcntId);
	}

	/* Set custom account logo. */
	GlobInstcs::acntLogos->storeDataboxLogo(*qAcntId, srcFilePath);
	return true;
}

bool Accounts::resetDataboxLogo(const QmlAcntId *qAcntId)
{
	if (Q_UNLIKELY((GlobInstcs::acntLogos == Q_NULLPTR)
	        || (qAcntId == Q_NULLPTR))) {
		Q_ASSERT(0);
		return false;
	}

	GlobInstcs::acntLogos->removeDataboxLogo(*qAcntId);
	return true;
}
