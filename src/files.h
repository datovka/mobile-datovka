/*
 * Copyright (C) 2014-2025 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

#pragma once

#include <QObject>

#include "src/messages.h"
#include "src/qml_interaction/message_info.h"

class FileListModel; /* Forward declaration. */
class QmlAcntId; /* Forward declaration. */

/*
 * Class Files provides interface between QML and file database.
 * Class is initialised in the main function (main.cpp)
 */
class Files : public QObject {
    Q_OBJECT

public:
	/* Defines file type to be saved or sent via email */
	enum MsgAttachFlag {
		NO_FILES = 0x00,
		MSG_ZFO = 0x01, /* Complete message in zfo format. */
		MSG_ATTACHS = 0x02 /* Attachments of the data message. */
	};
	Q_ENUM(MsgAttachFlag)
	/*
	 * Flags inside QML:
	 * https://forum.qt.io/topic/10060/q_enums-q_declare_metatype-and-qml/2
	 */
	Q_DECLARE_FLAGS(MsgAttachFlags, MsgAttachFlag)
	Q_FLAG(MsgAttachFlags)

	/* Defines missing file id vaule and zfo id */
	enum FileIdType {
		NO_FILE_ID = -1,
		DB_ZFO_ID = -2
	};
	Q_ENUM(FileIdType)

	/*!
	 * @brief Declare various properties to the QML system.
	 */
	static
	void declareQML(void);

	/*!
	 * @brief Constructor.
	 *
	 * @param[in] parent Parent object.
	 */
	explicit Files(QObject *parent = Q_NULLPTR);

	/*!
	 * @brief Delete files from all databases where file lifetime expired.
	 *
	 * @param[in] days Lifetime in days.
	 */
	void deleteExpiredFilesFromDbs(int days);

	/*!
	 * @brief Get attachment file icon from file name.
	 *
	 * @param[in] fileName File name.
	 * @return File icon resources string.
	 */
	Q_INVOKABLE static
	QString getAttachmentFileIcon(const QString &fileName);

	/*!
	 * @brief Get attachment file size in bytes.
	 *
	 * @param[in] filePath Path to file.
	 * @return File size in bytes.
	 */
	Q_INVOKABLE static
	qint64 getAttachmentSizeInBytes(const QString &filePath);

	/*!
	 * @brief Open directory dialog.
	 *
	 * @param[in] caption Caption string.
	 * @param[in] dir Base directory displayed in the dialog.
	 * @return Directory path.
	 */
	Q_INVOKABLE static
	QString openDirectoryDialog(const QString &caption, const QString &dir);

	/*!
	 * @brief Open file dialog.
	 *
	 * @param[in] caption Caption string.
	 * @param[in] dir Base directory displayed in the dialog.
	 * @param[in] filter File type filter.
	 * @return File path.
	 */
	Q_INVOKABLE static
	QString openFileDialog(const QString &caption, const QString &dir,
	    const QString &filter);

	/*!
	 * @brief Get real attachment file path.
	 *
	 * @param[in] filePath Path to target file.
	 * @return Full file path or file name.
	 */
	Q_INVOKABLE static
	QString getRealAttachmentFilePath(const QString &filePath);

	/*!
	 * @brief Get real attachment file name.
	 *
	 * @param[in] filePath Path to target file.
	 * @return Full file path or file name.
	 */
	Q_INVOKABLE static
	QString getRealFileNameFromPath(const QString &filePath);

	/*!
	 * @brief Get path to file only.
	 *
	 * @param[in] filePath Path to file.
	 * @return Return path only to target file.
	 */
	Q_INVOKABLE static
	QString getPathToFile(const QString &filePath);

	/*!
	 * @brief Test if OS is Android 11 and latest.
	 *
	 * @param[in] True if Android 11 and latest.
	 */
	Q_INVOKABLE static
	bool isAndroid11AndLatest(void);

	/*!
	 * @brief Obtain attachment from database.
	 *
	 * @param[in] qAcntId Account identifier.
	 * @param[in] fileId Attachment file identifier.
	 * @return Attachment content.
	 */
	Q_INVOKABLE static
	QByteArray getFileRawContentFromDb(const QmlAcntId *qAcntId, int fileId);

	/*!
	 * @brief Open attachment from database.
	 *
	 * @param[in] qAcntId Account identifier.nt.
	 * @param[in] fileId Attachment file identifier.
	 */
	Q_INVOKABLE
	void openAttachmentFromDb(const QmlAcntId *qAcntId, int fileId);

	/*!
	 * @brief Open attachment in default application.
	 *
	 * @param[in] fileName File name.
	 * @param[in] binaryData Raw file content.
	 */
	Q_INVOKABLE
	void openAttachment(const QString &fileName,
	    const QByteArray &binaryData);

	/*!
	 * @brief Send message attachments or complete zfo message
	 *     from database with email application.
	 *
	 * @param[in] qAcntId Account identifier.
	 * @param[in] msgId Message id.
	 * @param[in] msgType Message type.
	 * @param[in] attachFlags Specifies which attachments to send.
	 */
	Q_INVOKABLE
	void sendMsgFilesWithEmail(const QmlAcntId *qAcntId, qint64 msgId,
	    enum Messages::MessageType msgType, MsgAttachFlags attachFlags);

	/*!
	 * @brief Send message attachments from zfo with email application.
	 *
	 * @param[in] attachModel Attachment model.
	 * @param[in] qAcntId Account identifier.
	 * @param[in] msgIdStr String with message id.
	 * @param[in] msgType Message type.
	 * @param[in] attachFlags Specifies which attachments to send.
	 */
	Q_INVOKABLE static
	void sendAttachmentsWithEmail(const FileListModel *attachModel,
	    const QmlAcntId *qAcntId, const QString &msgIdStr,
	    enum Messages::MessageType msgType, MsgAttachFlags attachFlags);

	/*!
	 * @brief Checks whether file is readable.
	 *
	 * @param[in] filePath Path to file.
	 * @return True if file exists and is readable.
	 */
	static
	bool fileReadable(const QString &filePath);

	/*!
	 * @brief Tests whether attachment is ZFO file.
	 *
	 * @param[in] fileName File name.
	 * @return True if file has zfo suffix.
	 */
	Q_INVOKABLE static
	bool isZfoFile(const QString &fileName);

	/*!
	 * @brief Returns raw file content.
	 *
	 * @param[in] filePath Path to file.
	 * @return File content, QByteArray() on error.
	 */
	Q_INVOKABLE static
	QByteArray rawFileContent(const QString &filePath);

	/*!
	 * @brief Parse content of ZFO file.
	 *
	 * @note QML handles deallocation of returned objects. For more retail
	 *     see section 'Data Ownership'
	 *     of 'Data Type Conversion Between QML and C++'
	 *     at http://doc.qt.io/qt-5/qtqml-cppintegration-data.html .
	 *
	 * @param[in,out] attachModel Attachment model to be set.
	 * @param[in] rawZfoData Raw ZFO data.
	 * @return Pointer to newly allocated structure containing message
	 *     information.
	 */
	Q_INVOKABLE static
	MsgInfo *zfoData(FileListModel *attachModel,
	    const QByteArray &rawZfoData);

	/*!
	 * @brief Sets attachment model.
	 *
	 * @param[out] attachModel Attachment model to be set.
	 * @param[in] qAcntId Account identifier.
	 * @param[in] msgId Message identifier.
	 * @return True if success.
	 */
	static
	bool setAttachmentModel(FileListModel &attachModel,
	    const QmlAcntId *qAcntId, qint64 msgId);

	/*!
	 * @brief Send attachment from ZFO with email application.
	 *
	 * @note QML does not know qint64 therefore we use a QString as
	 *     message id.
	 *
	 * @param[in] attachModel Attachment model.
	 * @param[in] qAcntId Account identifier.
	 * @param[in] msgIdStr String with message id.
	 * @param[in] msgType Message type.
	 * @param[in] subject Email subject.
	 * @param[in] body Email body.
	 * @param[in] attachFlags Specifies which attachments to be sent.
	 */
	Q_INVOKABLE static
	void sendAttachmentEmailZfo(const FileListModel *attachModel,
	    const QmlAcntId *qAcntId, const QString &msgIdStr,
	    enum Messages::MessageType msgType, QString subject, QString body,
	    MsgAttachFlags attachFlags);

	/*!
	 * @brief Save message attachments or complete zfo message from
	 *        database to external storage.
	 *
	 * @param[in] qAcntId Account identifier.
	 * @param[in] msgIdStr String with message id.
	 * @param[in] msgType Message type.
	 * @param[in] attachFlags Specifies which attachments to save.
	 * @param[in] storeOnCloud True if store files on cloud.
	 */
	Q_INVOKABLE
	void saveMsgFilesToDisk(const QmlAcntId *qAcntId,
	    const QString &msgIdStr, enum Messages::MessageType msgType,
	    MsgAttachFlags attachFlags, bool storeOnCloud);

	/*!
	 * @brief Save attachments from ZFO to disk.
	 *
	 * @param[in] attachModel Attachment model.
	 * @param[in] qAcntId Account identifier.
	 * @param[in] msgIdStr String with message id.
	 * @param[in] storeOnCloud True if store files on cloud.
	 */
	Q_INVOKABLE
	void saveAttachmentsToDiskZfo(const FileListModel *attachModel,
	    const QmlAcntId *qAcntId, const QString &msgIdStr, bool storeOnCloud);

	/*!
	 * @brief Save ZFO file to disk.
	 *
	 * @param[in] qAcntId Account identifier.
	 * @param[in] msgIdStr String with message id.
	 * @param[in] msgType Message type.
	 * @param[in] storeOnCloud True if store files on cloud.
	 */
	Q_INVOKABLE
	void saveZfoFilesToDisk(const QmlAcntId *qAcntId,
	    const QString &msgIdStr, enum Messages::MessageType msgType,
	    bool storeOnCloud);

	/*!
	 * @brief Save PDF file to disk.
	 *
	 * @param[in] qAcntId Account identifier.
	 * @param[in] msgIdStr String with message id.
	 * @param[in] msgType Message type.
	 * @param[in] storeOnCloud True if store files on cloud.
	 */
	Q_INVOKABLE
	void saveMsgEnvelopePdfToDisk(const QmlAcntId *qAcntId,
	    const QString &msgIdStr, enum Messages::MessageType msgType,
	    bool storeOnCloud);

	/*!
	 * @brief Save PDF file to disk.
	 *
	 * @param[in] qAcntId Account identifier.
	 * @param[in] msgIdStr String with message id.
	 * @param[in] storeOnCloud True if store files on cloud.
	 */
	Q_INVOKABLE
	void saveDeliveryInfoPdfToDisk(const QmlAcntId *qAcntId,
	    const QString &msgIdStr, bool storeOnCloud);

	/*!
	 * @brief Delete temporary file from storage.
	 *
	 * @note This function is used on iOS when external ZFO file is opened.
	 *       iOS moves target file to Datovka private folder. Here, after
	 *       reading of file content, Datovka must this file removed itself.
	 *
	 * @param[filePath] Path to file.
	 */
	Q_INVOKABLE
	void deleteTmpFileFromStorage(const QString &filePath);

	/*!
	 * @brief Create final PDF file into send location.
	 *
	 * @param[in] text Text message.
	 * @return PDF file path.
	 */
	Q_INVOKABLE
	QString appendPDF(const QString &text);

	/*!
	 * @brief Create PDF file into tmp location and open preview.
	 *
	 * @param[in] text Text message.
	 */
	Q_INVOKABLE
	void viewPDF(const QString &text);

signals:

	/*!
	 * @brief Show QML info dialogue.
	 *
	 * @param[in] title Dialogue title.
	 * @param[in] text Dialogue text.
	 * @param[in] detailText Dialogue detail text.
	 */
	void showMessageBox(QString title, QString text, QString detailText);

private:

	/*!
	 * @brief Open attachment from path in default application.
	 *
	 * @param[in] filePath File path.
	 */
	Q_INVOKABLE
	void openAttachmentFromPath(const QString &filePath);

	/*!
	 * @brief Parse xml data of zfo file.
	 *
	 * @todo This function must be reworked as it is called multiple times
	 *     on a single message to obtain various data.
	 *
	 * @param[out] type ZFO type.
	 * @param[out] idStr Message identifier number held in a string.
	 * @param[out] annotation Annotation string.
	 * @param[out] msgDescrHtml Message text.
	 * @param[out] attachModel Attachment model to be set.
	 * @param[out] emailBody Email body.
	 * @param[in] xmlData Xml file data.
	 * @return True if success.
	 */
	static
	bool parseXmlData(enum MsgInfo::ZfoType *type, QString *idStr,
	    QString *annotation, QString *msgDescrHtml,
	    FileListModel *attachModel, QString *emailBody, QByteArray xmlData);

	/*!
	 * @brief Parse and show xml data of zfo file.
	 *
	 * @param[in] type ZFO file type.
	 * @param[out] idStr Message identifier number held in a string.
	 * @param[out] annotation Annotation string.
	 * @param[out] msgDescrHtml Message text.
	 * @param[out] attachModel Attachment model to be set.
	 * @param[out] emailBody Email body.
	 * @param[in] xmlData Xml file data.
	 * @return True if success.
	 */
	static
	bool parseAndShowXmlData(enum MsgInfo::ZfoType type, QString *idStr,
	    QString *annotation, QString *msgDescrHtml,
	    FileListModel *attachModel,  QString *emailBody,
	    QByteArray &xmlData);

	/*!
	 * @brief Export files and show export results.
	 *
	 * @param[in] iCloudPath iCloud path.
	 * @param[in] destFilePath Target path for export.
	 * @param[in] files File list for iOS export.
	 * @param[in] storeOnCloud True if store files on cloud.
	 */
	void exportFiles(const QString &iCloudPath, const QString &destFilePath,
	    const QStringList &files, bool storeOnCloud);
};

Q_DECLARE_OPERATORS_FOR_FLAGS(Files::MsgAttachFlags)

/* Declare FileIdType to QML. */
Q_DECLARE_METATYPE(Files::FileIdType)
Q_DECLARE_METATYPE(Files::MsgAttachFlag)
Q_DECLARE_METATYPE(Files::MsgAttachFlags)
