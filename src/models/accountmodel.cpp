/*
 * Copyright (C) 2014-2025 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

#include <QQmlEngine> /* qmlRegisterType */
#include <QSettings>

#include "src/datovka_shared/crypto/crypto_pwd.h"
#include "src/datovka_shared/crypto/crypto_wrapped.h"
#include "src/datovka_shared/log/log.h"
#include "src/datovka_shared/settings/prefs.h"
#include "src/models/accountmodel.h"
#include "src/settings/account.h"
#include "src/settings/account_logos.h"
#include "src/settings/accounts.h"
#include "src/settings/prefs_specific.h"
#include "src/sqlite/account_db.h"

/* Null objects - for convenience. */
static const AcntId nullAcntId;

void AccountListModel::declareQML(void)
{
	qmlRegisterType<AccountListModel>("cz.nic.mobileDatovka.models", 1, 0, "AccountListModel");
	qRegisterMetaType<AccountListModel>("AccountListModel");
	qRegisterMetaType<AccountListModel::Roles>("AccountListModel::Roles");

	qRegisterMetaType<AccountListModel *>("AccountListModel *");
	qRegisterMetaType<AccountListModel *>("const AccountListModel *");
}

AccountListModel::AccountListModel(QObject *parent)
    : QAbstractListModel(parent),
    m_accountsPtr(Q_NULLPTR),
    m_accountDbPtr(Q_NULLPTR),
    m_accountLogos(Q_NULLPTR),
    m_prefs(Q_NULLPTR),
    m_countersMap()
{
}

AccountListModel::AccountListModel(const AccountListModel &other,
    QObject *parent)
    : QAbstractListModel(parent),
    m_accountsPtr(Q_NULLPTR),
    m_accountDbPtr(Q_NULLPTR),
    m_accountLogos(Q_NULLPTR),
    m_prefs(Q_NULLPTR),
    m_countersMap(other.m_countersMap)
{
	loadContent(other.m_accountsPtr, other.m_accountDbPtr,
	    other.m_accountLogos, other.m_prefs);
}

int AccountListModel::rowCount(const QModelIndex &parent) const
{
	if (!parent.isValid()) {
		/* Root. */
		if (m_accountsPtr != Q_NULLPTR) {
			return m_accountsPtr->acntIds().size();
		}
	}

	return 0;

}

QHash<int, QByteArray> AccountListModel::roleNames(void) const
{
	static QHash<int, QByteArray> roles;
	if (roles.isEmpty()) {
		roles[ROLE_ACCOUNT_NAME] = "rAcntName";
		roles[ROLE_USER_NAME] = "rUserName";
		roles[ROLE_TEST_ACCOUNT] = "rTestAccount";
		roles[ROLE_STORE_INTO_DB] = "rStoreIntoDb";
		roles[ROLE_RECEIVED_UNREAD] = "rRcvdUnread";
		roles[ROLE_RECEIVED_TOTAL] = "rRcvdTotal";
		roles[ROLE_SENT_UNREAD] = "rSntUnread";
		roles[ROLE_SENT_TOTAL] = "rSntTotal";
		roles[ROLE_LAST_SYNC_TIME] = "rLastSyncTime";
		roles[ROLE_DATABOX_ID] = "rDataboxId";
		roles[ROLE_DATABOX_LOGO] = "rDataboxLogo";
	}
	return roles;
}

QVariant AccountListModel::data(const QModelIndex &index, int role) const
{
	if (Q_UNLIKELY(m_accountsPtr == Q_NULLPTR)) {
		Q_ASSERT(0);
		return QVariant();
	}

	if ((index.row() < 0) || (index.row() >= rowCount())) {
		return QVariant();
	}

	const AcntId &aId(acntId(index));
	if (Q_UNLIKELY(!aId.isValid())) {
		Q_ASSERT(0);
		return QVariant();
	}

	switch (role) {
	case ROLE_ACCOUNT_NAME:
		return m_accountsPtr->acntData(aId).accountName();
		break;
	case ROLE_USER_NAME:
		return m_accountsPtr->acntData(aId).userName();
		break;
	case ROLE_TEST_ACCOUNT:
		return m_accountsPtr->acntData(aId).isTestAccount();
		break;
	case ROLE_STORE_INTO_DB:
		if (m_prefs != Q_NULLPTR) {
			return PrefsSpecific::dataOnDisk(*m_prefs,
			    m_accountsPtr->acntData(aId));
		} else {
			return true;
		}
		break;
	case ROLE_RECEIVED_UNREAD:
		{
			Q_ASSERT(m_countersMap.constFind(aId) !=
			    m_countersMap.constEnd());
			const AccountCounters &cntrs = m_countersMap[aId];
			return cntrs.unreadReceived;
		}
		break;
	case ROLE_RECEIVED_TOTAL:
		{
			Q_ASSERT(m_countersMap.constFind(aId) !=
			    m_countersMap.constEnd());
			const AccountCounters &cntrs = m_countersMap[aId];
			return cntrs.totalReceived;
		}
		break;
	case ROLE_SENT_UNREAD:
		{
			Q_ASSERT(m_countersMap.constFind(aId) !=
			    m_countersMap.constEnd());
			const AccountCounters &cntrs = m_countersMap[aId];
			return cntrs.unreadSent;
		}
		break;
	case ROLE_SENT_TOTAL:
		{
			Q_ASSERT(m_countersMap.constFind(aId) !=
			    m_countersMap.constEnd());
			const AccountCounters &cntrs = m_countersMap[aId];
			return cntrs.totalSent;
		}
		break;
	case ROLE_LAST_SYNC_TIME:
		if (m_accountDbPtr != Q_NULLPTR) {
			return m_accountDbPtr->lastSyncTime(
			    aId.username(), aId.testing());
		}
		return QString();
		break;
	case ROLE_DATABOX_ID:
		if (m_accountDbPtr != Q_NULLPTR) {
			return m_accountDbPtr->dbId(aId.username());
		}
		return QString();
		break;
	case ROLE_DATABOX_LOGO:
		if (m_accountLogos != Q_NULLPTR) {
			return m_accountLogos->databoxLogoLocation(aId);
		}
		return QString();
		break;
	default:
		/* Do nothing. */
		break;
	}

	return QVariant();
}

Qt::ItemFlags AccountListModel::flags(const QModelIndex &index) const
{
	return QAbstractListModel::flags(index);
}

bool AccountListModel::moveRows(const QModelIndex &sourceParent, int sourceRow,
    int count, const QModelIndex &destinationParent, int destinationChild)
{
	if (sourceParent.isValid() || destinationParent.isValid()) {
		/* Only moves within root node are allowed. */
		return false;
	}

	return m_accountsPtr->moveAccounts(sourceRow, count,
	    destinationChild);
}

bool AccountListModel::removeRows(int row, int count, const QModelIndex &parent)
{
	Q_UNUSED(row);
	Q_UNUSED(count);
	Q_UNUSED(parent);

	return false;
}

void AccountListModel::loadContent(AccountsMap *accounts, AccountDb *accountDb,
    AccountLogos *accountLogos, Prefs *prefs)
{
	if (m_accountsPtr != Q_NULLPTR) {
		m_accountsPtr->disconnect(SIGNAL(accountAboutToBeAdded(AcntId, int)),
		    this, SLOT(watchAccountAboutToBeAdded(AcntId, int)));
		m_accountsPtr->disconnect(SIGNAL(accountAdded(AcntId, int)),
		    this, SLOT(watchAccountAdded(AcntId, int)));

		m_accountsPtr->disconnect(SIGNAL(accountAboutToBeRemoved(AcntId, int)),
		    this, SLOT(watchAccountAboutToBeRemoved(AcntId, int)));
		m_accountsPtr->disconnect(SIGNAL(accountRemoved(AcntId, int)),
		    this, SLOT(watchAccountRemoved(AcntId, int)));

		m_accountsPtr->disconnect(SIGNAL(accountsAboutToBeMoved(int, int, int)),
		    this, SLOT(watchAccountsAboutToBeMoved(int, int, int)));
		m_accountsPtr->disconnect(SIGNAL(accountsMoved(int, int, int)),
		    this, SLOT(watchAccountsMoved(int, int, int)));

		m_accountsPtr->disconnect(SIGNAL(accountUsernameChanged(AcntId, AcntId, int)),
		    this, SLOT(watchUsernameChanged(AcntId, AcntId, int)));
		m_accountsPtr->disconnect(SIGNAL(accountDataChanged(AcntId)),
		    this, SLOT(watchAcountChanged(AcntId)));
	}

	if (m_accountDbPtr != Q_NULLPTR) {
		m_accountDbPtr->disconnect(SIGNAL(dataUpdated(AcntId)),
		    this, SLOT(watchAcountChanged(AcntId)));

		m_accountDbPtr = Q_NULLPTR;
	}

	if (m_accountLogos != Q_NULLPTR) {
		m_accountLogos->disconnect(SIGNAL(contentAboutToBeReset()),
		    this, SLOT(watchContentAboutToBeReset()));
		m_accountLogos->disconnect(SIGNAL(contentReset()),
		    this, SLOT(watchContentReset()));
		m_accountLogos->disconnect(SIGNAL(dataInserted(AcntId)),
		    this, SLOT(watchAcountChanged(AcntId)));
		m_accountLogos->disconnect(SIGNAL(dataRemoved(AcntId)),
		    this, SLOT(watchAcountChanged(AcntId)));
		m_accountLogos->disconnect(SIGNAL(dataChanged(AcntId)),
		    this, SLOT(watchAcountChanged(AcntId)));

		m_accountLogos = Q_NULLPTR;
	}

	if (m_prefs != Q_NULLPTR) {
		m_prefs->disconnect(SIGNAL(dataUpdated(AcntId)),
		    this, SLOT(watchAcountChanged(AcntId)));

		m_prefs = Q_NULLPTR;
	}

	const QModelIndex parent;
	int rows = rowCount(parent);
	if (rows > 0) {
		beginRemoveRows(parent, 0, rows - 1);
		m_accountsPtr = Q_NULLPTR;
		m_countersMap.clear();
		endRemoveRows();
	}

	if (accounts == Q_NULLPTR) {
		return;
	}

	if (accounts->keys().isEmpty()) {
		m_accountsPtr = accounts;
	} else {
		rows = accounts->acntIds().size();
		beginInsertRows(parent, 0, rows - 1);
		m_accountsPtr = accounts;
		for (const AcntId &acntId : m_accountsPtr->acntIds()) {
			/* Prepare counters. */
			m_countersMap[acntId] = AccountCounters();
		}
		endInsertRows();
	}

	connect(m_accountsPtr, SIGNAL(accountAboutToBeAdded(AcntId, int)),
	    this, SLOT(watchAccountAboutToBeAdded(AcntId, int)));
	connect(m_accountsPtr, SIGNAL(accountAdded(AcntId, int)),
	    this, SLOT(watchAccountAdded(AcntId, int)));

	connect(m_accountsPtr, SIGNAL(accountAboutToBeRemoved(AcntId, int)),
	    this, SLOT(watchAccountAboutToBeRemoved(AcntId, int)));
	connect(m_accountsPtr, SIGNAL(accountRemoved(AcntId, int)),
	    this, SLOT(watchAccountRemoved(AcntId, int)));

	connect(m_accountsPtr, SIGNAL(accountsAboutToBeMoved(int, int, int)),
	    this, SLOT(watchAccountsAboutToBeMoved(int, int, int)));
	connect(m_accountsPtr, SIGNAL(accountsMoved(int, int, int)),
	    this, SLOT(watchAccountsMoved(int, int, int)));

	connect(m_accountsPtr, SIGNAL(accountUsernameChanged(AcntId, AcntId, int)),
	    this, SLOT(watchUsernameChanged(AcntId, AcntId, int)));
	connect(m_accountsPtr, SIGNAL(accountDataChanged(AcntId)),
	    this, SLOT(watchAcountChanged(AcntId)));

	if (accountDb != Q_NULLPTR) {
		m_accountDbPtr = accountDb;

		connect(m_accountDbPtr, SIGNAL(dataUpdated(AcntId)),
		    this, SLOT(watchAcountChanged(AcntId)));
	}

	if (accountLogos != Q_NULLPTR) {
		m_accountLogos = accountLogos;

		connect(m_accountLogos, SIGNAL(contentAboutToBeReset()),
		    this, SLOT(watchContentAboutToBeReset()));
		connect(m_accountLogos, SIGNAL(contentReset()),
		    this, SLOT(watchContentReset()));
		connect(m_accountLogos, SIGNAL(dataInserted(AcntId)),
		    this, SLOT(watchAcountChanged(AcntId)));
		connect(m_accountLogos, SIGNAL(dataRemoved(AcntId)),
		    this, SLOT(watchAcountChanged(AcntId)));
		connect(m_accountLogos, SIGNAL(dataChanged(AcntId)),
		    this, SLOT(watchAcountChanged(AcntId)));
	}

	if (prefs != Q_NULLPTR) {
		m_prefs = prefs;

		connect(m_prefs, SIGNAL(entrySet(int, QString, QVariant)),
		    this, SLOT(watchPrefsModification(int, QString, QVariant)));
	}
}

const AcntId &AccountListModel::acntId(const QModelIndex &index) const
{
	if (Q_UNLIKELY((m_accountsPtr == Q_NULLPTR) || (!index.isValid()))) {
		return nullAcntId;
	}

	int row = index.row();

	if ((row >= 0) && (row < m_accountsPtr->size())) {
		return m_accountsPtr->acntIds().at(row);
	} else {
		return nullAcntId;
	}
}

QModelIndex AccountListModel::acntIndex(const AcntId &acntId) const
{
	if (Q_UNLIKELY(m_accountsPtr == Q_NULLPTR)) {
		Q_ASSERT(0);
		return QModelIndex();
	}

	const int row = m_accountsPtr->acntIds().indexOf(acntId);
	if (row >= 0) {
		return index(row, 0, QModelIndex());
	}

	return QModelIndex();
}

void AccountListModel::updateCounters(const AcntId &acntId, int recNew,
    int recTot, int sntNew, int sntTot)
{
	if (Q_UNLIKELY(m_accountsPtr == Q_NULLPTR)) {
		Q_ASSERT(0);
		return;
	}

	if (Q_UNLIKELY(!acntId.isValid())) {
		Q_ASSERT(0);
		return;
	}

	if ((recNew < 0) && (recTot < 0) && (sntTot < 0)) {
		return;
	}

	if (Q_UNLIKELY(!m_accountsPtr->acntIds().contains(acntId))) {
		Q_ASSERT(0);
		return;
	}

	AccountCounters newCntrs;

	if (recNew >= 0) {
		newCntrs.unreadReceived = recNew;
	}

	if (recTot >= 0) {
		newCntrs.totalReceived = recTot;
	}

	if (sntNew >= 0) {
		newCntrs.unreadSent = sntNew;
	}

	if (sntTot >= 0) {
		newCntrs.totalSent = sntTot;
	}

	const AccountCounters oldCntrs = m_countersMap[acntId];
	if (oldCntrs != newCntrs) {
		m_countersMap[acntId] = newCntrs;
		watchAcountChanged(acntId);
	}
}

AccountListModel *AccountListModel::fromVariant(const QVariant &modelVariant)
{
	if (!modelVariant.canConvert<QObject *>()) {
		return Q_NULLPTR;
	}
	QObject *obj = qvariant_cast<QObject *>(modelVariant);
	return qobject_cast<AccountListModel *>(obj);
}

void AccountListModel::move(int source, int destination)
{
	if ((source >= 0) && (source < rowCount())
	    && (destination >= 0) && (destination < rowCount())
	    && (source != destination)) {
		int modelDestination = destination;
		if (source < destination) {
			/*
			 * Model method moveRows() behaves slightly
			 * differently than the list method move().
			 */
			++modelDestination;
		}
		moveRows(QModelIndex(), source, 1, QModelIndex(), modelDestination);
	}
}

QString AccountListModel::accountName(void) const
{
	if (rowCount() == 1) {
		const AcntData aData = m_accountsPtr->acntData(
		    m_accountsPtr->acntIds().at(0));
		return aData.accountName();
	} else {
		return QString();
	}
}

QString AccountListModel::userName(void) const
{
	if (rowCount() == 1) {
		const AcntId acntId = m_accountsPtr->acntIds().at(0);
		return acntId.username();
	} else {
		return QString();
	}
}

bool AccountListModel::testing(void) const
{
	if (rowCount() == 1) {
		const AcntId acntId = m_accountsPtr->acntIds().at(0);
		return acntId.testing();
	} else {
		return true;
	}
}

void AccountListModel::watchContentAboutToBeReset(void)
{
	beginResetModel();
}

void AccountListModel::watchContentReset(void)
{
	endResetModel();
}

void AccountListModel::watchAccountAboutToBeAdded(const AcntId &acntId, int row)
{
	beginInsertRows(QModelIndex(), row, row);

	m_countersMap[acntId] = AccountCounters();
}

void AccountListModel::watchAccountAdded(const AcntId &acntId, int row)
{
	Q_UNUSED(acntId);
	Q_UNUSED(row);

	endInsertRows();
}

void AccountListModel::watchAccountAboutToBeRemoved(const AcntId &acntId, int row)
{
	beginRemoveRows(QModelIndex(), row, row);

	m_countersMap.remove(acntId);
}

void AccountListModel::watchAccountRemoved(const AcntId &acntId, int row)
{
	Q_UNUSED(acntId);
	Q_UNUSED(row);

	endRemoveRows();
}

void AccountListModel::watchAccountsAboutToBeMoved(int srcFrst, int srcLst, int dst)
{
	const QModelIndex parent;

	beginMoveRows(parent, srcFrst, srcLst, parent, dst);
}

void AccountListModel::watchAccountsMoved(int srcFrst, int srcLst, int dst)
{
	Q_UNUSED(srcFrst);
	Q_UNUSED(srcLst);
	Q_UNUSED(dst);

	endMoveRows();
}

void AccountListModel::watchUsernameChanged(const AcntId &oldId,
    const AcntId &newId, int index)
{
	Q_UNUSED(index);

	m_countersMap.insert(newId, m_countersMap.take(oldId));

	const QModelIndex topIndex(acntIndex(newId));
	if (topIndex.isValid()) {
		Q_EMIT dataChanged(topIndex, topIndex);
	}
}

void AccountListModel::watchAcountChanged(const AcntId &acntId)
{
	const QModelIndex topIndex(acntIndex(acntId));

	if (topIndex.isValid()) {
		Q_EMIT dataChanged(topIndex, topIndex);
	}
}

void AccountListModel::watchPrefsModification(int valueType, const QString &name,
    const QVariant &value)
{
	Q_UNUSED(value);

	switch (valueType) {
	case PrefsDb::VAL_BOOLEAN:
		{
			const AcntId acntId =
			    PrefsSpecific::accntIdFromDataOnDiskKey(name);
			if (acntId.isValid()) {
				watchAcountChanged(acntId);
			}
		}
		break;
	default:
		/* Do nothing. */
		break;
	}
}
