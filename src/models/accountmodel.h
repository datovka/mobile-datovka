/*
 * Copyright (C) 2014-2025 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

#pragma once

#include <QAbstractListModel>
#include <QString>

#include "src/datovka_shared/identifiers/account_id.h"

class AccountDb; /* Forward declaration. */
class AccountLogos; /* Forward declaration. */
class AccountsMap; /* Forward declaration. */
class AcntData; /* Forward declaration. */
class QSettings; /* Forward declaration. */
class Prefs; /* Forward declaration. */

/*!
 * @brief Account hierarchy.
 */
class AccountListModel : public QAbstractListModel {
	Q_OBJECT

public:
	/*!
	 * @brief Roles which this model supports.
	 */
	enum Roles {
		ROLE_ACCOUNT_NAME = Qt::UserRole,
		ROLE_USER_NAME,
		ROLE_TEST_ACCOUNT,
		ROLE_STORE_INTO_DB,
		ROLE_RECEIVED_UNREAD,
		ROLE_RECEIVED_TOTAL,
		ROLE_SENT_UNREAD,
		ROLE_SENT_TOTAL,
		ROLE_LAST_SYNC_TIME,
		ROLE_DATABOX_ID,
		ROLE_DATABOX_LOGO
	};
	Q_ENUM(Roles)

	/*!
	 * @brief Return add account result.
	 */
	enum AddAcntResult {
		AA_SUCCESS = 0, /*!< Operation was successful. */
		AA_EXISTS, /*!< Account already exists in the model. */
		AA_ERR /*!< Internal error. */
	};

	/* Don't forget to declare various properties to the QML system. */
	static
	void declareQML(void);

	/*!
	 * @brief Constructor.
	 *
	 * @param[in] parent Pointer to parent object.
	 */
	explicit AccountListModel(QObject *parent = Q_NULLPTR);

	/*!
	 * @brief Copy constructor.
	 *
	 * @note Needed for QVariant conversion.
	 *
	 * @param[in] other Model to be copied.
	 * @param[in] parent Pointer to parent object.
	 */
	explicit AccountListModel(const AccountListModel &other,
	    QObject *parent = Q_NULLPTR);

	/*!
	 * @brief Return number of rows under the given parent.
	 *
	 * @param[in] parent Parent node index.
	 * @return Number of rows.
	 */
	virtual
	int rowCount(const QModelIndex &parent = QModelIndex()) const
	    Q_DECL_OVERRIDE;

	/*!
	 * @brief Returns the model's role names.
	 *
	 * @return Model's role names.
	 */
	virtual
	QHash<int, QByteArray> roleNames(void) const Q_DECL_OVERRIDE;

	/*!
	 * @brief Return data stored in given location under given role.
	 *
	 * @param[in] index Index specifying the item.
	 * @param[in] role  Data role.
	 * @return Data from model.
	 */
	virtual
	QVariant data(const QModelIndex &index, int role) const Q_DECL_OVERRIDE;

	/*!
	 * @brief Returns item flags for given index.
	 *
	 * @brief[in] index Index specifying the item.
	 * @return Item flags.
	 */
	virtual
	Qt::ItemFlags flags(const QModelIndex &index) const Q_DECL_OVERRIDE;

	/*!
	 * @brief Move rows.
	 *
	 * @param[in] sourceParent Source parent.
	 * @param[in] sourceRow Source row.
	 * @param[in] count Number of rows to be moved.
	 * @param[in] destinationParent Destination parent.
	 * @param[in] destinationChild Row to move data into.
	 * @return If move performed.
	 */
	virtual
	bool moveRows(const QModelIndex &sourceParent, int sourceRow,
	    int count, const QModelIndex &destinationParent,
	    int destinationChild) Q_DECL_OVERRIDE;

	/*!
	 * @brief Remove rows.
	 *
	 * @param[in] row Starting row.
	 * @param[in] count Number of rows to be removed.
	 * @param[in] parent Parent item the row is relative to.
	 * @return True if the rows were successfully removed.
	 */
	virtual
	bool removeRows(int row, int count,
	    const QModelIndex &parent = QModelIndex()) Q_DECL_OVERRIDE;

	/*!
	 * @brief Load content.
	 *
	 * @param[in] accounts Accounts.
	 * @param[in] accountDb Account database.
	 * @param[in] accountLogos Account logos.
	 * @param[in] prefs Prefs.
	 */
	void loadContent(AccountsMap *accounts, AccountDb *accountDb,
	    AccountLogos *accountLogos, Prefs *prefs);

	/*!
	 * @brief Returns account identifier for given node.
	 *
	 * @param[in] index Data index.
	 * @return Account identifier which the node belongs to or a null
	 *     identifier on error.
	 */
	const AcntId &acntId(const QModelIndex &index) const;

	/*!
	 * @brief Returns node related to user name.
	 *
	 * @param[in] acntId Account identifier.
	 * @return Top node index or invalid index if no such name found.
	 */
	QModelIndex acntIndex(const AcntId &acntId) const;

	/*!
	 * @brief Updates counters that are displayed by the model.
	 *
	 * @note Negative values are ignored.
	 *
	 * @param[in] acntId Account identifier.
	 * @param[in] recNew   Received new.
	 * @param[in] recTot   Received total.
	 * @param[in] sntNew   Sent new.
	 * @param[in] sntTot   Sent total.
	 */
	void updateCounters(const AcntId &acntId, int recNew, int recTot,
	    int sntNew, int sntTot);

	/*!
	 * @brief Converts QVariant (obtained from QML) into a pointer.
	 *
	 * @note Some weird stuff happens in QML when passing instances
	 *     directly as constant reference. Wrong constructors are called
	 *     and no data are passed.
	 * @note QML passes objects (which were created in QML) as QVariant
	 *     values holding pointers. You therefore may call invokable methods
	 *     with QVariant arguments from QML.
	 * @note If you use
	 *     qRegisterMetaType<Type *>("Type *") and
	 *     qRegisterMetaType<Type *>("const Type *")
	 *     then QML will be able to call invokable methods without explicit
	 *     conversion from QVariant arguments.
	 *     Q_DECLARE_METATYPE(Type *) is not needed.
	 *
	 * @param[in] variant QVariant holding the pointer.
	 * @return Pointer if it could be acquired, Q_NULLPTR else. This
	 *     function does not allocate a new instance.
	 */
	static
	AccountListModel *fromVariant(const QVariant &modelVariant);

	/*!
	 * @brief Move model item on new position.
	 *
	 * @param Behaves like move() on list of accounts.
	 *
	 * @param[in] source Source index.
	 * @param[in] destination Destination index.
	 */
	Q_INVOKABLE
	void move(int source, int destination);

	/*!
	 * @brief Return user name of first account.
	 * @return User name.
	 */
	Q_INVOKABLE
	QString accountName(void) const;

	/*!
	 * @brief Return name of first account.
	 * @return Account name.
	 */
	Q_INVOKABLE
	QString userName(void) const;

	/*!
	 * @brief Return testing of first account.
	 * @return True if testing account.
	 */
	Q_INVOKABLE
	bool testing(void) const;

private Q_SLOTS:
	/*!
	 * @brief Notify before significant changes in underlying data.
	 */
	void watchContentAboutToBeReset(void);

	/*!
	 * @brief Notify that underlying data have been reset.
	 */
	void watchContentReset(void);

	/*!
	 * @brief Notify before an account is going to be added.
	 */
	void watchAccountAboutToBeAdded(const AcntId &acntId, int row);

	/*!
	 * @brief Update the model after an account has been added.
	 */
	void watchAccountAdded(const AcntId &acntId, int row);

	/*!
	 * @brief Notify before an account is going to be removed.
	 */
	void watchAccountAboutToBeRemoved(const AcntId &acntId, int row);

	/*!
	 * @brief Update the model after an account has been removed.
	 */
	void watchAccountRemoved(const AcntId &acntId, int row);

	/*!
	 * @brief Notify before accounts are going to be moved.
	 */
	void watchAccountsAboutToBeMoved(int srcFrst, int srcLst, int dst);

	/*!
	 * @brief Update the model after accounts have been moved.
	 */
	void watchAccountsMoved(int srcFrst, int srcLst, int dst);

	/*!
	 * @brief This slot handles changes of username.
	 */
	void watchUsernameChanged(const AcntId &oldId, const AcntId &newId, int index);

	/*!
	 * @brief Update the model after account has been modified.
	 *
	 * @note Any non-specific account-data-related modification.
	 */
	void watchAcountChanged(const AcntId &acntId);

	/*!
	 * @brief Watch changes in preferences. Handle configuration signals.
	 *
	 * @param[in] valueType Utilises enum PrefsDb::ValueType values.
	 * @param[in] name Entry name.
	 * @param[in] value Entry value holding the respective type.
	 */
	void watchPrefsModification(int valueType, const QString &name,
	    const QVariant &value);

private:
	AccountsMap *m_accountsPtr; /*!< Pointer to account data container. */
	AccountDb *m_accountDbPtr; /*!< Account database. */
	AccountLogos *m_accountLogos; /*!< Account logos. */
	Prefs *m_prefs; /*! Prefs. */

	/*!
	 * @brief Holds additional information about the displayed data.
	 */
	class AccountCounters {
	public:
		/*!
		 * @brief Constructor.
		 */
		AccountCounters(void)
		    : unreadReceived(0), totalReceived(0),
		    unreadSent(0), totalSent(0)
		{
		}

		bool operator==(const AccountCounters &other) const
		{
			return (unreadReceived == other.unreadReceived)
			    && (totalReceived == other.totalReceived)
			    && (unreadSent == other.unreadSent)
			    && (totalSent == other.totalSent);
		}

		bool operator!=(const AccountCounters &other) const
		{
			return !operator==(other);
		}

		int unreadReceived; /*!< Number of unread received messages. */
		int totalReceived; /*!< Number of received messages. */
		int unreadSent; /*!< Number of unread sent messages. */
		int totalSent; /*!< Number of sent messages. */
	};

	QMap<AcntId, AccountCounters> m_countersMap; /*!< Unread counters. */
};

/* QML passes its arguments via QVariant. */
Q_DECLARE_METATYPE(AccountListModel)
Q_DECLARE_METATYPE(AccountListModel::Roles)
