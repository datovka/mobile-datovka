/*
 * Copyright (C) 2014-2025 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

#include <QDate>
#include <QDateTime>
#include <QQmlEngine> /* qmlRegisterType */
#include <QRegularExpression>
#include <QString>

#include "src/models/prefs_model.h"

void PrefListModel::declareQML(void)
{
	qmlRegisterType<PrefListModel>("cz.nic.mobileDatovka.models", 1, 0, "PrefListModel");
	qRegisterMetaType<PrefListModel>("PrefListModel");
	qRegisterMetaType<PrefListModel::Roles>("PrefListModel::Roles");
	qRegisterMetaType<PrefListModel::ValueType>("PrefListModel::ValueType");
	qRegisterMetaType<PrefListModel::Status>("PrefListModel::Status");

	qRegisterMetaType<PrefListModel *>("PrefListModel *");
	qRegisterMetaType<PrefListModel *>("const PrefListModel *");
}

PrefListModel::PrefListModel(QObject *parent)
    : QAbstractListModel(parent),
    m_prefs(Q_NULLPTR),
    m_prefKeys()
{
}

PrefListModel::PrefListModel(const PrefListModel &other, QObject *parent)
    : QAbstractListModel(parent),
    m_prefs(other.m_prefs),
    m_prefKeys(other.m_prefKeys)
{
	loadContent(m_prefs);
}

int PrefListModel::rowCount(const QModelIndex &parent) const
{
	if (parent.isValid()) {
		return 0;
	} else {
		return m_prefKeys.size();
	}
}

/*!
 * @brief Converts type into viewable string.
 *
 * @param[in] prefs Preferences container.
 * @param[in] name Preference name.
 * @return Type string.
 */
static
const QString &typeString(const Prefs &prefs, const QString &name)
{
	static const QString nullStr;
	static const QString booleanStr("boolean");
	static const QString intStr("integer");
	static const QString floatStr("float");
	static const QString strStr("string");
	static const QString colourStr("colour");
	static const QString dateTimeStr("datetime");
	static const QString dateStr("date");

	enum PrefsDb::ValueType type = PrefsDb::VAL_BOOLEAN;
	if (Q_UNLIKELY(!prefs.type(name, type))) {
		Q_ASSERT(0);
		return nullStr;
	}

	switch (type) {
	case PrefsDb::VAL_BOOLEAN:
		return booleanStr;
		break;
	case PrefsDb::VAL_INTEGER:
		return intStr;
		break;
	case PrefsDb::VAL_FLOAT:
		return floatStr;
		break;
	case PrefsDb::VAL_STRING:
		return strStr;
		break;
	case PrefsDb::VAL_COLOUR:
		return colourStr;
		break;
	case PrefsDb::VAL_DATETIME:
		return dateTimeStr;
		break;
	case PrefsDb::VAL_DATE:
		return dateStr;
		break;
	default:
		Q_ASSERT(0);
		return nullStr;
		break;
	}
}

/*!
 * @brief Converts status data into int.
 *
 * @param[in] prefs Preferences container.
 * @param[in] name Preference name.
 * @return Int containing status.
 */
static
int statusInt(const Prefs &prefs, const QString &name)
{
	enum Prefs::Status status = Prefs::STAT_NO_DEFAULT;
	if (Q_UNLIKELY(!prefs.status(name, status))) {
		/* Status of a known name should be determinable. */
		Q_ASSERT(0);
		return Prefs::STAT_NO_DEFAULT;
	}
	return status;
}

/*!
 * @brief Converts preferences data into viewable string.
 *
 * @param[in] prefs Preferences container.
 * @param[in] name Preference name.
 * @return String containing value data.
 */
static
QString valueString(const Prefs &prefs, const QString &name)
{
	enum PrefsDb::ValueType type = PrefsDb::VAL_BOOLEAN;
	if (Q_UNLIKELY(!prefs.type(name, type))) {
		Q_ASSERT(0);
		return QString();
	}

	switch (type) {
	case PrefsDb::VAL_BOOLEAN:
		{
			bool val = false;
			if (prefs.boolVal(name, val)) {
				return val ? QStringLiteral("true") : QStringLiteral("false");
			} else {
				Q_ASSERT(0);
				return QString();
			}
		}
		break;
	case PrefsDb::VAL_INTEGER:
		{
			qint64 val = 0;
			if (prefs.intVal(name, val)) {
				return QString::number(val);
			} else {
				Q_ASSERT(0);
				return QString();
			}
		}
		break;
	case PrefsDb::VAL_FLOAT:
		{
			double val = 0;
			if (prefs.floatVal(name, val)) {
				return QString::number(val);
			} else {
				Q_ASSERT(0);
				return QString();
			}
		}
		break;
	case PrefsDb::VAL_STRING:
		{
			QString val;
			if (Q_UNLIKELY(!prefs.strVal(name, val))) {
				Q_ASSERT(0);
			}
			return val;
		}
		break;
	case PrefsDb::VAL_COLOUR:
		{
			QString val;
			if (Q_UNLIKELY(!prefs.colourVal(name, val))) {
				Q_ASSERT(0);
			}
			return val;
		}
		break;
	case PrefsDb::VAL_DATETIME:
		{
			QDateTime val;
			if (prefs.dateTimeVal(name, val)) {
				return PrefsDb::dateTimeToString(val);
			} else {
				Q_ASSERT(0);
				return QString();
			}
		}
		break;
	case PrefsDb::VAL_DATE:
		{
			QDate val;
			if (prefs.dateVal(name, val)) {
				return val.toString(Qt::ISODate);
			} else {
				Q_ASSERT(0);
				return QString();
			}
		}
		break;
	default:
		Q_ASSERT(0);
		return QString();
		break;
	}
}

QHash<int, QByteArray> PrefListModel::roleNames(void) const
{
	static QHash<int, QByteArray> roles;
	if (roles.isEmpty()) {
		roles[ROLE_PREF_NAME] = "rPrefName";
		roles[ROLE_PREF_STATUS] = "rPrefStatus";
		roles[ROLE_PREF_TYPE] = "rPrefType";
		roles[ROLE_PREF_VALUE] = "rPrefValue";
	}
	return roles;
}

QVariant PrefListModel::data(const QModelIndex &index, int role) const
{
	const int row = index.row();
	if ((row < 0) || (row >= m_prefKeys.size())) {
		return QVariant();
	}

	switch (role) {
	case ROLE_PREF_NAME:
		return m_prefKeys[row];
		break;
	case ROLE_PREF_STATUS:
		return statusInt(*m_prefs, m_prefKeys[row]);
		break;
	case ROLE_PREF_TYPE:
		return typeString(*m_prefs, m_prefKeys[row]);
		break;
	case ROLE_PREF_VALUE:
		return valueString(*m_prefs, m_prefKeys[row]);
		break;
	default:
		/* Do nothing. */
		break;
	}

	return QVariant();
}

Qt::ItemFlags PrefListModel::flags(const QModelIndex &index) const
{
	Qt::ItemFlags defaultFlags = QAbstractListModel::flags(index);
	return defaultFlags;
}

void PrefListModel::loadContent(Prefs *prefs)
{
	beginResetModel();

	if (m_prefs != Q_NULLPTR) {
		m_prefs->disconnect(SIGNAL(entryCreated(int, QString, QVariant)),
		    this, SLOT(watchPrefsCreation(int, QString, QVariant)));
		m_prefs->disconnect(SIGNAL(entrySet(int, QString, QVariant)),
		    this, SLOT(watchPrefsModification(int, QString, QVariant)));
		m_prefs->disconnect(SIGNAL(entryRemoved(int, QString)),
		    this, SLOT(watchPrefsRemoval(int, QString)));

		m_prefs = Q_NULLPTR;
		m_prefKeys.clear();
	}

	if (prefs != Q_NULLPTR) {
		m_prefs = prefs;
		m_prefKeys = prefs->names(QRegularExpression(".*"));
	}

	if (m_prefs != Q_NULLPTR) {
		connect(m_prefs, SIGNAL(entryCreated(int, QString, QVariant)),
		    this, SLOT(watchPrefsCreation(int, QString, QVariant)));
		connect(m_prefs, SIGNAL(entrySet(int, QString, QVariant)),
		    this, SLOT(watchPrefsModification(int, QString, QVariant)));
		connect(m_prefs, SIGNAL(entryRemoved(int, QString)),
		    this, SLOT(watchPrefsRemoval(int, QString)));
	}

	endResetModel();
}

int PrefListModel::findRow(const QString &name) const
{
	int foundRow = 0;
	while ((foundRow < m_prefKeys.size()) && (m_prefKeys.at(foundRow) != name)) {
		++foundRow;
	}
	if (Q_UNLIKELY(foundRow >= m_prefKeys.size())) {
		return -1;
	}

	return foundRow;
}

int PrefListModel::type(int row) const
{
	if (Q_UNLIKELY((row < 0) || (row >= rowCount()))) {
		Q_ASSERT(0);
		return -1;
	}

	enum PrefsDb::ValueType type = PrefsDb::VAL_BOOLEAN;
	if (Q_UNLIKELY(!m_prefs->type(m_prefKeys[row], type))) {
		Q_ASSERT(0);
		return -1;
	}
	return type;
}

void PrefListModel::alterBool(int row)
{
	if (Q_UNLIKELY((row < 0) || (row >= rowCount()))) {
		Q_ASSERT(0);
		return;
	}
	const QString &name(m_prefKeys[row]);
	enum PrefsDb::ValueType type = PrefsDb::VAL_BOOLEAN;
	if (Q_UNLIKELY(!m_prefs->type(name, type))) {
		Q_ASSERT(0);
		return;
	}
	if (Q_UNLIKELY(type != PrefsDb::VAL_BOOLEAN)) {
		Q_ASSERT(0);
		return;
	}
	bool val= false;
	if (Q_UNLIKELY(!m_prefs->boolVal(name, val))) {
		Q_ASSERT(0);
		return;
	}
	if (Q_UNLIKELY(!m_prefs->setBoolVal(name, !val))) {
		Q_ASSERT(0);
		return;
	}
}

void PrefListModel::modifyInt(int row, qint64 val)
{
	if (Q_UNLIKELY((row < 0) || (row >= rowCount()))) {
		Q_ASSERT(0);
		return;
	}
	const QString &name(m_prefKeys[row]);
	enum PrefsDb::ValueType type = PrefsDb::VAL_BOOLEAN;
	if (Q_UNLIKELY(!m_prefs->type(name, type))) {
		Q_ASSERT(0);
		return;
	}
	if (Q_UNLIKELY(type != PrefsDb::VAL_INTEGER)) {
		Q_ASSERT(0);
		return;
	}
	if (Q_UNLIKELY(!m_prefs->setIntVal(name, val))) {
		Q_ASSERT(0);
		return;
	}
}

void PrefListModel::modifyFloat(int row, double val)
{
	if (Q_UNLIKELY((row < 0) || (row >= rowCount()))) {
		Q_ASSERT(0);
		return;
	}
	const QString &name(m_prefKeys[row]);
	enum PrefsDb::ValueType type = PrefsDb::VAL_BOOLEAN;
	if (Q_UNLIKELY(!m_prefs->type(name, type))) {
		Q_ASSERT(0);
		return;
	}
	if (Q_UNLIKELY(type != PrefsDb::VAL_FLOAT)) {
		Q_ASSERT(0);
		return;
	}
	if (Q_UNLIKELY(!m_prefs->setFloatVal(name, val))) {
		Q_ASSERT(0);
		return;
	}
}

void PrefListModel::modifyString(int row, const QString &val)
{
	if (Q_UNLIKELY((row < 0) || (row >= rowCount()))) {
		Q_ASSERT(0);
		return;
	}
	const QString &name(m_prefKeys[row]);
	enum PrefsDb::ValueType type = PrefsDb::VAL_BOOLEAN;
	if (Q_UNLIKELY(!m_prefs->type(name, type))) {
		Q_ASSERT(0);
		return;
	}
	if (Q_UNLIKELY(type != PrefsDb::VAL_STRING)) {
		Q_ASSERT(0);
		return;
	}
	if (Q_UNLIKELY(!m_prefs->setStrVal(name, val))) {
		Q_ASSERT(0);
		return;
	}
}

void PrefListModel::modifyColour(int row, const QString &val)
{
	if (Q_UNLIKELY((row < 0) || (row >= rowCount()))) {
		Q_ASSERT(0);
		return;
	}
	const QString &name(m_prefKeys[row]);
	enum PrefsDb::ValueType type = PrefsDb::VAL_BOOLEAN;
	if (Q_UNLIKELY(!m_prefs->type(name, type))) {
		Q_ASSERT(0);
		return;
	}
	if (Q_UNLIKELY(type != PrefsDb::VAL_COLOUR)) {
		Q_ASSERT(0);
		return;
	}
	if (Q_UNLIKELY(!m_prefs->setColourVal(name, val))) {
		Q_ASSERT(0);
		return;
	}
}

void PrefListModel::modifyDateTime(int row, const QDateTime &val)
{
	if (Q_UNLIKELY((row < 0) || (row >= rowCount()))) {
		Q_ASSERT(0);
		return;
	}
	const QString &name(m_prefKeys[row]);
	enum PrefsDb::ValueType type = PrefsDb::VAL_BOOLEAN;
	if (Q_UNLIKELY(!m_prefs->type(name, type))) {
		Q_ASSERT(0);
		return;
	}
	if (Q_UNLIKELY(type != PrefsDb::VAL_DATETIME)) {
		Q_ASSERT(0);
		return;
	}
	if (Q_UNLIKELY(!m_prefs->setDateTimeVal(name, val))) {
		Q_ASSERT(0);
		return;
	}
}

void PrefListModel::modifyDate(int row, const QDate &val)
{
	if (Q_UNLIKELY((row < 0) || (row >= rowCount()))) {
		Q_ASSERT(0);
		return;
	}
	const QString &name(m_prefKeys[row]);
	enum PrefsDb::ValueType type = PrefsDb::VAL_BOOLEAN;
	if (Q_UNLIKELY(!m_prefs->type(name, type))) {
		Q_ASSERT(0);
		return;
	}
	if (Q_UNLIKELY(type != PrefsDb::VAL_DATE)) {
		Q_ASSERT(0);
		return;
	}
	if (Q_UNLIKELY(!m_prefs->setDateVal(name, val))) {
		Q_ASSERT(0);
		return;
	}
}

void PrefListModel::modifyValue(int row, const QString &val)
{
	if (Q_UNLIKELY((row < 0) || (row >= rowCount()))) {
		Q_ASSERT(0);
		return;
	}

	const QString &name(m_prefKeys[row]);
	enum PrefsDb::ValueType type = PrefsDb::VAL_BOOLEAN;
	if (Q_UNLIKELY(!m_prefs->type(name, type))) {
		Q_ASSERT(0);
		return;
	}

	switch (type) {
	case PrefsDb::VAL_STRING:
		{
			QString oldVal;
			m_prefs->strVal(name, oldVal);
			if (oldVal != val) {
				modifyString(row, val);
			}
		}
		break;
	case PrefsDb::VAL_COLOUR:
		{
			QString oldVal;
			m_prefs->strVal(name, oldVal);
			if (oldVal != val) {
				modifyColour(row, val);
			}
		}
		break;
	case PrefsDb::VAL_DATE:
		{
			QDate oldVal;
			m_prefs->dateVal(name, oldVal);
			QDate newVal = QDate::fromString(val, Qt::ISODate);
			if (newVal.isValid() && (oldVal != newVal)) {
				modifyDate(row, newVal);
			}
		}
		break;
	case PrefsDb::VAL_DATETIME:
		{
			QDateTime oldVal;
			m_prefs->dateTimeVal(name, oldVal);
			QDateTime newVal = PrefsDb::stringToDateTime(val);
			if (newVal.isValid() && (oldVal != newVal)) {
				modifyDateTime(row, newVal);
			}
		}
		break;
	case PrefsDb::VAL_FLOAT:
		{
			bool ok;
			double oldVal;
			m_prefs->floatVal(name, oldVal);
			double newVal = val.toDouble(&ok);
			if (ok && (oldVal != newVal)) {
				modifyFloat(row, newVal);
			}
		}
		break;
	default:
		break;
	}
}

void PrefListModel::resetEntry(int row)
{
	if (Q_UNLIKELY((row < 0) || (row >= rowCount()))) {
		Q_ASSERT(0);
		return;
	}

	m_prefs->resetVal(m_prefKeys.at(row));
}

QString PrefListModel::statusString(int status)
{
	switch (status) {
	case Prefs::STAT_NO_DEFAULT:
		return QString();
		break;
	case Prefs::STAT_DEFAULT:
		return PrefListModel::tr("default");
		break;
	case Prefs::STAT_MODIFIED:
		return PrefListModel::tr("modified");
		break;
	default:
		//Q_ASSERT(0);
		return QString();
		break;
	}
}

void PrefListModel::watchPrefsCreation(int valueType, const QString &name,
    const QVariant &value)
{
	Q_UNUSED(valueType);
	Q_UNUSED(value);

	int row = findRow(name);
	if (Q_UNLIKELY(row >= 0)) {
		return;
	}

	row = rowCount();

	beginInsertRows(QModelIndex(), row, row);

	m_prefKeys.append(name);

	endInsertRows();
}

void PrefListModel::watchPrefsModification(int valueType, const QString &name,
    const QVariant &value)
{
	Q_UNUSED(valueType);
	Q_UNUSED(value);

	int row = findRow(name);
	if (Q_UNLIKELY(row < 0)) {
		return;
	}

	Q_EMIT dataChanged(index(row), index(row));
}

void PrefListModel::watchPrefsRemoval(int valueType, const QString &name)
{
	Q_UNUSED(valueType);

	int row = findRow(name);
	if (Q_UNLIKELY(row < 0)) {
		return;
	}

	beginRemoveRows(QModelIndex(), row, row);

	m_prefKeys.removeAt(row);

	endRemoveRows();
}
