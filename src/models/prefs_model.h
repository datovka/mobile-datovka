/*
 * Copyright (C) 2014-2025 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

#pragma once

#include <QAbstractListModel>
#include <QStringList>
#include <QVariant>

#include "src/datovka_shared/io/prefs_db.h"
#include "src/datovka_shared/settings/prefs.h"

/*!
 * @brief Custom prefs model class.
 */
class PrefListModel : public QAbstractListModel {
	Q_OBJECT

public:
	/*!
	 * @brief Types of values held in the preferences database.
	 *
	 * @note Used for QML interaction.
	 */
	enum ValueType {
		VAL_BOOLEAN = PrefsDb::VAL_BOOLEAN,
		VAL_INTEGER = PrefsDb::VAL_INTEGER,
		VAL_FLOAT = PrefsDb::VAL_FLOAT,
		VAL_STRING = PrefsDb::VAL_STRING,
		VAL_COLOUR = PrefsDb::VAL_COLOUR,
		VAL_DATETIME = PrefsDb::VAL_DATETIME,
		VAL_DATE = PrefsDb::VAL_DATE
	};
	Q_ENUM(ValueType)

	/*!
	 * @brief Preference value modification status.
	 *
	 * @note Used for QML interaction.
	 */
	enum Status {
		STAT_NO_DEFAULT = Prefs::STAT_NO_DEFAULT,
		STAT_DEFAULT = Prefs::STAT_DEFAULT,
		STAT_MODIFIED = Prefs::STAT_MODIFIED
	};
	Q_ENUM(Status)

	/*!
	 * @brief Roles which this model holds.
	 */
	enum Roles {
		ROLE_PREF_NAME = Qt::UserRole, /*!< Preference name. */
		ROLE_PREF_STATUS, /*!< Modification status. */
		ROLE_PREF_TYPE, /*!< Value type. */
		ROLE_PREF_VALUE /*!< Preference value. */
	};
	Q_ENUM(Roles)

	/* Don't forget to declare various properties to the QML system. */
	static
	void declareQML(void);

	/*!
	 * @brief Constructor.
	 *
	 * @param[in] parent Pointer to parent object.
	 */
	explicit PrefListModel(QObject *parent = Q_NULLPTR);

	/*!
	 * @brief Copy constructor.
	 *
	 * @note Needed for QVariant conversion.
	 *
	 * @param[in] other Model to be copied.
	 * @param[in] parent Pointer to parent object.
	 */
	explicit PrefListModel(const PrefListModel &other,
	    QObject *parent = Q_NULLPTR);

	/*!
	 * @brief Returns number of rows under given parent.
	 *
	 * @param[in] parent Parent index.
	 * @return Number of rows.
	 */
	virtual
	int rowCount(const QModelIndex &parent = QModelIndex()) const
	    Q_DECL_OVERRIDE;

	/*!
	 * @brief Returns the model's role names.
	 *
	 * @return Model's role names.
	 */
	virtual
	QHash<int, QByteArray> roleNames(void) const Q_DECL_OVERRIDE;

	/*!
	 * @brief Returns the data stored under the given role.
	 *
	 * @param[in] index Position.
	 * @param[in] role  Role if the position.
	 * @return Data or invalid QVariant if no matching data found.
	 */
	virtual
	QVariant data(const QModelIndex &index,
	    int role = Qt::DisplayRole) const Q_DECL_OVERRIDE;

	/*!
	 * @brief Used for checkable elements.
	 *
	 * @param[in] index Index which to obtain flags for.
	 */
	virtual
	Qt::ItemFlags flags(const QModelIndex &index) const Q_DECL_OVERRIDE;

	/*!
	 * @brief Load content.
	 *
	 * @param[in] prefs Prefs.
	 */
	void loadContent(Prefs *prefs);

	/*!
	 * @brief Find first row with given \a name.
	 *
	 * @param[in] name Name to search for.
	 * @return Number of given row or -1 if not found or on error.
	 */
	Q_INVOKABLE
	int findRow(const QString &name) const;

	/*!
	 * @brief Get type of entry on row.
	 *
	 * @param[in] row Row number.
	 * @return enum PrefsDb::ValueType or -1 on any error.
	 */
	Q_INVOKABLE
	int type(int row) const;

	/*!
	 * Toggles or modifies values value on row. Must be used with correct type.
	 */
	Q_INVOKABLE
	void alterBool(int row);
	Q_INVOKABLE
	void modifyInt(int row, qint64 val);
	void modifyFloat(int row, double val);
	void modifyString(int row, const QString &val);
	void modifyColour(int row, const QString &val);
	void modifyDateTime(int row, const QDateTime &val);
	void modifyDate(int row, const QDate &val);
	Q_INVOKABLE
	void modifyValue(int row, const QString &val);

	/*!
	 * @brief Reset selected entry on given \a row.
	 *
	 * @param[in] row Row number.
	 */
	Q_INVOKABLE
	void resetEntry(int row);

	/*!
	 * @brief Converts status data into viewable string.
	 *
	 * @param[in] status Status number.
	 * @return String containing status description.
	 */
	Q_INVOKABLE static
	QString statusString(int status);

private slots:
	/*!
	 * @brief Watch when a value is added to preferences.
	 *     Handle configuration signals.
	 *
	 * @param[in] valueType Utilises enum PrefsDb::ValueType values.
	 * @param[in] name Entry name.
	 * @param[in] value Entry value holding the respective type.
	 */
	void watchPrefsCreation(int valueType, const QString &name,
	    const QVariant &value);

	/*!
	 * @brief Watch changes in preferences. Handle configuration signals.
	 *
	 * @param[in] valueType Utilises enum PrefsDb::ValueType values.
	 * @param[in] name Entry name.
	 * @param[in] value Entry value holding the respective type.
	 */
	void watchPrefsModification(int valueType, const QString &name,
	    const QVariant &value);

	/*!
	 * @brief Watch when a value is removed from preferences.
	 *     Handle configuration signals.
	 *
	 * @param[in] valueType Utilises enum PrefsDb::ValueType values.
	 * @param[in] name Entry name.
	 */
	void watchPrefsRemoval(int valueType, const QString &name);

private:
	Prefs *m_prefs; /*!< Model source. */
	QStringList m_prefKeys; /*!< Preferences names. */
};

/* QML passes its arguments via QVariant. */
Q_DECLARE_METATYPE(PrefListModel)
Q_DECLARE_METATYPE(PrefListModel::ValueType)
Q_DECLARE_METATYPE(PrefListModel::Status)
Q_DECLARE_METATYPE(PrefListModel::Roles)
