/*
 * Copyright (C) 2014-2025 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

#pragma once

#include <QList>
#include <QObject>
#include <QString>

#include "src/json/backup.h"
#include "src/qml_identifiers/qml_account_id.h"

class BackupRestoreSelectionModel; /* Forward declaration. */
class QDateTime; /* Forward declaration. */
class QuaZipFile; /* Forward declaration. */

/*
 * @brief Provides interface between QML and backup core.
 * Class is initialised in the main function (main.cpp)
 */
class BackupRestoreZipData : public QObject {
	Q_OBJECT

public:
	enum BackupRestoreType {
		BRT_UNKNOWN = 0,
		BRT_BACKUP,
		BRT_TRANSFER
	};
	Q_ENUM(BackupRestoreType)

	/*!
	 * @brief Declare various properties to the QML system.
	 */
	static
	void declareQML(void);

	/*!
	 * @brief Constructor.
	 *
	 * @param[in] parent Parent object.
	 */
	explicit BackupRestoreZipData(QObject *parent = Q_NULLPTR);

	/*!
	 * @brief Copy constructor.
	 *
	 * @note Needed for QVariant conversion.
	 *
	 * @param[in] other Object to be copied.
	 */
	BackupRestoreZipData(const BackupRestoreZipData &other);

	/*!
	 * @brief Fill backup account model.
	 *
	 * @param[in,out] acntModel Account model.
	 */
	Q_INVOKABLE static
	void fillBackupModel(BackupRestoreSelectionModel *acntModel);

	/*!
	 * @brief Fill restore account model.
	 *
	 * @param[in,out] acntModel Account model.
	 */
	Q_INVOKABLE
	void fillRestoreModel(BackupRestoreSelectionModel *acntModel);

	/*!
	 * @brief Backup one account.
	 *
	 * @param[in] qAcntId Account id.
	 * @param[in] targetPath Target path for backup.
	 * @return True if success.
	 */
	Q_INVOKABLE
	bool backUpAccount(const QmlAcntId *qAcntId, const QString &targetPath);

	/*!
	 * @brief Backup all selected accounts.
	 *
	 * @param[in] acntModel Backup account selection model.
	 * @param[in] targetPath Target path for backup.
	 * @return True if success.
	 */
	Q_INVOKABLE
	bool backUpSelectedAccounts(BackupRestoreSelectionModel *acntModel,
	    const QString &targetPath);

	/*!
	 * @brief Check whether backup of single account can be processed.
	 *
	 * @param[in] qAcntId Account identifier.
	 * @param[in] targetPath Target path for backup.
	 * @param[in] transfer True if backup is transfer.
	 * @return True if it is possible to backup one account.
	 */
	Q_INVOKABLE
	bool canBackUpAccount(const QmlAcntId *qAcntId,
	    const QString &targetPath, bool transfer);

	/*!
	 * @brief Check whether backup of selected accounts can be processed.
	 *
	 * @param[in] acntModel Backup account selection model.
	 * @param[in] targetPath Target path for backup.
	 * @param[in] transfer True if backup is transfer.
	 * @return True if it is possible to backup all accounts and data.
	 */
	Q_INVOKABLE
	bool canBackupSelectedAccounts(BackupRestoreSelectionModel *acntModel,
	    const QString &targetPath, bool transfer);

	/*!
	 * @brief Check restoration of selected accounts can be processed.
	 *
	 * @param[in] acntModel Backup account selection model.
	 * @param[in] includeZfoDb Include zfo database.
	 * @return True if it is possible to restore selected accounts data.
	 */
	Q_INVOKABLE
	bool canRestoreSelectedAccounts(
	    BackupRestoreSelectionModel *acntModel, bool includeZfoDb);

	/*!
	 * @brief Load JSON file content from zip package.
	 *
	 * @param[in] zipPath Path where zip file is located.
	 * @param[in] zipFileName Name of zip file.
	 * @return Return backup type.
	 */
	Q_INVOKABLE
	enum BackupRestoreType loadBackupJsonFromZip(
	    const QString &zipPath, const QString &zipFileName);

	/*!
	 * @brief Restore complete application data from transfer zip.
	 *
	 * @return True if success.
	 */
	Q_INVOKABLE
	bool restoreDataFromTransferZiP(void);

	/*!
	 * @brief Restore selected accounts from backup zip package.
	 *
	 * @param[in] acntModel Backup account selection model.
	 * @param[in] includeZfoDb Include zfo database.
	 * @return True if success.
	 */
	Q_INVOKABLE
	bool restoreSelectedAccountsFromZip(
	    BackupRestoreSelectionModel *acntModel, bool includeZfoDb);

	/*!
	 * @brief Stop worker.
	 *
	 * @param[in] stop True if worker pool has stopped.
	 */
	Q_INVOKABLE
	void stopWorker(bool stop);

	/*!
	 * @brief Backup complete application data for transfer to other device.
	 *
	 * @param[in] targetPath Target path transfer data saving.
	 * @return True if success.
	 */
	Q_INVOKABLE
	bool transferAppDataToZip(const QString &targetPath);

	/*!
	 * @brief Check if PIN is set.
	 *
	 * @return True if success.
	 */
	Q_INVOKABLE
	bool isSetPin(void);

signals:
	/*!
	 * @brief Send backup date time info to QML.
	 *
	 * @param[in] backupDateTime Backup date time text.
	 */
	void bacupDateTextSig(QString backupDateTime);

	/*!
	 * @brief Send restore info to QML.
	 *
	 * @param[in] actionInfo Action info text.
	 */
	void actionTextSig(QString actionInfo);

	/*!
	 * @brief Send restore size info to QML.
	 *
	 * @param[in] sizeInfo Size info text.
	 */
	void sizeTextSig(QString sizeInfo);

	/*!
	 * @brief Set some QML elements visibility.
	 *
	 * @param[in] visible True if element can be seen.
	 */
	void visibleZfoSwitchSig(bool visible);

	/*!
	 * @brief Set some QML elements visibility.
	 *
	 * @param[in] restore True if element can be seen.
	 */
	void canRestoreFromTransferSig(bool restore);

	/*!
	 * @brief Show QML info dialogue.
	 *
	 * @param[in] title Dialogue title.
	 * @param[in] text Dialogue text.
	 * @param[in] detailText Dialogue detail text.
	 */
	void showMessageBox(QString title, QString text, QString detailText);

private:
	/*!
	 * @brief Backup one account.
	 *
	 * @param[in] acntId Account identifier.
	 * @param[in,out] quaZipFile Qua zip file in zip package.
	 * @return Valid entry on success, invalid entry on failure.
	 */
	Json::Backup::MessageDb backUpOneAccount(const AcntId &acntId,
	    QuaZipFile &quaZipFile);

	/*!
	 * @brief Back up accounts.
	 *
	 * @param[in] acntIds Account identifier list.
	 * @param[in] targetPath Target path for backup.
	 * @return True if success.
	 */
	bool backUpAccounts(const QList<AcntId> &acntIds,
	    const QString &targetPath);

	/*!
	 * @brief Check if backup can be processed.
	 *
	 * @param[in] acntIdList Account identifier (can be null).
	 * @param[in] targetPath Target path for backup.
	 * @param[in] transfer True if backup is transfer.
	 * @return True if it is possible to backup all accounts and data.
	 */
	bool canBackUpAccounts(const QList<AcntId> &acntIdList,
	    const QString &targetPath, bool transfer);

	/*!
	 * @brief Send size info to QML.
	 *
	 * @param[in] backupSize Backup size in bytes.
	 * @param[in] targetSize Free space size in bytes in target location.
	 */
	void sendSizeInfoToQml(qint64 backupSize, qint64 targetSize);

	QString m_zipPath; /*!< Path to ZIP file. */
	Json::Backup m_loadedBackup; /*!< Loaded backup content. */
};

/* Declare BackupRestoreType to QML. */
Q_DECLARE_METATYPE(BackupRestoreZipData::BackupRestoreType)
