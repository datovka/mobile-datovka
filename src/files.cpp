/*
 * Copyright (C) 2014-2025 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

#include <QDesktopServices>
#include <QFileDialog>
#include <QFileInfo>
#include <QQmlEngine>
#include <QStringBuilder>

#if defined (Q_OS_ANDROID)
#if (QT_VERSION >= QT_VERSION_CHECK(6, 0, 0))
	#include "android_qt6/src/android_io.h"
#else
	#include "android/src/android_io.h"
#endif
#endif
#include "ios/src/url_opener.h"
#include "src/auxiliaries/email_helper.h"
#include "src/auxiliaries/ios_helper.h"
#include "src/datovka_shared/html/html_export.h"
#include "src/datovka_shared/identifiers/account_id.h"
#include "src/datovka_shared/log/log.h"
#include "src/datovka_shared/utility/pdf_printer.h"
#include "src/files.h"
#include "src/global.h"
#include "src/io/filesystem.h"
#include "src/isds/services/message_interface_offline.h" /* Isds::toMessage() */
#include "src/isds/xml/cms.h"
#include "src/models/filemodel.h"
#include "src/qml_identifiers/qml_account_id.h"
#include "src/settings/account.h"
#include "src/settings/accounts.h"
#include "src/settings/prefs_specific.h"
#include "src/setwrapper.h"
#include "src/sqlite/dbs.h"
#include "src/sqlite/file_db_container.h"
#include "src/sqlite/file_db.h"
#include "src/sqlite/message_db_container.h"
#include "src/sqlite/message_db.h"
#include "src/sqlite/zfo_db.h"

void Files::declareQML(void)
{
	qmlRegisterUncreatableType<Files>("cz.nic.mobileDatovka.files", 1, 0, "FileIdType", "Access to enums & flags only.");
	qmlRegisterUncreatableType<Files>("cz.nic.mobileDatovka.files", 1, 0, "MsgAttachFlag", "Access to enums & flags only.");
	qRegisterMetaType<Files::FileIdType>("Files::FileIdType");
	qRegisterMetaType<Files::MsgAttachFlag>("Files::MsgAttachFlag");
	qRegisterMetaType<Files::MsgAttachFlags>("Files::MsgAttachFlags");
}

Files::Files(QObject *parent)
    : QObject(parent)
{
}

void Files::deleteExpiredFilesFromDbs(int days)
{
	debugFuncCall();

	if (Q_UNLIKELY((GlobInstcs::messageDbsPtr == Q_NULLPTR) ||
	        (GlobInstcs::fileDbsPtr == Q_NULLPTR) ||
	        (GlobInstcs::acntMapPtr == Q_NULLPTR))) {
		Q_ASSERT(0);
		return;
	}

	const QList<AcntId> acntIdList(GlobInstcs::acntMapPtr->keys());
	foreach (const AcntId &acntId, acntIdList) {
		const bool dataOnDisk = PrefsSpecific::dataOnDisk(*GlobInstcs::prefsPtr,
		    GlobInstcs::acntMapPtr->acntData(acntId));

		FileDb *fDb = GlobInstcs::fileDbsPtr->accessFileDb(
		    GlobalSettingsQmlWrapper::dbPath(), acntId.username(),
		    dataOnDisk);
		if (Q_UNLIKELY(fDb == Q_NULLPTR)) {
			logErrorNL(
			    "Cannot access file database for username '%s'.",
			    acntId.username().toUtf8().constData());
			return;
		}
		/*
		 * Files should be deleted from the database with a delay - after
		 * they have been really deleted from the server.
		 */
		const QList<qint64> msgIDList(fDb->cleanFilesInDb(days + 1));
		if (msgIDList.isEmpty()) {
			continue;
		}

		MessageDb *msgDb = GlobInstcs::messageDbsPtr->accessMessageDb(
		    GlobalSettingsQmlWrapper::dbPath(), acntId, dataOnDisk);
		if (Q_UNLIKELY(msgDb == Q_NULLPTR)) {
			logErrorNL(
			    "Cannot access message database for username '%s'.",
			    acntId.username().toUtf8().constData());
			return;
		}
		msgDb->beginTransaction();
		foreach (qint64 msgId, msgIDList) {
			msgDb->setAttachmentDownloaded(msgId, false);
		}
		msgDb->commitTransaction();
	}
}

QString Files::getAttachmentFileIcon(const QString &fileName)
{
	/* List of know file extension sufixes. For these we have png images. */
	static const QSet<QString> knowExp(QSet<QString> () <<
	    "avi" << "bmp" << "doc" << "docx" << "dwg" << "gif" << "html" <<
	    "htm" << "jpeg" << "jpg" << "mpeg" << "mpg" << "mp3" << "ods" <<
	    "odt" << "pdf" << "png" << "ppt" << "pptx" << "rtf" << "tiff" <<
	    "txt" << "wav" << "xls" << "xlsx" << "xml" << "zfo");

	const QString ext(QFileInfo(fileName).suffix().toLower());
	if ((!ext.isEmpty()) && knowExp.contains(ext)) {
		return QStringLiteral("qrc:/fileicons/fileicon_") % ext % QStringLiteral(".png");
	}
	return QStringLiteral("qrc:/fileicons/fileicon_blank.png");
}

qint64 Files::getAttachmentSizeInBytes(const QString &filePath)
{
	QFileInfo fileInfo(filePath);
	qint64 fSize = fileInfo.size();

	if (fSize > 0) {
		return fSize;
	}

	QFile f(filePath);
	if (!f.open(QIODevice::ReadOnly)) {
		return 0;
	}
	QByteArray data = f.readAll();
	f.close();
	return data.size();
}

QString Files::openDirectoryDialog(const QString &caption, const QString &dir)
{
	return QFileDialog::getExistingDirectory(Q_NULLPTR, caption, dir,
	    QFileDialog::ShowDirsOnly);
}

QString Files::openFileDialog(const QString &caption, const QString &dir,
    const QString &filter)
{
	return QFileDialog::getOpenFileName(Q_NULLPTR, caption, dir, filter);
}

QString Files::getRealAttachmentFilePath(const QString &filePath)
{
#if defined (Q_OS_ANDROID)
	return AndroidIO::getRealFilePath(filePath);
#endif
	return QFileInfo(filePath).filePath();
}

QString Files::getRealFileNameFromPath(const QString &filePath)
{
	return QFileInfo(filePath).fileName();
}

QString Files::getPathToFile(const QString &filePath)
{
	return QFileInfo(filePath).path();
}

bool Files::isAndroid11AndLatest(void)
{
#if defined (Q_OS_ANDROID)
	return AndroidIO::isSDKVersion30OrNewest();
#endif
	return false;
}

QByteArray Files::getFileRawContentFromDb(const QmlAcntId *qAcntId, int fileId)
{
	debugFuncCall();

	if (Q_UNLIKELY((GlobInstcs::fileDbsPtr == Q_NULLPTR) ||
	        (GlobInstcs::acntMapPtr == Q_NULLPTR) ||
	        (qAcntId == Q_NULLPTR))) {
		Q_ASSERT(0);
		return QByteArray();
	}

	FileDb *fDb = GlobInstcs::fileDbsPtr->accessFileDb(
	    GlobalSettingsQmlWrapper::dbPath(), qAcntId->username(),
	    PrefsSpecific::dataOnDisk(
	        *GlobInstcs::prefsPtr, GlobInstcs::acntMapPtr->acntData(*qAcntId)));

	if (Q_UNLIKELY(fDb == Q_NULLPTR)) {
		logErrorNL("Cannot access file database for username '%s'.",
		    qAcntId->username().toUtf8().constData());
		return QByteArray();
	}

	return fDb->getFileFromDb(fileId).binaryContent();
}

void Files::openAttachmentFromDb(const QmlAcntId *qAcntId, int fileId)
{
	debugFuncCall();

	if (Q_UNLIKELY((GlobInstcs::fileDbsPtr == Q_NULLPTR) ||
	        (GlobInstcs::acntMapPtr == Q_NULLPTR) ||
	        (qAcntId == Q_NULLPTR))) {
		Q_ASSERT(0);
		return;
	}

	FileDb *fDb = GlobInstcs::fileDbsPtr->accessFileDb(
	    GlobalSettingsQmlWrapper::dbPath(), qAcntId->username(),
	    PrefsSpecific::dataOnDisk(
	        *GlobInstcs::prefsPtr, GlobInstcs::acntMapPtr->acntData(*qAcntId)));

	if (Q_UNLIKELY(fDb == Q_NULLPTR)) {
		logErrorNL("Cannot access file database for username '%s'.",
		    qAcntId->username().toUtf8().constData());
		return;
	}

	Isds::Document document(fDb->getFileFromDb(fileId));

	openAttachment(document.fileDescr(), document.binaryContent());
}

void Files::openAttachment(const QString &fileName,
    const QByteArray &binaryData)
{
	if (Q_UNLIKELY(fileName.isEmpty() || binaryData.isEmpty())) {
		logErrorNL("%s", "File name or its content is empty.");
		Q_ASSERT(0);
		return;
	}

	if (Q_UNLIKELY(isZfoFile(fileName))) {
		/* Don't open zfo files from here. */
		logErrorNL("%s", "The application should open ZFO files by itself.");
		Q_ASSERT(0);
		return;
	}

	QString docLocationRoot = appTmpDirPath();
	QString tmpFileName = fileName;

#if defined (Q_OS_ANDROID)
	docLocationRoot = getAndroidFileProviderBasePath();
	/*
	 * Android has problems identifying the types of files with upper case
	 * suffixes. Therefore suffixes are converted to lower case.
	 */
	tmpFileName = QFileInfo(fileName).completeBaseName() + "."
	    + QFileInfo(fileName).suffix().toLower();
#endif

	QString filePath(writeFile(docLocationRoot, tmpFileName, binaryData));

	if (!filePath.isEmpty()) {
		logInfoNL("Creating temporary file '%s'.",
		    filePath.toUtf8().constData());
		openAttachmentFromPath(filePath);
	} else {
		logErrorNL("Cannot create temporary file for '%s'.",
		    tmpFileName.toUtf8().constData());
		emit showMessageBox(tr("Open attachment error"),
		    tr("Cannot save selected file to disk for opening."),
		    QString());
	}
}

void Files::openAttachmentFromPath(const QString &filePath)
{
	if (Q_UNLIKELY(filePath.isEmpty())) {
		logErrorNL("%s", "File path is empty.");
		Q_ASSERT(0);
		return;
	}

	if (Q_UNLIKELY(isZfoFile(filePath))) {
		/* Don't open zfo files from here. */
		logErrorNL("%s", "The application should open ZFO files by itself.");
		Q_ASSERT(0);
		return;
	}

#ifdef Q_OS_IOS

	UrlOpener::openFile(filePath);

#elif defined (Q_OS_ANDROID) and (QT_VERSION < QT_VERSION_CHECK(6, 0, 0))

	if (!AndroidIO::openFile(filePath)) {
		logErrorNL("%s", "Open attachment error. There is no application to open this file format.");
		emit showMessageBox(tr("Open attachment error"),
		    tr("There is no application to open this file format."),
		    tr("File: '%1'").arg(filePath));
	}

#else

	if (!QDesktopServices::openUrl(QUrl::fromLocalFile(filePath))) {
		logErrorNL("%s", "Open attachment error. There is no application to open this file format.");
		emit showMessageBox(tr("Open attachment error"),
		    tr("There is no application to open this file format."),
		    tr("File: '%1'").arg(filePath));
	}

#endif
}

void Files::sendMsgFilesWithEmail(const QmlAcntId *qAcntId, qint64 msgId,
    enum Messages::MessageType msgType, MsgAttachFlags attachFlags)
{
	debugFuncCall();

	if (Q_UNLIKELY((GlobInstcs::messageDbsPtr == Q_NULLPTR) ||
	        (GlobInstcs::fileDbsPtr == Q_NULLPTR) ||
	        (GlobInstcs::zfoDbPtr == Q_NULLPTR) ||
	        (GlobInstcs::acntMapPtr == Q_NULLPTR) ||
	        (qAcntId == Q_NULLPTR))) {
		Q_ASSERT(0);
		return;
	}

	if (!qAcntId->isValid() || (msgId <= 0)) {
		return;
	}

	QString body;
	QString subject;

	const bool dataOnDisk = PrefsSpecific::dataOnDisk(*GlobInstcs::prefsPtr,
	    GlobInstcs::acntMapPtr->acntData(*qAcntId));

	/* Fill email subject and email body */
	MessageDb *msgDb = GlobInstcs::messageDbsPtr->accessMessageDb(
	    GlobalSettingsQmlWrapper::dbPath(), *qAcntId, dataOnDisk);
	if (Q_UNLIKELY(msgDb == Q_NULLPTR)) {
		logErrorNL("Cannot access message database for '%s'.",
		    qAcntId->username().toUtf8().constData());
		return;
	}
	if (!msgDb->getMessageEmailDataFromDb(msgId, body, subject)) {
		logErrorNL("Missing data of message '%s'.",
		    QString::number(msgId).toUtf8().constData());
		return;
	}

	QList<Isds::Document> documents;

	/* Get attachment files from database if needed */
	if (attachFlags & MSG_ATTACHS) {
		FileDb *fDb = GlobInstcs::fileDbsPtr->accessFileDb(
		    GlobalSettingsQmlWrapper::dbPath(), qAcntId->username(),
		    dataOnDisk);
		if (Q_UNLIKELY(fDb == Q_NULLPTR)) {
			logErrorNL("Cannot access file database for '%s'.",
			    qAcntId->username().toUtf8().constData());
			return;
		}

		documents = fDb->getFilesFromDb(msgId);
		if (documents.isEmpty()) {
			logErrorNL("Missing attachments for message '%s'.",
			    QString::number(msgId).toUtf8().constData());
			return;
		}
	}

	/* Get zfo file from database if needed */
	if (attachFlags & MSG_ZFO) {
		Isds::Document document;
		document.setBase64Content(GlobInstcs::zfoDbPtr->getZfoContentFromDb(
		    msgId, qAcntId->testing()));
		if (Q_UNLIKELY(document.binaryContent().isEmpty())) {
			logErrorNL("Missing zfo data for message '%s'.",
			    QString::number(msgId).toUtf8().constData());
			emit showMessageBox(tr("ZFO missing"),
			    tr("ZFO message is not present in local database."),
			    tr("Download complete message again to obtain it."));
			Q_ASSERT(0);
			return;
		}
		document.setFileDescr(getMsgZfoName(msgId, msgType));
		documents.append(document);
	}

	/* Create email content, email attachment path, email eml content */
	removeDirFromDocLoc(DATOVKA_MAIL_DIR_NAME);
	QString targetPath(appEmailDirPath(QString::number(msgId)));
	const QString boundary = generateBoundaryString();
	QString emailMessage = createEmailMessage(QString(), body, subject,
	    boundary);
	QStringList filePathList;

#if defined (Q_OS_ANDROID)
	targetPath = getAndroidFileProviderBasePath();
#endif

	/* Write attachment files to email directory */
	foreach (const Isds::Document &document, documents) {
		QString fileName = document.fileDescr();
		if (fileName.isEmpty()) {
			logErrorNL("%s", "File name is empty.");
			return;
		}
		fileName = writeFile(targetPath, fileName,
		    document.binaryContent());
		filePathList.append(fileName);
		addAttachmentToEmailMessage(emailMessage, document.fileDescr(),
		    document.base64Content().toUtf8(), boundary);
	}

	finishEmailMessage(emailMessage, boundary);

	/* Send email */
	sendEmail(emailMessage, filePathList, QString(), subject, body,
	    QString::number(msgId));
}

void Files::sendAttachmentsWithEmail(const FileListModel *attachModel,
    const QmlAcntId *qAcntId, const QString &msgIdStr,
    enum Messages::MessageType msgType, MsgAttachFlags attachFlags)
{
	debugFuncCall();

	if (Q_UNLIKELY((GlobInstcs::messageDbsPtr == Q_NULLPTR) ||
	        (GlobInstcs::acntMapPtr == Q_NULLPTR) || msgIdStr.isEmpty() ||
	        (attachModel == Q_NULLPTR) || (qAcntId == Q_NULLPTR))) {
		Q_ASSERT(0);
		return;
	}

	QString body;
	QString subject;

	const bool dataOnDisk = PrefsSpecific::dataOnDisk(*GlobInstcs::prefsPtr,
	    GlobInstcs::acntMapPtr->acntData(*qAcntId));

	/* Fill email subject and email body */
	MessageDb *msgDb = GlobInstcs::messageDbsPtr->accessMessageDb(
	    GlobalSettingsQmlWrapper::dbPath(), *qAcntId, dataOnDisk);
	if (Q_UNLIKELY(msgDb == Q_NULLPTR)) {
		logErrorNL("Cannot access message database for '%s'.",
		    qAcntId->username().toUtf8().constData());
		return;
	}
	if (!msgDb->getMessageEmailDataFromDb(msgIdStr.toLongLong(),
	    body, subject)) {
		logErrorNL("Missing data of message '%s'.",
		    msgIdStr.toUtf8().constData());
		return;
	}

	sendAttachmentEmailZfo(attachModel, qAcntId, msgIdStr, msgType,
	    subject, body, attachFlags);
}

bool Files::fileReadable(const QString &filePath)
{
	if (Q_UNLIKELY(filePath.isEmpty())) {
		//logErrorNL("%s", "Target ZFO path is empty.");
		Q_ASSERT(0);
		return false;
	}

	{
		QFileInfo fileInfo(filePath);
		if (!fileInfo.isFile() || !fileInfo.isReadable()) {
			//logErrorNL("Cannot open ZFO file '%s'.",
			//    filePath.toUtf8().constData());
			return false;
		}
	}

	return true;
}

bool Files::isZfoFile(const QString &fileName)
{
	return QFileInfo(fileName).suffix().toLower() == QStringLiteral("zfo");
}

QByteArray Files::rawFileContent(const QString &filePath)
{
	if (!fileReadable(filePath)) {
		return QByteArray();
	}

	QFile file(filePath);
	if (Q_UNLIKELY(!file.open(QIODevice::ReadOnly))) {
		logErrorNL("Cannot open file '%s'.",
		    filePath.toUtf8().constData());
		Q_ASSERT(0);
		return QByteArray();
	}

	QByteArray rawData(file.readAll());
	file.close();
	return rawData;
}

MsgInfo *Files::zfoData(FileListModel *attachModel,
    const QByteArray &rawZfoData)
{
	enum MsgInfo::ZfoType type = MsgInfo::TYPE_UNKNOWN;
	QString idStr, annot, htmlDescr, emailBody;

	bool ret = parseXmlData(&type, &idStr, &annot, &htmlDescr,
	    attachModel, &emailBody, Isds::Xml::cmsContent(rawZfoData));

	return ret ?
	    new (::std::nothrow) MsgInfo(type, idStr, annot, htmlDescr,
	        emailBody) :
	    new (::std::nothrow) MsgInfo();
}

bool Files::setAttachmentModel(FileListModel &attachModel,
    const QmlAcntId *qAcntId, qint64 msgId)
{
	debugFuncCall();

	if (Q_UNLIKELY((GlobInstcs::fileDbsPtr == Q_NULLPTR) ||
	        (GlobInstcs::acntMapPtr == Q_NULLPTR) ||
	        (qAcntId == Q_NULLPTR))) {
		Q_ASSERT(0);
		return false;
	}

	FileDb *fDb = GlobInstcs::fileDbsPtr->accessFileDb(
	    GlobalSettingsQmlWrapper::dbPath(), qAcntId->username(),
	    PrefsSpecific::dataOnDisk(
	        *GlobInstcs::prefsPtr, GlobInstcs::acntMapPtr->acntData(*qAcntId)));

	if (Q_UNLIKELY(fDb == Q_NULLPTR)) {
		logErrorNL("%s", "Cannot access file database.");
		return false;
	}
	attachModel.clearAll();
	fDb->setFileModelFromDb(attachModel, msgId);
	return true;
}

void Files::sendAttachmentEmailZfo(const FileListModel *attachModel,
    const QmlAcntId *qAcntId, const QString &msgIdStr,
    enum Messages::MessageType msgType, QString subject,
    QString body, MsgAttachFlags attachFlags)
{
	debugFuncCall();

	/* Obtain pointer to attachment model. */
	if (Q_UNLIKELY(attachModel == Q_NULLPTR)) {
		Q_ASSERT(0);
		return;
	}

	/* Obtain message identifier. */
	bool ok = false;
	qint64 msgId = msgIdStr.toLongLong(&ok);
	if (!ok || (msgId < 0)) {
		return;
	}

	QStringList fileList;

	const QString boundary = generateBoundaryString();
	QString emailMessage = createEmailMessage(QString(), body, subject,
	    boundary);

	removeDirFromDocLoc(DATOVKA_MAIL_DIR_NAME);
	QString targetPath(appEmailDirPath(msgIdStr));

#if defined (Q_OS_ANDROID)
	targetPath = getAndroidFileProviderBasePath();
#endif

	if (attachFlags & MSG_ATTACHS) {
		for (int row = 0; row < attachModel->rowCount(); ++row) {
			QModelIndex idx(attachModel->index(row));
			/*
			 * On Android the attachment must be saved and then
			 * explicitly added into the email message.
			 */
			QByteArray binaryData(attachModel->data(idx,
			    FileListModel::ROLE_BINARY_DATA).toByteArray());
			QString attachName(attachModel->data(idx,
			    FileListModel::ROLE_FILE_NAME).toString());
			QString filePath(writeFile(targetPath, attachName,
			    binaryData));
			fileList.append(filePath);
			addAttachmentToEmailMessage(emailMessage, attachName,
			    binaryData.toBase64(), boundary);
		}
	} else if (attachFlags & MSG_ZFO) {
		QString attachName(getMsgZfoName(msgId, msgType));
		QString srcFilePath = joinDirs(getZfoPath(qAcntId->username(),
		    qAcntId->testing(), msgId), attachName);
		QByteArray binaryData(rawFileContent(srcFilePath));
		QString filePath(writeFile(targetPath, attachName, binaryData));
		fileList.append(filePath);
		addAttachmentToEmailMessage(emailMessage, attachName,
		    binaryData.toBase64(), boundary);
	} else {
		return;
	}

	finishEmailMessage(emailMessage, boundary);

	sendEmail(emailMessage, fileList, QString(), subject, body,
	    QString::number(msgId));
}

#ifdef Q_OS_IOS
static
void exportFilesToCloud(const QStringList &files, const QString &iCloudPath,
    QString &title, QString &text, QString &detailText)
{
	QList<IosHelper::FileICloud> iCloudFiles;
	for (const QString &file : files) {
		IosHelper::FileICloud iCloudFile;
		iCloudFile.srcFilePath = file;
		iCloudFile.destiCloudPath = iCloudPath;
		iCloudFiles.append(iCloudFile);
	}
	IosHelper::storeFilesToCloud(iCloudFiles, title, text, detailText);
}
#endif

#ifdef Q_OS_IOS
static
void exportFilesToDeviceStorage(const QStringList &files)
{
	IosHelper::storeFilesToDeviceStorage(files);
}
#endif

static
QString getTargetLocation(const QString &msgIdStr)
{
#ifdef Q_OS_IOS
	return appMsgAttachDirPathiOS(msgIdStr);
#else
	return appMsgAttachDirPath(msgIdStr);
#endif
}

void Files::exportFiles(const QString &iCloudPath, const QString &destFilePath,
    const QStringList &files, bool storeOnCloud)
{
	QString title, text, detailText;

#ifndef Q_OS_IOS

	Q_UNUSED(iCloudPath)
	Q_UNUSED(files)
	Q_UNUSED(storeOnCloud)

	const QString filePath = QFileInfo(destFilePath).absolutePath();
	if (!filePath.isEmpty()) {
		title = tr("Saving Successful");
		text = tr("Files have been saved.");
		detailText = tr("Path: '%1'").arg(filePath);
	} else {
		title = tr("Saving Failed");
		text = tr("Files have not been saved.");
		detailText = tr("Please check whether the application has permissions to access the storage.");
	}

#else

	if (storeOnCloud) {
		exportFilesToCloud(files, iCloudPath, title, text, detailText);
	} else {
		exportFilesToDeviceStorage(files);
		return;
	}

#endif

	emit showMessageBox(title, text, detailText);
}

void Files::saveMsgFilesToDisk(const QmlAcntId *qAcntId,
    const QString &msgIdStr, enum Messages::MessageType msgType,
    MsgAttachFlags attachFlags, bool storeOnCloud)
{
	debugFuncCall();

	if (Q_UNLIKELY((GlobInstcs::fileDbsPtr == Q_NULLPTR) ||
	        (GlobInstcs::zfoDbPtr == Q_NULLPTR) ||
	        (GlobInstcs::acntMapPtr == Q_NULLPTR) ||
	        (qAcntId == Q_NULLPTR))) {
		Q_ASSERT(0);
		return;
	}

	/* User name must be supplied. */
	if (Q_UNLIKELY(!qAcntId->isValid())) {
		Q_ASSERT(0);
		return;
	}

	/* Obtain message identifier. */
	bool ok = false;
	qint64 msgId = msgIdStr.toLongLong(&ok);
	if (!ok || (msgId < 0)) {
		return;
	}

	QList<Isds::Document> documents;

	/* Get attachment files from database if needed */
	if (attachFlags & MSG_ATTACHS) {
		FileDb *fDb = GlobInstcs::fileDbsPtr->accessFileDb(
		    GlobalSettingsQmlWrapper::dbPath(), qAcntId->username(),
		    PrefsSpecific::dataOnDisk(
		        *GlobInstcs::prefsPtr, GlobInstcs::acntMapPtr->acntData(*qAcntId)));
		if (Q_UNLIKELY(fDb == Q_NULLPTR)) {
			logErrorNL("Cannot access file database for '%s'.",
			    qAcntId->username().toUtf8().constData());
			return;
		}

		documents = fDb->getFilesFromDb(msgId);
		if (documents.isEmpty()) {
			logErrorNL("Missing attachments for message '%s'.",
			    QString::number(msgId).toUtf8().constData());
			return;
		}
	}

	/* Get zfo file from database if needed */
	if (attachFlags & MSG_ZFO) {
		Isds::Document document;
		document.setBase64Content(GlobInstcs::zfoDbPtr->getZfoContentFromDb(
		    msgId, qAcntId->testing()));
		if (Q_UNLIKELY(document.binaryContent().isEmpty())) {
			logErrorNL("Missing zfo data for message '%s'.",
			    QString::number(msgId).toUtf8().constData());
			emit showMessageBox(tr("ZFO missing"),
			    tr("ZFO message is not present in local database."),
			    tr("Download complete message again to obtain it."));
			Q_ASSERT(0);
			return;
		}
		document.setFileDescr(getMsgZfoName(msgId, msgType));
		documents.append(document);
	}

	QString targetPath(getTargetLocation(msgIdStr));
	QString destFilePath;
	QStringList files;

	foreach (const Isds::Document &document, documents) {
		destFilePath = writeFile(targetPath, document.fileDescr(),
		    document.binaryContent());
		if (!destFilePath.isEmpty()) {
			files.append(destFilePath);
		}
	}

	exportFiles(joinDirs(qAcntId->username(), msgIdStr), destFilePath,
	    files, storeOnCloud);
}

void Files::saveAttachmentsToDiskZfo(const FileListModel *attachModel,
    const QmlAcntId *qAcntId, const QString &msgIdStr, bool storeOnCloud)
{
	debugFuncCall();

	if (Q_UNLIKELY(attachModel == Q_NULLPTR)) {
		Q_ASSERT(0);
		return;
	}

	QString username = "extern_file";

	if (qAcntId != Q_NULLPTR) {
		username = qAcntId->username();
	}

	QString targetPath(getTargetLocation(msgIdStr));
	QString destFilePath;
	QStringList files;

	for (int row = 0; row < attachModel->rowCount(); ++row) {
		QModelIndex idx(attachModel->index(row));
		destFilePath = writeFile(targetPath, attachModel->data(idx,
		    FileListModel::ROLE_FILE_NAME).toString(),
		    attachModel->data(idx,
		        FileListModel::ROLE_BINARY_DATA).toByteArray());
		if (!destFilePath.isEmpty()) {
			files.append(destFilePath);
		}
	}

	exportFiles(joinDirs(username, msgIdStr), destFilePath, files,
	    storeOnCloud);
}

void Files::saveZfoFilesToDisk(const QmlAcntId *qAcntId,
    const QString &msgIdStr, enum Messages::MessageType msgType,
    bool storeOnCloud)
{
	debugFuncCall();

	if (Q_UNLIKELY(qAcntId == Q_NULLPTR)) {
		Q_ASSERT(0);
		return;
	}

	QString targetPath(getTargetLocation(msgIdStr));
	QStringList files;
	qint64 dmID = msgIdStr.toLongLong();
	QString srcFilePath = joinDirs(getZfoPath(qAcntId->username(),
	    qAcntId->testing(), dmID), getMsgZfoName(dmID, msgType));
	QString destFilePath = joinDirs(targetPath,
	    getMsgZfoName(dmID, msgType));
	QFile::copy(srcFilePath, destFilePath);

	if (!destFilePath.isEmpty()) {
		files.append(destFilePath);
	}

	srcFilePath = joinDirs(getZfoPath(qAcntId->username(),
	    qAcntId->testing(), dmID), getDelInfoZfoName(dmID));
	destFilePath = joinDirs(targetPath, getDelInfoZfoName(dmID));
	QFile::copy(srcFilePath, destFilePath);

	if (!destFilePath.isEmpty()) {
		files.append(destFilePath);
	}

	exportFiles(joinDirs(qAcntId->username(), msgIdStr), destFilePath,
	    files, storeOnCloud);
}

#define PDF_FONT_FILE_NAME ":/font/NotoSerif-Regular.ttf"

void Files::saveMsgEnvelopePdfToDisk(const QmlAcntId *qAcntId,
    const QString &msgIdStr, enum Messages::MessageType msgType,
    bool storeOnCloud)
{
	debugFuncCall();

	if (Q_UNLIKELY((qAcntId == Q_NULLPTR) ||
	        (GlobInstcs::messageDbsPtr == Q_NULLPTR) ||
	        (GlobInstcs::acntMapPtr == Q_NULLPTR))) {
		Q_ASSERT(0);
		return;
	}

	const bool dataOnDisk = PrefsSpecific::dataOnDisk(*GlobInstcs::prefsPtr,
	    GlobInstcs::acntMapPtr->acntData(*qAcntId));

	MessageDb *msgDb = GlobInstcs::messageDbsPtr->accessMessageDb(
		GlobalSettingsQmlWrapper::dbPath(), *qAcntId, dataOnDisk);
	if (Q_UNLIKELY(msgDb == Q_NULLPTR)) {
		logErrorNL(
		    "Cannot access message database for username '%s'.",
		    qAcntId->username().toUtf8().constData());
		return;
	}

	FileDb *fileDb = GlobInstcs::fileDbsPtr->accessFileDb(
	    GlobalSettingsQmlWrapper::dbPath(), qAcntId->username(),
	    dataOnDisk);
	if (Q_UNLIKELY(msgDb == Q_NULLPTR)) {
		logErrorNL(
			"Cannot access attachment database for username '%s'.",
			qAcntId->username().toUtf8().constData());
		return;
	}

	QString targetPath(getTargetLocation(msgIdStr));
	QStringList files;
	qint64 dmID = msgIdStr.toLongLong();
	QString destFilePath = joinDirs(targetPath,
	    getMsgEnvelopePdfName(dmID, msgType));

	if (Q_UNLIKELY(!Utility::printPDFWithFont(destFilePath,
	        Html::Export::htmlInfoPdf(Html::Export::TYPE_ENVELOPE_INFO,
	            msgDb->getMessageEnvelopeFromDb(dmID),
	            fileDb->getFilesFromDb(dmID),
	            msgDb->messageAuthorJsonStr(dmID)),
	        Utility::HTML_TEXT, PDF_FONT_FILE_NAME))) {
		logErrorNL(
		    "Cannot print delivery info PDF for username '%s'.",
		    qAcntId->username().toUtf8().constData());
		return;
	}

	if (!destFilePath.isEmpty()) {
		files.append(destFilePath);
	}

	exportFiles(joinDirs(qAcntId->username(), msgIdStr), destFilePath,
	    files, storeOnCloud);
}

void Files::saveDeliveryInfoPdfToDisk(const QmlAcntId *qAcntId,
    const QString &msgIdStr, bool storeOnCloud)
{
	debugFuncCall();

	if (Q_UNLIKELY((qAcntId == Q_NULLPTR) ||
	        (GlobInstcs::messageDbsPtr == Q_NULLPTR) ||
	        (GlobInstcs::acntMapPtr == Q_NULLPTR))) {
		Q_ASSERT(0);
		return;
	}

	const bool dataOnDisk = PrefsSpecific::dataOnDisk(*GlobInstcs::prefsPtr,
	    GlobInstcs::acntMapPtr->acntData(*qAcntId));

	MessageDb *msgDb = GlobInstcs::messageDbsPtr->accessMessageDb(
		GlobalSettingsQmlWrapper::dbPath(), *qAcntId, dataOnDisk);
	if (Q_UNLIKELY(msgDb == Q_NULLPTR)) {
		logErrorNL(
		    "Cannot create pdf delivery info for username '%s'.",
		    qAcntId->username().toUtf8().constData());
		return;
	}

	qint64 dmID = msgIdStr.toLongLong();
	QString targetPath(getTargetLocation(msgIdStr));
	QString destFilePath = joinDirs(targetPath, getDelInfoPdfName(dmID));
	QStringList files;

	if (Q_UNLIKELY(!Utility::printPDFWithFont(destFilePath,
	        Html::Export::htmlInfoPdf(Html::Export::TYPE_DELIVERY_INFO,
	            msgDb->getMessageEnvelopeFromDb(dmID),
	            QList<Isds::Document>(),
	            msgDb->messageAuthorJsonStr(dmID)),
	        Utility::HTML_TEXT, PDF_FONT_FILE_NAME))) {
		logErrorNL(
		    "Cannot print delivery info PDF for username '%s'.",
		    qAcntId->username().toUtf8().constData());
		return;
	}

	if (!destFilePath.isEmpty()) {
		files.append(destFilePath);
	}

	exportFiles(joinDirs(qAcntId->username(), msgIdStr), destFilePath,
	    files, storeOnCloud);
}

void Files::deleteTmpFileFromStorage(const QString &filePath)
{
#if defined Q_OS_IOS
	QFile file(filePath);
	file.remove();
#else
	Q_UNUSED(filePath);
#endif
}

bool Files::parseXmlData(enum MsgInfo::ZfoType *type, QString *idStr,
    QString *annotation, QString *msgDescrHtml, FileListModel *attachModel,
    QString *emailBody, QByteArray xmlData)
{
	debugFuncCall();

	if (xmlData.isEmpty()) {
		logErrorNL("%s", "XML content is empty.");
		return false;
	}

	/* Test if zfo is message, delivery info or unknown format */
	if (xmlData.contains(QByteArray("MessageDownloadResponse"))) {
		if (type != Q_NULLPTR) {
			*type = MsgInfo::TYPE_MESSAGE;
		}
		return parseAndShowXmlData(MsgInfo::TYPE_MESSAGE, idStr,
		    annotation, msgDescrHtml, attachModel, emailBody, xmlData);
	} else if (xmlData.contains(QByteArray("GetDeliveryInfoResponse"))) {
		if (type != Q_NULLPTR) {
			*type = MsgInfo::TYPE_DELIVERY_INFO;
		}
		return parseAndShowXmlData(MsgInfo::TYPE_DELIVERY_INFO, idStr,
		    annotation, msgDescrHtml, attachModel, emailBody, xmlData);
	} else {
		if (type != Q_NULLPTR) {
			*type = MsgInfo::TYPE_UNKNOWN;
		}
		logErrorNL("%s", "Unknown ZFO format.");
	}

	return false;
}

bool Files::parseAndShowXmlData(enum MsgInfo::ZfoType type, QString *idStr,
    QString *annotation, QString *msgDescrHtml, FileListModel *attachModel,
    QString *emailBody, QByteArray &xmlData)
{
	debugFuncCall();

	if (Q_UNLIKELY(type == MsgInfo::TYPE_UNKNOWN)) {
		Q_ASSERT(0);
		return false;
	}

	QString html;
	const Isds::Message message = Isds::toMessage(xmlData);

	if (type == MsgInfo::TYPE_MESSAGE) {
		html = Html::Export::htmlMessageInfoApp(message.envelope(),
		    QByteArray(), QStringList(), true, false, false);
	} else {
		html = Html::Export::htmlDeliveryInfoApp(message.envelope(),
		    QByteArray());
	}

	// Create body for email
	QString body = generateEmailBodyText(message.envelope().dmId(),
	    message.envelope().dmSender(), message.envelope().dmRecipient(),
	    dateTimeStrFromDbFormat(
	    dateTimeToDbFormatStr(message.envelope().dmAcceptanceTime()),
	    DATETIME_QML_FORMAT));

	if (idStr != Q_NULLPTR) {
		*idStr = QString::number(message.envelope().dmId());
	}
	if (annotation != Q_NULLPTR) {
		*annotation = message.envelope().dmAnnotation();
	}
	if (msgDescrHtml != Q_NULLPTR) {
		*msgDescrHtml = html;
	}
	if (attachModel != Q_NULLPTR) {
		attachModel->clearAll();
		foreach (const Isds::Document &document, message.documents()) {
			attachModel->appendFileEntry(
			    FileListModel::Entry(-1, document.fileDescr(),
			    document.binaryContent(), document.binaryContent().size(),
			    QString()));
		}
	}
	if (emailBody != Q_NULLPTR) {
		*emailBody = body;
	}

	return true;
}

QString Files::appendPDF(const QString &text)
{
	debugSlotCall();

	QString fileName = joinDirs(appShortMsgPdfDirPath(), DATOVKA_TEXT_MSG_FILE_NAME);

	if (Utility::printPDF(fileName, text)) {
		return fileName;
	}
	return QString();
}

void Files::viewPDF(const QString &text)
{
	debugSlotCall();

	QString docLocationRoot = appTmpDirPath();

#if defined (Q_OS_ANDROID)
	docLocationRoot = getAndroidFileProviderBasePath();
#endif

	QString fileName = joinDirs(docLocationRoot, DATOVKA_TEXT_MSG_FILE_NAME);

	if (Utility::printPDF(fileName, text)) {
		openAttachmentFromPath(fileName);
	}
}
