/*
 * Copyright (C) 2014-2025 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

#pragma once

#include <QByteArray>
#if (QT_VERSION >= QT_VERSION_CHECK(5, 12, 0))
#  include <memory> /* ::std::unique_ptr */
#else /* < Qt-5.12 */
#  include <QScopedPointer>
#endif /* >= Qt-5.12 */

#include "src/datovka_shared/json/object.h"

class QJsonValue; /* Forward declaration. */
class QString; /* Forward declaration. */

namespace RecMgmt {

	class ServiceInfoRespPrivate;
	/*!
	 * @brief Encapsulates the service_info response.
	 */
	class ServiceInfoResp : public Json::Object {
		Q_DECLARE_PRIVATE(ServiceInfoResp)
	public:
		/*!
		 * @brief Constructor. Creates an invalid structure.
		 */
		ServiceInfoResp(void);

		/*!
		 * @brief Copy constructor.
		 *
		 * @param[in] other Service info response.
		 */
		ServiceInfoResp(const ServiceInfoResp &other);
#ifdef Q_COMPILER_RVALUE_REFS
		ServiceInfoResp(ServiceInfoResp &&other) Q_DECL_NOEXCEPT;
#endif /* Q_COMPILER_RVALUE_REFS */

		~ServiceInfoResp(void);

		/*!
		 * @brief Constructor.
		 *
		 * @param[in] logoSvg SVG logo stored as raw data.
		 * @param[in] name Service provider name.
		 * @param[in] tokenMame Security token name.
		 */
		ServiceInfoResp(const QByteArray &logoSvg, const QString &name,
		    const QString &tokenName);
#ifdef Q_COMPILER_RVALUE_REFS
		ServiceInfoResp(QByteArray &&logoSvg, QString &&name,
		    QString &&tokenName);
#endif /* Q_COMPILER_RVALUE_REFS */

		ServiceInfoResp &operator=(const ServiceInfoResp &other) Q_DECL_NOTHROW;
#ifdef Q_COMPILER_RVALUE_REFS
		ServiceInfoResp &operator=(ServiceInfoResp &&other) Q_DECL_NOTHROW;
#endif /* Q_COMPILER_RVALUE_REFS */

		bool operator==(const ServiceInfoResp &other) const;
		bool operator!=(const ServiceInfoResp &other) const;

		static
		void swap(ServiceInfoResp &first, ServiceInfoResp &second) Q_DECL_NOTHROW;

		bool isNull(void) const;

		/*!
		 * @brief Check whether content is valid.
		 *
		 * @return True if content is valid.
		 */
		bool isValid(void) const;

		/*!
		 * @brief Return raw SVG data.
		 *
		 * @return Stored raw SVG data.
		 */
		const QByteArray &logoSvg(void) const;
		void setLogoSvg(const QByteArray &l);
#ifdef Q_COMPILER_RVALUE_REFS
		void setLogoSvg(QByteArray &&l);
#endif /* Q_COMPILER_RVALUE_REFS */

		/*!
		 * @brief Return service provider name.
		 *
		 * @return Stored name.
		 */
		const QString &name(void) const;
		void setName(const QString &n);
#ifdef Q_COMPILER_RVALUE_REFS
		void setName(QString &&n);
#endif /* Q_COMPILER_RVALUE_REFS */

		/*!
		 * @brief Return security token name.
		 *
		 * @return Stored token name.
		 */
		const QString &tokenName(void) const;
		void setTokenName(const QString &t);
#ifdef Q_COMPILER_RVALUE_REFS
		void setTokenName(QString &&t);
#endif /* Q_COMPILER_RVALUE_REFS */

		/*!
		 * @brief Creates a service info structure from supplied JSON document.
		 *
		 * @param[in]  json JSON document.
		 * @param[out] ok Set to true on success.
		 * @return Invalid structure on error a valid structure else.
		 */
		static
		ServiceInfoResp fromJson(const QByteArray &json,
		    bool *ok = Q_NULLPTR);

		static
		ServiceInfoResp fromJsonVal(const QJsonValue &jsonVal,
		    bool *ok = Q_NULLPTR);

		virtual
		bool toJsonVal(QJsonValue &jsonVal) const Q_DECL_OVERRIDE;

	private:
#if (QT_VERSION >= QT_VERSION_CHECK(5, 12, 0))
		::std::unique_ptr<ServiceInfoRespPrivate> d_ptr;
#else /* < Qt-5.12 */
		QScopedPointer<ServiceInfoRespPrivate> d_ptr;
#endif /* >= Qt-5.12 */
	};

}
