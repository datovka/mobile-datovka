/*
 * Copyright (C) 2014-2025 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

#include <QJsonArray>
#include <QJsonObject>
#include <QJsonValue>
#include <QString>
#include <QStringList>
#include <utility> /* ::std::swap */

#include "src/datovka_shared/compat/compiler.h" /* macroStdMove */
#include "src/datovka_shared/json/helper.h"
#include "src/datovka_shared/records_management/json/entry_error.h"
#include "src/datovka_shared/records_management/json/upload_file.h"

/* Null objects - for convenience. */
static const QStringList nullStringList;
static const QString nullString;
static const QByteArray nullByteArray;
static const RecMgmt::ErrorEntry nullErrorEntry;

static
const QString keyIds("ids");
static
const QString keyFileName("file_name");
static
const QString keyFileContent("file_content");

static
const QString keyId("id");
static
const QString keyError("error");
static
const QString keyLocations("locations");

class RecMgmt::UploadFileReqPrivate {
public:
	UploadFileReqPrivate(void)
	   : m_ids(), m_fileName(), m_fileContent()
	{ }

	UploadFileReqPrivate &operator=(const UploadFileReqPrivate &other) Q_DECL_NOTHROW
	{
		m_ids = other.m_ids;
		m_fileName = other.m_fileName;
		m_fileContent = other.m_fileContent;

		return *this;
	}

	bool operator==(const UploadFileReqPrivate &other) const
	{
		return (m_ids == other.m_ids) &&
		    (m_fileName == other.m_fileName) &&
		    (m_fileContent == other.m_fileContent);
	}

	QStringList m_ids; /*!< Location identifiers as obtained from upload_hierarchy. */
	QString m_fileName; /*!< Uploaded file name. */
	QByteArray m_fileContent; /*!< Raw content of uploaded file. */
};

RecMgmt::UploadFileReq::UploadFileReq(void)
    : Json::Object(),
    d_ptr(Q_NULLPTR)
{
}

RecMgmt::UploadFileReq::UploadFileReq(const UploadFileReq &other)
    : Json::Object(),
    d_ptr((other.d_func() != Q_NULLPTR) ? (new (::std::nothrow) UploadFileReqPrivate) : Q_NULLPTR)
{
	Q_D(UploadFileReq);
	if (d == Q_NULLPTR) {
		return;
	}

	*d = *other.d_func();
}

#ifdef Q_COMPILER_RVALUE_REFS
RecMgmt::UploadFileReq::UploadFileReq(UploadFileReq &&other) Q_DECL_NOEXCEPT
    : Json::Object(),
#if (QT_VERSION >= QT_VERSION_CHECK(5, 12, 0))
    d_ptr(other.d_ptr.release()) //d_ptr(::std::move(other.d_ptr))
#else /* < Qt-5.12 */
    d_ptr(other.d_ptr.take())
#endif /* >= Qt-5.12 */
{
}
#endif /* Q_COMPILER_RVALUE_REFS */

RecMgmt::UploadFileReq::~UploadFileReq(void)
{
}

RecMgmt::UploadFileReq::UploadFileReq(const QStringList &ids,
    const QString &fileName, const QByteArray &fileContent)
    : Json::Object(),
    d_ptr(new (::std::nothrow) UploadFileReqPrivate)
{
	Q_D(UploadFileReq);
	d->m_ids = ids;
	d->m_fileName = fileName;
	d->m_fileContent = fileContent;
}

#ifdef Q_COMPILER_RVALUE_REFS
RecMgmt::UploadFileReq::UploadFileReq(QStringList &&ids,
    QString &&fileName, QByteArray &&fileContent)
    : Json::Object(),
    d_ptr(new (::std::nothrow) UploadFileReqPrivate)
{
	Q_D(UploadFileReq);
	d->m_ids = ::std::move(ids);
	d->m_fileName = ::std::move(fileName);
	d->m_fileContent = ::std::move(fileContent);
}
#endif /* Q_COMPILER_RVALUE_REFS */

/*!
 * @brief Ensures UploadFileReqPrivate presence.
 *
 * @note Returns if UploadFileReqPrivate could not be allocated.
 */
#define ensureUploadFileReqPrivate(_x_) \
	do { \
		if (Q_UNLIKELY(d_ptr == Q_NULLPTR)) { \
			UploadFileReqPrivate *p = new (::std::nothrow) UploadFileReqPrivate; \
			if (Q_UNLIKELY(p == Q_NULLPTR)) { \
				Q_ASSERT(0); \
				return _x_; \
			} \
			d_ptr.reset(p); \
		} \
	} while (0)

RecMgmt::UploadFileReq &RecMgmt::UploadFileReq::operator=(const UploadFileReq &other) Q_DECL_NOTHROW
{
	if (other.d_func() == Q_NULLPTR) {
		d_ptr.reset(Q_NULLPTR);
		return *this;
	}
	ensureUploadFileReqPrivate(*this);
	Q_D(UploadFileReq);

	*d = *other.d_func();

	return *this;
}

#ifdef Q_COMPILER_RVALUE_REFS
RecMgmt::UploadFileReq &RecMgmt::UploadFileReq::operator=(UploadFileReq &&other) Q_DECL_NOTHROW
{
	swap(*this, other);
	return *this;
}
#endif /* Q_COMPILER_RVALUE_REFS */

bool RecMgmt::UploadFileReq::operator==(const UploadFileReq &other) const
{
	Q_D(const UploadFileReq);
	if ((d == Q_NULLPTR) && ((other.d_func() == Q_NULLPTR))) {
		return true;
	} else if ((d == Q_NULLPTR) || ((other.d_func() == Q_NULLPTR))) {
		return false;
	}

	return *d == *other.d_func();
}

bool RecMgmt::UploadFileReq::operator!=(const UploadFileReq &other) const
{
	return !operator==(other);
}

void RecMgmt::UploadFileReq::swap(UploadFileReq &first, UploadFileReq &second) Q_DECL_NOTHROW
{
	using ::std::swap;
	swap(first.d_ptr, second.d_ptr);
}

bool RecMgmt::UploadFileReq::isNull(void) const
{
	Q_D(const UploadFileReq);
	return d == Q_NULLPTR;
}

bool RecMgmt::UploadFileReq::isValid(void) const
{
	Q_D(const UploadFileReq);
	bool valid = (!isNull()) && (!d->m_ids.isEmpty()) &&
	    (!d->m_fileName.isEmpty()) && (!d->m_fileContent.isEmpty());
	if (!valid) {
		return false;
	}

	for (const QString &id : d->m_ids) {
		if (id.isEmpty()) {
			return false;
		}
	}

	return true;
}

const QStringList &RecMgmt::UploadFileReq::ids(void) const
{
	Q_D(const UploadFileReq);
	if (Q_UNLIKELY(d == Q_NULLPTR)) {
		return nullStringList;
	}

	return d->m_ids;
}

void RecMgmt::UploadFileReq::setIds(const QStringList &i)
{
	ensureUploadFileReqPrivate();
	Q_D(UploadFileReq);
	d->m_ids = i;
}

#ifdef Q_COMPILER_RVALUE_REFS
void RecMgmt::UploadFileReq::setIds(QStringList &&i)
{
	ensureUploadFileReqPrivate();
	Q_D(UploadFileReq);
	d->m_ids = ::std::move(i);
}
#endif /* Q_COMPILER_RVALUE_REFS */

const QString &RecMgmt::UploadFileReq::fileName(void) const
{
	Q_D(const UploadFileReq);
	if (Q_UNLIKELY(d == Q_NULLPTR)) {
		return nullString;
	}

	return d->m_fileName;
}

void RecMgmt::UploadFileReq::setFileName(const QString &f)
{
	ensureUploadFileReqPrivate();
	Q_D(UploadFileReq);
	d->m_fileName = f;
}

#ifdef Q_COMPILER_RVALUE_REFS
void RecMgmt::UploadFileReq::setFileName(QString &&f)
{
	ensureUploadFileReqPrivate();
	Q_D(UploadFileReq);
	d->m_fileName = ::std::move(f);
}
#endif /* Q_COMPILER_RVALUE_REFS */

const QByteArray &RecMgmt::UploadFileReq::fileContent(void) const
{
	Q_D(const UploadFileReq);
	if (Q_UNLIKELY(d == Q_NULLPTR)) {
		return nullByteArray;
	}

	return d->m_fileContent;
}

void RecMgmt::UploadFileReq::setFileContent(const QByteArray &c)
{
	ensureUploadFileReqPrivate();
	Q_D(UploadFileReq);
	d->m_fileContent = c;
}

#ifdef Q_COMPILER_RVALUE_REFS
void RecMgmt::UploadFileReq::setFileContent(QByteArray &&c)
{
	ensureUploadFileReqPrivate();
	Q_D(UploadFileReq);
	d->m_fileContent = ::std::move(c);
}
#endif /* Q_COMPILER_RVALUE_REFS */

RecMgmt::UploadFileReq RecMgmt::UploadFileReq::fromJson(
    const QByteArray &json, bool *ok)
{
	QJsonObject jsonObj;
	if (Q_UNLIKELY(!Json::Helper::readRootObject(json, jsonObj))) {
		if (ok != Q_NULLPTR) {
			*ok = false;
		}
		return UploadFileReq();
	}

	return fromJsonVal(jsonObj, ok);
}

RecMgmt::UploadFileReq RecMgmt::UploadFileReq::fromJsonVal(
    const QJsonValue &jsonVal, bool *ok)
{
	UploadFileReq ufr;

	if (Q_UNLIKELY(!jsonVal.isObject())) {
		goto fail;
	}

	{
		const QJsonObject jsonObj = jsonVal.toObject();
		{
			QStringList valStrList;
			if (Json::Helper::readStringList(jsonObj, keyIds, valStrList,
			        Json::Helper::ACCEPT_VALID)) {
				ufr.setIds(macroStdMove(valStrList));
			} else {
				goto fail;
			}
		}
		{
			QString valStr;
			if (Json::Helper::readString(jsonObj, keyFileName, valStr,
			        Json::Helper::ACCEPT_VALID)) {
				ufr.setFileName(macroStdMove(valStr));
			} else {
				goto fail;
			}
		}
		{
			QByteArray valData;
			if (Json::Helper::readBase64DataString(jsonObj, keyFileContent, valData,
			        Json::Helper::ACCEPT_VALID)) {
				ufr.setFileContent(macroStdMove(valData));
			} else {
				goto fail;
			}
		}

		if (ok != Q_NULLPTR) {
			*ok = true;
		}
		return ufr;
	}

fail:
	if (ok != Q_NULLPTR) {
		*ok = false;
	}
	return UploadFileReq();
}

bool RecMgmt::UploadFileReq::toJsonVal(QJsonValue &jsonVal) const
{
	QJsonObject jsonObj;

	jsonObj.insert(keyIds, QJsonArray::fromStringList(ids()));
	jsonObj.insert(keyFileName, !fileName().isNull() ?
	    fileName() : QJsonValue());
	jsonObj.insert(keyFileContent, !fileContent().isNull() ?
	    QString::fromUtf8(fileContent().toBase64()) : QJsonValue());

	jsonVal = jsonObj;
	return true;
}

class RecMgmt::UploadFileRespPrivate {
public:
	UploadFileRespPrivate(void)
	    : m_id(), m_error(), m_locations()
	{ }

	UploadFileRespPrivate &operator=(const UploadFileRespPrivate &other) Q_DECL_NOTHROW
	{
		m_id = other.m_id;
		m_error = other.m_error;
		m_locations = other.m_locations;

		return *this;
	}

	bool operator==(const UploadFileRespPrivate &other) const
	{
		return (m_id == other.m_id) &&
		    (m_error == other.m_error) &&
		    (m_locations == other.m_locations);
	}

	QString m_id; /*!< Uploaded file identifier (not necessary from ISDS). */
	ErrorEntry m_error; /*!< Brief error entry. */
	QStringList m_locations; /*!< Where the uploaded file is located in the service. */
};

RecMgmt::UploadFileResp::UploadFileResp(void)
    : Json::Object(),
    d_ptr(Q_NULLPTR)
{
}

RecMgmt::UploadFileResp::UploadFileResp(const UploadFileResp &other)
    : Json::Object(),
    d_ptr((other.d_func() != Q_NULLPTR) ? (new (::std::nothrow) UploadFileRespPrivate) : Q_NULLPTR)
{
	Q_D(UploadFileResp);
	if (d == Q_NULLPTR) {
		return;
	}

	*d = *other.d_func();
}

#ifdef Q_COMPILER_RVALUE_REFS
RecMgmt::UploadFileResp::UploadFileResp(UploadFileResp &&other) Q_DECL_NOEXCEPT
    : Json::Object(),
#if (QT_VERSION >= QT_VERSION_CHECK(5, 12, 0))
    d_ptr(other.d_ptr.release()) //d_ptr(::std::move(other.d_ptr))
#else /* < Qt-5.12 */
    d_ptr(other.d_ptr.take())
#endif /* >= Qt-5.12 */
{
}
#endif /* Q_COMPILER_RVALUE_REFS */

RecMgmt::UploadFileResp::~UploadFileResp(void)
{
}

RecMgmt::UploadFileResp::UploadFileResp(const QString &id,
    const ErrorEntry &error, const QStringList &locations)
    : Json::Object(),
    d_ptr(new (::std::nothrow) UploadFileRespPrivate)
{
	Q_D(UploadFileResp);
	d->m_id = id;
	d->m_error = error;
	d->m_locations = locations;
}

#ifdef Q_COMPILER_RVALUE_REFS
RecMgmt::UploadFileResp::UploadFileResp(QString &&id, ErrorEntry &&error,
    QStringList &&locations)
    : Json::Object(),
    d_ptr(new (::std::nothrow) UploadFileRespPrivate)
{
	Q_D(UploadFileResp);
	d->m_id = ::std::move(id);
	d->m_error = ::std::move(error);
	d->m_locations = ::std::move(locations);
}
#endif /* Q_COMPILER_RVALUE_REFS */

/*!
 * @brief Ensures UploadFileRespPrivate presence.
 *
 * @note Returns if UploadFileRespPrivate could not be allocated.
 */
#define ensureUploadFileRespPrivate(_x_) \
	do { \
		if (Q_UNLIKELY(d_ptr == Q_NULLPTR)) { \
			UploadFileRespPrivate *p = new (::std::nothrow) UploadFileRespPrivate; \
			if (Q_UNLIKELY(p == Q_NULLPTR)) { \
				Q_ASSERT(0); \
				return _x_; \
			} \
			d_ptr.reset(p); \
		} \
	} while (0)

RecMgmt::UploadFileResp &RecMgmt::UploadFileResp::operator=(const UploadFileResp &other) Q_DECL_NOTHROW
{
	if (other.d_func() == Q_NULLPTR) {
		d_ptr.reset(Q_NULLPTR);
		return *this;
	}
	ensureUploadFileRespPrivate(*this);
	Q_D(UploadFileResp);

	*d = *other.d_func();

	return *this;
}

#ifdef Q_COMPILER_RVALUE_REFS
RecMgmt::UploadFileResp &RecMgmt::UploadFileResp::operator=(UploadFileResp &&other) Q_DECL_NOTHROW
{
	swap(*this, other);
	return *this;
}
#endif /* Q_COMPILER_RVALUE_REFS */

bool RecMgmt::UploadFileResp::operator==(const UploadFileResp &other) const
{
	Q_D(const UploadFileResp);
	if ((d == Q_NULLPTR) && ((other.d_func() == Q_NULLPTR))) {
		return true;
	} else if ((d == Q_NULLPTR) || ((other.d_func() == Q_NULLPTR))) {
		return false;
	}

	return *d == *other.d_func();
}

bool RecMgmt::UploadFileResp::operator!=(const UploadFileResp &other) const
{
	return !operator==(other);
}

void RecMgmt::UploadFileResp::swap(UploadFileResp &first, UploadFileResp &second) Q_DECL_NOTHROW
{
	using ::std::swap;
	swap(first.d_ptr, second.d_ptr);
}

bool RecMgmt::UploadFileResp::isNull(void) const
{
	Q_D(const UploadFileResp);
	return d == Q_NULLPTR;
}

bool RecMgmt::UploadFileResp::isValid(void) const
{
	Q_D(const UploadFileResp);
	return (!isNull()) &&
	    (((!d->m_id.isEmpty()) && (!d->m_locations.isEmpty())) ||
	     (d->m_error.code() != ErrorEntry::ERR_NO_ERROR));
}

const QString &RecMgmt::UploadFileResp::id(void) const
{
	Q_D(const UploadFileResp);
	if (Q_UNLIKELY(d == Q_NULLPTR)) {
		return nullString;
	}

	return d->m_id;
}

void RecMgmt::UploadFileResp::setId(const QString &i)
{
	ensureUploadFileRespPrivate();
	Q_D(UploadFileResp);
	d->m_id = i;
}

#ifdef Q_COMPILER_RVALUE_REFS
void RecMgmt::UploadFileResp::setId(QString &&i)
{
	ensureUploadFileRespPrivate();
	Q_D(UploadFileResp);
	d->m_id = ::std::move(i);
}
#endif /* Q_COMPILER_RVALUE_REFS */

const RecMgmt::ErrorEntry &RecMgmt::UploadFileResp::error(void) const
{
	Q_D(const UploadFileResp);
	if (Q_UNLIKELY(d == Q_NULLPTR)) {
		return nullErrorEntry;
	}

	return d->m_error;
}

void RecMgmt::UploadFileResp::setError(const ErrorEntry &e)
{
	ensureUploadFileRespPrivate();
	Q_D(UploadFileResp);
	d->m_error = e;
}

#ifdef Q_COMPILER_RVALUE_REFS
void RecMgmt::UploadFileResp::setError(ErrorEntry &&e)
{
	ensureUploadFileRespPrivate();
	Q_D(UploadFileResp);
	d->m_error = ::std::move(e);
}
#endif /* Q_COMPILER_RVALUE_REFS */

const QStringList &RecMgmt::UploadFileResp::locations(void) const
{
	Q_D(const UploadFileResp);
	if (Q_UNLIKELY(d == Q_NULLPTR)) {
		return nullStringList;
	}

	return d->m_locations;
}

void RecMgmt::UploadFileResp::setLocations(const QStringList &l)
{
	ensureUploadFileRespPrivate();
	Q_D(UploadFileResp);
	d->m_locations = l;
}

#ifdef Q_COMPILER_RVALUE_REFS
void RecMgmt::UploadFileResp::setLocations(QStringList &&l)
{
	ensureUploadFileRespPrivate();
	Q_D(UploadFileResp);
	d->m_locations = ::std::move(l);
}
#endif /* Q_COMPILER_RVALUE_REFS */

RecMgmt::UploadFileResp RecMgmt::UploadFileResp::fromJson(
    const QByteArray &json, bool *ok)
{
	QJsonObject jsonObj;
	if (Q_UNLIKELY(!Json::Helper::readRootObject(json, jsonObj))) {
		if (ok != Q_NULLPTR) {
			*ok = false;
		}
		return UploadFileResp();
	}

	return fromJsonVal(jsonObj, ok);
}

RecMgmt::UploadFileResp RecMgmt::UploadFileResp::fromJsonVal(
    const QJsonValue &jsonVal, bool *ok)
{
	UploadFileResp ufr;

	if (Q_UNLIKELY(!jsonVal.isObject())) {
		goto fail;
	}

	{
		const QJsonObject jsonObj = jsonVal.toObject();
		{
			QString valStr;
			if (Json::Helper::readString(jsonObj, keyId, valStr,
			        Json::Helper::ACCEPT_NULL)) {
				ufr.setId(macroStdMove(valStr));
			} else {
				goto fail;
			}
		}
		{
			QJsonValue jsonVal;
			if (Json::Helper::readValue(jsonObj, keyError, jsonVal,
			        Json::Helper::ACCEPT_NULL)) {
				bool iOk = false;
				ufr.setError(ErrorEntry::fromJsonVal(jsonVal, &iOk));
				if (Q_UNLIKELY(!iOk)) {
					goto fail;
				}
			} else {
				goto fail;
			}
		}
		{
			QStringList valStrList;
			if (Json::Helper::readStringList(jsonObj, keyLocations,
			        valStrList, Json::Helper::ACCEPT_VALID)) {
				ufr.setLocations(macroStdMove(valStrList));
			} else {
				goto fail;
			}
		}

		if (ok != Q_NULLPTR) {
			*ok = true;
		}
		return ufr;
	}

fail:
	if (ok != Q_NULLPTR) {
		*ok = false;
	}
	return UploadFileResp();
}

bool RecMgmt::UploadFileResp::toJsonVal(QJsonValue &jsonVal) const
{
	QJsonObject jsonObj;

	jsonObj.insert(keyId, !id().isNull() ? id() : QJsonValue());
	{
		QJsonValue jsonVal;
		if (Q_UNLIKELY(!error().toJsonVal(jsonVal))) {
			goto fail;
		}
		jsonObj.insert(keyError, jsonVal);
	}
	jsonObj.insert(keyLocations, QJsonArray::fromStringList(locations()));

	jsonVal = jsonObj;
	return true;

fail:
	jsonVal = QJsonObject();
	return false;
}
