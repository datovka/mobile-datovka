/*
 * Copyright (C) 2014-2025 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

#include <QByteArray>
#include <QJsonObject>
#include <QJsonValue>
#include <QString>
#include <utility> /* ::std::swap */

#include "src/datovka_shared/compat/compiler.h" /* macroStdMove */
#include "src/datovka_shared/json/helper.h"
#include "src/datovka_shared/records_management/json/service_info.h"

/* Null objects - for convenience. */
static const QByteArray nullByteArray;
static const QString nullString;

static
const QString keyLogoSvg("logo_svg");
static
const QString keyName("name");
static
const QString keyTokenName("token_name");

class RecMgmt::ServiceInfoRespPrivate {
public:
	ServiceInfoRespPrivate(void)
	    : m_logoSvg(), m_name(), m_tokenName()
	{ }

	ServiceInfoRespPrivate &operator=(const ServiceInfoRespPrivate &other) Q_DECL_NOTHROW
	{
		m_logoSvg = other.m_logoSvg;
		m_name = other.m_name;
		m_tokenName = other.m_tokenName;

		return *this;
	}

	bool operator==(const ServiceInfoRespPrivate &other) const
	{
		return (m_logoSvg == other.m_logoSvg) &&
		    (m_name == other.m_name) &&
		    (m_tokenName == other.m_tokenName);
	}

	QByteArray m_logoSvg; /*!< Raw SVG data. */
	QString m_name; /*!< Service provider name. */
	QString m_tokenName; /*!< Obtained token identifier. */
};

RecMgmt::ServiceInfoResp::ServiceInfoResp(void)
    : Json::Object(),
    d_ptr(Q_NULLPTR)
{
}

RecMgmt::ServiceInfoResp::ServiceInfoResp(const ServiceInfoResp &other)
    : Json::Object(),
    d_ptr((other.d_func() != Q_NULLPTR) ? (new (::std::nothrow) ServiceInfoRespPrivate) : Q_NULLPTR)
{
	Q_D(ServiceInfoResp);
	if (d == Q_NULLPTR) {
		return;
	}

	*d = *other.d_func();
}

#ifdef Q_COMPILER_RVALUE_REFS
RecMgmt::ServiceInfoResp::ServiceInfoResp(ServiceInfoResp &&other) Q_DECL_NOEXCEPT
    : Json::Object(),
#if (QT_VERSION >= QT_VERSION_CHECK(5, 12, 0))
    d_ptr(other.d_ptr.release()) //d_ptr(::std::move(other.d_ptr))
#else /* < Qt-5.12 */
    d_ptr(other.d_ptr.take())
#endif /* >= Qt-5.12 */
{
}
#endif /* Q_COMPILER_RVALUE_REFS */

RecMgmt::ServiceInfoResp::~ServiceInfoResp(void)
{
}

RecMgmt::ServiceInfoResp::ServiceInfoResp(const QByteArray &logoSvg,
    const QString &name, const QString &tokenName)
    : Json::Object(),
    d_ptr(new (::std::nothrow) ServiceInfoRespPrivate)
{
	Q_D(ServiceInfoResp);
	d->m_logoSvg = logoSvg;
	d->m_name = name;
	d->m_tokenName = tokenName;
}

#ifdef Q_COMPILER_RVALUE_REFS
RecMgmt::ServiceInfoResp::ServiceInfoResp(QByteArray &&logoSvg, QString &&name,
    QString &&tokenName)
    : Json::Object(),
    d_ptr(new (::std::nothrow) ServiceInfoRespPrivate)
{
	Q_D(ServiceInfoResp);
	d->m_logoSvg = ::std::move(logoSvg);
	d->m_name = ::std::move(name);
	d->m_tokenName = ::std::move(tokenName);
}
#endif /* Q_COMPILER_RVALUE_REFS */

/*!
 * @brief Ensures ServiceInfoRespPrivate presence.
 *
 * @note Returns if ServiceInfoRespPrivate could not be allocated.
 */
#define ensureServiceInfoRespPrivate(_x_) \
	do { \
		if (Q_UNLIKELY(d_ptr == Q_NULLPTR)) { \
			ServiceInfoRespPrivate *p = new (::std::nothrow) ServiceInfoRespPrivate; \
			if (Q_UNLIKELY(p == Q_NULLPTR)) { \
				Q_ASSERT(0); \
				return _x_; \
			} \
			d_ptr.reset(p); \
		} \
	} while (0)

RecMgmt::ServiceInfoResp &RecMgmt::ServiceInfoResp::operator=(const ServiceInfoResp &other) Q_DECL_NOTHROW
{
	if (other.d_func() == Q_NULLPTR) {
		d_ptr.reset(Q_NULLPTR);
		return *this;
	}
	ensureServiceInfoRespPrivate(*this);
	Q_D(ServiceInfoResp);

	*d = *other.d_func();

	return *this;
}

#ifdef Q_COMPILER_RVALUE_REFS
RecMgmt::ServiceInfoResp &RecMgmt::ServiceInfoResp::operator=(ServiceInfoResp &&other) Q_DECL_NOTHROW
{
	swap(*this, other);
	return *this;
}
#endif /* Q_COMPILER_RVALUE_REFS */

bool RecMgmt::ServiceInfoResp::operator==(const ServiceInfoResp &other) const
{
	Q_D(const ServiceInfoResp);
	if ((d == Q_NULLPTR) && ((other.d_func() == Q_NULLPTR))) {
		return true;
	} else if ((d == Q_NULLPTR) || ((other.d_func() == Q_NULLPTR))) {
		return false;
	}

	return *d == *other.d_func();
}

bool RecMgmt::ServiceInfoResp::operator!=(const ServiceInfoResp &other) const
{
	return !operator==(other);
}

void RecMgmt::ServiceInfoResp::swap(ServiceInfoResp &first, ServiceInfoResp &second) Q_DECL_NOTHROW
{
	using ::std::swap;
	swap(first.d_ptr, second.d_ptr);
}

bool RecMgmt::ServiceInfoResp::isNull(void) const
{
	Q_D(const ServiceInfoResp);
	return d == Q_NULLPTR;
}

bool RecMgmt::ServiceInfoResp::isValid(void) const
{
	Q_D(const ServiceInfoResp);
	return (!isNull()) && (!d->m_logoSvg.isNull()) &&
	    (!d->m_name.isNull()) && (!d->m_tokenName.isNull());
}

const QByteArray &RecMgmt::ServiceInfoResp::logoSvg(void) const
{
	Q_D(const ServiceInfoResp);
	if (Q_UNLIKELY(d == Q_NULLPTR)) {
		return nullByteArray;
	}

	return d->m_logoSvg;
}

void RecMgmt::ServiceInfoResp::setLogoSvg(const QByteArray &l)
{
	ensureServiceInfoRespPrivate();
	Q_D(ServiceInfoResp);
	d->m_logoSvg = l;
}

#ifdef Q_COMPILER_RVALUE_REFS
void RecMgmt::ServiceInfoResp::setLogoSvg(QByteArray &&l)
{
	ensureServiceInfoRespPrivate();
	Q_D(ServiceInfoResp);
	d->m_logoSvg = ::std::move(l);
}
#endif /* Q_COMPILER_RVALUE_REFS */

const QString &RecMgmt::ServiceInfoResp::name(void) const
{
	Q_D(const ServiceInfoResp);
	if (Q_UNLIKELY(d == Q_NULLPTR)) {
		return nullString;
	}

	return d->m_name;
}

void RecMgmt::ServiceInfoResp::setName(const QString &n)
{
	ensureServiceInfoRespPrivate();
	Q_D(ServiceInfoResp);
	d->m_name = n;
}

#ifdef Q_COMPILER_RVALUE_REFS
void RecMgmt::ServiceInfoResp::setName(QString &&n)
{
	ensureServiceInfoRespPrivate();
	Q_D(ServiceInfoResp);
	d->m_name = ::std::move(n);
}
#endif /* Q_COMPILER_RVALUE_REFS */

const QString &RecMgmt::ServiceInfoResp::tokenName(void) const
{
	Q_D(const ServiceInfoResp);
	if (Q_UNLIKELY(d == Q_NULLPTR)) {
		return nullString;
	}

	return d->m_tokenName;
}

void RecMgmt::ServiceInfoResp::setTokenName(const QString &t)
{
	ensureServiceInfoRespPrivate();
	Q_D(ServiceInfoResp);
	d->m_tokenName = t;
}

#ifdef Q_COMPILER_RVALUE_REFS
void RecMgmt::ServiceInfoResp::setTokenName(QString &&t)
{
	{
	ensureServiceInfoRespPrivate();
	Q_D(ServiceInfoResp);
	d->m_tokenName = ::std::move(t);
}
}
#endif /* Q_COMPILER_RVALUE_REFS */

RecMgmt::ServiceInfoResp RecMgmt::ServiceInfoResp::fromJson(
    const QByteArray &json, bool *ok)
{
	QJsonObject jsonObj;
	if (Q_UNLIKELY(!Json::Helper::readRootObject(json, jsonObj))) {
		if (ok != Q_NULLPTR) {
			*ok = false;
		}
		return ServiceInfoResp();
	}

	return fromJsonVal(jsonObj, ok);
}

RecMgmt::ServiceInfoResp RecMgmt::ServiceInfoResp::fromJsonVal(
    const QJsonValue &jsonVal, bool *ok)
{
	ServiceInfoResp sir;
	QString valStr;

	if (Q_UNLIKELY(!jsonVal.isObject())) {
		goto fail;
	}

	{
		const QJsonObject jsonObj = jsonVal.toObject();
		{
			QByteArray valData;
			if (Json::Helper::readBase64DataString(jsonObj, keyLogoSvg, valData,
			        Json::Helper::ACCEPT_VALID)) {
				sir.setLogoSvg(macroStdMove(valData));
			} else {
				goto fail;
			}
		}

		if (Json::Helper::readString(jsonObj, keyName, valStr,
		        Json::Helper::ACCEPT_VALID)) {
			sir.setName(macroStdMove(valStr));
		} else {
			goto fail;
		}
		if (Json::Helper::readString(jsonObj, keyTokenName, valStr,
		        Json::Helper::ACCEPT_VALID)) {
			sir.setTokenName(macroStdMove(valStr));
		} else {
			goto fail;
		}

		if (ok != Q_NULLPTR) {
			*ok = true;
		}
		return sir;
	}

fail:
	if (ok != Q_NULLPTR) {
		*ok = false;
	}
	return ServiceInfoResp();
}

bool RecMgmt::ServiceInfoResp::toJsonVal(QJsonValue &jsonVal) const
{
	QJsonObject jsonObj;

	jsonObj.insert(keyLogoSvg, !logoSvg().isNull() ?
	    QString::fromUtf8(logoSvg().toBase64()) : QJsonValue());
	jsonObj.insert(keyName, !name().isNull() ? name() : QJsonValue());
	jsonObj.insert(keyTokenName, !tokenName().isNull() ?
	    tokenName() : QJsonValue());

	jsonVal = jsonObj;
	return true;
}
