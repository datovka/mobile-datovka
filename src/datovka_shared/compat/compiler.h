/*
 * Copyright (C) 2014-2025 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

#pragma once

#include <QtGlobal> /* Q_COMPILER_RVALUE_REFS */

/*
 * Use ignoreRetVal() in code to indicate that the return value is intentionally
 * ignored.
 * It can also potentially be used to silence static analysis tools to complain
 * about unchecked return values.
 *
 * Using static_cast<void>(x) instead of (void)(x).
 */
#define ignoreRetVal(x) static_cast<void>(x)

/*
 * Use macroStdMove() in code where move assignment can be used but the support
 * for such assignment is not checked.
 *
 * In portions of code where the availability of a move assignment operator is
 * guaranteed use directly ::std::move().
 */
#ifdef Q_COMPILER_RVALUE_REFS
#  define macroStdMove(x) ::std::move(x)
#else /* Q_COMPILER_RVALUE_REFS */
#  define macroStdMove(x) (x)
#endif /* Q_COMPILER_RVALUE_REFS */
