/*
 * Copyright (C) 2014-2025 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

#pragma once

class QByteArray; /* Forward declaration. */
class QJsonValue; /* Forward declaration. */

namespace Json {

	/*!
	 * @brief Describes generic JSON object/array interface.
	 */
	class Object {
	public:
		/*!
		 * @brief Constructor.
		 */
		Object(void);

		/*!
		 * @brief Destructor.
		 */
		virtual
		~Object(void);

		/*!
		 * @brief Creates a JSON hierarchy.
		 *
		 * @param[out] jsonVal JSON value to write to.
		 * @return True on success.
		 */
		virtual
		bool toJsonVal(QJsonValue &jsonVal) const = 0;

		/*!
		 * @brief Builds a JSON string.
		 *
		 * @param[in]  indent Whether to enable or disable indentation.
		 */
		QByteArray toJsonData(bool indent = false) const;
	};

}
