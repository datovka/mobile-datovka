/*
 * Copyright (C) 2014-2025 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

#pragma once

#if (QT_VERSION >= QT_VERSION_CHECK(5, 12, 0))
#  include <memory> /* ::std::unique_ptr */
#else /* < Qt-5.12 */
#  include <QScopedPointer>
#endif /* >= Qt-5.12 */

#include "src/datovka_shared/isds/types.h"

class QByteArray; /* Forward declaration. */
class QString; /* Forward declaration. */

/*
 * https://stackoverflow.com/questions/25250171/how-to-use-the-qts-pimpl-idiom
 *
 * QScopedPtr -> ::std::unique_ptr ?
 */

namespace Isds {

	class DmFilePrivate;
	/*!
	 * @brief Attachment, used for high-volume data messages.
	 */
	class DmFile {
		Q_DECLARE_PRIVATE(DmFile)

	public:
		DmFile(void);
		DmFile(const DmFile &other);
#ifdef Q_COMPILER_RVALUE_REFS
		DmFile(DmFile &&other) Q_DECL_NOEXCEPT;
#endif /* Q_COMPILER_RVALUE_REFS */
		~DmFile(void);

		DmFile &operator=(const DmFile &other) Q_DECL_NOTHROW;
#ifdef Q_COMPILER_RVALUE_REFS
		DmFile &operator=(DmFile &&other) Q_DECL_NOTHROW;
#endif /* Q_COMPILER_RVALUE_REFS */

		bool operator==(const DmFile &other) const;
		bool operator!=(const DmFile &other) const;

		friend void swap(DmFile &first, DmFile &second) Q_DECL_NOTHROW;

		bool isNull(void) const;

		/* binaryContent */
		const QByteArray &binaryContent(void) const;
		void setBinaryContent(const QByteArray &bc);
#ifdef Q_COMPILER_RVALUE_REFS
		void setBinaryContent(QByteArray &&bc);
#endif /* Q_COMPILER_RVALUE_REFS */

		/* dmFileMetaType */
		enum Type::FileMetaType fileMetaType(void) const;
		void setFileMetaType(enum Type::FileMetaType mt);

		/* dmMimeType */
		const QString &mimeType(void) const;
		void setMimeType(const QString &mt);
#ifdef Q_COMPILER_RVALUE_REFS
		void setMimeType(QString &&mt);
#endif /* Q_COMPILER_RVALUE_REFS */

		/* dmFileDescr */
		const QString &fileDescr(void) const;
		void setFileDescr(const QString &fd);
#ifdef Q_COMPILER_RVALUE_REFS
		void setFileDescr(QString &&fd);
#endif /* Q_COMPILER_RVALUE_REFS */

		/* dmFileGuid */
		const QString &fileGuid(void) const;
		void setFileGuid(const QString &g);
#ifdef Q_COMPILER_RVALUE_REFS
		void setFileGuid(QString &&g);
#endif /* Q_COMPILER_RVALUE_REFS */

		/* dmUpFileGuid */
		const QString &upFileGuid(void) const;
		void setUpFileGuid(const QString &ug);
#ifdef Q_COMPILER_RVALUE_REFS
		void setUpFileGuid(QString &&ug);
#endif /* Q_COMPILER_RVALUE_REFS */

	private:
#if (QT_VERSION >= QT_VERSION_CHECK(5, 12, 0))
		::std::unique_ptr<DmFilePrivate> d_ptr;
#else /* < Qt-5.12 */
		QScopedPointer<DmFilePrivate> d_ptr;
#endif /* >= Qt-5.12 */
	};

	void swap(DmFile &first, DmFile &second) Q_DECL_NOTHROW;

	class DmAttPrivate;
	/*!
	 * @brief Response for UploadAttachment.
	 *     Complete attachment identification.
	 *     Encapsulates the attachment identifier and hashes for high-volume data messages.
	 */
	class DmAtt {
		Q_DECLARE_PRIVATE(DmAtt)

	public:
		DmAtt(void);
		DmAtt(const DmAtt &other);
#ifdef Q_COMPILER_RVALUE_REFS
		DmAtt(DmAtt &&other) Q_DECL_NOEXCEPT;
#endif /* Q_COMPILER_RVALUE_REFS */
		~DmAtt(void);

		DmAtt &operator=(const DmAtt &other) Q_DECL_NOTHROW;
#ifdef Q_COMPILER_RVALUE_REFS
		DmAtt &operator=(DmAtt &&other) Q_DECL_NOTHROW;
#endif /* Q_COMPILER_RVALUE_REFS */

		bool operator==(const DmAtt &other) const;
		bool operator!=(const DmAtt &other) const;

		friend void swap(DmAtt &first, DmAtt &second) Q_DECL_NOTHROW;

		bool isNull(void) const;

		/* dmAttID */
		const QString &dmAttID(void) const;
		void setDmAttID(const QString &ai);
#ifdef Q_COMPILER_RVALUE_REFS
		void setDmAttID(QString &&ai);
#endif /* Q_COMPILER_RVALUE_REFS */

		/* dmAttHash1 */
		const QString &dmAttHash1(void) const;
		void setDmAttHash1(const QString &h1);
#ifdef Q_COMPILER_RVALUE_REFS
		void setDmAttHash1(QString &&h1);
#endif /* Q_COMPILER_RVALUE_REFS */
		/* dmAttHash1Alg */
		const QString &dmAttHash1Alg(void) const;
		void setDmAttHash1Alg(const QString &a1);
#ifdef Q_COMPILER_RVALUE_REFS
		void setDmAttHash1Alg(QString &&a1);
#endif /* Q_COMPILER_RVALUE_REFS */
		/* dmAttHash2 */
		const QString &dmAttHash2(void) const;
		void setDmAttHash2(const QString &h2);
#ifdef Q_COMPILER_RVALUE_REFS
		void setDmAttHash2(QString &&h2);
#endif /* Q_COMPILER_RVALUE_REFS */
		/* dmAttHash2Alg */
		const QString &dmAttHash2Alg(void) const;
		void setDmAttHash2Alg(const QString &a2);
#ifdef Q_COMPILER_RVALUE_REFS
		void setDmAttHash2Alg(QString &&a2);
#endif /* Q_COMPILER_RVALUE_REFS */

	private:
#if (QT_VERSION >= QT_VERSION_CHECK(5, 12, 0))
		::std::unique_ptr<DmAttPrivate> d_ptr;
#else /* < Qt-5.12 */
		QScopedPointer<DmAttPrivate> d_ptr;
#endif /* >= Qt-5.12 */
	};

	void swap(DmAtt &first, DmAtt &second) Q_DECL_NOTHROW;

	class DmExtFilePrivate;
	/*!
	 * @brief Attachment, used for high-volume data messages.
	 */
	class DmExtFile {
		Q_DECLARE_PRIVATE(DmExtFile)

	public:
		DmExtFile(void);
		DmExtFile(const DmExtFile &other);
#ifdef Q_COMPILER_RVALUE_REFS
		DmExtFile(DmExtFile &&other) Q_DECL_NOEXCEPT;
#endif /* Q_COMPILER_RVALUE_REFS */
		~DmExtFile(void);

		DmExtFile &operator=(const DmExtFile &other) Q_DECL_NOTHROW;
#ifdef Q_COMPILER_RVALUE_REFS
		DmExtFile &operator=(DmExtFile &&other) Q_DECL_NOTHROW;
#endif /* Q_COMPILER_RVALUE_REFS */

		bool operator==(const DmExtFile &other) const;
		bool operator!=(const DmExtFile &other) const;

		friend void swap(DmExtFile &first, DmExtFile &second) Q_DECL_NOTHROW;

		bool isNull(void) const;

		/* dmFileMetaType */
		enum Type::FileMetaType fileMetaType(void) const;
		void setFileMetaType(enum Type::FileMetaType t);

		/* Complete attachment identification. */
		const DmAtt &dmAtt(void) const;
		void setDmAtt(const DmAtt &a);
#ifdef Q_COMPILER_RVALUE_REFS
		void setDmAtt(DmAtt &&a);
#endif /* Q_COMPILER_RVALUE_REFS */

		/* dmFileGuid */
		const QString &fileGuid(void) const;
		void setFileGuid(const QString &g);
#ifdef Q_COMPILER_RVALUE_REFS
		void setFileGuid(QString &&g);
#endif /* Q_COMPILER_RVALUE_REFS */

		/* dmUpFileGuid */
		const QString &upFileGuid(void) const;
		void setUpFileGuid(const QString &ug);
#ifdef Q_COMPILER_RVALUE_REFS
		void setUpFileGuid(QString &&ug);
#endif /* Q_COMPILER_RVALUE_REFS */

	private:
#if (QT_VERSION >= QT_VERSION_CHECK(5, 12, 0))
		::std::unique_ptr<DmExtFilePrivate> d_ptr;
#else /* < Qt-5.12 */
		QScopedPointer<DmExtFilePrivate> d_ptr;
#endif /* >= Qt-5.12 */
	};

	void swap(DmExtFile &first, DmExtFile &second) Q_DECL_NOTHROW;

}
