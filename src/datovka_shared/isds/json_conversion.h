/*
 * Copyright (C) 2014-2025 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

#pragma once

#include <QByteArray>

#include "src/datovka_shared/isds/box_interface2.h" /* PersonName2 */
#include "src/datovka_shared/isds/message_interface2.h" /* DmMessageAuthor */

class QJsonValue; /* Forward declaration. */

/*
 * Encapsulates methods needed for JSON data conversion.
 */

namespace Isds {

	namespace Json {

		PersonName2 personName2FromJson(const QByteArray &json, bool *ok = Q_NULLPTR);
		PersonName2 personName2FromJsonVal(const QJsonValue &jsonVal, bool *ok = Q_NULLPTR);

		bool toJsonVal(const PersonName2 &n, QJsonValue &jsonVal);
		QByteArray toJsonData(const PersonName2 &n, bool indent = false);

		DmMessageAuthor dmMessageAuthorFromJson(const QByteArray &json, bool *ok = Q_NULLPTR);
		DmMessageAuthor dmMessageAuthorFromJsonVal(const QJsonValue &jsonVal, bool *ok = Q_NULLPTR);

		bool toJsonVal(const DmMessageAuthor &auth, QJsonValue &jsonVal);
		QByteArray toJsonData(const DmMessageAuthor &auth, bool indent = false);

	}

}
