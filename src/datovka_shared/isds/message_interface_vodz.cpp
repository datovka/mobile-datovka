/*
 * Copyright (C) 2014-2025 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

#include <QByteArray>
#include <QString>
#include <utility> /* ::std::move, ::std::swap */

#include "src/datovka_shared/isds/message_interface_vodz.h"

/* Null objects - for convenience. */
static const QByteArray nullByteArray;
static const QString nullString;
static Isds::DmAtt nullDmAtt;

/*!
 * @brief PIMPL DmFile class.
 */
class Isds::DmFilePrivate {
	//Q_DISABLE_COPY(DmFilePrivate)
public:
	DmFilePrivate(void)
	    : m_binaryContent(), m_fileMetaType(Type::FMT_UNKNOWN), m_mimeType(),
	    m_fileDescr(), m_fileGuid(), m_upFileGuid()
	{ }

	DmFilePrivate &operator=(const DmFilePrivate &other) Q_DECL_NOTHROW
	{
		m_binaryContent = other.m_binaryContent;
		m_fileMetaType = other.m_fileMetaType;
		m_mimeType = other.m_mimeType;
		m_fileDescr = other.m_fileDescr;
		m_fileGuid = other.m_fileGuid;
		m_upFileGuid = other.m_upFileGuid;

		return *this;
	}

	bool operator==(const DmFilePrivate &other) const
	{
		return (m_binaryContent == other.m_binaryContent) &&
		    (m_fileMetaType == other.m_fileMetaType) &&
		    (m_mimeType == other.m_mimeType) &&
		    (m_fileDescr == other.m_fileDescr) &&
		    (m_fileGuid == other.m_fileGuid) &&
		    (m_upFileGuid == other.m_upFileGuid);
	}

	QByteArray m_binaryContent;
	enum Type::FileMetaType m_fileMetaType;
	QString m_mimeType; /* See pril_2/WS_ISDS_Manipulace_s_datovymi_zpravami.pdf appendix 3. */
	QString m_fileDescr; /* Mandatory DmFile name. */
	QString m_fileGuid; /* Optional message-local document identifier. */
	QString m_upFileGuid; /* Optional reference to upper document. */
};

Isds::DmFile::DmFile(void)
    : d_ptr(Q_NULLPTR)
{
}

Isds::DmFile::DmFile(const DmFile &other)
    : d_ptr((other.d_func() != Q_NULLPTR) ? (new (::std::nothrow) DmFilePrivate) : Q_NULLPTR)
{
	Q_D(DmFile);
	if (d == Q_NULLPTR) {
		return;
	}

	*d = *other.d_func();
}

#ifdef Q_COMPILER_RVALUE_REFS
Isds::DmFile::DmFile(DmFile &&other) Q_DECL_NOEXCEPT
#  if (QT_VERSION >= QT_VERSION_CHECK(5, 12, 0))
    : d_ptr(other.d_ptr.release()) //d_ptr(::std::move(other.d_ptr))
#  else /* < Qt-5.12 */
    : d_ptr(other.d_ptr.take())
#  endif /* >= Qt-5.12 */
{
}
#endif /* Q_COMPILER_RVALUE_REFS */

Isds::DmFile::~DmFile(void)
{
}

/*!
 * @brief Ensures DmFilePrivate presence.
 *
 * @note Returns if DmFilePrivate could not be allocated.
 */
#define ensureDmFilePrivate(_x_) \
	do { \
		if (Q_UNLIKELY(d_ptr == Q_NULLPTR)) { \
			DmFilePrivate *p = new (::std::nothrow) DmFilePrivate; \
			if (Q_UNLIKELY(p == Q_NULLPTR)) { \
				Q_ASSERT(0); \
				return _x_; \
			} \
			d_ptr.reset(p); \
		} \
	} while (0)

Isds::DmFile &Isds::DmFile::operator=(const DmFile &other) Q_DECL_NOTHROW
{
	if (other.d_func() == Q_NULLPTR) {
		d_ptr.reset(Q_NULLPTR);
		return *this;
	}
	ensureDmFilePrivate(*this);
	Q_D(DmFile);

	*d = *other.d_func();

	return *this;
}

#ifdef Q_COMPILER_RVALUE_REFS
Isds::DmFile &Isds::DmFile::operator=(DmFile &&other) Q_DECL_NOTHROW
{
	swap(*this, other);
	return *this;
}
#endif /* Q_COMPILER_RVALUE_REFS */

bool Isds::DmFile::operator==(const DmFile &other) const
{
	Q_D(const DmFile);
	if ((d == Q_NULLPTR) && ((other.d_func() == Q_NULLPTR))) {
		return true;
	} else if ((d == Q_NULLPTR) || ((other.d_func() == Q_NULLPTR))) {
		return false;
	}

	return *d == *other.d_func();
}

bool Isds::DmFile::operator!=(const DmFile &other) const
{
	return !operator==(other);
}

bool Isds::DmFile::isNull(void) const
{
	Q_D(const DmFile);
	return d == Q_NULLPTR;
}

const QByteArray &Isds::DmFile::binaryContent(void) const
{
	Q_D(const DmFile);
	if (Q_UNLIKELY(d == Q_NULLPTR)) {
		return nullByteArray;
	}
	return d->m_binaryContent;
}

void Isds::DmFile::setBinaryContent(const QByteArray &bc)
{
	ensureDmFilePrivate();
	Q_D(DmFile);
	d->m_binaryContent = bc;
}

#ifdef Q_COMPILER_RVALUE_REFS
void Isds::DmFile::setBinaryContent(QByteArray &&bc)
{
	ensureDmFilePrivate();
	Q_D(DmFile);
	d->m_binaryContent = ::std::move(bc);
}
#endif /* Q_COMPILER_RVALUE_REFS */

enum Isds::Type::FileMetaType Isds::DmFile::fileMetaType(void) const
{
	Q_D(const DmFile);
	if (Q_UNLIKELY(d == Q_NULLPTR)) {
		return Isds::Type::FMT_UNKNOWN;
	}
	return d->m_fileMetaType;
}

void Isds::DmFile::setFileMetaType(enum Type::FileMetaType mt)
{
	ensureDmFilePrivate();
	Q_D(DmFile);
	d->m_fileMetaType = mt;
}

const QString &Isds::DmFile::mimeType(void) const
{
	Q_D(const DmFile);
	if (Q_UNLIKELY(d == Q_NULLPTR)) {
		return nullString;
	}
	return d->m_mimeType;
}

void Isds::DmFile::setMimeType(const QString &mt)
{
	ensureDmFilePrivate();
	Q_D(DmFile);
	d->m_mimeType = mt;
}

#ifdef Q_COMPILER_RVALUE_REFS
void Isds::DmFile::setMimeType(QString &&mt)
{
	ensureDmFilePrivate();
	Q_D(DmFile);
	d->m_mimeType = ::std::move(mt);
}
#endif /* Q_COMPILER_RVALUE_REFS */

const  QString &Isds::DmFile::fileDescr(void) const
{
	Q_D(const DmFile);
	if (Q_UNLIKELY(d == Q_NULLPTR)) {
		return nullString;
	}
	return d->m_fileDescr;
}

void Isds::DmFile::setFileDescr(const QString &fd)
{
	ensureDmFilePrivate();
	Q_D(DmFile);
	d->m_fileDescr = fd;
}

#ifdef Q_COMPILER_RVALUE_REFS
void Isds::DmFile::setFileDescr(QString &&fd)
{
	ensureDmFilePrivate();
	Q_D(DmFile);
	d->m_fileDescr = ::std::move(fd);
}
#endif /* Q_COMPILER_RVALUE_REFS */

const QString &Isds::DmFile::fileGuid(void) const
{
	Q_D(const DmFile);
	if (Q_UNLIKELY(d == Q_NULLPTR)) {
		return nullString;
	}
	return d->m_fileGuid;
}

void Isds::DmFile::setFileGuid(const QString &g)
{
	ensureDmFilePrivate();
	Q_D(DmFile);
	d->m_fileGuid = g;
}

#ifdef Q_COMPILER_RVALUE_REFS
void Isds::DmFile::setFileGuid(QString &&g)
{
	ensureDmFilePrivate();
	Q_D(DmFile);
	d->m_fileGuid = ::std::move(g);
}
#endif /* Q_COMPILER_RVALUE_REFS */

const QString &Isds::DmFile::upFileGuid(void) const
{
	Q_D(const DmFile);
	if (Q_UNLIKELY(d == Q_NULLPTR)) {
		return nullString;
	}
	return d->m_upFileGuid;
}

void Isds::DmFile::setUpFileGuid(const QString &ug)
{
	ensureDmFilePrivate();
	Q_D(DmFile);
	d->m_upFileGuid = ug;
}

#ifdef Q_COMPILER_RVALUE_REFS
void Isds::DmFile::setUpFileGuid(QString &&ug)
{
	ensureDmFilePrivate();
	Q_D(DmFile);
	d->m_upFileGuid = ::std::move(ug);
}
#endif /* Q_COMPILER_RVALUE_REFS */

void Isds::swap(DmFile &first, DmFile &second) Q_DECL_NOTHROW
{
	using ::std::swap;
	swap(first.d_ptr, second.d_ptr);
}

/*!
 * @brief PIMPL DmAtt class.
 */
class Isds::DmAttPrivate {
	//Q_DISABLE_COPY(DmAttPrivate)
public:
	DmAttPrivate(void)
	    : m_dmAttID(), m_dmAttHash1(), m_dmAttHash1Alg(), m_dmAttHash2(),
	    m_dmAttHash2Alg()
	{ }

	DmAttPrivate &operator=(const DmAttPrivate &other) Q_DECL_NOTHROW
	{
		m_dmAttID = other.m_dmAttID;
		m_dmAttHash1 = other.m_dmAttHash1;
		m_dmAttHash1Alg = other.m_dmAttHash1Alg;
		m_dmAttHash2 = other.m_dmAttHash2;
		m_dmAttHash2Alg = other.m_dmAttHash2Alg;

		return *this;
	}

	bool operator==(const DmAttPrivate &other) const
	{
		return (m_dmAttID == other.m_dmAttID) &&
		    (m_dmAttHash1 == other.m_dmAttHash1) &&
		    (m_dmAttHash1Alg == other.m_dmAttHash1Alg) &&
		    (m_dmAttHash2 == other.m_dmAttHash2) &&
		    (m_dmAttHash2Alg == other.m_dmAttHash2Alg);
	}

	QString m_dmAttID; /* Attachment identifier, nothing to do with message identifier. */
	QString m_dmAttHash1;
	QString m_dmAttHash1Alg;
	QString m_dmAttHash2;
	QString m_dmAttHash2Alg;
};

Isds::DmAtt::DmAtt(void)
    : d_ptr(Q_NULLPTR)
{
}

Isds::DmAtt::DmAtt(const DmAtt &other)
    : d_ptr((other.d_func() != Q_NULLPTR) ? (new (::std::nothrow) DmAttPrivate) : Q_NULLPTR)
{
	Q_D(DmAtt);
	if (d == Q_NULLPTR) {
		return;
	}

	*d = *other.d_func();
}

#ifdef Q_COMPILER_RVALUE_REFS
Isds::DmAtt::DmAtt(DmAtt &&other) Q_DECL_NOEXCEPT
#  if (QT_VERSION >= QT_VERSION_CHECK(5, 12, 0))
    : d_ptr(other.d_ptr.release()) //d_ptr(::std::move(other.d_ptr))
#  else /* < Qt-5.12 */
    : d_ptr(other.d_ptr.take())
#  endif /* >= Qt-5.12 */
{
}
#endif /* Q_COMPILER_RVALUE_REFS */

Isds::DmAtt::~DmAtt(void)
{
}

/*!
 * @brief Ensures DmAttPrivate presence.
 *
 * @note Returns if DmAttPrivate could not be allocated.
 */
#define ensureDmAttPrivate(_x_) \
	do { \
		if (Q_UNLIKELY(d_ptr == Q_NULLPTR)) { \
			DmAttPrivate *p = new (::std::nothrow) DmAttPrivate; \
			if (Q_UNLIKELY(p == Q_NULLPTR)) { \
				Q_ASSERT(0); \
				return _x_; \
			} \
			d_ptr.reset(p); \
		} \
	} while (0)

Isds::DmAtt &Isds::DmAtt::operator=(const DmAtt &other) Q_DECL_NOTHROW
{
	if (other.d_func() == Q_NULLPTR) {
		d_ptr.reset(Q_NULLPTR);
		return *this;
	}
	ensureDmAttPrivate(*this);
	Q_D(DmAtt);

	*d = *other.d_func();

	return *this;
}

#ifdef Q_COMPILER_RVALUE_REFS
Isds::DmAtt &Isds::DmAtt::operator=(DmAtt &&other) Q_DECL_NOTHROW
{
	swap(*this, other);
	return *this;
}
#endif /* Q_COMPILER_RVALUE_REFS */

bool Isds::DmAtt::operator==(const DmAtt &other) const
{
	Q_D(const DmAtt);
	if ((d == Q_NULLPTR) && ((other.d_func() == Q_NULLPTR))) {
		return true;
	} else if ((d == Q_NULLPTR) || ((other.d_func() == Q_NULLPTR))) {
		return false;
	}

	return *d == *other.d_func();
}

bool Isds::DmAtt::operator!=(const DmAtt &other) const
{
	return !operator==(other);
}

bool Isds::DmAtt::isNull(void) const
{
	Q_D(const DmAtt);
	return d == Q_NULLPTR;
}

const QString &Isds::DmAtt::dmAttID(void) const
{
	Q_D(const DmAtt);
	if (Q_UNLIKELY(d == Q_NULLPTR)) {
		return nullString;
	}
	return d->m_dmAttID;
}

void Isds::DmAtt::setDmAttID(const QString &ai)
{
	ensureDmAttPrivate();
	Q_D(DmAtt);
	d->m_dmAttID = ai;
}

#ifdef Q_COMPILER_RVALUE_REFS
void Isds::DmAtt::setDmAttID(QString &&ai)
{
	ensureDmAttPrivate();
	Q_D(DmAtt);
	d->m_dmAttID = ::std::move(ai);
}
#endif /* Q_COMPILER_RVALUE_REFS */

const QString &Isds::DmAtt::dmAttHash1(void) const
{
	Q_D(const DmAtt);
	if (Q_UNLIKELY(d == Q_NULLPTR)) {
		return nullString;
	}
	return d->m_dmAttHash1;
}

void Isds::DmAtt::setDmAttHash1(const QString &h1)
{
	ensureDmAttPrivate();
	Q_D(DmAtt);
	d->m_dmAttHash1 = h1;
}

#ifdef Q_COMPILER_RVALUE_REFS
void Isds::DmAtt::setDmAttHash1(QString &&h1)
{
	ensureDmAttPrivate();
	Q_D(DmAtt);
	d->m_dmAttHash1 = ::std::move(h1);
}
#endif /* Q_COMPILER_RVALUE_REFS */

const QString &Isds::DmAtt::dmAttHash1Alg(void) const
{
	Q_D(const DmAtt);
	if (Q_UNLIKELY(d == Q_NULLPTR)) {
		return nullString;
	}
	return d->m_dmAttHash1Alg;
}

void Isds::DmAtt::setDmAttHash1Alg(const QString &a1)
{
	ensureDmAttPrivate();
	Q_D(DmAtt);
	d->m_dmAttHash1Alg = a1;
}

#ifdef Q_COMPILER_RVALUE_REFS
void Isds::DmAtt::setDmAttHash1Alg(QString &&a1)
{
	ensureDmAttPrivate();
	Q_D(DmAtt);
	d->m_dmAttHash1Alg = ::std::move(a1);
}
#endif /* Q_COMPILER_RVALUE_REFS */

const QString &Isds::DmAtt::dmAttHash2(void) const
{
	Q_D(const DmAtt);
	if (Q_UNLIKELY(d == Q_NULLPTR)) {
		return nullString;
	}
	return d->m_dmAttHash2;
}

void Isds::DmAtt::setDmAttHash2(const QString &h2)
{
	ensureDmAttPrivate();
	Q_D(DmAtt);
	d->m_dmAttHash2 = h2;
}

#ifdef Q_COMPILER_RVALUE_REFS
void Isds::DmAtt::setDmAttHash2(QString &&h2)
{
	ensureDmAttPrivate();
	Q_D(DmAtt);
	d->m_dmAttHash2 = ::std::move(h2);
}
#endif /* Q_COMPILER_RVALUE_REFS */

const QString &Isds::DmAtt::dmAttHash2Alg(void) const
{
	Q_D(const DmAtt);
	if (Q_UNLIKELY(d == Q_NULLPTR)) {
		return nullString;
	}
	return d->m_dmAttHash2Alg;
}

void Isds::DmAtt::setDmAttHash2Alg(const QString &a2)
{
	ensureDmAttPrivate();
	Q_D(DmAtt);
	d->m_dmAttHash2Alg = a2;
}

#ifdef Q_COMPILER_RVALUE_REFS
void Isds::DmAtt::setDmAttHash2Alg(QString &&a2)
{
	ensureDmAttPrivate();
	Q_D(DmAtt);
	d->m_dmAttHash2Alg = ::std::move(a2);
}
#endif /* Q_COMPILER_RVALUE_REFS */

void Isds::swap(DmAtt &first, DmAtt &second) Q_DECL_NOTHROW
{
	using ::std::swap;
	swap(first.d_ptr, second.d_ptr);
}

/*!
 * @brief PIMPL DmExtFile class.
 */
class Isds::DmExtFilePrivate {
	//Q_DISABLE_COPY(DmExtFilePrivate)
public:
	DmExtFilePrivate(void)
	    : m_fileMetaType(Type::FMT_UNKNOWN), m_dmAtt(), m_fileGuid(),
	    m_upFileGuid()
	{ }

	DmExtFilePrivate &operator=(const DmExtFilePrivate &other) Q_DECL_NOTHROW
	{
		m_fileMetaType = other.m_fileMetaType;
		m_dmAtt = other.m_dmAtt;
		m_fileGuid = other.m_fileGuid;
		m_upFileGuid = other.m_upFileGuid;

		return *this;
	}

	bool operator==(const DmExtFilePrivate &other) const
	{
		return (m_fileMetaType == other.m_fileMetaType) &&
		    (m_dmAtt == other.m_dmAtt) &&
		    (m_fileGuid == other.m_fileGuid) &&
		    (m_upFileGuid == other.m_upFileGuid);
	}

	enum Type::FileMetaType m_fileMetaType;
	DmAtt m_dmAtt;
	QString m_fileGuid; /* Optional message-local document identifier. */
	QString m_upFileGuid; /* Optional reference to upper document. */
};

Isds::DmExtFile::DmExtFile(void)
    : d_ptr(Q_NULLPTR)
{
}

Isds::DmExtFile::DmExtFile(const DmExtFile &other)
    : d_ptr((other.d_func() != Q_NULLPTR) ? (new (::std::nothrow) DmExtFilePrivate) : Q_NULLPTR)
{
	Q_D(DmExtFile);
	if (d == Q_NULLPTR) {
		return;
	}

	*d = *other.d_func();
}

#ifdef Q_COMPILER_RVALUE_REFS
Isds::DmExtFile::DmExtFile(DmExtFile &&other) Q_DECL_NOEXCEPT
#  if (QT_VERSION >= QT_VERSION_CHECK(5, 12, 0))
    : d_ptr(other.d_ptr.release()) //d_ptr(::std::move(other.d_ptr))
#  else /* < Qt-5.12 */
    : d_ptr(other.d_ptr.take())
#  endif /* >= Qt-5.12 */
{
}
#endif /* Q_COMPILER_RVALUE_REFS */

Isds::DmExtFile::~DmExtFile(void)
{
}

/*!
 * @brief Ensures DmExtFilePrivate presence.
 *
 * @note Returns if DmExtFilePrivate could not be allocated.
 */
#define ensureDmExtFilePrivate(_x_) \
	do { \
		if (Q_UNLIKELY(d_ptr == Q_NULLPTR)) { \
			DmExtFilePrivate *p = new (::std::nothrow) DmExtFilePrivate; \
			if (Q_UNLIKELY(p == Q_NULLPTR)) { \
				Q_ASSERT(0); \
				return _x_; \
			} \
			d_ptr.reset(p); \
		} \
	} while (0)

Isds::DmExtFile &Isds::DmExtFile::operator=(const DmExtFile &other) Q_DECL_NOTHROW
{
	if (other.d_func() == Q_NULLPTR) {
		d_ptr.reset(Q_NULLPTR);
		return *this;
	}
	ensureDmExtFilePrivate(*this);
	Q_D(DmExtFile);

	*d = *other.d_func();

	return *this;
}

#ifdef Q_COMPILER_RVALUE_REFS
Isds::DmExtFile &Isds::DmExtFile::operator=(DmExtFile &&other) Q_DECL_NOTHROW
{
	swap(*this, other);
	return *this;
}
#endif /* Q_COMPILER_RVALUE_REFS */

bool Isds::DmExtFile::operator==(const DmExtFile &other) const
{
	Q_D(const DmExtFile);
	if ((d == Q_NULLPTR) && ((other.d_func() == Q_NULLPTR))) {
		return true;
	} else if ((d == Q_NULLPTR) || ((other.d_func() == Q_NULLPTR))) {
		return false;
	}

	return *d == *other.d_func();
}

bool Isds::DmExtFile::operator!=(const DmExtFile &other) const
{
	return !operator==(other);
}

bool Isds::DmExtFile::isNull(void) const
{
	Q_D(const DmExtFile);
	return d == Q_NULLPTR;
}

enum Isds::Type::FileMetaType Isds::DmExtFile::fileMetaType(void) const
{
	Q_D(const DmExtFile);
	if (Q_UNLIKELY(d == Q_NULLPTR)) {
		return Isds::Type::FMT_UNKNOWN;
	}
	return d->m_fileMetaType;
}

void Isds::DmExtFile::setFileMetaType(enum Type::FileMetaType t)
{
	ensureDmExtFilePrivate();
	Q_D(DmExtFile);
	d->m_fileMetaType = t;
}

const Isds::DmAtt &Isds::DmExtFile::dmAtt(void) const
{
	Q_D(const DmExtFile);
	if (Q_UNLIKELY(d == Q_NULLPTR)) {
		return nullDmAtt;
	}
	return d->m_dmAtt;
}

void Isds::DmExtFile::setDmAtt(const DmAtt &a)
{
	ensureDmExtFilePrivate();
	Q_D(DmExtFile);
	d->m_dmAtt = a;
}

#ifdef Q_COMPILER_RVALUE_REFS
void Isds::DmExtFile::setDmAtt(DmAtt &&a)
{
	ensureDmExtFilePrivate();
	Q_D(DmExtFile);
	d->m_dmAtt = ::std::move(a);
}
#endif /* Q_COMPILER_RVALUE_REFS */

const QString &Isds::DmExtFile::fileGuid(void) const
{
	Q_D(const DmExtFile);
	if (Q_UNLIKELY(d == Q_NULLPTR)) {
		return nullString;
	}
	return d->m_fileGuid;
}

void Isds::DmExtFile::setFileGuid(const QString &g)
{
	ensureDmExtFilePrivate();
	Q_D(DmExtFile);
	d->m_fileGuid = g;
}

#ifdef Q_COMPILER_RVALUE_REFS
void Isds::DmExtFile::setFileGuid(QString &&g)
{
	ensureDmExtFilePrivate();
	Q_D(DmExtFile);
	d->m_fileGuid = ::std::move(g);
}
#endif /* Q_COMPILER_RVALUE_REFS */

const QString &Isds::DmExtFile::upFileGuid(void) const
{
	Q_D(const DmExtFile);
	if (Q_UNLIKELY(d == Q_NULLPTR)) {
		return nullString;
	}
	return d->m_upFileGuid;
}

void Isds::DmExtFile::setUpFileGuid(const QString &ug)
{
	ensureDmExtFilePrivate();
	Q_D(DmExtFile);
	d->m_upFileGuid = ug;
}

#ifdef Q_COMPILER_RVALUE_REFS
void Isds::DmExtFile::setUpFileGuid(QString &&ug)
{
	ensureDmExtFilePrivate();
	Q_D(DmExtFile);
	d->m_upFileGuid = ::std::move(ug);
}
#endif /* Q_COMPILER_RVALUE_REFS */

void Isds::swap(DmExtFile &first, DmExtFile &second) Q_DECL_NOTHROW
{
	using ::std::swap;
	swap(first.d_ptr, second.d_ptr);
}
