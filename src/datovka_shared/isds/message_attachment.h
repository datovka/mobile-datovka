/*
 * Copyright (C) 2014-2025 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

#pragma once

#include <QSet>
#include <QString>

namespace Isds {

	/*
	 * Provides namespace for some convenience methods related to message
	 * attachments.
	 *
	 * @note For a list of allowed file suffixes see
	 *     pril_2/WS_manipulace_s_datovymi_zpravami.pdf
	 *     section 3.
	 */

	namespace Attachment {

		/*!
		 * @brief Get set of allowed file suffixes.
		 *
		 * @return A set of allowed file suffixes in lower case
		 *     (eg. odp, pdf, ...).
		 */
		const QSet<QString> &allowedFileSuffixes(void);

		/*!
		 * @brief Get set of expected MIME names for given file suffix.
		 *
		 * @param[in] suffix File suffix.
		 * @return A set of allowed expected MIME types for a suffix.
		 */
		const QSet<QString> expectedMimeTypeNamesForSuffix(const QString &suffix);

		/*!
		 * @brief Suggest a MIME name for given file suffix.
		 *
		 * @param[in] suffix File suffix.
		 * @return MIME name, null string in suffix unknown.
		 */
		const QString suggestedMimeTypeName(const QString &suffix);

	}

}
