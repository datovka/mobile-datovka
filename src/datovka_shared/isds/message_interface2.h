/*
 * Copyright (C) 2014-2025 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

#pragma once

#if (QT_VERSION >= QT_VERSION_CHECK(5, 12, 0))
#  include <memory> /* ::std::unique_ptr */
#else /* < Qt-5.12 */
#  include <QScopedPointer>
#endif /* >= Qt-5.12 */

#include "src/datovka_shared/isds/types.h"

namespace Isds {
	class PersonName2; /* Forward declaration. */
}
class QDate; /* Forward declaration. */
class QString; /* Forward declaration. */

/*
 * Structures originating from pril_2/WS_ISDS_Manipulace_s_datovymi_zpravami.pdf.
 */

namespace Isds {

	class DmMessageAuthorPrivate;
	/*!
	 * @brief Described in dmBaseTypes.xsd as group dmMessageAuthor
	 *     pril_2/WS_ISDS_Manipulace_s_datovymi_zpravami.pdf
	 *     section 2.10 (GetMessageAuthor2)
	 */
	class DmMessageAuthor {
		Q_DECLARE_PRIVATE(DmMessageAuthor)

	public:
		DmMessageAuthor(void);
		DmMessageAuthor(const DmMessageAuthor &other);
#ifdef Q_COMPILER_RVALUE_REFS
		DmMessageAuthor(DmMessageAuthor &&other) Q_DECL_NOEXCEPT;
#endif /* Q_COMPILER_RVALUE_REFS */
		~DmMessageAuthor(void);

		DmMessageAuthor &operator=(const DmMessageAuthor &other) Q_DECL_NOTHROW;
#ifdef Q_COMPILER_RVALUE_REFS
		DmMessageAuthor &operator=(DmMessageAuthor &&other) Q_DECL_NOTHROW;
#endif /* Q_COMPILER_RVALUE_REFS */

		bool operator==(const DmMessageAuthor &other) const;
		bool operator!=(const DmMessageAuthor &other) const;

		friend void swap(DmMessageAuthor &first, DmMessageAuthor &second) Q_DECL_NOTHROW;

		bool isNull(void) const;

		/* userType -- roughly specified sender type */
		enum Type::SenderType userType(void) const;
		void setUserType(enum Type::SenderType ut);
		/* pnGivenNames, pnLastName */
		const PersonName2 &personName(void) const;
		void setPersonName(const PersonName2 &pn);
#ifdef Q_COMPILER_RVALUE_REFS
		void setPersonName(PersonName2 &&pn);
#endif /* Q_COMPILER_RVALUE_REFS */
		/* biDate */
		const QDate &biDate(void) const;
		void setBiDate(const QDate &bd);
#ifdef Q_COMPILER_RVALUE_REFS
		void setBiDate(QDate &&bd);
#endif /* Q_COMPILER_RVALUE_REFS */
		/* biCity */
		const QString &biCity(void) const;
		void setBiCity(const QString &bc);
#ifdef Q_COMPILER_RVALUE_REFS
		void setBiCity(QString &&bc);
#endif /* Q_COMPILER_RVALUE_REFS */
		/* biCounty */
		const QString &biCounty(void) const;
		void setBiCounty(const QString &bi);
#ifdef Q_COMPILER_RVALUE_REFS
		void setBiCounty(QString &&bi);
#endif /* Q_COMPILER_RVALUE_REFS */
		/* adCode */
		const QString &adCode(void) const;
		void setAdCode(const QString &ac);
#ifdef Q_COMPILER_RVALUE_REFS
		void setAdCode(QString &&ac);
#endif /* Q_COMPILER_RVALUE_REFS */
		/* fullAddress */
		const QString &fullAddress(void) const;
		void setFullAddress(const QString &fa);
#ifdef Q_COMPILER_RVALUE_REFS
		void setFullAddress(QString &&fa);
#endif /* Q_COMPILER_RVALUE_REFS */
		/* robIdent */
		enum Type::NilBool robIdent(void) const;
		void setRobIdent(enum Type::NilBool ri);

	private:
#if (QT_VERSION >= QT_VERSION_CHECK(5, 12, 0))
		::std::unique_ptr<DmMessageAuthorPrivate> d_ptr;
#else /* < Qt-5.12 */
		QScopedPointer<DmMessageAuthorPrivate> d_ptr;
#endif /* >= Qt-5.12 */
	};

	void swap(DmMessageAuthor &first, DmMessageAuthor &second) Q_DECL_NOTHROW;

}
