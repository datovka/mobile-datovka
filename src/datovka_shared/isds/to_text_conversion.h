/*
 * Copyright (C) 2014-2025 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

#pragma once

#include <QList>
#include <QPair>
#include <QString>

#include "src/datovka_shared/compat_qt/misc.h"

namespace Isds {

	/* Forward class declaration. */
	class DmMessageAuthor;
	class PersonName2;

	Q_DECLARE_NAMESPACE_TR(Isds)

	QString textFullName(const PersonName2 &personName);

	/*!
	 * @brief Build list of pairs containing entry name + entry value.
	 *
	 * @param[in] messageAuthor Message author.
	 * @return List of string name--value pairs.
	 */
	QList< QPair<QString, QString> > textListMessageAuthor(
	    const DmMessageAuthor &messageAuthor);

}
