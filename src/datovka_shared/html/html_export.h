/*
 * Copyright (C) 2014-2025 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

#pragma once

#include <QCoreApplication> /* Q_DECLARE_TR_FUNCTIONS */
#include <QList>
#include <QPair>
#include <QString>
#include <QStringList>

/* Macro defines margins of HTML info. */
#define htmlMarginsApp(delInfo, envelope) \
	(QLatin1String("<p style=\"margin-left:10px;\">") + \
	(delInfo) + (envelope) + (QLatin1String("</p>")))
/* HTML macros for application UI */
#define htmlSectionLabel(title) \
	(QLatin1String("<h3>") + (title) + QLatin1String("</h3>"))
#define strongInfoLine(title, value) \
	(QLatin1String("<div><strong>") + (title) + ": </strong>" + (value) + \
	"</div>")

/* Forward declaration. */
class QByteArray;
namespace Isds {
	class Document;
	class Envelope;
	class DbOwnerInfoExt2;
	class DbUserInfoExt2;
}

namespace Html {

	/*!
	 * @brief Contains description of used types.
	 */
	class Export {
		Q_DECLARE_TR_FUNCTIONS(Export)

	private:
		/*!
		 * @brief Private constructor.
		 */
		Export(void);

	public:
		/*!
		 * @brief Type of export.
		 */
		enum ExportType {
			TYPE_DELIVERY_INFO = 0, /*!< Delivery info. */
			TYPE_ENVELOPE_INFO /*!< Message envelope info. */
		};

		/*!
		 * @brief HTML envelope or delivery info for PDF export.
		 *
		 * @param[in] type Export type.
		 * @param[in] envelope Message envelope.
		 * @param[in] attachments Message attachment list.
		 * @param[in] msgAuthorJsonStr Message author JSON data.
		 * @return HTML envelope or delivery info for PDF export.
		 */
		static
		QString htmlInfoPdf(ExportType type,
		    const Isds::Envelope &envelope,
		    const QList<Isds::Document> &attachments,
		    const QByteArray &msgAuthorJsonStr);

		/*!
		 * @brief HTML delivery info for application UI.
		 *
		 * @param[in] envelope Message envelope.
		 * @param[in] msgAuthorJsonStr Message author JSON data.
		 * @return HTML delivery info for application UI.
		 */
		static
		QString htmlDeliveryInfoApp(const Isds::Envelope &envelope,
		    const QByteArray &msgAuthorJsonStr);

		/*!
		 * @brief HTML message envelope info for application UI.
		 *
		 * @param[in] envelope Message envelope.
		 * @param[in] msgAuthorJsonStr Message author JSON data.
		 * @param[in] rmPathList List of records management path.
		 * @param[in] isReceived True if message is received.
		 * @param[in] shortVersion True if return short envelope info.
		 * @param[in] appendEvents True if return message info with events.
		 * @return HTML message envelope info for application UI.
		 */
		static
		QString htmlMessageInfoApp(const Isds::Envelope &envelope,
		    const QByteArray &msgAuthorJsonStr,
		    const QStringList &rmPathList, bool isReceived,
		    bool shortVersion, bool appendEvents);

		/*!
		 * @brief HTML short message envelope info for application UI.
		 *
		 * @param[in] envelope Message envelope.
		 * @param[in] isReceived True if message is received.
		 * @return HTML short message envelope info for application UI.
		 */
		static
		QString htmlShortMessageInfoApp(const Isds::Envelope &envelope,
		    bool isReceived);

		/*!
		 * @brief HTML databox owner info for application UI.
		 *
		 * @param[in] dbOwnerInfo Owner info struct.
		 * @return HTML databox owner info string for application UI.
		 */
		static
		QString htmlDataboxOwnerInfoApp(
		    const Isds::DbOwnerInfoExt2 &dbOwnerInfo);

		/*!
		 * @brief HTML databox user info for application UI.
		 *
		 * @param[in] dbUserInfo User info struct.
		 * @return HTML databox user info string for application UI.
		 */
		static
		QString htmlDataboxUserInfoApp(
		    const Isds::DbUserInfoExt2 &dbUserInfo);

		/*!
		 * @brief HTML password expiration date for application UI.
		 *
		 * @param[in] dateTime Password expiration datetime.
		 * @return HTML password expiration date and time.
		 */
		static
		QString htmlPwdExpirDateInfoApp(const QString &dateTime);

		/*!
		 * @brief HTML database file path list for application UI.
		 *
		 * @param[in] dbFiles List of database files.
		 * @return HTML list of database files.
		 */
		static
		QString htmlMsgDbLocationInfoApp(const QStringList &dbFiles);

		/*!
		 * @brief Generate HTML message counts info.
		 *
		 * @param[in] title Title of section.
		 * @param[in] msgCnts List of message counts.
		 * @return HTML formatted message counts.
		 */
		static
		QString htmlMessageInfoApp(const QString &title,
		    const QList< QPair<QString, int> > &msgCnts);

		/*!
		 * @brief HTML overall message counts info.
		 *
		 * @param[in] recMsgCnts Number of received messages.
		 * @param[in] sentMsgCnts Number of sent messages.
		 * @return HTML formatted message counts summary.
		 */
		static
		QString htmlAllMessagesInfoApp(
		    const QList< QPair<QString, int> > &recMsgCnts,
		    const QList< QPair<QString, int> > &sentMsgCnts);

		/*!
		 * @brief HTML signature and time stamp verification info.
		 *
		 * @param[in] sigResStr Signature verification result info.
		 * @param[in] certResStr Certificate verification result info.
		 * @param[in] tsResStr Time stamp verification result info.
		 * @return HTML formatted signature summary.
		 */
		static
		QString htmlSignatureInfoApp(const QString &sigResStr,
		    const QString &certResStr, const QString &tsResStr);
	};

}
