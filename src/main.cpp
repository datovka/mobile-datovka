/*
 * Copyright (C) 2014-2025 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

#include <QApplication>
#include <QCommandLineParser>
#include <QFont>
#include <QQmlApplicationEngine>
#include <QQuickStyle>
#include <QSettings>
#include <QSslCertificate>
#include <QSslConfiguration>
#include <QSslSocket>
#include <QTranslator>
#include <QtGlobal> /* qInstallMessageHandler() */
#include <QtQuick>

#include "src/auxiliaries/ios_helper.h"
#include "src/backup_zip.h"
#include "src/datovka_shared/compat_qt/random.h"
#include "src/datovka_shared/crypto/crypto_trusted_certs.h"
#include "src/datovka_shared/crypto/crypto_version.h"
#include "src/datovka_shared/identifiers/account_id.h"
#include "src/datovka_shared/io/db_tables.h"
#include "src/datovka_shared/io/records_management_db.h"
#include "src/datovka_shared/localisation/localisation.h"
#include "src/datovka_shared/log/log.h"
#include "src/datovka_shared/log/memory_log.h"
#include "src/datovka_shared/settings/pin.h"
#include "src/datovka_shared/settings/prefs.h"
#include "src/datovka_shared/settings/records_management.h"
#include "src/datovka_shared/worker/pool.h"
#include "src/files.h"
#include "src/font/font.h"
#include "src/global.h"
#include "src/gov_services/models/gov_form_list_model.h"
#include "src/gov_services/models/gov_service_list_model.h"
#include "src/gov_wrapper.h"
#include "src/initialisation.h"
#include "src/io/filesystem.h"
#include "src/isds/session/isds_sessions.h"
#include "src/locker.h"
#include "src/log.h"
#include "src/isds/isds_wrapper.h"
#include "src/models/accountmodel.h"
#include "src/models/backup_selection_model.h"
#include "src/models/databoxmodel.h"
#include "src/models/filemodel.h"
#include "src/models/list_sort_filter_proxy_model.h"
#include "src/models/messagemodel.h"
#include "src/models/prefs_model.h"
#if defined(Q_OS_ANDROID)
    #include "android_qt6/src/android_io.h"
#endif /* defined(Q_OS_ANDROID) */
#include "src/qml_identifiers/qml_account_id.h"
#include "src/qml_interaction/image_provider.h"
#include "src/qml_interaction/interaction_filesystem.h"
#include "src/qml_interaction/interaction_zfo_file.h"
#include "src/qml_interaction/string_manipulation.h"
#include "src/qml_isds/message_interface.h"
#include "src/records_management/models/upload_hierarchy_list_model.h"
#include "src/records_management/models/upload_hierarchy_qml_proxy_model.h"
#include "src/settings/account_logos.h"
#include "src/settings/accounts.h"
#include "src/settings/convert_for_compatibility.h"
#include "src/settings/convert_to_prefs.h"
#include "src/settings/ini_preferences.h"
#include "src/settings/ini_preferences_watcher.h"
#include "src/settings/prefs_defaults.h"
#include "src/setwrapper.h"
#include "src/sqlite/account_db.h"
#include "src/sqlite/file_db_container.h"
#include "src/sqlite/message_db_container.h"
#include "src/sqlite/zfo_db.h"
#include "src/worker/emitter.h"
#include "src/wrap_accounts.h"
#include "src/wrap_records_management.h"
#include "src/zfo.h"


/* iOS app_delegate - for interaction with iOS action Open in... */
#if defined Q_OS_IOS
#  include "ios/src/qt_app_delegate.h"
#endif

/* ACCOUNT DB filename */
#define ACNT_DB_NAME "accounts.db"

/* APPLICATION IDENTIFICATION */
#define APP_ORG_NAME "CZ.NIC, z.s.p.o."
#define APP_ORG_DOMAIN "cz.nic"

#define RECORDS_MANAGEMENT_DB_FILE "records_management.db"
#define PREFS_DB_FILE "mobile_prefs.db"

/* namespace for QML registered objects */
const char *uri = "cz.nic.mobileDatovka"; /* Pages and components. */

/*!
 * @brief Used when registering types.
 */
struct QmlTypeEntry {
	const char *typeName;
	int major;
	int minor;
};

#define QML_PAGE_LOC "qrc:/qml/pages"
/*!
 * @brief NULL-terminated list of pages.
 */
static
const struct QmlTypeEntry qmlPages[] = {
	{ "PageAboutApp", 1, 0 },
	{ "PageAccountDetail", 1, 0 },
	{ "PageAccountList", 1, 0 },
	{ "PageAccountLogo", 1, 0 },
	{ "PageBackupData", 1, 0 },
	{ "PageChangePassword", 1, 0 },
	{ "PageContactList", 1, 0 },
	{ "PageConvertDatabase", 1, 0 },
	{ "PageDataboxDetail", 1, 0 },
	{ "PageDataboxSearch", 1, 0 },
	{ "PageGovService", 1, 0 },
	{ "PageGovServiceList", 1, 0 },
	{ "PageImportMessage", 1, 0 },
	{ "PageLog", 1, 0 },
	{ "PagePrefsEditor", 1, 0 },
	{ "PageMessageDetail", 1, 0 },
	{ "PageMessageEnvelope", 1, 0 },
	{ "PageMessageList", 1, 0 },
	{ "PageMessageListSwipeView", 1, 0 },
	{ "PageMessageSearch", 1, 0 },
	{ "PageRecordsManagementUpload", 1, 0 },
	{ "PageRepairDatabase", 1, 0 },
	{ "PageRestoreData", 1, 0 },
	{ "PageSendMessage", 1, 0 },
	{ "PageSettingsAccount", 1, 0 },
	{ "PageSettingsGeneral", 1, 0 },
	{ "PageSettingsPin", 1, 0 },
	{ "PageSettingsRecordsManagement", 1, 0 },
	{ "PageSettingsSync", 1, 0 },
	{ "PageTransferData", 1, 0 },
	{ "PageWelcome", 1, 0 },
	{ NULL, 0, 0 }
};

#define QML_COMPONENT_LOC "qrc:/qml/components"
/*!
 * @brief NULL-terminated list of components.
 */
static
const struct QmlTypeEntry qmlComponents[] = {
	{ "AccessibleButton", 1, 0 },
	{ "AccessibleComboBox", 1, 0 },
	{ "AccessibleImageButton", 1, 0 },
	{ "AccessibleLabel", 1, 0 },
	{ "AccessibleLabelDark", 1, 0 },
	{ "AccessibleRoundButton", 1, 0 },
	{ "AccessibleSpinBox", 1, 0 },
	{ "AccessibleSpinBoxZeroMax", 1, 0 },
	{ "AccessibleSwitch", 1, 0 },
	{ "AccessibleSwitchInfo", 1, 0 },
	{ "AccessibleText", 1, 0 },
	{ "AccessibleTextButton", 1, 0 },
	{ "AccessibleTextField", 1, 0 },
	{ "AccessibleTextHelp", 1, 0 },
	{ "AccessibleTextInfo", 1, 0 },
	{ "AccessibleTextInfoSmall", 1, 0 },
	{ "AccessibleToolButton", 1, 0 },
	{ "AccountList", 1, 0 },
	{ "ControlGroupItem", 1, 0 },
	{ "DataboxList", 1, 0 },
	{ "FileDialogue", 1, 0 },
	{ "FilterBar", 1, 0 },
	{ "GovFormList", 1, 0 },
	{ "GovServiceList", 1, 0 },
	{ "ChangeLogBox", 1, 0 },
	{ "MessageBox", 1, 0 },
	{ "MessageList", 1, 0 },
	{ "PageHeader", 1, 0 },
	{ "Pictogram", 1, 0 },
	{ "PopupItem", 1, 0 },
	{ "ScrollableListView", 1, 0 },
	{ "SeparatorLine", 1, 0 },
	{ "ShowPasswordOverlaidImage", 1, 0 },
	{ "TextLineItem", 1, 0 },
	{ "TimedPasswordLine", 1, 0 },
	{ NULL, 0, 0 }
};

#define QML_DIALOGUES "qrc:/qml/dialogues"
/*!
 * @brief NULL-terminated list of components.
 */
static
const struct QmlTypeEntry qmlDialogues[] = {
	{ "InputDialogue", 1, 0},
	{ "YesNoDialogue", 1, 0},
	{ NULL, 0, 0 }
};

#define QML_WIZARDS "qrc:/qml/wizards"
/*!
 * @brief NULL-terminated list of components.
 */
static
const struct QmlTypeEntry qmlWizards[] = {
	{ "CreateAccountLoader", 1, 0 },
	{ "CreateAccountPage1", 1, 0 },
	{ "CreateAccountPage2", 1, 0},
	{ "CreateAccountPage3", 1, 0},
	{ "CreateAccountPage4", 1, 0 },
	{ NULL, 0, 0 }
};

#define QML_POPUPS "qrc:/qml/popups"
/*!
 * @brief NULL-terminated list of components.
 */
static
const struct QmlTypeEntry qmlPopups[] = {
	{ "DrawerMenuDatovka", 1, 0 },
	{ "PopupMenuAccount", 1, 0 },
	{ "PopupMenuMessage", 1, 0 },
	{ "PopupMenuMessageDetail", 1, 0 },
	{ "PopupMenuMessageList", 1, 0 },
	{ NULL, 0, 0 }
};

/*!
 * @brief Initialises command line parser.
 *
 * @param[in,out] parser Command line parser object to be initialised.
 * @return 0 on success, -1 on failure
 */
static
int setupCmdLineParser(QCommandLineParser &parser)
{
	parser.setApplicationDescription(QObject::tr("Data box application"));
	parser.addHelpOption();
	parser.addVersionOption();

	parser.addPositionalArgument("[zfo-file]",
	    QObject::tr("ZFO file to be viewed."));

	return 0;
}

/*!
 * @brief Registers QML types.
 *
 * @param[in] uri Namespace for QML objects.
 * @param[in] location Location of the QML description file.
 * @Param[in] entries List of entries describing the types.
 */
static
void registerQmlTypes(const char *uri, const char *location,
    const struct QmlTypeEntry *entries)
{
	if ((uri == NULL) || (location == NULL)) {
		Q_ASSERT(0);
		return;
	}

	const struct QmlTypeEntry *entry = entries;
	while ((entry != NULL) && (entry->typeName != NULL)) {
		qmlRegisterType(
		    QUrl(QString("%1/%2.qml").arg(location).arg(entry->typeName)),
		    uri, entry->major, entry->minor, entry->typeName);
		++entry;
	}
}

/*!
 * @brief Allocates global log facility.
 */
static
int allocGlobLog(void)
{
	GlobInstcs::logPtr = new (::std::nothrow) LogDevice;
	if (Q_UNLIKELY(Q_NULLPTR == GlobInstcs::logPtr)) {
		return -1;
	}

	return 0;
}

/*!
 * @brief Deallocates global log facility.
 */
static
void deallocGlobLog(void)
{
	if (Q_NULLPTR != GlobInstcs::logPtr) {
		delete GlobInstcs::logPtr;
		GlobInstcs::logPtr = Q_NULLPTR;
	}
}

/*!
 * @brief Deallocates global preferences database instance.
 */
static
void deallocGlobPrefs(void)
{
	if (Q_NULLPTR != GlobInstcs::prefsDbPtr) {
		delete GlobInstcs::prefsDbPtr;
		GlobInstcs::prefsDbPtr = Q_NULLPTR;
	}
}

/*!
 * @brief Allocates global preferences database instance.
 *
 * @param[in] dbPath Path to preferences database.
 * @return 0 on success, -1 on failure.
 */
static
int allocGlobPrefs(const QString &dbPath)
{
	if (Q_UNLIKELY(dbPath.isEmpty())) {
		Q_ASSERT(0);
		return -1;
	}

	SQLiteDb::OpenFlags flags = SQLiteDb::NO_OPTIONS;

	GlobInstcs::prefsDbPtr = new (::std::nothrow) PrefsDb("prefsDb", false);
	if (Q_UNLIKELY(Q_NULLPTR == GlobInstcs::prefsDbPtr)) {
		logErrorNL("%s", "Cannot allocate preference db.");
		goto fail;
	}
	/* Open preferences database. */
	flags = SQLiteDb::CREATE_MISSING;
	if (Q_UNLIKELY(!GlobInstcs::prefsDbPtr->openDb(dbPath, flags))) {
		logErrorNL("Error opening preference db '%s'.",
		    dbPath.toUtf8().constData());
		goto fail;
	}

	return 0;

fail:
	deallocGlobPrefs();
	return -1;
}

/*!
 * @brief Loads preferences from database into the preferences structure.
 *
 * @note Database must already be opened at this point.
 */
static
int loadPreferencesDatabase(void)
{
	if (Q_UNLIKELY((GlobInstcs::prefsPtr == Q_NULLPTR) ||
	        (GlobInstcs::prefsDbPtr == Q_NULLPTR))) {
		return -1;
	}

	PrefsDefaults::eraseVanished(GlobInstcs::prefsDbPtr);

	return
	    GlobInstcs::prefsPtr->setDatabase(GlobInstcs::prefsDbPtr) ? 0 : -1;
}

int main(int argc, char *argv[])
{

#if (QT_VERSION < QT_VERSION_CHECK(6, 0, 0))
	QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);
#endif

#if defined(Q_OS_IOS) && (QT_VERSION > QT_VERSION_CHECK(6, 4, 0))
	QQuickStyle::setStyle("iOS");
#else
	QQuickStyle::setStyle("Material");
#endif

	/* First thing to create is the application object. */
	QApplication app(argc, argv);

	if (0 != allocGlobLog()) {
		/*
		 * Cannot continue without logging facility.
		 * This exit is not logged.
		 */
		return EXIT_FAILURE;
	}

	/* Set application data, identification, etc. */
	app.setOrganizationName(APP_ORG_NAME);
	app.setOrganizationDomain(APP_ORG_DOMAIN);
	app.setApplicationName(APP_NAME);
	app.setApplicationVersion(VERSION);

	{
#if defined(Q_OS_ANDROID) && !defined(Q_OS_ANDROID_EMBEDDED)
		QtMessageHandler oldHandler = qInstallMessageHandler(globalLogOutput);
		GlobInstcs::logPtr->installMessageHandler(oldHandler);
#endif
	}

	/* Log everything to stderr. */
	GlobInstcs::logPtr->setLogLevelBits(LogDevice::LF_STDERR, LOGSRC_ANY,
	    LOG_UPTO(LOG_DEBUG));

	/* Memory log. */
	MemoryLog memLog;
	/* At least 2 or 3 maximum-sized (20 MB) ZFOs should fit into 80 MB. */
	memLog.setMaxMemory(80 * 1024 * 1024);
	GlobInstcs::logPtr->installMemoryLog(&memLog);

	/* Create and open new log file. Return negative number if an error. */
	if (GlobInstcs::logPtr->openFile(constructLogFileName(),
	    LogDevice::LM_WRONLY) < 0) {
		logWarningNL("%s", "Warning: Cannot create or open new log file.");
	}
	GlobInstcs::logPtr->setLogLevelBits(LogDevice::LF_FILE, LOGSRC_ANY,
	    LOG_UPTO(LOG_DEBUG));

	/* Create configuration file if file is missing. */
	INIPreferences::ensureConfPresence();

	/* Convert INI preferences to new format. */
	{
		QSettings settings(INIPreferences::confPath(),
		    QSettings::IniFormat);
		iniPreferencesForCompatibility(settings);
	}

	/* Create global settings object and load application settings. */
	{
		/* Perform check that configuration file can be accessed. */
		if (INIPreferences::confPath().isEmpty()) {
			return EXIT_FAILURE;
		}

		/* Create and init global PIN settings. */
		GlobInstcs::pinSetPtr = new (::std::nothrow) PinSettings;
		if (GlobInstcs::pinSetPtr == Q_NULLPTR) {
			logErrorNL("%s", "Cannot create PIN settings.");
			return EXIT_FAILURE;
		}

		/* Create and init global records management settings. */
		GlobInstcs::recMgmtSetPtr =
		    new (::std::nothrow) RecordsManagementSettings;
		if (GlobInstcs::recMgmtSetPtr == Q_NULLPTR) {
			logErrorNL("%s",
			    "Cannot create records management settings.");
			return EXIT_FAILURE;
		}

		/* Create and init global preferences. */
		GlobInstcs::prefsPtr = new (::std::nothrow) Prefs;
		if (Q_UNLIKELY(GlobInstcs::prefsPtr == Q_NULLPTR)) {
			logErrorNL("%s", "Cannot allocate database preferences.");
			return EXIT_FAILURE;
		}

		/* Load application settings from storage. */
		QSettings settings(INIPreferences::confPath(),
		    QSettings::IniFormat);
		GlobInstcs::pinSetPtr->loadFromSettings(settings);
		GlobInstcs::recMgmtSetPtr->loadFromSettings(settings);
	}

	/* Get application data writable location. */
	const QString dataDirPath(
	    existingWritableLoc(QStandardPaths::AppDataLocation));
	if (Q_UNLIKELY(dataDirPath.isEmpty())) {
		logErrorNL("%s",
		    "Cannot determine application data location.");
		Q_ASSERT(0);
		return EXIT_FAILURE;
	}

	if (Q_UNLIKELY(0 != allocGlobPrefs(dataDirPath + QDir::separator() + PREFS_DB_FILE))) {
		return EXIT_FAILURE;
	}

	/* Set default preferences. */
	PrefsDefaults::setDefaults(*GlobInstcs::prefsPtr);
	/* Load preferences from database. */
	if (Q_UNLIKELY(0 != loadPreferencesDatabase())) {
		return EXIT_FAILURE;
	}

	/* Convert INI preferences into database and remove them from INI file. */
	{
		QSettings settings(INIPreferences::confPath(),
		    QSettings::IniFormat);
#if (QT_VERSION >= QT_VERSION_CHECK(6, 0, 0))
		/*
		 * Qt-6 does not provide a replacement of QSettings::setIniCodec().
		 * The documentation claims that QSettings assumes the INI file
		 * is UTF-8 encoded.
		 * TODO - It would be great to make really sure the INI is UTF-8 encoded
		 * instead of relying on the developers and documentation.
		 */
#else /* < Qt-6.0 */
		settings.setIniCodec("UTF-8");
#endif /* >= Qt-6.0 */
		iniPreferencesToPrefs(settings, *GlobInstcs::prefsPtr);
	}

	/* Set debug and log verbosity level from settings. */
	{
		qint64 val = 0;
		if (GlobInstcs::prefsPtr->intVal("logging.verbosity.level.debug", val)) {
			GlobInstcs::logPtr->setDebugVerbosity(val);
		}
		if (GlobInstcs::prefsPtr->intVal("logging.verbosity.level.log", val)) {
			GlobInstcs::logPtr->setLogVerbosity(val);
		}
	}
	deleteOldestLogFile();

	logAppVersion();
	logTimeZone();
	logQtVersion();

	switch (crypto_compiled_lib_ver_check()) {
	case 1:
#if defined(Q_OS_WIN)
		/* Exit only on windows. */
		logErrorNL("%s", "Cryptographic library mismatch.");
		return EXIT_FAILURE;
# else
		logWarningNL("%s", "Cryptographic library mismatch.");
#endif /* defined(Q_OS_WIN) */
		break;
	case 0:
		break;
	case -1:
	default:
		logErrorNL("%s",
		    "Error checking the version of the cryptographic library.");
		return EXIT_FAILURE;
		break;
	}

	QCoreApplication::addLibraryPath("./");

	/* Set random generator. */
	Compat::seedRand();

	/* Setup command-line parser. */
	QCommandLineParser parser;
	if (0 != setupCmdLineParser(parser)) {
		return EXIT_FAILURE;
	}

	/* Process command-line arguments. */
	parser.process(app);

	/* Create globally accessible objects. */
	{
		GlobInstcs::msgProcEmitterPtr =
		    new (::std::nothrow) MessageProcessingEmitter;
		if (GlobInstcs::msgProcEmitterPtr == Q_NULLPTR) {
			logErrorNL("%s", "Cannot create status message emitter.");
			return EXIT_FAILURE;
		}
		/*
		 * Only one worker thread currently.
		 * TODO -- To be able to run multiple threads in the pool
		 * a locking mechanism over isds context structures must
		 * be implemented. Also, per-context queueing
		 * ought to be implemented to avoid unnecessary waiting.
		 */
		GlobInstcs::workPoolPtr = new (::std::nothrow) WorkerPool(1);
		if (GlobInstcs::workPoolPtr == Q_NULLPTR) {
			logErrorNL("%s", "Cannot create worker pool.");
			return EXIT_FAILURE;
		}

		GlobInstcs::recMgmtDbPtr = new (::std::nothrow)
		    RecordsManagementDb("recordsManagementDb", false);
		if (GlobInstcs::recMgmtDbPtr == Q_NULLPTR) {
			logErrorNL("%s",
			    "Cannot allocate records management db.");
			return EXIT_FAILURE;
		}

		GlobInstcs::isdsSessionsPtr = new (::std::nothrow) Sessions;
		if (GlobInstcs::isdsSessionsPtr == Q_NULLPTR) {
			logErrorNL("%s",
			    "Cannot allocate ISDS session container.");
			return EXIT_FAILURE;
		}

		GlobInstcs::acntMapPtr = new (::std::nothrow) AccountsMap(
		    AcntContainer::CT_REGULAR);
		if (GlobInstcs::acntMapPtr == Q_NULLPTR) {
			logErrorNL("%s", "Cannot allocate account map.");
			return EXIT_FAILURE;
		}

		GlobInstcs::acntLogos = new (::std::nothrow) AccountLogos;
		if (GlobInstcs::acntLogos == Q_NULLPTR) {
			logErrorNL("%s", "Cannot allocate account logo container.");
			return EXIT_FAILURE;
		}
		GlobInstcs::acntLogos->setObjectName("acntLogos");

		GlobInstcs::imgProvPtr = new (::std::nothrow) ImageProvider;
		if (GlobInstcs::imgProvPtr == Q_NULLPTR) {
			logErrorNL("%s", "Cannot create image provider.");
			return EXIT_FAILURE;
		}

		GlobInstcs::iOSHelperPtr = new (::std::nothrow) IosHelper;
		if (GlobInstcs::iOSHelperPtr == Q_NULLPTR) {
			logErrorNL("%s", "Cannot create iOS helper.");
			return EXIT_FAILURE;
		}
	}

	QStringList cmdLineFileNames;
#if not defined(Q_OS_ANDROID)
	cmdLineFileNames = parser.positionalArguments();
#endif /* defined(Q_OS_ANDROID) */

	/* Set font - default is system font. */
	Font::setSystemFont(QApplication::font());
	QFont font = Font::font(GlobalSettingsQmlWrapper::font());
	font.setPixelSize(GlobalSettingsQmlWrapper::fontSize());
#if defined(Q_OS_IOS)
	/*
	 * Use smaller font as default for some models of iOS devices
	 * with small screen size. iPhone series 8,7,6,5,4 and all SE generations.
	 * Logical Width <= 375, Scale Factor <= 2, values obtained from
	 * https://www.ios-resolution.com/
	 */
	int gw = QGuiApplication::primaryScreen()->geometry().width();
	qreal dpr = QGuiApplication::primaryScreen()->devicePixelRatio();
	if (gw <= 375 && dpr <= 2) {
		// Set apk default font size -2pt.
		font.setPixelSize(GlobalSettingsQmlWrapper::fontSize() - 2);
	}
#endif /* defined(Q_OS_IOS) */
	QApplication::setFont(font);

	/* Load datovka localization and qtbase localization. */
	QTranslator datovkaTrans;
	QTranslator qtbaseTrans;
	QString lang(GlobalSettingsQmlWrapper::language());
	QString shortLang(Localisation::shortLangName(lang));

#if defined(Q_OS_IOS)
	/*
	 * Only for iOS and Qt version >= 5.14: Use Czech language as default
	 * if system language was not recognised correctly from iOS.
	 */
	if ((shortLang != Localisation::langCs)
	        && (shortLang != Localisation::langEn)
	        && (shortLang != Localisation::langUk)) {
		shortLang = Localisation::langCs;
	}
#endif /* defined(Q_OS_ANDROID) */

	Localisation::setProgramLocale(lang);
	{
		QString fileName = "datovka_" + shortLang;
		if (datovkaTrans.load(fileName, ":/locale/")) {
			logDebugLv0NL("Loaded localisation file '%s'.",
			    fileName.toUtf8().constData());
		} else {
			logErrorNL("Could not load localisation file '%s'.",
			    fileName.toUtf8().constData());
		}
	}
	QCoreApplication::installTranslator(&datovkaTrans);
	{
		QString fileName = "qtbase_" + shortLang;
		if (qtbaseTrans.load(fileName, ":/locale/")) {
			logDebugLv0NL("Loaded localisation file '%s'.",
			    fileName.toUtf8().constData());
		} else {
			logErrorNL("Could not load localisation file '%s'.",
			    fileName.toUtf8().constData());
		}
	}
	QCoreApplication::installTranslator(&qtbaseTrans);

	/* Start worker threads. */
	GlobInstcs::workPoolPtr->start();
	logInfoNL("%s", "Worker pool started.");

	/* Initialise and use these classes - register into QML. */
	Messages messages;
	Accounts accounts;
	Files files;
	Log log(&memLog);
	IsdsWrapper isds;
	IosHelper iOSHelper;
	GovWrapper gov(&isds);
	GlobalSettingsQmlWrapper settings;
	InteractionZfoFile interactionZfoFile;
	RecordsManagement recordsManagement;
	StringManipulation strManipulation;
	Zfo zfo;

	/* Initialise app delegate component for interaction with iOS
	 * Reaction on the iOS action "Open in..." */
#if defined Q_OS_IOS

	QtAppDelegateInitialize(&interactionZfoFile);

	GlobInstcs::iOSHelperPtr = &iOSHelper;

	/* Clear send and tmp dir (iOS only). */
	IosHelper::clearSendAndTmpDirs();

	/* Clear backup, transfer dirs (iOS only). */
	IosHelper::clearBackupTransferDirs();

	/* Create restore folder in sandbox if not exists (iOS only). */
	appRestoreDirPath();
#endif

	/*
	 * Connect slot for isds cxt delete when account was deleted
	 * or updated.
	 */
	QObject::connect(&accounts, SIGNAL(removeIsdsCtx(AcntId)),
	    &isds, SLOT(removeIsdsCtx(AcntId)));

	/* Register application state changes. */
	class Locker locker;
	QObject::connect(&app,
	    SIGNAL(applicationStateChanged(Qt::ApplicationState)),
	    &locker, SLOT(processNewState(Qt::ApplicationState)));
	app.installEventFilter(&locker);

	/* Get main handle of the application and the QML engine. */
	QQmlApplicationEngine *engine = new (::std::nothrow) QQmlApplicationEngine;
	if (Q_UNLIKELY(engine == Q_NULLPTR)) {
		logErrorNL("%s", "Cannot create QML application engine.");
		return EXIT_FAILURE;
	}
	QQmlContext *ctx = engine->rootContext();

	/* Register application pages, components and dialogues into QML. */
	registerQmlTypes(uri, QML_PAGE_LOC, qmlPages);
	registerQmlTypes(uri, QML_COMPONENT_LOC, qmlComponents);
	registerQmlTypes(uri, QML_DIALOGUES, qmlDialogues);
	registerQmlTypes(uri, QML_WIZARDS, qmlWizards);
	registerQmlTypes(uri, QML_POPUPS, qmlPopups);

	/* Register types into QML. */
	AcntData::declareQML();
	AcntId::declareTypes();
	AccountListModel::declareQML();
	BackupRestoreSelectionModel::declareQML();
	BackupRestoreZipData::declareQML();
	DataboxListModel::declareQML();
	DataboxModelEntry::declareQML();
	FileListModel::declareQML();
	Files::declareQML();
	GovFormListModel::declareQML();
	GovServiceListModel::declareQML();
	InteractionFilesystem::declareQML();
	IosHelper::declareQML();
	ListSortFilterProxyModel::declareQML();
	MessageListModel::declareQML();
	Messages::declareQML();
	MsgInfo::declareQML();
	PrefListModel::declareQML();
	UploadHierarchyListModel::declareQML();
	UploadHierarchyQmlProxyModel::declareQML();
	QmlAcntId::declareQML();
	QmlIsdsEnvelope::declareQML();

	/* Register some classes into QML. */
	ctx->setContextProperty("isds", &isds);
	ctx->setContextProperty("messages", &messages);
	ctx->setContextProperty("accounts", &accounts);
	ctx->setContextProperty("files", &files);
	ctx->setContextProperty("gov", &gov);
	ctx->setContextProperty("iOSHelper", &iOSHelper);
	ctx->setContextProperty("settings", &settings);
	ctx->setContextProperty("strManipulation", &strManipulation);
	ctx->setContextProperty("locker", &locker);
	ctx->setContextProperty("log", &log);
	ctx->setContextProperty("interactionZfoFile", &interactionZfoFile);
	ctx->setContextProperty("zfo", &zfo);
	ctx->setContextProperty("recordsManagement", &recordsManagement);

	ctx->setContextProperty(GlobInstcs::acntLogos->objectName(),
	    GlobInstcs::acntLogos);

	/* Localise description in tables. */
	SQLiteTbls::localiseTableDescriptions();

	/* Initialise databases */
	AccountDb accountDb("ACCOUNTS", false);
	MsgDbContainer messageDbs("MESSAGES");
	FileDbContainer fileDbs("FILES");
	ZfoDb zfoDb("ZFOS", false);

	/* Append databases to global objects. */
	GlobInstcs::accountDbPtr = &accountDb;
	GlobInstcs::messageDbsPtr = &messageDbs;
	GlobInstcs::fileDbsPtr = &fileDbs;
	GlobInstcs::zfoDbPtr = &zfoDb;

	/* Create and initialise prefs model. */
	PrefListModel *prefModelPtr = new (::std::nothrow) PrefListModel;
	if (Q_UNLIKELY(Q_NULLPTR == prefModelPtr)) {
		logErrorNL("%s", "Cannot create preferences model.");
		return EXIT_FAILURE;
	}
	prefModelPtr->setObjectName("mainPrefListModel");
	prefModelPtr->loadContent(GlobInstcs::prefsPtr);

	/* Register and set account model in QML. */
	ctx->setContextProperty(prefModelPtr->objectName(), prefModelPtr);

	/* Create and initialise account model. */
	AccountListModel *accountModelPtr = new (::std::nothrow) AccountListModel;
	if (Q_UNLIKELY(Q_NULLPTR == accountModelPtr)) {
		logErrorNL("%s", "Cannot create account model.");
		return EXIT_FAILURE;
	}
	accountModelPtr->setObjectName("mainAccountModel");
	accountModelPtr->loadContent(GlobInstcs::acntMapPtr,
	    GlobInstcs::accountDbPtr, GlobInstcs::acntLogos,
	    GlobInstcs::prefsPtr);

	/* Register and set account model in QML. */
	ctx->setContextProperty(accountModelPtr->objectName(), accountModelPtr);

	/* Add image provider into QML engine. */
	engine->addImageProvider(IMAGE_PROVIDER_ID, GlobInstcs::imgProvPtr);

	/* Open account database. */
	QString dbPath(dataDirPath + QDir::separator() + ACNT_DB_NAME);
	if (!GlobInstcs::accountDbPtr->openDb(dbPath,
	        SQLiteDb::CREATE_MISSING)) {
		logErrorNL("Error opening account db '%s'.",
		    dbPath.toUtf8().constData());
		return EXIT_FAILURE;
	}

	/* Open records management database. */
	QString rmDbPath(dataDirPath + QDir::separator() + RECORDS_MANAGEMENT_DB_FILE);
	if (!GlobInstcs::recMgmtDbPtr->openDb(
	        rmDbPath, SQLiteDb::CREATE_MISSING)) {
		logErrorNL("Error opening records management db '%s'.",
		    rmDbPath.toUtf8().constData());
		return EXIT_FAILURE;
	}

	/* Load accounts, delete expired messages, set message counters */
	{
		/*
		 * Load accounts from settings to account container.
		 * Configuration directory is always unspecified in mobile app.
		 */
		QSettings settings(INIPreferences::confPath(),
		    QSettings::IniFormat);
		GlobInstcs::acntMapPtr->loadFromSettings(QString(), settings);

		if (Q_UNLIKELY(!GlobInstcs::acntLogos->setLogoDirPath(dfltDbAndConfigLoc() + "/logos"))) {
			logErrorNL("%s", "Error accessing logo location.");
			return EXIT_FAILURE;
		}

		/* Load message counters. */
		Accounts::loadModelCounters(accountModelPtr);
	}

	/*
	 * Open ZFO database, the second parameter means: true = zfo db will
	 * store on disk, false = only in memory.
	 */
	if (!GlobInstcs::zfoDbPtr->openDb(ZFO_DB_NAME, true)) {
		logWarningNL("%s", "ZFO db not found.");
	}

	/* Remember first apk startup time and other info. */
	if (Q_UNLIKELY(!GlobalSettingsQmlWrapper::appFirstLaunchTime().isValid())) {
		GlobalSettingsQmlWrapper::storeAppFirstLaunchTime();
	}
	GlobalSettingsQmlWrapper::incrementLaunchCount();
	GlobalSettingsQmlWrapper::storeAppLastLaunchTime();

	INIPreferencesWatcher *preferencesWatcher =
	    new (::std::nothrow) INIPreferencesWatcher;
	if (Q_NULLPTR != preferencesWatcher) {
		preferencesWatcher->setObjectName("preferencesWatcher");
		ctx->setContextProperty(preferencesWatcher->objectName(),
		    preferencesWatcher);
	}

	/* Load UI (QML). */
	engine->load(QUrl(QStringLiteral("qrc:/qml/main.qml")));

	/* OpenSSL support test. */
	if (!QSslSocket::supportsSsl()) {
		logWarningNL("%s", "The device does not support OpenSSL.");
	}

	/* Check for certificates. */
	{
		const struct pem_str *pem_desc = conn_pem_strs;
		Q_ASSERT(Q_NULLPTR != pem_desc);
		const QList<QSslCertificate> systemCaCerts(QSslConfiguration::systemCaCertificates());
		while ((NULL != pem_desc->name) && (NULL != pem_desc->pem)) {
			bool found = false;
			const QSslCertificate pemCert(QByteArray(pem_desc->pem),
			    QSsl::Pem);
			if (Q_UNLIKELY(pemCert.isNull())) {
				Q_ASSERT(0);
				logErrorNL("Cannot read CA certificate: %s", pem_desc->name);
				continue;
			}
			foreach (const QSslCertificate &cert, systemCaCerts) {
				if (cert == pemCert) {
					found = true;
					break;
				}
			}
			if (found) {
				logInfoNL("Found following CA certificate: %s", pem_desc->name);
			} else {
				logWarningNL("Missing following CA certificate: %s", pem_desc->name);
#if (QT_VERSION >= QT_VERSION_CHECK(5, 15, 0))
				{
					QSslConfiguration dfltSslConf =
					    QSslConfiguration::defaultConfiguration();
					dfltSslConf.addCaCertificate(pemCert);
					QSslConfiguration::setDefaultConfiguration(dfltSslConf);
				}
#else /* < Qt-5.15.0 */
				QSslSocket::addDefaultCaCertificate(pemCert);
#endif /* >= Qt-5.15.0 */
				logInfoNL("Added following CA certificate: %s", pem_desc->name);
			}
			++pem_desc;
		}
	}

	/* Inactivity locking is disabled when equal to 0. */
	{
		int secs = GlobalSettingsQmlWrapper::inactivityInterval();
		if (secs > 0) {
			locker.setInactivityInterval(secs);
		}
	}

	/*
	 * Show PIN screen if needed. Encoded PIN is checked because it hasn't
	 * been decoded yet.
	 */
	if (GlobInstcs::pinSetPtr->pinConfigured()) {
		emit locker.lockApp();
	}

	/* Open files passed via command line. */
	if (!cmdLineFileNames.isEmpty()) {
		foreach (const QString &filePath, cmdLineFileNames) {
			interactionZfoFile.openZfoFile(filePath);
		}
		settings.setCleanAppStart(false);
	}

#if defined(Q_OS_ANDROID)
	AndroidIO::checkAndAskStoragePermissions();
#endif /* defined(Q_OS_ANDROID) */

	/* Check or create backup message database for all accounts. */
	messages.checkOrCreateLocalBackupMsgDbs();

	/* Run some actions after application start up if no PIN is used. */
	if (settings.cleanAppStart() && (!GlobInstcs::pinSetPtr->pinConfigured())) {

		emit settings.runOnAppStartUpSig();

		settings.setCleanAppStart(false);
	}

	/* Run app main event loop. */
	int ret = app.exec();

	/*
	 * The QML engine must be destroyed before destroying any
	 * of the globally accessible resources.
	 */
	delete engine; engine = Q_NULLPTR;

	delete preferencesWatcher; preferencesWatcher = Q_NULLPTR;

	/* Wait until all threads finished. */
	logInfoNL("%s", "Waiting for pending worker threads.");
	GlobInstcs::workPoolPtr->wait();
	GlobInstcs::workPoolPtr->stop();
	logInfoNL("%s", "All worker threads finished.");

	/* Close all OTP connections if exist. */
	isds.closeAllOtpConnections();

	delete accountModelPtr; accountModelPtr = Q_NULLPTR;
	delete prefModelPtr; prefModelPtr = Q_NULLPTR;

	/* Destroy globally accessible objects. */
	{
		GlobInstcs::imgProvPtr = Q_NULLPTR; /* Is owned by engine. */

		delete GlobInstcs::acntLogos; GlobInstcs::acntLogos = Q_NULLPTR;

		delete GlobInstcs::acntMapPtr; GlobInstcs::acntMapPtr = Q_NULLPTR;
		delete GlobInstcs::isdsSessionsPtr; GlobInstcs::isdsSessionsPtr = Q_NULLPTR;

		delete GlobInstcs::recMgmtDbPtr; GlobInstcs::recMgmtDbPtr = Q_NULLPTR;
		GlobInstcs::zfoDbPtr = Q_NULLPTR;
		GlobInstcs::fileDbsPtr = Q_NULLPTR;
		GlobInstcs::messageDbsPtr = Q_NULLPTR;
		GlobInstcs::accountDbPtr = Q_NULLPTR;

		deallocGlobPrefs();

		delete GlobInstcs::prefsPtr; GlobInstcs::prefsPtr = Q_NULLPTR;
		delete GlobInstcs::recMgmtSetPtr; GlobInstcs::recMgmtSetPtr = Q_NULLPTR;
		delete GlobInstcs::pinSetPtr; GlobInstcs::pinSetPtr = Q_NULLPTR;

		delete GlobInstcs::workPoolPtr; GlobInstcs::workPoolPtr = Q_NULLPTR;
		delete GlobInstcs::msgProcEmitterPtr; GlobInstcs::msgProcEmitterPtr = Q_NULLPTR;

		GlobInstcs::iOSHelperPtr = Q_NULLPTR;
	}

	/* Finally, destroy global log object. */
	deallocGlobLog();

	return ret;
}
