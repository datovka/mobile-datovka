/*
 * Copyright (C) 2014-2025 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

#include "src/datovka_shared/identifiers/account_id.h"
#include "src/datovka_shared/io/records_management_db.h"
#include "src/datovka_shared/log/log.h"
#include "src/datovka_shared/records_management/json/entry_error.h"
#include "src/datovka_shared/records_management/json/service_info.h"
#include "src/datovka_shared/records_management/json/upload_file.h"
#include "src/datovka_shared/records_management/json/upload_hierarchy.h"
#include "src/datovka_shared/settings/records_management.h"
#include "src/datovka_shared/worker/pool.h"
#include "src/files.h"
#include "src/global.h"
#include "src/io/filesystem.h"
#include "src/qml_interaction/image_provider.h"
#include "src/records_management/models/upload_hierarchy_list_model.h"
#include "src/settings/account.h"
#include "src/settings/accounts.h"
#include "src/settings/prefs_specific.h"
#include "src/setwrapper.h"
#include "src/sqlite/message_db_container.h"
#include "src/sqlite/zfo_db.h"
#include "src/worker/emitter.h"
#include "src/worker/task_records_management_stored_messages.h"
#include "src/wrap_records_management.h"

RecordsManagement::RecordsManagement(QObject *parent)
    : QObject(parent),
    m_rmc(RecMgmt::Connection::ignoreSslErrorsDflt, this)
{
}

bool RecordsManagement::callServiceInfo(const QString &urlStr,
    const QString &tokenStr)
{
	QByteArray response;

	if (urlStr.trimmed().isEmpty() || tokenStr.trimmed().isEmpty()) {
		return false;
	}

	m_rmc.setConnection(urlStr.trimmed(), tokenStr.trimmed());

	if (RecMgmt::Connection::COMM_SUCCESS == m_rmc.communicate(
	        RecMgmt::Connection::SRVC_SERVICE_INFO,
	        QByteArray(), response)) {
		if (!response.isEmpty()) {
			bool ok = false;
			RecMgmt::ServiceInfoResp siRes(
			    RecMgmt::ServiceInfoResp::fromJson(response, &ok));
			if (ok && siRes.isValid()) {
				if (GlobInstcs::imgProvPtr != Q_NULLPTR) {
					GlobInstcs::imgProvPtr->setSvg(
					    RM_SVG_LOGO_ID, siRes.logoSvg());
				}
				emit serviceInfo(siRes.name(), siRes.tokenName());
				return true;
			}
		}
	}

	return false;
}

void RecordsManagement::callUploadHierarchy(
    UploadHierarchyListModel *hierarchyModel)
{
	if (Q_UNLIKELY(GlobInstcs::recMgmtSetPtr == Q_NULLPTR)) {
		Q_ASSERT(0);
		return;
	}

	QByteArray response;

	if (GlobInstcs::recMgmtSetPtr->url().isEmpty() ||
	    GlobInstcs::recMgmtSetPtr->token().isEmpty()) {
		return;
	}

	if (Q_UNLIKELY(hierarchyModel == Q_NULLPTR)) {
		logErrorNL("%s", "Cannot access upload hierarchy model.");
		Q_ASSERT(0);
		return;
	}

	/* Clear model. */
	hierarchyModel->setHierarchy(RecMgmt::UploadHierarchyResp());

	m_rmc.setConnection(GlobInstcs::recMgmtSetPtr->url(),
	    GlobInstcs::recMgmtSetPtr->token());

	if (RecMgmt::Connection::COMM_SUCCESS == m_rmc.communicate(
	        RecMgmt::Connection::SRVC_UPLOAD_HIERARCHY,
	        QByteArray(), response)) {
		if (!response.isEmpty()) {
			bool ok = false;
			RecMgmt::UploadHierarchyResp uhRes(
			    RecMgmt::UploadHierarchyResp::fromJson(response, &ok));
			if (ok && uhRes.isValid()) {
				/* Set model. */
				hierarchyModel->setHierarchy(uhRes);
				return;
			}
		}
	}
}

void RecordsManagement::getStoredMsgInfoFromRecordsManagement(
    const QString &urlStr, const QString &tokenStr)
{
	if (Q_UNLIKELY((GlobInstcs::workPoolPtr == Q_NULLPTR) ||
	        (GlobInstcs::messageDbsPtr == Q_NULLPTR) ||
	        (GlobInstcs::acntMapPtr == Q_NULLPTR))) {
		Q_ASSERT(0);
		return;
	}

	QString url(urlStr.trimmed());
	QString token(tokenStr.trimmed());

	if (url.isEmpty() || token.isEmpty()) {
		return;
	}

	TaskRecordsManagementStoredMessages *task =
	    new (::std::nothrow) TaskRecordsManagementStoredMessages(
	        url, token,
	        TaskRecordsManagementStoredMessages::RM_UPDATE_STORED,
	        Q_NULLPTR, AcntId(), 0, 0);
	if (Q_NULLPTR == task) {
		logErrorNL("%s", "Cannot create stored_files update task.");
		return;
	}
	task->setAutoDelete(true);
	/* Run in background. */
	GlobInstcs::workPoolPtr->assignHi(task);

	int accTotal = GlobInstcs::acntMapPtr->keys().count();
	int accNumber = 0;

	for (const AcntId &acntId : GlobInstcs::acntMapPtr->keys()) {
		accNumber++;

		MessageDb *msgDb = GlobInstcs::messageDbsPtr->accessMessageDb(
		    GlobalSettingsQmlWrapper::dbPath(), acntId,
		    PrefsSpecific::dataOnDisk(*GlobInstcs::prefsPtr,
		        GlobInstcs::acntMapPtr->acntData(acntId)));
		if (msgDb == Q_NULLPTR) {
			logWarningNL("%s", "Cannot access message database.");
			continue;
		}

		TaskRecordsManagementStoredMessages *task =
		    new (::std::nothrow) TaskRecordsManagementStoredMessages(
		        url, token,
		        TaskRecordsManagementStoredMessages::RM_DOWNLOAD_ALL,
		        msgDb, acntId, accNumber, accTotal);
		if (Q_NULLPTR == task) {
			logWarningNL("Cannot create stored_files task for '%s'.",
			    acntId.username().toUtf8().constData());
			continue;
		}
		task->setAutoDelete(true);
		/* Run in background. */
		GlobInstcs::workPoolPtr->assignHi(task);

	}
}

bool RecordsManagement::isValidRecordsManagement(void)
{
	if (GlobInstcs::recMgmtSetPtr != Q_NULLPTR) {
		return GlobInstcs::recMgmtSetPtr->isValid();
	} else {
		Q_ASSERT(0);
		return false;
	}
}

void RecordsManagement::loadStoredServiceInfo(void)
{
	if (Q_NULLPTR == GlobInstcs::recMgmtDbPtr) {
		return;
	}

	RecordsManagementDb::ServiceInfoEntry entry(
	    GlobInstcs::recMgmtDbPtr->serviceInfo());
	if (!entry.isValid()) {
		return;
	}

	if (GlobInstcs::imgProvPtr != Q_NULLPTR) {
		GlobInstcs::imgProvPtr->setSvg(RM_SVG_LOGO_ID, entry.logoSvg);
	}
	emit serviceInfo(entry.name, entry.tokenName);
}

bool RecordsManagement::uploadMessage(const QmlAcntId *qAcntId,
    const QString &dmId, enum Messages::MessageType messageType,
    UploadHierarchyListModel *hierarchyModel)
{
	if (Q_UNLIKELY((qAcntId == Q_NULLPTR) ||
	        (GlobInstcs::zfoDbPtr == Q_NULLPTR) ||
	        (GlobInstcs::acntMapPtr == Q_NULLPTR))) {
		Q_ASSERT(0);
		return false;
	}

	bool ok = false;
	qint64 msgId = dmId.toLongLong(&ok);
	if (!ok || (msgId < 0)) {
		return false;
	}

	QString srcFilePath = joinDirs(getZfoPath(qAcntId->username(),
	    qAcntId->testing(), msgId), getMsgZfoName(msgId, messageType));
	QByteArray msgData = Files::rawFileContent(srcFilePath);

	if (msgData.isEmpty()) {
		msgData = QByteArray::fromBase64(
		    GlobInstcs::zfoDbPtr->getZfoContentFromDb(msgId,
		    qAcntId->testing()));
	}

	if (msgData.isEmpty()) {
		return false;
	}

	QString msgType;
	switch (messageType) {
	case Messages::TYPE_RECEIVED:
		msgType = QLatin1String("DDZ");
		break;
	case Messages::TYPE_SENT:
		msgType = QLatin1String("ODZ");
		break;
	default:
		msgType = QLatin1String("DZ");
		break;
	}

	QString msgFileName = QString("%1_%2.zfo").arg(msgType).arg(dmId);

	if (Q_UNLIKELY(hierarchyModel == Q_NULLPTR)) {
		logErrorNL("%s", "Cannot access upload hierarchy model.");
		Q_ASSERT(0);
		return false;
	}

	return uploadFile(msgId, msgFileName, msgData, hierarchyModel->selectedIds());
}

bool RecordsManagement::updateServiceInfo(const QString &newUrlStr,
    const QString &oldUrlStr, const QString &srName, const QString &srToken)
{
	if (Q_NULLPTR == GlobInstcs::recMgmtDbPtr) {
		return false;
	}

	QString cUrlStr = newUrlStr.trimmed();

	if (!cUrlStr.isEmpty()) {
		RecordsManagementDb::ServiceInfoEntry entry;
		entry.url = cUrlStr;
		entry.name = srName;
		entry.tokenName = srToken;
		entry.logoSvg = (GlobInstcs::imgProvPtr != Q_NULLPTR) ?
		    GlobInstcs::imgProvPtr->svg(RM_SVG_LOGO_ID) : QByteArray();
		GlobInstcs::recMgmtDbPtr->updateServiceInfo(entry);
		if (oldUrlStr != cUrlStr) {
			return GlobInstcs::recMgmtDbPtr->deleteAllStoredMsg();
		}
	} else {
		return GlobInstcs::recMgmtDbPtr->deleteAllEntries();
	}

	return true;
}

bool RecordsManagement::uploadFile(qint64 dmId, const QString &msgFileName,
    const QByteArray &msgData, const QStringList &uploadIds)
{
	QByteArray response;

	if (Q_UNLIKELY(GlobInstcs::recMgmtSetPtr == Q_NULLPTR)) {
		Q_ASSERT(0);
		return false;
	}

	if (msgFileName.isEmpty() || msgData.isEmpty()) {
		Q_ASSERT(0);
		return false;
	}

	if (GlobInstcs::recMgmtSetPtr->url().isEmpty() ||
	    GlobInstcs::recMgmtSetPtr->token().isEmpty()) {
		return false;
	}

	RecMgmt::UploadFileReq ufReq(uploadIds, msgFileName, msgData);
	if (!ufReq.isValid()) {
		Q_ASSERT(0);
		return false;
	}

	m_rmc.setConnection(GlobInstcs::recMgmtSetPtr->url(),
	    GlobInstcs::recMgmtSetPtr->token());

	if (RecMgmt::Connection::COMM_SUCCESS == m_rmc.communicate(
	        RecMgmt::Connection::SRVC_UPLOAD_FILE,
	        ufReq.toJsonData(), response)) {
		if (!response.isEmpty()) {
			bool ok = false;
			RecMgmt::UploadFileResp ufRes(
			    RecMgmt::UploadFileResp::fromJson(response, &ok));
			if (ok && ufRes.isValid()) {

				if (ufRes.id().isEmpty()) {
					QString errorMessage(QObject::tr("Received error") +
						QLatin1String(": ") + ufRes.error().trVerbose());
					errorMessage += QLatin1String("\n");
					errorMessage += ufRes.error().description();

					emit showMessageBox(QObject::tr("File Upload Error"),
					    QObject::tr("Message '%1' could not be uploaded.").arg(dmId),
					    errorMessage);

					return false;
				}

				emit showMessageBox(QObject::tr("Successful File Upload"),
					QObject::tr("Message '%1' was successfully uploaded into the records management service.").arg(dmId),
					QObject::tr("It can be now found in the records management service in these locations:") +
					QStringLiteral("\n") + ufRes.locations().join(QStringLiteral("\n")));

				if (!ufRes.locations().isEmpty()) {
					logInfoNL("%s", "Message has been stored into records management service.");
					if (Q_NULLPTR != GlobInstcs::recMgmtDbPtr) {
						return GlobInstcs::recMgmtDbPtr->updateStoredMsg(dmId,
							ufRes.locations());
					} else {
						Q_ASSERT(0);
						return true;
					}
				} else {
					logErrorNL("%s",
						"Received empty location list when uploading message.");
				}

				return false;
			}
		}
	}

	return false;
}
