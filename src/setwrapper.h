/*
 * Copyright (C) 2014-2025 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

#pragma once

#include <QDateTime>
#include <QObject>
#include <QString>

/*
 * This class provide wrapper between
 * global settings class and QML settings page.
 * We need signal and slots for sending settings to/from QML page.
 * This object is initialized in main.cpp and register to QML.
 */
class GlobalSettingsQmlWrapper : public QObject {
    Q_OBJECT

public:
	/*!
	 * @brief Constructor.
	 */
	explicit GlobalSettingsQmlWrapper(QObject *parent = Q_NULLPTR);

	/*!
	 * @brief Get application version string.
	 *
	 * @return Application version string.
	*/
	Q_INVOKABLE static
	QString appVersion(void);

	/*!
	 * @brief Get download complete messages from global settings.
	 *
	 * @return True if it is enabled.
	*/
	Q_INVOKABLE static
	bool downloadCompleteMsgs(void);

	/*!
	 * @brief Set download complete messages to global settings.
	 *
	 * @param[in] downloadCompleteMsgs True if it is enabled.
	*/
	Q_INVOKABLE static
	void setDownloadCompleteMsgs(bool downloadCompleteMsgs);

	/*!
	 * @brief Get download only new messages from global settings.
	 *
	 * @return True if it is enabled.
	*/
	Q_INVOKABLE static
	bool downloadOnlyNewMsgs(void);

	/*!
	 * @brief Set download only new messages to global settings.
	 *
	 * @param[in] downloadOnlyNewMsgs True if it is enabled.
	*/
	Q_INVOKABLE static
	void setDownloadOnlyNewMsgs(bool downloadOnlyNewMsgs);

	/*!
	 * @brief Get database location from global settings.
	 *
	 * @return Database location string.
	*/
	Q_INVOKABLE static
	QString dbPath(void);

	/*!
	 * @brief Set new database location to global settings.
	 *
	 * @param[in] dbLocation New database location string.
	*/
	Q_INVOKABLE static
	void setDbPath(const QString &dbLocation);

	/*!
	 * @brief Get font size value from global settings.
	 *
	 * @return Font size value.
	*/
	Q_INVOKABLE static
	int fontSize(void);

	/*!
	 * @brief Set font size to global settings.
	 *
	 * @param[in] fontSize Font size value.
	 */
	Q_INVOKABLE static
	void setFontSize(int fontSize);

	/*!
	 * @brief Get font string from global settings.
	 *
	 * @return Font string.
	*/
	Q_INVOKABLE static
	QString font(void);

	/*!
	 * @brief Set font to global settings.
	 *
	 * @param[in] font Font identifier string.
	 */
	Q_INVOKABLE static
	void setFont(const QString &font);

	/*!
	 * @brief Get language string from global settings.
	 *
	 * @return Language string.
	*/
	Q_INVOKABLE static
	QString language(void);

	/*!
	 * @brief Set language to global settings.
	 *
	 * @param[in] language Language identifier string.
	 */
	Q_INVOKABLE static
	void setLanguage(const QString &language);

	/*!
	 * @brief Return current PIN value.
	 */
	Q_INVOKABLE static
	QString pinValue(void);

	/*!
	 * @brief Save PIN settings from QML.
	 *
	 * @param[in] pinValue PIN string.
	 */
	Q_INVOKABLE static
	void updatePinSettings(const QString &pinValue);

	/*!
	 * @brief Test if PIN is configured.
	 *
	 * @return True if PIN is configured.
	 */
	Q_INVOKABLE static
	bool pinConfigured(void);

	/*!
	 * @brief Verify PIN entered by user.
	 *
	 * @param[in] pinValue PIN string.
	 * @return True if PIN is valid.
	 */
	Q_INVOKABLE static
	bool pinValid(const QString &pinValue);

	/*!
	 * @brief Verify PIN entered by user and emit signal with result.
	 *
	 * @param[in] pinValue PIN string.
	 */
	Q_INVOKABLE
	void verifyPin(const QString &pinValue);

	/*!
	 * @brief Returns true if explicit clipboard operations should be used.
	 */
	Q_INVOKABLE static
	bool useExplicitClipboardOperations(void);

	/*!
	 * @brief Get inactivity timeout length.
	 *
	 * @return Inactivity interval in seconds.
	 */
	Q_INVOKABLE static
	int inactivityInterval(void);

	/*!
	 * @brief Set inactivity timeout length.
	 *
	 * @param[in] secs Inactivity interval length in seconds.
	 */
	Q_INVOKABLE static
	void setInactivityInterval(int secs);

	/*!
	 * @brief Last update string.
	 *
	 * @return String containing either date or time if date is today.
	 */
	static
	QString lastUpdateStr(void);

	/*!
	 * @brief Set last update time to current time.
	 */
	Q_INVOKABLE static
	void setLastUpdateToNow(void);

	/*!
	 * @brief Get records management url string.
	 *
	 * @return Records management url string.
	 */
	Q_INVOKABLE static
	QString rmUrl(void);

	/*!
	 * @brief Set records management url string.
	 *
	 * @param[in] rmUrl records management url string.
	 */
	Q_INVOKABLE static
	void setRmUrl(const QString &rmUrl);

	/*!
	 * @brief Get records management token string.
	 *
	 * @return Records management token string.
	 */
	Q_INVOKABLE static
	QString rmToken(void);

	/*!
	 * @brief Set records management token string.
	 *
	 * @param[in] rmToken records management token string.
	 */
	Q_INVOKABLE static
	void setRmToken(const QString &rmToken);

	/*!
	 * @brief Return debug verbosity level.
	 */
	Q_INVOKABLE static
	int debugVerbosityLevel(void);

	/*!
	 * @brief Set debug verbosity level.
	 *
	 * @param[in] dVL log verbosity level.
	 */
	Q_INVOKABLE static
	void setDebugVerbosityLevel(int dVL);

	/*!
	 * @brief Return log verbosity level.
	 */
	Q_INVOKABLE static
	int logVerbosityLevel(void);

	/*!
	 * @brief Set log verbosity level.
	 *
	 * @param[in] lVL log verbosity level.
	 */
	Q_INVOKABLE static
	void setLogVerbosityLevel(int lVL);

	/*!
	 * @brief Check whether the application has the clean start parameter set.
	 *
	 * @return True if the value is set.
	 */
	bool cleanAppStart(void) const;

	/*!
	 * @brief Set whether the application has been freshly started.
	 *
	 * @param[in] val Boolean value.
	 */
	void setCleanAppStart(bool val);

	/*!
	 * @brief Get synchronise accounts after start from global settings.
	 *
	 * @return True if it is enabled.
	*/
	Q_INVOKABLE static
	bool syncAfterCleanAppStart(void);

	/*!
	 * @brief Set synchronise accounts after start to global settings.
	 *
	 * @param[in] syncAfterAppStart True if it is enabled.
	*/
	Q_INVOKABLE static
	void setSyncAfterCleanAppStart(bool syncAfterAppStart);

	/*!
	 * @brief Get amount of days to check the password expiration.
	 *
	 * @return Amount of days to check the password expiration.
	*/
	Q_INVOKABLE static
	int pwdExpirationDays(void);

	/*!
	 * @brief Set amount of days to check the password expiration.
	 *
	 * @param[in] days Amount of days to check the password expiration.
	*/
	Q_INVOKABLE static
	void setPwdExpirationDays(int days);

	/*!
	 * @brief Get amount of days to show message deletion notification.
	 *
	 * @return Amount of days to show message deletion notification.
	*/
	Q_INVOKABLE static
	int msgDeletionNotifyAheadDays(void);

	/*!
	 * @brief Get banner visibility remaining value.
	 *
	 * @return Number of remaining banner showing.
	*/
	Q_INVOKABLE static
	int bannerVisibilityCount(void);

	/*!
	 * @brief Set banner visibility remaining value.
	 *
	 * @param[in] cnt Number of remaining banner showing.
	*/
	Q_INVOKABLE static
	void setBannerVisibilityCount(int cnt);

	/*!
	 * @brief Return true if sending of vodz message is enabled.
	 *
	 * @return True if sending of vodz message is enabled.
	 */
	Q_INVOKABLE static
	bool isVodzEnbaled(void);

	/*!
	 * @brief Get dark theme from global settings.
	 *
	 * @return True if it is enabled.
	*/
	Q_INVOKABLE static
	bool darkTheme(void);

	/*!
	 * @brief Set dark theme to global settings.
	 *
	 * @param[in] darkTheme True if it is enabled.
	*/
	Q_INVOKABLE static
	void setDarkTheme(bool darkTheme);

	/*!
	 * @brief iOS check for QML.
	 *
	 * @return True if iOS.
	 */
	Q_INVOKABLE static
	bool isIos(void);

	/*!
	 * @brief Get/store first application start-up time.
	 *
	 * @return First application launch time.
	 */
	static
	QDateTime appFirstLaunchTime(void);
	static
	void storeAppFirstLaunchTime(void);

	/*!
	 * @brief Get/store last application launch time.
	 *
	 * @return Last application launch time.
	 */
	static
	QDateTime appLastLaunchTime(void);
	static
	void storeAppLastLaunchTime(void);

	/*!
	 * @brief Get/set next donation dialogue view time.
	 *
	 * @param[in] nextViewTime Next view datetime.
	 *
	 * @return Next view datetime.
	 */
	static
	QDateTime appPlannedDonationViewTime(void);
	static
	void setAppPlannedDonationViewTime(const QDateTime &nextViewTime);

	/*!
	 * @brief Get/set next rating dialogue view time.
	 *
	 * @param[in] nextViewTime Next view datetime.
	 *
	 * @return Next view datetime.
	 */
	static
	QDateTime appPlannedRatingViewTime(void);
	static
	void setAppPlannedRatingViewTime(const QDateTime &nextViewTime);

	/*!
	 * @brief Get/set number of application launches.
	 *
	 * @return Number of application launches
	 */
	static
	qint64 appLaunchCount(void);
	static
	void incrementLaunchCount(void);

	/*!
	 * @brief Compute next view donation dialogue datetime.
	 *
	 * @param[in] shortTime True if planned short view interval.
	*/
	Q_INVOKABLE static
	void planNextDonationViewTime(bool shortTime);

	/*!
	 * @brief Compute next view rating dialogue datetime.
	 *
	 * @param[in] shortTime True if planned short view interval.
	*/
	static
	void planNextRatingViewTime(bool shortTime);

	/*!
	 * @brief Check whether to show the donation dialogue.
	 *
	 * @return True if show donation dialogue.
	*/
	Q_INVOKABLE static
	bool isTimeToShowDonationDlg(void);

	/*!
	 * @brief Check whether to show the rating dialogue.
	 *
	 * @return True if show rating dialogue.
	*/
	Q_INVOKABLE static
	bool isTimeToShowRatingDlg(void);

	/*!
	 * @brief Open application rating dialogue.
	*/
	Q_INVOKABLE static
	void openRatingDlg(void);

	/*!
	 * @brief Use QML file dialogue.
	 *
	 * @return True if use QML file dialogue.
	*/
	Q_INVOKABLE static
	bool useQmlFileDialog(void);

	/*!
	 * @brief Use iOS native document picker controller.
	 *
	 * @return True if use iOS native document picker controller.
	*/
	Q_INVOKABLE static
	bool useIosDocumentPicker(void);

	/*!
	 * @brief Check if new version after first startup.
	 *
	 * @note This check isn't performed if running app doesn't have a valid
	 *     release version.
	 *
	 * @return True if it is new version after first startup.
	*/
	Q_INVOKABLE static
	bool isNewVersion(void);

	/*!
	 * @brief Load changelog content.
	 *
	 * @return Changelog text.
	*/
	Q_INVOKABLE static
	QString loadChangeLogText(void);

signals:
	/*!
	 * @brief Send PIN verification result to QML.
	 *
	 * @param[in] success PIN comparison notification to QML.
	 */
	void sendPinReply(bool success);

	/*!
	 * @brief Run some actions after appliaction start up.
	 *
	 */
	void runOnAppStartUpSig(void);

private:
	bool m_cleanAppStart; /*!< True if application has been started as it is. No ZFO viewing intended. */
};
