/*
 * Copyright (C) 2014-2025 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

#include <QDomDocument>
#include <QDomElement>
#include <QDomNode>
#include <QString>

#include "src/datovka_shared/compat/compiler.h" /* macroStdMove */
#include "src/datovka_shared/isds/message_interface.h"
#include "src/datovka_shared/isds/type_conversion.h"
#include "src/isds/isds_const.h"
#include "src/isds/xml/helper.h"
#include "src/isds/xml/message_interface_vodz.h"
#include "src/isds/xml/soap.h"

#define UPLOAD_MTOM_XOP_ATT_ID "<inc:Include xmlns:inc=\"http://www.w3.org/2004/08/xop/include\" href=\"cid:att_1\" />"

/*!
 * @brief Read file content from DOM element node attributes.
 *
 * @param[in,out] dmFile File whose content should be updated.
 * @param[in]     elem DOM element node.
 * @return True on success, false on failure.
 */
static
bool readAttachmentAttributes(Isds::DmFile &dmFile, const QDomElement &elem)
{
	if (Q_UNLIKELY(elem.isNull())) {
		Q_ASSERT(0);
		return false;
	}

	const QDomNamedNodeMap attributeMap = elem.attributes();
	for (int i = 0; i < attributeMap.size(); ++i) {
		QDomNode node = attributeMap.item(i);
		if (Q_UNLIKELY(node.nodeType() != QDomNode::AttributeNode)) {
			Q_ASSERT(0);
			return false;
		}

		const QString localName = node.localName();

		node = node.firstChild();
		if (Q_UNLIKELY(node.nodeType() != QDomNode::TextNode)) {
			Q_ASSERT(0);
			return false;
		}

		if (localName == QLatin1String("dmFileMetaType")) {
			dmFile.setFileMetaType(
			    Isds::str2FileMetaType(node.nodeValue()));
		} else if (localName == QLatin1String("dmMimeType")) {
			dmFile.setMimeType(node.nodeValue());
		} else if (localName == QLatin1String("dmFileDescr")) {
			dmFile.setFileDescr(node.nodeValue());
		} else if (localName == QLatin1String("dmFileGuid")) {
			dmFile.setFileGuid(node.nodeValue());
		} else if (localName == QLatin1String("dmUpFileGuid")) {
			dmFile.setUpFileGuid(node.nodeValue());
		} else {
			/* There shouldn't be other content. */
			Q_ASSERT(0);
			return false;
		}
	}

	return true;
}

/*!
 * @brief Read file content from DOM element node child element nodes.
 *
 * @param[in,out] dmFile File whose content should be updated.
 * @param[in]     elem DOM element node.
 * @return True on success, false on failure.
 */
static
bool readAttachmentContent(Isds::DmFile &dmFile, const QDomElement &elem)
{
	if (Q_UNLIKELY(elem.isNull())) {
		Q_ASSERT(0);
		return false;
	}

	bool iOk = false;

	const QDomNodeList childNodes = elem.childNodes();
	for (int i = 0; i < childNodes.size(); ++i) {
		const QDomNode &node = childNodes.at(i);
		if (Q_UNLIKELY(node.nodeType() != QDomNode::ElementNode)) {
			continue;
		}
		const QDomElement elem = node.toElement();

		const QString localName = node.localName();
		if (localName == QLatin1String("dmEncodedContent")) {
			QString value = Isds::Xml::readSingleChildStringValue(elem, &iOk);
			if (iOk) {
				dmFile.setBinaryContent(
				    QByteArray::fromBase64(value.toUtf8()));
			} else {
				return false;
			}
		} else {
			/* There shouldn't be other content. */
			Q_ASSERT(0);
			return false;
		}
	}

	return true;
}

/*!
 * @brief Read attachment file entry from DOM element node.
 *
 * @param[in]  elem DOM element node.
 * @param[out] ok Set to false on error.
 * @return Attachment data, null value on error.
 */
static
Isds::DmFile readAttachment(const QDomElement &elem, bool *ok = Q_NULLPTR)
{
	if (Q_UNLIKELY(elem.isNull())) {
		if (ok != Q_NULLPTR) {
			*ok = false;
		}
		return Isds::DmFile();
	}

	Isds::DmFile dmFile;
	if (Q_UNLIKELY(!readAttachmentAttributes(dmFile, elem))) {
		goto fail;
	}
	if (Q_UNLIKELY(!readAttachmentContent(dmFile, elem))) {
		goto fail;
	}

	if (ok != Q_NULLPTR) {
		*ok = true;
	}
	return dmFile;

fail:
	if (ok != Q_NULLPTR) {
		*ok = false;
	}
	return Isds::DmFile();
}

/*!
 * @brief Write attachment entry into element node.
 *
 * @param[in]     dmExtFile Attachment identification data.
 * @param[in,out] doc DOM document.
 * @param[in]     parent DOM element node that should hold the values.
 * @return True on success.
 */
static
bool writeAttIdentification(const Isds::DmExtFile &dmExtFile,
    QDomDocument &doc, QDomElement &parent)
{
	static const QString dmExtFileTag("dmExtFile");
	static const QString dmFileMetaTypeTag("dmFileMetaType");
	static const QString dmAttIDTag("dmAttID");
	static const QString dmAttHash1Tag("dmAttHash1");
	static const QString dmAttHash1AlgTag("dmAttHash1Alg");
	static const QString dmAttHash2Tag("dmAttHash2");
	static const QString dmAttHash2AlgTag("dmAttHash2Alg");
	static const QString guidTag("dmFileGuid");
	static const QString upGuidTag("dmUpFileGuid");

	if (Q_UNLIKELY(dmExtFile.isNull())) {
		Q_ASSERT(0);
		return false;
	}

	QDomElement elem = doc.createElement(dmExtFileTag);
	elem.setAttribute(dmFileMetaTypeTag,
	    Isds::fileMetaType2Str(dmExtFile.fileMetaType()));
	elem.setAttribute(dmAttIDTag, dmExtFile.dmAtt().dmAttID());
	elem.setAttribute(dmAttHash1Tag, dmExtFile.dmAtt().dmAttHash1());
	elem.setAttribute(dmAttHash1AlgTag, dmExtFile.dmAtt().dmAttHash1Alg());
	elem.setAttribute(dmAttHash2Tag, dmExtFile.dmAtt().dmAttHash2());
	elem.setAttribute(dmAttHash2AlgTag, dmExtFile.dmAtt().dmAttHash2Alg());
	if (!dmExtFile.fileGuid().isEmpty()) {
		elem.setAttribute(guidTag, dmExtFile.fileGuid());
	}
	if (!dmExtFile.upFileGuid().isEmpty()) {
		elem.setAttribute(upGuidTag, dmExtFile.upFileGuid());
	}

	parent.appendChild(elem);

	return true;
}

/*!
 * @brief Read algorithm attribute value from supplied element node.
 *
 * @param[in] elem DOM element node.
 * @return Hash algorithm identifier. Returns HA_UNKNOWN on error.
 */
static
QString attributeAlgorithm(const QDomElement &elem)
{
	if (Q_UNLIKELY(elem.isNull())) {
		return QString();
	}

	bool iOk = false;
	const QString value = Isds::Xml::readAttributeValue(elem,
	    QLatin1String("AttHashAlg"), true, &iOk);
	if (Q_UNLIKELY(!iOk)) {
		return QString();
	}

	return value;
}

/*!
 * @brief Read hash from supplied element node content.
 *
 * @param[in]  elem DOM element node.
 * @param[out] hashAlg Hash algorithm.
 * @param[out] ok Set to false on error.
 * @return Hash string.
 */
static
QString readHash(const QDomElement &elem, QString &hashAlg,
    bool *ok = Q_NULLPTR)
{
	if (Q_UNLIKELY(elem.isNull())) {
		if (ok != Q_NULLPTR) {
			*ok = false;
		}
		return QString();
	}

	hashAlg = attributeAlgorithm(elem);
	if (Q_UNLIKELY(hashAlg.isEmpty())) {
		if (ok != Q_NULLPTR) {
			*ok = false;
		}
		return QString();
	}
	bool iOk = false;
	QString hash =
	    Isds::Xml::readSingleChildStringValue(elem, &iOk).toUtf8();
	if (Q_UNLIKELY(hash.isEmpty() || (!iOk))) {
		if (ok != Q_NULLPTR) {
			*ok = false;
		}
		return QString();
	}

	return hash;
}

Isds::DmAtt Isds::Xml::readUploadAttachmentResponse(const QByteArray &xmlData,
    bool *ok)
{
	if (Q_UNLIKELY(xmlData.isEmpty())) {
		if (ok != Q_NULLPTR) {
			*ok = false;
		}
		return DmAtt();
	}

	bool iOk = false;
	QDomDocument doc = toDomDocument(xmlData, &iOk);
	if (Q_UNLIKELY(!iOk)) {
		if (ok != Q_NULLPTR) {
			*ok = false;
		}
		return DmAtt();
	}

	QDomElement elem = findSingleDomElement(doc,
	    QLatin1String("UploadAttachmentResponse"));
	if (Q_UNLIKELY(elem.isNull())) {
		if (ok != Q_NULLPTR) {
			*ok = false;
		}
		return DmAtt();
	}

	DmAtt dmAtt;
	QString hash;
	QString hashAlg;

	const QDomNodeList childNodes = elem.childNodes();
	for (int i = 0; i < childNodes.size(); ++i) {
		const QDomNode &node = childNodes.at(i);
		if (Q_UNLIKELY(node.nodeType() != QDomNode::ElementNode)) {
			continue;
		}
		const QDomElement elem = node.toElement();
		const QString localName = elem.localName();
		if (localName == QLatin1String("dmAttID")) {
			QString value = readSingleChildStringValue(elem, &iOk);
			if (iOk) {
				if (!value.isNull()) {
					dmAtt.setDmAttID(macroStdMove(value));
				}
			} else {
				goto fail;
			}
		}
		if (localName == QLatin1String("dmAttHash1")) {
			hash = readHash(elem, hashAlg, &iOk);
			if (iOk) {
				dmAtt.setDmAttHash1(macroStdMove(hash));
				dmAtt.setDmAttHash1Alg(macroStdMove(hashAlg));
			} else {
				goto fail;
			}
		}
		hashAlg.clear();

		if (localName == QLatin1String("dmAttHash2")) {
			hash = readHash(elem, hashAlg, &iOk);
			if (iOk) {
				dmAtt.setDmAttHash2(macroStdMove(hash));
				dmAtt.setDmAttHash2Alg(macroStdMove(hashAlg));
			} else {
				goto fail;
			}
		}
	}

	if (ok != Q_NULLPTR) {
		*ok = true;
	}

	return dmAtt;
fail:
	if (ok != Q_NULLPTR) {
		*ok = false;
	}
	return DmAtt();
}

Isds::DmFile Isds::Xml::readDownloadAttachmentBase64Response(
    const QByteArray &xmlData, bool *ok)
{
	if (Q_UNLIKELY(xmlData.isEmpty())) {
		if (ok != Q_NULLPTR) {
			*ok = false;
		}
		return DmFile();
	}

	bool iOk = false;
	QDomDocument doc = toDomDocument(xmlData, &iOk);
	if (Q_UNLIKELY(!iOk)) {
		if (ok != Q_NULLPTR) {
			*ok = false;
		}
		return DmFile();
	}

	QDomElement elem = findSingleDomElement(doc,
	    QLatin1String("dmFile"));
	if (Q_UNLIKELY(elem.isNull())) {
		if (ok != Q_NULLPTR) {
			*ok = false;
		}
		return DmFile();
	}

	DmFile dmFile = readAttachment(elem, &iOk);

	if (ok != Q_NULLPTR) {
		*ok = true;
	}
	return dmFile;
}

QString Isds::Xml::readDownloadAttachmentFileName(const QByteArray &xmlData)
{
	if (Q_UNLIKELY(xmlData.isEmpty())) {
		return QString();
	}

	bool iOk = false;
	QDomDocument doc = toDomDocument(xmlData, &iOk);
	if (Q_UNLIKELY(!iOk)) {
		return QString();
	}

	QDomElement elem = findSingleDomElement(doc,
	    QLatin1String("dmFile"));
	if (Q_UNLIKELY(elem.isNull())) {
		return QString();
	}

	DmFile dmFile;
	if (Q_UNLIKELY(!readAttachmentAttributes(dmFile, elem))) {
		return QString();
	}

	return dmFile.fileDescr();
}

QByteArray Isds::Xml::soapRequestAuthenticateBigMessage(
    const QByteArray &rawData)
{
	QByteArray xmlData("<AuthenticateBigMessage><dmMessage>");
	xmlData.append(rawData.toBase64());
	xmlData.append("</dmMessage></AuthenticateBigMessage>");
	return coatWithSoapEnvelope(xmlData);
}

QByteArray Isds::Xml::soapRequestAuthenticateBigMessageMtomXop(void)
{
	QByteArray xmlData(
	    "<AuthenticateBigMessage>"
	        "<dmMessage>"
	            + QByteArray(UPLOAD_MTOM_XOP_ATT_ID) +
	        "</dmMessage>"
	    "</AuthenticateBigMessage>");
	return coatWithSoap12Envelope(xmlData);
}

/*!
 * @brief Constructs <SignedBigMessageDownload>
 *        or <SignedSentBigMessageDownload> request XML.
 *
 * @param[in] rsmd Determines request type.
 * @param[in] dmId Message identifier.
 * @return XML request.
 */
static
QByteArray soapRequestGetSignedBigMessage(
    enum Isds::Xml::RequestSignedMessageData rsmd, qint64 dmId)
{
	QByteArray xmlData;
	switch (rsmd) {
	case Isds::Xml::RSMD_RCVD_MSG:
		xmlData.append("<SignedBigMessageDownload><dmID>");
		break;
	case Isds::Xml::RSMD_SNT_MSG:
		xmlData.append("<SignedSentBigMessageDownload><dmID>");
		break;
	default:
		Q_ASSERT(0);
		return QByteArray();
		break;
	}
	xmlData.append(QString::number(dmId).toUtf8());

	switch (rsmd) {
	case Isds::Xml::RSMD_RCVD_MSG:
		xmlData.append("</dmID></SignedBigMessageDownload>");
		break;
	case Isds::Xml::RSMD_SNT_MSG:
		xmlData.append("</dmID></SignedSentBigMessageDownload>");
		break;
	default:
		Q_ASSERT(0);
		return QByteArray();
		break;
	}
	return xmlData;
}

QByteArray Isds::Xml::soapRequestGetSignedBigMessageBase64(
    enum RequestSignedMessageData rsmd, qint64 dmId)
{
	return coatWithSoapEnvelope(soapRequestGetSignedBigMessage(rsmd, dmId));
}

QByteArray Isds::Xml::soapRequestGetSignedBigMessageMtomXop(
    enum RequestSignedMessageData rsmd, qint64 dmId)
{
	return coatWithSoap12Envelope(soapRequestGetSignedBigMessage(rsmd, dmId));
}

QByteArray Isds::Xml::soapRequestUploadAttachmentBase64(
    const Isds::DmFile &dmFile)
{
	QByteArray xmlData("<UploadAttachment>"
	    "<dmFile dmMimeType=\"" + dmFile.mimeType().toUtf8() + "\" "
	        "dmFileDescr=\"" + dmFile.fileDescr().toUtf8() + "\">"
	        "<dmEncodedContent>"
	            + dmFile.binaryContent().toBase64() +
	        "</dmEncodedContent>"
	    "</dmFile></UploadAttachment>");
	return coatWithSoapEnvelope(xmlData);
}

QByteArray Isds::Xml::soapRequestUploadAttachmentMtomXop(
    const QString &dmMimeType, const QString &dmFileDescr)
{
	if (Q_UNLIKELY(dmMimeType.isEmpty() || dmFileDescr.isEmpty())) {
		Q_ASSERT(0);
		return QByteArray();
	}

	QByteArray xmlData("<UploadAttachment>"
	    "<dmFile dmMimeType=\"" + dmMimeType.toUtf8() + "\" "
	        "dmFileDescr=\"" + dmFileDescr.toUtf8() + "\">"
	        "<dmEncodedContent>"
	            + QByteArray(UPLOAD_MTOM_XOP_ATT_ID) +
	        "</dmEncodedContent>"
	    "</dmFile></UploadAttachment>");
	return coatWithSoap12Envelope(xmlData);
}

/*!
 * @brief Constructs <DownloadAttachment> request XML.
 *
 * @param[in] dmId Message identifier.
 * @param[in] attId Attachment identifier.
 * @return Request XML data.
 */
static
QByteArray soapRequestDownloadAttachment(qint64 dmId, qint64 attId)
{
	return QByteArray ("<DownloadAttachment>"
	    "<dmID>" + QString::number(dmId).toUtf8() + "</dmID>"
	    "<attNum>" + QString::number(attId).toUtf8() + "</attNum>"
	    "</DownloadAttachment>");

}

QByteArray Isds::Xml::soapRequestDownloadAttachmentBase64(qint64 dmId,
    qint64 attId)
{
	return coatWithSoapEnvelope(soapRequestDownloadAttachment(dmId, attId));
}

QByteArray Isds::Xml::soapRequestDownloadAttachmentMtomXop(qint64 dmId,
    qint64 attId)
{
	return coatWithSoap12Envelope(soapRequestDownloadAttachment(dmId, attId));
}

/*!
 * @brief Writes files list into supplied element node.
 *
 * @param[in]     documents Document list.
 * @param[in]     dmExtFiles Attachment identification list.
 * @param[in,out] doc DOM document.
 * @param[in]     parent DOM element node that should hold the values.
 * @return True on success.
 */
static
bool writeDocuments(const QList<Isds::Document> &documents,
    const QList<Isds::DmExtFile> &dmExtFiles, QDomDocument &doc,
    QDomElement &parent)
{
	static const QString filesTag("dmFiles");

	QDomElement elem = doc.createElement(filesTag);

	bool iOk = false;
	foreach (const Isds::Document &document, documents) {
		iOk = Isds::Xml::writeDocument(document, doc, elem);
		if (Q_UNLIKELY(!iOk)) {
			goto fail;
		}
	}

	foreach (const Isds::DmExtFile &dmExtFile, dmExtFiles) {
		iOk = writeAttIdentification(dmExtFile, doc, elem);
		if (Q_UNLIKELY(!iOk)) {
			goto fail;
		}
	}

	parent.appendChild(elem);

	return true;

fail:
	return false;
}

/*!
 * @brief Writes a CreateBigMessage request into a DOM document.
 *
 * @note Content of \a doc is undefined on failure.
 *
 * @param[in]     message Data message containing data to be sent.
 * @param[in,out] doc DOM document.
 * @param[in]     parent DOM element node which should hold
 *                       the created DOM tree.
 * @return True on success, false on any error.
 */
static
bool writeCreateBigMessage(const Isds::Message &message,
    QDomDocument &doc, QDomElement &parent)
{
	static const QString cmTag("CreateBigMessage");
	static const QString cmNs("xmlns:v20");
	static const QString cmNsUri(ISDS_XML_NS);

	if (Q_UNLIKELY(message.isNull() || doc.isNull() || parent.isNull())) {
		Q_ASSERT(0);
		return false;
	}

	QDomElement elem = doc.createElement(cmTag);
	elem.setAttribute(cmNs, cmNsUri);
	parent.appendChild(elem);

	bool iOk = Isds::Xml::writeEnvelope(message.envelope(), doc, elem);
	if (Q_UNLIKELY(!iOk)) {
		return false;
	}
	iOk = writeDocuments(message.documents(), message.extFiles(), doc, elem);
	if (Q_UNLIKELY(!iOk)) {
		return false;
	}

	return true;
}

QByteArray Isds::Xml::soapRequestCreateBigMessage(const Message &message,
    bool *ok)
{
	QDomDocument doc = buildEmptyDocument();
	QDomElement elem = buildSoap11Envelope(doc);
	bool iOk = writeCreateBigMessage(message, doc, elem);
	if (ok != Q_NULLPTR) {
		*ok = iOk;
	}
	return doc.toByteArray(-1);
}
