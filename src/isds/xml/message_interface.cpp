/*
 * Copyright (C) 2014-2025 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

#include <QDateTime>
#include <QDomDocument>
#include <QDomElement>
#include <QDomNode>
#include <QString>
#include <QXmlStreamReader>

#include "src/datovka_shared/compat/compiler.h" /* macroStdMove */
#include "src/datovka_shared/isds/box_interface2.h"
#include "src/datovka_shared/isds/type_conversion.h"
#include "src/datovka_shared/isds/types.h"
#include "src/isds/conversion/isds_time_conversion.h" /* isoDateTimeStrToUtcDateTime() */
#include "src/isds/conversion/isds_type_conversion.h"
#include "src/isds/isds_const.h" /* ISDS_XML_NS */
#include "src/isds/xml/helper.h"
#include "src/isds/xml/message_interface.h"
#include "src/isds/xml/soap.h"

enum Isds::Type::RawType Isds::Xml::guessRawType(const QDomDocument &dom)
{
	static const QString icomming("http://isds.czechpoint.cz/v20/message");
	static const QString outgoing("http://isds.czechpoint.cz/v20/SentMessage");
	static const QString delivery("http://isds.czechpoint.cz/v20/delivery");

	const QDomElement element = dom.documentElement();
	if (Q_UNLIKELY(element.isNull())) {
		return Type::RT_UNKNOWN;
	}

	const QString nsUri = element.namespaceURI();
	if (nsUri == icomming) {
		return Type::RT_PLAIN_SIGNED_INCOMING_MESSAGE;
	} else if (nsUri == outgoing) {
		return Type::RT_PLAIN_SIGNED_OUTGOING_MESSAGE;
	} else if (nsUri == delivery) {
		return Type::RT_PLAIN_SIGNED_DELIVERYINFO;
	} else {
		return Type::RT_UNKNOWN;
	}
}

/*!
 * @brief Read attribute value from supplied element node.
 *
 * @param[in] elem DOM element node whose attributes should be accessed.
 * @param[in] attribute Attribute name.
 * @return String value or null value on error or if not present.
 */
static
QString readAttributeValue(const QDomElement &elem, const QString &attrName)
{
	if (Q_UNLIKELY(elem.isNull())) {
		return QString();
	}

	bool iOk = false;
	const QString value = Isds::Xml::readAttributeValue(elem, attrName,
	    false, &iOk);
	if (Q_UNLIKELY(!iOk)) {
		return QString();
	}
	/* The attribute may not be present. */
	if (value.isNull()) {
		return QString();
	}
	return value;
}

/*!
 * @brief Read dmType attribute value from supplied element node.
 *
 * @param[in] node DOM node.
 * @return Type character, null value on error or if not present.
 */
static
QChar attributeDmType(const QDomElement &elem)
{
	if (Q_UNLIKELY(elem.isNull())) {
		return QChar();
	}

	bool iOk = false;
	const QString value = Isds::Xml::readAttributeValue(elem,
	    QLatin1String("dmType"), false, &iOk);
	if (Q_UNLIKELY(!iOk)) {
		return QChar();
	}
	/* The attribute may not be present. */
	if (value.isNull()) {
		return QChar();
	}
	if (Q_UNLIKELY(value.size() != 1)) {
		Q_ASSERT(0);
		return QChar();
	}
	return value.at(0);
}

/*!
 * @brief Read algorithm attribute value from supplied element node.
 *
 * @param[in] elem DOM element node.
 * @return Hash algorithm identifier. Returns HA_UNKNOWN on error.
 */
static
enum Isds::Type::HashAlg attributeAlgorithm(const QDomElement &elem)
{
	if (Q_UNLIKELY(elem.isNull())) {
		return Isds::Type::HA_UNKNOWN;
	}

	bool iOk = false;
	const QString value = Isds::Xml::readAttributeValue(elem,
	    QLatin1String("algorithm"), true, &iOk);
	if (Q_UNLIKELY(!iOk)) {
		return Isds::Type::HA_UNKNOWN;
	}

	return Isds::str2HashAlg(value);
}

/*!
 * @brief Read hash from supplied element node content.
 *
 * @param[in]  elem DOM element node.
 * @param[out] ok Set to false on error.
 * @return Hash, null value on error.
 */
static
Isds::Hash readHash(const QDomElement &elem, bool *ok = Q_NULLPTR)
{
	if (Q_UNLIKELY(elem.isNull())) {
		if (ok != Q_NULLPTR) {
			*ok = false;
		}
		return Isds::Hash();
	}

	enum Isds::Type::HashAlg hashAlg = attributeAlgorithm(elem);
	if (Q_UNLIKELY(hashAlg == Isds::Type::HA_UNKNOWN)) {
		if (ok != Q_NULLPTR) {
			*ok = false;
		}
		return Isds::Hash();
	}
	bool iOk = false;
	QByteArray hashVal = QByteArray::fromBase64(
	    Isds::Xml::readSingleChildStringValue(elem, &iOk).toUtf8());
	if (Q_UNLIKELY(hashVal.isEmpty() || (!iOk))) {
		if (ok != Q_NULLPTR) {
			*ok = false;
		}
		return Isds::Hash();
	}

	/* TODO -- Check whether hash length correlates with hash type. */

	Isds::Hash hash;
	hash.setAlgorithm(hashAlg);
	hash.setValue(macroStdMove(hashVal));
	if (ok != Q_NULLPTR) {
		*ok = true;
	}
	return hash;
}

/*!
 * @brief Read event content from DOM element node.
 *
 * @param[in]  elem DOM element node.
 * @param[out] ok Set to false on error.
 * @return Event, null value on error.
 */
static
Isds::Event readEvent(const QDomElement &elem, bool *ok = Q_NULLPTR)
{
	if (Q_UNLIKELY(elem.isNull())) {
		if (ok != Q_NULLPTR) {
			*ok = false;
		}
		return Isds::Event();
	}

	bool iOk = false;
	QString value;
	Isds::Event event;

	const QDomNodeList childNodes = elem.childNodes();
	for (int i = 0; i < childNodes.size(); ++i) {
		const QDomNode &node = childNodes.at(i);
		if (Q_UNLIKELY(node.nodeType() != QDomNode::ElementNode)) {
			continue;
		}
		const QDomElement elem = node.toElement();

		const QString localName = elem.localName();

		if (localName == QLatin1String("dmEventTime")) {
			value = Isds::Xml::readSingleChildStringValue(elem, &iOk);
			if (iOk) {
				event.setTime(Isds::isoDateTimeStrToUtcDateTime(value));
			} else {
				goto fail;
			}
		} else if (localName == QLatin1String("dmEventDescr")) {
			value = Isds::Xml::readSingleChildStringValue(elem, &iOk);
			if (iOk) {
				event.setType(Isds::eventDescr2EventType(value));
				event.setDescr(macroStdMove(value));
			} else {
				goto fail;
			}
		} else {
			/* There shouldn't be other content. */
			Q_ASSERT(0);
			goto fail;
		}
	}

	if (ok != Q_NULLPTR) {
		*ok = true;
	}
	return event;

fail:
	if (ok != Q_NULLPTR) {
		*ok = false;
	}
	return Isds::Event();
}

/*!
 * @brief Read event list content from DOM element node.
 *
 * @param[in]  elem DOM element node.
 * @param[out] ok Set to false on error.
 * @return Event list, empty list on error.
 */
static
QList<Isds::Event> readEvents(const QDomElement &elem, bool *ok = Q_NULLPTR)
{
	if (Q_UNLIKELY(elem.isNull())) {
		if (ok != Q_NULLPTR) {
			*ok = false;
		}
		return QList<Isds::Event>();
	}

	bool iOk = false;
	QList<Isds::Event> events;

	const QDomNodeList childNodes = elem.childNodes();
	for (int i = 0; i < childNodes.size(); ++i) {
		const QDomNode &node = childNodes.at(i);
		if (Q_UNLIKELY(node.nodeType() != QDomNode::ElementNode)) {
			continue;
		}
		const QDomElement elem = node.toElement();

		if (elem.localName() == QLatin1String("dmEvent")) {
			Isds::Event event = readEvent(elem, &iOk);
			if (Q_UNLIKELY(!iOk)) {
				goto fail;
			}
			events.append(event);
		} else {
			/* There shouldn't be other content. */
			Q_ASSERT(0);
			goto fail;
		}
	}

	if (ok != Q_NULLPTR) {
		*ok = true;
	}
	return events;

fail:
	if (ok != Q_NULLPTR) {
		*ok = false;
	}
	return QList<Isds::Event>();
}

/*!
 * @brief Gather available envelope data from supplied node.
 *
 * @note Unknown or missing entries are ignored.
 *
 * @param[in,out] env Envelope whose content should be modified.
 * @param[in]     elem DOM element node whose children should be scanned for data.
 * @param[in]     readDmType If true, the try to read the optional dmType
 *                           attribute from the root node.
 * @return False on failure, true on success.
 */
static
bool readEnvelopeContent(Isds::Envelope &env, const QDomElement &elem,
   bool readDmType = false)
{
	if (Q_UNLIKELY(elem.isNull())) {
		Q_ASSERT(0);
		return false;
	}

	bool iOk = false;
	QString value;

	if (readDmType) {
		QChar dmType = attributeDmType(elem);
		/*
		 * The dmType attribute may be unspecified. According to
		 * the ISDS documentation, if dmType is null or missing then
		 * message type defaults to 'V'.
		 *
		 * The enumeration values correlate to actual character values.
		*/
		if (!dmType.isNull()) {
			env.setDmType(Isds::Envelope::dmType2Char(
			    Isds::Envelope::char2DmType(dmType)));
		} else {
			env.setDmType(
			    Isds::Envelope::dmType2Char(Isds::Type::MT_V));
		}
	}

	env.setDmVODZ(Isds::str2BoolType(
	    readAttributeValue(elem, QLatin1String("dmVODZ"))));

	value = readAttributeValue(elem, QLatin1String("attsNum"));
	if (!value.isNull()) {
		qint64 val = value.toLongLong(&iOk);
		if (iOk) {
			env.setAttsNum(val);
		}
	}

	const QDomNodeList childNodes = elem.childNodes();
	for (int i = 0; i < childNodes.size(); ++i) {
		const QDomNode &node = childNodes.at(i);
		if (Q_UNLIKELY(node.nodeType() != QDomNode::ElementNode)) {
			continue;
		}
		const QDomElement elem = node.toElement();

		const QString localName = elem.localName();

		/*
		 * When parsing <MessageDownloadResponse> then:
		 *   <dmReturnedMessage> contains:
		 *     <dmDm>
		 *   <dmDm> contains:
		 *      <dmID> to <dmAllowSubstDelivery>
		 *   <dmReturnedMessage> or <dmDelivery> contain:
		 *      <dmHash>, <dmQTimestamp>, <dmEvents>, <dmDeliveryTime>,
		 *      <dmAcceptanceTime>, <dmMessageStatus>,
		 *      <dmAttachmentSize>
		 *
		 * Therefore this function must be called twice. Once on <dmDm>
		 * and once on <dmReturnedMessage> to obtain complete envelope
		 * data.
		 *
		 * When parsing <GetListOfReceivedMessagesResponse> or
		 * <GetListOfSentMessagesResponse> then:
		 *   <dmRecord> contains:
		 *     <dmOrdinal>, <dmID>, <dbIDSender>, <dmSender>,
		 *     <dmSenderAddress>, <dmSenderType>, <dmRecipient>,
		 *     <dmRecipientAddress>, <dmSenderOrgUnit>,
		 *     <dmSenderOrgUnitNum>, <dbIDRecipient>, <dmRecipientOrgUnit>,
		 *     <dmRecipientOrgUnitNum>, <dmToHands>, <dmAnnotation>,
		 *     <dmRecipientRefNumber>, <dmSenderRefNumber>, <dmRecipientIdent>,
		 *     <dmSenderIdent>, <dmLegalTitleLaw>, <dmLegalTitleYear>,
		 *     <dmLegalTitleSect>, <dmLegalTitlePar>, <dmLegalTitlePoint>,
		 *     <dmPersonalDelivery>, <dmAllowSubstDelivery>, <dmMessageStatus>,
		 *     <dmAttachmentSize>, <dmDeliveryTime>, <dmAcceptanceTime>
		 */
		/* GetListOf*MessagesResponse/dmRecords/dmRecord content: */
		if (localName == QLatin1String("dmOrdinal")) {
			value = Isds::Xml::readSingleChildStringValue(elem, &iOk);
			if (iOk) {
				if (!value.isNull()) {
					qint64 val = value.toLongLong(&iOk);
					if (iOk) {
						env.setDmOrdinal(val);
					}
				}
			} else {
				goto fail;
			}
		/* MessageDownloadResponse/dmReturnedMessage/dmDm content: */
		} else if (localName == QLatin1String("dmID")) {
			value = Isds::Xml::readSingleChildStringValue(elem, &iOk);
			if (iOk) {
				env.setDmID(macroStdMove(value));
			} else {
				goto fail;
			}
		} else if (localName == QLatin1String("dbIDSender")) {
			value = Isds::Xml::readSingleChildStringValue(elem, &iOk);
			if (iOk) {
				env.setDbIDSender(macroStdMove(value));
			} else {
				goto fail;
			}
		} else if (localName == QLatin1String("dmSender")) {
			value = Isds::Xml::readSingleChildStringValue(elem, &iOk);
			if (iOk) {
				env.setDmSender(macroStdMove(value));
			} else {
				goto fail;
			}
		} else if (localName == QLatin1String("dmSenderAddress")) {
			value = Isds::Xml::readSingleChildStringValue(elem, &iOk);
			if (iOk) {
				env.setDmSenderAddress(macroStdMove(value));
			} else {
				goto fail;
			}
		} else if (localName == QLatin1String("dmSenderType")) {
			value = Isds::Xml::readSingleChildStringValue(elem, &iOk);
			if (iOk) {
				if (!value.isNull()) {
					int val = value.toInt(&iOk);
					if (iOk) {
						enum Isds::Type::DbType type =
						    Isds::long2DbType(val, &iOk);
						if (iOk) {
							env.setDmSenderType(type);
						}
					}
				}
			} else {
				goto fail;
			}
		} else if (localName == QLatin1String("dmRecipient")) {
			value = Isds::Xml::readSingleChildStringValue(elem, &iOk);
			if (iOk) {
				env.setDmRecipient(macroStdMove(value));
			} else {
				goto fail;
			}
		} else if (localName == QLatin1String("dmRecipientAddress")) {
			value = Isds::Xml::readSingleChildStringValue(elem, &iOk);
			if (iOk) {
				env.setDmRecipientAddress(macroStdMove(value));
			} else {
				goto fail;
			}
		} else if (localName == QLatin1String("dmAmbiguousRecipient")) {
			value = Isds::Xml::readSingleChildStringValue(elem, &iOk);
			if (iOk) {
				env.setDmAmbiguousRecipient(Isds::str2BoolType(value));
			} else {
				goto fail;
			}
		} else if (localName == QLatin1String("dmSenderOrgUnit")) {
			value = Isds::Xml::readSingleChildStringValue(elem, &iOk);
			if (iOk) {
				env.setDmSenderOrgUnit(macroStdMove(value));
			} else {
				goto fail;
			}
		} else if (localName == QLatin1String("dmSenderOrgUnitNum")) {
			value = Isds::Xml::readSingleChildStringValue(elem, &iOk);
			if (iOk) {
				if (!value.isNull()) {
					qint64 val = value.toLongLong(&iOk);
					if (iOk) {
						env.setDmSenderOrgUnitNum(val);
					}
				}
			} else {
				goto fail;
			}
		} else if (localName == QLatin1String("dbIDRecipient")) {
			value = Isds::Xml::readSingleChildStringValue(elem, &iOk);
			if (iOk) {
				env.setDbIDRecipient(macroStdMove(value));
			} else {
				goto fail;
			}
		} else if (localName == QLatin1String("dmRecipientOrgUnit")) {
			value = Isds::Xml::readSingleChildStringValue(elem, &iOk);
			if (iOk) {
				env.setDmRecipientOrgUnit(macroStdMove(value));
			} else {
				goto fail;
			}
		} else if (localName == QLatin1String("dmRecipientOrgUnitNum")) {
			value = Isds::Xml::readSingleChildStringValue(elem, &iOk);
			if (iOk) {
				if (!value.isNull()) {
					qint64 val = value.toLongLong(&iOk);
					if (iOk) {
						env.setDmRecipientOrgUnitNum(val);
					}
				}
			} else {
				goto fail;
			}
		} else if (localName == QLatin1String("dmToHands")) {
			value = Isds::Xml::readSingleChildStringValue(elem, &iOk);
			if (iOk) {
				env.setDmToHands(macroStdMove(value));
			} else {
				goto fail;
			}
		} else if (localName == QLatin1String("dmAnnotation")) {
			value = Isds::Xml::readSingleChildStringValue(elem, &iOk);
			if (iOk) {
				env.setDmAnnotation(macroStdMove(value));
			} else {
				goto fail;
			}
		} else if (localName == QLatin1String("dmRecipientRefNumber")) {
			value = Isds::Xml::readSingleChildStringValue(elem, &iOk);
			if (iOk) {
				env.setDmRecipientRefNumber(macroStdMove(value));
			} else {
				goto fail;
			}
		} else if (localName == QLatin1String("dmSenderRefNumber")) {
			value = Isds::Xml::readSingleChildStringValue(elem, &iOk);
			if (iOk) {
				env.setDmSenderRefNumber(macroStdMove(value));
			} else {
				goto fail;
			}
		} else if (localName == QLatin1String("dmRecipientIdent")) {
			value = Isds::Xml::readSingleChildStringValue(elem, &iOk);
			if (iOk) {
				env.setDmRecipientIdent(macroStdMove(value));
			} else {
				goto fail;
			}
		} else if (localName == QLatin1String("dmSenderIdent")) {
			value = Isds::Xml::readSingleChildStringValue(elem, &iOk);
			if (iOk) {
				env.setDmSenderIdent(macroStdMove(value));
			} else {
				goto fail;
			}
		} else if (localName == QLatin1String("dmLegalTitleLaw")) {
			value = Isds::Xml::readSingleChildStringValue(elem, &iOk);
			if (iOk) {
				if (!value.isNull()) {
					qint64 val = value.toLongLong(&iOk);
					if (iOk) {
						env.setDmLegalTitleLaw(val);
					}
				}
			} else {
				goto fail;
			}
		} else if (localName == QLatin1String("dmLegalTitleYear")) {
			value = Isds::Xml::readSingleChildStringValue(elem, &iOk);
			if (iOk) {
				if (!value.isNull()) {
					qint64 val = value.toLongLong(&iOk);
					if (iOk) {
						env.setDmLegalTitleYear(val);
					}
				}
			} else {
				goto fail;
			}
		} else if (localName == QLatin1String("dmLegalTitleSect")) {
			value = Isds::Xml::readSingleChildStringValue(elem, &iOk);
			if (iOk) {
				env.setDmLegalTitleSect(macroStdMove(value));
			} else {
				goto fail;
			}
		} else if (localName == QLatin1String("dmLegalTitlePar")) {
			value = Isds::Xml::readSingleChildStringValue(elem, &iOk);
			if (iOk) {
				env.setDmLegalTitlePar(macroStdMove(value));
			} else {
				goto fail;
			}
		} else if (localName == QLatin1String("dmLegalTitlePoint")) {
			value = Isds::Xml::readSingleChildStringValue(elem, &iOk);
			if (iOk) {
				env.setDmLegalTitlePoint(macroStdMove(value));
			} else {
				goto fail;
			}
		} else if (localName == QLatin1String("dmPersonalDelivery")) {
			value = Isds::Xml::readSingleChildStringValue(elem, &iOk);
			if (iOk) {
				env.setDmPersonalDelivery(Isds::str2BoolType(value));
			} else {
				goto fail;
			}
		} else if (localName == QLatin1String("dmAllowSubstDelivery")) {
			value = Isds::Xml::readSingleChildStringValue(elem, &iOk);
			if (iOk) {
				env.setDmAllowSubstDelivery(Isds::str2BoolType(value));
			} else {
				goto fail;
			}
		/* MessageDownloadResponse/dmReturnedMessage content: */
		} else if (localName == QLatin1String("dmMessageStatus")) {
			value = Isds::Xml::readSingleChildStringValue(elem, &iOk);
			if (iOk) {
				env.setDmMessageStatus(Isds::variant2DmState(value));
			} else {
				goto fail;
			}
		} else if (localName == QLatin1String("dmAttachmentSize")) {
			value = Isds::Xml::readSingleChildStringValue(elem, &iOk);
			if (iOk) {
				if (!value.isNull()) {
					qint64 val = value.toLongLong(&iOk);
					if (iOk) {
						env.setDmAttachmentSize(val);
					}
				}
			} else {
				goto fail;
			}
		} else if (localName == QLatin1String("dmDeliveryTime")) {
			value = Isds::Xml::readSingleChildStringValue(elem, &iOk);
			if (iOk) {
				env.setDmDeliveryTime(Isds::isoDateTimeStrToUtcDateTime(value));
			} else {
				goto fail;
			}
		} else if (localName == QLatin1String("dmAcceptanceTime")) {
			value = Isds::Xml::readSingleChildStringValue(elem, &iOk);
			if (iOk) {
				env.setDmAcceptanceTime(Isds::isoDateTimeStrToUtcDateTime(value));
			} else {
				goto fail;
			}
		} else if (localName == QLatin1String("dmHash")) {
			Isds::Hash hash = readHash(elem, &iOk);
			if (iOk) {
				env.setDmHash(macroStdMove(hash));
			} else {
				goto fail;
			}
		} else if (localName == QLatin1String("dmQTimestamp")) {
			value = Isds::Xml::readSingleChildStringValue(elem, &iOk);
			if (iOk) {
				env.setDmQTimestamp(
				    QByteArray::fromBase64(value.toUtf8()));
			} else {
				goto fail;
			}
		} else if (localName == QLatin1String("dmEvents")) {
			QList<Isds::Event> events = readEvents(elem, &iOk);
			if (iOk) {
				env.setDmEvents(macroStdMove(events));
			} else {
				goto fail;
			}
		} else {
			/*
			 * There may be other entries in the supplied nodes
			 * but we shall ignore them.
			 */
		}

		/*
		 * TODO
		 * dmOrdinal
		 *
		 * Ignoring (because these are used only inside CreateMessage):
		 *   dmOVM, dmPublishOwnID
		 */
	}

	return true;

fail:
	return false;
}

Isds::Envelope Isds::Xml::readEnvelope(const QDomDocument &dom, bool *ok)
{
	bool iOk = false;
	Envelope envelope;
	const QDomElement elem = findSingleDomElement(dom, QLatin1String("dmDm"));
	const QDomElement msgElem = findSingleDomElement(dom, QLatin1String("dmReturnedMessage"));
	const QDomElement delinfoElem = findSingleDomElement(dom, QLatin1String("dmDelivery"));

	if (Q_UNLIKELY(elem.isNull())) {
		Q_ASSERT(0);
		goto fail;
	}
	if (Q_UNLIKELY(((msgElem.isNull() && delinfoElem.isNull())) ||
	               (((!msgElem.isNull()) && (!delinfoElem.isNull()))))) {
		Q_ASSERT(0);
		goto fail;
	}

	iOk = readEnvelopeContent(envelope, elem);
	if (Q_UNLIKELY(!iOk)) {
		Q_ASSERT(0);
		goto fail;
	}
	if (!msgElem.isNull()) {
		/*
		 * Documentation of SignedMessageDownload service says that
		 * <dmReturnedMessage> can contain the dmType attribute.
		 */
		iOk = readEnvelopeContent(envelope, msgElem, true);
		if (Q_UNLIKELY(!iOk)) {
			Q_ASSERT(0);
			goto fail;
		}
	}
	if (!delinfoElem.isNull()) {
		iOk = readEnvelopeContent(envelope, delinfoElem);
		if (Q_UNLIKELY(!iOk)) {
			Q_ASSERT(0);
			goto fail;
		}
	}

	if (ok != Q_NULLPTR) {
		*ok = true;
	}
	return envelope;

fail:
	if (ok != Q_NULLPTR) {
		*ok = false;
	}
	return Envelope();
}

/*!
 * @brief Read document content from DOM element node attributes.
 *
 * @param[in,out] document Document whose content should be updated.
 * @param[in]     elem DOM element node.
 * @return True on success, false on failure.
 */
static
bool readDocumentAttributes(Isds::Document &document, const QDomElement &elem)
{
	if (Q_UNLIKELY(elem.isNull())) {
		Q_ASSERT(0);
		return false;
	}

	const QDomNamedNodeMap attributeMap = elem.attributes();
	for (int i = 0; i < attributeMap.size(); ++i) {
		QDomNode node = attributeMap.item(i);
		if (Q_UNLIKELY(node.nodeType() != QDomNode::AttributeNode)) {
			Q_ASSERT(0);
			return false;
		}

		const QString localName = node.localName();

		node = node.firstChild();
		if (Q_UNLIKELY(node.nodeType() != QDomNode::TextNode)) {
			Q_ASSERT(0);
			return false;
		}

		if (localName == QLatin1String("dmMimeType")) {
			document.setMimeType(node.nodeValue());
		} else if (localName == QLatin1String("dmFileMetaType")) {
			document.setFileMetaType(
			    Isds::str2FileMetaType(node.nodeValue()));
		} else if (localName == QLatin1String("dmFileGuid")) {
			document.setFileGuid(node.nodeValue());
		} else if (localName == QLatin1String("dmUpFileGuid")) {
			document.setUpFileGuid(node.nodeValue());
		} else if (localName == QLatin1String("dmFileDescr")) {
			document.setFileDescr(node.nodeValue());
		} else if (localName == QLatin1String("dmFormat")) {
			document.setFormat(node.nodeValue());
		} else {
			/* There shouldn't be other content. */
			Q_ASSERT(0);
			return false;
		}
	}

	return true;
}

/*!
 * @brief Read message author item from DOM element node attributes.
 *
 * @param[in] attributeMap Node attribute map.
 * @param[out] key Key name.
 * @param[out] value Key value.
 * @return True on success, false on failure.
 */
static
bool readMessageAuthorItem(const QDomNamedNodeMap &attributeMap,
    QString &key, QString &value)
{
	for (int i = 0; i < attributeMap.size(); ++i) {
		QDomNode node = attributeMap.item(i);
		if (Q_UNLIKELY(node.nodeType() != QDomNode::AttributeNode)) {
			Q_ASSERT(0);
			return false;
		}
		const QString localName = node.localName();
		node = node.firstChild();
		if (Q_UNLIKELY(node.nodeType() != QDomNode::TextNode)) {
			Q_ASSERT(0);
			return false;
		}
		if (localName == QLatin1String("key")) {
			key = node.nodeValue();
		} else if (localName == QLatin1String("value")) {
			value = node.nodeValue();
		}
	}

	return (!key.isEmpty() && !value.isEmpty());
}

/*!
 * @brief Read message author info content from DOM element node.
 *
 * @param[in] elem DOM element node.
 * @param[out] msgAuthor Message author info whose content should be updated.
 * @return True on success, false on failure.
 */
static
bool readMessageAuthorItems(const QDomElement &elem,
    Isds::DmMessageAuthor &msgAuthor)
{
	if (Q_UNLIKELY(elem.isNull())) {
		Q_ASSERT(0);
		return false;
	}

	Isds::PersonName2 pn;
	QString key, value;

	const QDomNodeList childNodes = elem.childNodes();
	for (int i = 0; i < childNodes.size(); ++i) {
		const QDomNode &node = childNodes.at(i);
		if (Q_UNLIKELY(node.nodeType() != QDomNode::ElementNode)) {
			continue;
		}
		const QDomElement elem = node.toElement();
		if (elem.localName() == QLatin1String("maItem")) {
			if (Q_UNLIKELY(!readMessageAuthorItem(elem.attributes(),
			        key, value))) {
				return false;
			}
			if (key == QLatin1String("userType")) {
				msgAuthor.setUserType(Isds::str2SenderType(value));
			} else if (key == QLatin1String("pnGivenNames")) {
				pn.setGivenNames(value);
			} else if (key == QLatin1String("pnLastName")) {
				pn.setLastName(value);
			} else if (key == QLatin1String("biDate")) {
				msgAuthor.setBiDate(Isds::isoDateStrToDate(value));
			} else if (key == QLatin1String("biCity")) {
				msgAuthor.setBiCity(value);
			} else if (key == QLatin1String("biCounty")) {
				msgAuthor.setBiCounty(value);
			} else if (key == QLatin1String("adCode")) {
				msgAuthor.setAdCode(value);
			} else if (key == QLatin1String("fullAddress")) {
				msgAuthor.setFullAddress(value);
			} else if (key == QLatin1String("robIdent")) {
				msgAuthor.setRobIdent(Isds::str2BoolType(value));
			} else {
				/* There shouldn't be other content. */
				Q_ASSERT(0);
				return false;
			}
		} else {
			Q_ASSERT(0);
		}
	}

	msgAuthor.setPersonName(pn);

	return true;
}

/*!
 * @brief Read document content from DOM element node child element nodes.
 *
 * @node Does not handle <dmXMLContent> entries.
 *
 * @param[in,out] document Document whose content should be updated.
 * @param[in]     elem DOM element node.
 * @return True on success, false on failure.
 */
static
bool readDocumentContent(Isds::Document &document, const QDomElement &elem)
{
	if (Q_UNLIKELY(elem.isNull())) {
		Q_ASSERT(0);
		return false;
	}

	bool iOk = false;

	const QDomNodeList childNodes = elem.childNodes();
	for (int i = 0; i < childNodes.size(); ++i) {
		const QDomNode &node = childNodes.at(i);
		if (Q_UNLIKELY(node.nodeType() != QDomNode::ElementNode)) {
			continue;
		}
		const QDomElement elem = node.toElement();

		const QString localName = node.localName();

		if (localName == QLatin1String("dmEncodedContent")) {
			QString value = Isds::Xml::readSingleChildStringValue(elem, &iOk);
			if (iOk) {
				document.setBinaryContent(
				    QByteArray::fromBase64(value.toUtf8()));
			} else {
				return false;
			}
		} else if (localName == QLatin1String("dmXMLContent")) {
			/* Not supported. */
			Q_ASSERT(0);
			return false;
		} else {
			/* There shouldn't be other content. */
			Q_ASSERT(0);
			return false;
		}
	}

	return true;
}

/*!
 * @brief Read document entry from DOM element node.
 *
 * @param[in]  elem DOM element node.
 * @param[out] ok Set to false on error.
 * @return Document, null value on error.
 */
static
Isds::Document readDocument(const QDomElement &elem, bool *ok = Q_NULLPTR)
{
	if (Q_UNLIKELY(elem.isNull())) {
		if (ok != Q_NULLPTR) {
			*ok = false;
		}
		return Isds::Document();
	}

	Isds::Document document;
	if (Q_UNLIKELY(!readDocumentAttributes(document, elem))) {
		goto fail;
	}
	if (Q_UNLIKELY(!readDocumentContent(document, elem))) {
		goto fail;
	}

	if (ok != Q_NULLPTR) {
		*ok = true;
	}
	return document;

fail:
	if (ok != Q_NULLPTR) {
		*ok = false;
	}
	return Isds::Document();
}

QList<Isds::Document> Isds::Xml::readDocuments(const QDomDocument &dom,
    bool *ok)
{
	const QDomNode elem = findSingleDomElement(dom, QLatin1String("dmFiles"));

	if (Q_UNLIKELY(elem.isNull())) {
		/* No attachment list found, treat this as an error. */
		if (ok != Q_NULLPTR) {
			*ok = false;
		}
		return QList<Document>();
	}

	bool iOk = false;
	QList<Document> documents;

	const QDomNodeList childNodes = elem.childNodes();
	for (int i = 0; i < childNodes.size(); ++i) {
		const QDomNode &node = childNodes.at(i);
		if (Q_UNLIKELY(node.nodeType() != QDomNode::ElementNode)) {
			continue;
		}
		const QDomElement elem = node.toElement();

		if (elem.localName() == QLatin1String("dmFile")) {
			Document doc = readDocument(elem, &iOk);
			if (iOk) {
				documents.append(doc);
			} else {
				goto fail;
			}
		} else {
			/* There shouldn't be other content. */
			Q_ASSERT(0);
			goto fail;
		}
	}

	if (ok != Q_NULLPTR) {
		*ok = true;
	}
	return documents;

fail:
	if (ok != Q_NULLPTR) {
		*ok = false;
	}
	return QList<Document>();
}

/*!
 * @brief Write DOM element.
 *
 * @param[in]     tagName Element tag name.
 * @param[in]     textValue Text value.
 * @param[in,out] doc DOM document.
 * @param[in]     parent DOM element node that should hold the created node.
 * @return True on success.
 */
static
bool writeElement(const QString &tagName, const QString &textValue,
    QDomDocument &doc, QDomElement &parent)
{
	if (Q_UNLIKELY(tagName.isEmpty())) {
		Q_ASSERT(0);
		return false;
	}

	if (textValue.isNull()) {
		/* Don't generate element node with null content. */
		return true;
	}

	QDomElement elem = doc.createElement(tagName);
	QDomText text = doc.createTextNode(textValue);
	elem.appendChild(text);
	parent.appendChild(elem);

	return true;
}

/*!
 * @brief Write DOM element.
 *
 * @note Negative values are ignored.
 *
 * @param[in]     tagName Element tag name.
 * @param[in]     num Numerical value.
 * @param[in,out] doc DOM document.
 * @param[in]     parent DOM element node that should hold the created node.
 * @return True on success.
 */
static
bool writeElementNum(const QString &tagName, qint64 num, QDomDocument &doc,
    QDomElement &parent)
{
	if (Q_UNLIKELY(tagName.isEmpty())) {
		Q_ASSERT(0);
		return false;
	}

	if (num < 0) {
		/* Don't generate element node with negative values. */
		return true;
	}

	QDomElement elem = doc.createElement(tagName);
	QDomText text = doc.createTextNode(QString::number(num));
	elem.appendChild(text);
	parent.appendChild(elem);

	return true;
}

/*!
 * @brief Write DOM element.
 *
 * @note Null values are ignored.
 *
 * @param[in]     tagName Element tag name.
 * @param[in]     val Boolean value.
 * @param[in,out] doc DOM document.
 * @param[in]     parent DOM element node that should hold the created node.
 * @return True on success.
 */
static
bool writeElementBool(const QString &tagName, enum Isds::Type::NilBool val,
    QDomDocument &doc, QDomElement &parent)
{
	if (Q_UNLIKELY(tagName.isEmpty())) {
		Q_ASSERT(0);
		return false;
	}

	if (val == Isds::Type::BOOL_NULL) {
		/* Don't generate element node with null values. */
		return true;
	}

	QDomElement elem = doc.createElement(tagName);
	QDomText text = doc.createTextNode(Isds::boolType2Str(val));
	elem.appendChild(text);
	parent.appendChild(elem);

	return true;
}

bool Isds::Xml::writeDocument(const Document &document, QDomDocument &doc,
    QDomElement &parent)
{
	static const QString fileTag("p:dmFile");
	static const QString mimeTypeTag("dmMimeType");
	static const QString metaTypeTag("dmFileMetaType");
	static const QString guidTag("dmFileGuid");
	static const QString upGuidTag("dmUpFileGuid");
	static const QString descrTag("dmFileDescr");
	static const QString formatTag("dmFormat");

	static const QString encContentTag("p:dmEncodedContent");
	static const QString xmlContentTag("p:dmXMLContent"); /* Not supported by this app. */

	Q_UNUSED(xmlContentTag)

	if (Q_UNLIKELY(document.isNull())) {
		Q_ASSERT(0);
		return false;
	}

	QDomElement elem = doc.createElement(fileTag);

	elem.setAttribute(mimeTypeTag, document.mimeType());
	elem.setAttribute(metaTypeTag, fileMetaType2Str(document.fileMetaType()));
	if (!document.fileGuid().isNull()) {
		elem.setAttribute(guidTag, document.fileGuid());
	}
	if (!document.upFileGuid().isNull()) {
		elem.setAttribute(upGuidTag, document.upFileGuid());
	}
	elem.setAttribute(descrTag, document.fileDescr());
	if (!document.format().isNull()) {
		elem.setAttribute(formatTag, document.format());
	}

	bool iOk = writeElement(encContentTag, document.base64Content(), doc, elem);
	if (Q_UNLIKELY(!iOk)) {
		goto fail;
	}

	parent.appendChild(elem);

	return true;

fail:
	return false;
}

bool Isds::Xml::writeEnvelope(const Envelope &env, QDomDocument &doc,
    QDomElement &parent)
{
	static const QString envTag("dmEnvelope");
	static const QString sndrOrgUnitTag("dmSenderOrgUnit");
	static const QString sndrOrgUnitNumTag("dmSenderOrgUnitNum");
	static const QString idRecipTag("dbIDRecipient");
	static const QString recipOrgUnitTag("dmRecipientOrgUnit");
	static const QString recipOrgUnitNumTag("dmRecipientOrgUnitNum");
	static const QString toHandsTag("dmToHands");
	static const QString annotationTag("dmAnnotation");
	static const QString recipRefNumberTag("dmRecipientRefNumber");
	static const QString sndrRefNumber("dmSenderRefNumber");
	static const QString recipIdentTag("dmRecipientIdent");
	static const QString sndrIdent("dmSenderIdent");
	static const QString legalTitleLawTag("dmLegalTitleLaw");
	static const QString legalTitleYearTag("dmLegalTitleYear");
	static const QString legalTitleSectTag("dmLegalTitleSect");
	static const QString legalTitleParTag("dmLegalTitlePar");
	static const QString legalTitlePointTag("dmLegalTitlePoint");
	static const QString personalDeliveryTag("dmPersonalDelivery");
	static const QString allowSubstDeliveryTag("dmAllowSubstDelivery");
	static const QString typeTag("dmType");
	/*
	 * According to section 2.1 of WS_manipulace_s_datovymi_zpravami.pdf
	 * the value of dmOVM is ignored on the server now.
	 * Nevertheless, we are going to treat it like it isn't ignored.
	 .*/
	static const QString ovmTag("dmOVM");
	static const QString publishOwnIdTag("dmPublishOwnID");
	static const QString idLevelTag("IdLevel");

	if (Q_UNLIKELY(env.isNull() || doc.isNull() || parent.isNull())) {
		Q_ASSERT(0);
		return false;
	}

	QDomElement elem = doc.createElement(envTag);
	if ((!env.dmType().isNull()) &&
	    (env.dmType() != Envelope::dmType2Char(Type::MT_V))) {
		elem.setAttribute(typeTag, env.dmType());
	}
	parent.appendChild(elem);

	bool iOk = writeElement(idRecipTag, env.dbIDRecipient(), doc, elem);
	if (Q_UNLIKELY(!iOk)) {
		goto fail;
	}
	iOk = writeElement(sndrOrgUnitTag, env.dmSenderOrgUnit(), doc, elem);
	if (Q_UNLIKELY(!iOk)) {
		goto fail;
	}
	iOk = writeElementNum(sndrOrgUnitNumTag, env.dmSenderOrgUnitNum(), doc, elem);
	if (Q_UNLIKELY(!iOk)) {
		goto fail;
	}
	iOk = writeElement(recipOrgUnitTag, env.dmRecipientOrgUnit(), doc, elem);
	if (Q_UNLIKELY(!iOk)) {
		goto fail;
	}
	iOk = writeElementNum(recipOrgUnitNumTag, env.dmRecipientOrgUnitNum(), doc, elem);
	if (Q_UNLIKELY(!iOk)) {
		goto fail;
	}
	iOk = writeElement(toHandsTag, env.dmToHands(), doc, elem);
	if (Q_UNLIKELY(!iOk)) {
		goto fail;
	}
	iOk = writeElement(annotationTag, env.dmAnnotation(), doc, elem);
	if (Q_UNLIKELY(!iOk)) {
		goto fail;
	}
	iOk = writeElement(recipRefNumberTag, env.dmRecipientRefNumber(), doc, elem);
	if (Q_UNLIKELY(!iOk)) {
		goto fail;
	}
	iOk = writeElement(sndrRefNumber, env.dmSenderRefNumber(), doc, elem);
	if (Q_UNLIKELY(!iOk)) {
		goto fail;
	}
	iOk = writeElement(recipIdentTag, env.dmRecipientIdent(), doc, elem);
	if (Q_UNLIKELY(!iOk)) {
		goto fail;
	}
	iOk = writeElement(sndrIdent, env.dmSenderIdent(), doc, elem);
	if (Q_UNLIKELY(!iOk)) {
		goto fail;
	}
	iOk = writeElementNum(legalTitleLawTag, env.dmLegalTitleLaw(), doc, elem);
	if (Q_UNLIKELY(!iOk)) {
		goto fail;
	}
	iOk = writeElementNum(legalTitleYearTag, env.dmLegalTitleYear(), doc, elem);
	if (Q_UNLIKELY(!iOk)) {
		goto fail;
	}
	iOk = writeElement(legalTitleSectTag, env.dmLegalTitleSect(), doc, elem);
	if (Q_UNLIKELY(!iOk)) {
		goto fail;
	}
	iOk = writeElement(legalTitleParTag, env.dmLegalTitlePar(), doc, elem);
	if (Q_UNLIKELY(!iOk)) {
		goto fail;
	}
	iOk = writeElement(legalTitlePointTag, env.dmLegalTitlePoint(), doc, elem);
	if (Q_UNLIKELY(!iOk)) {
		goto fail;
	}
	iOk = writeElementBool(personalDeliveryTag, env.dmPersonalDelivery(), doc, elem);
	if (Q_UNLIKELY(!iOk)) {
		goto fail;
	}
	iOk = writeElementBool(allowSubstDeliveryTag, env.dmAllowSubstDelivery(), doc, elem);
	if (Q_UNLIKELY(!iOk)) {
		goto fail;
	}
	iOk = writeElementBool(ovmTag, env.dmOVM(), doc, elem);
	if (Q_UNLIKELY(!iOk)) {
		goto fail;
	}
	if (env.dmPublishOwnID() == Type::BOOL_TRUE &&
	        env.idLevel() > Type::IDLEV_PUBLISH_USERTYPE) {
		QDomElement poidElem = doc.createElement(publishOwnIdTag);
		poidElem.setAttribute(idLevelTag, env.idLevel());
		QDomText text = doc.createTextNode(boolType2Str(
		    env.dmPublishOwnID()));
		poidElem.appendChild(text);
		elem.appendChild(poidElem);
	} else {
		iOk = writeElementBool(publishOwnIdTag, env.dmPublishOwnID(),
		    doc, elem);
		if (Q_UNLIKELY(!iOk)) {
			goto fail;
		}
	}

	/*
	 * Ignoring values used only when downloading messages or envelopes:
	 * dmID, dbIDSender, dmSender, dmSenderAddress, dmSenderType,
	 * dmRecipient, dmRecipientAddress, dmAmbiguousRecipient, dmOrdinal,
	 * dmMessageStatus, dmAttachmentSize, dmDeliveryTime, dmAcceptanceTime,
	 * dmHash, dmQTimestamp, dmEvents
	 */

	return true;

fail:
	return false;
}

QList<Isds::Envelope> Isds::Xml::readMessageList(const QByteArray &xmlData,
    bool *ok)
{
	if (Q_UNLIKELY(xmlData.isEmpty())) {
		if (ok != Q_NULLPTR) {
			*ok = false;
		}
		return QList<Envelope>();
	}

	bool iOk = false;
	QDomDocument doc = toDomDocument(xmlData, &iOk);
	QDomElement elem;
	QList<Envelope> envelopes;
	if (Q_UNLIKELY(!iOk)) {
		goto fail;
	}

	elem = findSingleDomElement(doc, QLatin1String("dmRecords"));
	if (Q_UNLIKELY(elem.isNull())) {
		/* No record list found, treat this as an error. */
		goto fail;
	}

	{
		const QDomNodeList childNodes = elem.childNodes();
		for (int i = 0; i < childNodes.size(); ++i) {
			const QDomNode &node = childNodes.at(i);
			if (Q_UNLIKELY(node.nodeType() != QDomNode::ElementNode)) {
				continue;
			}
			const QDomElement elem = node.toElement();

			if (elem.localName() == QLatin1String("dmRecord")) {
				Envelope envelope;
				iOk = readEnvelopeContent(envelope, elem, true);
				if (Q_UNLIKELY(!iOk)) {
					goto fail;
				}
				envelopes.append(envelope);
			} else {
				/* There shouldn't be other content. */
				Q_ASSERT(0);
				goto fail;
			}
		}
	}

	if (ok != Q_NULLPTR) {
		*ok = true;
	}
	return envelopes;

fail:
	if (ok != Q_NULLPTR) {
		*ok = false;
	}
	return QList<Envelope>();
}

/*!
 * @brief Writes files list into supplied element node.
 *
 * @param[in]     documents Document list.
 * @param[in,out] doc DOM document.
 * @param[in]     parent DOM element node that should hold the values.
 * @return True on success.
 */
static
bool writeDocuments(const QList<Isds::Document> &documents, QDomDocument &doc,
    QDomElement &parent)
{
	static const QString filesTag("p:dmFiles");

	QDomElement elem = doc.createElement(filesTag);

	bool iOk = false;
	foreach (const Isds::Document &document, documents) {
		iOk = Isds::Xml::writeDocument(document, doc, elem);
		if (Q_UNLIKELY(!iOk)) {
			goto fail;
		}
	}

	parent.appendChild(elem);

	return true;

fail:
	return false;
}

/*!
 * @brief Writes a CreateMessage request into a DOM document.
 *
 * @note Content of \a doc is undefined on failure.
 *
 * @param[in]     message Data message containing data to be sent.
 * @param[in,out] doc DOM document.
 * @param[in]     parent DOM element node which should hold
 *                       the created DOM tree.
 * @return True on success, false on any error.
 */
static
bool writeCreateMessage(const Isds::Message &message, QDomDocument &doc,
    QDomElement &parent)
{
	static const QString cmTag("p:CreateMessage");
	static const QString cmNs("xmlns:p");
	static const QString cmNsUri(ISDS_XML_NS);

	if (Q_UNLIKELY(message.isNull() || doc.isNull() || parent.isNull())) {
		Q_ASSERT(0);
		return false;
	}

	QDomElement elem = doc.createElement(cmTag);
	elem.setAttribute(cmNs, cmNsUri);
	parent.appendChild(elem);

	bool iOk = Isds::Xml::writeEnvelope(message.envelope(), doc, elem);
	if (Q_UNLIKELY(!iOk)) {
		return false;
	}
	iOk = writeDocuments(message.documents(), doc, elem);
	if (Q_UNLIKELY(!iOk)) {
		return false;
	}

	return true;
}

qint64 Isds::Xml::readCreateMessageResponse(const QByteArray &xmlData)
{
	QXmlStreamReader xml(xmlData);

	while (!xml.atEnd() && !xml.hasError()) {
		QXmlStreamReader::TokenType tokenType = xml.readNext();
		if (tokenType == QXmlStreamReader::StartDocument) {
			continue;
		}
		if (tokenType == QXmlStreamReader::StartElement) {
			if (xml.name() == QLatin1String("dmID")) {
				xml.readNext();
				bool iOk = false;
				qint64 val = xml.text().toLongLong(&iOk);
				if (iOk) {
					return val;
				}
			}
		}
	}

	return -1;
}

bool Isds::Xml::readAuthenticateMessageResponse(const QByteArray &xmlData,
    bool *ok)
{
	QXmlStreamReader xml(xmlData);

	enum Type::NilBool val = Type::BOOL_NULL;
	while (!xml.atEnd() && !xml.hasError()) {
		QXmlStreamReader::TokenType token = xml.readNext();
		if (token == QXmlStreamReader::StartDocument) {
			continue;
		}
		if (token == QXmlStreamReader::StartElement) {
			if (xml.name() == QLatin1String("dmAuthResult")) {
				xml.readNext();
				val = str2BoolType(xml.text().toString());
			}
		}
	}

	/* Documentation states that only true/false can be returned. */
	if (ok != Q_NULLPTR) {
		*ok = (val != Type::BOOL_NULL);
	}
	return (val == Type::BOOL_TRUE);
}

bool Isds::Xml::readMessageAuthor(const QByteArray &xmlData,
    enum Type::SenderType &userType, QString &authorName)
{
	QXmlStreamReader xml(xmlData);

	enum Type::SenderType type = Type::ST_NULL;
	QString name;
	while (!xml.atEnd() && !xml.hasError()) {
		QXmlStreamReader::TokenType token = xml.readNext();
		if (token == QXmlStreamReader::StartDocument) {
			continue;
		}
		if (token == QXmlStreamReader::StartElement) {
			if (xml.name() == QLatin1String("userType")) {
				xml.readNext();
				type = str2SenderType(xml.text().toString());
			} else if (xml.name() == QLatin1String("authorName")) {
				xml.readNext();
				name = xml.text().toString();
			}
		}
	}

	userType = type;
	authorName = macroStdMove(name);
	/* The entry userType must be present, authorName may be empty. */
	return (type != Type::ST_NULL);
}

Isds::DmMessageAuthor Isds::Xml::readMessageAuthor2(const QByteArray &xmlData,
    bool *ok)
{
	if (Q_UNLIKELY(xmlData.isEmpty())) {
		if (ok != Q_NULLPTR) {
			*ok = false;
		}
		return DmMessageAuthor();
	}

	bool iOk = false;
	QDomDocument doc = toDomDocument(xmlData, &iOk);
	QDomElement elem;
	DmMessageAuthor msgAuthor;
	if (Q_UNLIKELY(!iOk)) {
		goto fail;
	}

	elem = findSingleDomElement(doc, QLatin1String("dmMessageAuthor"));
	if (Q_UNLIKELY(elem.isNull())) {
		goto fail;
	}

	if (Q_UNLIKELY(!readMessageAuthorItems(elem, msgAuthor))) {
		goto fail;
	}

	if (ok != Q_NULLPTR) {
		*ok = true;
	}
	return msgAuthor;

fail:
	if (ok != Q_NULLPTR) {
		*ok = false;
	}
	return DmMessageAuthor();
}

QByteArray Isds::Xml::soapRequestDummyOperation(void)
{
	QByteArray xmlData("<DummyOperation xmlns=\"" ISDS_XML_NS "\"/>");
	return coatWithSoapEnvelope(xmlData);
}

QByteArray Isds::Xml::soapRequestMarkMessageAsDownloaded(qint64 dmId)
{
	QByteArray xmlData(
	    "<MarkMessageAsDownloaded xmlns=\"" ISDS_XML_NS "\"><dmID>");
	xmlData.append(QString::number(dmId).toUtf8());
	xmlData.append("</dmID></MarkMessageAsDownloaded>");
	return coatWithSoapEnvelope(xmlData);
}

QByteArray Isds::Xml::soapRequestGetMessageAuthor(qint64 dmId)
{
	QByteArray xmlData("<GetMessageAuthor xmlns=\"" ISDS_XML_NS "\"><dmID>");
	xmlData.append(QString::number(dmId).toUtf8());
	xmlData.append("</dmID></GetMessageAuthor>");
	return coatWithSoapEnvelope(xmlData);
}

QByteArray Isds::Xml::soapRequestGetMessageAuthor2(qint64 dmId)
{
	QByteArray xmlData("<GetMessageAuthor2 xmlns=\"" ISDS_XML_NS "\"><dmID>");
	xmlData.append(QString::number(dmId).toUtf8());
	xmlData.append("</dmID></GetMessageAuthor2>");
	return coatWithSoapEnvelope(xmlData);
}

QByteArray Isds::Xml::soapRequestAuthenticateMessage(const QByteArray &rawData)
{
	QByteArray xmlData(
	    "<AuthenticateMessage xmlns=\"" ISDS_XML_NS "\"><dmMessage>");
	xmlData.append(rawData.toBase64());
	xmlData.append("</dmMessage></AuthenticateMessage>");
	return coatWithSoapEnvelope(xmlData);
}

QByteArray Isds::Xml::soapRequestMessageList(enum RequestMessageList rml,
    const QDateTime &fromTime, const QDateTime &toTime, long int orgUnitNum,
    Type::DmFiltStates statusFilter, unsigned long int offset,
    unsigned long int limit)
{
	QByteArray xmlData;
	switch (rml) {
	case RML_RCVD:
		xmlData.append("<p:GetListOfReceivedMessages xmlns:p=\"" ISDS_XML_NS "\">");
		break;
	case RML_SNT:
		xmlData.append("<p:GetListOfSentMessages xmlns:p=\"" ISDS_XML_NS "\">");
		break;
	default:
		Q_ASSERT(0);
		return QByteArray();
		break;
	}

	if (!fromTime.isNull()) {
		xmlData.append("<p:dmFromTime>");
		xmlData.append(dateTimetoIsoDateTimeStr(fromTime).toUtf8());
		xmlData.append("</p:dmFromTime>");
	}
	if (!toTime.isNull()) {
		xmlData.append("<p:dmToTime>");
		xmlData.append(dateTimetoIsoDateTimeStr(toTime).toUtf8());
		xmlData.append("</p:dmToTime>");
	}
	switch (rml) {
	case RML_RCVD:
		if (orgUnitNum > 0) {
			xmlData.append(
			    QString("<p:dmRecipientOrgUnitNum>%1</p:dmRecipientOrgUnitNum>")
			        .arg(orgUnitNum).toUtf8());
		}
		break;
	case RML_SNT:
		if (orgUnitNum > 0) {
			xmlData.append(
			    QString("<p:dmSenderOrgUnitNum>%1</p:dmSenderOrgUnitNum>")
			        .arg(orgUnitNum).toUtf8());
		}
		break;
	default:
		Q_ASSERT(0);
		return QByteArray();
		break;
	}
	if (statusFilter > 0) {
		xmlData.append(
		     QString("<p:dmStatusFilter>%1</p:dmStatusFilter>")
		         .arg((unsigned int)statusFilter).toUtf8());
	}
	if (offset > 0) { /* Counting from 1. */
		xmlData.append(
		    QString("<p:dmOffset>%1</p:dmOffset>").arg(offset).toUtf8());
	}
	if (limit > 0) { /* Suppose zero makes no sense. */
		xmlData.append(
		    QString("<p:dmLimit>%1</p:dmLimit>").arg(limit).toUtf8());
	}

	switch (rml) {
	case RML_RCVD:
		xmlData.append("</p:GetListOfReceivedMessages>");
		break;
	case RML_SNT:
		xmlData.append("</p:GetListOfSentMessages>");
		break;
	default:
		Q_ASSERT(0);
		return QByteArray();
		break;
	}

	return coatWithSoapEnvelope(xmlData);
}

QByteArray Isds::Xml::soapRequestGetSignedMessage(
    enum RequestSignedMessageData rsmd, qint64 dmId)
{
	QByteArray xmlData;
	switch (rsmd) {
	case RSMD_DELINFO:
		xmlData.append(
		    "<GetSignedDeliveryInfo xmlns=\"" ISDS_XML_NS "\"><dmID>");
		break;
	case RSMD_RCVD_MSG:
		xmlData.append(
		    "<SignedMessageDownload xmlns=\"" ISDS_XML_NS "\"><dmID>");
		break;
	case RSMD_SNT_MSG:
		xmlData.append(
		    "<SignedSentMessageDownload xmlns=\"" ISDS_XML_NS "\"><dmID>");
		break;
	default:
		Q_ASSERT(0);
		return QByteArray();
		break;
	}
	xmlData.append(QString::number(dmId).toUtf8());

	switch (rsmd) {
	case RSMD_DELINFO:
		xmlData.append("</dmID></GetSignedDeliveryInfo>");
		break;
	case RSMD_RCVD_MSG:
		xmlData.append("</dmID></SignedMessageDownload>");
		break;
	case RSMD_SNT_MSG:
		xmlData.append("</dmID></SignedSentMessageDownload>");
		break;
	default:
		Q_ASSERT(0);
		return QByteArray();
		break;
	}

	return coatWithSoapEnvelope(xmlData);
}

QByteArray Isds::Xml::soapRequestCreateMessage(const Message &message, bool *ok)
{
	QDomDocument doc = buildEmptyDocument();
	QDomElement elem = buildSoap11Envelope(doc);
	bool iOk = writeCreateMessage(message, doc, elem);
	if (ok != Q_NULLPTR) {
		*ok = iOk;
	}
	return doc.toByteArray(-1);
}
