/*
 * Copyright (C) 2014-2025 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

#pragma once

#include <QByteArray>

#include "src/datovka_shared/isds/message_interface_vodz.h"
#include "src/isds/xml/message_interface.h"

namespace Isds {

	namespace Xml {

		/*!
		 * @brief Read upload attachment response from XML data.
		 *        <UploadAttachmentResponse>
		 *
		 * @param[in]  xmlData Response XML data.
		 * @param[out] ok Set to false on error.
		 * @return Attachment upload response data.
		 */
		DmAtt readUploadAttachmentResponse(const QByteArray &xmlData,
		    bool *ok = Q_NULLPTR);

		/*!
		 * @brief Read download attachment response from XML data.
		 *        <DownloadAttachmentResponse>
		 *
		 * @param[in]  xmlData Response XML data.
		 * @param[out] ok Set to false on error.
		 * @return Attachment file in the base64.
		 */
		DmFile readDownloadAttachmentBase64Response(
		    const QByteArray &xmlData, bool *ok = Q_NULLPTR);

		/*!
		 * @brief Read download attachment response from XML data.
		 *        <DownloadAttachmentResponse>
		 *
		 * @param[in] xmlData Response XML data.
		 * @return Attachment file name.
		 */
		QString readDownloadAttachmentFileName(
		    const QByteArray &xmlData);

		/*!
		 * @brief Constructs <AuthenticateBigMessage> request XML.
		 *
		 * @param[in] rawData Message or delivery info in raw binary format.
		 * @return Request XML data.
		 */
		QByteArray soapRequestAuthenticateBigMessage(
		    const QByteArray &rawData);

		/*!
		 * @brief Constructs <AuthenticateBigMessage> request XML.
		 *
		 * @return Request XML data.
		 */
		QByteArray soapRequestAuthenticateBigMessageMtomXop(void);

		/*!
		 * @brief Constructs <SignedBigMessageDownload>
		 *     or <SignedSentBigMessageDownload> request XML.
		 *
		 * @param[in] rsmd Determines request type.
		 * @param[in] dmId Message identifier.
		 * @return Request XML data.
		 */
		QByteArray soapRequestGetSignedBigMessageBase64(
		    enum RequestSignedMessageData rsmd, qint64 dmId);

		/*!
		 * @brief Constructs <SignedBigMessageDownload>
		 *     or <SignedSentBigMessageDownload> request XML.
		 *
		 * @param[in] rsmd Determines request type.
		 * @param[in] dmId Message identifier.
		 * @return Request XML data.
		 */
		QByteArray soapRequestGetSignedBigMessageMtomXop(
		    enum RequestSignedMessageData rsmd, qint64 dmId);

		/*!
		 * @brief Constructs <UploadAttachment> request XML.
		 *
		 * @param[in] dmFile Attachment data and file content in base64.
		 * @return Request XML data.
		 */
		QByteArray soapRequestUploadAttachmentBase64(
		    const DmFile &dmFile);

		/*!
		 * @brief Constructs <UploadAttachment> request XML.
		 *
		 * @param[in] dmMimeType File mime type.
		 * @param[in] dmFileDescr File name.
		 * @return Request XML data.
		 */
		QByteArray soapRequestUploadAttachmentMtomXop(
		    const QString &dmMimeType, const QString &dmFileDescr);

		/*!
		 * @brief Constructs <DownloadAttachment> request XML.
		 *
		 * @param[in] dmId Message identifier.
		 * @param[in] attId Attachment identifier.
		 * @return Request XML data.
		 */
		QByteArray soapRequestDownloadAttachmentBase64(qint64 dmId,
		    qint64 attId);

		/*!
		 * @brief Constructs <DownloadAttachment> request XML.
		 *
		 * @param[in] dmId Message identifier.
		 * @param[in] attId Attachment identifier.
		 * @return Request XML data.
		 */
		QByteArray soapRequestDownloadAttachmentMtomXop(qint64 dmId,
		    qint64 attId);

		/*!
		 * @brief Constructs <CreateBigMessage> request XML.
		 *
		 * @param[in]  message Message data.
		 * @param[out] ok Set to false on error.
		 * @return XML request data.
		 */
		QByteArray soapRequestCreateBigMessage(const Message &message,
		    bool *ok = Q_NULLPTR);
	}

}
