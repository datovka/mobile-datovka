/*
 * Copyright (C) 2014-2025 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

#include <QByteArray>
#include <QDir>
#include <QEventLoop>
#include <QFile>
#include <QFileInfo>
#include <QNetworkReply>
#include <QNetworkRequest>
#include <QString>

#include "src/datovka_shared/log/log.h"
#include "src/isds/io/connection.h"
#include "src/isds/io/downloader.h"
#include "src/isds/isds_const.h"
#include "src/isds/xml/message_interface_vodz.h"
#include "src/isds/xml/response_status.h"

Isds::Downloader::Downloader(QObject *parent)
    : QObject(parent),
    m_currentReply(Q_NULLPTR),
    m_file(Q_NULLPTR),
    m_nam(),
    m_dmId(0),
    m_boundary(),
    m_xml(),
    m_targetFolder(),
    m_sectionCnt(0),
    m_isXmlPart(false),
    m_isFilePart(false)
{
	connect(&m_nam, &QNetworkAccessManager::finished,
	    this, &Downloader::onReply);
}

bool Isds::Downloader::downloadAttachment(Session &session, qint64 dmId,
    qint64 attId, const QString &targetFileName)
{
	if (Q_UNLIKELY(targetFileName.isEmpty())) {
		return false;
	}

	m_targetFolder = QFileInfo(targetFileName).absolutePath();
	m_dmId = dmId;

	return post(session,
	    Xml::soapRequestDownloadAttachmentMtomXop(dmId, attId),
	    targetFileName);
}

bool Isds::Downloader::getSignedReceivedBigMessage(Session &session,
    qint64 dmId, const QString &targetFileName)
{
	if (Q_UNLIKELY(targetFileName.isEmpty())) {
		return false;
	}

	m_dmId = dmId;

	return post(session,
	    Xml::soapRequestGetSignedBigMessageMtomXop(
	        Xml::RSMD_RCVD_MSG, dmId),
	    targetFileName);
}

bool Isds::Downloader::getSignedSentBigMessage(Session &session, qint64 dmId,
    const QString &targetFileName)
{
	if (Q_UNLIKELY(targetFileName.isEmpty())) {
		return false;
	}

	m_dmId = dmId;

	return post(session,
	    Xml::soapRequestGetSignedBigMessageMtomXop(
	        Xml::RSMD_SNT_MSG, dmId),
	    targetFileName);
}

bool Isds::Downloader::post(Session &session, const QByteArray &rqData,
    const QString &targetFileName)
{
	m_sectionCnt = 0;
	m_isXmlPart = false;
	m_isFilePart = false;
	m_boundary.clear();
	m_currentReply = Q_NULLPTR;
	m_file = new (::std::nothrow) QFile(targetFileName);
	if (Q_UNLIKELY(Q_NULLPTR == m_file)) {
		return false;
	}
	if (Q_UNLIKELY(!m_file->open(QIODevice::WriteOnly))) {
		delete m_file; m_file = Q_NULLPTR;
		return false;
	}

	QNetworkRequest request(Connection::createRequest(session,
	    MESSAGE_SERVICE_VODZ, QByteArray(), rqData.length(), true));

#if (QT_VERSION >= QT_VERSION_CHECK(6, 0, 0))
	request.setAttribute(QNetworkRequest::RedirectPolicyAttribute,
	    QNetworkRequest::ManualRedirectPolicy);
#else /* < Qt-6.0 */
	request.setAttribute(QNetworkRequest::FollowRedirectsAttribute, true);
#endif /* >= Qt-6.0 */

	QEventLoop loop;

	connect(this, SIGNAL(quitLoop()), &loop, SLOT(quit()),
	    Qt::QueuedConnection);

	m_currentReply = m_nam.post(request, rqData);
	if (Q_UNLIKELY(Q_NULLPTR == m_currentReply)) {
		logErrorNL("%s", "No reply.");
		return false;
	}

	connect(m_currentReply, &QNetworkReply::readyRead,
	    this, &Downloader::onReadyRead);
	connect(m_currentReply, &QNetworkReply::downloadProgress,
	    this, &Downloader::updateDownloadProgress);

	loop.exec();

	return true;
}

void Isds::Downloader::onReadyRead(void)
{
	/*
	 * m_file and m_currentReply are not NULL because this method is run
	 * from a slot after Q_NULLPTR check.
	 */

	/* Read boundary and XML part. */
	while ((m_sectionCnt < 2) && m_currentReply->canReadLine()) {

		QByteArray data = m_currentReply->readLine();

		if (m_isXmlPart) {
			m_xml = data.simplified();
			m_isXmlPart = false;
			continue;
		}
		if (m_boundary.isEmpty()) {
			m_boundary = data.simplified();
			continue;
		}
		if (data == "\r\n") {
			++m_sectionCnt;
			m_isXmlPart = (m_sectionCnt == 1);
			m_isFilePart = (m_sectionCnt == 2);
		}
	}

	/* Read binary file. */
	if (m_isFilePart && (Q_NULLPTR != m_file)) {
		m_file->write(m_currentReply->readAll());
	}
}

/*!
 * @brief Delete file path.
 *
 * @param[in] filePath File path.
 */
static
void deleteFilePath(const QString &filePath)
{
	QDir dir(QFileInfo(filePath).absoluteDir());
	dir.removeRecursively();
}

void Isds::Downloader::cancelDownload(void)
{
	if (Q_NULLPTR != m_currentReply) {
		m_currentReply->abort();
		if (m_targetFolder.isEmpty()) {
			emit downloadBigMessageFinished(m_dmId, false, QString());
		} else {
			emit downloadAttachmentFinished(m_dmId, false,
			    QString(), QString());
		}

		m_currentReply->deleteLater(); m_currentReply = Q_NULLPTR;
	}

	if (Q_NULLPTR != m_file) {
		m_file->remove();
		deleteFilePath(m_file->fileName());

		delete m_file; m_file = Q_NULLPTR;
	}

	emit quitLoop();
}

void Isds::Downloader::onReply(QNetworkReply *reply)
{
	bool success = false;
	QString fileName;
	QString errTxt;
	DmStatus dmStatus = Xml::toDmStatus(m_xml, &success);
	if (Q_UNLIKELY(!success)) {
		goto finish;
	}
	if (!dmStatus.ok()) {
		success = false;
		errTxt = dmStatus.message();
	}

	if ((reply->error() == QNetworkReply::NoError) && (m_file->size() > 0)) {
		/* Remove last boundary from file content. */
		m_file->resize(m_file->size() - m_boundary.size() - 4);
		success = m_file->flush();
		m_file->close();
		if (!m_targetFolder.isEmpty()) {
			fileName = Xml::readDownloadAttachmentFileName(m_xml);
		}
		if (!fileName.isEmpty()) {
			success = m_file->rename(m_targetFolder
			    + QDir::separator() + fileName);
		}
	}

finish:
	if (!success) {
		m_file->remove();
		deleteFilePath(m_file->fileName());
	}

	delete m_file; m_file = Q_NULLPTR;
	reply->deleteLater();
	if (m_currentReply == reply) {
		m_currentReply = Q_NULLPTR;
	}

	if (m_targetFolder.isEmpty()) {
		emit downloadBigMessageFinished(m_dmId, success, errTxt);
	} else {
		emit downloadAttachmentFinished(m_dmId, success, errTxt, fileName);
	}

	emit quitLoop();
}
