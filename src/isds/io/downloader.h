/*
 * Copyright (C) 2014-2025 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

#pragma once

#include <QNetworkAccessManager>

namespace Isds {
	class Session; /* Forward declaration. */
}

class QByteArray; /* Forward declaration. */
class QFile; /* Forward declaration. */
class QNetworkReply; /* Forward declaration. */
class QString; /* Forward declaration. */

namespace Isds {

	class Downloader : public QObject {
		Q_OBJECT

	public:
		/*!
		 * @brief Constructor.
		 */
		explicit Downloader(QObject *parent = Q_NULLPTR);

		/*!
		 * @brief Download attachment.
		 *
		 * @param[in] session ISDS account session.
		 * @param[in] dmId Message ID.
		 * @param[in] attId Attachment ID.
		 * @param[in] targetFileName Target file name.
		 * @return True on success.
		 */
		bool downloadAttachment(Session &session, qint64 dmId,
		    qint64 attId, const QString &targetFileName);

		/*!
		 * @brief Download signed received big message.
		 *
		 * @param[in] session ISDS account session.
		 * @param[in] dmId Message ID.
		 * @param[in] targetFileName Target file name.
		 * @return True on success.
		 */
		bool getSignedReceivedBigMessage(Session &session, qint64 dmId,
		    const QString &targetFileName);

		/*!
		 * @brief Download signed sent big message.
		 *
		 * @param[in] session ISDS account session.
		 * @param[in] dmId Message ID.
		 * @param[in] targetFileName Target file name.
		 * @return True on success.
		 */
		bool getSignedSentBigMessage(Session &session, qint64 dmId,
		    const QString &targetFileName);

	public slots:
		/*!
		 * @brief Cancel downloading.
		 */
		void cancelDownload(void);

	signals:
		/*!
		 * @brief This signal is emitted to signalise
		 *        the event loop to quit.
		 */
		void quitLoop(void);

		/*!
		 * @brief Fires if download progress has been changed.
		 *
		 * @param[in] bytesReceived Received bytes.
		 * @param[in] bytesTotal Total bytes.
		 */
		void updateDownloadProgress(qint64 bytesReceived,
		    qint64 bytesTotal);

		/*!
		 * @brief Fires if download finished.
		 *
		 * @param[in] dmId Message Id.
		 * @param[in] success True if success.
		 * @param[in] errTxt Error description.
		 * @param[in] fileName File name.
		 */
		 void downloadAttachmentFinished(qint64 dmId, bool success,
		    QString errTxt, QString fileName);

		/*!
		 * @brief Fires if download finished.
		 *
		 * @param[in] dmId Message Id.
		 * @param[in] success True if success.
		 * @param[in] errTxt Error description.
		 */
		 void downloadBigMessageFinished(qint64 dmId, bool success,
		    QString errTxt);

	private slots:
		/*!
		 * @brief Fires if some data have been received.
		 */
		void onReadyRead(void);

		/*!
		 * @brief Fires if complete data have been received.
		 *
		 * @param[in] reply Network reply.
		 */
		void onReply(QNetworkReply *reply);

	private:
		/*!
		 * @brief Send post request.
		 *
		 * @param[in] session ISDS account session.
		 * @param[in] rqData Request data.
		 * @param[in] targetFileName Target file name.
		 * @return True if success.
		 */
		bool post(Session &session, const QByteArray &rqData,
		    const QString &targetFileName);

		QNetworkReply *m_currentReply; /*!< Network reply. */
		QFile *m_file; /*!< File to be written to the storage. */
		QNetworkAccessManager m_nam; /*!< Loaded backup content. */
		qint64 m_dmId; /*!< Message ID. */
		QByteArray m_boundary; /*!< Boundary identifier. */
		QByteArray m_xml; /*!< XML of messages. */
		QString m_targetFolder; /*!< Target path where file will store. */
		qint64 m_sectionCnt; /*!< Multipart section counter. */
		bool m_isXmlPart; /*!< True if read XML section from multipart. */
		bool m_isFilePart; /*!< True if read file section from multipart. */
	};
}
