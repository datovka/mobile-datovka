/*
 * Copyright (C) 2014-2025 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

#include <QByteArray>
#include <QEventLoop>
#include <QFile>
#include <QHttpMultiPart>
#include <QHttpPart>
#include <QNetworkAccessManager>
#include <QNetworkCookie>
#include <QNetworkReply>
#include <QNetworkRequest>
#include <QObject>
#include <QSslCertificate>
#include <QSslKey>
#include <QStringBuilder>
#include <QTimer>
#include <QUrl>

#include "src/datovka_shared/identifiers/account_id.h"
#include "src/datovka_shared/log/log.h"
#include "src/isds/io/connection.h"
#include "src/isds/isds_const.h"
#include "src/isds/session/isds_context.h"
#include "src/isds/session/isds_session.h"

#ifdef Q_COMPILER_RVALUE_REFS
#  define macroStdMove(x) ::std::move(x)
#else /* Q_COMPILER_RVALUE_REFS */
#  define macroStdMove(x) (x)
#endif /* Q_COMPILER_RVALUE_REFS */

/* 1 = request data will be printed to console or log. 0 = Disable
 * Enable only for debugging.
 */
#define PRINT_REQUEST_DATA 0

/* 1 = reply data will be printed to console or log. 0 = Disable
 * Enable only for debugging.
*/
#define PRINT_RESPONSE_DATA 0

/*!
 * @brief Constructs a header listing.
 *
 * @param[in] request Network request.
 * @return Raw header listing.
 */
static
QByteArray requestHeaderListing(const QNetworkRequest &request)
{
	QByteArray listing;

	for (const QByteArray &reqName : request.rawHeaderList()) {
		if (reqName == "Authorization" || reqName == "Cookie") {
			listing += reqName + ": "
			    + QString::number(request.rawHeader(reqName).length()).toUtf8() + "\n";
		} else {
			listing += reqName + ": " + request.rawHeader(reqName) + "\n";
		}
	}

	return listing;
}

/*!
 * @brief Logs request header and its data.
 *
 * @param[in] request Request object.
 * @param[in] data Request data.
 */
static
void logRequest(const QNetworkRequest &request, const QByteArray &data)
{
	logDebugLv3NL("\n"
	    "====================REQUEST=========================\n"
	    "URL: %s\n"
	    "%s"
	    "--------------------Content-------------------------\n"
	    "%s\n"
	    "====================================================\n"
	    , request.url().toString().toUtf8().constData(),
	    requestHeaderListing(request).constData(),
	    (PRINT_REQUEST_DATA) ? data.constData() : "hidden"
	);
}

/*!
 * @brief Constructs a header listing.
 *
 * @param[in] reply Network reply.
 * @return Raw header listing.
 */
static
QByteArray replyHeaderListing(const QNetworkReply *reply)
{
	if (Q_UNLIKELY(reply == Q_NULLPTR)) {
		Q_ASSERT(0);
		return "\n";
	}

	QByteArray listing;

	for (const QNetworkReply::RawHeaderPair &pair : reply->rawHeaderPairs()) {
		listing += pair.first + ": " + pair.second + "\n";
	}

	return listing;
}

/*!
 * @brief Logs reply header and its data.
 *
 * @param[in] request Reply object.
 * @param[in] data Reply data.
 */
static
void logReply(const QNetworkReply *reply, const QByteArray &data)
{
	int rCode =
	    reply->attribute(QNetworkRequest::HttpStatusCodeAttribute).toInt();

	logDebugLv3NL("\n"
	    "====================REPLY===========================\n"
	    "URL: %s\n"
	    "CODE: %d\n"
	    "ERROR: %s\n"
	    "REASON: %s\n"
	    "--------------------Headers-------------------------\n"
	    "%s"
	    "--------------------Content-------------------------\n"
	    "%s\n"
	    "====================================================\n"
	    , reply->url().toString().toUtf8().constData(), rCode,
	    (reply->error() == QNetworkReply::NoError) ? "" : reply->errorString().toUtf8().constData(),
	    reply->attribute(QNetworkRequest::HttpReasonPhraseAttribute).toString().toUtf8().constData(),
	    replyHeaderListing(reply).constData(),
	    (PRINT_RESPONSE_DATA || 401 == rCode) ? data.constData() : "hidden");
}

/*!
 * @brief Create authorisation Base64 token.
 *
 * @param[in] userName User name string.
 * @param[in] password Password string.
 * @param[in] otp Otp string (may be NULL).
 * @return Authorisation string in Base64.
 */
static
QByteArray authorisationBase64(const QString &userName, const QString &password)
{
	return (userName % ":" % password).toUtf8().toBase64();
}

/*!
 * @brief Set SOAP XML headers.
 *
 * @param[in,out] request Network request.
 */
static
void setSoapXmlHeaders(QNetworkRequest &request)
{
	request.setHeader(QNetworkRequest::ContentTypeHeader,
	    "text/xml; charset=utf-8");
	request.setRawHeader("Accept",
	    "application/soap+xml,application/xml,text/xml");
}

/*!
 * @brief Set SOAP MTOM/XOP headers.
 *
 * @param[in,out] request Network request.
 * @param[in] boundary MTOM/XOP boundary.
 */
static
void setMtomXopHeaders(QNetworkRequest &request, const QByteArray &boundary,
    bool returnMtomXop)
{
	if (!boundary.isEmpty()) {
		request.setRawHeader("MIME-Version", "1.0");
		request.setHeader(QNetworkRequest::ContentTypeHeader,
		    "multipart/related;type=\"application/xop+xml\";"
		    "start=\""+QString(ISDS_MTOM_XOP_START_ID)+"\";"
		    "start-info=\"application/soap+xml\";"
		    "action=\"\";boundary=\""+boundary+"\"");
		request.setRawHeader("Accept-Encoding", "gzip,deflate");
	} else {
		if (returnMtomXop) {
			request.setHeader(QNetworkRequest::ContentTypeHeader,
			    "application/soap+xml; charset=utf-8");
			request.setRawHeader("Accept", "multipart/related");
		} else {
			request.setHeader(QNetworkRequest::ContentTypeHeader,
			    "text/xml; charset=utf-8");
			request.setRawHeader("Accept",
			    "application/soap+xml,application/xml,text/xml");
		}
	}
}

/*!
 * @brief Send request.
 *
 * @param[in,out] nam Network access manager.
 * @param[in]     request Network request.
 * @param[in]     data Data to be sent along with the request.
 * @return Null pointer on failure.
 */
static
QNetworkReply *sendRequest(QNetworkAccessManager &nam,
    const QNetworkRequest &request, const QByteArray &data)
{
	logRequest(request, data);

	QNetworkReply *reply = Q_NULLPTR;
	if (data.isEmpty()) {
		reply = nam.get(request);
	} else {
		reply = nam.post(request, data);
	}
	return reply;
}

/*!
 * @brief Blocks until all data are sent and received or until timed out.
 *
 * @param[in,out] reply Communication context.
 * @return Error code.
 */
static
enum Isds::Connection::ErrCode waitReplyFinished(QNetworkReply *reply)
{
	if (Q_UNLIKELY(reply == Q_NULLPTR)) {
		Q_ASSERT(0);
		return Isds::Connection::ERR_REPLY;
	}

	/* Set timeout timer */
	QTimer timer;
	timer.setSingleShot(true);
	QEventLoop eventLoop;
	QObject::connect(&timer, SIGNAL(timeout()), &eventLoop, SLOT(quit()));
	QObject::connect(reply, SIGNAL(finished()), &eventLoop, SLOT(quit()));

	do {
		timer.start(ISDS_TIMEOUT);
		eventLoop.exec();
	} while (reply->isRunning());

	timer.disconnect(SIGNAL(timeout()), &eventLoop, SLOT(quit()));
	reply->disconnect(SIGNAL(finished()), &eventLoop, SLOT(quit()));

	if (reply->isFinished()) {
		timer.stop();
	} else {
		logErrorNL("Connection timed out. Check your internet connection. %s",
		    reply->errorString().toUtf8().constData());
		reply->abort();
		return Isds::Connection::ERR_TIMEOUT;
	}

	return Isds::Connection::ERR_NO_ERROR;
}

/*!
 * @brief Translates MEP response message code into understandable message.
 *
 * @param[in] resCode Response code.
 * @return Text message.
 */
static
QString mepErrorDescription(const QByteArray &resCode) {

	QString txt = "Unspecified MEP error.";
	if (resCode == MEP_INFO_MEP_REQUESTED) {
		txt = "MEP login requested. Waiting for user response.";
	} else if (resCode == MEP_ERROR_USER_NOT_EXISTS) {
		txt = "Wrong login credentials. Username doesn't exist.";
	} else if (resCode == MEP_ERROR_USER_NOT_SET) {
		txt = "Wrong login credentials. Username or valid communication code hasn't been set.";
	}
	return txt;
}

/*!
 * @brief Translates OTP response message code into understandable message.
 *
 * @param[in] resCode Response code.
 * @return Text message.
 */
static
QString otpErrorDescription(const QByteArray &resCode) {

	QString txt = "Unspecified OTP error";
	if (resCode == OTP_ERROR_NOT_LOGGED) {
		txt = "Wrong login credentials.";
	} else if (resCode == OTP_ERROR_USER_BLOCKED) {
		txt = "Access has been blocked for 60 minutes.";
	} else if (resCode == OTP_PWD_EXPIR) {
		txt = "Your password has expired.";
	} else if (resCode == OTP_BAD_ROLE) {
		txt = "Permissions denied.";
	} else if (resCode == OTP_ERROR_SEND_QUICKLY) {
		txt = "SMS code can be sent once in 30 seconds.";
	} else if (resCode == OTP_ERROR_NOT_SEND) {
		txt = "SMS code couldn't be sent.";
	} else if (resCode == OTP_ERROR_NOT_ORDERED) {
		txt = "Your order on premium SMS has been exhausted or cancelled. SMS code cannot be sent.";
	}
	return txt;
}

/*!
 * @brief Process reply data.
 *
 * @param[in,out] reply Obtained reply.
 * @param[out]    replyData Obtained reply data (may be empty).
 * @param[out]    redirUrl Redirection URL (may be empty).
 * @param[out]    newCookies Cookie list (may be empty).
 * @return Error code.
 */
static
enum Isds::Connection::ErrCode processReply(QNetworkReply *reply,
    QByteArray &replyData, QUrl &redirUrl, QList<QNetworkCookie> &newCookies)
{
	enum Isds::Connection::ErrCode errCode = Isds::Connection::ERR_NO_ERROR;

	if (Q_UNLIKELY(reply == Q_NULLPTR)) {
		Q_ASSERT(0);
		return Isds::Connection::ERR_REPLY;
	}

	/* Response status code */
	int statusCode = reply->attribute(
	    QNetworkRequest::HttpStatusCodeAttribute).toInt();

	switch (statusCode) {
	case 200: /* HTTP 200 OK */
		errCode = Isds::Connection::ERR_NO_ERROR;
		break;
	case 301: /* HTTP 301 Moved Permanently - redirect */
	case 302: /* HTTP 302 Found - redirect */
	case 303: /* HTTP 303 See Other - redirect */
	case 307: /* HTTP 307 Temporary Redirect - redirect */
		{
			if (!reply->rawHeader("X-Response-message-code").isEmpty()) {
				QString errTxt(mepErrorDescription(
				    reply->rawHeader("X-Response-message-code")));
				logDebugLv3NL("MEP authorization info: %s",
				    errTxt.toUtf8().constData());
			}

			/* Store location URL for redirection. */
			QVariant possibleRedirectUrl(reply->attribute(
			    QNetworkRequest::RedirectionTargetAttribute));
			redirUrl = possibleRedirectUrl.toUrl();

			/* Store cookies */
			QVariant variantCookies =
			    reply->header(QNetworkRequest::SetCookieHeader);
			newCookies =
			    qvariant_cast<QList<QNetworkCookie>>(variantCookies);
			errCode = Isds::Connection::ERR_NO_ERROR;
		}
		break;
	case 400: /* HTTP 400 Bad Request */
		{
			logErrorNL("Reply error: %s",
			    reply->errorString().toUtf8().constData());
			errCode = Isds::Connection::ERR_BAD_REQUEST;
		}
		break;
	case 401: /* HTTP 401 Unauthorized */
		{
			/* Test if some OTP error */
			QString otpType(reply->rawHeader("WWW-Authenticate"));
			QString errTxt(otpErrorDescription(reply->rawHeader("X-Response-message-code")));
			if (otpType == "hotp") {
				logErrorNL("HOTP authorization error: %s", errTxt.toUtf8().constData());
				errCode = Isds::Connection::ERR_UNAUTHORIZED_HOTP;
			} else if (otpType == "totp") {
				logErrorNL("TOTP authorization error: %s", errTxt.toUtf8().constData());
				errCode = Isds::Connection::ERR_UNAUTHORIZED_TOTP;
			} else if (otpType == "totpsendsms") {
				logErrorNL("SMS send error: %s", errTxt.toUtf8().constData());
				errCode = Isds::Connection::ERR_UNAUTHORIZED_TOTP_SMS;
			} else {
				logErrorNL("%s", "Unauthorized: Wrong login credentials.");
				errCode = Isds::Connection::ERR_UNAUTHORIZED;
			}
		}
		break;
	case 503: /* HTTP 503 Service Unavailable */
		logErrorNL("%s", "Service Unavailable: ISDS server is out of service. Scheduled maintenance in progress.");
		errCode = Isds::Connection::ERR_SERVER_UNAVAILABLE;
		break;
	case 599: /* HTTP 599 Bad MTOM/XOP request */
		logErrorNL("Reply error: %s",
		    reply->errorString().toUtf8().constData());
		errCode = Isds::Connection::ERR_BAD_REQUEST;
		break;
	default: /* Any other error. */
		logErrorNL("Reply error: %s", reply->errorString().toUtf8().constData());
		errCode = Isds::Connection::ERR_UNSPECIFIED;
		break;
	}

	/* Read reply data */
	replyData.clear();
	replyData = reply->readAll();

	logReply(reply, replyData);

	reply->deleteLater(); reply = Q_NULLPTR;

	return errCode;
}

QNetworkRequest Isds::Connection::createRequest(Session &session,
    const QString &soapEndPoint, const QByteArray &boundary, int contentLength,
    bool returnMtomXop)
{
	QUrl url(session.ctx()->url());
	if (!soapEndPoint.isNull()) {
		url.setUrl(session.ctx()->url().toString().append(soapEndPoint));
	}

	QNetworkRequest request(url);

	/* Set general request headers */
	request.setHeader(QNetworkRequest::UserAgentHeader, QString(APP_NAME));
	if (contentLength > 0) {
		request.setHeader(QNetworkRequest::ContentLengthHeader,
		    contentLength);
	}
	request.setRawHeader("Cache-Control", "no-store, no-cache, max-age=0");
	request.setAttribute(QNetworkRequest::RedirectPolicyAttribute,
	    QNetworkRequest::ManualRedirectPolicy);

	/* Set specific request headers */
	if (soapEndPoint == MESSAGE_SERVICE_VODZ) {
		request.setUrl(session.ctx()->urlVodz().toString().append(soapEndPoint));
		request.setRawHeader("Host", session.ctx()->urlVodz().host().toUtf8());
		setMtomXopHeaders(request, boundary, returnMtomXop);
	} else {
		request.setRawHeader("Host", session.ctx()->url().host().toUtf8());
		setSoapXmlHeaders(request);
	}

	/* Set cookies */
	if (!session.ctx()->cookies().isEmpty()) {
		QVariant cks;
		cks.setValue(session.ctx()->cookies());
		request.setHeader(QNetworkRequest::CookieHeader, cks);
	}

	/*  Set user credentials to request */
	if (!session.ctx()->password().isEmpty()) {
		QByteArray authorization("Basic ");
		authorization.append(
		    authorisationBase64(session.ctx()->acntId().username(),
		    session.ctx()->password()));
		request.setRawHeader("Authorization", authorization);
	}

	/* Set user login certificate */
	if (!session.ctx()->sslCertificate().isNull()) {
		QSslConfiguration sslConf(QSslConfiguration::defaultConfiguration());
		sslConf.setPeerVerifyMode(QSslSocket::VerifyNone);
		sslConf.setProtocol(QSsl::AnyProtocol);
		sslConf.setLocalCertificate(session.ctx()->sslCertificate());
		sslConf.setPrivateKey(session.ctx()->sslKey());
		request.setSslConfiguration(sslConf);
	}

	return request;
}

enum Isds::Connection::ErrCode Isds::Connection::communicate(
    Session &session, const QString &soapEndPoint,
    const QByteArray &requestData, QByteArray &replyData)
{
	return sendRequestToIsds(session, soapEndPoint, requestData, false,
	    replyData);
}

enum Isds::Connection::ErrCode Isds::Connection::communicateMtomXop(
    Session &session, const QString &soapEndPoint,
    const QByteArray &requestData, QByteArray &replyData)
{
	return sendRequestToIsds(session, soapEndPoint, requestData, true,
	    replyData);
}

/*!
 * @brief Create headers and body for XML multipart.
 *
 * @param[in] xmlData XML envelope data.
 * @param[in] soapAction SOAP action.
 * @return XML part for multipart.
 */
static
QHttpPart xmlPart(const QByteArray &xmlData, const QString &soapAction)
{
	QHttpPart xmlPart;
	xmlPart.setHeader(QNetworkRequest::ContentTypeHeader,
	    QVariant("application/xop+xml;charset=UTF-8;type=\"application/soap+xml\";action=\""+soapAction+"\""));
	xmlPart.setRawHeader("Content-Transfer-Encoding", "8bit");
	xmlPart.setRawHeader("Content-ID", ISDS_MTOM_XOP_START_ID);
	xmlPart.setBody(xmlData);
	return xmlPart;
}

enum Isds::Connection::ErrCode Isds::Connection::uploadMtomXop(
    Session &session, const QString &soapEndPoint,
    const QString &soapAction, const QByteArray &xmlData,
    const QString &fileName, const QString &fileMimeType,
    const QString &filePath, QByteArray &replyData)
{
	QNetworkAccessManager nam;

#if (QT_VERSION < QT_VERSION_CHECK(6, 0, 0))
	/*
	 * TODO
	 * QNetworkAccessManager::networkAccessible() is obsolete in Qt-5.15.0
	 * and was removed from Qt-6 without replacement.
	 * https://doc.qt.io/qt-5/qnetworkaccessmanager-obsolete.html
	 * Have to look for a replacement or workaround.
	 */
	switch (nam.networkAccessible()) {
	case QNetworkAccessManager::UnknownAccessibility:
	case QNetworkAccessManager::NotAccessible:
		logErrorNL("%s", "Internet connection is probably not available.");
		session.ctx()->setLastIsdsMsg(errorDescription(ERR_NO_CONNECTION));
		return ERR_NO_CONNECTION;
		break;
	default:
		break;
	}
#endif /* < Qt-6.0 */

	/* Create file part. */
	QHttpPart filePart;
	filePart.setHeader(QNetworkRequest::ContentTypeHeader,
	    QVariant(fileMimeType+"; name="+fileName));
	filePart.setRawHeader("Content-Transfer-Encoding", "binary");
	filePart.setRawHeader("Content-ID", "<att_1>");
	filePart.setHeader(QNetworkRequest::ContentDispositionHeader,
	    QVariant("attachment; name=\""+fileName+"\"; filename=\""+fileName+"\""));
	QFile *file = new (::std::nothrow) QFile(filePath);
	if (Q_UNLIKELY(Q_NULLPTR == file)) {
		logErrorNL("Cannot open file. %s", filePath.toUtf8().constData());
		return ERR_UNSPECIFIED;
	}
	if (Q_UNLIKELY(!file->open(QIODevice::ReadOnly))) {
		logErrorNL("Cannot open file. %s", filePath.toUtf8().constData());
		delete file; file = Q_NULLPTR;
		return ERR_BAD_REQUEST;
	}
	filePart.setBodyDevice(file);

	/* Create multipart request. */
	QHttpMultiPart *multiPart =
	    new (::std::nothrow) QHttpMultiPart(QHttpMultiPart::RelatedType);
	if (Q_NULLPTR == multiPart) {
		logErrorNL("%s", "Cannot build multipart request.");
		delete file; file = Q_NULLPTR;
		return ERR_UNSPECIFIED;
	}
	/* Cannot delete the file now, so delete it with multiPart. */
	file->setParent(multiPart);

	/* Append XML part. */
	multiPart->append(xmlPart(xmlData, soapAction));
	/* Append file part. */
	multiPart->append(filePart);

	/* Create request and fill headers. */
	QNetworkRequest request(createRequest(session, soapEndPoint,
	    multiPart->boundary(), 0, false));

	logRequest(request, xmlData);

	/* Send request. */
	QNetworkReply *reply = nam.post(request, multiPart);
	if (Q_UNLIKELY(reply == Q_NULLPTR)) {
		logErrorNL("%s", "No reply.");
		delete multiPart; multiPart = Q_NULLPTR;
		return ERR_REPLY;
	}
	/* Delete the multiPart with reply. */
	multiPart->setParent(reply);

	/* Wait for reply. */
	enum ErrCode errCode = waitReplyFinished(reply);
	if (Q_UNLIKELY(ERR_NO_ERROR != errCode)) {
		session.ctx()->setLastIsdsMsg(errorDescription(errCode));
		delete multiPart; multiPart = Q_NULLPTR;
		return errCode;
	}

	/* Process reply. */
	QUrl redirUrl;
	QList<QNetworkCookie> newCookies;
	errCode = processReply(reply, replyData, redirUrl, newCookies);

	session.ctx()->setLastIsdsMsg(errorDescription(errCode));

	return errCode;
}

enum Isds::Connection::ErrCode Isds::Connection::sendRequestToIsds(
    Session &session, const QString &soapEndPoint,
    const QByteArray &requestData, bool returnMtomXop, QByteArray &replyData)
{
	QNetworkAccessManager nam;

#if (QT_VERSION < QT_VERSION_CHECK(6, 0, 0))
	/*
	 * TODO
	 * QNetworkAccessManager::networkAccessible() is obsolete in Qt-5.15.0
	 * and was removed from Qt-6 without replacement.
	 * https://doc.qt.io/qt-5/qnetworkaccessmanager-obsolete.html
	 * Have to look for a replacement or workaround.
	 */
	switch (nam.networkAccessible()) {
	case QNetworkAccessManager::UnknownAccessibility:
	case QNetworkAccessManager::NotAccessible:
		logErrorNL("%s", "Internet connection is probably not available.");
		session.ctx()->setLastIsdsMsg(errorDescription(ERR_NO_CONNECTION));
		return ERR_NO_CONNECTION;
		break;
	default:
		break;
	}
#endif /* < Qt-6.0 */

	QNetworkRequest request(createRequest(session, soapEndPoint,
	    QByteArray(), requestData.length(), returnMtomXop));

	/* Send request */
	QNetworkReply *reply = sendRequest(nam, request, requestData);
	if (Q_UNLIKELY(reply == Q_NULLPTR)) {
		logErrorNL("%s", "No reply.");
		return ERR_REPLY;
	}

	/* Wait for reply */
	enum ErrCode errCode = waitReplyFinished(reply);
	if (Q_UNLIKELY(ERR_NO_ERROR != errCode)) {
		session.ctx()->setLastIsdsMsg(errorDescription(errCode));
		return errCode;
	}

	/* Process reply */
	QUrl redirUrl;
	QList<QNetworkCookie> newCookies;
	errCode = processReply(reply, replyData, redirUrl, newCookies);

	/* Reply processed, save cookies and new URL if were sent. */
	if (!redirUrl.isEmpty()) {
		session.ctx()->setUrl(macroStdMove(redirUrl));
	}
	if (!newCookies.isEmpty()) {
		session.ctx()->setCookies(macroStdMove(newCookies));
	}

	session.ctx()->setLastIsdsMsg(errorDescription(errCode));

	return errCode;
}

QString Isds::Connection::errorDescription(enum ErrCode errCode)
{
	QString txt(tr("Success."));
	switch (errCode) {
	case ERR_NO_ERROR:
		txt = tr("Successfully finished.");
		break;
	case ERR_NO_CONNECTION:
		txt = tr("Internet connection is probably not available. Check your network settings.");
		break;
	case ERR_BAD_REQUEST:
		txt = tr("Authorization failed. Server complains about a bad request.");
		break;
	case ERR_REPLY:
		txt = tr("Error reply.");
		break;
	case ERR_SERVER_UNAVAILABLE:
		txt = tr("ISDS server is out of service. Scheduled maintenance in progress. Try again later.");
		break;
	case ERR_TIMEOUT:
		txt = tr("Connection with ISDS server timed out. Request was cancelled.");
		break;
	case ERR_UNAUTHORIZED:
		txt = tr("Authorization failed. Check your credentials in the data-box settings whether they are correct and try again. It is also possible that your password expired. Check your credentials validity by logging in to the data box using the ISDS web portal.");
		break;
	case ERR_UNAUTHORIZED_HOTP:
		txt = tr("OTP authorization failed. OTP code is wrong or expired.");
		break;
	case ERR_UNAUTHORIZED_TOTP:
		txt = tr("SMS authorization failed. SMS code is wrong or expired.");
		break;
	case ERR_UNAUTHORIZED_TOTP_SMS:
		txt = tr("SMS authorization failed. SMS code couldn't be sent. Your order on premium SMS has been exhausted or cancelled.");
		break;
	case ERR_UNSPECIFIED:
	default:
		txt = tr("An communication error. See log for more detail.");
		break;
	}
	return txt;
}
