/*
 * Copyright (C) 2014-2025 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

#include <QCryptographicHash>
#include <QFile>
#include <QFileInfo>
#include <QMimeDatabase>
#include <QMimeType>

#include "src/datovka_shared/compat/compiler.h" /* macroStdMove */
#include "src/datovka_shared/identifiers/account_id.h"
#include "src/datovka_shared/isds/message_interface.h"
#include "src/datovka_shared/isds/message_interface_vodz.h"
#include "src/datovka_shared/log/log.h"
#include "src/datovka_shared/utility/strings.h"
#include "src/datovka_shared/worker/pool.h"
#include "src/files.h"
#include "src/global.h"
#include "src/isds/isds_tasks.h"
#include "src/net/db_wrapper.h"
#include "src/settings/account.h"
#include "src/settings/accounts.h"
#include "src/settings/prefs_specific.h"
#include "src/setwrapper.h"
#include "src/sqlite/file_db_container.h"
#include "src/sqlite/file_db.h"
#include "src/sqlite/zfo_db.h"
#include "src/worker/task_change_password.h"
#include "src/worker/task_credit_info.h"
#include "src/worker/task_download_account_info.h"
#include "src/worker/task_download_attachment.h"
#include "src/worker/task_download_big_message.h"
#include "src/worker/task_download_delivery_info.h"
#include "src/worker/task_download_dt_info.h"
#include "src/worker/task_download_message.h"
#include "src/worker/task_download_message_list.h"
#include "src/worker/task_find_databox.h"
#include "src/worker/task_find_databox_fulltext.h"
#include "src/worker/task_import_zfo.h"
#include "src/worker/task_pdz_info.h"
#include "src/worker/task_recipient_info.h"
#include "src/worker/task_send_big_message.h"
#include "src/worker/task_send_message.h"
#include "src/worker/task_send_sms.h"
#include "src/worker/task_upload_attachment.h"

bool Isds::Tasks::changePassword(const AcntId &acntId, const QString &oldPwd,
    const QString &newPwd, const QString &otpCode, QString &errTxt)
{
	debugFuncCall();

	if (Q_UNLIKELY((GlobInstcs::workPoolPtr == Q_NULLPTR) ||
	        (GlobInstcs::acntMapPtr == Q_NULLPTR))) {
		Q_ASSERT(0);
		return false;
	}

	bool success = false;
	errTxt = tr("Unknown error");

	/* These fields must be set */
	if (!acntId.isValid() || oldPwd.isEmpty() || newPwd.isEmpty()) {
		logErrorNL("%s", "Internal error. Wrong username.");
		return success;
	}

	/* Set OTP type and OTP code */
	enum Type::OtpMethod otpMethod = Type::OM_UNKNOWN;
	AcntData aData = GlobInstcs::acntMapPtr->acntData(acntId);
	if (aData.loginMethod() == AcntData::LIM_UNAME_PWD_TOTP) {
		otpMethod = Type::OM_TIME;
	} else if (aData.loginMethod() == AcntData::LIM_UNAME_PWD_HOTP) {
		otpMethod = Type::OM_HMAC;
	}

	TaskChangePassword *task = new (::std::nothrow) TaskChangePassword(
	    acntId, otpMethod, oldPwd, newPwd, otpCode);
	if (Q_UNLIKELY(task == Q_NULLPTR)) {
		Q_ASSERT(0);
		return false;
	}
	task->setAutoDelete(false);
	if (GlobInstcs::workPoolPtr->runSingle(task)) {
		success = TaskChangePassword::DL_SUCCESS == task->m_result;
		errTxt = macroStdMove(task->m_isdsText);
	}
	delete task; task = Q_NULLPTR;

	if (success) {
		/* Store new password to account settings */
		aData.setPassword(newPwd);
		GlobInstcs::acntMapPtr->updateAccount(aData);
	}

	return success;
}

void Isds::Tasks::downloadMessage(const AcntId &acntId,
    enum Messages::MessageType messageType, qint64 msgId)
{
	debugFuncCall();

	if (Q_UNLIKELY((GlobInstcs::workPoolPtr == Q_NULLPTR))) {
		Q_ASSERT(0);
		return;
	}

	TaskDownloadMessage *task = new (::std::nothrow) TaskDownloadMessage(
	    acntId, msgId, messageType);
	if (task != Q_NULLPTR) {
		task->setAutoDelete(true);
		GlobInstcs::workPoolPtr->assignLo(task);
	}
}

void Isds::Tasks::downloadBigMessage(const AcntId &acntId,
    enum Messages::MessageType messageType, qint64 msgId, bool useMtomXop)
{
	debugFuncCall();

	if (Q_UNLIKELY((GlobInstcs::workPoolPtr == Q_NULLPTR))) {
		Q_ASSERT(0);
		return;
	}

	TaskDownloadBigMessage *task = new (::std::nothrow)
	    TaskDownloadBigMessage(acntId, msgId, messageType, useMtomXop);
	if (task != Q_NULLPTR) {
		task->setAutoDelete(true);
		GlobInstcs::workPoolPtr->assignHi(task);
	}
}

QString Isds::Tasks::findDatabox(const AcntId &acntId, const QString &dbID,
    enum Type::DbType dbType)
{
	debugFuncCall();

	if (Q_UNLIKELY((GlobInstcs::workPoolPtr == Q_NULLPTR))) {
		Q_ASSERT(0);
		return QString();
	}

	TaskFindDatabox *task = new (::std::nothrow) TaskFindDatabox(
	    acntId, dbID, dbType);
	if (Q_UNLIKELY(task == Q_NULLPTR)) {
		Q_ASSERT(0);
		return QString();
	}
	task->setAutoDelete(false);
	QString dbInfo;
	if (GlobInstcs::workPoolPtr->runSingle(task)) {
		dbInfo = macroStdMove(task->m_dbInfo);
	}
	delete task; task = Q_NULLPTR;

	return dbInfo;
}

/* Maximal number of search results on one page: ISDS allows 100 */
#define ISDS_SEARCH2_ITEMS_PER_PAGE 100

bool Isds::Tasks::findDataboxFulltext(const AcntId &acntId,
    const QString &phrase, enum Type::FulltextSearchType searchType,
    enum Type::DbType searchScope, long int page,
    QList<DbResult2> &dbList, long int &totalCount,
    long int &currentCount, long int &position, bool &lastPage,
    QString &lastError)
{
	TaskFindDataboxFulltext *task =
	    new (::std::nothrow) TaskFindDataboxFulltext(acntId, phrase,
	        searchType, searchScope, page, ISDS_SEARCH2_ITEMS_PER_PAGE,
	        false);
	if (Q_UNLIKELY(task == Q_NULLPTR)) {
		Q_ASSERT(0);
		return false;
	}
	task->setAutoDelete(false);
	bool success = false;
	if (GlobInstcs::workPoolPtr->runSingle(task)) {
		success = TaskFindDataboxFulltext::DL_SUCCESS == task->m_result;
		dbList = macroStdMove(task->m_dbList);
		totalCount = task->m_totalCount;
		currentCount = task->m_currentCount;
		position = task->m_position;
		lastPage = task->m_lastPage;
		lastError = macroStdMove(task->m_lastError);
	}
	delete task; task = Q_NULLPTR;

	return success;
}

QString Isds::Tasks::getAccountDbId(const AcntId &acntId)
{
	debugFuncCall();

	if (Q_UNLIKELY((GlobInstcs::workPoolPtr == Q_NULLPTR))) {
		Q_ASSERT(0);
		return QString();
	}

	/* Last param: false = account info won't store into database */
	TaskDownloadAccountInfo *task =
	    new (::std::nothrow) TaskDownloadAccountInfo(acntId, false);
	if (Q_UNLIKELY(task == Q_NULLPTR)) {
		Q_ASSERT(0);
		return QString();
	}
	task->setAutoDelete(false);
	QString dbId;
	if (GlobInstcs::workPoolPtr->runSingle(task)) {
		dbId = macroStdMove(task->m_dbID);
	}
	delete task; task = Q_NULLPTR;
	return dbId;
}

void Isds::Tasks::getAccountInfo(const AcntId &acntId)
{
	debugFuncCall();

	if (Q_UNLIKELY(GlobInstcs::workPoolPtr == Q_NULLPTR)) {
		Q_ASSERT(0);
		return;
	}

	/* Last param: true = store account info into database */
	TaskDownloadAccountInfo *task =
	    new (::std::nothrow) TaskDownloadAccountInfo(acntId, true);
	if (Q_NULLPTR != task) {
		task->setAutoDelete(true);
		GlobInstcs::workPoolPtr->assignHi(task);
	}
}

void Isds::Tasks::downloadDTInfo(const AcntId &acntId, const QString &dbID)
{
	debugFuncCall();

	if (Q_UNLIKELY((GlobInstcs::workPoolPtr == Q_NULLPTR) ||
		dbID.isEmpty())) {
		Q_ASSERT(0);
		return;
	}

	TaskDTInfo *task =
	    new (::std::nothrow) TaskDTInfo(acntId, dbID);
	if (Q_UNLIKELY(task == Q_NULLPTR)) {
		Q_ASSERT(0);
	}
	task->setAutoDelete(false);
	DTInfoOutput dtInfo;
	if (GlobInstcs::workPoolPtr->runSingle(task)) {
		dtInfo = macroStdMove(task->m_dtInfo);
	}
	delete task; task = Q_NULLPTR;

	/* Store data into db */
	if (!DbWrapper::insertDTInfoToDb(dbID, dtInfo)) {
		logErrorNL("%s", "DB: Cannot store long term storage info into database");
	}
}

void Isds::Tasks::getDeliveryInfo(const AcntId &acntId, qint64 msgId)
{
	debugFuncCall();

	if (Q_UNLIKELY((GlobInstcs::workPoolPtr == Q_NULLPTR))) {
		Q_ASSERT(0);
		return;
	}

	TaskDownloadDeliveryInfo *task =
	    new (::std::nothrow) TaskDownloadDeliveryInfo(acntId, msgId);
	if (Q_UNLIKELY(task == Q_NULLPTR)) {
		Q_ASSERT(0);
		return;
	}
	task->setAutoDelete(true);
	GlobInstcs::workPoolPtr->assignHi(task);
	delete task; task = Q_NULLPTR;
}

QString Isds::Tasks::importZfoMessages(const QList<AcntId> &acntIdList,
    const QStringList &filePathList, bool authenticate)
{
	debugFuncCall();

	if (Q_UNLIKELY((GlobInstcs::workPoolPtr == Q_NULLPTR) ||
	        (GlobInstcs::acntMapPtr == Q_NULLPTR))) {
		Q_ASSERT(0);
		return QString();
	}

	int zfoTotal = filePathList.count();
	int zfoNumber = 0;

	foreach (const QString &file, filePathList) {
		++zfoNumber;
		TaskImportZfo *task = new (::std::nothrow) TaskImportZfo(
		    acntIdList, file, authenticate, zfoNumber, zfoTotal);
		if (task != Q_NULLPTR) {
			task->setAutoDelete(true);
			GlobInstcs::workPoolPtr->assignLo(task);
		}
	}

	return tr("ZFO import is running... Wait until import will be finished.");
}

void Isds::Tasks::recipientInfo(const AcntId &acntId, const QString &dbID,
    bool senderOvm, bool canUseInitReply, const QList<PDZInfoRec> &pdzInfos)
{
	debugFuncCall();

	if (Q_UNLIKELY((GlobInstcs::workPoolPtr == Q_NULLPTR))) {
		Q_ASSERT(0);
		return;
	}

	TaskRecipientInfo *task = new (::std::nothrow) TaskRecipientInfo(
	    acntId, dbID, senderOvm, canUseInitReply, pdzInfos);
	if (task != Q_NULLPTR) {
		task->setAutoDelete(true);
		GlobInstcs::workPoolPtr->assignHi(task);
	}
}

bool Isds::Tasks::sendGovRequest(const AcntId &acntId, const Message &msg,
    QSet<QString> &transactIds)
{
	debugFuncCall();

	/* List of unique identifiers. */
	QStringList taskIdentifiers;
	const QDateTime currentTime(QDateTime::currentDateTimeUtc());
	taskIdentifiers.append(acntId.username() + "_"
	    + currentTime.toString() + "_"
	    + Utility::generateRandomString(6));

#if (QT_VERSION >= QT_VERSION_CHECK(5, 14, 0))
	transactIds = QSet<QString>(taskIdentifiers.begin(), taskIdentifiers.end());
#else /* < Qt-5.14.0 */
	transactIds = taskIdentifiers.toSet();
#endif /* >= Qt-5.14.0 */

	TaskSendMessage *task = new (::std::nothrow) TaskSendMessage(acntId,
	    msg, taskIdentifiers.first());
	if (Q_UNLIKELY(task == Q_NULLPTR)) {
		Q_ASSERT(0);
		return false;
	}
	task->setAutoDelete(true);
	GlobInstcs::workPoolPtr->assignHi(task);

	return true;
}

bool Isds::Tasks::sendMessage(const AcntId &acntId, qint64 dmID,
    const QString &dmAnnotation, const DataboxListModel *databoxModel,
    const FileListModel *attachModel,
    const QString &dmLegalTitleLaw, const QString &dmLegalTitleYear,
    const QString &dmLegalTitleSect, const QString &dmLegalTitlePar,
    const QString &dmLegalTitlePoint, const QString &dmToHands,
    const QString &dmRecipientRefNumber, const QString &dmRecipientIdent,
    const QString &dmSenderRefNumber, const QString &dmSenderIdent,
    bool dmOVM, bool dmPublishOwnID, bool dmAllowSubstDelivery,
    bool dmPersonalDelivery, int idLevel, bool vodz, QSet<QString> &transactIds)
{
	debugFuncCall();

	if (Q_UNLIKELY((GlobInstcs::workPoolPtr == Q_NULLPTR) ||
	        (GlobInstcs::fileDbsPtr == Q_NULLPTR) ||
	        (GlobInstcs::zfoDbPtr == Q_NULLPTR) ||
	        (GlobInstcs::acntMapPtr == Q_NULLPTR))) {
		Q_ASSERT(0);
		return false;
	}

	if (Q_UNLIKELY(databoxModel == Q_NULLPTR)) {
		logErrorNL("%s", "Cannot access data box model.");
		Q_ASSERT(0);
		return false;
	}

	if (Q_UNLIKELY(attachModel == Q_NULLPTR)) {
		logErrorNL("%s", "Cannot access attachment model.");
		Q_ASSERT(0);
		return false;
	}

	Envelope envelope;
	/* Fill message envelope */
	envelope.setDmAnnotation(dmAnnotation);
	envelope.setDmLegalTitleLawStr(dmLegalTitleLaw);
	envelope.setDmLegalTitleYearStr(dmLegalTitleYear);
	envelope.setDmLegalTitleSect(dmLegalTitleSect);
	envelope.setDmLegalTitlePar(dmLegalTitlePar);
	envelope.setDmLegalTitlePoint(dmLegalTitlePoint);
	envelope.setDmToHands(dmToHands);
	envelope.setDmRecipientRefNumber(dmRecipientRefNumber);
	envelope.setDmRecipientIdent(dmRecipientIdent);
	envelope.setDmSenderRefNumber(dmSenderRefNumber);
	envelope.setDmSenderIdent(dmSenderIdent);
	envelope.setDmAllowSubstDelivery(dmAllowSubstDelivery ?
	   Type::BOOL_TRUE : Type::BOOL_FALSE);
	envelope.setDmPersonalDelivery(dmPersonalDelivery ?
	   Type::BOOL_TRUE : Type::BOOL_FALSE);
	envelope.setDmMessageStatus(Type::MS_POSTED);
	envelope.setDmOVM(dmOVM ? Type::BOOL_TRUE : Type::BOOL_FALSE);
	envelope.setDmPublishOwnID(dmPublishOwnID ?
	    Type::BOOL_TRUE : Type::BOOL_FALSE);
	if (dmPublishOwnID) {
		envelope.setIdLevel(idLevel);
	}
	if (vodz) {
		envelope.setDmVODZ(Type::BOOL_TRUE);
		envelope.setAttsNum(attachModel->allEntries().size());
	}

	FileDb *fDb = GlobInstcs::fileDbsPtr->accessFileDb(
	    GlobalSettingsQmlWrapper::dbPath(), acntId.username(),
	    PrefsSpecific::dataOnDisk(
	        *GlobInstcs::prefsPtr, GlobInstcs::acntMapPtr->acntData(acntId)));
	if (Q_UNLIKELY(fDb == Q_NULLPTR)) {
		logErrorNL("Cannot access file database for username '%s'.",
		    acntId.username().toUtf8().constData());
		return false;
	}

	QList<Document> documents;
	QList<DmExtFile> dmExtFiles;
	Type::FileMetaType fileMetaType = Type::FMT_MAIN;
	/* Load file contents from storage or database to file structure */
	foreach (const FileListModel::Entry &file, attachModel->allEntries()) {

		if (vodz) {
			DmAtt dmAtt;
			if (uploadAttachment(acntId, file.filePath(),
			     file.fileName(), true, dmAtt)) {
				DmExtFile dmExtFile;
				dmExtFile.setFileMetaType(fileMetaType);
				dmExtFile.setDmAtt(dmAtt);
				dmExtFiles.append(dmExtFile);
				fileMetaType = Type::FMT_ENCLOSURE;
			} else {
				logErrorNL("Cannot upload file '%s'.",
				    file.fileName().toUtf8().constData());
				return false;
			}
			continue;
		}

		Document document;
		document.setFileDescr(file.fileName());
		document.setFileMetaType(fileMetaType);

		/*
		 * Since 2011 Mime Type can be empty and MIME type will
		 * be filled up on the ISDS server. It allows sending files
		 * with special mime types without recognition by application.
		 */
		{
			/* Must be empty non-null string. */
			QString mimeType = QStringLiteral("");

			const QString suffix = QFileInfo(file.fileName()).completeSuffix().toLower();
			if (suffix == QStringLiteral("xml")) {
				mimeType = QStringLiteral("application/xml");
			} else if (suffix == QStringLiteral("zip")) {
				mimeType = QStringLiteral("application/zip");
			}

			document.setMimeType(macroStdMove(mimeType));
		}

		if (file.fileId() > 0) {
			// load file content from database, valid file ID
			document = fDb->getFileFromDb(file.fileId());
			document.setFileMetaType(fileMetaType);
		} else if (file.fileId() == Files::DB_ZFO_ID) {
			// load zfo content from zfo database
			document.setBase64Content(
			    GlobInstcs::zfoDbPtr->getZfoContentFromDb(dmID,
			        acntId.testing()));
		} else if (!file.filePath().isEmpty()) {
			// load file content from storage (path)
			QFile fin(file.filePath());
			if (!fin.open(QIODevice::ReadOnly)) {
				logErrorNL("Cannot load file content from path: '%s'.",
				    file.filePath().toUtf8().constData());
				return false;
			}
			document.setBinaryContent(fin.readAll());
		} else if (file.fileId() == Files::NO_FILE_ID) {
			// load content from model
			document.setBinaryContent(file.binaryContent());
		} else {
			logErrorNL("%s", "Internal error. No file to send.");
			return false;
		}
		documents.append(document);
		fileMetaType = Type::FMT_ENCLOSURE;
	}

	/* Set total attachment size in KB from attachment model. */
	envelope.setDmAttachmentSize(attachModel->dataSizeSum() / 1024);

	/* List of unique identifiers. */
	QStringList taskIdentifiers;
	const QDateTime currentTime(QDateTime::currentDateTimeUtc());

	const QList<DataboxModelEntry> dbList = databoxModel->allEntries();

	/*
	 * Generate unique identifiers.
	 * These must be complete before creating first task.
	 */
	foreach (const DataboxModelEntry &db, dbList) {
		taskIdentifiers.append(acntId.username() + "_"
		    + db.dbID() + "_"
		    + currentTime.toString() + "_"
		    + Utility::generateRandomString(6));
	}

#if (QT_VERSION >= QT_VERSION_CHECK(5, 14, 0))
	transactIds = QSet<QString>(taskIdentifiers.begin(), taskIdentifiers.end());
#else /* < Qt-5.14.0 */
	transactIds = taskIdentifiers.toSet();
#endif /* >= Qt-5.14.0 */

	Message message;
	message.setDocuments(macroStdMove(documents));
	if (vodz) {
		message.setExtFiles(macroStdMove(dmExtFiles));
	}

	/* Send message to all recipients. */
	for (int i = 0; i < dbList.size(); ++i) {
		const DataboxModelEntry &db(dbList.at(i));
		envelope.setDbIDRecipient(db.dbID());
		envelope.setDmRecipient(db.dbName());
		envelope.setDmRecipientAddress(db.dbAddress());
		envelope.setDmType(Envelope::dmType2Char(db.dmType()));
		message.setEnvelope(envelope);

		if (vodz) {
			TaskSendBigMessage *task =
			    new (::std::nothrow) TaskSendBigMessage(
				acntId, message, taskIdentifiers.at(i));
			if (task != Q_NULLPTR) {
				task->setAutoDelete(true);
				GlobInstcs::workPoolPtr->assignHi(task);
			}
		} else {
			TaskSendMessage *task =
			    new (::std::nothrow) TaskSendMessage(
				acntId, message, taskIdentifiers.at(i));
			if (task != Q_NULLPTR) {
				task->setAutoDelete(true);
				GlobInstcs::workPoolPtr->assignHi(task);
			}
		}
	}

	return true;
}

bool Isds::Tasks::sendSMS(const AcntId &acntId, const QString &oldPwd)
{
	debugFuncCall();

	if (Q_UNLIKELY((GlobInstcs::workPoolPtr == Q_NULLPTR))) {
		Q_ASSERT(0);
		return false;
	}

	TaskSendSMS *task = new (::std::nothrow) TaskSendSMS(acntId, oldPwd);
	if (Q_UNLIKELY(task == Q_NULLPTR)) {
		Q_ASSERT(0);
		return false;
	}
	task->setAutoDelete(false);
	bool success = false;
	if (GlobInstcs::workPoolPtr->runSingle(task)) {
		success = TaskSendSMS::DL_SUCCESS == task->m_result;
	}
	delete task; task = Q_NULLPTR;
	return success;
}

#define ANY_EXCEPT_VAULT \
	(Isds::Type::MFS_POSTED | Isds::Type::MFS_STAMPED | \
	 Isds::Type::MFS_INFECTED | Isds::Type::MFS_DELIVERED | \
	 Isds::Type::MFS_ACCEPTED_FICT | Isds::Type::MFS_ACCEPTED | \
	 Isds::Type::MFS_READ | Isds::Type::MFS_UNDELIVERABLE | \
	 Isds::Type::MFS_REMOVED)
/*!
 * @brief Maximum length of message list to be downloaded.
 */
#define MESSAGE_LIST_LIMIT 100000

void Isds::Tasks::syncSingleAccount(const AcntId &acntId,
    enum Messages::MessageType msgDirect)
{
	debugFuncCall();

	if (Q_UNLIKELY((GlobInstcs::workPoolPtr == Q_NULLPTR))) {
		Q_ASSERT(0);
		return;
	}

	TaskDownloadMessageList *task =
	    new (::std::nothrow) TaskDownloadMessageList(acntId, msgDirect,
	    GlobalSettingsQmlWrapper::downloadOnlyNewMsgs() ?
	         ANY_EXCEPT_VAULT : Type::MFS_ANY,
	    1, MESSAGE_LIST_LIMIT, GlobInstcs::workPoolPtr,
	    GlobalSettingsQmlWrapper::downloadCompleteMsgs());
	if (task != Q_NULLPTR) {
		task->setAutoDelete(true);
		GlobInstcs::workPoolPtr->assignHi(task);
	}
}

void Isds::Tasks::downloadPdzInfo(const AcntId &acntId,
    const QString &dbID)
{
	debugFuncCall();

	if (Q_UNLIKELY((GlobInstcs::workPoolPtr == Q_NULLPTR) ||
		dbID.isEmpty())) {
		Q_ASSERT(0);
		return;
	}

	TaskPDZInfo *task =
	    new (::std::nothrow) TaskPDZInfo(acntId, dbID);
	if (Q_UNLIKELY(task == Q_NULLPTR)) {
		Q_ASSERT(0);
		return;
	}
	task->setAutoDelete(true);
	GlobInstcs::workPoolPtr->assignHi(task);
}

QList<Isds::CreditEvent> Isds::Tasks::downloadCreditInfo(const AcntId &acntId,
    const QString &dbID, const QDate &fromDate, const QDate &toDate,
    qint64 &currentCredit, QString &notifEmail)
{
	debugFuncCall();

	if (Q_UNLIKELY((GlobInstcs::workPoolPtr == Q_NULLPTR) ||
		dbID.isEmpty())) {
		Q_ASSERT(0);
		return QList<CreditEvent>();
	}

	TaskCreditInfo *task =
	    new (::std::nothrow) TaskCreditInfo(acntId, dbID, fromDate, toDate);
	if (Q_UNLIKELY(task == Q_NULLPTR)) {
		Q_ASSERT(0);
		return QList<CreditEvent>();
	}
	task->setAutoDelete(false);
	QList<CreditEvent> creditEvents;
	if (GlobInstcs::workPoolPtr->runSingle(task)) {
		currentCredit = task->m_currentCredit;
		notifEmail = macroStdMove(task->m_notifEmail);
		creditEvents = macroStdMove(task->m_creditEvents);
	}
	delete task; task = Q_NULLPTR;

	return creditEvents;
}

/*!
 * @brief Get MIME type from file.
 *
 * @param[in] fi File info.
 * @return MIME type.
 */
static
const QString getMimeType(const QFileInfo &fi)
{
	if (fi.suffix().toLower() == "zfo") {
		return QStringLiteral("application/vnd.software602.filler.form-xml-zip");
	}

#if QT_VERSION >= QT_VERSION_CHECK(5, 0, 0)
	QMimeDatabase db;
	QMimeType type = db.mimeTypeForFile(fi);
	return type.name();
#endif
	return QStringLiteral("application/octet-stream");
}

/*!
 * @brief Compute hash from file.
 *
 * @param[in] filePath File path.
 * @param[in] hashAlgorithm Hash algorithm.
 * @return Hash in hexa or null.
 */
static
const QByteArray fileChecksum(const QString &filePath,
    QCryptographicHash::Algorithm hashAlgorithm)
{
	QFile f(filePath);
	if (f.open(QFile::ReadOnly)) {
		QCryptographicHash hash(hashAlgorithm);
		if (hash.addData(&f)) {
			return hash.result().toHex();
		}
	}
	return QByteArray();
}

bool Isds::Tasks::uploadAttachment(const AcntId &acntId,
    const QString &filePath, const QString &fileName, bool useMtomXop,
    DmAtt &dmAtt)
{
	debugFuncCall();

	if (Q_UNLIKELY((GlobInstcs::workPoolPtr == Q_NULLPTR) ||
		filePath.isEmpty() || fileName.isEmpty())) {
		Q_ASSERT(0);
		return false;
	}

	if (fileName.length() > 255) {
		Q_ASSERT(0);
		return false;
	}
	TaskUploadAttachment *task =
	    new (::std::nothrow) TaskUploadAttachment(acntId,
	    fileName, getMimeType(QFileInfo(filePath)), filePath, useMtomXop);
	if (Q_UNLIKELY(task == Q_NULLPTR)) {
		Q_ASSERT(0);
		return false;
	}
	task->setAutoDelete(false);
	GlobInstcs::workPoolPtr->runSingle(task);
	dmAtt = task->m_dmAtt;
	delete task; task = Q_NULLPTR;

	QByteArray sha = fileChecksum(filePath, QCryptographicHash::Sha256);
	QByteArray sha3 = fileChecksum(filePath, QCryptographicHash::Sha3_256);

	return (dmAtt.dmAttHash1() == sha && dmAtt.dmAttHash2() == sha3);
}

void Isds::Tasks::downloadAttachment(const AcntId &acntId, qint64 dmId,
    qint64 attId, bool useMtomXop)
{
	debugFuncCall();

	if (Q_UNLIKELY((GlobInstcs::workPoolPtr == Q_NULLPTR))) {
		Q_ASSERT(0);
		return;
	}

	TaskDownloadAttachment *task =
	    new (::std::nothrow) TaskDownloadAttachment(acntId, dmId,
	    attId, useMtomXop);
	if (Q_UNLIKELY(task == Q_NULLPTR)) {
		Q_ASSERT(0);
		return;
	}
	task->setAutoDelete(true);
	GlobInstcs::workPoolPtr->assignHi(task);
}
