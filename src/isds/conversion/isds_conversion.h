/*
 * Copyright (C) 2014-2025 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

#pragma once

#include <QCoreApplication> // Q_DECLARE_TR_FUNCTIONS
#include <QString>

#include "src/datovka_shared/isds/types.h"

/*!
 * @brief Provides conversion functions for ISDS types.
 */
class IsdsConversion {
	Q_DECLARE_TR_FUNCTIONS(IsdsConversion)

private:
	/*!
	 * @brief Private constructor.
	 */
	IsdsConversion(void);

public:
	/*!
	 * @brief Returns sender type from description.
	 *
	 * @param[in] typeDescr Sender type description.
	 * @return Sender type as given by GetMessageAuthor service.
	 */
	static
	enum Isds::Type::SenderType descrToSenderType(const QString &typeDescr);

	/*!
	 * @brief Convert MEP resolution code from isds to enum.
	 *
	 * @param[in] status Isds MEP resolution code.
	 * @return MEP resolution code.
	 */
	static
	enum Isds::Type::MepResolution isdsMepStatusToMepResolution(int status);
};
