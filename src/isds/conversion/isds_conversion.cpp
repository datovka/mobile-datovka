/*
 * Copyright (C) 2014-2025 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

#include "src/isds/conversion/isds_conversion.h"

/* MEP STATES */
#define MRSTATE_UNRECOGNISED -1
#define MRSTATE_ACK_REQUESTED 1
#define MRSTATE_MR_ACK 2
#define MRSTATE_ACK_EXPIRED 3

enum Isds::Type::SenderType IsdsConversion::descrToSenderType(
    const QString &typeDescr)
{
	if ((typeDescr == "Primary user") || (typeDescr == tr("Primary user"))) {
		return Isds::Type::ST_PRIMARY;
	} else if ((typeDescr == "Entrusted user") ||
	    (typeDescr == tr("Entrusted user"))) {
		return Isds::Type::ST_ENTRUSTED;
	} else if ((typeDescr == "Administrator") ||
	    (typeDescr == tr("Administrator"))) {
		return Isds::Type::ST_ADMINISTRATOR;
	} else if ((typeDescr == "Official") || (typeDescr == tr("Official"))) {
		return Isds::Type::ST_OFFICIAL;
	} else if ((typeDescr == "Virtual") || (typeDescr == tr("Virtual"))) {
		return Isds::Type::ST_VIRTUAL;
	} else if ((typeDescr == "Liquidator") || (typeDescr == tr("Liquidator"))) {
		return Isds::Type::ST_LIQUIDATOR;
	} else if ((typeDescr == "Receiver") || (typeDescr == tr("Receiver"))) {
		return Isds::Type::ST_RECEIVER;
	} else if ((typeDescr == "Guardian") || (typeDescr == tr("Guardian"))) {
		return Isds::Type::ST_GUARDIAN;
	}

	return Isds::Type::ST_PRIMARY;
}

enum Isds::Type::MepResolution IsdsConversion::isdsMepStatusToMepResolution(
    int status)
{
	switch (status) {
	case MRSTATE_ACK_REQUESTED:
		return Isds::Type::MR_ACK_REQUESTED;
		break;
	case MRSTATE_MR_ACK:
		return Isds::Type::MR_ACK;
		break;
	case MRSTATE_ACK_EXPIRED:
		return Isds::Type::MR_ACK_EXPIRED;
		break;
	case MRSTATE_UNRECOGNISED:
		return Isds::Type::MR_UNRECOGNISED;
		break;
	default:
		return Isds::Type::MR_UNKNOWN;
		break;
	}
}
