/*
 * Copyright (C) 2014-2025 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

#include <QTimeZone>

#include "src/isds/conversion/isds_time_conversion.h"

QDateTime Isds::isoDateTimeStrToUtcDateTime(const QString &dateTimeStr)
{
	QDateTime dateTime = QDateTime::fromString(dateTimeStr, Qt::ISODateWithMs);
	dateTime =
#if (QT_VERSION >= QT_VERSION_CHECK(6, 5, 0))
	    dateTime.toTimeZone(QTimeZone(QTimeZone::UTC));
#else /* < Qt-6.5 */
	    dateTime.toTimeSpec(Qt::UTC);
#endif /* >= Qt-6.5 */
	if (dateTime.isValid()) {
		return dateTime;
	} else {
		return QDateTime();
	}
}

QString Isds::dateTimetoIsoDateTimeStr(const QDateTime &dateTime)
{
	if (!dateTime.isNull()) {
		return dateTime.toString(Qt::ISODateWithMs);
	} else {
		return QString();
	}
}

QDate Isds::isoDateStrToDate(const QString &dateStr)
{
	return QDate::fromString(dateStr, Qt::ISODate);
}
