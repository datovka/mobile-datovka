/*
 * Copyright (C) 2014-2025 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

#pragma once

#include "src/datovka_shared/isds/message_interface.h"
#include "src/datovka_shared/isds/message_interface_vodz.h"

class QByteArray; /* Forward declaration. */
class QString; /* Forward declaration. */

namespace Isds {

	class DmStatus; /* Forward declaration. */
	class Session; /* Forward declaration. */

	/*!
	 * @brief Download signed received message using
	 *        <SignedBigMessageDownload>.
	 *
	 * @param[in,out] session Communication session.
	 * @param[in]     dmId Message identifier.
	 * @param[out]    message Downloaded message.
	 * @param[out]    status Status as reported by the ISDS.
	 * @param[out]    errMsg Error message.
	 * @return Error status.
	 */
	enum Type::Error getSignedReceivedBigMessageBase64(Session &session,
	    qint64 dmId, Message &message, DmStatus *status = Q_NULLPTR,
	    QString *errMsg = Q_NULLPTR);

	/*!
	 * @brief Download signed received message using
	 *        <SignedBigMessageDownload>.
	 *
	 * @param[in,out] session Communication session.
	 * @param[in]     dmId Message identifier.
	 * @param[out]    message Downloaded message.
	 * @param[out]    status Status as reported by the ISDS.
	 * @param[out]    errMsg Error message.
	 * @return Error status.
	 */
	enum Type::Error getSignedReceivedBigMessageMtomXop(Session &session,
	    qint64 dmId, Message &message, DmStatus *status = Q_NULLPTR,
	    QString *errMsg = Q_NULLPTR);

	/*!
	 * @brief Download signed sent message using
	 *        <SignedSentBigMessageDownload>.
	 *
	 * @param[in,out] session Communication session.
	 * @param[in]     dmId Message identifier.
	 * @param[out]    message Downloaded message.
	 * @param[out]    status Status as reported by the ISDS.
	 * @param[out]    errMsg Error message.
	 * @return Error status.
	 */
	enum Type::Error getSignedSentBigMessageBase64(Session &session,
	    qint64 dmId, Message &message, DmStatus *status = Q_NULLPTR,
	    QString *errMsg = Q_NULLPTR);

	/*!
	 * @brief Download signed sent message using
	 *        <SignedSentBigMessageDownload>.
	 *
	 * @param[in,out] session Communication session.
	 * @param[in]     dmId Message identifier.
	 * @param[out]    message Downloaded message.
	 * @param[out]    status Status as reported by the ISDS.
	 * @param[out]    errMsg Error message.
	 * @return Error status.
	 */
	enum Type::Error getSignedSentBigMessageMtomXop(Session &session,
	    qint64 dmId, Message &message, DmStatus *status = Q_NULLPTR,
	    QString *errMsg = Q_NULLPTR);

	/*!
	 * @brief Authenticate message using <AuthenticateBigMessage>.
	 *
	 * @param[in,out] session Communication session.
	 * @param[in]     rawData Message or delivery info in raw binary format.
	 * @param[out]    valid True if is valid.
	 * @param[out]    status Status as reported by the ISDS.
	 * @param[out]    errMsg Error message.
	 * @return Error status.
	 */
	enum Type::Error authenticateBigMessageBase64(Session &session,
	    const QByteArray &rawData, bool &valid,
	    DmStatus *status = Q_NULLPTR, QString *errMsg = Q_NULLPTR);

	/*!
	 * @brief Authenticate message using <AuthenticateBigMessage>.
	 *
	 * @param[in,out] session Communication session.
	 * @param[in]     fileName File name.
	 * @param[in]     fileMimeType File mime type.
	 * @param[in]     filePath File path.
	 * @param[out]    valid True if is valid.
	 * @param[out]    status Status as reported by the ISDS.
	 * @param[out]    errMsg Error message.
	 * @return Error status.
	 */
	enum Type::Error authenticateBigMessageMtomXop(Session &session,
	    const QString &fileName, const QString &fileMimeType,
	    const QString &filePath, bool &valid,
	    DmStatus *status = Q_NULLPTR, QString *errMsg = Q_NULLPTR);

	/*!
	 * @brief Download attachment from big message using
	 *        <DownloadAttachment>.
	 *
	 * @param[in,out] session Communication session.
	 * @param[in]     dmId Message identifier.
	 * @param[in]     attNum Attachment identifier.
	 * @param[out]    dmFile Attachment content.
	 * @param[out]    status Status as reported by the ISDS.
	 * @param[out]    errMsg Error message.
	 * @return Error status.
	 */
	enum Type::Error downloadAttachmentBase64(Session &session,
	    qint64 dmId, qint64 attNum, DmFile &dmFile,
	    DmStatus *status = Q_NULLPTR, QString *errMsg = Q_NULLPTR);

	/*!
	 * @brief Download attachment from big message using
	 *        <DownloadAttachment>.
	 *
	 * @param[in,out] session Communication session.
	 * @param[in]     dmId Message identifier.
	 * @param[in]     attId Attachment identifier.
	 * @param[out]    status Status as reported by the ISDS.
	 * @param[out]    errMsg Error message.
	 * @return Error status.
	 */
	enum Type::Error downloadAttachmentMtomXop(Session &session,
	    qint64 dmId, qint64 attId,
	    DmStatus *status = Q_NULLPTR, QString *errMsg = Q_NULLPTR);

	/*!
	 * @brief Upload attachment for big message using <UploadAttachment>.
	 *
	 * @param[in,out] session Communication session.
	 * @param[in]     dmFile Attachment file content.
	 * @param[out]    dmAtt Attachment identification.
	 * @param[out]    status Status as reported by the ISDS.
	 * @param[out]    errMsg Error message.
	 * @return Error status.
	 */
	enum Type::Error uploadAttachmentBase64(Session &session,
	    const DmFile &dmFile, DmAtt &dmAtt,
	    DmStatus *status = Q_NULLPTR, QString *errMsg = Q_NULLPTR);

	/*!
	 * @brief Upload attachment for big message using <UploadAttachment>.
	 *
	 * @param[in,out] session Communication session.
	 * @param[in]     fileName File name.
	 * @param[in]     fileMimeType File mime type.
	 * @param[in]     filePath File path.
	 * @param[out]    dmAtt Attachment identification.
	 * @param[out]    status Status as reported by the ISDS.
	 * @param[out]    errMsg Error message.
	 * @return Error status.
	 */
	enum Type::Error uploadAttachmentMtomXop(Session &session,
	    const QString &fileName, const QString &fileMimeType,
	    const QString &filePath, DmAtt &dmAtt,
	    DmStatus *status = Q_NULLPTR, QString *errMsg = Q_NULLPTR);

	/*!
	 * @brief Create and send big message using <CreateBigMessage>.
	 *
	 * @param[in,out] session Communication session.
	 * @param[in,out] message Message to be sent. The value of dmAnnotation,
	 *                        dbIDRecipient and at least one attachment with
	 *                        FMT_MAIN must be set.
	 *                        The dmID value is set upon successful sending.
	 * @param[out]    status Status as reported by the ISDS.
	 * @param[out]    errMsg Error message.
	 * @return Error status.
	 */
	enum Type::Error createBigMessage(Session &session, Message &message,
	    DmStatus *status = Q_NULLPTR, QString *errMsg = Q_NULLPTR);
}
