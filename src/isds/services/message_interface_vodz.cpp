/*
 * Copyright (C) 2014-2025 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

#include "src/datovka_shared/compat/compiler.h" /* macroStdMove */
#include "src/isds/io/connection.h"
#include "src/isds/isds_const.h"
#include "src/isds/services/helper.h" /* errCode2Error() */
#include "src/isds/services/message_interface_vodz.h"
#include "src/isds/services/message_interface_offline.h"
#include "src/isds/session/isds_session.h"
#include "src/isds/xml/message_interface_vodz.h"
#include "src/isds/xml/response_status.h"
#include "src/isds/xml/services_message.h"

/*!
 * @brief Download signed big message.
 *
 * @param[in]     rsmd Determines request type.
 * @param[in,out] session Communication session.
 * @param[in]     dmId Message identifier.
 * @param[out]    message Downloaded message.
 * @param[out]    status Status as reported by the ISDS.
 * @param[out]    errMsg Error message.
 * @return Error status.
 */
static
enum Isds::Type::Error getSignedBigMessageBase64(
    enum Isds::Xml::RequestSignedMessageData rsmd, Isds::Session &session,
    qint64 dmId, Isds::Message &message, Isds::DmStatus *status,
    QString *errMsg)
{
	if (Q_UNLIKELY(dmId < 0)) {
		return Isds::Type::ERR_INVAL;
	}

	if (Q_UNLIKELY(rsmd == Isds::Xml::RSMD_DELINFO)) {
		return Isds::Type::ERR_INVAL;
	}

	QByteArray response;
	enum Isds::Connection::ErrCode errCode = Isds::Connection::communicate(
	    session, MESSAGE_SERVICE_VODZ,
	    Isds::Xml::soapRequestGetSignedBigMessageBase64(rsmd, dmId),
	    response);
	if (errCode != Isds::Connection::ERR_NO_ERROR) {
		if (errMsg != Q_NULLPTR) {
			*errMsg = session.ctx()->lastIsdsMsg();
		}
		return errCode2Error(errCode);
	}

	bool iOk = false;
	Isds::DmStatus dmStatus = Isds::Xml::toDmStatus(response, &iOk);
	if (Q_UNLIKELY(!iOk)) {
		return Isds::Type::ERR_XML;
	}
	if (errMsg != Q_NULLPTR) {
		*errMsg = dmStatus.message();
	}
	iOk = dmStatus.ok();
	if (status != Q_NULLPTR) {
		*status = macroStdMove(dmStatus);
	}

	if (Q_UNLIKELY(!iOk)) {
		return Isds::Type::ERR_ISDS;
	}

	/* Obtain CMS message. */
	response = Isds::Xml::cmsZfoFromSoap(response, &iOk);
	if (Q_UNLIKELY(!iOk)) {
		return Isds::Type::ERR_XML;
	}

	/* Read message content. */
	message = Isds::toMessage(macroStdMove(response), &iOk);
	if (Q_UNLIKELY(!iOk)) {
		return Isds::Type::ERR_XML;
	}

	return Isds::Type::ERR_SUCCESS;
}

/*!
 * @brief Download signed big message.
 *
 * @param[in]     rsmd Determines request type.
 * @param[in,out] session Communication session.
 * @param[in]     dmId Message identifier.
 * @param[out]    message Downloaded message.
 * @param[out]    status Status as reported by the ISDS.
 * @param[out]    errMsg Error message.
 * @return Error status.
 */
static
enum Isds::Type::Error getSignedBigMessageMtomXop(
    enum Isds::Xml::RequestSignedMessageData rsmd, Isds::Session &session,
    qint64 dmId, Isds::Message &message, Isds::DmStatus *status,
    QString *errMsg)
{
	if (Q_UNLIKELY(dmId < 0)) {
		return Isds::Type::ERR_INVAL;
	}

	if (Q_UNLIKELY(rsmd == Isds::Xml::RSMD_DELINFO)) {
		return Isds::Type::ERR_INVAL;
	}

	QByteArray response;
	enum Isds::Connection::ErrCode errCode =
	    Isds::Connection::communicateMtomXop(session, MESSAGE_SERVICE_VODZ,
	    Isds::Xml::soapRequestGetSignedBigMessageMtomXop(rsmd, dmId),
	    response);
	if (errCode != Isds::Connection::ERR_NO_ERROR) {
		if (errMsg != Q_NULLPTR) {
			*errMsg = session.ctx()->lastIsdsMsg();
		}
		return errCode2Error(errCode);
	}

	bool iOk = false;
	Isds::DmStatus dmStatus = Isds::Xml::toDmStatus(response, &iOk);
	if (Q_UNLIKELY(!iOk)) {
		return Isds::Type::ERR_XML;
	}
	if (errMsg != Q_NULLPTR) {
		*errMsg = dmStatus.message();
	}
	iOk = dmStatus.ok();
	if (status != Q_NULLPTR) {
		*status = macroStdMove(dmStatus);
	}

	if (Q_UNLIKELY(!iOk)) {
		return Isds::Type::ERR_ISDS;
	}

	/* Obtain CMS message. */
	response = Isds::Xml::cmsZfoFromSoap(response, &iOk);
	if (Q_UNLIKELY(!iOk)) {
		return Isds::Type::ERR_XML;
	}

	/* Read message content. */
	message = Isds::toMessage(macroStdMove(response), &iOk);
	if (Q_UNLIKELY(!iOk)) {
		return Isds::Type::ERR_XML;
	}

	return Isds::Type::ERR_SUCCESS;
}

enum Isds::Type::Error Isds::getSignedReceivedBigMessageBase64(Session &session,
    qint64 dmId, Message &message, DmStatus *status, QString *errMsg)
{
	return getSignedBigMessageBase64(Xml::RSMD_RCVD_MSG, session, dmId,
	    message, status, errMsg);
}

enum Isds::Type::Error Isds::getSignedReceivedBigMessageMtomXop(Session &session,
    qint64 dmId, Message &message, DmStatus *status, QString *errMsg)
{
	return getSignedBigMessageMtomXop(Xml::RSMD_RCVD_MSG, session, dmId,
	    message, status, errMsg);
}

enum Isds::Type::Error Isds::getSignedSentBigMessageBase64(Session &session,
    qint64 dmId, Message &message, DmStatus *status, QString *errMsg)
{
	return getSignedBigMessageBase64(Xml::RSMD_SNT_MSG, session, dmId,
	    message, status, errMsg);
}

enum Isds::Type::Error Isds::getSignedSentBigMessageMtomXop(Session &session,
    qint64 dmId, Message &message, DmStatus *status, QString *errMsg)
{
	return getSignedBigMessageMtomXop(Xml::RSMD_SNT_MSG, session, dmId,
	    message, status, errMsg);
}

enum Isds::Type::Error Isds::authenticateBigMessageBase64(Session &session,
    const QByteArray &rawData, bool &valid, DmStatus *status, QString *errMsg)
{
	if (Q_UNLIKELY(rawData.isEmpty())) {
		return Type::ERR_INVAL;
	}

	QByteArray response;
	enum Connection::ErrCode errCode = Connection::communicate(session,
	    MESSAGE_SERVICE_VODZ,
	    Xml::soapRequestAuthenticateBigMessage(rawData), response);
	if (Q_UNLIKELY(errCode != Connection::ERR_NO_ERROR)) {
		if (errMsg != Q_NULLPTR) {
			*errMsg = session.ctx()->lastIsdsMsg();
		}
		return errCode2Error(errCode);
	}

	bool iOk = false;
	DmStatus dmStatus = Xml::toDmStatus(response, &iOk);
	if (Q_UNLIKELY(!iOk)) {
		return Type::ERR_XML;
	}
	if (errMsg != Q_NULLPTR) {
		*errMsg = dmStatus.message();
	}
	iOk = dmStatus.ok();
	if (status != Q_NULLPTR) {
		*status = macroStdMove(dmStatus);
	}

	if (Q_UNLIKELY(!iOk)) {
		return Type::ERR_ISDS;
	}

	valid = Xml::readAuthenticateMessageResponse(response, &iOk);
	if (Q_UNLIKELY(!iOk)) {
		return Type::ERR_XML;
	}

	return Type::ERR_SUCCESS;
}

enum Isds::Type::Error Isds::authenticateBigMessageMtomXop(Session &session,
    const QString &fileName, const QString &fileMimeType,
    const QString &filePath, bool &valid, DmStatus *status, QString *errMsg)
{
	if (Q_UNLIKELY(fileName.isEmpty() || fileMimeType.isEmpty() ||
	        filePath.isEmpty())) {
		return Type::ERR_INVAL;
	}

	QByteArray response;
	enum Connection::ErrCode errCode = Connection::uploadMtomXop(session,
	    MESSAGE_SERVICE_VODZ, QStringLiteral("AuthenticateBigMessage"),
	    Xml::soapRequestAuthenticateBigMessageMtomXop(),
	    fileName, fileMimeType, filePath, response);
	if (Q_UNLIKELY(errCode != Connection::ERR_NO_ERROR)) {
		if (errMsg != Q_NULLPTR) {
			*errMsg = session.ctx()->lastIsdsMsg();
		}
		return errCode2Error(errCode);
	}

	bool iOk = false;
	DmStatus dmStatus = Xml::toDmStatus(response, &iOk);
	if (Q_UNLIKELY(!iOk)) {
		return Type::ERR_XML;
	}
	if (errMsg != Q_NULLPTR) {
		*errMsg = dmStatus.message();
	}
	iOk = dmStatus.ok();
	if (status != Q_NULLPTR) {
		*status = macroStdMove(dmStatus);
	}

	if (Q_UNLIKELY(!iOk)) {
		return Type::ERR_ISDS;
	}

	valid = Xml::readAuthenticateMessageResponse(response, &iOk);
	if (Q_UNLIKELY(!iOk)) {
		return Type::ERR_XML;
	}

	return Type::ERR_SUCCESS;
}

enum Isds::Type::Error Isds::downloadAttachmentBase64(Session &session,
    qint64 dmId, qint64 attNum, DmFile &dmFile, DmStatus *status,
    QString *errMsg)
{
	QByteArray response;
	enum Connection::ErrCode errCode = Connection::communicate(session,
	    MESSAGE_SERVICE_VODZ,
	    Xml::soapRequestDownloadAttachmentBase64(dmId, attNum),
	    response);
	if (Q_UNLIKELY(errCode != Connection::ERR_NO_ERROR)) {
		if (errMsg != Q_NULLPTR) {
			*errMsg = session.ctx()->lastIsdsMsg();
		}
		return errCode2Error(errCode);
	}

	bool iOk = false;
	DmStatus dmStatus = Xml::toDmStatus(response, &iOk);
	if (Q_UNLIKELY(!iOk)) {
		return Type::ERR_XML;
	}
	if (errMsg != Q_NULLPTR) {
		*errMsg = dmStatus.message();
	}
	iOk = dmStatus.ok();
	if (status != Q_NULLPTR) {
		*status = macroStdMove(dmStatus);
	}

	if (Q_UNLIKELY(!iOk)) {
		return Type::ERR_ISDS;
	}

	dmFile = Xml::readDownloadAttachmentBase64Response(response, &iOk);
	if (Q_UNLIKELY(!iOk)) {
		return Type::ERR_XML;
	}

	return Type::ERR_SUCCESS;
}

enum Isds::Type::Error Isds::downloadAttachmentMtomXop(Session &session,
    qint64 dmId, qint64 attId, DmStatus *status, QString *errMsg)
{
	QByteArray response;
	enum Connection::ErrCode errCode = Connection::communicateMtomXop(
	    session, MESSAGE_SERVICE_VODZ,
	    Xml::soapRequestDownloadAttachmentMtomXop(dmId, attId),
	    response);
	if (Q_UNLIKELY(errCode != Connection::ERR_NO_ERROR)) {
		if (errMsg != Q_NULLPTR) {
			*errMsg = session.ctx()->lastIsdsMsg();
		}
		return errCode2Error(errCode);
	}

	bool iOk = false;
	DmStatus dmStatus = Xml::toDmStatus(response, &iOk);
	if (Q_UNLIKELY(!iOk)) {
		return Type::ERR_XML;
	}
	if (errMsg != Q_NULLPTR) {
		*errMsg = dmStatus.message();
	}
	iOk = dmStatus.ok();
	if (status != Q_NULLPTR) {
		*status = macroStdMove(dmStatus);
	}

	if (Q_UNLIKELY(!iOk)) {
		return Type::ERR_ISDS;
	}

	return Type::ERR_SUCCESS;
}

enum Isds::Type::Error Isds::uploadAttachmentBase64(Session &session,
    const DmFile &dmFile, DmAtt &dmAtt, DmStatus *status, QString *errMsg)
{
	QByteArray response;
	enum Connection::ErrCode errCode = Connection::communicate(session,
	    MESSAGE_SERVICE_VODZ,
	    Xml::soapRequestUploadAttachmentBase64(dmFile), response);
	if (Q_UNLIKELY(errCode != Connection::ERR_NO_ERROR)) {
		if (errMsg != Q_NULLPTR) {
			*errMsg = session.ctx()->lastIsdsMsg();
		}
		return errCode2Error(errCode);
	}

	bool iOk = false;
	DmStatus dmStatus = Xml::toDmStatus(response, &iOk);
	if (Q_UNLIKELY(!iOk)) {
		return Type::ERR_XML;
	}
	if (errMsg != Q_NULLPTR) {
		*errMsg = dmStatus.message();
	}
	iOk = dmStatus.ok();
	if (status != Q_NULLPTR) {
		*status = macroStdMove(dmStatus);
	}

	if (Q_UNLIKELY(!iOk)) {
		return Type::ERR_ISDS;
	}

	dmAtt = Xml::readUploadAttachmentResponse(response, &iOk);
	if (Q_UNLIKELY(!iOk)) {
		return Type::ERR_XML;
	}

	return Type::ERR_SUCCESS;
}

enum Isds::Type::Error Isds::uploadAttachmentMtomXop(Session &session,
    const QString &fileName, const QString &fileMimeType,
    const QString &filePath, DmAtt &dmAtt, DmStatus *status, QString *errMsg)
{
	QByteArray response;

	enum Connection::ErrCode errCode = Connection::uploadMtomXop(session,
	    MESSAGE_SERVICE_VODZ, QStringLiteral("UploadAttachment"),
	    Xml::soapRequestUploadAttachmentMtomXop(
	    fileMimeType, fileName), fileName, fileMimeType, filePath,
	    response);
	if (Q_UNLIKELY(errCode != Connection::ERR_NO_ERROR)) {
		if (errMsg != Q_NULLPTR) {
			*errMsg = session.ctx()->lastIsdsMsg();
		}
		return errCode2Error(errCode);
	}

	bool iOk = false;
	DmStatus dmStatus = Xml::toDmStatus(response, &iOk);
	if (Q_UNLIKELY(!iOk)) {
		return Type::ERR_XML;
	}
	if (errMsg != Q_NULLPTR) {
		*errMsg = dmStatus.message();
	}
	iOk = dmStatus.ok();
	if (status != Q_NULLPTR) {
		*status = macroStdMove(dmStatus);
	}

	if (Q_UNLIKELY(!iOk)) {
		return Type::ERR_ISDS;
	}

	dmAtt = Xml::readUploadAttachmentResponse(response, &iOk);
	if (Q_UNLIKELY(!iOk)) {
		return Type::ERR_XML;
	}

	return Type::ERR_SUCCESS;
}

enum Isds::Type::Error Isds::createBigMessage(Session &session,
    Message &message, DmStatus *status, QString *errMsg)
{
	QByteArray response;
	enum Connection::ErrCode errCode = Connection::communicate(session,
	    MESSAGE_SERVICE_VODZ,
	    Xml::soapRequestCreateBigMessage(message),
	    response);
	if (Q_UNLIKELY(errCode != Connection::ERR_NO_ERROR)) {
		if (errMsg != Q_NULLPTR) {
			*errMsg = session.ctx()->lastIsdsMsg();
		}
		return errCode2Error(errCode);
	}

	bool iOk = false;
	DmStatus dmStatus = Xml::toDmStatus(response, &iOk);
	if (Q_UNLIKELY(!iOk)) {
		return Type::ERR_XML;
	}
	if (errMsg != Q_NULLPTR) {
		*errMsg = dmStatus.message();
	}
	iOk = dmStatus.ok();
	if (status != Q_NULLPTR) {
		*status = macroStdMove(dmStatus);
	}

	if (Q_UNLIKELY(!iOk)) {
		return Type::ERR_ISDS;
	}

	qint64 dmId = Xml::readCreateMessageResponse(response);
	if (Q_UNLIKELY(dmId < 0)) {
		return Type::ERR_XML;
	}

	{
		Envelope env = message.envelope();
		env.setDmId(dmId);
		message.setEnvelope(macroStdMove(env));
	}

	return Type::ERR_SUCCESS;
}
