/*
 * Copyright (C) 2014-2025 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

#include <QFileInfo>
#include <QRegularExpression>
#include <QSslCertificate>

#include "src/datovka_shared/identifiers/account_id.h"
#include "src/datovka_shared/isds/type_conversion.h"
#include "src/datovka_shared/log/log.h"
#include "src/global.h"
#include "src/io/filesystem.h"
#include "src/isds/conversion/isds_type_conversion.h"
#include "src/isds/isds_login.h"
#include "src/isds/isds_tasks.h"
#include "src/isds/isds_wrapper.h"
#include "src/isds/session/isds_session.h"
#include "src/isds/session/isds_sessions.h"
#include "src/models/databoxmodel.h"
#include "src/qml_identifiers/qml_account_id.h"
#include "src/settings/accounts.h"
#include "src/worker/emitter.h"
#include "src/worker/task_recipient_info.h"
#include "src/wrap_accounts.h"

IsdsWrapper::IsdsWrapper(QObject *parent)
    : QObject(parent),
    m_mepRunning(false),
    m_transactIds(),
    m_downloadSentMsg(false),
    m_pdzInfos(),
    m_pdzCredit(),
    m_databoxModel()
{
	if (GlobInstcs::msgProcEmitterPtr == Q_NULLPTR) {
		logErrorNL("%s", "Cannot connect to status message emitter.");
		return;
	}

	qRegisterMetaType<QList<Isds::PDZInfoRec>>("QList<Isds::PDZInfoRec>");
	qRegisterMetaType<DataboxModelEntry>("DataboxModelEntry");

	/* Worker-related processing signals. */
	connect(GlobInstcs::msgProcEmitterPtr,
	    SIGNAL(downloadAccountInfoFinishedSignal(AcntId, bool, QString)),
	    this,
	    SLOT(downloadAccountInfoFinished(AcntId, bool, QString)));

	connect(GlobInstcs::msgProcEmitterPtr,
	    SIGNAL(downloadMessageFinishedSignal(AcntId, qint64, bool, QString)),
	    this,
	    SLOT(downloadMessageFinished(AcntId, qint64, bool, QString)));

	connect(GlobInstcs::msgProcEmitterPtr,
	    SIGNAL(downloadMessageListFinishedSignal(AcntId, bool, QString, bool)),
	    this,
	    SLOT(downloadMessageListFinished(AcntId, bool, QString, bool)));

	connect(GlobInstcs::msgProcEmitterPtr,
	    SIGNAL(importZFOFinishedSignal(int, int, QString)),
	    this, SLOT(importZFOFinished(int, int, QString)));

	connect(GlobInstcs::msgProcEmitterPtr,
	    SIGNAL(sendMessageFinishedSignal(AcntId, QString, QString, QString,
	    qint64, bool, bool, QString)),
	    this,
	    SLOT(sendMessageFinished(AcntId, QString, QString, QString,
	    qint64, bool, bool, QString)));

	connect(GlobInstcs::msgProcEmitterPtr,
	    SIGNAL(pdzInfoFinishedSignal(AcntId, qint64,
	    QList<Isds::PDZInfoRec>, bool, QString)),
	    this,
	    SLOT(pdzInfoFinished(AcntId, qint64,
	    QList<Isds::PDZInfoRec>, bool, QString)));

	connect(GlobInstcs::msgProcEmitterPtr,
	    SIGNAL(recipientInfoFinishedSignal(DataboxModelEntry, int)),
	    this, SLOT(addRecipientIntoModel(DataboxModelEntry, int)));
}

void IsdsWrapper::closeAllOtpConnections(void)
{
	if (Q_UNLIKELY(GlobInstcs::isdsSessionsPtr == Q_NULLPTR)) {
		Q_ASSERT(0);
		return;
	}

	Isds::Login::closeAllOtpConnections(*GlobInstcs::isdsSessionsPtr);
}

bool IsdsWrapper::changePassword(const QmlAcntId *qAcntId, const QString &oldPwd,
    const QString &newPwd, const QString &otpCode)
{
	if (Q_UNLIKELY(qAcntId == Q_NULLPTR)) {
		Q_ASSERT(0);
		return false;
	}

	QString errTxt;
	return Isds::Tasks::changePassword(*qAcntId, oldPwd, newPwd, otpCode, errTxt);
}

void IsdsWrapper::doIsdsAction(const QString &isdsAction,
    const QmlAcntId *qAcntId)
{
	if (Q_UNLIKELY(qAcntId == Q_NULLPTR)) {
		Q_ASSERT(0);
		return;
	}
	doIsdsAction(isdsAction, *qAcntId);
}

void IsdsWrapper::downloadMessage(const QmlAcntId *qAcntId,
    enum Messages::MessageType messageType, qint64 msgId)
{
	if (Q_UNLIKELY(qAcntId == Q_NULLPTR)) {
		Q_ASSERT(0);
		return;
	}

	Isds::Tasks::downloadMessage(*qAcntId, messageType, msgId);
}

QString IsdsWrapper::findDatabox(const QmlAcntId *qAcntId, const QString &dbID,
    const QString &dbType)
{
	if (Q_UNLIKELY(qAcntId == Q_NULLPTR)) {
		Q_ASSERT(0);
		return QString();
	}

	return Isds::Tasks::findDatabox(*qAcntId, dbID, Isds::str2DbType(dbType));
}

void IsdsWrapper::findDataboxFulltext(const QmlAcntId *qAcntId,
    DataboxListModel *databoxModel, const QString &phrase,
    const QString &searchType, const QString &searchScope, int page)
{
	if (Q_UNLIKELY(databoxModel == Q_NULLPTR)) {
		emit sendSearchResultsSig(0, 0, 0, 0);
		Q_ASSERT(0);
		return;
	}

	/* Clear the model. */
	databoxModel->clearAll();

	if (Q_UNLIKELY((GlobInstcs::workPoolPtr == Q_NULLPTR) ||
	        (qAcntId == Q_NULLPTR) || phrase.isEmpty())) {
		emit sendSearchResultsSig(0, 0, 0, 0);
		Q_ASSERT(0);
		return;
	}

	QCoreApplication::processEvents();

	long int totalCount = 0;
	long int currentCount = 0;
	long int position = 0;
	bool lastPage = false;
	QList<Isds::DbResult2> dbList;
	QString lastError;

	/* Find databox. */
	bool success = Isds::Tasks::findDataboxFulltext(*qAcntId, phrase,
	    Isds::str2FulltextSearchType(searchType),
	    Isds::str2FulltextDbType(searchScope), page, dbList, totalCount,
	    currentCount, position, lastPage, lastError);
	if (Q_UNLIKELY(!success)) {
		emit sendSearchResultsSig(0, 0, 0, 0);
		emit showMessageBox(tr("Find data box: %1").arg(qAcntId->username()),
		    tr("Failed finding data box to phrase '%1'.").arg(phrase),
		    lastError);
		return;
	}

	/* Fill the model. */
	for (const Isds::DbResult2 &db : dbList) {
		databoxModel->addEntry(DataboxModelEntry(db.dbID(),
		    Isds::dbType2Str(db.dbType()), db.dbName(),
		    db.dbAddress(), db.ic(),
		    Isds::dbSendOptions2str(db.active(),
		    db.publicSending(), db.commercialSending()),
		    Isds::Type::BOOL_NULL, Isds::Type::MT_UNKNOWN,
		    QList<enum Isds::Type::DmType>()));
		QCoreApplication::processEvents();
	}

	emit sendSearchResultsSig(totalCount, currentCount,
	    position, lastPage);
}

QString IsdsWrapper::getAccountDbId(const QmlAcntId *qAcntId)
{
	if (Q_UNLIKELY(qAcntId == Q_NULLPTR)) {
		Q_ASSERT(0);
		return QString();
	}

	return Isds::Tasks::getAccountDbId(*qAcntId);
}

void IsdsWrapper::getAccountInfo(const QmlAcntId *qAcntId)
{
	if (Q_UNLIKELY(qAcntId == Q_NULLPTR)) {
		Q_ASSERT(0);
		return;
	}

	getAccountInfo(*qAcntId);
}

enum AcntData::LoginMethod IsdsWrapper::getAccountLoginMethod(
    const QmlAcntId *qAcntId)
{
	if (Q_UNLIKELY((GlobInstcs::acntMapPtr == Q_NULLPTR) ||
	        (qAcntId == Q_NULLPTR))) {
		Q_ASSERT(0);
		return AcntData::LIM_UNKNOWN;
	}

	const AcntData aData = GlobInstcs::acntMapPtr->acntData(*qAcntId);
	return aData.loginMethod();
}

QString IsdsWrapper::importZfoMessages(const QmlAcntId *qAcntId,
    const QStringList &fileList, bool authenticate)
{
	if (fileList.isEmpty()) {
		return tr("No zfo files or selected folder.");
	}

	/* Get user names of all accounts if an user name is not specified */
	QList<AcntId> acntIdList;
	if ((Q_NULLPTR == qAcntId) || (!qAcntId->isValid())) {
		acntIdList = GlobInstcs::acntMapPtr->keys();
	} else {
		acntIdList.append(*qAcntId);
	}

	if (acntIdList.isEmpty()) {
		return tr("No data box for zfo import.");
	}

	/* Test, if folder or file(s) was selected */
	QStringList filePathList = fileList;
	if (1 == fileList.count()) {
		if (!QFileInfo(fileList.at(0)).isFile()) {
			filePathList = zfoFilesFromDir(fileList.at(0));
			if (filePathList.isEmpty()) {
				return tr("No zfo files in the selected directory.");
			}
		}
	}

	if (filePathList.count() < 1) {
		return tr("No zfo file for import.");
	}

	/* User must be logged to ISDS if messages must be verified */
	if (authenticate) {
		foreach (const AcntId &acntId, acntIdList) {
			/* Check if an account is logged */
			if (!Isds::Login::isLoggedIn(
			        *GlobInstcs::isdsSessionsPtr, acntId)) {
				acntIdList.removeOne(acntId);
			}
		}
		if (acntIdList.isEmpty()) {
			return tr("ZFO messages cannot be imported because there is no active data box for their verification.");
		}
	}

	return Isds::Tasks::importZfoMessages(acntIdList, filePathList,
	    authenticate);
}

void IsdsWrapper::inputDialogCancel(const QString &isdsAction,
    const QmlAcntId *qAcntId)
{
	if (Q_UNLIKELY(qAcntId == Q_NULLPTR)) {
		Q_ASSERT(0);
		return;
	}

	if (isdsAction == "addNewAccount" || isdsAction == "changeUserName") {
		emit unsuccessfulLoginToIsdsSig(qAcntId->username(),
		    qAcntId->testing());
	}
	emit loginFailed(qAcntId->username(), qAcntId->testing());
}

void IsdsWrapper::inputDialogReturnPassword(const QString &isdsAction,
     enum AcntData::LoginMethod loginMethod, const QmlAcntId *qAcntId,
     const QString &pwd)
{
	debugSlotCall();

	if (Q_UNLIKELY((GlobInstcs::acntMapPtr == Q_NULLPTR) ||
	        (GlobInstcs::isdsSessionsPtr == Q_NULLPTR) ||
	        (qAcntId == Q_NULLPTR))) {
		Q_ASSERT(0);
		return;
	}

	if (!GlobInstcs::isdsSessionsPtr->holdsSession(*qAcntId)) {
		return;
	}

	QString errTxt;
	switch (loginMethod) {
	case AcntData::LIM_UNAME_PWD:
		GlobInstcs::isdsSessionsPtr->session(*qAcntId)->
		    ctx()->setPassword(pwd);
		doLoginAction(isdsAction, *qAcntId);
		break;
	case AcntData::LIM_UNAME_MEP:
		GlobInstcs::isdsSessionsPtr->session(*qAcntId)->
		    ctx()->setPassword(pwd);
		doLoginAction(isdsAction, *qAcntId);
		break;
	case AcntData::LIM_UNAME_PWD_HOTP:
		if (!Isds::Login::loginHotp(*GlobInstcs::isdsSessionsPtr,
		        *qAcntId, pwd, errTxt)) {
			doLoginFailAction(isdsAction, *qAcntId, errTxt);
			return;
		}
		getAccountInfo(*qAcntId);
		doIsdsAction(isdsAction, *qAcntId);
		break;
	case AcntData::LIM_UNAME_PWD_TOTP:
		if (!Isds::Login::loginTotp(*GlobInstcs::isdsSessionsPtr,
		        *qAcntId, pwd, errTxt)) {
			doLoginFailAction(isdsAction, *qAcntId, errTxt);
			return;
		}
		getAccountInfo(*qAcntId);
		doIsdsAction(isdsAction, *qAcntId);
		break;
	case AcntData::LIM_UNAME_PWD_CRT:
		if (GlobInstcs::isdsSessionsPtr->session(*qAcntId)->setCertificate(
		        GlobInstcs::acntMapPtr->acntData(*qAcntId).p12File(),
		        pwd, errTxt)) {
			doLoginAction(isdsAction, *qAcntId);
		} else {
			showLoginErrorDlg(isdsAction, *qAcntId, errTxt);
			GlobInstcs::isdsSessionsPtr->removeSession(*qAcntId);
		}
		break;
	default:
		Q_ASSERT(0);
		break;
	}
}

void IsdsWrapper::yesNoDialogResult(const QString &isdsAction,
     enum AcntData::LoginMethod loginMethod, const QmlAcntId *qAcntId,
     bool accepted)
{
	debugSlotCall();

	if (Q_UNLIKELY((GlobInstcs::acntMapPtr == Q_NULLPTR) ||
	        (GlobInstcs::isdsSessionsPtr == Q_NULLPTR) ||
	        (qAcntId == Q_NULLPTR))) {
		Q_ASSERT(0);
		return;
	}

	if (!GlobInstcs::isdsSessionsPtr->holdsSession(*qAcntId)) {
		emit loginFailed(qAcntId->username(), qAcntId->testing());
		return;
	}

	QString errTxt;

	switch (loginMethod) {
	case AcntData::LIM_UNAME_PWD_TOTP:

		if (Q_UNLIKELY(!accepted)) {
			errTxt = tr("SMS code sending request has been cancelled.");
			doLoginFailAction(isdsAction, *qAcntId, errTxt);
			return;
		}

		/* Send basic authorization (username, password:otp or MEP code). */
		if (!Isds::Login::loginBasicAuth(*GlobInstcs::isdsSessionsPtr, *qAcntId,
		        errTxt)) {
			doLoginFailAction(isdsAction, *qAcntId, errTxt);
			return;
		}
		/* Basic login and SMS request succeeded. Wait for SMS login. */
		waitForSmsCode(*qAcntId, isdsAction);
		return;
		break;
	default:
		Q_ASSERT(0);
		break;
	}
}

bool IsdsWrapper::isCorrectPassword(const QString &password)
{
	if (password.isEmpty()) {
		return false;
	}
	if (password.length() < 8 || password.length() > 32) {
		return false;
	}
	if (!password.contains(QRegularExpression(".*[a-z]+.*"))) {
		return false;
	}
	if (!password.contains(QRegularExpression(".*[A-Z]+.*"))) {
		return false;
	}
	if (!password.contains(QRegularExpression(".*[0-9]+.*"))) {
		return false;
	}

	return true;
}

bool IsdsWrapper::isLogged(const QmlAcntId *qAcntId)
{
	if (Q_UNLIKELY((GlobInstcs::isdsSessionsPtr == Q_NULLPTR) ||
	        (qAcntId == Q_NULLPTR))) {
		Q_ASSERT(0);
		return false;
	}

	return Isds::Login::isLoggedIn(*GlobInstcs::isdsSessionsPtr, *qAcntId);
}

void IsdsWrapper::loginMepCancel(void)
{
	m_mepRunning = false;
}

bool IsdsWrapper::loginMepRunning(void)
{
	return m_mepRunning;
}

bool IsdsWrapper::sendGovRequest(const AcntId &acntId,
    const Isds::Message &msg)
{
	m_sentMsgResultList.clear();
	return Isds::Tasks::sendGovRequest(acntId, msg, m_transactIds);
}

bool IsdsWrapper::sendMessage(const QmlAcntId *qAcntId, qint64 dmID,
    const QString &dmAnnotation, const DataboxListModel *databoxModel,
    const FileListModel *attachModel,
    const QString &dmLegalTitleLaw, const QString &dmLegalTitleYear,
    const QString &dmLegalTitleSect, const QString &dmLegalTitlePar,
    const QString &dmLegalTitlePoint, const QString &dmToHands,
    const QString &dmRecipientRefNumber, const QString &dmRecipientIdent,
    const QString &dmSenderRefNumber, const QString &dmSenderIdent,
    bool dmOVM, bool dmPublishOwnID, bool dmAllowSubstDelivery,
    bool dmPersonalDelivery, int idLevel, bool downloadSentMsg, bool vodz)
{
	if (Q_UNLIKELY(qAcntId == Q_NULLPTR)) {
		Q_ASSERT(0);
		return false;
	}

	if (Q_UNLIKELY(databoxModel == Q_NULLPTR)) {
		logErrorNL("%s", "Cannot access data box model.");
		Q_ASSERT(0);
		return false;
	}

	m_sentMsgResultList.clear();
	m_downloadSentMsg = downloadSentMsg;

	/* Check mandatory fields for initiatory and reply commercial messages. */
	foreach (const DataboxModelEntry &db, databoxModel->allEntries()) {
		if (db.dmType() == Isds::Type::MT_I && dmSenderRefNumber.isEmpty()) {
			/* Message is initiatory PDZ, dmSenderRefNumber must be filled */
			emit showMessageBox(tr("Error sending message"),
			    tr("Pre-paid transfer charges for message reply have been enabled."),
			    tr("The sender reference number must be specified in the additional section in order to send the message."));
			return false;
		} else if (db.dmType() == Isds::Type::MT_O && dmRecipientRefNumber.isEmpty()) {
			/* Message is PDZ reply on initiatory message, dmRecipientRefNumber must be filled */
			emit showMessageBox(tr("Error sending message"),
			    tr("Message uses the offered payment of transfer charges by the recipient."),
			    tr("The recipient reference number must be specified in the additional section in order to send the message."));
			return false;
		}
	}

	return Isds::Tasks::sendMessage(*qAcntId, dmID, dmAnnotation, databoxModel,
	attachModel, dmLegalTitleLaw, dmLegalTitleYear, dmLegalTitleSect,
	dmLegalTitlePar, dmLegalTitlePoint, dmToHands, dmRecipientRefNumber,
	dmRecipientIdent, dmSenderRefNumber, dmSenderIdent, dmOVM,
	dmPublishOwnID, dmAllowSubstDelivery, dmPersonalDelivery, idLevel,
	vodz, m_transactIds);
}

bool IsdsWrapper::sendSMS(const QmlAcntId *qAcntId, const QString &oldPwd)
{
	if (Q_UNLIKELY(qAcntId == Q_NULLPTR)) {
		Q_ASSERT(0);
		return false;
	}

	return Isds::Tasks::sendSMS(*qAcntId, oldPwd);
}

void IsdsWrapper::syncAllAccounts(void)
{
	debugSlotCall();

	if (Q_UNLIKELY(GlobInstcs::acntMapPtr == Q_NULLPTR)) {
		Q_ASSERT(0);
		return;
	}

	for (const AcntId &acntId : GlobInstcs::acntMapPtr->keys()) {

		const AcntData aData = GlobInstcs::acntMapPtr->acntData(acntId);
		if (!aData.syncWithAll()) {
			continue;
		}

		doIsdsAction("syncOneAccount", acntId);
	}
}

void IsdsWrapper::syncOneAccount(const QmlAcntId *qAcntId)
{
	if (Q_UNLIKELY(qAcntId == Q_NULLPTR)) {
		Q_ASSERT(0);
		return;
	}

	syncSingleAccountSent(qAcntId);
	syncSingleAccountReceived(qAcntId);
}

void IsdsWrapper::syncSingleAccountReceived(const QmlAcntId *qAcntId)
{
	if (Q_UNLIKELY(qAcntId == Q_NULLPTR)) {
		Q_ASSERT(0);
		return;
	}

	Isds::Tasks::syncSingleAccount(*qAcntId, Messages::TYPE_RECEIVED);
}

void IsdsWrapper::syncSingleAccountSent(const QmlAcntId *qAcntId)
{
	if (Q_UNLIKELY(qAcntId == Q_NULLPTR)) {
		Q_ASSERT(0);
		return;
	}

	Isds::Tasks::syncSingleAccount(*qAcntId, Messages::TYPE_SENT);
}

void IsdsWrapper::getPdzInfo(const QmlAcntId *qAcntId, const QString &dbID)
{
	if (Q_UNLIKELY(qAcntId == Q_NULLPTR) || dbID.isEmpty()) {
		Q_ASSERT(0);
		return;
	}

	Isds::Tasks::downloadPdzInfo(*qAcntId, dbID);
}

void IsdsWrapper::addRecipient(const QmlAcntId *qAcntId, const QString &dbID,
    const QString &dbName, const QString &dbAddress, bool canUseInitReply,
    DataboxListModel *databoxModel)
{
	if (Q_UNLIKELY((qAcntId == Q_NULLPTR) || (dbID.isEmpty())) ||
	        (databoxModel == Q_NULLPTR)) {
		Q_ASSERT(0);
		return;
	}

	/* Add temporary recipient data into model. */
	DataboxModelEntry dbEntry;
	dbEntry.setDbID(dbID);
	dbEntry.setDbName(dbName);
	dbEntry.setDbAddress(dbAddress);
	dbEntry.setPdz(Isds::Type::BOOL_NULL);
	dbEntry.setDmType(Isds::Type::MT_UNKNOWN);
	databoxModel->addEntry(dbEntry);

	/* Remember model for callback from task. */
	m_databoxModel = databoxModel;

	bool senderOvm = Accounts::isOvm(qAcntId);

	Isds::Tasks::recipientInfo(*qAcntId, dbID, senderOvm,
	    canUseInitReply, m_pdzInfos);
}

void IsdsWrapper::addRecipientIntoModel(DataboxModelEntry dbEntry, int errCode)
{
	if (Q_UNLIKELY((m_databoxModel == Q_NULLPTR)
	        || (dbEntry.dbID().isEmpty()))) {
		Q_ASSERT(0);
		return;
	}

	switch (errCode) {
	case TaskRecipientInfo::TR_SUCCESS:
		break;
	case TaskRecipientInfo::TR_DB_UNKN_OVM_UNKN_PDZ:
		emit showMessageBox(tr("Unknown Message Type"),
		    tr("No information about the recipient data box '%1' could be obtained. It is unknown whether public or commercial messages can be sent to this recipient.").arg(dbEntry.dbID()),
		    QString());
		break;
	case TaskRecipientInfo::TR_DB_UNKN_PUBLIC_NO_PDZ:
		emit showMessageBox(tr("Unknown Message Type"),
		    tr("No commercial message to the recipient data box '%1' can be sent. It is unknown whether a public messages can be sent to this recipient.").arg(dbEntry.dbID()),
		    QString());
		break;
	case TaskRecipientInfo::TR_DB_NO_PUBLIC_UNKN_PDZ:
		emit showMessageBox(tr("Unknown Message Type"),
		    tr("No public message to the recipient data box '%1' can be sent. It is unknown whether a commercial messages can be sent to this recipient.").arg(dbEntry.dbID()),
		    QString());
		break;
	case TaskRecipientInfo::TR_DB_NO_PUBLIC_NO_PDZ:
		emit showMessageBox(tr("Cannot send message"),
		    tr("No public data message nor a commercial data message (PDZ) can be sent to the recipient data box '%1'.").arg(dbEntry.dbID()),
		    tr("Receiving of PDZs has been disabled in the recipient data box or there are no available payment methods for PDZs."));
		m_databoxModel->removeEntry(dbEntry.dbID());
		return;
		break;
	case TaskRecipientInfo::TR_DB_NO_ACTIVE:
		emit showMessageBox(tr("Data box is not active"),
		    tr("Recipient data-box ID '%1' isn't active.").arg(dbEntry.dbID()),
		    tr("The message cannot be delivered."));
		m_databoxModel->removeEntry(dbEntry.dbID());
		return;
		break;
	case TaskRecipientInfo::TR_DB_NOT_EXISTS:
		emit showMessageBox(tr("Wrong Recipient"),
		    tr("Recipient with data-box ID '%1' doesn't exist.").arg(dbEntry.dbID()),
		    tr("The message cannot be delivered."));
		m_databoxModel->removeEntry(dbEntry.dbID());
		return;
		break;
	case TaskRecipientInfo::TR_DB_SEARCH_ERROR:
		emit showMessageBox(tr("Recipient Search Failed"),
			tr("Information about recipient data box '%1' couldn't be obtained.").arg(dbEntry.dbID()),
		    tr("The message may not be delivered."));
		return;
		break;
	default:
		m_databoxModel->removeEntry(dbEntry.dbID());
		return;
		break;
	}

	DataboxModelEntry modelEntry(*m_databoxModel->entry(dbEntry.dbID()));
	if (dbEntry.dbName().isEmpty()) {
		dbEntry.setDbName(modelEntry.dbName());
	}
	if (dbEntry.dbAddress().isEmpty()) {
		dbEntry.setDbAddress(modelEntry.dbAddress());
	}

	/* Remove temporary recipient data from model. */
	m_databoxModel->removeEntry(dbEntry.dbID());
	/* Add updated recipient data into model. */
	m_databoxModel->addEntry(dbEntry);
}

void IsdsWrapper::uploadAttachment(const QmlAcntId *qAcntId,
    const QString &filePath, const QString &fileName)
{
	if (Q_UNLIKELY((qAcntId == Q_NULLPTR) || (filePath.isEmpty() ||
	        (fileName.isEmpty())))) {
		Q_ASSERT(0);
		return;
	}

	Isds::DmAtt dmAtt;

	/* True = use MTOM/XOP. May be loaded from preferences in the future. */
	Isds::Tasks::uploadAttachment(*qAcntId, filePath, fileName, true,
	    dmAtt);
}

void IsdsWrapper::downloadAttachment(const QmlAcntId *qAcntId,
    qint64 dmId, qint64 attId)
{
	if (Q_UNLIKELY((qAcntId == Q_NULLPTR))) {
		Q_ASSERT(0);
		return;
	}

	/* True = use MTOM/XOP. May be loaded from preferences in the future. */
	Isds::Tasks::downloadAttachment(*qAcntId, dmId, attId, true);
}

void IsdsWrapper::downloadBigMessage(const QmlAcntId *qAcntId,
    enum Messages::MessageType messageType, qint64 dmId)
{
	if (Q_UNLIKELY((qAcntId == Q_NULLPTR))) {
		Q_ASSERT(0);
		return;
	}

	/* True = use MTOM/XOP. May be loaded from preferences in the future. */
	Isds::Tasks::downloadBigMessage(*qAcntId, messageType, dmId, false);
}

void IsdsWrapper::removeIsdsCtx(const AcntId &acntId)
{
	if (Q_UNLIKELY(GlobInstcs::isdsSessionsPtr == Q_NULLPTR)) {
		Q_ASSERT(0);
		return;
	}
	GlobInstcs::isdsSessionsPtr->removeSession(acntId);
}

void IsdsWrapper::doLoginAction(const QString &isdsAction,
    const AcntId &acntId)
{
	debugFuncCall();

	if (Q_UNLIKELY((GlobInstcs::acntMapPtr == Q_NULLPTR) ||
	        (GlobInstcs::isdsSessionsPtr == Q_NULLPTR))) {
		Q_ASSERT(0);
		return;
	}

	/* Check if account has all required data for login. */
	if (!hasAllLoginData(acntId, isdsAction)) {
		return;
	}

	const AcntData aData = GlobInstcs::acntMapPtr->acntData(acntId);

	/* Is TOTP login, show confirm dialogue for SMS code sending. */
	if (aData.loginMethod() == AcntData::LIM_UNAME_PWD_TOTP) {
		emit openYesNoDialogue(isdsAction, AcntData::LIM_UNAME_PWD_TOTP,
		    acntId.username(), acntId.testing(),
		    tr("SMS code: %1").arg(acntId.username()),
		    tr("Account '%1' requires authentication with an SMS code.").
		        arg(aData.accountName()),
		    tr("Do you want to request the SMS code now?"));
		return;
	}

	/* HOTP login do separately. */
	if (aData.loginMethod() == AcntData::LIM_UNAME_PWD_HOTP) {
		waitForHotpCode(acntId, isdsAction);
		return;
	}

	/* Send basic authorization (username, password:otp or MEP code). */
	QString errTxt;
	if (!Isds::Login::loginBasicAuth(*GlobInstcs::isdsSessionsPtr, acntId,
	        errTxt)) {
		doLoginFailAction(isdsAction, acntId, errTxt);
		return;
	}

	if (aData.loginMethod() == AcntData::LIM_UNAME_MEP) {
		/* Basic login succeeded. Run MEP login. */
		emit showMepWaitDialog(acntId.username(), true);
		if (!Isds::Login::loginMep(*GlobInstcs::isdsSessionsPtr, acntId,
		        m_mepRunning, errTxt)) {
			emit showMepWaitDialog(acntId.username(), false);
			QCoreApplication::processEvents(QEventLoop::AllEvents, 100);
			doLoginFailAction(isdsAction, acntId, errTxt);
			return;
		}
		emit showMepWaitDialog(acntId.username(), false);
	}

	/* Login succeeded, update account info, password expiration. */
	getAccountInfo(acntId);
	doIsdsAction(isdsAction, acntId);
}

void IsdsWrapper::doLoginFailAction(const QString &isdsAction,
     const AcntId &acntId, QString errTxt)
{
	if (errTxt.isEmpty()) {
		errTxt = tr("Internal error.");
	}

	/* Remove the fail session from container */
	GlobInstcs::isdsSessionsPtr->removeSession(acntId);

	/* Show error dialogue. */
	emit loginFailed(acntId.username(), acntId.testing());
	showLoginErrorDlg(isdsAction, acntId, errTxt);

	/* Post QML actions */
	if (isdsAction == "addNewAccount" || isdsAction == "changeUserName") {
		emit unsuccessfulLoginToIsdsSig(acntId.username(), acntId.testing());
	}
	if (isdsAction == "sendMessage") {
		emit sentMessageFinished(acntId.username(), acntId.testing(),
		    false, errTxt);
	}
}

void IsdsWrapper::doIsdsAction(const QString &isdsAction, const AcntId &acntId)
{
	debugFuncCall();

	QCoreApplication::processEvents();

	/* If user is logged to ISDS then run ISDS action else run login phase */
	if (Isds::Login::isLoggedIn(*GlobInstcs::isdsSessionsPtr, acntId)) {
		if (isdsAction == "downloadMessage") {
			emit runDownloadMessageSig(acntId.username(),
			    acntId.testing());
		} else if (isdsAction == "addNewAccount") {
			emit runGetAccountInfoSig(acntId.username(),
			    acntId.testing());
		} else if (isdsAction == "changePassword") {
			emit runChangePasswordSig(acntId.username(),
			    acntId.testing());
		} else if (isdsAction == "changeUserName") {
			emit runChangeUserNameSig(acntId.username(),
			    acntId.testing());
		} else if (isdsAction == "getAccountInfo") {
			getAccountInfo(acntId);
		} else if (isdsAction == "syncOneAccount") {
			emit runSyncOneAccountSig(acntId.username(),
			    acntId.testing());
		} else if (isdsAction == "syncSingleAccountReceived") {
			emit runSyncSingleAccountReceivedSig(acntId.username(),
			    acntId.testing());
		} else if (isdsAction == "syncSingleAccountSent") {
			emit runSyncSingleAccountSentSig(acntId.username(),
			    acntId.testing());
		} else if (isdsAction == "findDataboxFulltext") {
			emit runFindDataboxFulltextSig(acntId.username(),
			    acntId.testing());
		} else if (isdsAction == "findDatabox") {
			emit runFindDataboxSig(acntId.username(),
			    acntId.testing());
		} else if (isdsAction == "sendMessage") {
			emit runSendMessageSig(acntId.username(),
			    acntId.testing());
		} else if (isdsAction == "sendGovMessage") {
			emit runSendGovMessageSig(acntId.username(),
			    acntId.testing());
		} else if (isdsAction == "importZfoMessage") {
			emit runImportZfoMessageSig(acntId.username(),
			    acntId.testing());
		} else if (isdsAction == "initSendMsgDlg") {
			emit initSendMsgDlgSig(acntId.username(), acntId.testing());
		}
	} else {
		doLoginAction(isdsAction, acntId);
	}
}

void IsdsWrapper::getAccountInfo(const AcntId &acntId)
{
	Isds::Tasks::getAccountInfo(acntId);
}

bool IsdsWrapper::hasAllLoginData(const AcntId &acntId,
    const QString &isdsAction)
{
	debugFuncCall();

	if (Q_UNLIKELY((GlobInstcs::acntMapPtr == Q_NULLPTR) ||
		(GlobInstcs::isdsSessionsPtr == Q_NULLPTR))) {
		Q_ASSERT(0);
		return false;
	}

	const AcntData aData = GlobInstcs::acntMapPtr->acntData(acntId);

	/* Create session if not exists. */
	if (!GlobInstcs::isdsSessionsPtr->holdsSession(acntId)) {
		Isds::Session *session = Isds::Session::createSession(aData);
		if (session == Q_NULLPTR) {
			logErrorNL("Cannot create session for username %s",
			    acntId.username().toUtf8().constData());
			return false;
		}

		/* Set login URL. */
		session->setLoginUrl();

		if (!GlobInstcs::isdsSessionsPtr->insertSession(acntId, session)) {
			logErrorNL("Cannot insert session (%s) into container.",
			    acntId.username().toUtf8().constData());
			delete session;
			return false;
		}
	}

	/* Has MEP account communication code in the ISDS context. */
	if (aData.loginMethod() == AcntData::LIM_UNAME_MEP  &&
	        GlobInstcs::isdsSessionsPtr->session(acntId)->
	        ctx()->password().isEmpty()) {
		emit openDialogRequest(isdsAction, AcntData::LIM_UNAME_MEP,
		    aData.userName(), aData.isTestAccount(),
		    tr("Enter communication code: %1").arg(aData.userName()),
		    tr("Communication code for data box '%1' is required.").
		        arg(aData.accountName()),
		    tr("Enter communication code"),
		    false);
		return false;
	}

	/* Test if other accounts have set password in the ISDS context. */
	if (GlobInstcs::isdsSessionsPtr->holdsSession(acntId) &&
	        GlobInstcs::isdsSessionsPtr->session(acntId)->
	        ctx()->password().isEmpty()) {
		emit openDialogRequest(isdsAction, AcntData::LIM_UNAME_PWD,
		    aData.userName(), aData.isTestAccount(),
		    tr("Enter password: %1").arg(aData.userName()),
		    tr("Password for data box '%1' missing.").
		        arg(aData.accountName()),
		    tr("Enter password"),
		    true);
		return false;
	}

	/* Certificate login, set private key password. */
	if (aData.loginMethod() == AcntData::LIM_UNAME_PWD_CRT  &&
	        GlobInstcs::isdsSessionsPtr->session(acntId)->
	        ctx()->sslCertificate().isNull()) {
		emit openDialogRequest(isdsAction, AcntData::LIM_UNAME_PWD_CRT,
		    acntId.username(), acntId.testing(),
		    tr("Enter certificate password: %1").arg(acntId.username()),
		    tr("Certificate password for data box '%1' is required.").
		        arg(aData.accountName()),
		    tr("Enter certificate password"),
		    true);
		return false;
	}

	return true;
}

void IsdsWrapper::showLoginErrorDlg(const QString &isdsAction,
    const AcntId &acntId, const QString &errTxt)
{
	QString title, text, detailText;

	if (isdsAction == "addNewAccount") {
		title = tr("New data box problem: %1").arg(acntId.username()),
		text = tr("New data box could not be created for username '%1'.").arg(acntId.username()),
		detailText = errTxt + "\n\n" + tr("Note: You must change your password via the ISDS web portal if this is your first login to the data box. You can start using mobile Datovka to access the data box after changing.");
	} else if (isdsAction == "changeUserName") {
		title = tr("Username change: %1").arg(acntId.username()),
		text = tr("Username could not be changed on the new username '%1'.").arg(acntId.username()),
		detailText = errTxt;
	} else {
		title = tr("Login problem: %1").arg(acntId.username()),
		text = tr("Error while logging in with username '%1'.").arg(acntId.username()),
		detailText = errTxt;
	}
	emit showLoginErrMsgBox(title, text, detailText);
}

void IsdsWrapper::waitForHotpCode(const AcntId &acntId,
    const QString &isdsAction)
{
	debugFuncCall();

	emit openDialogRequest(isdsAction, AcntData::LIM_UNAME_PWD_HOTP,
	    acntId.username(), acntId.testing(),
	    tr("Enter security code: %1").arg(acntId.username()),
	    tr("Security code for data box '%1' required").arg(acntId.username()),
	    tr("Enter security code"),
	    false);
}

void IsdsWrapper::waitForSmsCode(const AcntId &acntId,
    const QString &isdsAction)
{
	debugFuncCall();

	emit openDialogRequest(isdsAction, AcntData::LIM_UNAME_PWD_TOTP,
	    acntId.username(), acntId.testing(),
	    tr("Enter SMS code: %1").arg(acntId.username()),
	    tr("SMS code for data box '%1' required").arg(acntId.username()),
	    tr("Enter SMS code"),
	    false);
}

void IsdsWrapper::downloadAccountInfoFinished(const AcntId &acntId,
    bool success, const QString &errTxt)
{
	emit downloadAccountInfoFinishedSig(acntId.username(), acntId.testing(),
	    success, errTxt);
}

void IsdsWrapper::downloadMessageFinished(const AcntId &acntId, qint64 msgId,
    bool success, const QString &errTxt)
{
	emit downloadMessageFinishedSig(acntId.username(), acntId.testing(),
	    msgId, QString::number(msgId), success, errTxt);
}

void IsdsWrapper::downloadMessageListFinished(const AcntId &acntId,
    bool success, const QString &errTxt, bool isMsgReceived)
{
	emit downloadMessageListFinishedSig(isMsgReceived, acntId.username(),
	    acntId.testing(), success, errTxt);
}

void IsdsWrapper::importZFOFinished(int zfoNumber, int zfoTotal,
    QString infoText)
{
	if (zfoNumber < zfoTotal) {
		emit zfoImportProgressSig(QString::number(zfoNumber) + ". " + infoText);
	} else {
		emit zfoImportProgressSig(QString::number(zfoNumber) + ". " + infoText);
		emit zfoImportFinishedSig(tr("ZFO import has been finished with the result:"));
	}
}

void IsdsWrapper::sendMessageFinished(const AcntId &acntId,
    const QString &transactId, const QString &dbIDRecipient,
    const QString &dmRecipient, qint64 msgId, bool isVodz,
    bool success, const QString &errTxt)
{
	if (m_transactIds.end() == m_transactIds.find(transactId)) {
		/* Nothing found. */
		return;
	}

	/* Gather data. */
	m_sentMsgResultList.append(TaskSendMessage::ResultData(
	    success, errTxt, dbIDRecipient, dmRecipient, msgId));

	if (!m_transactIds.remove(transactId)) {
		logErrorNL("%s",
		    "Unable to remove a transaction identifier from list of unfinished transactions.");
	}

	if (!m_transactIds.isEmpty()) {
		/* Still has some pending transactions. */
		return;
	}

	/* All transactions finished. */

	int sentErrCnt = 0;
	QString dialogueInfoText;
	QSet<qint64> dmIds;

	foreach (const TaskSendMessage::ResultData &rData, m_sentMsgResultList) {
		if (rData.result) {
			dialogueInfoText += tr(
			    "Message has been sent to <b>'%1'</b> <i>(%2)</i> as message number <b>%3</b>.").
			    arg(rData.dmRecipient).
			    arg(rData.dbIDRecipient).
			    arg(rData.dmId) + "<br/><br/>";

			if (m_downloadSentMsg) {
				dmIds.insert(rData.dmId);
			}
		} else {
			++sentErrCnt;
			dialogueInfoText += tr(
			    "Message has NOT been sent to <b>'%1'</b> <i>(%2)</i>. ISDS returns: %3").
			    arg(rData.dmRecipient).
			    arg(rData.dbIDRecipient).
			    arg(rData.errInfo) + "<br/><br/>";
		}
	}

	m_sentMsgResultList.clear();
	m_downloadSentMsg = false;

	emit sentMessageFinished(acntId.username(), acntId.testing(),
	    0 == sentErrCnt, dialogueInfoText);

	for (qint64 dmId : dmIds) {
		if (isVodz) {
			/* True = use MTOM/XOP. May be loaded
			 * from preferences in the future. */
			Isds::Tasks::downloadBigMessage(acntId,
			    Messages::MessageType::TYPE_SENT, dmId, false);
		} else {
			Isds::Tasks::downloadMessage(acntId,
			    Messages::MessageType::TYPE_SENT, dmId);
		}
	}
}

/*!
 * @brief Insert separator prepend text.
 *
 * @param[in] str Text.
 * @return Text with separator.
 */
static
QString insertSeparator(const QString &str)
{
	return QStringLiteral("; ") + str;
}

void IsdsWrapper::pdzInfoFinished(const AcntId &acntId, qint64 currentCredit,
    const QList<Isds::PDZInfoRec> &infos, bool success, const QString &errTxt)
{
	QString pdzSending(insertSeparator(tr("PDZ sending: unavailable")));

	if (!success) {
		logErrorNL("Cannot download pdz info for username %s; Error: %s",
		    acntId.username().toUtf8().constData(),
		    errTxt.toUtf8().constData());
		goto finish;
	}

	m_pdzInfos = infos;

	if (m_pdzInfos.count() == 0) {
		goto finish;
	}

	pdzSending = tr("PDZ: active");
	foreach (const Isds::PDZInfoRec &info, m_pdzInfos) {
		if (info.pdzType() == Isds::Type::PPT_G) {
			pdzSending += insertSeparator(tr("Subsidised by data box '%1'.").arg(info.pdzPayer()));
			break;
		} else if (info.pdzType() == Isds::Type::PPT_K) {
			pdzSending += insertSeparator(tr("Contract with Czech post"));
		} else if (info.pdzType() == Isds::Type::PPT_E) {
			m_pdzCredit =
			    QString::number((float)currentCredit / 100, 'f', 2);
			pdzSending += insertSeparator(tr("Remaining credit: %1 Kč").arg(m_pdzCredit));
		}
	}

finish:
	emit getPdzInfoSig(pdzSending);
}
