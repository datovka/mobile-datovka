/*
 * Copyright (C) 2014-2025 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

#include <QApplication>
#include <QByteArray>
#include <QDateTime>
#include <QDirIterator>
#include <QFile>
#include <QFileInfo>
#include <QQmlEngine> /* qmlRegisterType */
#include <QSettings>
#include <QStorageInfo>
#include <QStringBuilder>
#include <quazip/quazip.h> /* bundled or system */
#include <quazip/quazipdir.h> /* bundled or system */
#include <quazip/quazipfile.h> /* bundled or system */

#include "src/auxiliaries/ios_helper.h"
#include "src/backup_zip.h"
#include "src/datovka_shared/io/prefs_db.h"
#include "src/datovka_shared/io/records_management_db.h"
#include "src/datovka_shared/localisation/localisation.h"
#include "src/datovka_shared/log/log.h"
#include "src/datovka_shared/settings/pin.h"
#include "src/datovka_shared/worker/pool.h"
#include "src/global.h"
#include "src/io/filesystem.h"
#include "src/messages.h"
#include "src/models/backup_selection_model.h"
#include "src/setwrapper.h"
#include "src/settings/account.h"
#include "src/settings/accounts.h"
#include "src/settings/ini_preferences.h"
#include "src/settings/prefs_specific.h"
#include "src/sqlite/account_db.h"
#include "src/sqlite/dbs.h"
#include "src/sqlite/file_db_container.h"
#include "src/sqlite/message_db_container.h"
#include "src/sqlite/zfo_db.h"

#ifdef Q_COMPILER_RVALUE_REFS
#  define macroStdMove(x) ::std::move(x)
#else /* Q_COMPILER_RVALUE_REFS */
#  define macroStdMove(x) (x)
#endif /* Q_COMPILER_RVALUE_REFS */

#define JSON_FILE_SIZE_BYTES 1024
#define APP_VARIANT "mobile"

#define KILOBYTE Q_INT64_C(1024)
#define MEGABYTE Q_INT64_C(1048576)
#define GIGABYTE Q_INT64_C(1073741824)
#define TERABYTE Q_INT64_C(1099511627776)

/* JSON files describing stored data. */
#define BACKUP_JSON "backup_description.json"
#define TRANSFER_JSON "transfer_description.json"

/* Macosx metadata directory name. */
#define MACOSX_METADATA_DIR_NAME "__MACOSX/"

/*!
 * @brief Compute approximate size string including units.
 *
 * @param[in] size Size in bytes.
 * @return String with unit.
 */
static
QString sizeString(qint64 size)
{
	qint64 divider = 1;
	QString unit("B"); /* Bytes. */

	if (Q_UNLIKELY(size < 0)) {
		return BackupRestoreZipData::tr("unknown");
	} else if (size > TERABYTE) {
		divider = TERABYTE;
		unit = QStringLiteral("TB");
	} else if (size > GIGABYTE) {
		divider = GIGABYTE;
		unit = QStringLiteral("GB");
	} else if (size > MEGABYTE) {
		divider = MEGABYTE;
		unit = QStringLiteral("MB");
	} else if (size > KILOBYTE) {
		divider = KILOBYTE;
		unit = QStringLiteral("kB");
	} else {
		/* Bytes. */
	}

	qint64 whole = size / divider;
	qint64 decimal = 0;

	if (divider > 1) {
		decimal = size % divider; /* Remainder. */
		decimal = (10 * decimal) / divider; /* One decimal digit. */
	}

	return QString::number(whole) %
	    ((decimal > 0) ? (Localisation::programLocale.decimalPoint() + QString("%1").arg(decimal, 1, 10, QChar('0'))) : QString()) %
	    QStringLiteral(" ") % unit;
}

/*!
 * @brief Compute file checksum.
 *
 * @param[in] filePath File path.
 * @param[in] algorithm Algorithm to use for the checksum.
 * @return Checksum, null checksum on failure.
 */
static
Json::Backup::Checksum computeFileChecksum(const QString &filePath,
    enum Json::Backup::Checksum::Algorithm
    algorith = Json::Backup::Checksum::ALG_SHA512)
{
	QFile f(filePath);
	if (f.open(QFile::ReadOnly)) {
		return Json::Backup::Checksum::computeChecksum(&f, algorith);
	}
	return Json::Backup::Checksum();
}

/*!
 * @brief Compute target location free space in bytes.
 *
 * @param[in] targetPath Target path for backup.
 * @return Target location free space in bytes.
 */
static
qint64 computeAvailableSpace(const QString &targetPath)
{
	const QStorageInfo info(targetPath);
	return info.bytesAvailable();
}

/*!
 * @brief Remove target folder.
 *
 * @param[in] targetDir Backup target folder.
 * @return Return always false.
 */
static
bool errorBackup(const QString &targetDir)
{
	QDir(targetDir).removeRecursively();
	logErrorNL("%s", "Application data backup has been cancelled.");
	return false;
}

/*!
 * @brief Get list of all files in source directory tree.
 *
 * @param[in] dir Source directory path.
 * @return List of all files in directory tree.
 */
static
QStringList recurseAddDir(const QDir &dir) {

	QStringList fileList;
	QStringList eList = dir.entryList(QDir::NoDotAndDotDot |
	    QDir::Dirs | QDir::Files);

	foreach (const QString &file, eList) {
		QFileInfo fi(joinDirs(dir.path(),file));
		if (fi.isSymLink()) {
			return QStringList();
		}
		if (fi.isDir()) {
			QDir sd(fi.filePath());
			fileList.append(recurseAddDir(sd));
		} else {
			fileList << QDir::toNativeSeparators(fi.filePath());
		}
	}
	return fileList;
}

/*!
 * @brief Get relative file name path in ZIP archive.
 *
 * @param[in] fileInfo File info of source file.
 * @param[in] sourceDirPath Source directory path where data are stored.
 * @param[in] subDir Target sub directory where data will be stored.
 * @return Relative file name path in ZIP archive.
 */
static
QString fileNameWithRelativePath(const QFileInfo &fileInfo,
    const QDir &sourceDirPath, const QString &subDir)
{
	QString fileNameWithRelativePath = fileInfo.filePath().remove(
	    0, sourceDirPath.absolutePath().length() + 1);
	if (!subDir.isEmpty()) {
		fileNameWithRelativePath = joinDirs(subDir,
		    fileNameWithRelativePath);
	}
	return fileNameWithRelativePath;
}

/*!
 * @brief Add file into ZIP archive.
 *
 * @param[in] fileInfo File info of source file.
 * @param[in] sourceDirPath Source directory path where data are stored.
 * @param[in] subDir Target sub directory where data will be stored.
 * @param[in,out] quaZipFile Qua zip file stored into ZIP archive.
 * @return True if success.
 */
static
bool addFileIntoZip(const QFileInfo &fileInfo, const QDir &sourceDirPath,
    const QString &subDir, QuaZipFile &quaZipFile)
{
	QFile inFile(fileInfo.filePath());
	if (Q_UNLIKELY(!inFile.open(QIODevice::ReadOnly))) {
		return false;
	}
	if (Q_UNLIKELY(!quaZipFile.open(QIODevice::WriteOnly,
	        QuaZipNewInfo(fileNameWithRelativePath(fileInfo,
	        sourceDirPath, subDir), fileInfo.filePath())))) {
		goto fail;
	}
	if (Q_UNLIKELY(quaZipFile.write(inFile.readAll()) != inFile.size())) {
		quaZipFile.close();
		goto fail;
	}
	quaZipFile.close();
	if (Q_UNLIKELY(quaZipFile.getZipError() != UNZ_OK)) {
		goto fail;
	}
	inFile.close();
	return true;
fail:
	inFile.close();
	return false;
}

/*!
 * @brief Add JSON file into ZIP archive.
 *
 * @param[in] jsonName File name.
 * @param[in] jsonData File data.
 * @param[in,out] quaZipFile Qua zip file stored into ZIP archive.
 * @return True if success.
 */
static
bool addJsonIntoZip(const QString &jsonName, const QByteArray &jsonData,
    QuaZipFile &quaZipFile)
{
	if (Q_UNLIKELY(!quaZipFile.open(QIODevice::WriteOnly,
	        QuaZipNewInfo(jsonName)))) {
		return false;
	}
	if (Q_UNLIKELY(quaZipFile.write(jsonData) != jsonData.size())) {
		quaZipFile.close();
		return false;
	}
	quaZipFile.close();
	if (Q_UNLIKELY(quaZipFile.getZipError() != UNZ_OK)) {
		return false;
	}

	return true;
}

/*!
 * @brief Extract file from ZIP archive into target location.
 *
 * @param[in] quaZipFile Qua zip file taken from ZIP archive.
 * @param[in] backupBaseDir Backup base dir path in ZIP archive (can be null).
 * @param[in] subDir Source file subdir in ZIP archive (can be null).
 * @param[in] targetDir Target directory path where file will be stored.
 * @param[out] checksum Target file checksum.
 * @return True if success.
 */
static
bool extractFileFromZip(QuaZipFile &quaZipFile, const QString &backupBaseDir,
    const QString &subDir, const QDir &targetDir,
    Json::Backup::Checksum &checksum)
{
	QString outFilePathName(joinDirs(targetDir.absolutePath(),
	    quaZipFile.getActualFileName()));
	/*
	 * If backupBaseDir is present in quaZipFile path,
	 * remove it from target file path.
	 */
	if (!backupBaseDir.isEmpty()) {
		outFilePathName.remove(backupBaseDir);
	}
	/*
	 * If account subdir is present in quaZipFile path,
	 * remove it from target file path.
	 */
	if (!subDir.isEmpty()) {
		outFilePathName.remove(QDir::separator() + subDir);
	}

	QDir dir(QFileInfo(outFilePathName).path());
	QFile outFile(outFilePathName);

	if (Q_UNLIKELY(!quaZipFile.open(QIODevice::ReadOnly))) {
		return false;
	}
	if (Q_UNLIKELY((!dir.exists()) && (!dir.mkpath(".")))) {
		goto fail;
	}
	if (Q_UNLIKELY(!outFile.open(QIODevice::WriteOnly))) {
		goto fail;
	}
	if (Q_UNLIKELY(outFile.write(quaZipFile.readAll())
	        != quaZipFile.size())) {
		outFile.close();
		goto fail;
	}
	outFile.close();
	if (Q_UNLIKELY(!quaZipFile.atEnd())) {
		goto fail;
	}
	quaZipFile.close();
	if (Q_UNLIKELY(quaZipFile.getZipError() != UNZ_OK)) {
		return false;
	}

	checksum = computeFileChecksum(outFilePathName);
	return true;
fail:
	quaZipFile.close();
	return false;
}

/*!
 * @brief Fill application data into the backup entry.
 *
 * @param[in,out] bu Backup description structure.
 * @param[in]     dateTime Time information.
 */
static
void fillAppData(Json::Backup &bu, const QDateTime &dateTime)
{
	bu.setDateTime(dateTime);
	bu.setAppInfo(Json::Backup::AppInfo(
	    QString(APP_NAME), QString(APP_VARIANT), QString(VERSION)));
}

/*!
 * @brief Create JSON transfer content.
 *
 * @param[in] currentDateTime Backup date time.
 * @param[in] files List of files and their checksums.
 * @return JSON content.
 */
static
QByteArray createTransferJson(const QDateTime &currentDateTime,
    const QList<Json::Backup::File> &files)
{
	Json::Backup bu;
	bu.setType(Json::Backup::BUT_TRANSFER);
	fillAppData(bu, currentDateTime);

	bu.setFiles(files);
	return bu.toJsonData(true);
}

/*!
 * @brief Constructs backup-transfer directory name.
 *
 * @param[in] backupType Backup type.
 * @param[in] dateTime Time information.
 * @return Backup-transfer directory name.
 */
static
QString backupDirName(enum BackupRestoreZipData::BackupRestoreType backupType,
    const QDateTime &dateTime)
{
	QString backupName;

	switch (backupType) {
	case BackupRestoreZipData::BRT_BACKUP:
		backupName = DATOVKA_BACK_DIR_NAME;
		break;
	case BackupRestoreZipData::BRT_TRANSFER:
		backupName = DATOVKA_TRAN_DIR_NAME;
		break;
	default:
		Q_ASSERT(0);
		return QString();
		break;
	}

	QString dirName(backupName % QStringLiteral("_")
	    % QStringLiteral(APP_NAME) % QStringLiteral("-") % QStringLiteral("mobile")
	    % QStringLiteral("_") % dateTime.toString(Qt::ISODate));

	/*
	 * Android 11 refuses to access directories and files with a colon ':'
	 * in path.
	 * https://forum.syncthing.net/t/permission-denied-on-files-with-colon-on-android-11-permission-denied-on-syncthing-tmp-files-on-android/16096
	 * https://stackoverflow.com/questions/2679699/what-characters-allowed-in-file-names-on-android
	 */
	return dirName.replace(":", "-");
}

/*!
 * @brief Constructs full path to backup-transfer directory.
 *
 * @param[in] targetPath Path where the directory should be created.
 * @param[in] backupType Backup type.
 * @param[in] dateTime Time information.
 * @return Full path.
 */
static
QString backupDirPath(const QString &targetPath,
    enum BackupRestoreZipData::BackupRestoreType backupType,
    const QDateTime &dateTime)
{
	const QString subdir = backupDirName(backupType, dateTime);
	QDir dir(joinDirs(targetPath, subdir));
	if (Q_UNLIKELY((!dir.exists()) && (!dir.mkpath(".")))) {
		logErrorNL("Cannot create or access directory '%s'.",
		    dir.absolutePath().toUtf8().constData());
		return targetPath;
	}

	return dir.path();
}

/*!
 * @brief Compute the total sum of sizes of all files in directory.
 *
 * @param[in] dirPath Target path for backup.
 * @return Account backup data size in bytes.
 */
static
qint64 dirContentSize(const QString &dirPath)
{
	if (Q_UNLIKELY(dirPath.isEmpty())) {
		Q_ASSERT(0);
		return 0;
	}

	qint64 backupSize = 0;

	QDir dir(dirPath);
	QDirIterator iterator(dir.absolutePath(), QDirIterator::Subdirectories);
	while (iterator.hasNext()) {
		QFile file(iterator.next());
		QFileInfo fi(file.fileName());
		if (fi.isFile()) {
			backupSize += fi.size();
		}
	}

	return backupSize;
}

/*!
 * @brief Compute account backup data size in bytes.
 *
 * @param[in] acntId Account identifier.
 * @return Account backup data size in bytes.
 */
static
qint64 getAccountBackupSizeBytes(const AcntId &acntId)
{
	if (Q_UNLIKELY((GlobInstcs::messageDbsPtr == Q_NULLPTR) ||
	        (GlobInstcs::fileDbsPtr == Q_NULLPTR) ||
	        (GlobInstcs::prefsPtr == Q_NULLPTR) ||
	        (GlobInstcs::acntMapPtr == Q_NULLPTR))) {
		Q_ASSERT(0);
		return 0;
	}

	if (!acntId.isValid()) {
		return 0;
	}

	/* Get message db size. */
	MessageDb *msgDb = GlobInstcs::messageDbsPtr->accessMessageDb(
	    GlobalSettingsQmlWrapper::dbPath(), acntId, PrefsSpecific::dataOnDisk(
		*GlobInstcs::prefsPtr, GlobInstcs::acntMapPtr->acntData(acntId)));
	if (Q_UNLIKELY(msgDb == Q_NULLPTR)) {
		logErrorNL("Cannot access message database for username '%s'.",
		    acntId.username().toUtf8().constData());
		return 0;
	}
	qint64 size(msgDb->getDbSizeInBytes());

	/* Get file db size if exists. */
	FileDb *fDb = GlobInstcs::fileDbsPtr->accessFileDb(
	    GlobalSettingsQmlWrapper::dbPath(), acntId.username(),
	    PrefsSpecific::dataOnDisk(
	    *GlobInstcs::prefsPtr, GlobInstcs::acntMapPtr->acntData(acntId)));
	if (Q_UNLIKELY(fDb == Q_NULLPTR)) {
		logWarningNL("Cannot access file database for username '%s'.",
		    acntId.username().toUtf8().constData());
	} else {
		size += fDb->getDbSizeInBytes();
	}

	/* Get zfo files size. */
	QDir srcDir(joinDirs(GlobalSettingsQmlWrapper::dbPath(),
	    getAccountZfoDir(acntId.username(), acntId.testing())));
	QStringList fList = recurseAddDir(srcDir.absolutePath());
	foreach (const QString &file, fList) {
		QFileInfo fileInfo(file);
		if (!fileInfo.isFile()) {
			continue;
		}
		size += fileInfo.size();
	}

	return size;
}

/*!
 * @brief Remove message and file databases of account or create a new account.
 *
 * @param[in] acntId Account identifier.
 * @param[in] accountName Account name.
 * @return True if success.
 */
static
bool removedDbOrCreateAccount(const AcntId &acntId, const QString &accountName)
{
	if (Q_UNLIKELY((GlobInstcs::acntMapPtr == Q_NULLPTR) ||
	        (GlobInstcs::messageDbsPtr == Q_NULLPTR) ||
	        (GlobInstcs::fileDbsPtr == Q_NULLPTR) ||
	        (GlobInstcs::prefsPtr == Q_NULLPTR))) {
		Q_ASSERT(0);
		return false;
	}

	if (GlobInstcs::acntMapPtr->acntIds().contains(acntId)) {
		/* Close and delete both account databases. */
		MessageDb *msgDb = GlobInstcs::messageDbsPtr->accessMessageDb(
		    GlobalSettingsQmlWrapper::dbPath(), acntId,
		    PrefsSpecific::dataOnDisk(*GlobInstcs::prefsPtr,
		    GlobInstcs::acntMapPtr->acntData(acntId)));
		if (Q_UNLIKELY(msgDb == Q_NULLPTR)) {
			logErrorNL("%s", "Cannot access message database.");
			return false;
		}
		if (!GlobInstcs::messageDbsPtr->deleteDb(msgDb)) {
			logErrorNL("%s", "Could not delete message database.");
			return false;
		}
		FileDb *fDb = GlobInstcs::fileDbsPtr->accessFileDb(
		    GlobalSettingsQmlWrapper::dbPath(), acntId.username(),
		    PrefsSpecific::dataOnDisk(
		        *GlobInstcs::prefsPtr, GlobInstcs::acntMapPtr->acntData(acntId)));
		if (Q_UNLIKELY(fDb == Q_NULLPTR)) {
			logErrorNL("Cannot access file database for username '%s'.",
			    acntId.username().toUtf8().constData());
			return false;
		}
		if (!GlobInstcs::fileDbsPtr->deleteDb(fDb)) {
			logErrorNL("%s", "Could not delete file database.");
			return false;
		}
	} else {
		/* Create new account if not exists in application settings. */
		QString acntName(BackupRestoreZipData::tr("From backup") +  " " + accountName);
		QSettings settings(INIPreferences::confPath(),
		    QSettings::IniFormat);
		GlobInstcs::acntMapPtr->addCredentialsTorso(settings,
		    acntName, acntId);
		settings.sync();
	}

	return true;
}

/*!
 * @brief Get dir path to the JSON file in the ZIP archive.
 *
 * @param[in] zip QuaZip archive.
 * @return Return dir path to the JSON file or null if root.
 */
static
QString getBackupBaseDir(QuaZip &zip)
{
	QuaZipFileInfo info;
	QString jsonFilePath;

	/* Find JSON file. */
	for (bool f = zip.goToFirstFile(); f; f = zip.goToNextFile()) {
		if (Q_UNLIKELY(!zip.getCurrentFileInfo(&info))) {
			return QString();;
		}
		QFileInfo fi(info.name);
		/* Skip root, dirs and macosx metadata in zip file. */
		if (fi.fileName().isEmpty() ||
		        (info.name.contains(QString(MACOSX_METADATA_DIR_NAME)))) {
			continue;
		}
		if (fi.suffix() == QString("json")) {
			jsonFilePath = info.name;
		}
	}

	if (Q_UNLIKELY(jsonFilePath.isEmpty())) {
		return QString();
	}

	/* Return dir path to JSON or null if is in the root of zip archive. */
	QFileInfo jsonFi(jsonFilePath);
	if (jsonFi.path() == ".") {
		return QString();
	}

	return jsonFi.path() + QDir::separator();
}

/*!
 * @brief Test if file is excluded from restore.
 *
 * @param[in] fi File info.
 * @return True if success.
 */
static
bool isExcludedFileFromRestore(const QFileInfo &fi)
{
	return (fi.fileName().isEmpty()
	    || fi.filePath().contains(QString(MACOSX_METADATA_DIR_NAME))
	    || (fi.suffix() == QString("json")));
}

void BackupRestoreZipData::declareQML(void)
{
	qmlRegisterType<BackupRestoreZipData>("cz.nic.mobileDatovka.qmlInteraction", 1, 0, "BackupRestoreZipData");
	qRegisterMetaType<BackupRestoreZipData>("BackupRestoreZipData");
	qRegisterMetaType<BackupRestoreZipData::BackupRestoreType>("BackupRestoreZipData::BackupRestoreType");
}

BackupRestoreZipData::BackupRestoreZipData(QObject *parent)
    : QObject(parent),
    m_zipPath(),
    m_loadedBackup()
{
}

BackupRestoreZipData::BackupRestoreZipData(const BackupRestoreZipData &other)
    : QObject(),
    m_zipPath(other.m_zipPath),
    m_loadedBackup(other.m_loadedBackup)
{
}

void BackupRestoreZipData::fillBackupModel(BackupRestoreSelectionModel *acntModel)
{
	debugSlotCall();

	if (Q_UNLIKELY((GlobInstcs::acntMapPtr == Q_NULLPTR) ||
	        (GlobInstcs::accountDbPtr == Q_NULLPTR))) {
		Q_ASSERT(0);
		return;
	}

	if (Q_UNLIKELY(acntModel == Q_NULLPTR)) {
		logErrorNL("%s", "Cannot access backup model.");
		Q_ASSERT(0);
		return;
	}

	QList<AcntId> acntIdList = GlobInstcs::acntMapPtr->keys();
	foreach (const AcntId &acntId, acntIdList) {
		acntModel->appendData(false,
		    GlobInstcs::acntMapPtr->acntData(acntId).accountName(),
		    acntId, GlobInstcs::accountDbPtr->dbId(acntId.username()),
		    QString());
	}
}

void BackupRestoreZipData::fillRestoreModel(BackupRestoreSelectionModel *acntModel)
{
	debugSlotCall();

	if (Q_UNLIKELY(acntModel == Q_NULLPTR)) {
		logErrorNL("%s", "Cannot access backup model.");
		Q_ASSERT(0);
		return;
	}

	if (Q_UNLIKELY(!m_loadedBackup.isValid())) {
		logErrorNL("Cannot open or read file '%s'.",
		    m_zipPath.toUtf8().constData());
		emit actionTextSig(tr("Cannot open or read file '%1'.").arg(m_zipPath));
		return;
	}

	if (m_loadedBackup.zfoDb().isValid()) {
		emit visibleZfoSwitchSig(true);
	}

	foreach (const Json::Backup::MessageDb &mbu, m_loadedBackup.messageDbs()) {
		acntModel->appendData(false, mbu.accountName(),
		    AcntId(mbu.username(), mbu.testing()),
		    mbu.boxId(), mbu.subdir());
	}

	emit actionTextSig(tr("Select accounts from the backup ZIP file which you want to restore. "
	    "Data of existing accounts will be replaced by data from the backup."));
}

bool BackupRestoreZipData::backUpAccount(const QmlAcntId *qAcntId,
    const QString &targetPath)
{

	if (Q_UNLIKELY((qAcntId == Q_NULLPTR) || (!qAcntId->isValid()))) {
		Q_ASSERT(0);
		return false;
	}

	QList<AcntId> acntIdList;
	acntIdList.append(*qAcntId);
	return backUpAccounts(acntIdList, targetPath);
}

bool BackupRestoreZipData::backUpSelectedAccounts(
    BackupRestoreSelectionModel *acntModel, const QString &targetPath)
{
	debugSlotCall();

	if (Q_UNLIKELY(acntModel == Q_NULLPTR)) {
		logErrorNL("%s", "Cannot access backup model.");
		Q_ASSERT(0);
		return false;
	}

	return backUpAccounts(acntModel->selectedAccountIds(), targetPath);
}

bool BackupRestoreZipData::canBackUpAccount(const QmlAcntId *qAcntId,
    const QString &targetPath, bool transfer)
{
	if (Q_UNLIKELY((qAcntId == Q_NULLPTR) || (!qAcntId->isValid()))) {
		Q_ASSERT(0);
		return false;
	}

	QList<AcntId> acntIdList;
	acntIdList.append(*qAcntId);
	return canBackUpAccounts(acntIdList, targetPath, transfer);
}

bool BackupRestoreZipData::canBackupSelectedAccounts(
    BackupRestoreSelectionModel *acntModel, const QString &targetPath,
    bool transfer)
{
	if (Q_UNLIKELY(acntModel == Q_NULLPTR)) {
		logErrorNL("%s", "Cannot access backup model.");
		Q_ASSERT(0);
		return false;
	}

	return canBackUpAccounts(acntModel->selectedAccountIds(), targetPath,
	   transfer);
}

bool BackupRestoreZipData::canRestoreSelectedAccounts(
    BackupRestoreSelectionModel *acntModel, bool includeZfoDb)
{
	debugSlotCall();

	if (Q_UNLIKELY(acntModel == Q_NULLPTR)) {
		logErrorNL("%s", "Cannot access backup model.");
		Q_ASSERT(0);
		return false;
	}

	qint64 targetSize =
	    computeAvailableSpace(GlobalSettingsQmlWrapper::dbPath());
	/* Account database size. */
	qint64 backupSize = m_loadedBackup.accountDb().size();
	/* Zfo database size. */
	if (includeZfoDb) {
		backupSize += m_loadedBackup.zfoDb().size();
	}

	/* Account message and file databases and zfo files size. */
	QList<AcntId> acntIdList = acntModel->selectedAccountIds();
	foreach (const AcntId &acntId, acntIdList) {
		foreach (const Json::Backup::MessageDb &mbu,
		        m_loadedBackup.messageDbs()) {
			/* Get size of all backup files of selected account. */
			if (mbu.username() == acntId.username() && mbu.testing()
			        == acntId.testing()) {
				foreach (const Json::Backup::File &file,
				        mbu.files()) {
					backupSize += file.size();
				}
			}
		}
	}

	bool canRestore = (backupSize < targetSize);
	if (Q_UNLIKELY(!canRestore && targetSize >= 0)) {
		logErrorNL("%s", "There is not enough space in the selected storage.");
		emit actionTextSig(tr("There is not enough space in the selected storage."));
	}

	sendSizeInfoToQml(backupSize, targetSize);

	if (acntIdList.isEmpty()) {
		return false;
	}

	return canRestore;
}

enum BackupRestoreZipData::BackupRestoreType
    BackupRestoreZipData::loadBackupJsonFromZip(const QString &zipPath,
    const QString &zipFileName)
{
	debugSlotCall();

	/* Check if selected file is zip archive. */
	QFileInfo fi(zipFileName);
	if (Q_UNLIKELY(fi.suffix() != QString("zip"))) {
		logErrorNL("Zip file is missing or invalid: '%s'.",
		    zipFileName.toUtf8().constData());
		emit actionTextSig(tr("The selected file isn't a valid ZIP archive containing a backup."));
		return BRT_UNKNOWN;
	}
	QuaZip zip(zipPath);
	if (Q_UNLIKELY(!zip.open(QuaZip::mdUnzip))) {
		logErrorNL("Zip file is missing or invalid: '%s'.",
		    zipFileName.toUtf8().constData());
		emit actionTextSig(tr("The selected file isn't a valid ZIP archive containing a backup."));
		return BRT_UNKNOWN;
	}

	QuaZipFile quaZipFile(&zip);
	QuaZipFileInfo info;
	QString jsonFileName;
	qint64 backupSize = 0;
	qint64 zipEntries = 0;

	/* Find and read JSON backup file. */
	for (bool f = zip.goToFirstFile(); f; f = zip.goToNextFile()) {
		if (Q_UNLIKELY(!zip.getCurrentFileInfo(&info))) {
			return BRT_UNKNOWN;
		}

		QFileInfo fi(info.name);

		/* Skip root, dirs and macosx metadata in zip file. */
		if (fi.fileName().isEmpty() ||
		        (info.name.contains(QString(MACOSX_METADATA_DIR_NAME)))) {
			continue;
		}

		if (fi.suffix() == QString("json")) {
			quaZipFile.open(QIODevice::ReadOnly | QIODevice::Text);
			m_loadedBackup = Json::Backup::fromJsonData(quaZipFile.readAll());
			jsonFileName = info.name;
			quaZipFile.close();
		} else {
			++zipEntries;
			backupSize += info.uncompressedSize;
		}
	}
	zip.close();

	if (Q_UNLIKELY(!m_loadedBackup.isValid())) {
		logErrorNL("JSON file is missing or invalid in ZIP archive: '%s'.",
		    zipFileName.toUtf8().constData());
		emit actionTextSig(tr("The selected ZIP archive doesn't contain any JSON file."));
		return BRT_UNKNOWN;
	}

	if ((m_loadedBackup.appInfo().appName() != QStringLiteral(APP_NAME)) ||
	    (m_loadedBackup.appInfo().appVariant() != QStringLiteral(APP_VARIANT))) {
		logErrorNL("JSON file '%s' does not contain valid application information.",
		    jsonFileName.toUtf8().constData());
		emit actionTextSig(
		    tr("JSON file '%1' does not contain valid application information.").arg(jsonFileName));
		return BRT_UNKNOWN;
	}

	QDateTime dateTime = m_loadedBackup.dateTime();
	qint64 targetSize = computeAvailableSpace(GlobalSettingsQmlWrapper::dbPath());
	QString versionText;
	bool canRestore = false;

	enum BackupRestoreType restoreType = BRT_UNKNOWN;
	m_zipPath = zipPath;
	switch (m_loadedBackup.type()) {
	case Json::Backup::BUT_BACKUP:
		restoreType = BRT_BACKUP;
		if (dateTime.isValid()) {
			emit bacupDateTextSig(
			    tr("Backup was taken at %1").arg(dateTime.toString(DATETIME_QML_FORMAT))
			    + " " + tr("and contains %n file(s).", "", zipEntries));
		}
		break;
	case Json::Backup::BUT_TRANSFER:
		restoreType = BRT_TRANSFER;
		if (m_loadedBackup.appInfo().appVersion() != QStringLiteral(VERSION)) {
			logWarningNL("%s", "Application version does not match the transfer version.");
			versionText = tr("Warning: Application version '%1' does not match the transfer version '%2'. Is is recommended to transfer data between same versions of the application to prevent incompatibility issues.").arg(QStringLiteral(VERSION)).arg(m_loadedBackup.appInfo().appVersion());
		}
		if (dateTime.isValid()) {
			emit bacupDateTextSig(
			    tr("Transfer ZIP archive was taken at %1").arg(dateTime.toString(DATETIME_QML_FORMAT))
			    + " " + tr("and contains %n file(s).", "", zipEntries) + " " + versionText);
		}

		canRestore = (backupSize < targetSize);
		sendSizeInfoToQml(backupSize, targetSize);

		if (canRestore) {
			emit actionTextSig(tr("Restore all application data from transfer ZIP file. "
			    "The current application data will be complete rewritten with new data from transfer backup."));
		} else {
			logErrorNL("%s", "There is not enough space in the selected storage.");
			emit actionTextSig(tr("There is not enough space in the selected storage."));
		}
		emit canRestoreFromTransferSig(canRestore);
		break;
	default:
		logErrorNL("%s", "Unknown backup type.");
		emit actionTextSig(tr("Unknown backup type. "
		    "JSON file contains no valid backup data."));
		break;
	}

	return restoreType;
}

bool BackupRestoreZipData::restoreDataFromTransferZiP(void)
{
	debugSlotCall();

	/* Open zip archive. */
	QuaZip zip(m_zipPath);
	if (Q_UNLIKELY(!zip.open(QuaZip::mdUnzip))) {
		logErrorNL("ZIP file is missing or invalid: '%s'.",
		    m_zipPath.toUtf8().constData());
		emit actionTextSig(tr("Cannot open ZIP archive."));
		return false;
	}

	QuaZipDir quaZipDir(&zip);
	QString backupBaseDir(getBackupBaseDir(zip));
	QuaZipFile quaZipFile(&zip);
	QuaZipFileInfo info;
	Json::Backup::Checksum checksum;
	bool success = true;

	/* Delete all files from appdata location. */
	QDir targetDir(GlobalSettingsQmlWrapper::dbPath());
	targetDir.removeRecursively();

	/* Read and transfer all files from ZIP archive (without JSON file). */
	for (bool f = zip.goToFirstFile(); f; f = zip.goToNextFile()) {
		if (Q_UNLIKELY(!zip.getCurrentFileInfo(&info))) {
			logErrorNL("Cannot read file info '%s' from ZIP archive.",
			    info.name.toUtf8().constData());
			success = false;
			continue;
		}

		QFileInfo fi(info.name);

		/* Skip root, dirs and macosx metadata in zip file. */
		if (isExcludedFileFromRestore(fi)) {
			continue;
		}

		emit actionTextSig(tr("Restoring file '%1'.").arg(fi.fileName()));
		QCoreApplication::processEvents();

		/* Extract file from ZIP archive and write into appdata location. */
		if (Q_UNLIKELY(!extractFileFromZip(quaZipFile, backupBaseDir,
		        QString(), targetDir, checksum))) {
			logErrorNL("Cannot extract file '%s' from ZIP archive.",
			    fi.fileName().toUtf8().constData());
			success = false;
			continue;
		}
		if (Q_UNLIKELY(!checksum.isValid())) {
			logWarningNL("Invalid checksum. Skipping checksum check on file '%s'.",
			    fi.fileName().toUtf8().constData());
			success = false;
			continue;
		}
		/* Checksum comparison. */
		foreach (const Json::Backup::File &file, m_loadedBackup.files()) {
			if (file.fileName() == fi.fileName() &&
			        file.size() == info.uncompressedSize) {
				if (Q_UNLIKELY(file.checksum() != checksum)) {
					logWarningNL("Checksum of file '%s' does not match.",
					    file.fileName().toUtf8().constData());
					success = false;
				}
				break;
			}
		}
	}
	zip.close();

	if (success) {
		logInfoNL("%s", "All data were restored successfully.");
		emit actionTextSig(tr("Restoration finished."));
	} else {
		targetDir.removeRecursively();
		logErrorNL("%s", "Application data restoration has been cancelled.");
	}

	return success;
}

bool BackupRestoreZipData::restoreSelectedAccountsFromZip(
    BackupRestoreSelectionModel *acntModel, bool includeZfoDb)
{
	debugSlotCall();

	if (Q_UNLIKELY(acntModel == Q_NULLPTR)) {
		logErrorNL("%s", "Cannot access backup model.");
		Q_ASSERT(0);
		return false;
	}

	/* Open ZIP archive. */
	QuaZip zip(m_zipPath);
	if (Q_UNLIKELY(!zip.open(QuaZip::mdUnzip))) {
		logErrorNL("Zip file is missing or invalid: '%s'.",
		    m_zipPath.toUtf8().constData());
		emit actionTextSig(tr("Cannot open ZIP archive."));
		return false;
	}

	QuaZipDir quaZipDir(&zip);
	QString backupBaseDir(getBackupBaseDir(zip));
	QuaZipFile quaZipFile(&zip);
	QuaZipFileInfo info;
	Json::Backup::Checksum checksum;
	QDir targetDir(GlobalSettingsQmlWrapper::dbPath());
	bool success = true;

	/* Get selected account subdir name. */
	QList<AcntId> acntIdList = acntModel->selectedAccountIds();
	QList<QString> subDirList;
	foreach (const AcntId &acntId, acntIdList) {
		foreach (const Json::Backup::MessageDb &mbu,
		        m_loadedBackup.messageDbs()) {
			if (mbu.username() == acntId.username()
			        && mbu.testing() == acntId.testing()) {
				/* Extract message subdir from account JSON data. */
				subDirList.append(mbu.subdir());
				/* Remove old databases or create new account. */
				removedDbOrCreateAccount(acntId,
				     mbu.accountName());
			}
		}
	}

	/* Read and extract all selected accounts from ZIP archive (without JSON file). */
	for (bool f = zip.goToFirstFile(); f; f = zip.goToNextFile()) {

		bool isSelected = false;
		QString subdir;
		if (Q_UNLIKELY(!zip.getCurrentFileInfo(&info))) {
			logErrorNL("Cannot read file info '%s' from ZIP archive.",
			    info.name.toUtf8().constData());
			success = false;
			continue;
		}

		QFileInfo fi(info.name);
		/* Skip root, dirs and macosx metadata in zip file. */
		if (isExcludedFileFromRestore(fi)) {
			continue;
		}
		/* Account db file. */
		if (info.name.contains(m_loadedBackup.accountDb().fileName())) {
			isSelected = true;
		}
		/* Zfo db file. */
		if (info.name.contains(m_loadedBackup.zfoDb().fileName())
		        && includeZfoDb) {
			isSelected = true;
		}
		/* Test if file in ZIP archive contains subdir of selected account. */
		if (!isSelected) {
			foreach (const QString &subDir, subDirList) {
				if (info.name.contains(subDir)) {
					subdir = subDir;
					isSelected = true;
					break;
				}
			}
		}
		/* If the file is not valid for selected account, read next. */
		if (!isSelected) {
			continue;
		}

		emit actionTextSig(tr("Restoring file '%1'.").arg(fi.fileName()));
		QCoreApplication::processEvents();

		/* Extract file from ZIP archive and write into sandbox. */
		if (Q_UNLIKELY(!extractFileFromZip(quaZipFile, backupBaseDir,
		        subdir, targetDir, checksum))) {
			logErrorNL("Cannot extract file '%s' from ZIP archive.",
			    fi.fileName().toUtf8().constData());
			success = false;
			continue;
		}

		/* Checksum match is disabled due old backup format. */
#if 0
		if (Q_UNLIKELY(!checksum.isValid())) {
			logWarningNL("Invalid checksum. Skipping checksum check on file '%s'.",
			    fi.fileName().toUtf8().constData());
			success = false;
			continue;
		}
		/* Checksum comparison. */
		foreach (const Json::Backup::File &file, m_loadedBackup.files()) {
			if (file.fileName() == fi.fileName() &&
			        file.size() == info.uncompressedSize) {
				if (file.checksum() != checksum) {
					logWarningNL("Checksum of file '%s' does not match.",
					    file.fileName().toUtf8().constData());
					success = false;
				}
				break;
			}
		}
#endif
		QCoreApplication::processEvents();
	}
	zip.close();

	if (success) {
		logInfoNL("%s", "All selected accounts were successfully restored.");
		emit actionTextSig(tr("Restoration finished."));
#ifdef Q_OS_IOS
		IosHelper::clearRestoreFolder(QFileInfo(m_zipPath).path());
#endif /* Q_OS_IOS */
	} else {
		logErrorNL("%s", "Account data restoration has been cancelled.");
	}

	return success;
}

void BackupRestoreZipData::stopWorker(bool stop)
{
	debugSlotCall();

	if (Q_UNLIKELY(GlobInstcs::workPoolPtr == Q_NULLPTR)) {
		Q_ASSERT(0);
		return;
	}

	if (stop) {
		logInfoNL("%s", "Waiting for pending worker threads.");
		GlobInstcs::workPoolPtr->wait();
		GlobInstcs::workPoolPtr->stop();
		logInfoNL("%s", "Worker pool stopped.");
	} else {
		GlobInstcs::workPoolPtr->start();
		logInfoNL("%s", "Worker pool started.");
	}
}

bool BackupRestoreZipData::transferAppDataToZip(const QString &targetPath)
{
	debugSlotCall();

	if (Q_UNLIKELY(GlobInstcs::pinSetPtr == Q_NULLPTR)) {
		Q_ASSERT(0);
		return false;
	}

	/* Check if application PIN is set. */
	if (!GlobInstcs::pinSetPtr->pinConfigured()) {
		return false;
	}

	/* Create ZIP archive destination path. */
	QString zipDir(targetPath);
	const QDateTime currentDateTime = QDateTime::currentDateTime();

#ifdef Q_OS_IOS
	zipDir = appTransferDirPath();
#endif /* Q_OS_IOS */

	/* Used default transfer location. */
	zipDir = appTransferDirPath();
	zipDir = backupDirPath(zipDir, BRT_TRANSFER, currentDateTime);

	/* Create zip file. */
	QString zipFilePath = joinDirs(zipDir,
	    backupDirName(BRT_TRANSFER, currentDateTime)
	        + QStringLiteral(".zip"));
	QuaZip zipFile(zipFilePath);
	if (Q_UNLIKELY(!zipFile.open(QuaZip::mdCreate))) {
		logErrorNL("Cannot create ZIP file in location '%s'.",
		    zipFilePath.toUtf8().constData());
		return errorBackup(zipDir);
	}

	/* Store all files into ZIP archive. */
	QuaZipFile quaZipFile(&zipFile);
	QList<Json::Backup::File> files;
	QDir sDirPath(GlobalSettingsQmlWrapper::dbPath());
	QStringList fileList = recurseAddDir(sDirPath.absolutePath());
	QCoreApplication::processEvents();

	foreach (const QString &file, fileList) {
		QFileInfo fileInfo(file);
		if (!fileInfo.isFile()) {
			continue;
		}

		emit actionTextSig(tr("Transferring file '%1'.").arg(fileInfo.fileName()));
		QCoreApplication::processEvents();

		if (Q_UNLIKELY(!addFileIntoZip(fileInfo, sDirPath,
		        QString(), quaZipFile))) {
			logErrorNL("File '%s' has not been added into ZIP archive.",
			    fileInfo.fileName().toUtf8().constData());
			return errorBackup(zipDir);
		}

		Json::Backup::File jsonData(fileNameWithRelativePath(fileInfo,
		    GlobalSettingsQmlWrapper::dbPath(), QString()),
		    computeFileChecksum(fileInfo.filePath()), fileInfo.size());
		if (fileInfo.suffix() == QStringLiteral("conf")) {
			files.prepend(jsonData);
		} else {
			files.append(jsonData);
		}

		QCoreApplication::processEvents();
	}

	/* Create and store transfer JSON file into ZIP archive. */
	const QString jsonName(QStringLiteral(TRANSFER_JSON));
	const QByteArray jsonData(createTransferJson(currentDateTime, files));
	if (Q_UNLIKELY(!addJsonIntoZip(jsonName, jsonData, quaZipFile))) {
		logErrorNL("File '%s' has not been added into ZIP archive.",
		    jsonName.toUtf8().constData());
		return errorBackup(zipDir);
	}

	/* Close ZIP file. */
	zipFile.close();

	/* Show final notification. */
	emit actionTextSig(tr("Transfer finished."));
	logInfoNL(
	    "All application data have been successfully written into transfer ZIP file '%s'.",
	    zipFilePath.toUtf8().constData());
	QCoreApplication::processEvents();

#ifdef Q_OS_IOS

	/* Prepare iCloud transfer. */
	QString title, text, detailText;
	IosHelper::FileICloud iCloudFile;
	iCloudFile.srcFilePath = zipFilePath;
	iCloudFile.destiCloudPath = joinDirs(QStringLiteral(DATOVKA_TRAN_DIR_NAME),
	    backupDirName(BackupRestoreZipData::BRT_TRANSFER, currentDateTime));
	QList<IosHelper::FileICloud> iCloudFiles;
	iCloudFiles.append(iCloudFile);
	IosHelper::storeFilesToCloud(iCloudFiles, title, text, detailText);
	emit showMessageBox(title, text, detailText);

#endif /* Q_OS_IOS */

	return true;
}

Json::Backup::MessageDb BackupRestoreZipData::backUpOneAccount(
    const AcntId &acntId, QuaZipFile &quaZipFile)
{
	debugFuncCall();

	if (Q_UNLIKELY((GlobInstcs::acntMapPtr == Q_NULLPTR) ||
	        (GlobInstcs::messageDbsPtr == Q_NULLPTR) ||
	        (GlobInstcs::fileDbsPtr == Q_NULLPTR) ||
	        (GlobInstcs::prefsPtr == Q_NULLPTR))) {
		Q_ASSERT(0);
		return Json::Backup::MessageDb();
	}

	if (Q_UNLIKELY(!acntId.isValid())) {
		Q_ASSERT(0);
		return Json::Backup::MessageDb();
	}

	const QString boxId = GlobInstcs::accountDbPtr->dbId(acntId.username());
	if (Q_UNLIKELY(boxId.isEmpty())) {
		logErrorNL("Backup: data-box ID missing for username '%s'.",
		    acntId.username().toUtf8().constData());
		return Json::Backup::MessageDb();
	}

	Json::Backup::MessageDb mbe;
	mbe.setTesting(acntId.testing());
	mbe.setAccountName(GlobInstcs::acntMapPtr->acntData(acntId).accountName());
	mbe.setBoxId(boxId);
	mbe.setUsername(acntId.username());
	const QString subdir(boxId + QStringLiteral("_") + acntId.username());
	mbe.setSubdir(subdir);

	MessageDb *msgDb = GlobInstcs::messageDbsPtr->accessMessageDb(
	    GlobalSettingsQmlWrapper::dbPath(), acntId, PrefsSpecific::dataOnDisk(
	    *GlobInstcs::prefsPtr, GlobInstcs::acntMapPtr->acntData(acntId)));
	if (Q_UNLIKELY(msgDb == Q_NULLPTR)) {
		logErrorNL("%s", "Cannot access message database.");
		return Json::Backup::MessageDb();
	}

	QList<Json::Backup::File> fileList;
	QFileInfo fi(msgDb->fileName());
	QString fileName = fi.fileName();
	emit actionTextSig(tr("Backing up file '%1'.").arg(fileName));
	QCoreApplication::processEvents();
	if (Q_UNLIKELY(!addFileIntoZip(fi, GlobalSettingsQmlWrapper::dbPath(),
	        subdir, quaZipFile))) {
		logErrorNL("File '%s' has not been added into ZIP archive.",
		    fi.fileName().toUtf8().constData());
		return Json::Backup::MessageDb();
	}
	fileList.append(Json::Backup::File(fileName,
	    computeFileChecksum(fi.filePath()), fi.size()));

	FileDb *fDb = GlobInstcs::fileDbsPtr->accessFileDb(
	    GlobalSettingsQmlWrapper::dbPath(), acntId.username(),
	    PrefsSpecific::dataOnDisk(
	        *GlobInstcs::prefsPtr, GlobInstcs::acntMapPtr->acntData(acntId)));
	if (Q_UNLIKELY(fDb == Q_NULLPTR)) {
		logErrorNL("Cannot access file database for username '%s'.",
		    acntId.username().toUtf8().constData());
		return Json::Backup::MessageDb();
	}

	fi = QFileInfo(fDb->fileName());
	fileName = fi.fileName();
	emit actionTextSig(tr("Backing up file '%1'.").arg(fileName));
	QCoreApplication::processEvents();
	if (Q_UNLIKELY(!addFileIntoZip(fi, GlobalSettingsQmlWrapper::dbPath(),
	    subdir, quaZipFile))) {
		logErrorNL("File '%s' has not been added into ZIP archive.",
		    fi.fileName().toUtf8().constData());
		return Json::Backup::MessageDb();
	}
	fileList.append(Json::Backup::File(fileName,
	    computeFileChecksum(fi.filePath()), fi.size()));

	QDir srcDir(joinDirs(GlobalSettingsQmlWrapper::dbPath(),
	    getAccountZfoDir(acntId.username(), acntId.testing())));
	QStringList fList = recurseAddDir(srcDir.absolutePath());

	foreach (const QString &file, fList) {
		QFileInfo fileInfo(file);
		if (!fileInfo.isFile()) {
			continue;
		}

		emit actionTextSig(tr("Backing up file '%1'.").arg(fileInfo.fileName()));
		QCoreApplication::processEvents();

		if (Q_UNLIKELY(!addFileIntoZip(fileInfo,
		        GlobalSettingsQmlWrapper::dbPath(), subdir, quaZipFile))) {
			logErrorNL("File '%s' has not been added into ZIP archive.",
			    fileInfo.fileName().toUtf8().constData());
			return Json::Backup::MessageDb();
		}

		Json::Backup::File jsonData(fileNameWithRelativePath(fileInfo,
		    GlobalSettingsQmlWrapper::dbPath(), QString()),
		    computeFileChecksum(fileInfo.filePath()), fileInfo.size());
		fileList.append(jsonData);

		QCoreApplication::processEvents();
	}

	mbe.setFiles(macroStdMove(fileList));

	return mbe;
}

bool BackupRestoreZipData::backUpAccounts(const QList<AcntId> &acntIds,
    const QString &targetPath)
{
	debugFuncCall();

	if (Q_UNLIKELY(GlobInstcs::accountDbPtr == Q_NULLPTR)) {
		Q_ASSERT(0);
		return false;
	}

	QCoreApplication::processEvents();

	/* Create ZIP archive destination path. */
	QString zipDir(targetPath);
	const QDateTime currentDateTime = QDateTime::currentDateTime();
	Json::Backup bu;
	bu.setType(Json::Backup::BUT_BACKUP);
	fillAppData(bu, currentDateTime);

	/* Used default backup location. */
	zipDir = appBackupDirPath();
	zipDir = backupDirPath(zipDir, BRT_BACKUP, currentDateTime);

	/* Create ZIP file. */
	QString zipFilePath = joinDirs(zipDir,
	    backupDirName(BRT_BACKUP, currentDateTime)
	        + QStringLiteral(".zip"));
	QuaZip zipFile(zipFilePath);
	if (Q_UNLIKELY(!zipFile.open(QuaZip::mdCreate))) {
		logErrorNL("Cannot create ZIP file in location '%s'.",
		    zipFilePath.toUtf8().constData());
		return errorBackup(zipDir);
	}

	/* Store all files into ZIP archive. */
	QuaZipFile quaZipFile(&zipFile);
	QDir sDirPath(GlobalSettingsQmlWrapper::dbPath());
	QStringList fileList = recurseAddDir(sDirPath.absolutePath());
	QCoreApplication::processEvents();

	/* Backup account message and file databases and zfo files. */
	QList<Json::Backup::MessageDb> mbeList;
	foreach (const AcntId &acntId, acntIds) {
		Json::Backup::MessageDb mbe =
		    backUpOneAccount(acntId, quaZipFile);
		if (mbe.isValid()) {
			mbeList.append(mbe);
		} else {
			return errorBackup(zipDir);
		}
	}
	bu.setMessageDbs(macroStdMove(mbeList));

	/* Backup account database. */
	QFileInfo fi(GlobInstcs::accountDbPtr->fileName());
	QString fileName(fi.fileName());
	emit actionTextSig(tr("Backing up file '%1'.").arg(fileName));
	QCoreApplication::processEvents();
	if (Q_UNLIKELY(!addFileIntoZip(fi, sDirPath, QString(), quaZipFile))) {
		logErrorNL("File '%s' has not been added into ZIP archive.",
		    fi.fileName().toUtf8().constData());
		return errorBackup(zipDir);
	}
	bu.setAccountDb(Json::Backup::File(fileName,
	    computeFileChecksum(fi.filePath()), fi.size()));

	/* Backup zfo database. */
	if (Q_UNLIKELY(GlobInstcs::zfoDbPtr == Q_NULLPTR)) {
		return errorBackup(zipDir);
	}
	fi.setFile(GlobInstcs::zfoDbPtr->fileName());
	fileName = fi.fileName();
	emit actionTextSig(tr("Backing up file '%1'.").arg(fileName));
	QCoreApplication::processEvents();
	if (Q_UNLIKELY(!addFileIntoZip(fi, sDirPath, QString(), quaZipFile))) {
		logErrorNL("File '%s' has not been added into ZIP archive.",
		    fi.fileName().toUtf8().constData());
		return errorBackup(zipDir);
	}
	bu.setZfoDb(Json::Backup::File(fileName,
	    computeFileChecksum(fi.filePath()), fi.size()));

	/* Create and store backup JSON file into ZIP archive. */
	const QString jsonName(QStringLiteral(BACKUP_JSON));
	if (Q_UNLIKELY(!addJsonIntoZip(jsonName, bu.toJsonData(true), quaZipFile))) {
		logErrorNL("File '%s' has not been added into ZIP archive.",
		    jsonName.toUtf8().constData());
		return errorBackup(zipDir);
	}

	/* Close ZIP file. */
	zipFile.close();

	/* Show final notification. */
	emit actionTextSig(tr("Backup finished."));
	logInfoNL(
	    "All selected accounts have been successfully backed up into target location '%s'.",
	    zipFilePath.toUtf8().constData());
	QCoreApplication::processEvents();

#ifdef Q_OS_IOS
	/* Prepare iCloud transfer. */
	QString title, text, detailText;
	IosHelper::FileICloud iCloudFile;
	iCloudFile.srcFilePath = zipFilePath;
	iCloudFile.destiCloudPath = joinDirs(QStringLiteral(DATOVKA_BACK_DIR_NAME),
	    backupDirName(BRT_BACKUP, currentDateTime));
	QList<IosHelper::FileICloud> iCloudFiles;
	iCloudFiles.append(iCloudFile);
	IosHelper::storeFilesToCloud(iCloudFiles, title, text, detailText);
	emit showMessageBox(title, text, detailText);
#endif /* Q_OS_IOS */

	return true;
}

bool BackupRestoreZipData::canBackUpAccounts(const QList<AcntId> &acntIdList,
    const QString &targetPath, bool transfer)
{
	debugFuncCall();

	Q_UNUSED(targetPath);

	qint64 backupSize = JSON_FILE_SIZE_BYTES;
	qint64 targetSize = 0;

#ifdef Q_OS_IOS
	targetSize = computeAvailableSpace(GlobalSettingsQmlWrapper::dbPath());
#else /* !Q_OS_IOS */
	targetSize = (transfer ) ? computeAvailableSpace(appTransferDirPath())
	    : computeAvailableSpace(appBackupDirPath());
#endif /* Q_OS_IOS */

	emit actionTextSig("");
	QCoreApplication::processEvents();

	/* Compute transfer data or accounts data size in bytes. */
	if (transfer) {
		backupSize = dirContentSize(GlobalSettingsQmlWrapper::dbPath());
	} else {
		if (Q_UNLIKELY(GlobInstcs::accountDbPtr == Q_NULLPTR)) {
			logErrorNL("%s", "Cannot access account database.");
			Q_ASSERT(0);
			return false;
		}
		backupSize += GlobInstcs::accountDbPtr->fileSize();
		if (Q_UNLIKELY(GlobInstcs::zfoDbPtr == Q_NULLPTR)) {
			logErrorNL("%s", "Cannot access zfo database.");
			Q_ASSERT(0);
			return false;
		}
		backupSize += GlobInstcs::zfoDbPtr->fileSize();
		if (Q_UNLIKELY(acntIdList.isEmpty())) {
			sendSizeInfoToQml(backupSize, targetSize);
			return false;
		}
		foreach (const AcntId &acntId, acntIdList) {
			backupSize += getAccountBackupSizeBytes(acntId);
		}
	}

	bool canBackup = (backupSize < targetSize);
	if (Q_UNLIKELY(!canBackup && (targetSize >= 0))) {
		logErrorNL("%s", "There is not enough space in the selected storage.");
		emit actionTextSig(tr("There is not enough space in the selected storage."));
	}

	sendSizeInfoToQml(backupSize, targetSize);
	return canBackup;
}

void BackupRestoreZipData::sendSizeInfoToQml(qint64 backupSize,
    qint64 targetSize)
{
	QString infoText = tr("Required space") % QStringLiteral(": ") % sizeString(backupSize)
	    % QStringLiteral("\n") % tr("Available space") % QStringLiteral(": ") % sizeString(targetSize);
	emit sizeTextSig(infoText);
}

bool BackupRestoreZipData::isSetPin(void)
{
	if (Q_UNLIKELY(GlobInstcs::pinSetPtr == Q_NULLPTR)) {
		Q_ASSERT(0);
		return false;
	}
	return GlobInstcs::pinSetPtr->pinConfigured();
}
