/*
 * Copyright (C) 2014-2025 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

#pragma once

#include <QMap>
#include <QObject>
#include <QFileSystemWatcher>
#include <QSet>
#include <QString>

#include "src/datovka_shared/identifiers/account_id.h"

/*!
 * @brief Holds account logos in a directory as separate files.
 *
 * @note File names obey the PrefsHelper::accountIdentifier format.
 */
class AccountLogos : public QObject {
	Q_OBJECT
public:
	/*!
	 * @brief Constructor.
	 */
	AccountLogos(void);

	/*!
	 * @brief Logos need to be stored in a dedicated directory.
	 *
	 * @note Emits contentAboutToBeReset() and contentReset().
	 *
	 * @param[in] logoDirPath Directory where to hold the logos.
	 */
	bool setLogoDirPath(const QString &logoDirPath);

	/*!
	 * @brief Return logo URI.
	 *
	 * @param[in] acntId Account identifier.
	 * @return URI containing a custom logo image file or a generic logo.
	 */
	QString databoxLogoLocation(const AcntId &acntId) const;

	/*!
	 * @brief Store image file to account.
	 *
	 * @note Emits dataAboutToBeInserted(), dataInserted(), dataChanged().
	 *
	 * @param[in] acntId Account identifier.
	 * @param[in] srcPath Source image file path.
	 * @return True if success.
	 */
	bool storeDataboxLogo(const AcntId &acntId,
	    const QString &srcPath);

	/*!
	 * @brief Remove image logo file and set default image.
	 *
	 * @note Emits dataAboutToBeRemoved(), dataRemoved().
	 *
	 * @param[in] acntId Account identifier.
	 * @return True if success.
	 */
	bool removeDataboxLogo(const AcntId &acntId);

	/*!
	 * @brief Check whether file contains any image.
	 *
	 * @param[in] srcPath Source image file path.
	 * @return True if is contains an image.
	 */
	static
	bool fileContainsImage(const QString &srcPath);

Q_SIGNALS:
	/*!
	 * @brief Emitted when beginning a data reset.
	 */
	void contentAboutToBeReset(void);

	/*!
	 * @brief Emitted when a data reset finished.
	 */
	void contentReset(void);

	/*!
	 * @brief Emitted before inserting new data.
	 */
	void dataAboutToBeInserted(const AcntId &acntId);

	/*!
	 * @brief Emitted after new data inserted.
	 */
	void dataInserted(const AcntId &acntId);

	/*!
	 * @brief Emitted before removing data.
	 */
	void dataAboutToBeRemoved(const AcntId &acntId);

	/*!
	 * @brief Emitted after data removed.
	 */
	void dataRemoved(const AcntId &acntId);

	/*!
	 * @brief Emitted after existing data changed.
	 */
	void dataChanged(const AcntId &acntId);

private Q_SLOTS:
	/*!
	 * @brief Processes newly added files.
	 */
	void watchDirectoryChanged(const QString &path);

	/*!
	 * @brief Processes removed and modified files.
	 */
	void watchFileChanged(const QString &path);

private:
	QString m_logoDirPath; /*!< Path to logo directory. */

	QFileSystemWatcher m_fsWatcher; /*!< Watches file system changes. */
	QSet<QString> m_watchedLogoFiles; /*!< Set of watched logo files within logo directory. */

	QMap<AcntId, QString> m_logoFilePaths; /*!< Data-box logo locations. */
};
