/*
 * Copyright (C) 2014-2025 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

#include <QDir>
#include <QDirIterator>
#include <QFile>
#include <QFileInfo>
#include <QStringBuilder>
#include <QStringList>

#include "src/io/filesystem.h"
#include "src/settings/ini_preferences.h"

/*! Default configuration file name. */
#define DFLT_CONF_FILE "datovka.conf"

/*!
 * @brief Search for auxiliary file in specified location.
 *
 * @note The file should be created using the auxConfFilePath() function.
 *
 * @param[in] confDir Directory to search in.
 * @param[in] confFileName Normal configuration file name (without the aux suffix).
 * @return Path to the latest found file.
 */
static
QString findAuxFilePath(const QString &confDir, const QString &confFileName)
{
	QStringList auxFiles;

	{
		QDirIterator dirIt(confDir, {confFileName + ".aux.*"},
		    QDir::Files | QDir::NoDotAndDotDot,
		    QDirIterator::NoIteratorFlags);
		while (dirIt.hasNext()) {
			dirIt.next();

			auxFiles.append(dirIt.fileName());
		}
	}

	if (auxFiles.isEmpty()) {
		return QString();
	}

	/* File name contains creation time. */
	auxFiles.sort();

	/* Use last matching found. */
	return confDir % QDir::separator() % auxFiles.last();
}

bool INIPreferences::ensureConfPresence(void)
{
	{
		QDir dir(confDir());
		if (!dir.exists()) {
			if (!dir.mkpath(".")) {
				return false;
			}
		}
	}

	/* Try finding an auxiliary file and renaming it to the original. */
	if (!QFileInfo(confPath()).exists()) {
		const QString auxFilePath = findAuxFilePath(confDir(), DFLT_CONF_FILE);

		if (!auxFilePath.isEmpty()) {
			/* Rename found file. */
			QFile::rename(auxFilePath, confPath());
		}
	}

	{
		/* Create empty file if still missing. */
		QFile file(confPath());
		if (!file.exists()) {
			if (!file.open(QIODevice::ReadWrite)) {
				return false;
			}
			file.close();
		}
	}
	return true;
}

QString INIPreferences::confDir(void)
{
	return dfltDbAndConfigLoc();
}

QString INIPreferences::confPath(void)
{
	const QString settingsDirPath = confDir();

	if (settingsDirPath.isEmpty()) {
		return QString();
	}

	return settingsDirPath % QDir::separator()
	    % QStringLiteral(DFLT_CONF_FILE);
}
