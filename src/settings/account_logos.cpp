/*
 * Copyright (C) 2014-2025 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

#include <QDir>
#include <QDirIterator>
#include <QFileInfo>
#include <QImage>
#include <QImageReader>
#include <QStringBuilder>
#include <QStringList>

#include "src/datovka_shared/log/log.h"
#include "src/datovka_shared/settings/prefs_helper.h"
#include "src/settings/account_logos.h"

#define URL_DEFAULT_REGULAR "qrc:/ui/databox-regular.png"
#define URL_DEFAULT_TEST "qrc:/ui/databox-test.png"

#define MAX_LOGO_SIZE_PX 128

static const QString logoImgSuffix(".img");
static const QStringList logoImgFileFilters({"*.img"});

AccountLogos::AccountLogos(void)
    : QObject(),
    m_logoDirPath(),
    m_fsWatcher(),
    m_watchedLogoFiles(),
    m_logoFilePaths()
{
	connect(&m_fsWatcher, SIGNAL(directoryChanged(QString)),
	    this, SLOT(watchDirectoryChanged(QString)));
	connect(&m_fsWatcher, SIGNAL(fileChanged(QString)),
	    this, SLOT(watchFileChanged(QString)));
}

/*!
 * @brief Check whether image is too large.
 *
 * @pram[in] image Image to be checked for size.
 * @return True if image is too large.
 */
static
bool imageNeedsResizing(const QImage &image)
{
	const QSize imgSize = image.size();
	return (imgSize.width() > MAX_LOGO_SIZE_PX)
	    || (imgSize.height() > MAX_LOGO_SIZE_PX);
}

/*!
 * @brief Check whether image is too large.
 *
 * @param[in] srcPath Source image file path.
 * @return True if image is too large.
 */
static
bool imageFileNeedsResising(const QString &srcPath)
{
	return imageNeedsResizing(QImage(srcPath));
}

/*!
 * @brief Copy and resize source logo image if needed and save to target location.
 *
 * @param[in] srcPath Source image file path.
 * @param[in] tgtPath Target image file path.
 * @return True if success.
 */
static
bool resizeAndCopyImageFile(const QString &srcPath, const QString &tgtPath)
{
	if (Q_UNLIKELY(srcPath.isEmpty() || tgtPath.isEmpty())) {
		return false;
	}

	/* Get image format. */
	const QByteArray format = QImageReader(srcPath).format().toLower();
	/* Resize and copy. */
	QImage img(srcPath);
	if (imageNeedsResizing(img)) {
		img = img.scaled(MAX_LOGO_SIZE_PX, MAX_LOGO_SIZE_PX,
		    Qt::KeepAspectRatio);
		logInfoNL("Logo image has been scaled: '%s'.",
		    srcPath.toUtf8().constData());
	}
	return img.save(tgtPath, format);
}

/*!
 * @brief Loads all suitable files from \a logoDirPath into \a logoLocations.
 *
 * @note It also resizes images and deletes files with unsupported content.
 *
 * @param[in]  logoDirPath Path to logo directory.
 * @param[out] logoLocations Maps account identifies onto logo file paths.
 * @return True if directory exists and is readable.
 */
static
bool reloadLogos(const QString &logoDirPath, QMap<AcntId, QString> &logoLocations)
{
	logoLocations.clear();

	{
		const QFileInfo fi(logoDirPath);
		if (Q_UNLIKELY((!fi.exists()) || (!fi.isDir()) || (!fi.isReadable()))) {
			/* Do nothing. */
			return true;
		}
	}

	QStringList filePathList;

	QDirIterator it(logoDirPath, logoImgFileFilters, QDir::Files,
	    QDirIterator::NoIteratorFlags);
	while (it.hasNext()) {
		QString filePath = it.next();
		const QString fileBaseName = QFileInfo(filePath).baseName();
		QString username;
		bool testing = false;
		if (PrefsHelper::accountIdentifierData(fileBaseName, username, testing)) {
			/* Delete if no image contained. */
			if (Q_UNLIKELY(!AccountLogos::fileContainsImage(filePath))) {
				logInfoNL(
				    "Deleting file with non-image content: '%s'",
				    filePath.toUtf8().constData());
				QFile::remove(filePath);
				continue;
			}

			/* Legitimate logo file -- check dimensions. */
			if (imageFileNeedsResising(filePath)) {
				logInfoNL("Image in file needs resizing: '%s'",
				    filePath.toUtf8().constData());

				if (Q_UNLIKELY(!resizeAndCopyImageFile(filePath, filePath))) {
					logWarningNL(
					    "Image couldn't be resized, deleting: '%s'",
					    filePath.toUtf8().constData());
					QFile::remove(filePath);
					continue;
				}
			}

			logoLocations[AcntId(username, testing)] = filePath;
		} else {
			/* Any other file. */
			logWarningNL("Unsupported file name format: '%s'",
			    filePath.toUtf8().constData());
		}
	}

	return true;
}

bool AccountLogos::setLogoDirPath(const QString &logoDirPath)
{
	if (Q_UNLIKELY(logoDirPath.isEmpty())) {
		/* Must be set to something. */
		Q_ASSERT(0);
		return false;
	}

	if (Q_UNLIKELY(m_logoDirPath == logoDirPath)) {
		/* Do nothing, already set. */
		return true;
	}

	/* Create the directory if missing. */
	{
		QFileInfo fi(logoDirPath);
		if (!fi.exists()) {
			QDir(logoDirPath).mkpath(".");
		}

		if (!fi.exists()) {
			return false;
		}
	}

	/* Forget about any previously watched content. */
	if (!m_logoDirPath.isEmpty()) {
		m_fsWatcher.removePath(m_logoDirPath);
	}
	{
		const QStringList watchedFiles = m_fsWatcher.files();
		if (!watchedFiles.isEmpty()) {
			m_fsWatcher.removePaths(m_fsWatcher.files());
		}
	}
	m_watchedLogoFiles.clear();

	Q_EMIT contentAboutToBeReset();

	m_logoDirPath = logoDirPath;

	reloadLogos(m_logoDirPath, m_logoFilePaths);

	Q_EMIT contentReset();

	/* Start watching new content. */
	m_fsWatcher.addPath(m_logoDirPath);
	for (const QString &filePath : m_logoFilePaths) {
		m_fsWatcher.addPath(filePath);
		m_watchedLogoFiles.insert(filePath);
	}

	return true;
}

/*!
 * @brief Return default data-box logo URL.
 *
 * @param[in] testing True if account is in ISDS test environment.
 * @return Default data-box logo URL.
 */
static
const QString &defaultLogo(bool testing)
{
	static const QString urlRegular(URL_DEFAULT_REGULAR);
	static const QString urlTest(URL_DEFAULT_TEST);

	return (testing) ? urlTest : urlRegular;
}

/*!
 * @brief Construct data-box logo file name.
 *
 * @param[in] acntId Account identifier.
 * @return Data-box logo file name.
 */
static
QString databoxLogoFileName(const AcntId &acntId)
{
	return PrefsHelper::accountIdentifier(acntId.username(), acntId.testing())
	    + logoImgSuffix;
}

/*!
 * @brief Construct data-box logo file path.
 *
 * @param[in] acntId Account identifier.
 * @return Data-box logo file path.
 */
static
QString databoxLogoFilePath(const QString &dirPath, const AcntId &acntId)
{
	return dirPath + QDir::separator() + databoxLogoFileName(acntId);
}

QString AccountLogos::databoxLogoLocation(const AcntId &acntId) const
{
	static const QString urlFilePrefix("file:///");

	const QMap<AcntId, QString>::const_iterator cit =
	    m_logoFilePaths.find(acntId);
	if (cit == m_logoFilePaths.constEnd()) {
		/* No logo held, return default. */
		return defaultLogo(acntId.testing());
	}

	const QFileInfo fi(cit.value());
	if (Q_UNLIKELY(!fi.exists())) {
		/* File non-existent, return default. */
		return defaultLogo(acntId.testing());
	}


	return urlFilePrefix + cit.value();
}

bool AccountLogos::storeDataboxLogo(const AcntId &acntId,
    const QString &srcPath)
{
	const bool alreadyContained = m_logoFilePaths.contains(acntId);
	{
		const QString filePath = m_logoFilePaths.value(acntId);
		QFileInfo fi(filePath);
		if (fi.exists() && ((!fi.isFile()) || (!fi.isWritable()))) {
			/* Cannot delete existing file/directory. */
			return false;
		}
	}

	const QString tgtFilePath = databoxLogoFilePath(m_logoDirPath, acntId);
	const QString tmpFilePath = tgtFilePath + QStringLiteral(".aux");

	if (Q_UNLIKELY(!fileContainsImage(srcPath))) {
		return false;
	}

	/* Prepare auxiliary image file. */
	if (Q_UNLIKELY(!resizeAndCopyImageFile(srcPath, tmpFilePath))) {
		return false;
	}

	/*
	 * Temporarily stop watching this path as the code deals with
	 * the change here.
	 */
	m_fsWatcher.removePath(m_logoDirPath);

	if (alreadyContained) {
		m_logoFilePaths.remove(acntId);
		QFile::remove(tgtFilePath);
	} else {
		Q_EMIT dataAboutToBeInserted(acntId);
	}

	QFile::rename(tmpFilePath, tgtFilePath);
	m_logoFilePaths[acntId] = tgtFilePath;

	/* Resume watching. */
	m_fsWatcher.addPath(m_logoDirPath);

	m_fsWatcher.addPath(tgtFilePath);
	m_watchedLogoFiles.insert(tgtFilePath);

	if (alreadyContained) {
		Q_EMIT dataChanged(acntId);
	} else {
		Q_EMIT dataInserted(acntId);
	}

	return true;
}

bool AccountLogos::removeDataboxLogo(const AcntId &acntId)
{
	if (Q_UNLIKELY(!m_logoFilePaths.contains(acntId))) {
		return true;
	}

	const QString filePath = m_logoFilePaths.value(acntId);
	{
		QFileInfo fi(filePath);
		if (fi.exists() && ((!fi.isFile()) || (!fi.isWritable()))) {
			/* Cannot delete existing file/directory. */
			return false;
		}
	}

	m_watchedLogoFiles.remove(filePath);
	m_fsWatcher.removePath(filePath);

	/*
	 * Temporarily stop watching this path as the code deals with
	 * the change here.
	 */
	m_fsWatcher.removePath(m_logoDirPath);

	Q_EMIT dataAboutToBeRemoved(acntId);

	m_logoFilePaths.remove(acntId);
	QFile::remove(filePath);

	/* Resume watching. */
	m_fsWatcher.addPath(m_logoDirPath);

	Q_EMIT dataRemoved(acntId);

	return true;
}

bool AccountLogos::fileContainsImage(const QString &srcPath)
{
	const QImage::Format format = QImageReader(srcPath).imageFormat();
	return (QImage::Format_Invalid != format);
}

/*!
 * @brief Convert file map to set of paths.
 *
 * @param[in] map Map of file paths.
 * @return Set of file paths.
 */
static
QSet<QString> fileMapToSet(const QMap<AcntId, QString> &map)
{
	QSet<QString> contentSet;

	for (const QString &value : map) {
		contentSet.insert(value);
	}

	return contentSet;
}

void AccountLogos::watchDirectoryChanged(const QString &path)
{
	logInfoNL("Detected change of watched directory: '%s'",
	    path.toUtf8().constData());

	QMap<AcntId, QString> newLogoFilePaths;

	reloadLogos(path, newLogoFilePaths);

	QSet<QString> newLogoFiles = fileMapToSet(newLogoFilePaths);

	/*
	 * Just check added files.
	 * Watched file removal is handled by watchFileChanged().
	 */
	newLogoFiles.subtract(m_watchedLogoFiles);

	if (!newLogoFiles.isEmpty()) {
		for (const QString &filePath : newLogoFiles) {
			const QString fileBaseName = QFileInfo(filePath).baseName();
			QString username;
			bool testing = false;
			if (PrefsHelper::accountIdentifierData(fileBaseName, username, testing)) {
				logInfoNL("Detected new image file: '%s'",
				    filePath.toUtf8().constData());

				/*
				 * No additional check here as they have been
				 * done by reloadLogos().
				 */

				const AcntId acntId(username, testing);

				Q_EMIT dataAboutToBeInserted(acntId);

				m_fsWatcher.addPath(filePath);
				m_watchedLogoFiles.insert(filePath);

				m_logoFilePaths[acntId] = filePath;

				Q_EMIT dataInserted(acntId);
			} else {
				Q_ASSERT(0);
			}
		}
	}
}

void AccountLogos::watchFileChanged(const QString &path)
{
	logInfoNL("Detected change of watched file: '%s'",
	    path.toUtf8().constData());

	const QFileInfo fi(path);
	AcntId acntId;
	{
		const QString fileBaseName = fi.baseName();
		QString username;
		bool testing = false;
		if (PrefsHelper::accountIdentifierData(fileBaseName, username, testing)) {
			acntId = AcntId(username, testing);
		} else {
			Q_ASSERT(0);
		}
	}

	if (Q_UNLIKELY(!acntId.isValid())) {
		Q_ASSERT(0);
		return;
	}

	if (!fi.exists()) {
		/* File has been deleted. */

		Q_EMIT dataAboutToBeRemoved(acntId);

		m_fsWatcher.removePath(path);
		m_watchedLogoFiles.remove(path);

		m_logoFilePaths.remove(acntId);

		Q_EMIT dataRemoved(acntId);
	} else {
		/* File has been modified. */

		/* Delete if no image contained. */
		if (Q_UNLIKELY(!fileContainsImage(path))) {
			logInfoNL(
			    "Deleting file with non-image content: '%s'",
			    path.toUtf8().constData());
			QFile::remove(path);
			return;
		}

		/* Legitimate logo file -- check dimensions. */
		if (imageFileNeedsResising(path)) {
			logInfoNL("Image in file needs resizing: '%s'",
			    path.toUtf8().constData());

			if (Q_UNLIKELY(!resizeAndCopyImageFile(path, path))) {
				logWarningNL(
				    "Image couldn't be resized, deleting: '%s'",
				    path.toUtf8().constData());
				QFile::remove(path);
				return;
			}
		}

		Q_EMIT dataChanged(acntId);
	}
}
