/*
 * Copyright (C) 2014-2025 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

#pragma once

#include <QString>

/*!
 * @brief Encapsulates INI-style preferences.
 */
class INIPreferences {
public:
	/*!
	 * @brief Create configuration file if not present.
	 */
	static
	bool ensureConfPresence(void);

	/*!
	 * @brief Return path to configuration directory.
	 *
	 * @return Path to configuration directory.
	 */
	static
	QString confDir(void);

	/*!
	 * @brief Returns whole configuration file path.
	 *
	 * @return Whole path to configuration file.
	 */
	static
	QString confPath(void);
};
