/*
 * Copyright (C) 2014-2025 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

#include <QFileInfo>
#include <QMetaMethod>
#include <QSettings>
#include <QString>

#include "src/datovka_shared/log/log.h"
#include "src/datovka_shared/settings/pin.h"
#include "src/datovka_shared/settings/records_management.h"
#include "src/global.h"
#include "src/io/filesystem.h"
#include "src/settings/accounts.h"
#include "src/settings/ini_preferences.h"
#include "src/settings/ini_preferences_watcher.h"

INIPreferencesWatcher::INIPreferencesWatcher(void)
    : QObject(Q_NULLPTR)
{
	connect(GlobInstcs::pinSetPtr, SIGNAL(contentChanged()),
	    this, SLOT(watchSettingsChange()));

	/* Mobile application doesn't support tags for messages. */

	connect(GlobInstcs::acntMapPtr, SIGNAL(accountAdded(AcntId, int)),
	    this, SLOT(watchSettingsChange()));
	connect(GlobInstcs::acntMapPtr, SIGNAL(accountRemoved(AcntId, int)),
	    this, SLOT(watchSettingsChange()));
	connect(GlobInstcs::acntMapPtr, SIGNAL(accountsMoved(int, int, int)),
	    this, SLOT(watchSettingsChange()));
	connect(GlobInstcs::acntMapPtr, SIGNAL(accountUsernameChanged(AcntId, AcntId, int)),
	    this, SLOT(watchSettingsChange()));
	connect(GlobInstcs::acntMapPtr, SIGNAL(accountDataChanged(AcntId)),
	    this, SLOT(watchSettingsChange()));
	connect(GlobInstcs::acntMapPtr, SIGNAL(contentChanged()), /* Cannot use contentReset() here. */
	    this, SLOT(watchSettingsChange()));

	connect(GlobInstcs::recMgmtSetPtr, SIGNAL(contentChanged()),
	    this, SLOT(watchSettingsChange()));
}

/*!
 * @brief Store configuration file version.
 *
 * @param[in,out] settings Settings to be updated.
 */
static
void saveAppIdConfigFormat(QSettings &settings)
{
	settings.beginGroup("version");
	settings.setValue("config_format", 1);
	settings.endGroup();
}

/*!
 * @brief Store configuration into INI file.
 *
 * @param[in] filePath Path to file.
 * @return True is configuration was written.
 */
static
bool saveINISettings(const QString &filePath)
{
	{
		/*
		 * TODO -- Target file name differs from source for testing purposes.
		 */
		QSettings settings(filePath, QSettings::IniFormat);
#if (QT_VERSION >= QT_VERSION_CHECK(6, 0, 0))
		/*
		 * Qt-6 does not provide a replacement of QSettings::setIniCodec().
		 * The documentation claims that QSettings assumes the INI file
		 * is UTF-8 encoded.
		 * TODO - It would be great to make really sure the INI is UTF-8 encoded
		 * instead of relying on the developers and documentation.
		 */
#else /* < Qt-6.0 */
		settings.setIniCodec("UTF-8");
#endif /* >= Qt-6.0 */

		if (Q_UNLIKELY(QSettings::NoError != settings.status())) {
			return false;
		}

		settings.clear();

		/* Store application ID and config format. */
		saveAppIdConfigFormat(settings);

		/* PIN settings. */
		GlobInstcs::pinSetPtr->saveToSettings(settings);

		/* Mobile application doesn't support tags for messages. */

		/* Accounts. */
		/* Configuration directory is always unspecified in mobile app. */
		GlobInstcs::acntMapPtr->saveToSettings(
		    GlobInstcs::pinSetPtr->pinValue(), QString(), settings);

		/* Records management settings. */
		GlobInstcs::recMgmtSetPtr->saveToSettings(
		    GlobInstcs::pinSetPtr->pinValue(), settings);

		/* No global preferences are stored into INI file. */

		settings.sync();

		if (Q_UNLIKELY(QSettings::NoError != settings.status())) {
			return false;
		}
	}
	/* Remove '"' symbols from passwords in INI file. */
	/* TODO ??? */

	return true;
}

/*!
 * @brief Store configuration into INI file.
 */
static
bool saveINIFile(void)
{
	const QString filePath = INIPreferences::confPath();
	const QString tmpFilePath = auxConfFilePath(filePath);

	return QFileInfo(filePath).isWritable()
	   && saveINISettings(tmpFilePath)
	   && QFile::remove(filePath)
	   && QFile::rename(tmpFilePath, filePath);
}

void INIPreferencesWatcher::watchSettingsChange(void)
{
	if (meetsDebugLv1()) {
		const QObject *senderObj = QObject::sender();
		const int signalIndex = QObject::senderSignalIndex();
		const QMetaObject *metaObj = Q_NULLPTR;
		if (Q_NULLPTR != senderObj) {
			metaObj = senderObj->metaObject();
		}
		if (Q_NULLPTR != metaObj) {
			logDebugLv1NL("<SLOT> %s() called by signal %s()",
			    __func__,
			    metaObj->method(signalIndex).name().constData());
		}
	}
	if (saveINIFile()) {
		logDebugLv0NL("%s", "Saved INI settings.");
	} else {
		Q_EMIT fileWriteError(INIPreferences::confPath());
	}
}
