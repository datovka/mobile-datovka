/*
 * Copyright (C) 2014-2025 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

#include <QRegularExpression>

#include "src/datovka_shared/settings/prefs.h"
#include "src/datovka_shared/settings/prefs_helper.h"
#include "src/settings/account.h"
#include "src/settings/prefs_specific.h"

bool PrefsSpecific::dataOnDisk(Prefs &prefs, const AcntData &acnt)
{
	bool val = true; /* Use true as default. */

	const QString acntId = PrefsHelper::accountIdentifier(
	   acnt.userName(), acnt.isTestAccount());

	if (Q_UNLIKELY(!prefs.boolVal(
	        "account." + acntId + ".storage.databases.on_disk.enabled", val))) {
		/*
		 * Set the missing value to prevent the code to generate errors.
		 */
		setDataOnDisk(prefs, acnt, val);
	}
	return val;
}

void PrefsSpecific::setDataOnDisk(Prefs &prefs, const AcntData &acnt, bool val)
{
	const QString acntId = PrefsHelper::accountIdentifier(
	   acnt.userName(), acnt.isTestAccount());

	prefs.setBoolVal(
	    "account." + acntId + ".storage.databases.on_disk.enabled", val);
}

/*!
 * @brief Returns list of group matches from \a wholeKey that match the
 *     asterisk occurrence in \a asteriskPattern.
 *
 * @note Example: Returns {"a", "b"} when searching for "c.*.d.*.e" in "c.a.d.b.e".
 *    It searches using the regular expression "^c\\.(.*)\\.d\\.(.*)\\.e$".
 *
 * @param[in] wholeKey The key to search for.
 * @param[in] ateriskPattern Sought pattern.
 * @return List of asterisk group matches.
 */
static
QStringList extractAsteriskData(const QString &wholeKey, const QString &asteriskPattern)
{
	QString regExpStr = asteriskPattern;
	regExpStr.replace(".", "\\.");
	regExpStr.replace("*", "(.*)");
	regExpStr = QString("^%1$").arg(regExpStr);

	QRegularExpression re(regExpStr);

	QRegularExpressionMatch match = re.match(wholeKey);
	QStringList capturedTexts = match.capturedTexts();
	if (capturedTexts.size() > 1) {
		/* The first entry contains the entire match. */
		capturedTexts.removeAt(0);
	}

	return capturedTexts;
}

AcntId PrefsSpecific::accntIdFromDataOnDiskKey(const QString &key)
{
	QStringList keyParts = extractAsteriskData(key,
	    "account.*.storage.databases.on_disk.enabled");

	QString username;
	bool testing = false;
	if ((keyParts.size() == 1)
	        && PrefsHelper::accountIdentifierData(keyParts.at(0), username, testing)) {
		return AcntId(username, testing);
	}

	return AcntId();
}
