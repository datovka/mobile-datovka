/*
 * Copyright (C) 2014-2025 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

#include <QApplication>
#include <QFile>
#include <QPair>
#include <QQmlEngine> /* qmlRegisterType */
#include <QStorageInfo>

#include "src/datovka_shared/html/html_export.h"
#include "src/datovka_shared/io/records_management_db.h"
#include "src/datovka_shared/log/log.h"
#include "src/datovka_shared/worker/pool.h"
#include "src/global.h"
#include "src/io/filesystem.h"
#include "src/messages.h"
#include "src/models/accountmodel.h"
#include "src/models/databoxmodel.h"
#include "src/models/messagemodel.h"
#include "src/settings/account.h"
#include "src/settings/accounts.h"
#include "src/settings/prefs_specific.h"
#include "src/setwrapper.h"
#include "src/sqlite/file_db_container.h"
#include "src/sqlite/file_db.h"
#include "src/sqlite/message_db_container.h"
#include "src/sqlite/message_db.h"

/*!
 * @brief Translate message type enumeration type into database representation.
 */
static
enum MessageDb::MessageType enumToDbRepr(enum Messages::MessageType msgType)
{
	switch (msgType) {
	case Messages::TYPE_RECEIVED:
		return MessageDb::TYPE_RECEIVED;
		break;
	case Messages::TYPE_SENT:
		return MessageDb::TYPE_SENT;
		break;
	default:
		/*
		 * Any other translates to sent, but forces the application to
		 * crash in debugging mode.
		 */
		Q_ASSERT(0);
		return MessageDb::TYPE_SENT;
		break;
	}
}

void Messages::declareQML(void)
{
	qmlRegisterUncreatableType<Messages>("cz.nic.mobileDatovka.messages", 1, 0, "MessageType", "Access to enums & flags only.");
	qRegisterMetaType<Messages::MessageType>("Messages::MessageType");
	qRegisterMetaType<Messages::MessageTypes>("Messages::MessageTypes");
}

Messages::Messages(QObject *parent) : QObject(parent)
{
}

void Messages::fillContactList(DataboxListModel *databoxModel,
    const QmlAcntId *qAcntId, const QString &dbId)
{
	debugFuncCall();

	if (Q_UNLIKELY((GlobInstcs::messageDbsPtr == Q_NULLPTR) ||
	        (GlobInstcs::acntMapPtr == Q_NULLPTR) ||
	        (qAcntId == Q_NULLPTR))) {
		Q_ASSERT(0);
		return;
	}

	if (Q_UNLIKELY(databoxModel == Q_NULLPTR)) {
		logErrorNL("%s", "Cannot access data box model.");
		Q_ASSERT(0);
		return;
	}

	MessageDb *msgDb = GlobInstcs::messageDbsPtr->accessMessageDb(
	    GlobalSettingsQmlWrapper::dbPath(), *qAcntId,
	    PrefsSpecific::dataOnDisk(
	        *GlobInstcs::prefsPtr, GlobInstcs::acntMapPtr->acntData(*qAcntId)));
	if (Q_UNLIKELY(msgDb == Q_NULLPTR)) {
		logErrorNL("%s", "Cannot access message database.");
		return;
	}

	databoxModel->clearAll();
	msgDb->getContactsFromDb(databoxModel, dbId);
}

void Messages::fillMessageList(MessageListModel *messageModel,
    const QmlAcntId *qAcntId, enum MessageType msgType)
{
	debugFuncCall();

	if (Q_UNLIKELY((GlobInstcs::messageDbsPtr == Q_NULLPTR) ||
	        (GlobInstcs::acntMapPtr == Q_NULLPTR) ||
	        (qAcntId == Q_NULLPTR))) {
		Q_ASSERT(0);
		return;
	}

	if (Q_UNLIKELY(messageModel == Q_NULLPTR)) {
		logErrorNL("%s", "Cannot access message model.");
		Q_ASSERT(0);
		return;
	}

	if (Q_UNLIKELY(qAcntId == Q_NULLPTR)) {
		Q_ASSERT(0);
		return;
	}

	MessageDb *msgDb = GlobInstcs::messageDbsPtr->accessMessageDb(
	    GlobalSettingsQmlWrapper::dbPath(), *qAcntId,
	    PrefsSpecific::dataOnDisk(
	        *GlobInstcs::prefsPtr, GlobInstcs::acntMapPtr->acntData(*qAcntId)));
	if (Q_UNLIKELY(msgDb == Q_NULLPTR)) {
		logErrorNL("%s", "Cannot access message database.");
		return;
	}

	messageModel->clearAll();
	/* Translate into database enum type. */
	msgDb->setMessageListModelFromDb(messageModel, *qAcntId,
	    enumToDbRepr(msgType));
}

QString Messages::getMessageDetail(const QmlAcntId *qAcntId,
    const QString &msgIdStr, enum MessageType msgType, bool shortVersion)
{
	debugFuncCall();

	if (Q_UNLIKELY((GlobInstcs::messageDbsPtr == Q_NULLPTR) ||
	        (GlobInstcs::acntMapPtr == Q_NULLPTR) ||
	        (GlobInstcs::recMgmtDbPtr == Q_NULLPTR) ||
	        (qAcntId == Q_NULLPTR))) {
		Q_ASSERT(0);
		return QString();
	}

	bool ok = false;
	qint64 msgId = msgIdStr.toLongLong(&ok);
	if (Q_UNLIKELY(!ok || (msgId < 0))) {
		return QString();
	}

	MessageDb *msgDb = GlobInstcs::messageDbsPtr->accessMessageDb(
	    GlobalSettingsQmlWrapper::dbPath(), *qAcntId,
	    PrefsSpecific::dataOnDisk(
	        *GlobInstcs::prefsPtr, GlobInstcs::acntMapPtr->acntData(*qAcntId)));
	if (Q_UNLIKELY(msgDb == Q_NULLPTR)) {
		logErrorNL("%s", "Cannot access message database.");
		return QString();
	}

	return Html::Export::htmlMessageInfoApp(
	    msgDb->getMessageEnvelopeFromDb(msgId),
	    msgDb->messageAuthorJsonStr(msgId),
	    GlobInstcs::recMgmtDbPtr->storedMsgLocations(msgId),
	    msgType == TYPE_RECEIVED, shortVersion, false);
}

QString Messages::getShortMessageDetail(const QmlAcntId *qAcntId,
    const QString &msgIdStr, enum MessageType msgType)
{
	debugFuncCall();

	if (Q_UNLIKELY((GlobInstcs::messageDbsPtr == Q_NULLPTR) ||
	        (GlobInstcs::acntMapPtr == Q_NULLPTR) ||
	        (qAcntId == Q_NULLPTR))) {
		Q_ASSERT(0);
		return QString();
	}

	bool ok = false;
	qint64 msgId = msgIdStr.toLongLong(&ok);
	if (Q_UNLIKELY(!ok || (msgId < 0))) {
		return QString();
	}

	MessageDb *msgDb = GlobInstcs::messageDbsPtr->accessMessageDb(
	    GlobalSettingsQmlWrapper::dbPath(), *qAcntId,
	    PrefsSpecific::dataOnDisk(
	        *GlobInstcs::prefsPtr, GlobInstcs::acntMapPtr->acntData(*qAcntId)));
	if (Q_UNLIKELY(msgDb == Q_NULLPTR)) {
		logErrorNL("%s", "Cannot access message database.");
		return QString();
	}

	return Html::Export::htmlShortMessageInfoApp(
	    msgDb->getMessageEnvelopeFromDb(msgId), msgType == TYPE_RECEIVED);
}

QString Messages::getDeliveryInfoDetail(const QmlAcntId *qAcntId,
    const QString &msgIdStr)
{
	debugFuncCall();

	if (Q_UNLIKELY((GlobInstcs::messageDbsPtr == Q_NULLPTR) ||
	        (GlobInstcs::acntMapPtr == Q_NULLPTR) ||
	        (qAcntId == Q_NULLPTR))) {
		Q_ASSERT(0);
		return QString();
	}

	bool ok = false;
	qint64 msgId = msgIdStr.toLongLong(&ok);
	if (Q_UNLIKELY(!ok || (msgId < 0))) {
		return QString();
	}

	MessageDb *msgDb = GlobInstcs::messageDbsPtr->accessMessageDb(
	    GlobalSettingsQmlWrapper::dbPath(), *qAcntId,
	    PrefsSpecific::dataOnDisk(
	        *GlobInstcs::prefsPtr, GlobInstcs::acntMapPtr->acntData(*qAcntId)));
	if (Q_UNLIKELY(msgDb == Q_NULLPTR)) {
		logErrorNL("%s", "Cannot access message database.");
		return QString();
	}

	return Html::Export::htmlDeliveryInfoApp(
	    msgDb->getMessageEnvelopeFromDb(msgId),
	    msgDb->messageAuthorJsonStr(msgId));
}

QmlIsdsEnvelope *Messages::getMsgEnvelopeDataAndSetRecipient(
    const QmlAcntId *qAcntId, qint64 msgId)
{
	debugFuncCall();

	if (Q_UNLIKELY((GlobInstcs::messageDbsPtr == Q_NULLPTR) ||
	        (GlobInstcs::acntMapPtr == Q_NULLPTR) ||
	        (qAcntId == Q_NULLPTR))) {
		Q_ASSERT(0);
		return Q_NULLPTR;
	}

	MessageDb *msgDb = GlobInstcs::messageDbsPtr->accessMessageDb(
	    GlobalSettingsQmlWrapper::dbPath(), *qAcntId,
	    PrefsSpecific::dataOnDisk(
	        *GlobInstcs::prefsPtr, GlobInstcs::acntMapPtr->acntData(*qAcntId)));
	if (Q_UNLIKELY(msgDb == Q_NULLPTR)) {
		logErrorNL("%s", "Cannot access message database.");
		return Q_NULLPTR;
	}

	Isds::Envelope envelope = msgDb->getMessageEnvelopeFromDb(msgId);

	// Return message envelope data into QML
	return new (::std::nothrow) QmlIsdsEnvelope(envelope);
}

void Messages::markMessageAsLocallyRead(MessageListModel *messageModel,
    const QmlAcntId *qAcntId, qint64 msgId, bool isRead)
{
	debugFuncCall();

	if (Q_UNLIKELY((GlobInstcs::messageDbsPtr == Q_NULLPTR) ||
	        (GlobInstcs::acntMapPtr == Q_NULLPTR) ||
	        (qAcntId == Q_NULLPTR))) {
		Q_ASSERT(0);
		return;
	}

	MessageDb *msgDb = GlobInstcs::messageDbsPtr->accessMessageDb(
	    GlobalSettingsQmlWrapper::dbPath(), *qAcntId,
	    PrefsSpecific::dataOnDisk(
	        *GlobInstcs::prefsPtr, GlobInstcs::acntMapPtr->acntData(*qAcntId)));
	if (Q_UNLIKELY(msgDb == Q_NULLPTR)) {
		logErrorNL("%s", "Cannot access message database.");
		return;
	}
	msgDb->markMessageLocallyRead(msgId, isRead);

	if (Q_UNLIKELY(messageModel == Q_NULLPTR)) {
		logWarningNL("%s", "Cannot access message model.");
		return;
	}
	messageModel->overrideRead(msgId, isRead);
}

void Messages::markMessagesAsLocallyRead(MessageListModel *messageModel,
    const QmlAcntId *qAcntId, enum MessageType msgType, bool isRead)
{
	debugFuncCall();

	if (Q_UNLIKELY((GlobInstcs::messageDbsPtr == Q_NULLPTR) ||
	        (GlobInstcs::acntMapPtr == Q_NULLPTR) ||
	        (qAcntId == Q_NULLPTR))) {
		Q_ASSERT(0);
		return;
	}

	MessageDb *msgDb = GlobInstcs::messageDbsPtr->accessMessageDb(
	    GlobalSettingsQmlWrapper::dbPath(), *qAcntId,
	    PrefsSpecific::dataOnDisk(
	        *GlobInstcs::prefsPtr, GlobInstcs::acntMapPtr->acntData(*qAcntId)));
	if (Q_UNLIKELY(msgDb == Q_NULLPTR)) {
		logErrorNL("%s", "Cannot access message database.");
		return;
	}
	msgDb->markMessagesLocallyRead(enumToDbRepr(msgType), isRead);

	if (Q_UNLIKELY(messageModel == Q_NULLPTR)) {
		logWarningNL("%s", "Cannot access message model.");
		return;
	}
	/* The current model should correspond with supplied type. */
	messageModel->overrideReadAll(isRead);
}

void Messages::overrideDownloaded(MessageListModel *messageModel,
    qint64 dmId, bool forceDownloaded)
{
	if (Q_UNLIKELY(messageModel == Q_NULLPTR)) {
		logErrorNL("%s", "Cannot access message model.");
		Q_ASSERT(0);
		return;
	}

	messageModel->overrideDownloaded(dmId, forceDownloaded);
}

void Messages::updateRmStatus(MessageListModel *messageModel,
    const QmlAcntId *qAcntId, qint64 dmId, enum MessageType msgType,
    bool isUploadRm)
{
	debugFuncCall();

	if (Q_UNLIKELY(qAcntId == Q_NULLPTR)) {
		Q_ASSERT(0);
		return;
	}

	if (Q_UNLIKELY(messageModel == Q_NULLPTR)) {
		logWarningNL("%s", "Cannot access message model.");
		return;
	}

	messageModel->updateRmStatus(dmId, isUploadRm);
	emit updateMessageDetail(getMessageDetail(qAcntId,
	    QString::number(dmId), msgType, true));
}

void Messages::deleteMessageFromDbs(AccountListModel *accountModel,
    MessageListModel *messageModel, const QmlAcntId *qAcntId, qint64 msgId)
{
	debugFuncCall();

	if (Q_UNLIKELY((GlobInstcs::messageDbsPtr == Q_NULLPTR) ||
	        (GlobInstcs::fileDbsPtr == Q_NULLPTR) ||
	        (GlobInstcs::acntMapPtr == Q_NULLPTR) ||
	        (qAcntId == Q_NULLPTR))) {
		Q_ASSERT(0);
		return;
	}

	if (Q_UNLIKELY(!qAcntId->isValid())) {
		return;
	}

	if (Q_UNLIKELY(!GlobInstcs::acntMapPtr->acntIds().contains(*qAcntId))) {
		return;
	}

	const bool dataOnDisk = PrefsSpecific::dataOnDisk(*GlobInstcs::prefsPtr,
	    GlobInstcs::acntMapPtr->acntData(*qAcntId));

	MessageDb *msgDb = GlobInstcs::messageDbsPtr->accessMessageDb(
	    GlobalSettingsQmlWrapper::dbPath(), *qAcntId, dataOnDisk);
	if (Q_UNLIKELY(msgDb == Q_NULLPTR)) {
		logErrorNL("%s", "Cannot access message database.");
		return;
	}

	QDir dir(getZfoPath(qAcntId->username(), qAcntId->testing(), msgId));
	if (Q_UNLIKELY(!dir.removeRecursively())) {
		logWarningNL("Cannot remove zfo files from storage for %s.",
		    qAcntId->username().toUtf8().constData());
	} else {

		FileDb *fDb = GlobInstcs::fileDbsPtr->accessFileDb(
		    GlobalSettingsQmlWrapper::dbPath(), qAcntId->username(),
		    dataOnDisk);
		if (Q_UNLIKELY(fDb == Q_NULLPTR)) {
			logErrorNL("Cannot access file database for %s.",
			    qAcntId->username().toUtf8().constData());
			return;
		}

		if (Q_UNLIKELY(!fDb->deleteFilesFromDb(msgId))) {
			logWarningNL("Cannot delete files from database for %s.",
			    qAcntId->username().toUtf8().constData());
			return;
		}
	}

	if (Q_UNLIKELY(accountModel == Q_NULLPTR)) {
		logWarningNL("%s", "Cannot access account model.");
	}

	if (Q_UNLIKELY(messageModel == Q_NULLPTR)) {
		logWarningNL("%s", "Cannot access message model.");
	}

	if (msgDb->deleteMessageFromDb(msgId, true)) {
		/* Remove row from model, don't regenerate data. */
		if (accountModel != Q_NULLPTR) {
			accountModel->updateCounters(*qAcntId,
			    msgDb->getNewMessageCountFromDb(MessageDb::TYPE_RECEIVED),
			    msgDb->getMessageCountFromDb(MessageDb::TYPE_RECEIVED),
			    msgDb->getNewMessageCountFromDb(MessageDb::TYPE_SENT),
			    msgDb->getMessageCountFromDb(MessageDb::TYPE_SENT));
		}
		if (messageModel != Q_NULLPTR) {
			messageModel->removeMessage(msgId);
		}
	}
}

bool Messages::moveOrCreateNewDbsToNewLocation(const QString &newLocation)
{
	debugFuncCall();

	int allDbSize = 0;

	/* Select action what to do with the currently used databases. */
	enum ReloactionAction action = askAction();
	switch (action) {
	case RA_NONE:
		/* Do nothing and exit. */
		return false;
		break;
	case RA_RELOCATE:
		allDbSize = getAllDbSize();
		break;
	case RA_CREATE_NEW:
		allDbSize = estimateAllDbSize();
		break;
	default:
		/* Unsupported action. */
		Q_ASSERT(0);
		return false;
		break;
	}

	if (Q_UNLIKELY(allDbSize <= 0)) {
		/* Expecting a positive value. */
		return false;
	}

	/* Check whether new location has required size for database storing. */
	{
		QStorageInfo si(newLocation);
		if (si.bytesAvailable() < allDbSize) {
			logErrorNL("%s", "It is not possible to store databases in the new location because there is not enough free space left.");
			return false;
		}
	}

	return relocateDatabases(newLocation, action);
}

enum Messages::ReloactionAction Messages::askAction(void)
{
	QList< QPair<QString, int> > customButtons;
	customButtons.append(QPair<QString, int>(tr("Move"), RA_RELOCATE));
	customButtons.append(QPair<QString, int>(tr("Create new"), RA_CREATE_NEW));
	int customVal = RA_RELOCATE;

	if (customVal == RA_RELOCATE) {
		return RA_RELOCATE;
	} else if (customVal == RA_CREATE_NEW) {
		return RA_CREATE_NEW;
	} else {
		return RA_NONE;
	}
}

int Messages::getAllDbSize(void)
{
	int size = 0;

	if (Q_UNLIKELY((GlobInstcs::messageDbsPtr == Q_NULLPTR) ||
	        (GlobInstcs::fileDbsPtr == Q_NULLPTR) ||
	        (GlobInstcs::acntMapPtr == Q_NULLPTR))) {
		Q_ASSERT(0);
		return -1;
	}

	const QList<AcntId> acntIdList(GlobInstcs::acntMapPtr->keys());
	foreach (const AcntId &acntId, acntIdList) {
		const bool dataOnDisk = PrefsSpecific::dataOnDisk(
		    *GlobInstcs::prefsPtr, GlobInstcs::acntMapPtr->acntData(acntId));

		MessageDb *msgDb = GlobInstcs::messageDbsPtr->accessMessageDb(
		    GlobalSettingsQmlWrapper::dbPath(), acntId, dataOnDisk);
		if (Q_UNLIKELY(msgDb == Q_NULLPTR)) {
			logErrorNL(
			    "Cannot access message database for username '%s'.",
			    acntId.username().toUtf8().constData());
			return -1;
		}
		size += msgDb->getDbSizeInBytes();

		FileDb *fDb = GlobInstcs::fileDbsPtr->accessFileDb(
		    GlobalSettingsQmlWrapper::dbPath(), acntId.username(), dataOnDisk);
		if (Q_UNLIKELY(fDb == Q_NULLPTR)) {
			logErrorNL(
			    "Cannot access file database for username '%s'.",
			    acntId.username().toUtf8().constData());
			return -1;
		}
		size += fDb->getDbSizeInBytes();
	}

	return size;
}

int Messages::estimateAllDbSize(void)
{
	if (Q_UNLIKELY(GlobInstcs::acntMapPtr == Q_NULLPTR)) {
		Q_ASSERT(0);
		return -1;
	}

	/* 1 MB free disk space is required for new database */
	return 2 * GlobInstcs::acntMapPtr->size() * 1000000;
}

bool Messages::relocateDatabases(const QString &newLocationDir,
    enum ReloactionAction action)
{
	if (Q_UNLIKELY((GlobInstcs::messageDbsPtr == Q_NULLPTR) ||
	        (GlobInstcs::fileDbsPtr == Q_NULLPTR) ||
	        (GlobInstcs::acntMapPtr == Q_NULLPTR))) {
		Q_ASSERT(0);
		return false;
	}

	if ((action != RA_RELOCATE) && (action != RA_CREATE_NEW)) {
		return false;
	}

	/* List of message databases and old locations. */
	typedef QPair<MessageDb *, QString> MsgDbListEntry;
	QList< MsgDbListEntry > relocatedMsgDbs;
	typedef QPair<FileDb *, QString> FileDbListEntry;
	QList< FileDbListEntry > relocatedFileDbs;

	bool relocationSucceeded = true;

	const QList<AcntId> acntIdList(GlobInstcs::acntMapPtr->keys());
	foreach (const AcntId &acntId, acntIdList) {
		const bool dataOnDisk = PrefsSpecific::dataOnDisk(
		    *GlobInstcs::prefsPtr, GlobInstcs::acntMapPtr->acntData(acntId));

		/* Ignore accounts with data stored in memory. */
		if (!dataOnDisk) {
			continue;
		}

		MessageDb *mDb = GlobInstcs::messageDbsPtr->accessMessageDb(
		    GlobalSettingsQmlWrapper::dbPath(), acntId, dataOnDisk);
		relocationSucceeded = (mDb != Q_NULLPTR);
		if (!relocationSucceeded) {
			logWarningNL(
			    "Cannot access message database for username '%s'.",
			    acntId.username().toUtf8().constData());
			break; /* Break the for cycle. */
		}
		FileDb *fDb = GlobInstcs::fileDbsPtr->accessFileDb(
		    GlobalSettingsQmlWrapper::dbPath(), acntId.username(),
		    dataOnDisk);
		relocationSucceeded = (fDb != Q_NULLPTR);
		if (!relocationSucceeded) {
			logWarningNL(
			    "Cannot access file database for username '%s'.",
			    acntId.username().toUtf8().constData());
			break; /* Break the for cycle. */
		}

		QString oldPath, oldFileName;

		/* Message database file. */
		oldPath = mDb->fileName();
		oldFileName = QFileInfo(oldPath).fileName();
		switch (action) {
		case RA_RELOCATE:
			relocationSucceeded = mDb->copyDb(
			    newLocationDir + QDir::separator() + oldFileName);
			break;
		case RA_CREATE_NEW:
			relocationSucceeded = mDb->reopenDb(
			    newLocationDir + QDir::separator() + oldFileName);
			break;
		default:
			Q_ASSERT(0);
			relocationSucceeded = false;
			break;
		}
		if (relocationSucceeded) {
			/*
			 * Remember original file name as the file still
			 * exists.
			 */
			relocatedMsgDbs.append(
			    MsgDbListEntry(mDb, oldPath));
		} else {
			break; /* Break the for cycle. */
		}

		/* File database file. */
		oldPath = fDb->fileName();
		oldFileName = QFileInfo(oldPath).fileName();
		switch (action) {
		case RA_RELOCATE:
			relocationSucceeded = fDb->copyDb(
			    newLocationDir + QDir::separator() + oldFileName);
			break;
		case RA_CREATE_NEW:
			relocationSucceeded = fDb->reopenDb(
			    newLocationDir + QDir::separator() + oldFileName);
			break;
		default:
			Q_ASSERT(0);
			relocationSucceeded = false;
			break;
		}
		if (relocationSucceeded) {
			/*
			 * Remember original file name as the file still
			 * exists.
			 */
			relocatedFileDbs.append(
			    FileDbListEntry(fDb, oldPath));
		} else {
			break; /* Break the for cycle. */
		}
	}

	if (relocationSucceeded) {
		/* Delete all original files. */
		foreach (const MsgDbListEntry &entry, relocatedMsgDbs) {
			QFile::remove(entry.second);
		}
		foreach (const FileDbListEntry &entry, relocatedFileDbs) {
			QFile::remove(entry.second);
		}
	} else {
		/*
		 * Revert all databases to original locations and delete the
		 * new locations.
		 */
		QString newLocation;
		foreach (const MsgDbListEntry &entry, relocatedMsgDbs) {
			newLocation = entry.first->fileName();
			if (entry.first->openDb(entry.second, true)) {
				QFile::remove(newLocation);
			} else {
				logErrorNL(
				    "Cannot revert to original message database file '%s'.",
				    entry.second.toUtf8().constData());
				/* TODO -- Critical. Cannot revert. */
			}
		}
		foreach (const FileDbListEntry &entry, relocatedFileDbs) {
			newLocation = entry.first->fileName();
			if (entry.first->openDb(entry.second, true)) {
				QFile::remove(newLocation);
			} else {
				logErrorNL(
				    "Cannot revert to original file database file '%s'.",
				    entry.second.toUtf8().constData());
				/* TODO -- Critical. Cannot revert. */
			}
		}
	}

	return relocationSucceeded;
}

int Messages::searchMsg(MessageListModel *messageModel, const QString &phrase,
    MessageTypes msgTypes)
{
	debugFuncCall();

	if (Q_UNLIKELY((GlobInstcs::messageDbsPtr == Q_NULLPTR) ||
	        (GlobInstcs::acntMapPtr == Q_NULLPTR))) {
		Q_ASSERT(0);
		return 0;
	}

	int msgs = 0;

	if (Q_UNLIKELY(messageModel == Q_NULLPTR)) {
		logErrorNL("%s", "Cannot access message model.");
		Q_ASSERT(0);
		return 0;
	}

	messageModel->clearAll();

	QCoreApplication::processEvents();

	const QList<AcntId> acntIdList(GlobInstcs::acntMapPtr->keys());
	foreach (const AcntId &acntId, acntIdList) {
		const bool dataOnDisk = PrefsSpecific::dataOnDisk(
		    *GlobInstcs::prefsPtr, GlobInstcs::acntMapPtr->acntData(acntId));

		/* First step: Search in attachment names, return msg ID list */
		QList<qint64> msgIds;

		FileDb *fDb = GlobInstcs::fileDbsPtr->accessFileDb(
		    GlobalSettingsQmlWrapper::dbPath(), acntId.username(),
		    dataOnDisk);
		if (fDb != Q_NULLPTR) {
			msgIds = fDb->searchAttachmentName(phrase);
		} else {
			logWarningNL("%s", "Cannot access file database.");
		}

		/*
		 * Second step: Search in message envelopes and
		 *              add messages from first step into model.
		 */
		MessageDb *msgDb = GlobInstcs::messageDbsPtr->accessMessageDb(
		    GlobalSettingsQmlWrapper::dbPath(), acntId, dataOnDisk);
		if (Q_UNLIKELY(msgDb == Q_NULLPTR)) {
			logErrorNL("%s", "Cannot access message database.");
			continue;
		}

		if (msgTypes & TYPE_RECEIVED) {
			msgs += msgDb->searchMessagesAndSetModelFromDb(
			    messageModel, acntId, phrase,
			    MessageDb::TYPE_RECEIVED, msgIds);
		}
		if (msgTypes & TYPE_SENT) {
			msgs += msgDb->searchMessagesAndSetModelFromDb(
			    messageModel, acntId, phrase,
			    MessageDb::TYPE_SENT, msgIds);
		}

		QCoreApplication::processEvents();
	}

	return msgs;
}

QString Messages::getZfoFilePath(const QmlAcntId *qAcntId, qint64 msgId,
    enum Messages::MessageType msgType, bool deliveryInfo)
{
	debugFuncCall();

	if (Q_UNLIKELY((GlobInstcs::acntMapPtr == Q_NULLPTR) ||
	        (qAcntId == Q_NULLPTR)) || (msgId <= 0)) {
		Q_ASSERT(0);
		return QString();
	}

	if (deliveryInfo) {
		return joinDirs(getZfoPath(qAcntId->username(), qAcntId->testing(),
		    msgId), getDelInfoZfoName(msgId));
	}

	return joinDirs(getZfoPath(qAcntId->username(), qAcntId->testing(),
	    msgId), getMsgZfoName(msgId, msgType));
}

/*!
 * @brief Check if required free space is available for database convert.
 * @param[in] locDir Directory where to search for the file.
 * @param[in] dbsSizeByte Total size of all message databases.
 * @return True on success.
 */
static
bool isAvailableSpace(const QString &locDir, qint64 dbsSizeByte)
{
	const QStorageInfo sti(locDir);

	if (sti.isReadOnly()) {
		return false;
	}

	/* We need 4*databses size for conversions into new db structure. */
	return ((dbsSizeByte * 4) < sti.bytesAvailable());
}

/*!
 * @brief Convert one message database into new format.
 *
 * @param[in] acntId Account identifier.
 * @return True if success.
 */
static
bool convertOneDbIntoNewFormat(const AcntId &acntId)
{
	const QString locDir(GlobalSettingsQmlWrapper::dbPath());

	/* Test if new database file exists. */
	QString dbFileName(MessageDb::constructDbFileNameNew(acntId.username(),
	    acntId.testing()));
	QString newMsgDb = joinDirs(locDir, dbFileName);
	if (QFile::exists(newMsgDb)) {
		logInfoNL("%s", "New message database exists.");
		return true;
	}

	/* Access old message database. */
	const bool dataOnDisk = PrefsSpecific::dataOnDisk(
	    *GlobInstcs::prefsPtr, GlobInstcs::acntMapPtr->acntData(acntId));
	MessageDb *oldDb = GlobInstcs::messageDbsPtr->accessMessageDb(
	    GlobalSettingsQmlWrapper::dbPath(), acntId, dataOnDisk);
	QString oldMsgDb = oldDb->fileName();
	/* Old database is intentionally left in storage. */
	GlobInstcs::messageDbsPtr->closeAndRemoveDb(oldDb);

	/* Copy the old database file to the new database file. */
	if (Q_UNLIKELY(!QFile::copy(oldMsgDb, newMsgDb))) {
		logErrorNL("%s", "Cannot copy old message database.");
		return false;
	}

	/*
	 * Access the new database file without modifications.
	 *
	 * New database during conversion is not stored into the container.
	 */
	MessageDb *newDb = MsgDbContainer::accessMessageDbNew(locDir, acntId,
	    dataOnDisk, false);
	if (Q_UNLIKELY(Q_NULLPTR == newDb)) {
		logErrorNL("%s", "Cannot access a new message database. Delete new message database.");
		QFile::remove(newMsgDb);
		return false;
	}

	/* Covert content to new database format. */
	if (Q_UNLIKELY(!newDb->converMsgDbIntoNewFormat())) {
		logErrorNL("%s", "Cannot convert a new message database. Delete new message database.");
		QFile::remove(newMsgDb);
		delete newDb;
		return false;
	}

	newDb->addNewTables();

	delete newDb;

	return true;
}

bool Messages::convertMsgDbsIntoNewFormat(const QList<AcntId> &acntIdList)
{
	qint64 size = 0;
	const QString locDir(GlobalSettingsQmlWrapper::dbPath());
	bool success = true;

	/* Compute size of all message databases. */
	foreach (const AcntId &acntId, acntIdList) {
		const bool dataOnDisk = PrefsSpecific::dataOnDisk(
		    *GlobInstcs::prefsPtr, GlobInstcs::acntMapPtr->acntData(acntId));
		MessageDb *oldDb = GlobInstcs::messageDbsPtr->accessMessageDb(
		    GlobalSettingsQmlWrapper::dbPath(), acntId, dataOnDisk);
		if (Q_UNLIKELY(oldDb == Q_NULLPTR)) {
			logErrorNL(
			    "Cannot access message database for username '%s'.",
			    acntId.username().toUtf8().constData());
			return false;
		}
		size += oldDb->getDbSizeInBytes();
	}

	/* Test if free space is available for all databases convert. */
	if (Q_UNLIKELY(!isAvailableSpace(locDir, size))) {
		logErrorNL("%s", "No enough free space for message databases conversion.");
		return false;
	}

	QCoreApplication::processEvents();

	/* Convert all databases into new format. */
	foreach (const AcntId &acntId, acntIdList) {
		if (Q_UNLIKELY(!(convertOneDbIntoNewFormat(acntId)))) {
			logErrorNL(
			    "Cannot convert message database into new format for username '%s'.",
			    acntId.username().toUtf8().constData());
			logInfoNL(
			    "Create new empty database file for account '%s'.",
			    acntId.username().toUtf8().constData());
			const bool dataOnDisk = PrefsSpecific::dataOnDisk(
			    *GlobInstcs::prefsPtr, GlobInstcs::acntMapPtr->acntData(acntId));
			MessageDb *newDb = MsgDbContainer::accessMessageDbNew(locDir,
			    acntId, dataOnDisk, true);
			delete newDb;
			emit sendMsgDbConvertInfo(tr("The message database for username '%1' couldn't be converted into the new format. A new empty database file has been created.").arg(acntId.username()));
			success = success && false;
		} else {
			success = success && true;
		}
	}

	return success;
}

/*!
 * @brief Get list of accounts for conversion.
 *
 * @return List of account ids for conversion.
 */
static
QList<AcntId> getAcntListForConversion(void)
{
	/*
	 * Check format of all message databases.
	 * If old message database is present only, add account id into list
	 * for conversion.
	 */
	QList<AcntId> convertAcntIdList;
	const QList<AcntId> acntIdList(GlobInstcs::acntMapPtr->keys());
	foreach (const AcntId &acntId, acntIdList) {
		MsgDbContainer::DatabaseType msgType =
		    MsgDbContainer::msgDbExists(
		        GlobalSettingsQmlWrapper::dbPath(), acntId);
		if (msgType == MsgDbContainer::OLD_MSG_DB) {
			convertAcntIdList.append(acntId);
		}
	}

	return convertAcntIdList;

}

bool Messages::checkNewDatabasesFormat(void)
{
	debugSlotCall();

	return getAcntListForConversion().isEmpty();
}

bool Messages::convertDatabases(void)
{
	debugSlotCall();

	QList<AcntId> convertAcntIdList = getAcntListForConversion();
	if (convertAcntIdList.isEmpty()) {
		return true;
	}

	QCoreApplication::processEvents();

	return convertMsgDbsIntoNewFormat(convertAcntIdList);
}

void Messages::quitApp(void)
{
	QApplication::quit();
}

/*!
 * @brief Construct a local message database backup file name with path.
 *
 * @param[in] acntId Account identifier.
 * @param[in] monthsBack Number of months to add to current date.
 *                       Use negative to set back in time.
 * @return Backup message database file path.
 */
static
QString constructDbBackupFileNamePath(const AcntId &acntId, int monthsBack)
{
	return joinDirs(GlobalSettingsQmlWrapper::dbPath(),
	    MessageDb::constructDbBackupFileName(acntId.username(),
	    acntId.testing(),
	    QDate::currentDate().addMonths(monthsBack)));
}

/*!
 * @brief Check if a newest backup message database exists.
 *
 * @param[in] acntId Account identifier.
 * @return True on success.
 */
static
bool isNewestBackupDbFilePresent(const AcntId &acntId)
{
	const QFileInfo fi(constructDbBackupFileNamePath(acntId, 0));
	return fi.exists();
}

/*!
 * @brief Create a local backup message database if it doesn't exist.
 *
 * @param[in] acntId Account identifier.
 * @return True on success.
 */
static
bool createLocalBackupDbFile(const AcntId &acntId)
{
	const bool dataOnDisk = PrefsSpecific::dataOnDisk(
	    *GlobInstcs::prefsPtr, GlobInstcs::acntMapPtr->acntData(acntId));
	if (Q_UNLIKELY(!dataOnDisk)) {
		/* Don't backup data held only in memory. */
		return false;
	}
	MessageDb *oldDb = GlobInstcs::messageDbsPtr->accessMessageDb(
	    GlobalSettingsQmlWrapper::dbPath(), acntId, dataOnDisk);
	if (Q_UNLIKELY(oldDb == Q_NULLPTR)) {
		logErrorNL(
		    "Cannot access message database for username '%s'.",
		    acntId.username().toUtf8().constData());
		return false;
	}
	/* Check message database integrity. */
	if (Q_UNLIKELY(!oldDb->checkMsgDbIntegrity())) {
		return false;
	}

	const QString oldDbPath = oldDb->fileName();
	const QFileInfo fi(constructDbBackupFileNamePath(acntId, 0));
	/* Copy message database file to the backup database file. */
	if (Q_UNLIKELY(!oldDb->copyDb(fi.filePath()))) {
		logErrorNL(
		     "Cannot copy message database for username '%s' into a new local backup file.",
		     acntId.username().toUtf8().constData());
		return false;
	}

	oldDb->openDb(oldDbPath, dataOnDisk);

	return true;
}

#define MONTHS_LOOK_BACK 12
#define MONTHS_LEAVE_BACK 3

/*!
 * @brief Delete all older backup files up MONTHS_LOOK_BACK months back leaving
 *     only MONTHS_LEAVE_BACK latest.
 *
 * @param[in] acntId Account identifier.
 */
static
void deleteOlderLocalBackupDbFiles(const AcntId &acntId)
{
	QFile file;
	int backupFileCnt = 0;

	/*
	 * Delete all older backup files up to one year back
	 * except the last three most recent.
	 */
	for (int i = 0; i < MONTHS_LOOK_BACK; ++i) {
		file.setFileName(constructDbBackupFileNamePath(acntId, -i));
		if (file.exists()) {
			++backupFileCnt;
			if (backupFileCnt > MONTHS_LEAVE_BACK) {
				file.remove();
			}
		}
	}
}

/*!
 * @brief Check if a new format message database exists.
 *
 * @param[in] acntId Account identifier.
 * @return True on success.
 */
static
bool isNewDbFilePresent(const AcntId &acntId)
{
	const enum MsgDbContainer::DatabaseType msgType =
	    MsgDbContainer::msgDbExists(GlobalSettingsQmlWrapper::dbPath(),
	        acntId);
	return ((msgType == MsgDbContainer::BOTH_MSG_DB) ||
	    (msgType == MsgDbContainer::NEW_MSG_DB));
}

void Messages::checkOrCreateLocalBackupMsgDbs(void)
{
	const QList<AcntId> acntIdList(GlobInstcs::acntMapPtr->keys());
	for (const AcntId &acntId : acntIdList) {
		/* Skip old format message database format. */
		if (Q_UNLIKELY(!isNewDbFilePresent(acntId))) {
				continue;
		}
		if (Q_UNLIKELY(!isNewestBackupDbFilePresent(acntId))) {
			if (Q_UNLIKELY(!createLocalBackupDbFile(acntId))) {
				continue;
			}
			deleteOlderLocalBackupDbFiles(acntId);
		}
	}
}

QStringList Messages::getLocalBackupFiles(const QmlAcntId *qAcntId)
{
	if (Q_UNLIKELY(qAcntId == Q_NULLPTR)) {
		Q_ASSERT(0);
		return QStringList();
	}

	QFile f;
	QStringList backupFiles;

	/*
	 * Find MONTHS_LEAVE_BACK newest backup database files from last
	 * MONTHS_LOOK_BACK months.
	 */
	for (int i = 0; i < MONTHS_LOOK_BACK; ++i) {
		f.setFileName(constructDbBackupFileNamePath(*qAcntId, -i));
		if (f.exists()) {
			backupFiles.append(f.fileName());
			if (backupFiles.size() > MONTHS_LEAVE_BACK) {
				break;
			}
		}
	}

	return backupFiles;
}

QString Messages::getFileDateFromLocalBackup(const QString &filePath)
{
	QFileInfo fi(filePath);
	QString date = fi.baseName().right(6);
	return date.insert(4, "/");
}

/*!
 * @brief Remove and delete message database from container.
 *
 * @param[in] acntId Account identifier.
 * @return True on success.
 */
static
bool removeAndDeleteMsgDbFromContainer(const AcntId &acntId)
{
	const bool dataOnDisk = PrefsSpecific::dataOnDisk(
	    *GlobInstcs::prefsPtr, GlobInstcs::acntMapPtr->acntData(acntId));
	MessageDb *oldDb = GlobInstcs::messageDbsPtr->accessMessageDb(
	    GlobalSettingsQmlWrapper::dbPath(), acntId, dataOnDisk);
	if (Q_UNLIKELY(oldDb == Q_NULLPTR)) {
		/*
		 * Database file is so corrupted that it couldn't be opened
		 * by the container itself.
		 * True is returned because the opened file is not held within
		 * the container.
		 */
		logErrorNL(
		    "Cannot access corrupted message database for username '%s'.",
		    acntId.username().toUtf8().constData());
		return true;
	}
	return GlobInstcs::messageDbsPtr->closeAndRemoveDb(oldDb);
}

/*!
 * @brief Remove and delete message database from container and storage.
 *
 * @param[in] acntId Account identifier.
 * @param[in] filePath File path.
 */
static
void removeAndDeleteCorruptedMsgDb(const AcntId &acntId,
    const QString &filePath)
{
	/* Remove and delete corrupted database from container. */
	removeAndDeleteMsgDbFromContainer(acntId);
	/* Delete corrupted database from storage. */
	QFile dbFile(filePath);
	if (Q_UNLIKELY(!dbFile.remove())) {
		logErrorNL("Cannot delete corrupted message database '%s'.",
		    dbFile.fileName().toUtf8().constData());
	}
}

void Messages::removeAndDeleteMsgDb(const QmlAcntId *qAcntId)
{
	if (Q_UNLIKELY((qAcntId == Q_NULLPTR))) {
		Q_ASSERT(0);
		return;
	}

	removeAndDeleteCorruptedMsgDb(*qAcntId,
	joinDirs(GlobalSettingsQmlWrapper::dbPath(),
	    MessageDb::constructDbFileNameNew(qAcntId->username(),
	    qAcntId->testing())));
	logInfoNL("%s", "Create new empty database file.");
	const bool dataOnDisk = PrefsSpecific::dataOnDisk(
	    *GlobInstcs::prefsPtr, GlobInstcs::acntMapPtr->acntData(*qAcntId));
	MessageDb *newDb = GlobInstcs::messageDbsPtr->accessMessageDbNew(
	    GlobalSettingsQmlWrapper::dbPath(), *qAcntId, dataOnDisk,
	    true);
	delete newDb;
}

bool Messages::restoreMsgDbFromLocalBackup(const QmlAcntId *qAcntId,
    const QString &filePath)
{
	if (Q_UNLIKELY((qAcntId == Q_NULLPTR) || filePath.isEmpty())) {
		Q_ASSERT(0);
		return false;
	}

	QFile bakFile(filePath);
	QFile dbFile(joinDirs(GlobalSettingsQmlWrapper::dbPath(),
	    MessageDb::constructDbFileNameNew(qAcntId->username(),
	    qAcntId->testing())));
	const QString originDbPath = dbFile.fileName();
	const QString originDbPathBad = originDbPath + QStringLiteral(".bad");

	if (Q_UNLIKELY(!bakFile.exists())) {
		logErrorNL("Cannot find backup message database '%s'.",
		    bakFile.fileName().toUtf8().constData());
		return false;
	}
	/* Remove old corrupted file if it does for some reason exist. */
	QFile::remove(originDbPathBad);
	/* Remove and delete corrupted message database from container. */
	if (Q_UNLIKELY(!removeAndDeleteMsgDbFromContainer(*qAcntId))) {
		logErrorNL("Cannot remove corrupted message database '%s'.",
		    dbFile.fileName().toUtf8().constData());
		return false;
	}
	/* Rename corrupted database file. */
	if (Q_UNLIKELY(!dbFile.rename(originDbPathBad))) {
		logErrorNL("Cannot rename corrupted message database '%s'.",
		    dbFile.fileName().toUtf8().constData());
		return false;
	}
	/* Copy backup. */
	if (Q_UNLIKELY(!bakFile.copy(originDbPath))) {
		logErrorNL("Cannot copy backup message database '%s'.",
		    bakFile.fileName().toUtf8().constData());
		removeAndDeleteCorruptedMsgDb(*qAcntId, dbFile.fileName());
		return false;
	}
	/* Delete corrupted database from storage. */
	if (Q_UNLIKELY(!dbFile.remove())) {
		logErrorNL("Cannot delete corrupted message database '%s'.",
		    dbFile.fileName().toUtf8().constData());
	}

	return true;
}

bool Messages::checkMsgDbIntegrity(const QmlAcntId *qAcntId)
{
	if (Q_UNLIKELY(qAcntId == Q_NULLPTR)) {
		Q_ASSERT(0);
		return false;
	}

	MessageDb *msgDb = GlobInstcs::messageDbsPtr->accessMessageDb(
	    GlobalSettingsQmlWrapper::dbPath(), *qAcntId,
	    PrefsSpecific::dataOnDisk(
	        *GlobInstcs::prefsPtr, GlobInstcs::acntMapPtr->acntData(*qAcntId)));
	if (Q_UNLIKELY(msgDb == Q_NULLPTR)) {
		logErrorNL(
		    "Cannot access message database for username '%s'.",
		    qAcntId->username().toUtf8().constData());
		return false;
	}
	return msgDb->checkMsgDbIntegrity();
}

bool Messages::testMsgDbIntegrity(const QmlAcntId *qAcntId)
{
	if (Q_UNLIKELY(qAcntId == Q_NULLPTR)) {
		Q_ASSERT(0);
		return false;
	}

	MessageDb *msgDb = GlobInstcs::messageDbsPtr->accessMessageDb(
	    GlobalSettingsQmlWrapper::dbPath(), *qAcntId,
	    PrefsSpecific::dataOnDisk(
	        *GlobInstcs::prefsPtr, GlobInstcs::acntMapPtr->acntData(*qAcntId)));
	if (Q_UNLIKELY(msgDb == Q_NULLPTR)) {
		logErrorNL(
		    "Cannot access message database for username '%s'.",
		    qAcntId->username().toUtf8().constData());
		return false;
	}

	return msgDb->runCompleteTestMsgDbIntegrity();
}

void Messages::stopWorker(bool stop)
{
	debugSlotCall();

	if (Q_UNLIKELY(GlobInstcs::workPoolPtr == Q_NULLPTR)) {
		Q_ASSERT(0);
		return;
	}

	if (stop) {
		logInfoNL("%s", "Waiting for pending worker threads.");
		GlobInstcs::workPoolPtr->wait();
		GlobInstcs::workPoolPtr->stop();
		logInfoNL("%s", "Worker pool stopped.");
	} else {
		GlobInstcs::workPoolPtr->start();
		logInfoNL("%s", "Worker pool started.");
	}
}

bool Messages::isMessageVodz(const QmlAcntId *qAcntId, qint64 msgId)
{
	debugFuncCall();

	if (Q_UNLIKELY((GlobInstcs::messageDbsPtr == Q_NULLPTR) ||
	        (GlobInstcs::acntMapPtr == Q_NULLPTR) ||
	        (qAcntId == Q_NULLPTR))) {
		Q_ASSERT(0);
		return false;
	}

	MessageDb *msgDb = GlobInstcs::messageDbsPtr->accessMessageDb(
	    GlobalSettingsQmlWrapper::dbPath(), *qAcntId,
	    PrefsSpecific::dataOnDisk(
	        *GlobInstcs::prefsPtr, GlobInstcs::acntMapPtr->acntData(*qAcntId)));
	if (Q_UNLIKELY(msgDb == Q_NULLPTR)) {
		logErrorNL("%s", "Cannot access message database.");
		return false;
	}

	return msgDb->isVodz(msgId);
}
