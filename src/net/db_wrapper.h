/*
 * Copyright (C) 2014-2025 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

#pragma once

#include <QCoreApplication> /* Q_DECLARE_TR_FUNCTIONS */

#include "src/sqlite/message_db.h"

class AcntId; /* Forward declaration. */
class QDateTime; /* Forward declaration. */

namespace Isds {
	/* Forward declarations. */
	class DbOwnerInfoExt2;
	class DbUserInfoExt2;
	class DmMessageAuthor;
	class DTInfoOutput;
}

/*
 * Class DbWrapper updates data from http response to database.
 * Class is initialised and used in the ISDS wrapper class (isds_wrapper.h)
 */
class DbWrapper {
	Q_DECLARE_TR_FUNCTIONS(DbWrapper)

public:
	/*!
	 * @brief Insert messagelist to db.
	 *
	 * @param[in] acntId Account identifier.
	 * @param[in] messageType Message type.
	 * @param[in] envelopes List of message envelopes.
	 * @param[out] messageChangedStatusList List of message ids
	 *             where message status has changed.
	 * @param[out] txt Error description if something fails.
	 * @param[out] listOfNewMsgIds List of new message Ids for complete
	 *             message downloading.
	 * @param[out] listOfNewBigMsgIds List of new big message Ids for complete
	 *             message downloading.
	 * @return true if success.
	 */
	static
	bool insertMessageListToDb(const AcntId &acntId,
	    enum MessageDb::MessageType messageType,
	    const QList<Isds::Envelope> &envelopes,
	    QList<qint64> &messageChangedStatusList, QString &txt,
	    QList<qint64> &listOfNewMsgIds, QList<qint64> &listOfNewBigMsgIds);

	/*!
	 * @brief Insert complete message to db.
	 *
	 * @param[in] acntId Account identifier.
	 * @param[in] message Message data including message identifier.
	 * @param[in] messageType Message type.
	 * @param[in] storeFiles True if store attachment files into database.
	 * @param[out] txt Error description if something fails.
	 * @return true if success.
	 */
	static
	bool insertCompleteMessageToDb(const AcntId &acntId,
	    const Isds::Message &message,
	    enum MessageDb::MessageType messageType, bool storeFiles,
	    QString &txt);

	/*!
	 * @brief Insert password expiration date to db.
	 *
	 * @param[in] acntId Account identifier.
	 * @param[in] expirDate Password expiration date.
	 * @return true if success.
	 */
	static
	bool insertPwdExpirationToDb(const AcntId &acntId,
	    const QDateTime &expirDate);

	/*!
	 * @brief Insert account info to db.
	 *
	 * @param[in] acntId Account identifier.
	 * @param[in] dbOwnerInfo Account info struct.
	 * @return true if success.
	 */
	static
	bool insertAccountInfoToDb(const AcntId &acntId,
	    const Isds::DbOwnerInfoExt2 &dbOwnerInfo);

	/*!
	 * @brief Insert long term storage info into db.
	 *
	 * @param[in] dbID Data box id.
	 * @param[in] dtInfo Long term storage info struct.
	 * @return True if success.
	 */
	static
	bool insertDTInfoToDb(const QString &dbID,
	    const Isds::DTInfoOutput &dtInfo);

	/*!
	 * @brief Insert user info to db.
	 *
	 * @param[in] acntId Account identifier.
	 * @param[in] dbUserInfo User info struct.
	 * @return true if success.
	 */
	static
	bool insertUserInfoToDb(const AcntId &acntId,
	    const Isds::DbUserInfoExt2 &dbUserInfo);

	/*!
	 * @brief Update message author info 2 to db.
	 *
	 * @param[in] acntId Account identifier.
	 * @param[in] msgId Message ID.
	 * @param[in] msgAuthor Message author info.
	 * @param[out] txt Error description if something fails.
	 * @return true if success.
	 */
	static
	bool updateAuthorInfo2(const AcntId &acntId, qint64 msgId,
	    const Isds::DmMessageAuthor &msgAuthor, QString &txt);

	/*!
	 * @brief Insert or update delivery info to db.
	 *
	 * @param[in] acntId Account identifier.
	 * @param[in] events Event list.
	 * @param[in] msgId Message ID.
	 * @param[out] txt Error description if something fails.
	 * @return true if success.
	 */
	static
	bool insertMesasgeDeliveryInfoToDb(const AcntId &acntId,
	    const QList<Isds::Event> &events, qint64 msgId,
	    QString &txt);

private:
	/*!
	 * @brief Private constructor.
	 */
	DbWrapper(void);
};
