/*
 * Copyright (C) 2014-2025 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

#include "src/datovka_shared/identifiers/account_id.h"
#include "src/global.h"
#include "src/net/db_wrapper.h"
#include "src/settings/accounts.h"
#include "src/settings/prefs_specific.h"
#include "src/setwrapper.h"
#include "src/sqlite/account_db.h"
#include "src/sqlite/file_db_container.h"
#include "src/sqlite/file_db.h"
#include "src/sqlite/message_db_container.h"
#include "src/sqlite/message_db.h"

bool DbWrapper::insertMessageListToDb(const AcntId &acntId,
    enum MessageDb::MessageType messageType,
    const QList<Isds::Envelope> &envelopes,
    QList<qint64> &messageChangedStatusList, QString &txt,
    QList<qint64> &listOfNewMsgIds, QList<qint64> &listOfNewBigMsgIds)
{
	if (Q_UNLIKELY((GlobInstcs::messageDbsPtr == Q_NULLPTR) ||
	        (GlobInstcs::acntMapPtr == Q_NULLPTR))) {
		txt = tr("Internal error!");
		Q_ASSERT(0);
		return false;
	}

	MessageDb *msgDb = GlobInstcs::messageDbsPtr->accessMessageDb(
	    GlobalSettingsQmlWrapper::dbPath(), acntId,
	    PrefsSpecific::dataOnDisk(
	        *GlobInstcs::prefsPtr, GlobInstcs::acntMapPtr->acntData(acntId)));
	if (Q_UNLIKELY(msgDb == Q_NULLPTR)) {
		txt = tr("Cannot open message database!");
		return false;
	}

	bool isSentMessage = (messageType == MessageDb::TYPE_SENT);
	int newMsgs = 0;

	msgDb->beginTransaction();
	foreach (const Isds::Envelope &envelope, envelopes) {
		bool hasFiles;
		int msgStatus =
		    msgDb->getMessageStatusFromDb(envelope.dmId(), hasFiles);
		// -1 = message is not in the database, mesasge is new
		if (-1 == msgStatus) {
			newMsgs++;
			if (!msgDb->insertOrUpdateMessageEnvelopeInDb(
			    envelope.dmId(), messageType, envelope, false)) {
				txt = tr("Message %1 envelope insertion failed!").
				    arg(envelope.dmId());
			} else {
				// New message envelope has been saved into
				// database. Append message id to list
				// of complete messages to be downloaded.
				if (Isds::Type::BOOL_TRUE == envelope.dmVODZ()) {
					listOfNewBigMsgIds.append(envelope.dmId());
				} else {
					listOfNewMsgIds.append(envelope.dmId());
				}
			}
		} else {
			if (!msgDb->updateMessageEnvelopeInDb(envelope, false)) {
				txt = tr("Message %1 envelope update failed!").
				    arg(envelope.dmId());
			}
			if (isSentMessage) {
				if (msgStatus != envelope.dmMessageStatus() && hasFiles) {
					messageChangedStatusList.append(envelope.dmId());
				}
			}
		}
	}
	msgDb->commitTransaction();

	if (!isSentMessage) {
		if (newMsgs > 0) {
			txt = tr("%1: new messages: %2")
			    .arg(acntId.username()).arg(QString::number(newMsgs));
		} else {
			txt = tr("%1: No new messages.").arg(acntId.username());
		}
	}

	return true;
}

bool DbWrapper::insertCompleteMessageToDb(const AcntId &acntId,
    const Isds::Message &message, enum MessageDb::MessageType messageType,
    bool storeFiles, QString &txt)
{
	if (Q_UNLIKELY((GlobInstcs::messageDbsPtr == Q_NULLPTR) ||
	        (GlobInstcs::fileDbsPtr == Q_NULLPTR) ||
	        (GlobInstcs::acntMapPtr == Q_NULLPTR))) {
		Q_ASSERT(0);
		txt = tr("Internal error!");
		return false;
	}

	qint64 dmId = message.envelope().dmId();
	FileDb *fDb;

	const bool dataOnDisk = PrefsSpecific::dataOnDisk(*GlobInstcs::prefsPtr,
	    GlobInstcs::acntMapPtr->acntData(acntId));

	if (storeFiles) {
		/* Open file database */
		fDb = GlobInstcs::fileDbsPtr->accessFileDb(
		    GlobalSettingsQmlWrapper::dbPath(), acntId.username(), dataOnDisk);
		if (Q_UNLIKELY(fDb == Q_NULLPTR)) {
			txt = tr("Cannot open file database!");
		}

		/* Insert or update attachment files */
		fDb->beginTransaction();
		foreach (const Isds::Document &document, message.documents()) {
			if (Q_UNLIKELY(!fDb->insertUpdateFileIntoDb(dmId, document))) {
				txt = tr("File %1 insertion failed!").
				    arg(document.fileDescr());
				continue;
			}
		}
		fDb->commitTransaction();
	}

	/* Open message database */
	MessageDb *msgDb = GlobInstcs::messageDbsPtr->accessMessageDb(
	    GlobalSettingsQmlWrapper::dbPath(), acntId, dataOnDisk);
	if (Q_UNLIKELY(msgDb == Q_NULLPTR)) {
		txt = tr("Cannot open message database!");
		goto fail;
	}

	/* Insert or update message envelope data */
	if (Q_UNLIKELY(!msgDb->insertOrUpdateMessageEnvelopeInDb(dmId,
	        messageType, message.envelope(), storeFiles))) {
		txt = tr("Message %1 envelope update failed!").
		    arg(dmId);
		goto fail;
	}

	return true;
fail:
	if (storeFiles) {
		fDb->rollbackTransaction();
	}
	return false;
}

bool DbWrapper::insertPwdExpirationToDb(const AcntId &acntId,
    const QDateTime &expirDate)
{
	if (GlobInstcs::accountDbPtr != Q_NULLPTR) {
		return GlobInstcs::accountDbPtr->updatePwdExpirInDb(
		    acntId.username(), acntId.testing(), expirDate);
	} else {
		Q_ASSERT(0);
		return false;
	}
}

bool DbWrapper::insertAccountInfoToDb(const AcntId &acntId,
    const Isds::DbOwnerInfoExt2 &dbOwnerInfo)
{
	if (GlobInstcs::accountDbPtr != Q_NULLPTR) {
		return GlobInstcs::accountDbPtr->insertAccountInfoIntoDb(
		    acntId.username(), dbOwnerInfo);
	} else {
		Q_ASSERT(0);
		return false;
	}
}

bool DbWrapper::insertDTInfoToDb(const QString &dbID,
    const Isds::DTInfoOutput &dtInfo)
{
	if (GlobInstcs::accountDbPtr != Q_NULLPTR) {
		return GlobInstcs::accountDbPtr->insertDTInfoIntoDb(
		    dbID, dtInfo);
	} else {
		Q_ASSERT(0);
		return false;
	}
}

bool DbWrapper::insertUserInfoToDb(const AcntId &acntId,
    const Isds::DbUserInfoExt2 &dbUserInfo)
{
	if (GlobInstcs::accountDbPtr != Q_NULLPTR) {
		return GlobInstcs::accountDbPtr->insertUserInfoIntoDb(
		    acntId.username(), dbUserInfo);
	} else {
		Q_ASSERT(0);
		return false;
	}
}

bool DbWrapper::updateAuthorInfo2(const AcntId &acntId, qint64 msgId,
    const Isds::DmMessageAuthor &msgAuthor, QString &txt)
{
	if (Q_UNLIKELY((GlobInstcs::messageDbsPtr == Q_NULLPTR) ||
	        (GlobInstcs::acntMapPtr == Q_NULLPTR))) {
		txt = tr("Internal error!");
		Q_ASSERT(0);
		return false;
	}

	MessageDb *msgDb = GlobInstcs::messageDbsPtr->accessMessageDb(
	    GlobalSettingsQmlWrapper::dbPath(), acntId,
	    PrefsSpecific::dataOnDisk(
	        *GlobInstcs::prefsPtr, GlobInstcs::acntMapPtr->acntData(acntId)));
	if (Q_UNLIKELY(msgDb == Q_NULLPTR)) {
		txt = tr("Cannot open message database!");
		return false;
	}

	return msgDb->updateMessageAuthorInfo2(msgId, msgAuthor);
}

bool DbWrapper::insertMesasgeDeliveryInfoToDb(const AcntId &acntId,
    const QList<Isds::Event> &events, qint64 msgId,  QString &txt)
{
	if (Q_UNLIKELY((GlobInstcs::messageDbsPtr == Q_NULLPTR) ||
	        (GlobInstcs::acntMapPtr == Q_NULLPTR))) {
		txt = tr("Internal error!");
		Q_ASSERT(0);
		return false;
	}

	MessageDb *msgDb = GlobInstcs::messageDbsPtr->accessMessageDb(
	    GlobalSettingsQmlWrapper::dbPath(), acntId,
	    PrefsSpecific::dataOnDisk(
	        *GlobInstcs::prefsPtr, GlobInstcs::acntMapPtr->acntData(acntId)));
	if (Q_UNLIKELY(msgDb == Q_NULLPTR)) {
		txt = tr("Cannot open message database!");
		return false;
	}

	return msgDb->setMessageEvents(msgId, events, true);
}
