/*
 * Copyright (C) 2014-2025 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

#pragma once

#include <QObject>

#include "src/datovka_shared/identifiers/account_id.h"
#include "src/settings/account.h"

class AccountListModel; /* Forward declaration. */
class QmlAcntId; /* Forward declaration. */

/*
 * Class Accounts provides interface between QML and account database, settings.
 * Class is initialised in the main function (main.cpp)
 */
class Accounts : public QObject {
	Q_OBJECT

public:
	explicit Accounts(QObject *parent = Q_NULLPTR);

	/*!
	 * @brief Get account data box type from database.
	 *
	 * @param[in] qAcntId Account identifier.
	 * @return Databox type string.
	 */
	Q_INVOKABLE static
	QString dbType(const QmlAcntId *qAcntId);

	/*!
	 * @brief Get account data box ID from database.
	 *
	 * @param[in] qAcntId Account identifier.
	 * @return Databox id string.
	 */
	Q_INVOKABLE static
	QString dbId(const QmlAcntId *qAcntId);

	/*!
	 * @brief Check whether data box is OVM or sub-OVM.
	 *
	 * @param[in] qAcntId Account identifier.
	 * @return True if data box OVM or sub-OVM.
	 */
	Q_INVOKABLE static
	bool isOvm(const QmlAcntId *qAcntId);

	/*!
	 * @brief Update message counters for the account.
	 *
	 * @param[in,out] accountModel Account model.
	 * @param[in] qAcntId Account identifier.
	 */
	Q_INVOKABLE static
	void updateOneAccountCounters(AccountListModel *accountModel,
	    const QmlAcntId *qAcntId);

	/*!
	 * @brief Load account info from database for QML page.
	 *
	 * @param[in] acntId Account identifier.
	 * @return Account detail info html string.
	 */
	Q_INVOKABLE static
	QString fillAccountInfo(const QmlAcntId *qAcntId);

	/*!
	 * @brief Get account data for QML page.
	 *
	 * @param[in] qAcntId Account identifier.
	 */
	Q_INVOKABLE
	void getAccountData(const QmlAcntId *qAcntId);

	/*!
	 * @brief Get current long term storage type for QML page.
	 *
	 * @param[in] qAcntId Account identifier.
	 * @return Current long term storage type.
	 */
	Q_INVOKABLE
	int actDTType(const QmlAcntId *qAcntId);

	/*!
	 * @brief Check if current long term storage capacity is full.
	 *
	 * @param[in] qAcntId Account identifier.
	 * @return True if current long term storage capacity is full.
	 */
	Q_INVOKABLE
	bool isDTCapacityFull(const QmlAcntId *qAcntId);

	/*!
	 * @brief Create account.
	 *
	 * @param[in] loginMethod User name identifying account.
	 * @param[in] acntName Account name.
	 * @param[in] userName Account user name.
	 * @param[in] pwd Password.
	 * @param[in] mepToken Communication MEP code.
	 * @param[in] isTestAccount True if account is ISDS test environment.
	 * @param[in] rememberPwd True if remember password.
	 * @param[in] storeToDisk True if database store to local storage.
	 * @param[in] syncWithAll True if the account will be included into
	 *                        the synchronisation process of all accounts.
	 * @param[in] certPath Certificate path (can be null).
	 * @return Error text or NULL if success.
	 */
	Q_INVOKABLE static
	QString createAccount(
	    enum AcntData::LoginMethod loginMethod, const QString &acntName,
	    const QString &userName, const QString &pwd,const QString &mepToken,
	    bool isTestAccount, bool rememberPwd, bool storeToDisk,
	    bool syncWithAll, const QString &certPath);

	/*!
	 * @brief Update account.
	 *
	 * @param[in,out] accountModel Account model.
	 * @param[in] loginMethod User name identifying account.
	 * @param[in] acntName Account name.
	 * @param[in] userName Account user name.
	 * @param[in] pwd Password.
	 * @param[in] mepToken Communication MEP code.
	 * @param[in] isTestAccount True if account is ISDS test environment.
	 * @param[in] rememberPwd True if remember password.
	 * @param[in] storeToDisk True if database store to local storage.
	 * @param[in] syncWithAll True if the account will be included into
	 *                        the synchronisation process of all accounts.
	 * @param[in] certPath Certificate path (can be null).
	 * @return True if success.
	 */
	Q_INVOKABLE
	bool updateAccount(AccountListModel *accountModel,
	    enum AcntData::LoginMethod loginMethod, const QString &acntName,
	    const QString &userName, const QString &pwd, const QString &mepToken,
	    bool isTestAccount, bool rememberPwd, bool storeToDisk,
	    bool syncWithAll, const QString &certPath);

	/*!
	 * @brief Remove account.
	 *
	 * @param[in] qAcntId Account identifier.
	 */
	Q_INVOKABLE
	bool removeAccount(const QmlAcntId *qAcntId);

	/*!
	 * @brief Load accounts counters from database.
	 *
	 * @param[in,out] accountModel Model whose counters should be updated.
	 */
	Q_INVOKABLE static
	void loadModelCounters(AccountListModel *accountModel);

	/*!
	 * @brief Prepare change account user name.
	 *
	 * @param[in] loginMethod User name identifying account.
	 * @param[in] acntName Account name.
	 * @param[in] newUserName New user name identifying account.
	 * @param[in] pwd Password.
	 * @param[in] mepToken Communication MEP code.
	 * @param[in] isTestAccount True if account is ISDS test environment.
	 * @param[in] rememberPwd True if remember password.
	 * @param[in] storeToDisk True if database store to local storage.
	 * @param[in] syncWithAll True if the account will be included into
	 *                        the synchronisation process of all accounts.
	 * @param[in] certPath Certificate path (can be null).
	 * @return Error text or NULL if success.
	 */
	Q_INVOKABLE static
	QString prepareChangeUserName(
	    enum AcntData::LoginMethod loginMethod, const QString &acntName,
	    const QString &newUserName, const QString &pwd, const QString &mepToken,
	    bool isTestAccount, bool rememberPwd, bool storeToDisk,
	    bool syncWithAll, const QString &certPath);

	/*!
	 * @brief Change account user name and store account to settings.
	 *
	 * @param[in] newUserName New user name identifying account.
	 * @param[in] oldUserName Original user name of account.
	 * @param[in] storeToDisk True if database store to local storage.
	 * @param[in] newDbId Databox id of new user name.
	 * @return Error text or NULL if success.
	 */
	Q_INVOKABLE
	QString changeAccountUserName(
	    const QString &newUserName, const QString &oldUserName,
	    bool storeToDisk, const QString &newDbId);

	/*!
	 * @brief Show notification with usernames where password expires.
	 *
	 * @return Usernames where password expires.
	 */
	Q_INVOKABLE
	QString getPasswordExpirationInfo(void);

	/*!
	 * @brief Update last sync datetime.
	 *
	 * @param[in] qAcntId Account identifier.
	 */
	Q_INVOKABLE
	void updateLastSyncTime(const QmlAcntId *qAcntId);

	/*!
	 * @brief Load image file.
	 *
	 * @param[in] qAcntId Account identifier.
	 * @return Image file path.
	 */
	Q_INVOKABLE
	QString databoxLogo(const QmlAcntId *qAcntId);

	/*!
	 * @brief Store new user image file.
	 *
	 * @param[in] qAcntId Account identifier.
	 * @param[in] srcFilePath New source image file path.
	 * @return True on success.
	 */
	Q_INVOKABLE
	bool setDataboxLogo(const QmlAcntId *qAcntId,
	    const QString &srcFilePath);

	/*!
	 * @brief Reset user image file and set default image.
	 *
	 * @param[in] qAcntId Account identifier.
	 * @return True on success.
	 */
	Q_INVOKABLE
	bool resetDataboxLogo(const QmlAcntId *qAcntId);

signals:
	/*!
	 * @brief Send account settings to QML settings page.
	 *
	 * @param[in] acntName Account name.
	 * @param[in] userName Account user name.
	 * @param[in] loginMethod User name identifying account.
	 * @param[in] password Password.
	 * @param[in] mepToken Communication MEP code.
	 * @param[in] isTestAccount True if account is ISDS test environment.
	 * @param[in] rememberPwd True if remember password.
	 * @param[in] storeToDisk True if database store to local storage.
	 * @param[in] syncWithAll True if the account will be included into
	 *                        the synchronisation process of all accounts.
	 * @param[in] certPath Certificate path (can be null).
	 */
	void sendAccountData(QString acntName, QString userName,
	    enum AcntData::LoginMethod loginMethod, QString password,
	    QString mepToken, bool isTestAccount, bool rememberPwd,
	    bool storeToDisk, bool syncWithAll, QString certPath);

	/*!
	 * @brief Remove ISDS context for account in the ISDS session.
	 *
	 * @param[in] acntId Account identifier.
	 */
	void removeIsdsCtx(AcntId acntId);
};
