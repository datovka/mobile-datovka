/*
 * Copyright (C) 2014-2025 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

#include <QtGlobal> /* QT_VERSION, qVersion() */

#if defined (Q_OS_ANDROID)
#  if (QT_VERSION >= QT_VERSION_CHECK(6, 0, 0))
#    include "android_qt6/src/android_io.h"
#  else
#    include "android/src/android_io.h"
#  endif
#endif

#if defined (Q_OS_IOS)
#  include "ios/src/url_opener.h"
#endif

#include "src/datovka_shared/app_version_info.h"
#include "src/datovka_shared/compat_qt/random.h"
#include "src/datovka_shared/localisation/localisation.h"
#include "src/datovka_shared/log/log.h"
#include "src/datovka_shared/settings/pin.h"
#include "src/datovka_shared/settings/prefs.h"
#include "src/datovka_shared/settings/records_management.h"
#include "src/datovka_shared/utility/date_time.h"
#include "src/font/font.h"
#include "src/global.h"
#include "src/io/filesystem.h"
#include "src/settings/accounts.h"
#include "src/settings/ini_preferences.h"
#include "src/setwrapper.h"

GlobalSettingsQmlWrapper::GlobalSettingsQmlWrapper(QObject *parent)
    : QObject(parent),
    m_cleanAppStart(true)
{
}

QString GlobalSettingsQmlWrapper::appVersion(void)
{
	return QStringLiteral(VERSION);
}

bool GlobalSettingsQmlWrapper::downloadCompleteMsgs(void)
{
	if (GlobInstcs::prefsPtr != Q_NULLPTR) {
		bool val = false;
		return GlobInstcs::prefsPtr->boolVal(
		    "action.sync_account.tie.action.download_message", val) && val;
	} else {
		Q_ASSERT(0);
		return false;
	}
}

void GlobalSettingsQmlWrapper::setDownloadCompleteMsgs(bool downloadCompleteMsgs)
{
	if (GlobInstcs::prefsPtr != Q_NULLPTR) {
		GlobInstcs::prefsPtr->setBoolVal(
		    "action.sync_account.tie.action.download_message",
		    downloadCompleteMsgs);
	} else {
		Q_ASSERT(0);
	}
}

bool GlobalSettingsQmlWrapper::downloadOnlyNewMsgs(void)
{
	if (GlobInstcs::prefsPtr != Q_NULLPTR) {
		bool val = false;
		return GlobInstcs::prefsPtr->boolVal(
		    "action.sync_account.restrict_to.new_messages.enabled", val) && val;
	} else {
		Q_ASSERT(0);
		return false;
	}
}

void GlobalSettingsQmlWrapper::setDownloadOnlyNewMsgs(bool downloadOnlyNewMsgs)
{
	if (GlobInstcs::prefsPtr != Q_NULLPTR) {
		GlobInstcs::prefsPtr->setBoolVal(
		    "action.sync_account.restrict_to.new_messages.enabled",
		    downloadOnlyNewMsgs);
	} else {
		Q_ASSERT(0);
	}
}

QString GlobalSettingsQmlWrapper::dbPath(void)
{
	QString val = dfltDbAndConfigLoc();

	if ((GlobInstcs::prefsPtr != Q_NULLPTR) &&
	    GlobInstcs::prefsPtr->strVal("storage.database.path", val)) {
		return val;
	} else {
		Q_ASSERT(0);
		return val;
	}
}

void GlobalSettingsQmlWrapper::setDbPath(const QString &dbLocation)
{
	if (GlobInstcs::prefsPtr != Q_NULLPTR) {
		GlobInstcs::prefsPtr->setStrVal("storage.database.path",
		    !dbLocation.isEmpty() ? dbLocation : dfltDbAndConfigLoc());
	} else {
		Q_ASSERT(0);
	}
}

int GlobalSettingsQmlWrapper::fontSize(void)
{
	qint64 val = 16;

	if ((GlobInstcs::prefsPtr != Q_NULLPTR) &&
	    GlobInstcs::prefsPtr->intVal("font.size", val)) {
		return val;
	} else {
		Q_ASSERT(0);
		return val;
	}
}

void GlobalSettingsQmlWrapper::setFontSize(int fontSize)
{
	if (GlobInstcs::prefsPtr != Q_NULLPTR) {
		GlobInstcs::prefsPtr->setIntVal("font.size", fontSize);
	} else {
		Q_ASSERT(0);
	}
}

QString GlobalSettingsQmlWrapper::font(void)
{
	QString val = Font::fontSystem;

	if ((GlobInstcs::prefsPtr != Q_NULLPTR) &&
	    GlobInstcs::prefsPtr->strVal("font.type", val)) {
		return Font::fontCode(val);
	} else {
		Q_ASSERT(0);
		return Font::fontCode(val);
	}
}

void GlobalSettingsQmlWrapper::setFont(const QString &font)
{
	if (GlobInstcs::prefsPtr != Q_NULLPTR) {
		GlobInstcs::prefsPtr->setStrVal("font.type", Font::fontCode(font));
	} else {
		Q_ASSERT(0);
	}
}

QString GlobalSettingsQmlWrapper::language(void)
{
	QString val = Localisation::langSystem;

	if ((GlobInstcs::prefsPtr != Q_NULLPTR) &&
	    GlobInstcs::prefsPtr->strVal("translation.language", val)) {
		return val;
	} else {
		Q_ASSERT(0);
		return val;
	}
}

void GlobalSettingsQmlWrapper::setLanguage(const QString &language)
{
	if (GlobInstcs::prefsPtr != Q_NULLPTR) {
		GlobInstcs::prefsPtr->setStrVal("translation.language", language);
	} else {
		Q_ASSERT(0);
	}
}

QString GlobalSettingsQmlWrapper::pinValue(void)
{
	if (GlobInstcs::pinSetPtr != Q_NULLPTR) {
		return GlobInstcs::pinSetPtr->pinValue();
	} else {
		Q_ASSERT(0);
		return QString();
	}
}

void GlobalSettingsQmlWrapper::updatePinSettings(const QString &pinValue)
{
	if (GlobInstcs::pinSetPtr != Q_NULLPTR) {
		GlobInstcs::pinSetPtr->updatePinValue(pinValue);
	} else {
		Q_ASSERT(0);
	}
}

QString GlobalSettingsQmlWrapper::rmUrl(void)
{
	if (GlobInstcs::recMgmtSetPtr != Q_NULLPTR) {
		return GlobInstcs::recMgmtSetPtr->url();
	} else {
		Q_ASSERT(0);
		return QString();
	}
}

void GlobalSettingsQmlWrapper::setRmUrl(const QString &rmUrl)
{
	if (Q_UNLIKELY(GlobInstcs::recMgmtSetPtr == Q_NULLPTR)) {
		Q_ASSERT(0);
		return;
	}

	GlobInstcs::recMgmtSetPtr->setUrl(rmUrl);
}

QString GlobalSettingsQmlWrapper::rmToken(void)
{
	if (GlobInstcs::recMgmtSetPtr != Q_NULLPTR) {
		return GlobInstcs::recMgmtSetPtr->token();
	} else {
		Q_ASSERT(0);
		return QString();
	}
}

void GlobalSettingsQmlWrapper::setRmToken(const QString &rmToken)
{
	if (Q_UNLIKELY(GlobInstcs::recMgmtSetPtr == Q_NULLPTR)) {
		Q_ASSERT(0);
		return;
	}

	GlobInstcs::recMgmtSetPtr->setToken(rmToken);
}

bool GlobalSettingsQmlWrapper::pinConfigured(void)
{
	return GlobInstcs::pinSetPtr->pinConfigured();
}

bool GlobalSettingsQmlWrapper::pinValid(const QString &pinValue)
{
	return GlobInstcs::pinSetPtr->verifyPin(pinValue);
}

void GlobalSettingsQmlWrapper::verifyPin(const QString &pinValue)
{
	if (Q_UNLIKELY((GlobInstcs::pinSetPtr == Q_NULLPTR) ||
	        (GlobInstcs::acntMapPtr == Q_NULLPTR))) {
		Q_ASSERT(0);
		return;
	}

	if (!GlobInstcs::pinSetPtr->pinConfigured()) {
		return;
	}

	bool verResult = GlobInstcs::pinSetPtr->verifyPin(pinValue);
	if (verResult) {
		/* Decrypt all keys in account container. */
		GlobInstcs::acntMapPtr->decryptAllPwds(
		    GlobInstcs::pinSetPtr->pinValue());
		/* Decrypt records management token in global settings */
		if (GlobInstcs::recMgmtSetPtr != Q_NULLPTR) {
			GlobInstcs::recMgmtSetPtr->decryptToken(
			    GlobInstcs::pinSetPtr->pinValue());
		}
	}

	emit sendPinReply(verResult);

	if (verResult && m_cleanAppStart) {

		emit runOnAppStartUpSig();

		m_cleanAppStart = false;
	}
}

bool GlobalSettingsQmlWrapper::useExplicitClipboardOperations(void)
{
//#if defined Q_OS_LINUX /* For testing purposes. */
#if defined Q_OS_ANDROID
	return true;
#else /* !defined Q_OS_ANDROID */
	return false;
#endif /* defined Q_OS_ANDROID */
}

int GlobalSettingsQmlWrapper::inactivityInterval(void)
{
	qint64 val = 0;

	if ((GlobInstcs::prefsPtr != Q_NULLPTR) &&
	    GlobInstcs::prefsPtr->intVal("ui.inactivity.lock.timeout.ms", val)) {
		return val / 1000;
	} else {
		Q_ASSERT(0);
		return -1;
	}
}

void GlobalSettingsQmlWrapper::setInactivityInterval(int secs)
{
	if (GlobInstcs::prefsPtr != Q_NULLPTR) {
		GlobInstcs::prefsPtr->setIntVal("ui.inactivity.lock.timeout.ms",
		    (secs > 0) ? (secs * 1000) : 0);
	} else {
		Q_ASSERT(0);
	}
}

QString GlobalSettingsQmlWrapper::lastUpdateStr(void)
{
	QDateTime val;
	if (GlobInstcs::prefsPtr != Q_NULLPTR) {
		GlobInstcs::prefsPtr->dateTimeVal(
		    "action.sync_account.last_invocation.datetime", val);
	} else {
		Q_ASSERT(0);
	}
	if (Q_UNLIKELY(!val.isValid())) {
		return QString();
	}
	val = val.toLocalTime();
	QDateTime currentDateTime(QDateTime::currentDateTime());

#define DATE_FMT QStringLiteral("yyyy-MM-dd")
	if (val.toString(DATE_FMT) == currentDateTime.toString(DATE_FMT)) {
		/* Only time. */
		return val.toString(Utility::timeDisplayFormat);
	} else {
		/* Only date. */
		return val.toString(Utility::dateDisplayFormat);
	}
#undef DATE_FMT
}

void GlobalSettingsQmlWrapper::setLastUpdateToNow(void)
{
	if (GlobInstcs::prefsPtr != Q_NULLPTR) {
		GlobInstcs::prefsPtr->setDateTimeVal(
		    "action.sync_account.last_invocation.datetime",
		    QDateTime::currentDateTimeUtc());
	} else {
		Q_ASSERT(0);
	}
}

int GlobalSettingsQmlWrapper::debugVerbosityLevel(void)
{
	if (GlobInstcs::logPtr != Q_NULLPTR) {
		return GlobInstcs::logPtr->debugVerbosity();
	} else {
		Q_ASSERT(0);
		return 0;
	}
}

void GlobalSettingsQmlWrapper::setDebugVerbosityLevel(int dVL)
{
	if ((GlobInstcs::logPtr != Q_NULLPTR) &&
	    (GlobInstcs::prefsPtr != Q_NULLPTR)) {
		/* Set verbosity directly and store it to settings. */
		GlobInstcs::logPtr->setDebugVerbosity(dVL);
		GlobInstcs::prefsPtr->setIntVal("logging.verbosity.level.debug",
		    dVL);
	} else {
		Q_ASSERT(0);
	}
}

int GlobalSettingsQmlWrapper::logVerbosityLevel(void)
{
	if (GlobInstcs::logPtr != Q_NULLPTR) {
		return GlobInstcs::logPtr->logVerbosity();
	} else {
		Q_ASSERT(0);
		return 0;
	}
}

void GlobalSettingsQmlWrapper::setLogVerbosityLevel(int lVL)
{
	if ((GlobInstcs::logPtr != Q_NULLPTR) &&
	    (GlobInstcs::prefsPtr != Q_NULLPTR)) {
		GlobInstcs::logPtr->setLogVerbosity(lVL);
		GlobInstcs::prefsPtr->setIntVal("logging.verbosity.level.log",
		    lVL);
	} else {
		Q_ASSERT(0);
	}
}

bool GlobalSettingsQmlWrapper::cleanAppStart(void) const
{
	return m_cleanAppStart;
}

void GlobalSettingsQmlWrapper::setCleanAppStart(bool val)
{
	m_cleanAppStart = val;
}

bool GlobalSettingsQmlWrapper::syncAfterCleanAppStart(void)
{
	if (GlobInstcs::prefsPtr != Q_NULLPTR) {
		bool val = false;
		return GlobInstcs::prefsPtr->boolVal(
		    "page.account_list.on.start.tie.action.sync_all_accounts", val) && val;
	} else {
		Q_ASSERT(0);
		return false;
	}
}

void GlobalSettingsQmlWrapper::setSyncAfterCleanAppStart(bool syncAfterAppStart)
{
	if (GlobInstcs::prefsPtr != Q_NULLPTR) {
		GlobInstcs::prefsPtr->setBoolVal(
		    "page.account_list.on.start.tie.action.sync_all_accounts",
		    syncAfterAppStart);
	} else {
		Q_ASSERT(0);
	}
}

int GlobalSettingsQmlWrapper::pwdExpirationDays(void)
{
	qint64 val = 0;

	if ((GlobInstcs::prefsPtr != Q_NULLPTR) &&
	    GlobInstcs::prefsPtr->intVal("accounts.password.expiration.notify_ahead.days", val)) {
		return val;
	} else {
		Q_ASSERT(0);
		return val;
	}
}

void GlobalSettingsQmlWrapper::setPwdExpirationDays(int days)
{
	if (GlobInstcs::prefsPtr != Q_NULLPTR) {
		GlobInstcs::prefsPtr->setIntVal(
		    "accounts.password.expiration.notify_ahead.days", days);
	} else {
		Q_ASSERT(0);
	}
}

int GlobalSettingsQmlWrapper::msgDeletionNotifyAheadDays(void)
{
	qint64 val = 7;

	if ((GlobInstcs::prefsPtr != Q_NULLPTR) &&
	    GlobInstcs::prefsPtr->intVal("accounts.message.deletion.notify_ahead.days", val)) {
		return val;
	} else {
		Q_ASSERT(0);
		return val;
	}
}

int GlobalSettingsQmlWrapper::bannerVisibilityCount(void)
{
	qint64 val = 3;

	if ((GlobInstcs::prefsPtr != Q_NULLPTR) &&
	        GlobInstcs::prefsPtr->intVal(
	        "page.account_list.on.start.banner.mojeid2.visibility.count", val)) {
		return val;
	} else {
		Q_ASSERT(0);
		return val;
	}
}

void GlobalSettingsQmlWrapper::setBannerVisibilityCount(int cnt)
{
	if (GlobInstcs::prefsPtr != Q_NULLPTR) {
		GlobInstcs::prefsPtr->setIntVal(
		    "page.account_list.on.start.banner.mojeid2.visibility.count",
		    cnt);
	} else {
		Q_ASSERT(0);
	}
}

bool GlobalSettingsQmlWrapper::isVodzEnbaled(void)
{
	if (GlobInstcs::prefsPtr != Q_NULLPTR) {
		bool val = false;
		return GlobInstcs::prefsPtr->boolVal(
		    "send_message.vodz.enable", val) && val;
	} else {
		Q_ASSERT(0);
		return false;
	}
}

bool GlobalSettingsQmlWrapper::darkTheme(void)
{
	if (GlobInstcs::prefsPtr != Q_NULLPTR) {
		bool val = false;
		return GlobInstcs::prefsPtr->boolVal("ui.dark_theme", val) && val;
	} else {
		Q_ASSERT(0);
		return false;
	}
}

void GlobalSettingsQmlWrapper::setDarkTheme(bool darkTheme)
{
	if (GlobInstcs::prefsPtr != Q_NULLPTR) {
		GlobInstcs::prefsPtr->setBoolVal("ui.dark_theme", darkTheme);
	} else {
		Q_ASSERT(0);
	}
}

bool GlobalSettingsQmlWrapper::isIos(void)
{
#ifdef Q_OS_IOS
	return true;
#else
	return false;
#endif
}

QDateTime GlobalSettingsQmlWrapper::appFirstLaunchTime(void)
{
	QDateTime val;
	if (GlobInstcs::prefsPtr != Q_NULLPTR) {
		GlobInstcs::prefsPtr->dateTimeVal(
		    "stats.app.launch.first.datetime", val);
	} else {
		Q_ASSERT(0);
	}
	if (Q_UNLIKELY(!val.isValid())) {
		return QDateTime();
	}
	return val;
}

void GlobalSettingsQmlWrapper::storeAppFirstLaunchTime(void)
{
	if (GlobInstcs::prefsPtr != Q_NULLPTR) {
		GlobInstcs::prefsPtr->setDateTimeVal(
		    "stats.app.launch.first.datetime",
		    QDateTime::currentDateTime());
	} else {
		Q_ASSERT(0);
	}
}

QDateTime GlobalSettingsQmlWrapper::appLastLaunchTime(void)
{
	QDateTime val;
	if (GlobInstcs::prefsPtr != Q_NULLPTR) {
		GlobInstcs::prefsPtr->dateTimeVal(
		    "stats.app.launch.last.datetime", val);
	} else {
		Q_ASSERT(0);
	}
	if (Q_UNLIKELY(!val.isValid())) {
		return QDateTime();
	}

	return val;
}

void GlobalSettingsQmlWrapper::storeAppLastLaunchTime(void)
{
	if (GlobInstcs::prefsPtr != Q_NULLPTR) {
		GlobInstcs::prefsPtr->setDateTimeVal(
		    "stats.app.launch.last.datetime",
		    QDateTime::currentDateTime());
	} else {
		Q_ASSERT(0);
	}
}

QDateTime GlobalSettingsQmlWrapper::appPlannedDonationViewTime(void)
{
	QDateTime val;
	if (GlobInstcs::prefsPtr != Q_NULLPTR) {
		GlobInstcs::prefsPtr->dateTimeVal(
		    "stats.app.action.planned_donation_view.datetime", val);
	} else {
		Q_ASSERT(0);
	}
	if (Q_UNLIKELY(!val.isValid())) {
		return QDateTime();
	}

	return val;
}

void GlobalSettingsQmlWrapper::setAppPlannedDonationViewTime(
    const QDateTime &nextViewTime)
{
	if (GlobInstcs::prefsPtr != Q_NULLPTR) {
		GlobInstcs::prefsPtr->setDateTimeVal(
		    "stats.app.action.planned_donation_view.datetime", nextViewTime);
	} else {
		Q_ASSERT(0);
	}
}

QDateTime GlobalSettingsQmlWrapper::appPlannedRatingViewTime(void)
{
	QDateTime val;
	if (GlobInstcs::prefsPtr != Q_NULLPTR) {
		GlobInstcs::prefsPtr->dateTimeVal(
		    "stats.app.action.planned_rating_view.datetime", val);
	} else {
		Q_ASSERT(0);
	}
	if (Q_UNLIKELY(!val.isValid())) {
		return QDateTime();
	}

	return val;
}

void GlobalSettingsQmlWrapper::setAppPlannedRatingViewTime(
    const QDateTime &nextViewTime)
{
	if (GlobInstcs::prefsPtr != Q_NULLPTR) {
		GlobInstcs::prefsPtr->setDateTimeVal(
		    "stats.app.action.planned_rating_view.datetime", nextViewTime);
	} else {
		Q_ASSERT(0);
	}
}

qint64 GlobalSettingsQmlWrapper::appLaunchCount(void)
{
	qint64 val = -1;

	if ((GlobInstcs::prefsPtr != Q_NULLPTR) &&
	        GlobInstcs::prefsPtr->intVal(
	        "stats.app.launch.count", val)) {
		return val;
	} else {
		Q_ASSERT(0);
		return val;
	}
}

void GlobalSettingsQmlWrapper::incrementLaunchCount(void)
{
	if (GlobInstcs::prefsPtr != Q_NULLPTR) {
		GlobInstcs::prefsPtr->setIntVal(
		    "stats.app.launch.count", appLaunchCount() + 1);
	} else {
		Q_ASSERT(0);
	}
}

void GlobalSettingsQmlWrapper::planNextDonationViewTime(bool shortTime)
{
	int intervalDays = 0;

	if (shortTime) {
		/* Short Time Interval: 3 months, tolerance +- 15 days */
		intervalDays = Compat::randBoundedInterval(75, 105);
	} else {
		/* Long Time Interval: 6 months, tolerance +- 30 days */
		intervalDays = Compat::randBoundedInterval(150, 210);
	}

	setAppPlannedDonationViewTime(
	    QDateTime::currentDateTimeUtc().addDays(intervalDays));
}

void GlobalSettingsQmlWrapper::planNextRatingViewTime(bool shortTime)
{
	int intervalDays = 0;

	if (shortTime) {
		/* Short Time Interval: 3 months, tolerance +- 15 days */
		intervalDays = Compat::randBoundedInterval(75, 105);
	} else {
		/* Long Time Interval: 6 months, tolerance +- 30 days */
		intervalDays = Compat::randBoundedInterval(150, 210);
	}

	setAppPlannedRatingViewTime(
	    QDateTime::currentDateTimeUtc().addDays(intervalDays));
}

bool GlobalSettingsQmlWrapper::isTimeToShowDonationDlg(void)
{
	bool showDlg = false;
	const QDateTime nextViewTime = appPlannedDonationViewTime();

	if (Q_UNLIKELY(!nextViewTime.isValid())) {
		planNextDonationViewTime(false);
		return false;
	}

	showDlg = (nextViewTime < QDateTime::currentDateTimeUtc());
	if (Q_UNLIKELY(showDlg)) {
		planNextDonationViewTime(false);
	}

	return showDlg;
}

bool GlobalSettingsQmlWrapper::isTimeToShowRatingDlg(void)
{
	bool showDlg = false;
	const QDateTime nextViewTime = appPlannedRatingViewTime();

	if (Q_UNLIKELY(!nextViewTime.isValid())) {
		planNextRatingViewTime(true);
		return false;
	}

	showDlg = (nextViewTime < QDateTime::currentDateTimeUtc());
	if (Q_UNLIKELY(showDlg)) {
		planNextRatingViewTime(false);
	}

	return showDlg;
}

void GlobalSettingsQmlWrapper::openRatingDlg(void)
{
#if defined (Q_OS_ANDROID)
	//AndroidIO::openAppRatingPage();
	AndroidIO::openAppRatingDlg();
#endif

#if defined (Q_OS_IOS)
	UrlOpener::openAppRatingDlg();
#endif
}

bool GlobalSettingsQmlWrapper::useQmlFileDialog(void)
{
	if (GlobInstcs::prefsPtr != Q_NULLPTR) {
		bool val = false;
		return GlobInstcs::prefsPtr->boolVal(
		    "ui.action.file_dialogue.qml", val) && val;
	} else {
		Q_ASSERT(0);
		return false;
	}
}

bool GlobalSettingsQmlWrapper::useIosDocumentPicker(void)
{
	if (GlobInstcs::prefsPtr != Q_NULLPTR) {
		bool val = false;
		return GlobInstcs::prefsPtr->boolVal(
		    "ui.action.file_dialogue.ios_native", val) && val;
	} else {
		Q_ASSERT(0);
		return false;
	}
}

bool GlobalSettingsQmlWrapper::isNewVersion(void)
{
	if (Q_UNLIKELY(Q_NULLPTR == GlobInstcs::prefsPtr)) {
		Q_ASSERT(0);
		return false;
	}

	bool isNew = true;
	QString storedVersion;

	/*
	 * If running app doesn't have release version then don't check.
	 */
	if (Q_UNLIKELY(!AppVersionInfo::isReleaseVersionString(VERSION))) {
		return false;
	}

	GlobInstcs::prefsPtr->strVal("application.notification_shown.last_version", storedVersion);

	/*
	 * If stored version is empty or non-release version string then behave
	 * as having a new version.
	 */
	isNew = storedVersion.isEmpty()
	    || (!AppVersionInfo::isReleaseVersionString(storedVersion))
	    || (1 == AppVersionInfo::compareVersionStrings(VERSION, storedVersion));

	if (isNew) {
		GlobInstcs::prefsPtr->setStrVal("application.notification_shown.last_version", VERSION);
	}

	return isNew;
}

QString GlobalSettingsQmlWrapper::loadChangeLogText(void)
{
	return AppVersionInfo::releaseNewsText();
}
