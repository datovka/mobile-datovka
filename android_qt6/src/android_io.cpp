/*
 * Copyright (C) 2014-2025 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

#include <QtGlobal>

#if (QT_VERSION < QT_VERSION_CHECK(6, 0, 0))
    #include <QAndroidJniObject>
    #include <QtAndroidExtras>
#else
    #include <QtCore/private/qandroidextras_p.h>
#endif

#include <QJniObject>
#include <QJsonArray>
#include <QJsonDocument>
#include <QJsonObject>

#include "android_qt6/src/android_io.h"

AndroidIO::AndroidIO(void)
{
}

bool AndroidIO::createEmail(const QString &subject, const QString &to,
    const QString &body, const QStringList &filePaths)
{
	QJniObject jsSubject = QJniObject::fromString(subject);
	QJniObject jsTo = QJniObject::fromString(to);
	QJniObject jsBody = QJniObject::fromString(body);
	QJsonArray arr = QJsonArray::fromStringList(filePaths);
	QJsonObject jsonObj;
	jsonObj[QLatin1String("attachments")] = arr;
	QJsonDocument doc(jsonObj);
	QJniObject jsJsonPathList = QJniObject::fromString(doc.toJson(QJsonDocument::Compact));
	jboolean ok = QJniObject::callStaticMethod<jboolean>(
	     "cz/nic/mobiledatovka/java/QFileProvider",
	     "sendEmail", "(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z",
	     jsSubject.object<jstring>(), jsTo.object<jstring>(),
	     jsBody.object<jstring>(), jsJsonPathList.object<jstring>());
	return (ok);
}

bool AndroidIO::openAppRatingPage(void)
{
	jboolean ok = QJniObject::callStaticMethod<jboolean>(
	     "cz/nic/mobiledatovka/java/QFileProvider",
	     "openAppRatingPage");
	return (ok);
}

bool AndroidIO::openAppRatingDlg(void)
{
	jboolean ok = QJniObject::callStaticMethod<jboolean>(
	     "cz/nic/mobiledatovka/java/QFileProvider",
	     "openAppRatingDlg");
	return (ok);
}

QString AndroidIO::getRealFilePath(const QString &filePath)
{
	return QString();
}

bool AndroidIO::isSDKVersion30OrNewest(void)
{
	return true;
}

bool AndroidIO::openFile(const QString &filePath)
{
	return true;
}

bool AndroidIO::checkAndAskStoragePermissions(void)
{
#if (QT_VERSION < QT_VERSION_CHECK(6, 0, 0))
	auto result = QtAndroid::checkPermission(
	    QString("android.permission.WRITE_EXTERNAL_STORAGE"));
	if (result == QtAndroid::PermissionResult::Denied) {
		QtAndroid::PermissionResultMap resultHash =
		    QtAndroid::requestPermissionsSync(
		    QStringList({"android.permission.WRITE_EXTERNAL_STORAGE"}));
		return !(resultHash["android.permission.WRITE_EXTERNAL_STORAGE"]
		    == QtAndroid::PermissionResult::Denied);
	}
#else
	auto r = QtAndroidPrivate::checkPermission(
	    "android.permission.WRITE_EXTERNAL_STORAGE").result();
	if (r != QtAndroidPrivate::Authorized) {
		r = QtAndroidPrivate::requestPermission(
		    "android.permission.WRITE_EXTERNAL_STORAGE").result();
		if (r == QtAndroidPrivate::Denied) {
			return false;
		}
	}
	r = QtAndroidPrivate::checkPermission(
	    "android.permission.READ_MEDIA_IMAGES").result();
	if (r != QtAndroidPrivate::Authorized) {
		r = QtAndroidPrivate::requestPermission(
		    "android.permission.READ_MEDIA_IMAGES").result();
		if (r == QtAndroidPrivate::Denied) {
			return false;
		}
	}
#endif
	return true;
}
