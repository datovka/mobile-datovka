<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="uk_UA">
<context>
    <name>AccessibleSpinBox</name>
    <message>
        <location filename="../../qml/components/AccessibleSpinBox.qml" line="69"/>
        <location filename="../../qml/components/AccessibleSpinBox.qml" line="69"/>
        <source>Decrease value &apos;%1&apos;.</source>
        <translation>Зменшити значення&apos;%1&apos;.</translation>
    </message>
    <message>
        <location filename="../../qml/components/AccessibleSpinBox.qml" line="102"/>
        <location filename="../../qml/components/AccessibleSpinBox.qml" line="102"/>
        <source>Increase value &apos;%1&apos;.</source>
        <translation>Збільшити значення &apos;%1&apos;.</translation>
    </message>
</context>
<context>
    <name>AccessibleSpinBoxZeroMax</name>
    <message>
        <location filename="../../qml/components/AccessibleSpinBoxZeroMax.qml" line="32"/>
        <location filename="../../qml/components/AccessibleSpinBoxZeroMax.qml" line="32"/>
        <source>max</source>
        <translation>макс</translation>
    </message>
    <message>
        <location filename="../../qml/components/AccessibleSpinBoxZeroMax.qml" line="92"/>
        <location filename="../../qml/components/AccessibleSpinBoxZeroMax.qml" line="92"/>
        <source>Decrease value &apos;%1&apos;.</source>
        <translation>Зменшити значення&apos;%1&apos;.</translation>
    </message>
    <message>
        <location filename="../../qml/components/AccessibleSpinBoxZeroMax.qml" line="125"/>
        <location filename="../../qml/components/AccessibleSpinBoxZeroMax.qml" line="125"/>
        <source>Increase value &apos;%1&apos;.</source>
        <translation>Збільшити значення &apos;%1&apos;.</translation>
    </message>
</context>
<context>
    <name>AccessibleTextHelp</name>
    <message>
        <location filename="../../qml/components/AccessibleTextHelp.qml" line="89"/>
        <location filename="../../qml/components/AccessibleTextHelp.qml" line="89"/>
        <source>Help</source>
        <translation>Допомога</translation>
    </message>
</context>
<context>
    <name>AccountList</name>
    <message>
        <location filename="../../qml/components/AccountList.qml" line="85"/>
        <location filename="../../qml/components/AccountList.qml" line="85"/>
        <source>Show messages of data box &apos;%1&apos;. New or unread %2 of %3 received messages.</source>
        <translation>Показати повідомлення скриньки із даними &apos;%1&apos;. Нові або непрочитані повідомлення - %2 з %3 отриманих.</translation>
    </message>
    <message>
        <location filename="../../qml/components/AccountList.qml" line="141"/>
        <location filename="../../qml/components/AccountList.qml" line="141"/>
        <source>New messages: %1</source>
        <translation>Нові повідомлення: %1</translation>
    </message>
    <message>
        <location filename="../../qml/components/AccountList.qml" line="158"/>
        <location filename="../../qml/components/AccountList.qml" line="158"/>
        <source>Last synchronisation</source>
        <translation>Остання синхронізація</translation>
    </message>
    <message>
        <location filename="../../qml/components/AccountList.qml" line="167"/>
        <location filename="../../qml/components/AccountList.qml" line="167"/>
        <source>Received</source>
        <translation>Отримані</translation>
    </message>
    <message>
        <location filename="../../qml/components/AccountList.qml" line="167"/>
        <location filename="../../qml/components/AccountList.qml" line="167"/>
        <source>Sent</source>
        <translation>Надіслані</translation>
    </message>
    <message>
        <location filename="../../qml/components/AccountList.qml" line="175"/>
        <location filename="../../qml/components/AccountList.qml" line="175"/>
        <source>Synchronise data box &apos;%1&apos;.</source>
        <translation>Синхронізувати скриньку &quot;%1&quot;.</translation>
    </message>
    <message>
        <location filename="../../qml/components/AccountList.qml" line="191"/>
        <location filename="../../qml/components/AccountList.qml" line="191"/>
        <source>Create and send new message from data box &apos;%1&apos;.</source>
        <translation>Створити та надіслати нове повідомлення зі скриньки &quot;%1&quot;.</translation>
    </message>
    <message>
        <location filename="../../qml/components/AccountList.qml" line="197"/>
        <location filename="../../qml/components/AccountList.qml" line="197"/>
        <source>Data-box properties &apos;%1&apos;.</source>
        <translation>Властивості скриньки &quot;%1&quot;.</translation>
    </message>
</context>
<context>
    <name>Accounts</name>
    <message>
        <location filename="../../src/wrap_accounts.cpp" line="261"/>
        <source>Account with username &apos;%1&apos; already exists.</source>
        <translation>Акаунт з іменем користувача &apos;%1&apos; вже існує.</translation>
    </message>
    <message>
        <source>Cannot access data-box model.</source>
        <translation type="vanished">Неможливо отримати дані зі списку поштових скриньок.</translation>
    </message>
    <message>
        <location filename="../../src/wrap_accounts.cpp" line="237"/>
        <source>Cannot access data-box data.</source>
        <translation>Неможливо отримати дані зі списку поштових скриньок.</translation>
    </message>
    <message>
        <location filename="../../src/wrap_accounts.cpp" line="243"/>
        <source>Username, communication code or data-box name has not been specified. These fields must be filled in.</source>
        <translation>Ім’я користувача, код зв’язку або назва скриньки не вказані. Ці поля необхідно заповнити.</translation>
    </message>
    <message>
        <location filename="../../src/wrap_accounts.cpp" line="247"/>
        <source>Username, password or data-box name has not been specified. These fields must be filled in.</source>
        <translation>Ім&apos;я користувача, пароль або назва скриньки не вказані. Ці поля необхідно заповнити.</translation>
    </message>
    <message>
        <location filename="../../src/wrap_accounts.cpp" line="514"/>
        <source>Cannot access file database for the username &apos;%1&apos;.</source>
        <translation>Не вдається отримати доступ до бази даних файлів користувача &apos;%1&apos;.</translation>
    </message>
    <message>
        <location filename="../../src/wrap_accounts.cpp" line="523"/>
        <source>Cannot change file database to username &apos;%1&apos;.</source>
        <translation>Неможливо змінити файл бази даних користувача &apos;%1&apos;.</translation>
    </message>
    <message>
        <location filename="../../src/wrap_accounts.cpp" line="550"/>
        <source>Cannot access message database for username &apos;%1&apos;.</source>
        <translation>Не вдається отримати доступ до бази даних повідомлень для користувача &apos;%1&apos;.</translation>
    </message>
    <message>
        <location filename="../../src/wrap_accounts.cpp" line="559"/>
        <source>Cannot change message database to username &apos;%1&apos;.</source>
        <translation>Неможливо змінити базу даних повідомлень користувача &apos;%1&apos;.</translation>
    </message>
    <message>
        <location filename="../../src/wrap_accounts.cpp" line="583"/>
        <source>Internal error.</source>
        <translation>Внутрішня помилка.</translation>
    </message>
    <message>
        <location filename="../../src/wrap_accounts.cpp" line="589"/>
        <source>Data-box identifier related to the new username &apos;%1&apos; doesn&apos;t correspond with the data-box identifier related to the old username &apos;%2&apos;.</source>
        <translation>Ідентифікатор скриньки, пов’язаний із новим ім’ям користувача &quot;%1&quot;, не відповідає ідентифікатору скриньки, пов’язаним зі старим ім’ям користувача &quot;%2&quot;.</translation>
    </message>
    <message>
        <location filename="../../src/wrap_accounts.cpp" line="595"/>
        <source>Cannot change the file database to match the new username &apos;%1&apos;.</source>
        <translation>Неможливо змінити базу даних файлів, щоб вона відповідала новому імені користувача &quot;%1&quot;.</translation>
    </message>
    <message>
        <location filename="../../src/wrap_accounts.cpp" line="602"/>
        <source>Cannot change the message database to match the new username &apos;%1&apos;.</source>
        <translation>Неможливо змінити базу даних повідомлень, щоб вона відповідала новому імені користувача &quot;%1&quot;.</translation>
    </message>
</context>
<context>
    <name>AppVersionInfo</name>
    <message>
        <location filename="../../src/app_version_info.cpp" line="35"/>
        <source>Displaying release news in the application.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>BackupRestoreZipData</name>
    <message>
        <location filename="../../src/backup_zip.cpp" line="95"/>
        <source>unknown</source>
        <translation>невідомий/а</translation>
    </message>
    <message>
        <location filename="../../src/backup_zip.cpp" line="581"/>
        <source>From backup</source>
        <translation>З резервної копії</translation>
    </message>
    <message>
        <location filename="../../src/backup_zip.cpp" line="706"/>
        <source>Cannot open or read file &apos;%1&apos;.</source>
        <translation>Не вдається відкрити або прочитати файл &apos;%1&apos;.</translation>
    </message>
    <message>
        <location filename="../../src/backup_zip.cpp" line="720"/>
        <source>Select accounts from the backup ZIP file which you want to restore. Data of existing accounts will be replaced by data from the backup.</source>
        <translation>Виберіть акаунти з резервного ZIP-файлу, який потрібно відновити. Дані існуючих акаунтів будуть замінені даними з резервної копії.</translation>
    </message>
    <message>
        <location filename="../../src/backup_zip.cpp" line="818"/>
        <location filename="../../src/backup_zip.cpp" line="936"/>
        <location filename="../../src/backup_zip.cpp" line="1573"/>
        <source>There is not enough space in the selected storage.</source>
        <translation>У вибраному сховищі недостатньо місця.</translation>
    </message>
    <message>
        <location filename="../../src/backup_zip.cpp" line="841"/>
        <location filename="../../src/backup_zip.cpp" line="848"/>
        <source>The selected file isn&apos;t a valid ZIP archive containing a backup.</source>
        <translation>Вибраний файл не є дійсним архівом ZIP, що містить резервну копію.</translation>
    </message>
    <message>
        <location filename="../../src/backup_zip.cpp" line="887"/>
        <source>The selected ZIP archive doesn&apos;t contain any JSON file.</source>
        <translation>Вибраний архів ZIP не містить файлів JSON.</translation>
    </message>
    <message>
        <location filename="../../src/backup_zip.cpp" line="896"/>
        <source>JSON file &apos;%1&apos; does not contain valid application information.</source>
        <translation>Файл JSON &quot;%1&quot; не містить дійсної інформації про програму.</translation>
    </message>
    <message>
        <location filename="../../src/backup_zip.cpp" line="912"/>
        <source>Backup was taken at %1</source>
        <translation>Резервну копію зроблено на %1</translation>
    </message>
    <message numerus="yes">
        <location filename="../../src/backup_zip.cpp" line="913"/>
        <location filename="../../src/backup_zip.cpp" line="925"/>
        <source>and contains %n file(s).</source>
        <translation>
            <numerusform>і містить %n файл.</numerusform>
            <numerusform>і містить %n файли.</numerusform>
            <numerusform>і містить %n файлів.</numerusform>
        </translation>
    </message>
    <message>
        <location filename="../../src/backup_zip.cpp" line="920"/>
        <source>Warning: Application version &apos;%1&apos; does not match the transfer version &apos;%2&apos;. Is is recommended to transfer data between same versions of the application to prevent incompatibility issues.</source>
        <translation>Попередження: версія програми &quot;%1&quot; не відповідає версії &quot;%2&quot;, оголошеній під час передачі. Рекомендується передавати дані між тими самими версіями програми, щоб уникнути проблем із несумісністю.</translation>
    </message>
    <message>
        <location filename="../../src/backup_zip.cpp" line="924"/>
        <source>Transfer ZIP archive was taken at %1</source>
        <translation>ZIP-архів для передачі був створений в %1</translation>
    </message>
    <message>
        <location filename="../../src/backup_zip.cpp" line="932"/>
        <source>Restore all application data from transfer ZIP file. The current application data will be complete rewritten with new data from transfer backup.</source>
        <translation>Відновити всі дані програми з переданого ZIP-файлу. Поточні дані програми будуть повністю переписані новими даними з резервної копії передачі.</translation>
    </message>
    <message>
        <location filename="../../src/backup_zip.cpp" line="942"/>
        <source>Unknown backup type. JSON file contains no valid backup data.</source>
        <translation>Невідомий тип резервної копії. Файл JSON не містить дійсних даних резервної копії.</translation>
    </message>
    <message>
        <location filename="../../src/backup_zip.cpp" line="959"/>
        <location filename="../../src/backup_zip.cpp" line="1049"/>
        <source>Cannot open ZIP archive.</source>
        <translation>Не вдається відкрити ZIP-архів.</translation>
    </message>
    <message>
        <location filename="../../src/backup_zip.cpp" line="990"/>
        <location filename="../../src/backup_zip.cpp" line="1119"/>
        <source>Restoring file &apos;%1&apos;.</source>
        <translation>Відновлюю файл &quot;%1&quot;.</translation>
    </message>
    <message>
        <location filename="../../src/backup_zip.cpp" line="1024"/>
        <location filename="../../src/backup_zip.cpp" line="1158"/>
        <source>Restoration finished.</source>
        <translation>Відновлення закінчено.</translation>
    </message>
    <message>
        <location filename="../../src/backup_zip.cpp" line="1239"/>
        <source>Transferring file &apos;%1&apos;.</source>
        <translation>Передача файлу &quot;%1&quot;.</translation>
    </message>
    <message>
        <location filename="../../src/backup_zip.cpp" line="1274"/>
        <source>Transfer finished.</source>
        <translation>Передача закінчена.</translation>
    </message>
    <message>
        <location filename="../../src/backup_zip.cpp" line="1342"/>
        <location filename="../../src/backup_zip.cpp" line="1365"/>
        <location filename="../../src/backup_zip.cpp" line="1386"/>
        <location filename="../../src/backup_zip.cpp" line="1465"/>
        <location filename="../../src/backup_zip.cpp" line="1481"/>
        <source>Backing up file &apos;%1&apos;.</source>
        <translation>Створення резервної копії файлу &quot;%1&quot;.</translation>
    </message>
    <message>
        <location filename="../../src/backup_zip.cpp" line="1503"/>
        <source>Backup finished.</source>
        <translation>Резервне копіювання завершено.</translation>
    </message>
    <message>
        <location filename="../../src/backup_zip.cpp" line="1583"/>
        <source>Required space</source>
        <translation>Необхідний простір</translation>
    </message>
    <message>
        <location filename="../../src/backup_zip.cpp" line="1584"/>
        <source>Available space</source>
        <translation>Доступний простір</translation>
    </message>
</context>
<context>
    <name>ChangeLogBox</name>
    <message>
        <location filename="../../qml/components/ChangeLogBox.qml" line="56"/>
        <location filename="../../qml/components/ChangeLogBox.qml" line="56"/>
        <source>Version</source>
        <translation type="unfinished">Версія</translation>
    </message>
    <message>
        <location filename="../../qml/components/ChangeLogBox.qml" line="63"/>
        <location filename="../../qml/components/ChangeLogBox.qml" line="63"/>
        <source>What&apos;s new?</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Connection</name>
    <message>
        <location filename="../../src/isds/io/connection.cpp" line="695"/>
        <source>Success.</source>
        <translation>Успіх.</translation>
    </message>
    <message>
        <location filename="../../src/isds/io/connection.cpp" line="698"/>
        <source>Successfully finished.</source>
        <translation>Успішно закінчено.</translation>
    </message>
    <message>
        <location filename="../../src/isds/io/connection.cpp" line="701"/>
        <source>Internet connection is probably not available. Check your network settings.</source>
        <translation>Можливо, підключення до Інтернету недоступне. Перевірте налаштування мережі.</translation>
    </message>
    <message>
        <location filename="../../src/isds/io/connection.cpp" line="704"/>
        <source>Authorization failed. Server complains about a bad request.</source>
        <translation>Помилка авторизації. Сервер повертає попередження про неправильний запит.</translation>
    </message>
    <message>
        <location filename="../../src/isds/io/connection.cpp" line="707"/>
        <source>Error reply.</source>
        <translation>Помилкова відповідь.</translation>
    </message>
    <message>
        <location filename="../../src/isds/io/connection.cpp" line="710"/>
        <source>ISDS server is out of service. Scheduled maintenance in progress. Try again later.</source>
        <translation>Сервер ISDS не працює. Триває планове технічне обслуговування. Спробуйте ще раз пізніше.</translation>
    </message>
    <message>
        <location filename="../../src/isds/io/connection.cpp" line="713"/>
        <source>Connection with ISDS server timed out. Request was cancelled.</source>
        <translation>Час очікування з’єднання з сервером ISDS закінчився. Запит скасовано.</translation>
    </message>
    <message>
        <location filename="../../src/isds/io/connection.cpp" line="716"/>
        <source>Authorization failed. Check your credentials in the data-box settings whether they are correct and try again. It is also possible that your password expired. Check your credentials validity by logging in to the data box using the ISDS web portal.</source>
        <translation>Виникла помилка авторизації. Перевірте правильність облікових даних у налаштуваннях скриньки та повторіть спробу. Також, можливо, термін дії вашого пароля завершився. Переконайтеся у правильності ваших облікових даних, увійшовши до поля даних через веб-портал ISDS.</translation>
    </message>
    <message>
        <location filename="../../src/isds/io/connection.cpp" line="719"/>
        <source>OTP authorization failed. OTP code is wrong or expired.</source>
        <translation>Помилка авторизації OTP. Код OTP неправильний або прострочений.</translation>
    </message>
    <message>
        <location filename="../../src/isds/io/connection.cpp" line="722"/>
        <source>SMS authorization failed. SMS code is wrong or expired.</source>
        <translation>Помилка авторизації SMS. SMS-код неправильний або прострочений.</translation>
    </message>
    <message>
        <location filename="../../src/isds/io/connection.cpp" line="725"/>
        <source>SMS authorization failed. SMS code couldn&apos;t be sent. Your order on premium SMS has been exhausted or cancelled.</source>
        <translation>Помилка авторизації SMS. Не вдалося надіслати SMS-код. Ваша підписка на преміум SMS вичерпана або скасована.</translation>
    </message>
    <message>
        <location filename="../../src/isds/io/connection.cpp" line="729"/>
        <source>An communication error. See log for more detail.</source>
        <translation>Помилка зв’язку. Дивіться журнал для більш детальної інформації.</translation>
    </message>
</context>
<context>
    <name>CreateAccountPage1</name>
    <message>
        <location filename="../../qml/wizards/CreateAccountPage1.qml" line="58"/>
        <location filename="../../qml/wizards/CreateAccountPage1.qml" line="58"/>
        <source>Add Data Box</source>
        <translation>Додати скриньку</translation>
    </message>
    <message>
        <location filename="../../qml/wizards/CreateAccountPage1.qml" line="84"/>
        <location filename="../../qml/wizards/CreateAccountPage1.qml" line="84"/>
        <source>The data-box title must be filled in.</source>
        <translation>Ім&apos;я скриньки повинно бути заповнено.</translation>
    </message>
    <message>
        <location filename="../../qml/wizards/CreateAccountPage1.qml" line="88"/>
        <location filename="../../qml/wizards/CreateAccountPage1.qml" line="88"/>
        <source>Data-Box Title</source>
        <translation>Назва скриньки</translation>
    </message>
    <message>
        <location filename="../../qml/wizards/CreateAccountPage1.qml" line="89"/>
        <location filename="../../qml/wizards/CreateAccountPage1.qml" line="89"/>
        <source>Enter custom data-box name.</source>
        <translation>Введіть ім&apos;я скриньки із даними.</translation>
    </message>
    <message>
        <location filename="../../qml/wizards/CreateAccountPage1.qml" line="96"/>
        <location filename="../../qml/wizards/CreateAccountPage1.qml" line="96"/>
        <source>The data-box title is a user-specified name used for the identification of the data box in the application (e.g. &apos;My Personal Data Box&apos;, &apos;Firm Box&apos;, etc.). The chosen name serves only for your convenience. The entry must be filled in.</source>
        <translation>Назва скриньки - це вибране користувачем ім&apos;я, яке використовується для ідентифікації скриньки із даними в цій програмі (наприклад, &quot;Моя особиста скринька із даними&quot;, &quot;Скринька компанії&quot; тощо). Обрана назва призначена лише для вашої зручності. Це поле повинно бути заповнено.</translation>
    </message>
    <message>
        <location filename="../../qml/wizards/CreateAccountPage1.qml" line="101"/>
        <location filename="../../qml/wizards/CreateAccountPage1.qml" line="101"/>
        <source>Data-Box Environment</source>
        <translation>Тип скриньки із даними</translation>
    </message>
    <message>
        <location filename="../../qml/wizards/CreateAccountPage1.qml" line="106"/>
        <location filename="../../qml/wizards/CreateAccountPage1.qml" line="106"/>
        <source>regular</source>
        <translation>регулярна</translation>
    </message>
    <message>
        <location filename="../../qml/wizards/CreateAccountPage1.qml" line="111"/>
        <location filename="../../qml/wizards/CreateAccountPage1.qml" line="111"/>
        <source>test</source>
        <translation>тестова</translation>
    </message>
    <message>
        <location filename="../../qml/wizards/CreateAccountPage1.qml" line="116"/>
        <location filename="../../qml/wizards/CreateAccountPage1.qml" line="116"/>
        <source>The regular data box option is used to access the production (official) ISDS environment (&lt;a href=&quot;https://www.mojedatovaschranka.cz&quot;&gt;www.mojedatovaschranka.cz&lt;/a&gt;). Test data box is used to access the ISDS testing environment (&lt;a href=&quot;https://www.czebox.cz&quot;&gt;www.czebox.cz&lt;/a&gt;).</source>
        <translation>Для доступу до виробничого (офіційного) середовища ISDS використовується звичайна  скринька (&lt;a href=&quot;https://www.mojedatovaschranka.cz&quot;&gt;www.mojedatovaschranka.cz&lt;/a&gt;). Тестова скринька із даними використовується для доступу до тестового середовища ISDS (&lt;a href=&quot;https://www.czebox.cz&quot;&gt;www.czebox.cz&lt;/a&gt;).</translation>
    </message>
    <message>
        <location filename="../../qml/wizards/CreateAccountPage1.qml" line="124"/>
        <location filename="../../qml/wizards/CreateAccountPage1.qml" line="124"/>
        <source>Next</source>
        <translation>Наступні</translation>
    </message>
    <message>
        <location filename="../../qml/wizards/CreateAccountPage1.qml" line="125"/>
        <location filename="../../qml/wizards/CreateAccountPage1.qml" line="125"/>
        <source>Next step</source>
        <translation>Наступний крок</translation>
    </message>
</context>
<context>
    <name>CreateAccountPage2</name>
    <message>
        <location filename="../../qml/wizards/CreateAccountPage2.qml" line="131"/>
        <location filename="../../qml/wizards/CreateAccountPage2.qml" line="131"/>
        <source>Next step</source>
        <translation>Наступний крок</translation>
    </message>
    <message>
        <location filename="../../qml/wizards/CreateAccountPage2.qml" line="50"/>
        <location filename="../../qml/wizards/CreateAccountPage2.qml" line="50"/>
        <source>Add Data Box</source>
        <translation>Додати скриньку</translation>
    </message>
    <message>
        <location filename="../../qml/wizards/CreateAccountPage2.qml" line="73"/>
        <location filename="../../qml/wizards/CreateAccountPage2.qml" line="73"/>
        <source>Data-box title</source>
        <translation>Назва скриньки</translation>
    </message>
    <message>
        <location filename="../../qml/wizards/CreateAccountPage2.qml" line="76"/>
        <location filename="../../qml/wizards/CreateAccountPage2.qml" line="76"/>
        <source>Data-box type</source>
        <translation>Тип датової скриньки</translation>
    </message>
    <message>
        <location filename="../../qml/wizards/CreateAccountPage2.qml" line="83"/>
        <location filename="../../qml/wizards/CreateAccountPage2.qml" line="83"/>
        <source>Login Method</source>
        <translation>Метод входу</translation>
    </message>
    <message>
        <location filename="../../qml/wizards/CreateAccountPage2.qml" line="88"/>
        <location filename="../../qml/wizards/CreateAccountPage2.qml" line="88"/>
        <source>username + password</source>
        <translation>ім&apos;я користувача + пароль</translation>
    </message>
    <message>
        <location filename="../../qml/wizards/CreateAccountPage2.qml" line="93"/>
        <location filename="../../qml/wizards/CreateAccountPage2.qml" line="93"/>
        <source>username + Mobile Key</source>
        <translation>ім&apos;я користувача + мобільний ключ</translation>
    </message>
    <message>
        <location filename="../../qml/wizards/CreateAccountPage2.qml" line="98"/>
        <location filename="../../qml/wizards/CreateAccountPage2.qml" line="98"/>
        <source>username + password + SMS</source>
        <translation>ім&apos;я користувача + пароль + SMS</translation>
    </message>
    <message>
        <location filename="../../qml/wizards/CreateAccountPage2.qml" line="103"/>
        <location filename="../../qml/wizards/CreateAccountPage2.qml" line="103"/>
        <source>username + password + security code</source>
        <translation>ім&apos;я користувача + пароль + код безпеки</translation>
    </message>
    <message>
        <location filename="../../qml/wizards/CreateAccountPage2.qml" line="108"/>
        <location filename="../../qml/wizards/CreateAccountPage2.qml" line="108"/>
        <source>username + password + certificate</source>
        <translation>ім&apos;я користувача + пароль + сертифікат</translation>
    </message>
    <message>
        <location filename="../../qml/wizards/CreateAccountPage2.qml" line="111"/>
        <location filename="../../qml/wizards/CreateAccountPage2.qml" line="111"/>
        <source>Select the login method which you use to access the data box in the %1 ISDS environment.</source>
        <translation>Виберіть спосіб входу, який ви використовуєте для доступу до датової криньки у середовищі %1 ISDS.</translation>
    </message>
    <message>
        <location filename="../../qml/wizards/CreateAccountPage2.qml" line="111"/>
        <location filename="../../qml/wizards/CreateAccountPage2.qml" line="111"/>
        <source>testing</source>
        <translation>тестування</translation>
    </message>
    <message>
        <location filename="../../qml/wizards/CreateAccountPage2.qml" line="111"/>
        <location filename="../../qml/wizards/CreateAccountPage2.qml" line="111"/>
        <source>production</source>
        <translation>основний</translation>
    </message>
    <message>
        <location filename="../../qml/wizards/CreateAccountPage2.qml" line="111"/>
        <location filename="../../qml/wizards/CreateAccountPage2.qml" line="111"/>
        <source>Note: NIA login methods such as bank ID, mojeID or eCitizen aren&apos;t supported because the ISDS system doesn&apos;t provide such functionality for third-party applications.</source>
        <translation>Примітка: Методи входу в NIA, такі як Bank ID, MojeID або eCitizen, не підтримуються, оскільки система ISDS не підтримує їхню функціональність для сторонніх додатків.</translation>
    </message>
    <message>
        <location filename="../../qml/wizards/CreateAccountPage2.qml" line="120"/>
        <location filename="../../qml/wizards/CreateAccountPage2.qml" line="120"/>
        <source>Previous step</source>
        <translation>Попередній крок</translation>
    </message>
    <message>
        <location filename="../../qml/wizards/CreateAccountPage2.qml" line="121"/>
        <location filename="../../qml/wizards/CreateAccountPage2.qml" line="121"/>
        <source>Back</source>
        <translation>Назад</translation>
    </message>
    <message>
        <location filename="../../qml/wizards/CreateAccountPage2.qml" line="132"/>
        <location filename="../../qml/wizards/CreateAccountPage2.qml" line="132"/>
        <source>Next</source>
        <translation>Наступні</translation>
    </message>
</context>
<context>
    <name>CreateAccountPage3</name>
    <message>
        <location filename="../../qml/wizards/CreateAccountPage3.qml" line="65"/>
        <location filename="../../qml/wizards/CreateAccountPage3.qml" line="65"/>
        <source>The username must be filled in.</source>
        <translation>Ім’я користувача необхідно заповнити.</translation>
    </message>
    <message>
        <location filename="../../qml/wizards/CreateAccountPage3.qml" line="71"/>
        <location filename="../../qml/wizards/CreateAccountPage3.qml" line="71"/>
        <source>The communication code must be filled in.</source>
        <translation>Необхідно заповнити код зв&apos;язку.</translation>
    </message>
    <message>
        <location filename="../../qml/wizards/CreateAccountPage3.qml" line="78"/>
        <location filename="../../qml/wizards/CreateAccountPage3.qml" line="78"/>
        <source>The password must be filled in.</source>
        <translation>Пароль необхідно заповнити.</translation>
    </message>
    <message>
        <location filename="../../qml/wizards/CreateAccountPage3.qml" line="86"/>
        <location filename="../../qml/wizards/CreateAccountPage3.qml" line="86"/>
        <source>The certificate file must be specified.</source>
        <translation>Необхідно вказати файл сертифіката.</translation>
    </message>
    <message>
        <location filename="../../qml/wizards/CreateAccountPage3.qml" line="283"/>
        <location filename="../../qml/wizards/CreateAccountPage3.qml" line="283"/>
        <source>Next step</source>
        <translation>Наступний крок</translation>
    </message>
    <message>
        <location filename="../../qml/wizards/CreateAccountPage3.qml" line="179"/>
        <location filename="../../qml/wizards/CreateAccountPage3.qml" line="179"/>
        <source>Login method</source>
        <translation>Метод входу</translation>
    </message>
    <message>
        <location filename="../../qml/wizards/CreateAccountPage3.qml" line="111"/>
        <location filename="../../qml/wizards/CreateAccountPage3.qml" line="111"/>
        <source>Choose a certificate</source>
        <translation>Вибрати сертифікат</translation>
    </message>
    <message>
        <location filename="../../qml/wizards/CreateAccountPage3.qml" line="151"/>
        <location filename="../../qml/wizards/CreateAccountPage3.qml" line="151"/>
        <source>Add Data Box</source>
        <translation>Додати скриньку</translation>
    </message>
    <message>
        <location filename="../../qml/wizards/CreateAccountPage3.qml" line="173"/>
        <location filename="../../qml/wizards/CreateAccountPage3.qml" line="173"/>
        <source>Data-box title</source>
        <translation>Назва скриньки</translation>
    </message>
    <message>
        <location filename="../../qml/wizards/CreateAccountPage3.qml" line="176"/>
        <location filename="../../qml/wizards/CreateAccountPage3.qml" line="176"/>
        <source>Data-box type</source>
        <translation>Тип датової скриньки</translation>
    </message>
    <message>
        <location filename="../../qml/wizards/CreateAccountPage3.qml" line="193"/>
        <location filename="../../qml/wizards/CreateAccountPage3.qml" line="193"/>
        <source>Username</source>
        <translation>Ім&apos;я користувача</translation>
    </message>
    <message>
        <location filename="../../qml/wizards/CreateAccountPage3.qml" line="194"/>
        <location filename="../../qml/wizards/CreateAccountPage3.qml" line="194"/>
        <source>Enter the login name.</source>
        <translation>Введіть ім&apos;я для входу.</translation>
    </message>
    <message>
        <location filename="../../qml/wizards/CreateAccountPage3.qml" line="200"/>
        <location filename="../../qml/wizards/CreateAccountPage3.qml" line="200"/>
        <source>Warning: The username should contain only combinations of lower-case letters and digits.</source>
        <translation>Попередження: ім’я користувача має містити лише комбінації малих літер і цифр.</translation>
    </message>
    <message>
        <location filename="../../qml/wizards/CreateAccountPage3.qml" line="210"/>
        <location filename="../../qml/wizards/CreateAccountPage3.qml" line="210"/>
        <source>The username must consist of at least 6 characters without spaces (only combinations of lower-case letters and digits are allowed). Notification: The username is not a data-box ID.</source>
        <translation>Ім’я користувача має складатися щонайменше з 6 символів без пробілів (допускаються лише комбінації малих літер і цифр). Повідомлення: ім’я користувача не є ідентифікатором датової скриньки.</translation>
    </message>
    <message>
        <location filename="../../qml/wizards/CreateAccountPage3.qml" line="219"/>
        <location filename="../../qml/wizards/CreateAccountPage3.qml" line="219"/>
        <source>Password</source>
        <translation>Пароль</translation>
    </message>
    <message>
        <location filename="../../qml/wizards/CreateAccountPage3.qml" line="220"/>
        <location filename="../../qml/wizards/CreateAccountPage3.qml" line="220"/>
        <source>Enter the password.</source>
        <translation>Введіть пароль.</translation>
    </message>
    <message>
        <location filename="../../qml/wizards/CreateAccountPage3.qml" line="226"/>
        <location filename="../../qml/wizards/CreateAccountPage3.qml" line="226"/>
        <source>The password must be valid and non-expired. To check whether you&apos;ve entered the password correctly you may use the icon in the field on the right. Note: You must fist change the password using the ISDS web portal if it is your very first attempt to log into the data box.</source>
        <translation>Пароль має бути дійсним і непростроченим. Щоб перевірити, чи правильно ви ввели пароль, скористайтеся значком у полі праворуч. Примітка: Ви повинні спочатку змінити пароль за допомогою веб-порталу ISDS, якщо це ваша перша спроба увійти до датової скриньки.</translation>
    </message>
    <message>
        <location filename="../../qml/wizards/CreateAccountPage3.qml" line="234"/>
        <location filename="../../qml/wizards/CreateAccountPage3.qml" line="234"/>
        <source>Communication Code</source>
        <translation>Код зв&apos;язку</translation>
    </message>
    <message>
        <location filename="../../qml/wizards/CreateAccountPage3.qml" line="235"/>
        <location filename="../../qml/wizards/CreateAccountPage3.qml" line="235"/>
        <source>Enter the communication code.</source>
        <translation>Введіть код зв&apos;язку.</translation>
    </message>
    <message>
        <location filename="../../qml/wizards/CreateAccountPage3.qml" line="242"/>
        <location filename="../../qml/wizards/CreateAccountPage3.qml" line="242"/>
        <source>The &lt;a href=&quot;%1&quot;&gt;communication code&lt;/a&gt; is a string which can be generated in the ISDS web portal. You have to have the Mobile Key application installed. The Mobile Key application needs to be paired with the corresponding data-box account on the ISDS web portal.</source>
        <translation>&lt;a href=&quot;%1&quot;&gt;Код зв&apos;язку&lt;/a&gt; – це ланцюжок символів, який можна згенерувати на веб-порталі ISDS. У вас повинна бути встановлена програма Mobile Key. Додаток Mobile Key має бути з’єднаний з відповідним акаунтом ящика даних на веб-порталі ISDS.</translation>
    </message>
    <message>
        <location filename="../../qml/wizards/CreateAccountPage3.qml" line="249"/>
        <location filename="../../qml/wizards/CreateAccountPage3.qml" line="249"/>
        <source>Certificate File</source>
        <translation>Файл сертифіката</translation>
    </message>
    <message>
        <location filename="../../qml/wizards/CreateAccountPage3.qml" line="258"/>
        <location filename="../../qml/wizards/CreateAccountPage3.qml" line="258"/>
        <source>Choose certificate</source>
        <translation>Вибрати сертифікат</translation>
    </message>
    <message>
        <location filename="../../qml/wizards/CreateAccountPage3.qml" line="271"/>
        <location filename="../../qml/wizards/CreateAccountPage3.qml" line="271"/>
        <source>Previous step</source>
        <translation>Попередній крок</translation>
    </message>
    <message>
        <location filename="../../qml/wizards/CreateAccountPage3.qml" line="272"/>
        <location filename="../../qml/wizards/CreateAccountPage3.qml" line="272"/>
        <source>Back</source>
        <translation>Назад</translation>
    </message>
    <message>
        <location filename="../../qml/wizards/CreateAccountPage3.qml" line="284"/>
        <location filename="../../qml/wizards/CreateAccountPage3.qml" line="284"/>
        <source>Next</source>
        <translation>Наступні</translation>
    </message>
    <message>
        <location filename="../../qml/wizards/CreateAccountPage3.qml" line="262"/>
        <location filename="../../qml/wizards/CreateAccountPage3.qml" line="262"/>
        <source>The certificate file is needed for authentication purposes. The supplied file must contain a certificate and its corresponding private key. Only PEM and PFX file formats are accepted.</source>
        <translation>Файл сертифіката потрібен для аутентифікації. Наданий файл повинен містити сертифікат і відповідний йому приватний ключ. Приймаються лише формати файлів PEM та PFX.</translation>
    </message>
</context>
<context>
    <name>CreateAccountPage4</name>
    <message>
        <location filename="../../qml/wizards/CreateAccountPage4.qml" line="38"/>
        <location filename="../../qml/wizards/CreateAccountPage4.qml" line="38"/>
        <source>During the log-in procedure you will be asked to enter a private key password.</source>
        <translation>Під час процедури входу вам буде запропоновано ввести пароль приватного ключа.</translation>
    </message>
    <message>
        <location filename="../../qml/wizards/CreateAccountPage4.qml" line="41"/>
        <location filename="../../qml/wizards/CreateAccountPage4.qml" line="41"/>
        <source>During the log-in procedure you will be asked to confirm a notification in the Mobile Key application.</source>
        <translation>Під час процедури входу вам буде запропоновано підтвердити сповіщення в програмі Mobile Key.</translation>
    </message>
    <message>
        <location filename="../../qml/wizards/CreateAccountPage4.qml" line="43"/>
        <location filename="../../qml/wizards/CreateAccountPage4.qml" line="43"/>
        <source>During the log-in procedure you will be asked to enter a code which you should obtain via an SMS.</source>
        <translation>Під час процедури входу вам буде запропоновано ввести код, який ви повинні отримати через SMS.</translation>
    </message>
    <message>
        <location filename="../../qml/wizards/CreateAccountPage4.qml" line="45"/>
        <location filename="../../qml/wizards/CreateAccountPage4.qml" line="45"/>
        <source>During the log-in procedure you will be asked to enter a security code.</source>
        <translation>Під час процедури входу вам буде запропоновано ввести код безпеки.</translation>
    </message>
    <message>
        <location filename="../../qml/wizards/CreateAccountPage4.qml" line="100"/>
        <location filename="../../qml/wizards/CreateAccountPage4.qml" line="100"/>
        <source>Add Data Box</source>
        <translation>Додати скриньку</translation>
    </message>
    <message>
        <location filename="../../qml/wizards/CreateAccountPage4.qml" line="122"/>
        <location filename="../../qml/wizards/CreateAccountPage4.qml" line="122"/>
        <source>Data-box title</source>
        <translation>Назва скриньки</translation>
    </message>
    <message>
        <location filename="../../qml/wizards/CreateAccountPage4.qml" line="125"/>
        <location filename="../../qml/wizards/CreateAccountPage4.qml" line="125"/>
        <source>Data-box type</source>
        <translation>Тип датової скриньки</translation>
    </message>
    <message>
        <location filename="../../qml/wizards/CreateAccountPage4.qml" line="142"/>
        <location filename="../../qml/wizards/CreateAccountPage4.qml" line="142"/>
        <source>Local Data-Box Settings</source>
        <translation>Налаштування локальних скриньок</translation>
    </message>
    <message>
        <location filename="../../qml/wizards/CreateAccountPage4.qml" line="164"/>
        <location filename="../../qml/wizards/CreateAccountPage4.qml" line="164"/>
        <source>The data box will be included into the synchronisation process of all data boxes.</source>
        <translation>Cкринька буде включена до масової синхронізації всіх скриньок.</translation>
    </message>
    <message>
        <location filename="../../qml/wizards/CreateAccountPage4.qml" line="165"/>
        <location filename="../../qml/wizards/CreateAccountPage4.qml" line="165"/>
        <source>The data box won&apos;t be included into the synchronisation process of all data boxes.</source>
        <translation>Скриньку не буде включено до масової синхронізації всіх скриньок.</translation>
    </message>
    <message>
        <location filename="../../qml/wizards/CreateAccountPage4.qml" line="174"/>
        <location filename="../../qml/wizards/CreateAccountPage4.qml" line="174"/>
        <source>All done. The data box will be added only when the application successfully logs into the data box using the supplied login credentials.</source>
        <translation>Все зроблено. Скриньку буде додано лише в тому випадку, якщо програма успішно увійде до скриньки із даними на сервері ISDS, використовуючи введені облікові дані.</translation>
    </message>
    <message>
        <location filename="../../qml/wizards/CreateAccountPage4.qml" line="192"/>
        <location filename="../../qml/wizards/CreateAccountPage4.qml" line="192"/>
        <source>Connect to data box.</source>
        <translation>Увійти до скриньки.</translation>
    </message>
    <message>
        <location filename="../../qml/wizards/CreateAccountPage4.qml" line="193"/>
        <location filename="../../qml/wizards/CreateAccountPage4.qml" line="193"/>
        <source>Connect Data Box</source>
        <translation>Увійти до скриньки</translation>
    </message>
    <message>
        <location filename="../../qml/wizards/CreateAccountPage4.qml" line="213"/>
        <location filename="../../qml/wizards/CreateAccountPage4.qml" line="213"/>
        <source>Logging into the data box %1 ...</source>
        <translation>Вхід до скриньки %1 ...</translation>
    </message>
    <message>
        <location filename="../../qml/wizards/CreateAccountPage4.qml" line="152"/>
        <location filename="../../qml/wizards/CreateAccountPage4.qml" line="152"/>
        <source>Use local storage</source>
        <translation>Використовувати локальне сховище</translation>
    </message>
    <message>
        <location filename="../../qml/wizards/CreateAccountPage4.qml" line="70"/>
        <location filename="../../qml/wizards/CreateAccountPage4.qml" line="70"/>
        <source>Creating data box: %1</source>
        <translation>Створення скриньки: %1</translation>
    </message>
    <message>
        <location filename="../../qml/wizards/CreateAccountPage4.qml" line="71"/>
        <location filename="../../qml/wizards/CreateAccountPage4.qml" line="71"/>
        <source>Data box &apos;%1&apos; could not be created.</source>
        <translation>Не вдалося створити скриньку &quot;%1&quot;.</translation>
    </message>
    <message>
        <location filename="../../qml/wizards/CreateAccountPage4.qml" line="155"/>
        <location filename="../../qml/wizards/CreateAccountPage4.qml" line="155"/>
        <source>Messages and attachments will be locally stored. No active internet connection is needed to access locally stored data.</source>
        <translation>Повідомлення та вкладення зберігатимуться локально. Для доступу до локально збережених даних не потрібне активне підключення до Інтернету.</translation>
    </message>
    <message>
        <location filename="../../qml/wizards/CreateAccountPage4.qml" line="156"/>
        <location filename="../../qml/wizards/CreateAccountPage4.qml" line="156"/>
        <source>Messages and attachments will be stored only temporarily in memory. These data will be lost on application exit.</source>
        <translation>Повідомлення та вкладення зберігатимуться в пам’яті лише тимчасово. Ці дані будуть втрачені під час виходу з програми.</translation>
    </message>
    <message>
        <location filename="../../qml/wizards/CreateAccountPage4.qml" line="161"/>
        <location filename="../../qml/wizards/CreateAccountPage4.qml" line="161"/>
        <source>Synchronise with all</source>
        <translation>Синхронізувати з усіма</translation>
    </message>
    <message>
        <location filename="../../qml/wizards/CreateAccountPage4.qml" line="182"/>
        <location filename="../../qml/wizards/CreateAccountPage4.qml" line="182"/>
        <source>Previous step</source>
        <translation>Попередній крок</translation>
    </message>
    <message>
        <location filename="../../qml/wizards/CreateAccountPage4.qml" line="183"/>
        <location filename="../../qml/wizards/CreateAccountPage4.qml" line="183"/>
        <source>Back</source>
        <translation>Назад</translation>
    </message>
    <message>
        <location filename="../../qml/wizards/CreateAccountPage4.qml" line="128"/>
        <location filename="../../qml/wizards/CreateAccountPage4.qml" line="128"/>
        <source>Login method</source>
        <translation>Метод входу</translation>
    </message>
    <message>
        <location filename="../../qml/wizards/CreateAccountPage4.qml" line="131"/>
        <location filename="../../qml/wizards/CreateAccountPage4.qml" line="131"/>
        <source>Username</source>
        <translation>Ім&apos;я користувача</translation>
    </message>
    <message>
        <location filename="../../qml/wizards/CreateAccountPage4.qml" line="136"/>
        <location filename="../../qml/wizards/CreateAccountPage4.qml" line="136"/>
        <source>Certificate</source>
        <translation>Сертифікат</translation>
    </message>
    <message>
        <location filename="../../qml/wizards/CreateAccountPage4.qml" line="146"/>
        <location filename="../../qml/wizards/CreateAccountPage4.qml" line="146"/>
        <source>Remember password</source>
        <translation>Запам&apos;ятати пароль</translation>
    </message>
</context>
<context>
    <name>DataboxList</name>
    <message>
        <location filename="../../qml/components/DataboxList.qml" line="54"/>
        <location filename="../../qml/components/DataboxList.qml" line="171"/>
        <location filename="../../qml/components/DataboxList.qml" line="54"/>
        <location filename="../../qml/components/DataboxList.qml" line="171"/>
        <source>Public</source>
        <translation>Публічна</translation>
    </message>
    <message>
        <location filename="../../qml/components/DataboxList.qml" line="56"/>
        <location filename="../../qml/components/DataboxList.qml" line="56"/>
        <source>Response to initiatory</source>
        <translation>Відповідь на ініціативу</translation>
    </message>
    <message>
        <location filename="../../qml/components/DataboxList.qml" line="58"/>
        <location filename="../../qml/components/DataboxList.qml" line="58"/>
        <source>Subsidised</source>
        <translation>Субсидований</translation>
    </message>
    <message>
        <location filename="../../qml/components/DataboxList.qml" line="60"/>
        <location filename="../../qml/components/DataboxList.qml" line="60"/>
        <source>Contractual</source>
        <translation>Договірна</translation>
    </message>
    <message>
        <location filename="../../qml/components/DataboxList.qml" line="62"/>
        <location filename="../../qml/components/DataboxList.qml" line="62"/>
        <source>Initiatory</source>
        <translation>Ініціаторська</translation>
    </message>
    <message>
        <location filename="../../qml/components/DataboxList.qml" line="64"/>
        <location filename="../../qml/components/DataboxList.qml" line="64"/>
        <source>Prepaid credit</source>
        <translation>Передоплачений кредит</translation>
    </message>
    <message>
        <location filename="../../qml/components/DataboxList.qml" line="175"/>
        <location filename="../../qml/components/DataboxList.qml" line="175"/>
        <source>Unknown</source>
        <translation>Невідомий/а</translation>
    </message>
    <message>
        <location filename="../../qml/components/DataboxList.qml" line="180"/>
        <location filename="../../qml/components/DataboxList.qml" line="180"/>
        <source>Message type: %1</source>
        <translation>Тип повідомлення: %1</translation>
    </message>
    <message>
        <location filename="../../qml/components/DataboxList.qml" line="189"/>
        <location filename="../../qml/components/DataboxList.qml" line="189"/>
        <source>Payment:</source>
        <translation>Оплата:</translation>
    </message>
    <message>
        <location filename="../../qml/components/DataboxList.qml" line="194"/>
        <location filename="../../qml/components/DataboxList.qml" line="194"/>
        <source>Select commercial message payment method.</source>
        <translation>Виберіть спосіб оплати комерційних повідомлень (PDZ).</translation>
    </message>
</context>
<context>
    <name>DbWrapper</name>
    <message>
        <location filename="../../src/net/db_wrapper.cpp" line="44"/>
        <location filename="../../src/net/db_wrapper.cpp" line="117"/>
        <location filename="../../src/net/db_wrapper.cpp" line="224"/>
        <location filename="../../src/net/db_wrapper.cpp" line="246"/>
        <source>Internal error!</source>
        <translation>Внутрішня помилка!</translation>
    </message>
    <message>
        <location filename="../../src/net/db_wrapper.cpp" line="54"/>
        <location filename="../../src/net/db_wrapper.cpp" line="151"/>
        <location filename="../../src/net/db_wrapper.cpp" line="234"/>
        <location filename="../../src/net/db_wrapper.cpp" line="256"/>
        <source>Cannot open message database!</source>
        <translation>Не вдається відкрити базу даних повідомлень!</translation>
    </message>
    <message>
        <location filename="../../src/net/db_wrapper.cpp" line="71"/>
        <source>Message %1 envelope insertion failed!</source>
        <translation>Помилка обробки повідомлення  %1!</translation>
    </message>
    <message>
        <location filename="../../src/net/db_wrapper.cpp" line="85"/>
        <location filename="../../src/net/db_wrapper.cpp" line="158"/>
        <source>Message %1 envelope update failed!</source>
        <translation>Помилка оновлення конверта  %1!</translation>
    </message>
    <message>
        <location filename="../../src/net/db_wrapper.cpp" line="99"/>
        <source>%1: new messages: %2</source>
        <translation>%1: нових повідомлень: %2</translation>
    </message>
    <message>
        <location filename="../../src/net/db_wrapper.cpp" line="102"/>
        <source>%1: No new messages.</source>
        <translation>%1: Немає нових повідомлень.</translation>
    </message>
    <message>
        <location filename="../../src/net/db_wrapper.cpp" line="132"/>
        <source>Cannot open file database!</source>
        <translation>Не вдається відкрити файлову базу даних!</translation>
    </message>
    <message>
        <location filename="../../src/net/db_wrapper.cpp" line="139"/>
        <source>File %1 insertion failed!</source>
        <translation>Не вдалося вставити файл %1!</translation>
    </message>
</context>
<context>
    <name>Description</name>
    <message>
        <location filename="../../src/datovka_shared/isds/type_description.cpp" line="34"/>
        <source>system box</source>
        <translation>системна скринька</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/isds/type_description.cpp" line="35"/>
        <source>public authority</source>
        <translation>державний орган</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/isds/type_description.cpp" line="36"/>
        <source>public authority - notary</source>
        <translation>державний орган - нотаріус</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/isds/type_description.cpp" line="37"/>
        <source>public authority - bailiff</source>
        <translation>державний орган - судовий виконавець</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/isds/type_description.cpp" line="38"/>
        <source>public authority - at request</source>
        <translation>державний орган - на заяву</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/isds/type_description.cpp" line="39"/>
        <source>public authority - natural person</source>
        <translation>державний орган - фізична особа</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/isds/type_description.cpp" line="40"/>
        <source>public authority - self-employed person</source>
        <translation>державний орган - приватний підприємець</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/isds/type_description.cpp" line="41"/>
        <source>public authority - legal person</source>
        <translation>державний орган - юридична особа</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/isds/type_description.cpp" line="42"/>
        <source>legal person</source>
        <translation>юридична особа</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/isds/type_description.cpp" line="43"/>
        <source>legal person - founded by an act</source>
        <translation>юридична особа - відповідно закону</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/isds/type_description.cpp" line="44"/>
        <source>legal person - at request</source>
        <translation>юридична особа - на заяви</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/isds/type_description.cpp" line="45"/>
        <source>self-employed person</source>
        <translation>приватний підприємець</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/isds/type_description.cpp" line="46"/>
        <source>self-employed person - advocate</source>
        <translation>приватний підприємець - адвокат</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/isds/type_description.cpp" line="47"/>
        <source>self-employed person - tax advisor</source>
        <translation>приватний підприємець - податковий консультант</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/isds/type_description.cpp" line="48"/>
        <source>self-employed person - insolvency administrator</source>
        <translation>приватний підприємець - керівник справами про банкрутство</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/isds/type_description.cpp" line="49"/>
        <source>self-employed person - statutory auditor</source>
        <translation>приватний підприємець - аудитор</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/isds/type_description.cpp" line="50"/>
        <source>self-employed person - expert witness</source>
        <translation>приватний підприємець - судовий експерт</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/isds/type_description.cpp" line="51"/>
        <source>self-employed person - sworn translator</source>
        <translation>приватний підприємець - судовий перекладач</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/isds/type_description.cpp" line="52"/>
        <source>self-employed person - architect</source>
        <translation>приватний підприємець – архітектор</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/isds/type_description.cpp" line="53"/>
        <source>self-employed person - authorised engineer / technician</source>
        <translation>приватний підприємець - уповноважений інженер / технік</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/isds/type_description.cpp" line="54"/>
        <source>self-employed person - authorised geodetics engineer</source>
        <translation>приватний підприємець – уповноважений геодезичний інженер</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/isds/type_description.cpp" line="55"/>
        <source>self-employed person - at request</source>
        <translation>приватний підприємець - на заяви</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/isds/type_description.cpp" line="56"/>
        <source>natural person</source>
        <translation>фізична особа</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/isds/type_description.cpp" line="60"/>
        <location filename="../../src/datovka_shared/isds/type_description.cpp" line="341"/>
        <location filename="../../src/datovka_shared/isds/type_description.cpp" line="387"/>
        <source>An error occurred while checking the type.</source>
        <translation>Під час перевірки типу сталася помилка.</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/isds/type_description.cpp" line="73"/>
        <source>The data box is accessible. It is possible to send messages into it. It can be looked up on the Portal.</source>
        <translation>Датова скринька доступна. У неї можна надсилати повідомлення. Її можна переглянути на порталі.</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/isds/type_description.cpp" line="78"/>
        <source>The data box is temporarily inaccessible (at own request). It may be made accessible again at some point in the future.</source>
        <translation>Датова скринька тимчасово недоступна (за власним бажанням). Можливо, в якийсь момент у майбутньому вона знову стане доступною.</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/isds/type_description.cpp" line="83"/>
        <source>The data box is so far inactive. The owner of the box has to log into the web interface at first in order to activate the box.</source>
        <translation>Поки що датова скринька неактивна. Власник скриньки повинен спочатку увійти у веб-інтерфейс, щоб її активувати.</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/isds/type_description.cpp" line="88"/>
        <source>The data box is permanently inaccessible. It is waiting to be deleted (but it may be made accessible again).</source>
        <translation>Датова скринька постійно недоступна. Вона очікує на видалення (але може бути знову доступною).</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/isds/type_description.cpp" line="93"/>
        <source>The data box has been deleted (none the less it exists in ISDS).</source>
        <translation>Скриньку даних видалено (проте, вона існує в ISDS).</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/isds/type_description.cpp" line="98"/>
        <source>The data box is temporarily inaccessible (because of reasons listed in law). It may be made accessible again at some point in the future.</source>
        <translation>Скринька даних тимчасово недоступна (через причини, зазначені в законі). Вона може бути знову доступною у майбутньому.</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/isds/type_description.cpp" line="104"/>
        <location filename="../../src/datovka_shared/isds/type_description.cpp" line="235"/>
        <source>An error occurred while checking the status.</source>
        <translation>Під час перевірки стану сталася помилка.</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/isds/type_description.cpp" line="115"/>
        <source>Full control</source>
        <translation>Повний контроль</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/isds/type_description.cpp" line="117"/>
        <source>Restricted control</source>
        <translation>Обмежений контроль</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/isds/type_description.cpp" line="122"/>
        <source>download and read incoming DM</source>
        <translation>завантажити та читайти вхідні повідомлення</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/isds/type_description.cpp" line="128"/>
        <source>download and read DM sent into own hands</source>
        <translation>завантажити та читайти вхідні повідомлення, надіслані у власні руки</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/isds/type_description.cpp" line="133"/>
        <source>create and send DM, download sent DM</source>
        <translation>створити та надіслати повідомлення, завантажити надіслані повідомлення</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/isds/type_description.cpp" line="138"/>
        <source>retrieve DM lists, delivery and acceptance reports</source>
        <translation>отримати списки повідомлень, звіти про доставку та приймання</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/isds/type_description.cpp" line="143"/>
        <source>search for data boxes</source>
        <translation>пошук скриньки даних</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/isds/type_description.cpp" line="147"/>
        <source>manage the data box</source>
        <translation>налаштування скриньки даних</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/isds/type_description.cpp" line="151"/>
        <source>read message in data vault</source>
        <translation>прочитати повідомлення в скриньці даних</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/isds/type_description.cpp" line="155"/>
        <source>erase messages from data vault</source>
        <translation>видалити повідомлення із скриньки даних</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/isds/type_description.cpp" line="169"/>
        <source>Message has been submitted (has been created in ISDS)</source>
        <translation>Повідомлення надіслано (створено в ISDS)</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/isds/type_description.cpp" line="175"/>
        <source>Data message including its attachments signed with time-stamp.</source>
        <translation>Повідомлення з даними, включаючи вкладення, підписане міткою часу.</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/isds/type_description.cpp" line="182"/>
        <source>Message did not pass through AV check; infected paper deleted; final status before deletion.</source>
        <translation>Повідомлення не пройшло AV-перевірку; заражений папір видалено; остаточний статус перед видаленням.</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/isds/type_description.cpp" line="187"/>
        <source>Message handed into ISDS (delivery time recorded).</source>
        <translation>Повідомлення передано в ISDS (час доставки записано).</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/isds/type_description.cpp" line="195"/>
        <source>10 days have passed since the delivery of the public message which has not been accepted by logging-in (assumption of acceptance through fiction in non-OVM DS); this state cannot occur for commercial messages.</source>
        <translation>минуло 10 днів з моменту доставки публічного повідомлення, яке не було прийнято авторизацією (припущення прийняття через фікцію в не-OVM СД); до цього стану не може дійти у випадку поштових повідомлень.</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/isds/type_description.cpp" line="205"/>
        <source>A person authorised to read this message has logged in -- delivered message has been accepted.</source>
        <translation>Особа, уповноважена читати це повідомлення, увійшла в систему – доставлене повідомлення прийнято.</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/isds/type_description.cpp" line="210"/>
        <source>Message has been read (on the portal or by ESS action).</source>
        <translation>Повідомлення прочитано (на порталі або дією ESS).</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/isds/type_description.cpp" line="217"/>
        <source>Message marked as undeliverable because the target DS has been made inaccessible.</source>
        <translation>Повідомлення позначено як недоставлене, оскільки цільовий СД є недоступною.</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/isds/type_description.cpp" line="225"/>
        <source>Message content deleted, envelope including hashes has been moved into archive.</source>
        <translation>Вміст повідомлення видалено, конверт із хешами переміщено в архів.</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/isds/type_description.cpp" line="230"/>
        <source>Message resides in data vault.</source>
        <translation>Повідомлення знаходиться в сховищі даних.</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/isds/type_description.cpp" line="256"/>
        <source>Subsidised postal data message, initiating reply postal data message</source>
        <translation>Повідомлення з даними субсидованої пошти ініціює використання повідомлення з даними відповіді</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/isds/type_description.cpp" line="260"/>
        <source>Subsidised postal data message, initiating reply postal data message - used for sending reply</source>
        <translation>Повідомлення субсидованих поштових даних ініціює використання повідомлення з даними відповіді, яке використовується для надсилання відповіді</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/isds/type_description.cpp" line="264"/>
        <source>Subsidised postal data message, initiating reply postal data message - unused for sending reply, expired</source>
        <translation>Повідомлення з субсидованими поштовими даними, ініціює використання відповідального повідомлення з поштовими даними - невикористане та прострочене</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/isds/type_description.cpp" line="268"/>
        <source>Postal data message sent using a subscription (prepaid credit)</source>
        <translation>Повідомлення з поштовими даними, надіслане за допомогою підписки (передоплачений кредит)</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/isds/type_description.cpp" line="272"/>
        <source>Postal data message sent in endowment mode by another data box to the benefactor account</source>
        <translation>Повідомлення з поштовими даними, надіслане в режимі благодійності іншим ящиком даних на акаунт благодійника</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/isds/type_description.cpp" line="276"/>
        <source>Postal data message</source>
        <translation>Повідомлення з поштовими даними</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/isds/type_description.cpp" line="279"/>
        <source>Initiating postal data message</source>
        <translation>Початокове повідомлення з поштовими даними</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/isds/type_description.cpp" line="282"/>
        <source>Reply postal data message; sent at the expense of the sender of the initiating postal data message</source>
        <translation>Відповідь на повідомлення з поштовими даними; надсилається за рахунок відправника ініційного повідомлення з поштовими даними</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/isds/type_description.cpp" line="286"/>
        <source>Public message (recipient or sender is a public authority)</source>
        <translation>Публічне повідомлення (одержувач або відправник є державним органом)</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/isds/type_description.cpp" line="289"/>
        <source>Initiating postal data message - unused for sending reply, expired</source>
        <translation>Початкове повідомлення з поштовими даними - не використовується для надсилання відповіді, термін дії закінчився</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/isds/type_description.cpp" line="292"/>
        <source>Initiating postal data message - used for sending reply</source>
        <translation>Початкове повідомлення з поштовими даними - не використовується для відповіді, дії закінчилися</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/isds/type_description.cpp" line="297"/>
        <source>Unrecognised message type</source>
        <translation>Нерозпізнаний тип повідомлення</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/isds/type_description.cpp" line="310"/>
        <location filename="../../src/datovka_shared/isds/type_description.cpp" line="354"/>
        <source>Primary user</source>
        <translation>Основний користувач</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/isds/type_description.cpp" line="314"/>
        <location filename="../../src/datovka_shared/isds/type_description.cpp" line="358"/>
        <source>Entrusted user</source>
        <translation>Довірена особа</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/isds/type_description.cpp" line="318"/>
        <location filename="../../src/datovka_shared/isds/type_description.cpp" line="362"/>
        <source>Administrator</source>
        <translation>Адміністратор</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/isds/type_description.cpp" line="321"/>
        <location filename="../../src/datovka_shared/isds/type_description.cpp" line="366"/>
        <source>Official</source>
        <translation>Система ISDS</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/isds/type_description.cpp" line="324"/>
        <location filename="../../src/datovka_shared/isds/type_description.cpp" line="373"/>
        <source>???</source>
        <translation>???</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/isds/type_description.cpp" line="328"/>
        <location filename="../../src/datovka_shared/isds/type_description.cpp" line="376"/>
        <source>Liquidator</source>
        <translation>Ліквідатор</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/isds/type_description.cpp" line="332"/>
        <location filename="../../src/datovka_shared/isds/type_description.cpp" line="379"/>
        <source>Receiver</source>
        <translation>Вимушений адміністратор</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/isds/type_description.cpp" line="336"/>
        <location filename="../../src/datovka_shared/isds/type_description.cpp" line="382"/>
        <source>Guardian</source>
        <translation>Опікун</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/isds/type_description.cpp" line="370"/>
        <source>Virtual</source>
        <translation>Віртуальна особистість</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/isds/type_description.cpp" line="395"/>
        <source>Success</source>
        <translation>Успіх</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/isds/type_description.cpp" line="396"/>
        <source>Unspecified error</source>
        <translation>Невизначена помилка</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/isds/type_description.cpp" line="397"/>
        <source>Not supported</source>
        <translation>Не підтримується</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/isds/type_description.cpp" line="398"/>
        <source>Invalid value</source>
        <translation>Неправильне значення</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/isds/type_description.cpp" line="399"/>
        <source>Invalid context</source>
        <translation>Недійсний контекст</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/isds/type_description.cpp" line="400"/>
        <source>Not logged in</source>
        <translation>Не ввійшли в систему</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/isds/type_description.cpp" line="401"/>
        <source>Connection closed</source>
        <translation>З&apos;єднання закрите</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/isds/type_description.cpp" line="404"/>
        <source>Out of memory</source>
        <translation>Не вистачає пам&apos;яті</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/isds/type_description.cpp" line="405"/>
        <source>Network problem</source>
        <translation>Проблема з мережею</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/isds/type_description.cpp" line="406"/>
        <source>HTTP problem</source>
        <translation>HTTP помилка</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/isds/type_description.cpp" line="407"/>
        <source>SOAP problem</source>
        <translation>SOAP помилка</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/isds/type_description.cpp" line="408"/>
        <source>XML problem</source>
        <translation>XML помилка</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/isds/type_description.cpp" line="409"/>
        <source>ISDS server problem</source>
        <translation>Помилка ISDS серверу</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/isds/type_description.cpp" line="410"/>
        <source>Invalid enumeration value</source>
        <translation>Недійсне значення переліку</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/isds/type_description.cpp" line="411"/>
        <source>Invalid date value</source>
        <translation>Недійсна дата</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/isds/type_description.cpp" line="412"/>
        <source>Too big</source>
        <translation>Занадто велике</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/isds/type_description.cpp" line="413"/>
        <source>Too small</source>
        <translation>Занадто мале</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/isds/type_description.cpp" line="414"/>
        <source>Value not unique</source>
        <translation>Значення не є унікальним</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/isds/type_description.cpp" line="415"/>
        <source>Values not equal</source>
        <translation>Значення нерівні</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/isds/type_description.cpp" line="416"/>
        <source>Some suboperations failed</source>
        <translation>Деякі підоперації не вдалося виконати</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/isds/type_description.cpp" line="417"/>
        <source>Operation aborted</source>
        <translation>Операцію перервано</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/isds/type_description.cpp" line="418"/>
        <source>Security problem</source>
        <translation>Проблема безпеки</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/isds/type_description.cpp" line="421"/>
        <source>Unknown error</source>
        <translation>Невідома помилка</translation>
    </message>
</context>
<context>
    <name>DrawerMenuDatovka</name>
    <message>
        <location filename="../../qml/popups/DrawerMenuDatovka.qml" line="83"/>
        <location filename="../../qml/popups/DrawerMenuDatovka.qml" line="107"/>
        <location filename="../../qml/popups/DrawerMenuDatovka.qml" line="83"/>
        <location filename="../../qml/popups/DrawerMenuDatovka.qml" line="107"/>
        <source>Messages</source>
        <translation>Повідомлення</translation>
    </message>
    <message>
        <location filename="../../qml/popups/DrawerMenuDatovka.qml" line="87"/>
        <location filename="../../qml/popups/DrawerMenuDatovka.qml" line="109"/>
        <location filename="../../qml/popups/DrawerMenuDatovka.qml" line="87"/>
        <location filename="../../qml/popups/DrawerMenuDatovka.qml" line="109"/>
        <source>Settings</source>
        <translation>Налаштування</translation>
    </message>
    <message>
        <location filename="../../qml/popups/DrawerMenuDatovka.qml" line="88"/>
        <location filename="../../qml/popups/DrawerMenuDatovka.qml" line="110"/>
        <location filename="../../qml/popups/DrawerMenuDatovka.qml" line="88"/>
        <location filename="../../qml/popups/DrawerMenuDatovka.qml" line="110"/>
        <source>General</source>
        <translation>Загальне</translation>
    </message>
    <message>
        <location filename="../../qml/popups/DrawerMenuDatovka.qml" line="91"/>
        <location filename="../../qml/popups/DrawerMenuDatovka.qml" line="113"/>
        <location filename="../../qml/popups/DrawerMenuDatovka.qml" line="91"/>
        <location filename="../../qml/popups/DrawerMenuDatovka.qml" line="113"/>
        <source>Records Management</source>
        <translation>Управління записами</translation>
    </message>
    <message>
        <location filename="../../qml/popups/DrawerMenuDatovka.qml" line="50"/>
        <location filename="../../qml/popups/DrawerMenuDatovka.qml" line="68"/>
        <location filename="../../qml/popups/DrawerMenuDatovka.qml" line="50"/>
        <location filename="../../qml/popups/DrawerMenuDatovka.qml" line="68"/>
        <source>Data-box properties</source>
        <translation>Особливості скриньки</translation>
    </message>
    <message>
        <location filename="../../qml/popups/DrawerMenuDatovka.qml" line="81"/>
        <location filename="../../qml/popups/DrawerMenuDatovka.qml" line="105"/>
        <location filename="../../qml/popups/DrawerMenuDatovka.qml" line="81"/>
        <location filename="../../qml/popups/DrawerMenuDatovka.qml" line="105"/>
        <source>Data Boxes</source>
        <translation>Скриньки</translation>
    </message>
    <message>
        <location filename="../../qml/popups/DrawerMenuDatovka.qml" line="82"/>
        <location filename="../../qml/popups/DrawerMenuDatovka.qml" line="106"/>
        <location filename="../../qml/popups/DrawerMenuDatovka.qml" line="82"/>
        <location filename="../../qml/popups/DrawerMenuDatovka.qml" line="106"/>
        <source>Add Data Box</source>
        <translation>Додати скриньку</translation>
    </message>
    <message>
        <location filename="../../qml/popups/DrawerMenuDatovka.qml" line="84"/>
        <location filename="../../qml/popups/DrawerMenuDatovka.qml" line="108"/>
        <location filename="../../qml/popups/DrawerMenuDatovka.qml" line="84"/>
        <location filename="../../qml/popups/DrawerMenuDatovka.qml" line="108"/>
        <source>View Message</source>
        <translation>Переглянути повідомлення</translation>
    </message>
    <message>
        <location filename="../../qml/popups/DrawerMenuDatovka.qml" line="85"/>
        <location filename="../../qml/popups/DrawerMenuDatovka.qml" line="85"/>
        <source>Search Message</source>
        <translation>Пошук повідомлення</translation>
    </message>
    <message>
        <location filename="../../qml/popups/DrawerMenuDatovka.qml" line="86"/>
        <location filename="../../qml/popups/DrawerMenuDatovka.qml" line="86"/>
        <source>Import Message</source>
        <translation>Імпортувати повідомлення</translation>
    </message>
    <message>
        <location filename="../../qml/popups/DrawerMenuDatovka.qml" line="89"/>
        <location filename="../../qml/popups/DrawerMenuDatovka.qml" line="111"/>
        <location filename="../../qml/popups/DrawerMenuDatovka.qml" line="89"/>
        <location filename="../../qml/popups/DrawerMenuDatovka.qml" line="111"/>
        <source>Synchronisation</source>
        <translation>Синхронізація</translation>
    </message>
    <message>
        <location filename="../../qml/popups/DrawerMenuDatovka.qml" line="90"/>
        <location filename="../../qml/popups/DrawerMenuDatovka.qml" line="112"/>
        <location filename="../../qml/popups/DrawerMenuDatovka.qml" line="90"/>
        <location filename="../../qml/popups/DrawerMenuDatovka.qml" line="112"/>
        <source>Security &amp;&amp; PIN</source>
        <translation>Безпека та PIN-код</translation>
    </message>
    <message>
        <location filename="../../qml/popups/DrawerMenuDatovka.qml" line="92"/>
        <location filename="../../qml/popups/DrawerMenuDatovka.qml" line="114"/>
        <location filename="../../qml/popups/DrawerMenuDatovka.qml" line="92"/>
        <location filename="../../qml/popups/DrawerMenuDatovka.qml" line="114"/>
        <source>View All Settings</source>
        <translation>Переглянути всі параметри</translation>
    </message>
    <message>
        <location filename="../../qml/popups/DrawerMenuDatovka.qml" line="93"/>
        <location filename="../../qml/popups/DrawerMenuDatovka.qml" line="115"/>
        <location filename="../../qml/popups/DrawerMenuDatovka.qml" line="93"/>
        <location filename="../../qml/popups/DrawerMenuDatovka.qml" line="115"/>
        <source>Back up &amp;&amp; Restore</source>
        <translation>Створення резервних копій та відновлення</translation>
    </message>
    <message>
        <location filename="../../qml/popups/DrawerMenuDatovka.qml" line="94"/>
        <location filename="../../qml/popups/DrawerMenuDatovka.qml" line="94"/>
        <source>Transfer Data</source>
        <translation>Передача даних</translation>
    </message>
    <message>
        <location filename="../../qml/popups/DrawerMenuDatovka.qml" line="95"/>
        <location filename="../../qml/popups/DrawerMenuDatovka.qml" line="95"/>
        <source>Back up Data</source>
        <translation>Резервне копіювання даних</translation>
    </message>
    <message>
        <location filename="../../qml/popups/DrawerMenuDatovka.qml" line="96"/>
        <location filename="../../qml/popups/DrawerMenuDatovka.qml" line="116"/>
        <location filename="../../qml/popups/DrawerMenuDatovka.qml" line="96"/>
        <location filename="../../qml/popups/DrawerMenuDatovka.qml" line="116"/>
        <source>Restore Data</source>
        <translation>Відновити дані</translation>
    </message>
    <message>
        <location filename="../../qml/popups/DrawerMenuDatovka.qml" line="97"/>
        <location filename="../../qml/popups/DrawerMenuDatovka.qml" line="117"/>
        <location filename="../../qml/popups/DrawerMenuDatovka.qml" line="97"/>
        <location filename="../../qml/popups/DrawerMenuDatovka.qml" line="117"/>
        <source>Help</source>
        <translation>Допомога</translation>
    </message>
    <message>
        <location filename="../../qml/popups/DrawerMenuDatovka.qml" line="98"/>
        <location filename="../../qml/popups/DrawerMenuDatovka.qml" line="118"/>
        <location filename="../../qml/popups/DrawerMenuDatovka.qml" line="98"/>
        <location filename="../../qml/popups/DrawerMenuDatovka.qml" line="118"/>
        <source>Log Viewer</source>
        <translation>Перегляд журналу</translation>
    </message>
    <message>
        <location filename="../../qml/popups/DrawerMenuDatovka.qml" line="99"/>
        <location filename="../../qml/popups/DrawerMenuDatovka.qml" line="119"/>
        <location filename="../../qml/popups/DrawerMenuDatovka.qml" line="99"/>
        <location filename="../../qml/popups/DrawerMenuDatovka.qml" line="119"/>
        <source>User Guide</source>
        <translation>Посібник користувача</translation>
    </message>
    <message>
        <location filename="../../qml/popups/DrawerMenuDatovka.qml" line="100"/>
        <location filename="../../qml/popups/DrawerMenuDatovka.qml" line="120"/>
        <location filename="../../qml/popups/DrawerMenuDatovka.qml" line="100"/>
        <location filename="../../qml/popups/DrawerMenuDatovka.qml" line="120"/>
        <source>About Datovka</source>
        <translation>Про програму Datovka</translation>
    </message>
</context>
<context>
    <name>ErrorEntry</name>
    <message>
        <location filename="../../src/datovka_shared/records_management/json/entry_error.cpp" line="312"/>
        <source>No error occurred</source>
        <translation>Жодної помилки не відбулося</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/records_management/json/entry_error.cpp" line="315"/>
        <source>Request was malformed</source>
        <translation>Запит був сформований неправильно</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/records_management/json/entry_error.cpp" line="318"/>
        <source>Identifier is missing</source>
        <translation>Ідентифікатор відсутній</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/records_management/json/entry_error.cpp" line="321"/>
        <source>Supplied identifier is wrong</source>
        <translation>Надано неправильний ідентифікатор</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/records_management/json/entry_error.cpp" line="324"/>
        <source>File format is not supported</source>
        <translation>Формат файлу не підтримується</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/records_management/json/entry_error.cpp" line="327"/>
        <source>Data are already present</source>
        <translation>Дані вже присутні</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/records_management/json/entry_error.cpp" line="330"/>
        <source>Service limit was exceeded</source>
        <translation>Було перевищено ліміт обслуговування</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/records_management/json/entry_error.cpp" line="333"/>
        <source>Unspecified error</source>
        <translation>Невизначена помилка</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/records_management/json/entry_error.cpp" line="337"/>
        <source>Unknown error</source>
        <translation>Невідома помилка</translation>
    </message>
</context>
<context>
    <name>Export</name>
    <message>
        <location filename="../../src/datovka_shared/html/html_export.cpp" line="95"/>
        <source>at</source>
        <translation>о</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/html/html_export.cpp" line="162"/>
        <location filename="../../src/datovka_shared/html/html_export.cpp" line="922"/>
        <source>User Type</source>
        <translation>Тип користувача</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/html/html_export.cpp" line="169"/>
        <source>Full Name</source>
        <translation>Повне ім&apos;я</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/html/html_export.cpp" line="277"/>
        <source>paragraph</source>
        <translation>абзац</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/html/html_export.cpp" line="281"/>
        <source>letter</source>
        <translation>буква</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/html/html_export.cpp" line="303"/>
        <location filename="../../src/datovka_shared/html/html_export.cpp" line="489"/>
        <source>Our Reference Number</source>
        <translation>Наш довідковий номер</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/html/html_export.cpp" line="307"/>
        <location filename="../../src/datovka_shared/html/html_export.cpp" line="492"/>
        <source>Our File Mark</source>
        <translation>Позначка нашого файлу</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/html/html_export.cpp" line="311"/>
        <location filename="../../src/datovka_shared/html/html_export.cpp" line="495"/>
        <source>Your Reference Number</source>
        <translation>Ваш довідковий номер</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/html/html_export.cpp" line="315"/>
        <location filename="../../src/datovka_shared/html/html_export.cpp" line="498"/>
        <source>Your File Mark</source>
        <translation>Позначка вашого файлу</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/html/html_export.cpp" line="354"/>
        <location filename="../../src/datovka_shared/html/html_export.cpp" line="501"/>
        <source>To Hands</source>
        <translation>До рук</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/html/html_export.cpp" line="349"/>
        <location filename="../../src/datovka_shared/html/html_export.cpp" line="487"/>
        <source>Mandate</source>
        <translation>Посвідчення</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/html/html_export.cpp" line="358"/>
        <location filename="../../src/datovka_shared/html/html_export.cpp" line="503"/>
        <source>Personal Delivery</source>
        <translation>Особиста доставка</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/html/html_export.cpp" line="359"/>
        <location filename="../../src/datovka_shared/html/html_export.cpp" line="505"/>
        <location filename="../../src/datovka_shared/html/html_export.cpp" line="508"/>
        <location filename="../../src/datovka_shared/html/html_export.cpp" line="628"/>
        <source>yes</source>
        <translation>так</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/html/html_export.cpp" line="380"/>
        <location filename="../../src/datovka_shared/html/html_export.cpp" line="454"/>
        <location filename="../../src/datovka_shared/html/html_export.cpp" line="718"/>
        <source>Sender</source>
        <translation>Відправник</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/html/html_export.cpp" line="381"/>
        <location filename="../../src/datovka_shared/html/html_export.cpp" line="470"/>
        <location filename="../../src/datovka_shared/html/html_export.cpp" line="718"/>
        <source>Recipient</source>
        <translation>Отримувач</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/html/html_export.cpp" line="382"/>
        <source>Name</source>
        <translation>Ім&apos;я</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/html/html_export.cpp" line="383"/>
        <source>Address</source>
        <translation>Адреса</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/html/html_export.cpp" line="384"/>
        <location filename="../../src/datovka_shared/html/html_export.cpp" line="458"/>
        <location filename="../../src/datovka_shared/html/html_export.cpp" line="474"/>
        <location filename="../../src/datovka_shared/html/html_export.cpp" line="741"/>
        <source>Data-Box ID</source>
        <translation>ID скриньки</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/html/html_export.cpp" line="386"/>
        <location filename="../../src/datovka_shared/html/html_export.cpp" line="460"/>
        <location filename="../../src/datovka_shared/html/html_export.cpp" line="476"/>
        <location filename="../../src/datovka_shared/html/html_export.cpp" line="742"/>
        <source>Data-Box Type</source>
        <translation>Тип датової скриньки</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/html/html_export.cpp" line="389"/>
        <location filename="../../src/datovka_shared/html/html_export.cpp" line="463"/>
        <source>Message Author</source>
        <translation>Автор повідомлення</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/html/html_export.cpp" line="405"/>
        <source>Delivery Info</source>
        <translation>Інформація щодо доручення</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/html/html_export.cpp" line="408"/>
        <source>Acceptance Info</source>
        <translation>Інформація щодо прийняття</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/html/html_export.cpp" line="412"/>
        <location filename="../../src/datovka_shared/html/html_export.cpp" line="425"/>
        <source>Message Envelope</source>
        <translation>Конверт повідомлення</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/html/html_export.cpp" line="420"/>
        <source>Message Delivery Info</source>
        <translation>Інформація щодо доручення повідомлення</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/html/html_export.cpp" line="422"/>
        <source>Message Acceptance Info</source>
        <translation>Інформація щодо прийняття повідомлення</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/html/html_export.cpp" line="429"/>
        <location filename="../../src/datovka_shared/html/html_export.cpp" line="559"/>
        <location filename="../../src/datovka_shared/html/html_export.cpp" line="619"/>
        <location filename="../../src/datovka_shared/html/html_export.cpp" line="717"/>
        <source>Subject</source>
        <translation>Тема</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/html/html_export.cpp" line="430"/>
        <location filename="../../src/datovka_shared/html/html_export.cpp" line="558"/>
        <location filename="../../src/datovka_shared/html/html_export.cpp" line="618"/>
        <location filename="../../src/datovka_shared/html/html_export.cpp" line="716"/>
        <source>Message ID</source>
        <translation>Ідентифікатор повідомлення</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/html/html_export.cpp" line="432"/>
        <location filename="../../src/datovka_shared/html/html_export.cpp" line="561"/>
        <location filename="../../src/datovka_shared/html/html_export.cpp" line="623"/>
        <source>Message Type</source>
        <translation>Тип повідомлення</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/html/html_export.cpp" line="436"/>
        <location filename="../../src/datovka_shared/html/html_export.cpp" line="528"/>
        <location filename="../../src/datovka_shared/html/html_export.cpp" line="564"/>
        <location filename="../../src/datovka_shared/html/html_export.cpp" line="667"/>
        <location filename="../../src/datovka_shared/html/html_export.cpp" line="682"/>
        <source>Message State</source>
        <translation>Стан повідомлення</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/html/html_export.cpp" line="440"/>
        <location filename="../../src/datovka_shared/html/html_export.cpp" line="526"/>
        <location filename="../../src/datovka_shared/html/html_export.cpp" line="568"/>
        <location filename="../../src/datovka_shared/html/html_export.cpp" line="670"/>
        <location filename="../../src/datovka_shared/html/html_export.cpp" line="679"/>
        <location filename="../../src/datovka_shared/html/html_export.cpp" line="721"/>
        <source>Acceptance Time</source>
        <translation>Час приймання</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/html/html_export.cpp" line="443"/>
        <location filename="../../src/datovka_shared/html/html_export.cpp" line="524"/>
        <location filename="../../src/datovka_shared/html/html_export.cpp" line="571"/>
        <location filename="../../src/datovka_shared/html/html_export.cpp" line="673"/>
        <location filename="../../src/datovka_shared/html/html_export.cpp" line="677"/>
        <source>Delivery Time</source>
        <translation>Час доставки</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/html/html_export.cpp" line="447"/>
        <location filename="../../src/datovka_shared/html/html_export.cpp" line="620"/>
        <location filename="../../src/datovka_shared/html/html_export.cpp" line="723"/>
        <source>Attachment Size</source>
        <translation>Розмір вкладень</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/html/html_export.cpp" line="740"/>
        <source>Data-Box Info</source>
        <translation>Інформація про датову скриньку</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/html/html_export.cpp" line="744"/>
        <source>Open Addressing</source>
        <translation>Відкрита адресація</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/html/html_export.cpp" line="746"/>
        <source>Yes</source>
        <translation>Так</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/html/html_export.cpp" line="746"/>
        <source>No</source>
        <translation>Ні</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/html/html_export.cpp" line="747"/>
        <source>Data-Box State</source>
        <translation>Стан датовох скриньки</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/html/html_export.cpp" line="750"/>
        <source>Upper Data-Box ID</source>
        <translation>ID родительської скриньки</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/html/html_export.cpp" line="754"/>
        <source>OVM ID</source>
        <translation>OMV ID</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/html/html_export.cpp" line="758"/>
        <location filename="../../src/datovka_shared/html/html_export.cpp" line="931"/>
        <source>ROB Identification</source>
        <translation>Ідентифікація ROB</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/html/html_export.cpp" line="760"/>
        <location filename="../../src/datovka_shared/html/html_export.cpp" line="933"/>
        <source>identified</source>
        <translation>ідентифікован</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/html/html_export.cpp" line="760"/>
        <location filename="../../src/datovka_shared/html/html_export.cpp" line="933"/>
        <source>not identified</source>
        <translation>не ідентифікован</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/html/html_export.cpp" line="764"/>
        <source>Data-Box Owner Info</source>
        <translation>Інформація про власника датової скриньки</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/html/html_export.cpp" line="766"/>
        <location filename="../../src/datovka_shared/html/html_export.cpp" line="850"/>
        <source>Company Name</source>
        <translation>Назва компанії</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/html/html_export.cpp" line="770"/>
        <location filename="../../src/datovka_shared/html/html_export.cpp" line="854"/>
        <source>IČ</source>
        <translation>IČ</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/html/html_export.cpp" line="773"/>
        <location filename="../../src/datovka_shared/html/html_export.cpp" line="857"/>
        <source>Given Names</source>
        <translation>Імена</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/html/html_export.cpp" line="777"/>
        <location filename="../../src/datovka_shared/html/html_export.cpp" line="861"/>
        <source>Last Name</source>
        <translation>Прізвище</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/html/html_export.cpp" line="783"/>
        <location filename="../../src/datovka_shared/html/html_export.cpp" line="865"/>
        <source>Date of Birth</source>
        <translation>Дата народження</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/html/html_export.cpp" line="787"/>
        <source>City of Birth</source>
        <translation>Місто народження</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/html/html_export.cpp" line="791"/>
        <source>County of Birth</source>
        <translation>Область народження</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/html/html_export.cpp" line="795"/>
        <source>State of Birth</source>
        <translation>Країна народження</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/html/html_export.cpp" line="801"/>
        <location filename="../../src/datovka_shared/html/html_export.cpp" line="905"/>
        <source>Street of Residence</source>
        <translation>Місце реєстрації компанії</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/html/html_export.cpp" line="805"/>
        <location filename="../../src/datovka_shared/html/html_export.cpp" line="875"/>
        <source>Number in Street</source>
        <translation>Номер на вулиці</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/html/html_export.cpp" line="809"/>
        <location filename="../../src/datovka_shared/html/html_export.cpp" line="879"/>
        <source>Number in Municipality</source>
        <translation>Номер у кадастрі</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/html/html_export.cpp" line="813"/>
        <location filename="../../src/datovka_shared/html/html_export.cpp" line="891"/>
        <location filename="../../src/datovka_shared/html/html_export.cpp" line="913"/>
        <source>Zip Code</source>
        <translation>Поштовий індекс</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/html/html_export.cpp" line="817"/>
        <source>District of Residence</source>
        <translation>Район проживання</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/html/html_export.cpp" line="821"/>
        <location filename="../../src/datovka_shared/html/html_export.cpp" line="909"/>
        <source>City of Residence</source>
        <translation>Місто проживання</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/html/html_export.cpp" line="825"/>
        <location filename="../../src/datovka_shared/html/html_export.cpp" line="917"/>
        <source>State of Residence</source>
        <translation>Країна проживання</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/html/html_export.cpp" line="829"/>
        <source>Nationality</source>
        <translation>Громадянство</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/html/html_export.cpp" line="833"/>
        <location filename="../../src/datovka_shared/html/html_export.cpp" line="899"/>
        <source>RUIAN Address Code</source>
        <translation>Код адреси RUIAN</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/html/html_export.cpp" line="848"/>
        <source>User Info</source>
        <translation>Інформація про користувача</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/html/html_export.cpp" line="871"/>
        <source>Street</source>
        <translation>Вулиця</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/html/html_export.cpp" line="883"/>
        <source>District</source>
        <translation>Район</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/html/html_export.cpp" line="887"/>
        <source>City</source>
        <translation>Місто</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/html/html_export.cpp" line="895"/>
        <source>State</source>
        <translation>Країна</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/html/html_export.cpp" line="924"/>
        <source>Permissions</source>
        <translation>Дозволи</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/html/html_export.cpp" line="927"/>
        <source>Unique User ISDS ID</source>
        <translation>Унікальний ID користувача ISDS</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/html/html_export.cpp" line="952"/>
        <source>Local Database Location</source>
        <translation>Розміщення локальної бази даних</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/html/html_export.cpp" line="955"/>
        <source>Data box has no own message database associated.</source>
        <translation>Скринька не має власної бази даних повідомлень.</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/html/html_export.cpp" line="958"/>
        <source>local database file</source>
        <translation>локальна база даних</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/html/html_export.cpp" line="983"/>
        <source>All Messages</source>
        <translation>Усі повідомлення</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/html/html_export.cpp" line="984"/>
        <source>Received</source>
        <translation>Отримані</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/html/html_export.cpp" line="985"/>
        <source>Sent</source>
        <translation>Надіслані</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/html/html_export.cpp" line="994"/>
        <source>Signature</source>
        <translation>Підпис</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/html/html_export.cpp" line="996"/>
        <source>Message Signature</source>
        <translation>Підпис повідомлення</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/html/html_export.cpp" line="999"/>
        <source>Signing Certificate</source>
        <translation>Сертифікат для підписування</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/html/html_export.cpp" line="1002"/>
        <source>Time Stamp</source>
        <translation>Відмітка часу</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/html/html_export.cpp" line="946"/>
        <source>Password Expiration</source>
        <translation>Термін дії пароля</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/html/html_export.cpp" line="505"/>
        <location filename="../../src/datovka_shared/html/html_export.cpp" line="508"/>
        <location filename="../../src/datovka_shared/html/html_export.cpp" line="628"/>
        <source>no</source>
        <translation>ні</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/html/html_export.cpp" line="485"/>
        <location filename="../../src/datovka_shared/html/html_export.cpp" line="488"/>
        <source>not specified</source>
        <translation>не ідентифікован</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/html/html_export.cpp" line="506"/>
        <location filename="../../src/datovka_shared/html/html_export.cpp" line="626"/>
        <source>Prohibited Acceptance through Fiction</source>
        <translation>Заборонити прийняття через фікцію</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/html/html_export.cpp" line="513"/>
        <source>Message Events</source>
        <translation>Події повідомлення</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/html/html_export.cpp" line="557"/>
        <location filename="../../src/datovka_shared/html/html_export.cpp" line="617"/>
        <location filename="../../src/datovka_shared/html/html_export.cpp" line="715"/>
        <source>General</source>
        <translation>Загальне</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/html/html_export.cpp" line="593"/>
        <location filename="../../src/datovka_shared/html/html_export.cpp" line="662"/>
        <source>Additional</source>
        <translation>Додаткові</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/html/html_export.cpp" line="598"/>
        <location filename="../../src/datovka_shared/html/html_export.cpp" line="688"/>
        <source>Events</source>
        <translation>Події</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/html/html_export.cpp" line="536"/>
        <source>Message Attachments</source>
        <translation>Вкладення в повідомлення</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/html/html_export.cpp" line="698"/>
        <source>Records Management Location</source>
        <translation>Розташування управління записами</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/html/html_export.cpp" line="700"/>
        <source>Location</source>
        <translation>Розташування</translation>
    </message>
</context>
<context>
    <name>FileDialogue</name>
    <message>
        <location filename="../../qml/components/FileDialogue.qml" line="36"/>
        <location filename="../../qml/components/FileDialogue.qml" line="36"/>
        <source>Choose a File</source>
        <translation>Виберіть файл</translation>
    </message>
</context>
<context>
    <name>Files</name>
    <message>
        <location filename="../../src/files.cpp" line="288"/>
        <location filename="../../src/files.cpp" line="317"/>
        <location filename="../../src/files.cpp" line="326"/>
        <source>Open attachment error</source>
        <translation>Помилка підчас відкриття вкладеного файлу</translation>
    </message>
    <message>
        <location filename="../../src/files.cpp" line="289"/>
        <source>Cannot save selected file to disk for opening.</source>
        <translation>Не вдається зберегти вибраний файл на диск для відкриття.</translation>
    </message>
    <message>
        <location filename="../../src/files.cpp" line="318"/>
        <location filename="../../src/files.cpp" line="327"/>
        <source>There is no application to open this file format.</source>
        <translation>Немає програми для відкриття цього формату файлу.</translation>
    </message>
    <message>
        <location filename="../../src/files.cpp" line="319"/>
        <location filename="../../src/files.cpp" line="328"/>
        <source>File: &apos;%1&apos;</source>
        <translation>Файл: &apos;%1&apos;</translation>
    </message>
    <message>
        <location filename="../../src/files.cpp" line="401"/>
        <location filename="../../src/files.cpp" line="763"/>
        <source>ZFO missing</source>
        <translation>ZFO відсутній</translation>
    </message>
    <message>
        <location filename="../../src/files.cpp" line="402"/>
        <location filename="../../src/files.cpp" line="764"/>
        <source>ZFO message is not present in local database.</source>
        <translation>Повідомлення ZFO відсутнє в локальній базі даних.</translation>
    </message>
    <message>
        <location filename="../../src/files.cpp" line="403"/>
        <location filename="../../src/files.cpp" line="765"/>
        <source>Download complete message again to obtain it.</source>
        <translation>Завантажте повне повідомлення ще раз.</translation>
    </message>
    <message>
        <location filename="../../src/files.cpp" line="683"/>
        <source>Saving Successful</source>
        <translation>Збереження успішне</translation>
    </message>
    <message>
        <location filename="../../src/files.cpp" line="684"/>
        <source>Files have been saved.</source>
        <translation>Files have been saved.</translation>
    </message>
    <message>
        <location filename="../../src/files.cpp" line="685"/>
        <source>Path: &apos;%1&apos;</source>
        <translation>Шлях: &apos;%1&apos;</translation>
    </message>
    <message>
        <location filename="../../src/files.cpp" line="687"/>
        <source>Saving Failed</source>
        <translation>Не вдалося зберегти</translation>
    </message>
    <message>
        <location filename="../../src/files.cpp" line="688"/>
        <source>Files have not been saved.</source>
        <translation>Файли не збережено.</translation>
    </message>
    <message>
        <location filename="../../src/files.cpp" line="689"/>
        <source>Please check whether the application has permissions to access the storage.</source>
        <translation>Будь ласка, перевірте, чи програма має дозволи на доступ до сховища.</translation>
    </message>
</context>
<context>
    <name>FilterBar</name>
    <message>
        <location filename="../../qml/components/FilterBar.qml" line="51"/>
        <location filename="../../qml/components/FilterBar.qml" line="51"/>
        <source>Enter filter text</source>
        <translation>Введіть текст для пошуку</translation>
    </message>
</context>
<context>
    <name>InputDialogue</name>
    <message>
        <location filename="../../qml/dialogues/InputDialogue.qml" line="114"/>
        <location filename="../../qml/dialogues/InputDialogue.qml" line="114"/>
        <source>Close</source>
        <translation>Закрити</translation>
    </message>
    <message>
        <location filename="../../qml/dialogues/InputDialogue.qml" line="123"/>
        <location filename="../../qml/dialogues/InputDialogue.qml" line="123"/>
        <source>OK</source>
        <translation>ОК</translation>
    </message>
</context>
<context>
    <name>IosHelper</name>
    <message>
        <location filename="../../src/auxiliaries/ios_helper.cpp" line="59"/>
        <source>iCloud error</source>
        <translation>Помилка iCloud</translation>
    </message>
    <message>
        <location filename="../../src/auxiliaries/ios_helper.cpp" line="60"/>
        <source>Unable to access iCloud account. Open the settings and check your iCloud settings.</source>
        <translation>Не вдається отримати доступ до акаунту iCloud. Відкрийте налаштування та перевірте налаштування iCloud.</translation>
    </message>
    <message>
        <location filename="../../src/auxiliaries/ios_helper.cpp" line="80"/>
        <source>Unable to access iCloud!</source>
        <translation>Не вдається отримати доступ до iCloud!</translation>
    </message>
    <message>
        <location filename="../../src/auxiliaries/ios_helper.cpp" line="85"/>
        <source>Cannot create subdirectory &apos;%1&apos; in iCloud.</source>
        <translation>Не вдається створити підкаталог &quot;%1&quot; в iCloud.</translation>
    </message>
    <message>
        <location filename="../../src/auxiliaries/ios_helper.cpp" line="90"/>
        <source>File &apos;%1&apos; already exists in iCloud.</source>
        <translation>Файл &quot;%1&quot; вже існує в iCloud.</translation>
    </message>
    <message>
        <location filename="../../src/auxiliaries/ios_helper.cpp" line="94"/>
        <source>File &apos;%1&apos; upload failed.</source>
        <translation>Не вдалося завантажити файл &quot;%1&quot;.</translation>
    </message>
    <message>
        <location filename="../../src/auxiliaries/ios_helper.cpp" line="97"/>
        <source>File &apos;%1&apos; has been stored into iCloud.</source>
        <translation>Файл &apos;%1&apos; збережено в iCloud.</translation>
    </message>
    <message>
        <location filename="../../src/auxiliaries/ios_helper.cpp" line="113"/>
        <source>Saved to iCloud</source>
        <translation>Збережено в iCloud</translation>
    </message>
    <message>
        <location filename="../../src/auxiliaries/ios_helper.cpp" line="114"/>
        <source>Files have been stored into iCloud.</source>
        <translation>Файли були збережені в iCloud.</translation>
    </message>
    <message>
        <location filename="../../src/auxiliaries/ios_helper.cpp" line="115"/>
        <source>Path: &apos;%1&apos;</source>
        <translation>Шлях: &apos;%1&apos;</translation>
    </message>
    <message>
        <location filename="../../src/auxiliaries/ios_helper.cpp" line="121"/>
        <source>iCloud Problem</source>
        <translation>Проблема із iCloud</translation>
    </message>
    <message>
        <location filename="../../src/auxiliaries/ios_helper.cpp" line="122"/>
        <source>Files have not been saved!</source>
        <translation>Файли не збережено!</translation>
    </message>
</context>
<context>
    <name>Isds</name>
    <message>
        <location filename="../../src/datovka_shared/isds/to_text_conversion.cpp" line="64"/>
        <source>User type</source>
        <translation>Тип користувача</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/isds/to_text_conversion.cpp" line="70"/>
        <source>Full name</source>
        <translation>Повне ім&apos;я</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/isds/to_text_conversion.cpp" line="77"/>
        <source>Date of birth</source>
        <translation>Дата народження</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/isds/to_text_conversion.cpp" line="83"/>
        <source>City of birth</source>
        <translation>Місто народження</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/isds/to_text_conversion.cpp" line="87"/>
        <source>County of birth</source>
        <translation>Область народження</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/isds/to_text_conversion.cpp" line="91"/>
        <source>RUIAN address code</source>
        <translation>Код адреси RUIAN</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/isds/to_text_conversion.cpp" line="95"/>
        <source>Full address</source>
        <translation>Повна адреса</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/isds/to_text_conversion.cpp" line="100"/>
        <source>ROB identification</source>
        <translation>Ідентифікація ROB</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/isds/to_text_conversion.cpp" line="101"/>
        <source>identified</source>
        <translation>ідентифікован</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/isds/to_text_conversion.cpp" line="101"/>
        <source>not identified</source>
        <translation>не ідентифікован</translation>
    </message>
</context>
<context>
    <name>IsdsConversion</name>
    <message>
        <location filename="../../src/isds/conversion/isds_conversion.cpp" line="35"/>
        <source>Primary user</source>
        <translation>Основний користувач</translation>
    </message>
    <message>
        <location filename="../../src/isds/conversion/isds_conversion.cpp" line="38"/>
        <source>Entrusted user</source>
        <translation>Довірена особа</translation>
    </message>
    <message>
        <location filename="../../src/isds/conversion/isds_conversion.cpp" line="41"/>
        <source>Administrator</source>
        <translation>Адміністратор</translation>
    </message>
    <message>
        <location filename="../../src/isds/conversion/isds_conversion.cpp" line="43"/>
        <source>Official</source>
        <translation>Система ISDS</translation>
    </message>
    <message>
        <location filename="../../src/isds/conversion/isds_conversion.cpp" line="47"/>
        <source>Liquidator</source>
        <translation>Ліквідатор</translation>
    </message>
    <message>
        <location filename="../../src/isds/conversion/isds_conversion.cpp" line="49"/>
        <source>Receiver</source>
        <translation>Вимушений адміністратор</translation>
    </message>
    <message>
        <location filename="../../src/isds/conversion/isds_conversion.cpp" line="51"/>
        <source>Guardian</source>
        <translation>Опікун</translation>
    </message>
    <message>
        <location filename="../../src/isds/conversion/isds_conversion.cpp" line="45"/>
        <source>Virtual</source>
        <translation>Віртуальна особистість</translation>
    </message>
</context>
<context>
    <name>IsdsWrapper</name>
    <message>
        <location filename="../../src/isds/isds_wrapper.cpp" line="192"/>
        <source>Find data box: %1</source>
        <translation>Знайти датову скриньку: %1</translation>
    </message>
    <message>
        <location filename="../../src/isds/isds_wrapper.cpp" line="193"/>
        <source>Failed finding data box to phrase &apos;%1&apos;.</source>
        <translation>Не вдалося знайти датову скриньку відповідно до фрази &quot;%1&quot;.</translation>
    </message>
    <message>
        <location filename="../../src/isds/isds_wrapper.cpp" line="251"/>
        <source>No zfo files or selected folder.</source>
        <translation>Немає файлів zfo або вибраної папки.</translation>
    </message>
    <message>
        <location filename="../../src/isds/isds_wrapper.cpp" line="272"/>
        <source>No zfo files in the selected directory.</source>
        <translation>У вибраній папці немає файлів zfo.</translation>
    </message>
    <message>
        <location filename="../../src/isds/isds_wrapper.cpp" line="278"/>
        <source>No zfo file for import.</source>
        <translation>Немає файлу zfo для імпорту.</translation>
    </message>
    <message>
        <location filename="../../src/isds/isds_wrapper.cpp" line="263"/>
        <source>No data box for zfo import.</source>
        <translation>Немає скриньки для імпорту zfo.</translation>
    </message>
    <message>
        <location filename="../../src/isds/isds_wrapper.cpp" line="291"/>
        <source>ZFO messages cannot be imported because there is no active data box for their verification.</source>
        <translation>Імпорт повідомлень у форматі ZFO неможливий через відсутність активної скриньки для їх перевірки.</translation>
    </message>
    <message>
        <location filename="../../src/isds/isds_wrapper.cpp" line="401"/>
        <source>SMS code sending request has been cancelled.</source>
        <translation>Скасовано запит на відправку SMS-коду.</translation>
    </message>
    <message>
        <location filename="../../src/isds/isds_wrapper.cpp" line="500"/>
        <location filename="../../src/isds/isds_wrapper.cpp" line="506"/>
        <source>Error sending message</source>
        <translation>Помилка надсилання повідомлення</translation>
    </message>
    <message>
        <location filename="../../src/isds/isds_wrapper.cpp" line="501"/>
        <source>Pre-paid transfer charges for message reply have been enabled.</source>
        <translation>Увімкнено передоплачену плату за переказ для відповіді на повідомлення.</translation>
    </message>
    <message>
        <location filename="../../src/isds/isds_wrapper.cpp" line="502"/>
        <source>The sender reference number must be specified in the additional section in order to send the message.</source>
        <translation>Для відправлення повідомлення в додатковому розділі необхідно вказати номер відправника.</translation>
    </message>
    <message>
        <location filename="../../src/isds/isds_wrapper.cpp" line="507"/>
        <source>Message uses the offered payment of transfer charges by the recipient.</source>
        <translation>Використати запропоновану одержувачем плату за переказ.</translation>
    </message>
    <message>
        <location filename="../../src/isds/isds_wrapper.cpp" line="508"/>
        <source>The recipient reference number must be specified in the additional section in order to send the message.</source>
        <translation>Для відправки повідомлення в додатковому розділі необхідно вказати контрольний номер одержувача.</translation>
    </message>
    <message>
        <location filename="../../src/isds/isds_wrapper.cpp" line="632"/>
        <location filename="../../src/isds/isds_wrapper.cpp" line="637"/>
        <location filename="../../src/isds/isds_wrapper.cpp" line="642"/>
        <source>Unknown Message Type</source>
        <translation>Невідомий тип повідомлення</translation>
    </message>
    <message>
        <location filename="../../src/isds/isds_wrapper.cpp" line="633"/>
        <source>No information about the recipient data box &apos;%1&apos; could be obtained. It is unknown whether public or commercial messages can be sent to this recipient.</source>
        <translation>Не вдалося отримати інформацію про датову скриньку одержувача &apos;%1&apos;. Невідомо, чи можна надсилати цьому одержувачу публічні чи комерційні повідомлення.</translation>
    </message>
    <message>
        <location filename="../../src/isds/isds_wrapper.cpp" line="638"/>
        <source>No commercial message to the recipient data box &apos;%1&apos; can be sent. It is unknown whether a public messages can be sent to this recipient.</source>
        <translation>Не можна надіслати комерційне повідомлення до датової скриньки одержувача &apos;%1&apos;. Невідомо, чи можна надіслати публічні повідомлення цьому одержувачу.</translation>
    </message>
    <message>
        <location filename="../../src/isds/isds_wrapper.cpp" line="643"/>
        <source>No public message to the recipient data box &apos;%1&apos; can be sent. It is unknown whether a commercial messages can be sent to this recipient.</source>
        <translation>Не можна надіслати публічне повідомлення до датової скриньки одержувача &apos;%1&apos;. Невідомо, чи можна надіслати комерційне повідомлення цьому одержувачу.</translation>
    </message>
    <message>
        <location filename="../../src/isds/isds_wrapper.cpp" line="647"/>
        <source>Cannot send message</source>
        <translation>Не вдається надіслати повідомлення</translation>
    </message>
    <message>
        <location filename="../../src/isds/isds_wrapper.cpp" line="648"/>
        <source>No public data message nor a commercial data message (PDZ) can be sent to the recipient data box &apos;%1&apos;.</source>
        <translation>До датової скриньки одержувача &apos;%1&apos; не можна надсилати ні публічне повідомлення, ні повідомлення комерційних даних (PDZ).</translation>
    </message>
    <message>
        <location filename="../../src/isds/isds_wrapper.cpp" line="649"/>
        <source>Receiving of PDZs has been disabled in the recipient data box or there are no available payment methods for PDZs.</source>
        <translation>Отримання PDZ було вимкнено у датовій скриньці одержувача або немає доступних способів оплати для PDZ.</translation>
    </message>
    <message>
        <location filename="../../src/isds/isds_wrapper.cpp" line="654"/>
        <source>Data box is not active</source>
        <translation>Датова скринька неактивна</translation>
    </message>
    <message>
        <location filename="../../src/isds/isds_wrapper.cpp" line="668"/>
        <source>Recipient Search Failed</source>
        <translation>Помилка пошуку одержувача</translation>
    </message>
    <message>
        <location filename="../../src/isds/isds_wrapper.cpp" line="669"/>
        <source>Information about recipient data box &apos;%1&apos; couldn&apos;t be obtained.</source>
        <translation>Не вдалося отримати інформацію про датову скриньку одержувача &quot;%1&quot;.</translation>
    </message>
    <message>
        <location filename="../../src/isds/isds_wrapper.cpp" line="670"/>
        <source>The message may not be delivered.</source>
        <translation>Повідомлення не може бути доставлено.</translation>
    </message>
    <message>
        <location filename="../../src/isds/isds_wrapper.cpp" line="764"/>
        <source>SMS code: %1</source>
        <translation>SMS-код: %1</translation>
    </message>
    <message>
        <location filename="../../src/isds/isds_wrapper.cpp" line="765"/>
        <source>Account &apos;%1&apos; requires authentication with an SMS code.</source>
        <translation>Акаунт &quot;%1&quot; вимагає вхід за допомогою SMS-коду.</translation>
    </message>
    <message>
        <location filename="../../src/isds/isds_wrapper.cpp" line="767"/>
        <source>Do you want to request the SMS code now?</source>
        <translation>Бажаєте відправити SMS-код зараз?</translation>
    </message>
    <message>
        <location filename="../../src/isds/isds_wrapper.cpp" line="656"/>
        <location filename="../../src/isds/isds_wrapper.cpp" line="663"/>
        <source>The message cannot be delivered.</source>
        <translation>Повідомлення неможливо доставити.</translation>
    </message>
    <message>
        <location filename="../../src/isds/isds_wrapper.cpp" line="661"/>
        <source>Wrong Recipient</source>
        <translation>Неправильний одержувач</translation>
    </message>
    <message>
        <location filename="../../src/isds/isds_wrapper.cpp" line="807"/>
        <source>Internal error.</source>
        <translation>Внутрішня помилка.</translation>
    </message>
    <message>
        <location filename="../../src/isds/isds_wrapper.cpp" line="926"/>
        <source>Communication code for data box &apos;%1&apos; is required.</source>
        <translation>Для скриньки &quot;%1&quot; потрібен код звʼязку.</translation>
    </message>
    <message>
        <location filename="../../src/isds/isds_wrapper.cpp" line="940"/>
        <source>Password for data box &apos;%1&apos; missing.</source>
        <translation>Відсутній пароль для скриньки &quot;%1&quot;.</translation>
    </message>
    <message>
        <location filename="../../src/isds/isds_wrapper.cpp" line="954"/>
        <source>Certificate password for data box &apos;%1&apos; is required.</source>
        <translation>Потрібен пароль сертифіката для скриньки &quot;%1&quot;.</translation>
    </message>
    <message>
        <location filename="../../src/isds/isds_wrapper.cpp" line="970"/>
        <source>New data box problem: %1</source>
        <translation>Проблема при створенні скриньки: %1</translation>
    </message>
    <message>
        <location filename="../../src/isds/isds_wrapper.cpp" line="971"/>
        <source>New data box could not be created for username &apos;%1&apos;.</source>
        <translation>Створення нової скриньки для користувача з ім&apos;ям &quot;%1&quot; не вдалося.</translation>
    </message>
    <message>
        <location filename="../../src/isds/isds_wrapper.cpp" line="993"/>
        <source>Security code for data box &apos;%1&apos; required</source>
        <translation>Потрібен захисний код для скриньки &quot;%1&quot;</translation>
    </message>
    <message>
        <location filename="../../src/isds/isds_wrapper.cpp" line="1006"/>
        <source>SMS code for data box &apos;%1&apos; required</source>
        <translation>Потрібен SMS-код для скриньки &quot;%1&quot;</translation>
    </message>
    <message>
        <location filename="../../src/isds/isds_wrapper.cpp" line="925"/>
        <source>Enter communication code: %1</source>
        <translation>Введіть код зв&apos;язку: %1</translation>
    </message>
    <message>
        <location filename="../../src/isds/isds_wrapper.cpp" line="928"/>
        <source>Enter communication code</source>
        <translation>Введіть код зв&apos;язку</translation>
    </message>
    <message>
        <location filename="../../src/isds/isds_wrapper.cpp" line="939"/>
        <source>Enter password: %1</source>
        <translation>Введіть пароль: %1</translation>
    </message>
    <message>
        <location filename="../../src/isds/isds_wrapper.cpp" line="942"/>
        <source>Enter password</source>
        <translation>Введіть пароль</translation>
    </message>
    <message>
        <location filename="../../src/isds/isds_wrapper.cpp" line="953"/>
        <source>Enter certificate password: %1</source>
        <translation>Введіть пароль сертифіката: %1</translation>
    </message>
    <message>
        <location filename="../../src/isds/isds_wrapper.cpp" line="956"/>
        <source>Enter certificate password</source>
        <translation>Введіть пароль сертифіката</translation>
    </message>
    <message>
        <location filename="../../src/isds/isds_wrapper.cpp" line="972"/>
        <source>Note: You must change your password via the ISDS web portal if this is your first login to the data box. You can start using mobile Datovka to access the data box after changing.</source>
        <translation>Примітка: Ви повинні змінити свій пароль через веб-портал ISDS, якщо це ваш перший вхід у датову скриньку. Ви можете почати використовувати мобільну Datovka для доступу до датової скриньки після зміни.</translation>
    </message>
    <message>
        <location filename="../../src/isds/isds_wrapper.cpp" line="974"/>
        <source>Username change: %1</source>
        <translation>Зміна імені користувача: %1</translation>
    </message>
    <message>
        <location filename="../../src/isds/isds_wrapper.cpp" line="975"/>
        <source>Username could not be changed on the new username &apos;%1&apos;.</source>
        <translation>Не вдалося змінити ім’я користувача для нового імені користувача &quot;%1&quot;.</translation>
    </message>
    <message>
        <location filename="../../src/isds/isds_wrapper.cpp" line="978"/>
        <source>Login problem: %1</source>
        <translation>Проблема входу: %1</translation>
    </message>
    <message>
        <location filename="../../src/isds/isds_wrapper.cpp" line="979"/>
        <source>Error while logging in with username &apos;%1&apos;.</source>
        <translation>Помилка під час входу з іменем користувача &quot;%1&quot;.</translation>
    </message>
    <message>
        <location filename="../../src/isds/isds_wrapper.cpp" line="992"/>
        <source>Enter security code: %1</source>
        <translation>Введіть код безпеки: %1</translation>
    </message>
    <message>
        <location filename="../../src/isds/isds_wrapper.cpp" line="994"/>
        <source>Enter security code</source>
        <translation>Введіть код безпеки</translation>
    </message>
    <message>
        <location filename="../../src/isds/isds_wrapper.cpp" line="1005"/>
        <source>Enter SMS code: %1</source>
        <translation>Введіть SMS-код: %1</translation>
    </message>
    <message>
        <location filename="../../src/isds/isds_wrapper.cpp" line="1007"/>
        <source>Enter SMS code</source>
        <translation>Введіть SMS-код</translation>
    </message>
    <message>
        <location filename="../../src/isds/isds_wrapper.cpp" line="655"/>
        <source>Recipient data-box ID &apos;%1&apos; isn&apos;t active.</source>
        <translation>Датовв скринька одержувача &quot;%1&quot; неактивна.</translation>
    </message>
    <message>
        <location filename="../../src/isds/isds_wrapper.cpp" line="662"/>
        <source>Recipient with data-box ID &apos;%1&apos; doesn&apos;t exist.</source>
        <translation>Одержувач з ідентифікатором датовох скриньки &quot;%1&quot; не існує.</translation>
    </message>
    <message>
        <location filename="../../src/isds/isds_wrapper.cpp" line="1039"/>
        <source>ZFO import has been finished with the result:</source>
        <translation>Імпорт ZFO завершено з результатом:</translation>
    </message>
    <message>
        <location filename="../../src/isds/isds_wrapper.cpp" line="1075"/>
        <source>Message has been sent to &lt;b&gt;&apos;%1&apos;&lt;/b&gt; &lt;i&gt;(%2)&lt;/i&gt; as message number &lt;b&gt;%3&lt;/b&gt;.</source>
        <translation>Повідомлення надіслано на &lt;b&gt;&apos;%1&apos;&lt;/b&gt; &lt;i&gt;(%2)&lt;/i&gt; за номером &lt;b&gt;%3&lt;/b&gt;.</translation>
    </message>
    <message>
        <location filename="../../src/isds/isds_wrapper.cpp" line="1086"/>
        <source>Message has NOT been sent to &lt;b&gt;&apos;%1&apos;&lt;/b&gt; &lt;i&gt;(%2)&lt;/i&gt;. ISDS returns: %3</source>
        <translation>Повідомлення НЕ надіслано на адресу &lt;b&gt;&apos;%1&apos;&lt;/b&gt; &lt;i&gt;(%2)&lt;/i&gt;. ISDS повертає: %3</translation>
    </message>
    <message>
        <location filename="../../src/isds/isds_wrapper.cpp" line="1128"/>
        <source>PDZ sending: unavailable</source>
        <translation>Відправка PDZ: недоступна</translation>
    </message>
    <message>
        <location filename="../../src/isds/isds_wrapper.cpp" line="1143"/>
        <source>PDZ: active</source>
        <translation>PDZ: активна</translation>
    </message>
    <message>
        <location filename="../../src/isds/isds_wrapper.cpp" line="1146"/>
        <source>Subsidised by data box &apos;%1&apos;.</source>
        <translation>Субсидується датовою скринькою &quot;%1&quot;.</translation>
    </message>
    <message>
        <location filename="../../src/isds/isds_wrapper.cpp" line="1149"/>
        <source>Contract with Czech post</source>
        <translation>Договір з чеською поштою</translation>
    </message>
    <message>
        <location filename="../../src/isds/isds_wrapper.cpp" line="1153"/>
        <source>Remaining credit: %1 Kč</source>
        <translation>Залишок кредиту: %1 Kč</translation>
    </message>
</context>
<context>
    <name>Login</name>
    <message>
        <location filename="../../src/isds/isds_login.cpp" line="328"/>
        <source>MEP confirmation timeout.</source>
        <translation>Закінчився час очікування підтвердження MEP.</translation>
    </message>
    <message>
        <location filename="../../src/isds/isds_login.cpp" line="333"/>
        <source>MEP login was cancelled.</source>
        <translation>Вхід у MEP скасовано.</translation>
    </message>
    <message>
        <location filename="../../src/isds/isds_login.cpp" line="338"/>
        <source>Wrong MEP login credentials. Invalid username or communication code.</source>
        <translation>Неправильні облікові дані для входу в MEP. Недійсне ім’я користувача або код зв’язку.</translation>
    </message>
    <message>
        <location filename="../../src/isds/isds_login.cpp" line="343"/>
        <source>MEP login failed.</source>
        <translation>Помилка входу в MEP.</translation>
    </message>
</context>
<context>
    <name>MessageBox</name>
    <message>
        <location filename="../../qml/components/MessageBox.qml" line="147"/>
        <location filename="../../qml/components/MessageBox.qml" line="147"/>
        <source>Yes</source>
        <translation>Так</translation>
    </message>
    <message>
        <location filename="../../qml/components/MessageBox.qml" line="156"/>
        <location filename="../../qml/components/MessageBox.qml" line="156"/>
        <source>No</source>
        <translation>Ні</translation>
    </message>
</context>
<context>
    <name>MessageList</name>
    <message>
        <location filename="../../qml/components/MessageList.qml" line="93"/>
        <location filename="../../qml/components/MessageList.qml" line="93"/>
        <source>Delivered</source>
        <translation>Час доставлення</translation>
    </message>
    <message>
        <location filename="../../qml/components/MessageList.qml" line="99"/>
        <location filename="../../qml/components/MessageList.qml" line="99"/>
        <source>Accepted</source>
        <translation>Час отримання</translation>
    </message>
    <message>
        <location filename="../../qml/components/MessageList.qml" line="127"/>
        <location filename="../../qml/components/MessageList.qml" line="127"/>
        <source>ID</source>
        <translation>ID</translation>
    </message>
    <message>
        <location filename="../../qml/components/MessageList.qml" line="140"/>
        <location filename="../../qml/components/MessageList.qml" line="140"/>
        <source>Unread message from sender</source>
        <translation>Непрочитане повідомлення від відправника</translation>
    </message>
    <message>
        <location filename="../../qml/components/MessageList.qml" line="140"/>
        <location filename="../../qml/components/MessageList.qml" line="140"/>
        <source>Read message from sender</source>
        <translation>Прочитати повідомлення від відправника</translation>
    </message>
    <message>
        <location filename="../../qml/components/MessageList.qml" line="142"/>
        <location filename="../../qml/components/MessageList.qml" line="142"/>
        <source>Message to receiver</source>
        <translation>Повідомлення одержувачу</translation>
    </message>
    <message>
        <location filename="../../qml/components/MessageList.qml" line="145"/>
        <location filename="../../qml/components/MessageList.qml" line="145"/>
        <source>Subject</source>
        <translation>Тема</translation>
    </message>
</context>
<context>
    <name>Messages</name>
    <message>
        <location filename="../../src/messages.cpp" line="492"/>
        <source>Move</source>
        <translation>Перемістити</translation>
    </message>
    <message>
        <location filename="../../src/messages.cpp" line="493"/>
        <source>Create new</source>
        <translation>Створити новий</translation>
    </message>
    <message>
        <location filename="../../src/messages.cpp" line="921"/>
        <source>The message database for username &apos;%1&apos; couldn&apos;t be converted into the new format. A new empty database file has been created.</source>
        <translation>Базу даних повідомлень для імені користувача &apos;%1&apos; не вдалося перетворити в новий формат. Створено новий порожній файл бази даних.</translation>
    </message>
</context>
<context>
    <name>MvCrrVbhData</name>
    <message>
        <location filename="../../src/datovka_shared/gov_services/service/gov_mv_crr_vbh.cpp" line="91"/>
        <source>Driving licence ID</source>
        <translation>Посвідчення водія</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/gov_services/service/gov_mv_crr_vbh.cpp" line="92"/>
        <source>Enter driving licence ID without spaces</source>
        <translation>Введіть ідентифікатор водійського посвідчення без пробілів</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/gov_services/service/gov_mv_crr_vbh.cpp" line="102"/>
        <source>Data-box owner name</source>
        <translation>Ім&apos;я власника скриньки даних</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/gov_services/service/gov_mv_crr_vbh.cpp" line="112"/>
        <source>Data-box owner surname</source>
        <translation>Прізвище власника скриньки даних</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/gov_services/service/gov_mv_crr_vbh.cpp" line="122"/>
        <source>Birth date</source>
        <translation>Дата народження</translation>
    </message>
</context>
<context>
    <name>MvIrVpData</name>
    <message>
        <location filename="../../src/datovka_shared/gov_services/service/gov_mv_ir_vp.cpp" line="516"/>
        <source>Identification number (IČO)</source>
        <translation>IČO</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/gov_services/service/gov_mv_ir_vp.cpp" line="517"/>
        <source>Enter identification number (IČO)</source>
        <translation>Введіть ідентифікаційний номер (IČO)</translation>
    </message>
</context>
<context>
    <name>MvRtVtData</name>
    <message>
        <location filename="../../src/datovka_shared/gov_services/service/gov_mv_rt_vt.cpp" line="106"/>
        <source>Data-box owner name</source>
        <translation>Ім&apos;я власника скриньки даних</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/gov_services/service/gov_mv_rt_vt.cpp" line="116"/>
        <source>Data-box owner surname</source>
        <translation>Прізвище власника скриньки даних</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/gov_services/service/gov_mv_rt_vt.cpp" line="126"/>
        <source>Birth date</source>
        <translation>Дата народження</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/gov_services/service/gov_mv_rt_vt.cpp" line="136"/>
        <source>Birth region</source>
        <translation>Регіон народження</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/gov_services/service/gov_mv_rt_vt.cpp" line="146"/>
        <source>City</source>
        <translation>Місто</translation>
    </message>
</context>
<context>
    <name>MvRtpoVtData</name>
    <message>
        <location filename="../../src/datovka_shared/gov_services/service/gov_mv_rtpo_vt.cpp" line="521"/>
        <source>Identification number (IČO)</source>
        <translation>IČO</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/gov_services/service/gov_mv_rtpo_vt.cpp" line="522"/>
        <source>Enter identification number (IČO)</source>
        <translation>Введіть ідентифікаційний номер (IČO)</translation>
    </message>
</context>
<context>
    <name>MvSkdVpData</name>
    <message>
        <location filename="../../src/datovka_shared/gov_services/service/gov_mv_skd_vp.cpp" line="517"/>
        <source>Identification number (IČO)</source>
        <translation>IČO</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/gov_services/service/gov_mv_skd_vp.cpp" line="518"/>
        <source>Enter identification number (IČO)</source>
        <translation>Введіть ідентифікаційний номер (IČO)</translation>
    </message>
</context>
<context>
    <name>MvVrVpData</name>
    <message>
        <location filename="../../src/datovka_shared/gov_services/service/gov_mv_vr_vp.cpp" line="550"/>
        <source>Identification number (IČO)</source>
        <translation>IČO</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/gov_services/service/gov_mv_vr_vp.cpp" line="551"/>
        <source>Enter identification number (IČO)</source>
        <translation>Введіть ідентифікаційний номер (IČO)</translation>
    </message>
</context>
<context>
    <name>MvZrVpData</name>
    <message>
        <location filename="../../src/datovka_shared/gov_services/service/gov_mv_zr_vp.cpp" line="516"/>
        <source>Identification number (IČO)</source>
        <translation>IČO</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/gov_services/service/gov_mv_zr_vp.cpp" line="517"/>
        <source>Enter identification number (IČO)</source>
        <translation>Введіть ідентифікаційний номер (IČO)</translation>
    </message>
</context>
<context>
    <name>PageAboutApp</name>
    <message>
        <location filename="../../qml/pages/PageAboutApp.qml" line="32"/>
        <location filename="../../qml/pages/PageAboutApp.qml" line="32"/>
        <source>About Datovka</source>
        <translation>Про програму Datovka</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageAboutApp.qml" line="54"/>
        <location filename="../../qml/pages/PageAboutApp.qml" line="54"/>
        <source>Open application home page.</source>
        <translation>Відкрийте домашню сторінку програми.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageAboutApp.qml" line="65"/>
        <location filename="../../qml/pages/PageAboutApp.qml" line="65"/>
        <source>Datovka</source>
        <translation>Datovka</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageAboutApp.qml" line="65"/>
        <location filename="../../qml/pages/PageAboutApp.qml" line="65"/>
        <source>Free mobile data-box client.</source>
        <translation>Безкоштовний мобільний клієнт Датової-Скриньки.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageAboutApp.qml" line="83"/>
        <location filename="../../qml/pages/PageAboutApp.qml" line="83"/>
        <source>It&apos;s distributed free of charge. It doesn&apos;t contain advertisements or in-app purchases.</source>
        <translation>Поширюється на безкоштовній основі без використання вбудованої реклами та покупок в додатку.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageAboutApp.qml" line="108"/>
        <location filename="../../qml/pages/PageAboutApp.qml" line="108"/>
        <source>Learn more...</source>
        <translation>Детальніше…</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageAboutApp.qml" line="81"/>
        <location filename="../../qml/pages/PageAboutApp.qml" line="142"/>
        <location filename="../../qml/pages/PageAboutApp.qml" line="81"/>
        <location filename="../../qml/pages/PageAboutApp.qml" line="142"/>
        <source>This application is being developed by %1 as open source.</source>
        <translation>Програму розроблено компанією %1 як open source.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageAboutApp.qml" line="85"/>
        <location filename="../../qml/pages/PageAboutApp.qml" line="85"/>
        <source>We want to provide an easy-to-use data-box client.</source>
        <translation>Ми хочемо забезпечити простий у використанні клієнт скриньки із даними.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageAboutApp.qml" line="87"/>
        <location filename="../../qml/pages/PageAboutApp.qml" line="87"/>
        <source>Do you want to support us in this effort?</source>
        <translation>Бажаєте підтримати нашу ініціативу?</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageAboutApp.qml" line="89"/>
        <location filename="../../qml/pages/PageAboutApp.qml" line="89"/>
        <source>Make a donation.</source>
        <translation>Зробити пожертву.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageAboutApp.qml" line="96"/>
        <location filename="../../qml/pages/PageAboutApp.qml" line="96"/>
        <source>The behaviour of the ISDS environment and this application is different from the behaviour of an e-mail client. Delivery of data messages is governed by law.</source>
        <translation>Поведінка системи скриньок із даними ISDS і цієї програми відрізняється від поведінки звичайних поштових клієнтів. Прийняття повідомлень із даними регулюється законодавством.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageAboutApp.qml" line="150"/>
        <location filename="../../qml/pages/PageAboutApp.qml" line="150"/>
        <source>If you have any problems with the application or you have found any errors or you just have an idea how to improve this application please contact us using the following e-mail address:</source>
        <translation>Якщо у вас виникли проблеми з додатком, або ви знайшли помилку, або ж ви просто маєте ідею як покращити додаток. Будь ласка, зв&apos;яжіться з нами, використовуючи адресу електронної пошти:</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageAboutApp.qml" line="156"/>
        <location filename="../../qml/pages/PageAboutApp.qml" line="156"/>
        <source>Show News</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageAboutApp.qml" line="157"/>
        <location filename="../../qml/pages/PageAboutApp.qml" line="157"/>
        <source>What&apos;s new?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageAboutApp.qml" line="178"/>
        <location filename="../../qml/pages/PageAboutApp.qml" line="178"/>
        <source>This application is available as it is. Usage of this application is at own risk. The association CZ.NIC is in no circumstances liable for any damage directly or indirectly caused by using or not using this application.</source>
        <translation>Цей додаток є доступний, як він є. Його використання здійснюється на власний ризик користувача. Спілка CZ.NIC несе відповідальність за будь-які збитки, спричинені використанням  цим додатком.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageAboutApp.qml" line="180"/>
        <location filename="../../qml/pages/PageAboutApp.qml" line="180"/>
        <source>CZ.NIC isn&apos;t the operator nor the administrator of the Data Box Information System (ISDS). The operation of the ISDS is regulated by Czech law and regulations.</source>
        <translation>CZ.NIC це ані оператор, ані адміністратор Інформаційних систем скриньок із даними (ISDS).</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageAboutApp.qml" line="189"/>
        <location filename="../../qml/pages/PageAboutApp.qml" line="189"/>
        <source>This software is distributed under the %1 licence.</source>
        <translation>Це програмне забезпечення поширюється під  ліцензією %1.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageAboutApp.qml" line="189"/>
        <location filename="../../qml/pages/PageAboutApp.qml" line="189"/>
        <source>GNU GPL version 3</source>
        <translation>GNU GPL модифікація 3</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageAboutApp.qml" line="211"/>
        <location filename="../../qml/pages/PageAboutApp.qml" line="211"/>
        <source>About Datovka.</source>
        <translation>Про програму Datovka.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageAboutApp.qml" line="212"/>
        <location filename="../../qml/pages/PageAboutApp.qml" line="212"/>
        <source>About</source>
        <translation>Про програму</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageAboutApp.qml" line="217"/>
        <location filename="../../qml/pages/PageAboutApp.qml" line="217"/>
        <source>Datovka development.</source>
        <translation>Розробка Datovky.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageAboutApp.qml" line="218"/>
        <location filename="../../qml/pages/PageAboutApp.qml" line="218"/>
        <source>Development</source>
        <translation>Розробка</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageAboutApp.qml" line="223"/>
        <location filename="../../qml/pages/PageAboutApp.qml" line="223"/>
        <source>Licence.</source>
        <translation>Ліцензія.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageAboutApp.qml" line="224"/>
        <location filename="../../qml/pages/PageAboutApp.qml" line="224"/>
        <source>Licence</source>
        <translation>Ліцензія</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageAboutApp.qml" line="98"/>
        <location filename="../../qml/pages/PageAboutApp.qml" line="98"/>
        <source>Read the &lt;a href=&quot;%1&quot;&gt;user manual&lt;/a&gt; for more information about the application behaviour.</source>
        <translation>Прочитайте &lt;a href=&quot;%1&quot;&gt;посібник користувача&lt;/a&gt; про те, як працює додаток.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageAboutApp.qml" line="131"/>
        <location filename="../../qml/pages/PageAboutApp.qml" line="131"/>
        <source>Open the home page of the CZ.NIC association.</source>
        <translation>Відкрити домашню сторінку асоціації CZ.NIC.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageAboutApp.qml" line="73"/>
        <location filename="../../qml/pages/PageAboutApp.qml" line="73"/>
        <source>Version: %1</source>
        <translation>Версія: %1</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageAboutApp.qml" line="106"/>
        <location filename="../../qml/pages/PageAboutApp.qml" line="106"/>
        <source>Did you know that there is a desktop version of Datovka?</source>
        <translation>Чи знаєте ви, що існує і десктопна версія Datovky?</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageAboutApp.qml" line="126"/>
        <location filename="../../qml/pages/PageAboutApp.qml" line="126"/>
        <source>Powered by</source>
        <translation>Працює на базі</translation>
    </message>
</context>
<context>
    <name>PageAccountDetail</name>
    <message>
        <location filename="../../qml/pages/PageAccountDetail.qml" line="55"/>
        <location filename="../../qml/pages/PageAccountDetail.qml" line="55"/>
        <source>Data-Box Info</source>
        <translation>Інформація про датову скриньку</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageAccountDetail.qml" line="63"/>
        <location filename="../../qml/pages/PageAccountDetail.qml" line="63"/>
        <source>Refresh information about the data box.</source>
        <translation>Перезавантажити інформацію скриньки.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageAccountDetail.qml" line="89"/>
        <location filename="../../qml/pages/PageAccountDetail.qml" line="89"/>
        <source>Information about the data box haven&apos;t been downloaded yet.</source>
        <translation>Інформація скриньки ще не завантажена.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageAccountDetail.qml" line="111"/>
        <location filename="../../qml/pages/PageAccountDetail.qml" line="111"/>
        <source>Data-Box Info: %1</source>
        <translation>Інформація про датову скриньку: %1</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageAccountDetail.qml" line="112"/>
        <location filename="../../qml/pages/PageAccountDetail.qml" line="112"/>
        <source>Failed to get data-box info for username &apos;%1&apos;.</source>
        <translation>Інформацію щодо скриньки для користувача &quot;%1&quot; не вдалося отримати.</translation>
    </message>
</context>
<context>
    <name>PageAccountList</name>
    <message>
        <location filename="../../qml/pages/PageAccountList.qml" line="227"/>
        <location filename="../../qml/pages/PageAccountList.qml" line="227"/>
        <source>Database problem: %1</source>
        <translation>Проблема з базою даних: %1</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageAccountList.qml" line="69"/>
        <location filename="../../qml/pages/PageAccountList.qml" line="69"/>
        <source>Synchronising...</source>
        <translation>Синхронізуємо...</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageAccountList.qml" line="69"/>
        <location filename="../../qml/pages/PageAccountList.qml" line="69"/>
        <source>Last synchronisation</source>
        <translation>Остання синхронізація</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageAccountList.qml" line="127"/>
        <location filename="../../qml/pages/PageAccountList.qml" line="127"/>
        <source>Data Boxes</source>
        <translation>Скриньки</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageAccountList.qml" line="134"/>
        <location filename="../../qml/pages/PageAccountList.qml" line="134"/>
        <source>Synchronise all data boxes.</source>
        <translation>Синхронізація всіх скриньок.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageAccountList.qml" line="213"/>
        <location filename="../../qml/pages/PageAccountList.qml" line="213"/>
        <source>%1: messages</source>
        <translation>%1: повідомлень</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageAccountList.qml" line="214"/>
        <location filename="../../qml/pages/PageAccountList.qml" line="214"/>
        <source>Failed to download list of messages for username &apos;%1&apos;.</source>
        <translation>Не вдалося завантажити список повідомлень для імені користувача &quot;%1&quot;.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageAccountList.qml" line="228"/>
        <location filename="../../qml/pages/PageAccountList.qml" line="228"/>
        <source>The message database for username &apos;%1&apos; appears to be corrupt. Synchronisation won&apos;t most likely be able to write new data and therefore is going to be aborted.</source>
        <translation>Схоже, що база даних повідомлень для імені користувача &quot;%1&quot; пошкоджена. Синхронізація, швидше за все, не зможе записати нові дані, і тому буде скасована.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageAccountList.qml" line="229"/>
        <location filename="../../qml/pages/PageAccountList.qml" line="229"/>
        <source>Do you want to restore the damaged database from a local back-up copy?</source>
        <translation>Ви хочете відновити пошкоджену базу даних з локальної резервної копії?</translation>
    </message>
</context>
<context>
    <name>PageAccountLogo</name>
    <message>
        <location filename="../../qml/pages/PageAccountLogo.qml" line="66"/>
        <location filename="../../qml/pages/PageAccountLogo.qml" line="66"/>
        <source>Choose an Image File</source>
        <translation>Виберіть файл зображення</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageAccountLogo.qml" line="105"/>
        <location filename="../../qml/pages/PageAccountLogo.qml" line="105"/>
        <source>Data-Box Logo</source>
        <translation>Логотип скриньки</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageAccountLogo.qml" line="121"/>
        <location filename="../../qml/pages/PageAccountLogo.qml" line="121"/>
        <source>Here you can change the default data-box logo to a custom image.</source>
        <translation>Тут ви можете змінити стандартний логотип скриньки на власне зображення.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageAccountLogo.qml" line="123"/>
        <location filename="../../qml/pages/PageAccountLogo.qml" line="123"/>
        <source>The set logo will be used only locally. Logos aren&apos;t shared between application installations nor the ISDS.</source>
        <translation>Встановлений логотип буде використовуватися тільки локально. Логотипи не діляться між установками додатків або ISDS.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageAccountLogo.qml" line="125"/>
        <location filename="../../qml/pages/PageAccountLogo.qml" line="125"/>
        <source>Allowed image formats are: %1</source>
        <translation>Допустимі формати зображень: %1</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageAccountLogo.qml" line="127"/>
        <location filename="../../qml/pages/PageAccountLogo.qml" line="127"/>
        <source>The recommended image size is %1x%1 pixels.</source>
        <translation>Рекомендований розмір зображення %1x%1 у пікселях.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageAccountLogo.qml" line="145"/>
        <location filename="../../qml/pages/PageAccountLogo.qml" line="145"/>
        <source>Set New Logo</source>
        <translation>Встановити новий логотип</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageAccountLogo.qml" line="150"/>
        <location filename="../../qml/pages/PageAccountLogo.qml" line="150"/>
        <source>Reset Logo</source>
        <translation>Перезавантажити логотип</translation>
    </message>
</context>
<context>
    <name>PageBackupData</name>
    <message>
        <location filename="../../qml/pages/PageBackupData.qml" line="128"/>
        <location filename="../../qml/pages/PageBackupData.qml" line="128"/>
        <source>Target</source>
        <translation>Ціль</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageBackupData.qml" line="64"/>
        <location filename="../../qml/pages/PageBackupData.qml" line="64"/>
        <source>An error occurred when backing up data to target location. Backup has been cancelled.</source>
        <translation>Під час резервного копіювання даних у цільове місце сталася помилка. Резервне копіювання скасовано.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageBackupData.qml" line="63"/>
        <location filename="../../qml/pages/PageBackupData.qml" line="63"/>
        <source>Backup Error</source>
        <translation>Помилка резервного копіювання</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageBackupData.qml" line="65"/>
        <location filename="../../qml/pages/PageBackupData.qml" line="65"/>
        <source>Application data haven&apos;t been backed up to target location.</source>
        <translation>Дані програми не були створені в цільовому місці.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageBackupData.qml" line="74"/>
        <location filename="../../qml/pages/PageBackupData.qml" line="107"/>
        <location filename="../../qml/pages/PageBackupData.qml" line="74"/>
        <location filename="../../qml/pages/PageBackupData.qml" line="107"/>
        <source>Back up Data Boxes</source>
        <translation>Резервне копіювання скриньок</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageBackupData.qml" line="74"/>
        <location filename="../../qml/pages/PageBackupData.qml" line="74"/>
        <source>Back up Data Box</source>
        <translation>Резервне копіювання скриньки</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageBackupData.qml" line="75"/>
        <location filename="../../qml/pages/PageBackupData.qml" line="75"/>
        <source>The action allows to back up data of selected data boxes into a ZIP archive.</source>
        <translation>Ця дія дозволяє створити резервну копію даних вибраних скриньок до ZIP-архіву.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageBackupData.qml" line="75"/>
        <location filename="../../qml/pages/PageBackupData.qml" line="75"/>
        <source>The action allows to back up data of the data box &apos;%1&apos; into a ZIP archive.</source>
        <translation>Ця дія дозволяє створити резервну копію даних буфера обміну &quot;%1&quot; до ZIP-архіву.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageBackupData.qml" line="77"/>
        <location filename="../../qml/pages/PageBackupData.qml" line="77"/>
        <source>Archive will be stored into the application sandbox and uploaded into the iCloud.</source>
        <translation>Архів буде збережено в робочій області додатка, а потім завантажено в iCloud.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageBackupData.qml" line="78"/>
        <location filename="../../qml/pages/PageBackupData.qml" line="78"/>
        <source>Application sandbox and iCloud.</source>
        <translation>Робоча область додатка та iCloud.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageBackupData.qml" line="143"/>
        <location filename="../../qml/pages/PageBackupData.qml" line="143"/>
        <source>Create ZIP Archive</source>
        <translation>Створіть ZIP-архів</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageBackupData.qml" line="150"/>
        <location filename="../../qml/pages/PageBackupData.qml" line="150"/>
        <source>Select all data boxes</source>
        <translation>Виберіть усі скриньки із даними</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageBackupData.qml" line="144"/>
        <location filename="../../qml/pages/PageBackupData.qml" line="144"/>
        <source>Proceed with operation.</source>
        <translation>Продовжувати операцію.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageBackupData.qml" line="177"/>
        <location filename="../../qml/pages/PageBackupData.qml" line="177"/>
        <source>Select %1.</source>
        <translation>Виберіть %1.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageBackupData.qml" line="177"/>
        <location filename="../../qml/pages/PageBackupData.qml" line="177"/>
        <source>Deselect %1.</source>
        <translation>Зніміть вибір %1.</translation>
    </message>
</context>
<context>
    <name>PageChangePassword</name>
    <message>
        <location filename="../../qml/pages/PageChangePassword.qml" line="46"/>
        <location filename="../../qml/pages/PageChangePassword.qml" line="46"/>
        <source>Enter SMS code</source>
        <translation>Введіть SMS-код</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageChangePassword.qml" line="52"/>
        <location filename="../../qml/pages/PageChangePassword.qml" line="52"/>
        <source>Enter security code</source>
        <translation>Введіть код безпеки</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageChangePassword.qml" line="101"/>
        <location filename="../../qml/pages/PageChangePassword.qml" line="101"/>
        <source>Change password</source>
        <translation>Змінити пароль</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageChangePassword.qml" line="139"/>
        <location filename="../../qml/pages/PageChangePassword.qml" line="139"/>
        <source>Log in now into data box.</source>
        <translation>Увійдіть до своєї скриньки із даними.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageChangePassword.qml" line="219"/>
        <location filename="../../qml/pages/PageChangePassword.qml" line="219"/>
        <source>Old and new password must both be filled in.</source>
        <translation>Необхідно ввести як старий, так і новий пароль.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageChangePassword.qml" line="229"/>
        <location filename="../../qml/pages/PageChangePassword.qml" line="229"/>
        <source>Wrong password format. The new password must contain at least 8 characters including at least 1 number and at least 1 upper-case letter.</source>
        <translation>Неправильний формат пароля. Новий пароль має містити щонайменше 8 символів, включаючи принаймні 1 цифру та принаймні 1 велику літеру.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageChangePassword.qml" line="235"/>
        <location filename="../../qml/pages/PageChangePassword.qml" line="235"/>
        <source>Security/SMS code must be filled in.</source>
        <translation>Необхідно ввести код безпеки/SMS.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageChangePassword.qml" line="248"/>
        <location filename="../../qml/pages/PageChangePassword.qml" line="248"/>
        <source>Password change failed.</source>
        <translation>Не вдалося змінити пароль.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageChangePassword.qml" line="153"/>
        <location filename="../../qml/pages/PageChangePassword.qml" line="153"/>
        <source>Current password</source>
        <translation>Поточний пароль</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageChangePassword.qml" line="164"/>
        <location filename="../../qml/pages/PageChangePassword.qml" line="164"/>
        <source>New password</source>
        <translation>Новий пароль</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageChangePassword.qml" line="175"/>
        <location filename="../../qml/pages/PageChangePassword.qml" line="175"/>
        <source>Confirm the new password</source>
        <translation>Підтвердьте новий пароль</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageChangePassword.qml" line="187"/>
        <location filename="../../qml/pages/PageChangePassword.qml" line="187"/>
        <source>Enter the current password to be able to send the SMS code.</source>
        <translation>Введіть поточний пароль, щоб мати можливість надіслати SMS-код.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageChangePassword.qml" line="45"/>
        <location filename="../../qml/pages/PageChangePassword.qml" line="45"/>
        <source>Enter the current password, twice the new password and the SMS code.</source>
        <translation>Введіть поточний пароль, двічі новий пароль і SMS-код.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageChangePassword.qml" line="45"/>
        <location filename="../../qml/pages/PageChangePassword.qml" line="51"/>
        <location filename="../../qml/pages/PageChangePassword.qml" line="56"/>
        <location filename="../../qml/pages/PageChangePassword.qml" line="45"/>
        <location filename="../../qml/pages/PageChangePassword.qml" line="51"/>
        <location filename="../../qml/pages/PageChangePassword.qml" line="56"/>
        <source>User is logged in to ISDS.</source>
        <translation>Користувач авторизований в системі ISDS.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageChangePassword.qml" line="51"/>
        <location filename="../../qml/pages/PageChangePassword.qml" line="51"/>
        <source>Enter the current password, twice the new password and the security code.</source>
        <translation>Введіть поточний пароль, двічі новий пароль і код безпеки.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageChangePassword.qml" line="56"/>
        <location filename="../../qml/pages/PageChangePassword.qml" line="56"/>
        <source>Enter the current password and twice the new password.</source>
        <translation>Введіть поточний пароль і двічі новий пароль.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageChangePassword.qml" line="76"/>
        <location filename="../../qml/pages/PageChangePassword.qml" line="76"/>
        <source>User must be logged in to the data box to be able to change the password.</source>
        <translation>Щоб змінити пароль, користувач має бути авторизований до скриньки із даними.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageChangePassword.qml" line="86"/>
        <location filename="../../qml/pages/PageChangePassword.qml" line="86"/>
        <source>SMS code has been sent...</source>
        <translation>SMS-код надіслано...</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageChangePassword.qml" line="91"/>
        <location filename="../../qml/pages/PageChangePassword.qml" line="91"/>
        <source>SMS code cannot be sent. Entered wrong current password or the ISDS server is out of order.</source>
        <translation>Неможливо надіслати SMS-код. Введено неправильний поточний пароль або сервер ISDS не працює.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageChangePassword.qml" line="110"/>
        <location filename="../../qml/pages/PageChangePassword.qml" line="110"/>
        <source>Hide Passwords</source>
        <translation>Приховати паролі</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageChangePassword.qml" line="110"/>
        <location filename="../../qml/pages/PageChangePassword.qml" line="110"/>
        <source>Show Passwords</source>
        <translation>Показати паролі</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageChangePassword.qml" line="131"/>
        <location filename="../../qml/pages/PageChangePassword.qml" line="131"/>
        <source>Enter the current password and the new password.</source>
        <translation>Введіть поточний пароль і новий пароль.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageChangePassword.qml" line="137"/>
        <location filename="../../qml/pages/PageChangePassword.qml" line="137"/>
        <source>Log in Now</source>
        <translation>Увійдіть зараз</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageChangePassword.qml" line="184"/>
        <location filename="../../qml/pages/PageChangePassword.qml" line="184"/>
        <source>Send SMS Code</source>
        <translation>Надіслати SMS-код</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageChangePassword.qml" line="192"/>
        <location filename="../../qml/pages/PageChangePassword.qml" line="192"/>
        <source>SMS Code: %1</source>
        <translation>SMS-код: %1</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageChangePassword.qml" line="193"/>
        <location filename="../../qml/pages/PageChangePassword.qml" line="193"/>
        <source>Data box &apos;%1&apos; requires a SMS code for password changing.</source>
        <translation>Скринька &quot;%1&quot; вимагає SMS-код для зміни пароля.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageChangePassword.qml" line="243"/>
        <location filename="../../qml/pages/PageChangePassword.qml" line="243"/>
        <source>Successful Password Change</source>
        <translation>Пароль успішно оновлено</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageChangePassword.qml" line="244"/>
        <location filename="../../qml/pages/PageChangePassword.qml" line="244"/>
        <source>Password for username &apos;%1&apos; has been changed.</source>
        <translation>Пароль для імені користувача &quot;%1&quot; змінено.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageChangePassword.qml" line="245"/>
        <location filename="../../qml/pages/PageChangePassword.qml" line="245"/>
        <source>The new password has been saved.</source>
        <translation>Новий пароль збережено.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageChangePassword.qml" line="266"/>
        <location filename="../../qml/pages/PageChangePassword.qml" line="266"/>
        <source>Data box settings</source>
        <translation>Налаштування скриньки</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageChangePassword.qml" line="194"/>
        <location filename="../../qml/pages/PageChangePassword.qml" line="194"/>
        <source>Do you want to send the SMS code now?</source>
        <translation>Бажаєте відправити SMS-код зараз?</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageChangePassword.qml" line="206"/>
        <location filename="../../qml/pages/PageChangePassword.qml" line="206"/>
        <source>Enter OTP code</source>
        <translation>Введіть OTP-код</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageChangePassword.qml" line="214"/>
        <location filename="../../qml/pages/PageChangePassword.qml" line="214"/>
        <source>Accept Changes</source>
        <translation>Прийняти зміни</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageChangePassword.qml" line="216"/>
        <location filename="../../qml/pages/PageChangePassword.qml" line="216"/>
        <source>Accept all changes.</source>
        <translation>Прийняти всі зміни.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageChangePassword.qml" line="224"/>
        <location filename="../../qml/pages/PageChangePassword.qml" line="224"/>
        <source>The new password fields don&apos;t match.</source>
        <translation>Поля нового пароля не збігаються.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageChangePassword.qml" line="262"/>
        <location filename="../../qml/pages/PageChangePassword.qml" line="262"/>
        <source>This will change the password on the ISDS server. You won&apos;t be able to log in to ISDS without the new password. If your current password has already expired then you must log into the ISDS web portal to change it.</source>
        <translation>Це змінить пароль на сервері ISDS. Ви не зможете ввійти в систему ISDS без нового пароля. Якщо термін дії вашого поточного пароля вже закінчився, ви повинні увійти на веб-портал ISDS, щоб змінити його.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageChangePassword.qml" line="266"/>
        <location filename="../../qml/pages/PageChangePassword.qml" line="266"/>
        <source>If you just want to update the password because it has been changed elsewhere then go to the </source>
        <translation>Якщо ви просто хочете оновити пароль, оскільки він був змінений в іншому місці, перейдіть до </translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageChangePassword.qml" line="270"/>
        <location filename="../../qml/pages/PageChangePassword.qml" line="270"/>
        <source>Keep a copy of your login credentials and store it in a safe place where only authorised persons have access to.</source>
        <translation>Зберігайте копію облікових даних для входу та зберігайте її в безпечному місці, до якого має доступ лише уповноважена особа.</translation>
    </message>
</context>
<context>
    <name>PageContactList</name>
    <message>
        <location filename="../../qml/pages/PageContactList.qml" line="69"/>
        <location filename="../../qml/pages/PageContactList.qml" line="69"/>
        <source>Contacts</source>
        <translation>Контакти</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageContactList.qml" line="77"/>
        <location filename="../../qml/pages/PageContactList.qml" line="77"/>
        <source>Filter data boxes.</source>
        <translation>Фільтрувати датові скриньки.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageContactList.qml" line="124"/>
        <location filename="../../qml/pages/PageContactList.qml" line="124"/>
        <source>No data box found in contacts.</source>
        <translation>У контактах не знайдено датові скриньки.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageContactList.qml" line="135"/>
        <location filename="../../qml/pages/PageContactList.qml" line="135"/>
        <source>Append selected</source>
        <translation>Підключити вибране</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageContactList.qml" line="137"/>
        <location filename="../../qml/pages/PageContactList.qml" line="137"/>
        <source>Append selected data boxes into recipient list.</source>
        <translation>Додайте вибрані скриньки до списку отримувачів.</translation>
    </message>
</context>
<context>
    <name>PageConvertDatabase</name>
    <message>
        <location filename="../../qml/pages/PageConvertDatabase.qml" line="63"/>
        <location filename="../../qml/pages/PageConvertDatabase.qml" line="63"/>
        <source>The application requires the conversion of database files into a new format. Don&apos;t close or shut down the application until the conversion process finishes. The application needs to be restarted in order to load the changes after conversion.</source>
        <translation>Програма вимагає конвертацію файлів бази даних у новий формат. Не закривайте і не вимикайте програму, доки процес конвертації не завершиться. Програму потрібно перезапустити, щоб завантажити зміни після конвертації.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageConvertDatabase.qml" line="48"/>
        <location filename="../../qml/pages/PageConvertDatabase.qml" line="48"/>
        <source>Database Conversion</source>
        <translation>Конвертація бази даних</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageConvertDatabase.qml" line="67"/>
        <location filename="../../qml/pages/PageConvertDatabase.qml" line="67"/>
        <source>Start Conversion</source>
        <translation>Почати конвертацію</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageConvertDatabase.qml" line="74"/>
        <location filename="../../qml/pages/PageConvertDatabase.qml" line="74"/>
        <source>Database conversion finished. Some message databases couldn&apos;t be converted. The application needs to be restarted in order to load the changes.</source>
        <translation>Конвертацію бази даних завершено. Деякі бази даних повідомлень не вдалося конвертувати. Щоб завантажити зміни, програму потрібно перезапустити.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageConvertDatabase.qml" line="94"/>
        <location filename="../../qml/pages/PageConvertDatabase.qml" line="94"/>
        <source>Database conversion finished. All message databases have successfully been converted. The application needs to be restarted in order to load the changes.</source>
        <translation>Конвертацію бази даних завершено. Усі бази даних повідомлень було успішно конвертовано. Щоб завантажити зміни, програму потрібно перезапустити.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageConvertDatabase.qml" line="84"/>
        <location filename="../../qml/pages/PageConvertDatabase.qml" line="84"/>
        <source>Restart</source>
        <translation>Перезапустити</translation>
    </message>
</context>
<context>
    <name>PageDataboxDetail</name>
    <message>
        <location filename="../../qml/pages/PageDataboxDetail.qml" line="43"/>
        <location filename="../../qml/pages/PageDataboxDetail.qml" line="43"/>
        <source>Data-Box Info</source>
        <translation>Інформація про датову скриньку</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageDataboxDetail.qml" line="77"/>
        <location filename="../../qml/pages/PageDataboxDetail.qml" line="77"/>
        <source>Information about the data box &apos;%1&apos; aren&apos;t available. The data box may have been temporarily disabled. It is likely that you cannot send data message to this data box.</source>
        <translation>Інформація про датову скриньку &quot;%1&quot; недоступна. Можливо, датова скринька була тимчасово вимкнена. Цілком імовірно, що ви не можете надіслати датові повідомлення даних на цю датову скриньку.</translation>
    </message>
</context>
<context>
    <name>PageDataboxSearch</name>
    <message>
        <location filename="../../qml/pages/PageDataboxSearch.qml" line="100"/>
        <location filename="../../qml/pages/PageDataboxSearch.qml" line="100"/>
        <source>Filter data boxes.</source>
        <translation>Фільтрувати датові скриньки.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageDataboxSearch.qml" line="88"/>
        <location filename="../../qml/pages/PageDataboxSearch.qml" line="88"/>
        <source>Search data boxes.</source>
        <translation>Пошук датових скриньок.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageDataboxSearch.qml" line="80"/>
        <location filename="../../qml/pages/PageDataboxSearch.qml" line="80"/>
        <source>Find Data Box</source>
        <translation>Знайти датову скриньку</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageDataboxSearch.qml" line="132"/>
        <location filename="../../qml/pages/PageDataboxSearch.qml" line="132"/>
        <source>Select type of sought data box</source>
        <translation>Виберіть тип датових скриньок для пошуку</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageDataboxSearch.qml" line="135"/>
        <location filename="../../qml/pages/PageDataboxSearch.qml" line="135"/>
        <source>All data boxes</source>
        <translation>Усі скриньки</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageDataboxSearch.qml" line="136"/>
        <location filename="../../qml/pages/PageDataboxSearch.qml" line="136"/>
        <source>OVM</source>
        <translation>OMV</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageDataboxSearch.qml" line="137"/>
        <location filename="../../qml/pages/PageDataboxSearch.qml" line="137"/>
        <source>PO</source>
        <translation>PO</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageDataboxSearch.qml" line="138"/>
        <location filename="../../qml/pages/PageDataboxSearch.qml" line="138"/>
        <source>PFO</source>
        <translation>PFO</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageDataboxSearch.qml" line="139"/>
        <location filename="../../qml/pages/PageDataboxSearch.qml" line="139"/>
        <source>FO</source>
        <translation>FO</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageDataboxSearch.qml" line="146"/>
        <location filename="../../qml/pages/PageDataboxSearch.qml" line="146"/>
        <source>Select entries to search in</source>
        <translation>Виберіть записи для пошуку</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageDataboxSearch.qml" line="149"/>
        <location filename="../../qml/pages/PageDataboxSearch.qml" line="149"/>
        <source>All fields</source>
        <translation>Усі поля</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageDataboxSearch.qml" line="150"/>
        <location filename="../../qml/pages/PageDataboxSearch.qml" line="150"/>
        <source>Address</source>
        <translation>Адреса</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageDataboxSearch.qml" line="151"/>
        <location filename="../../qml/pages/PageDataboxSearch.qml" line="151"/>
        <source>IČO</source>
        <translation>IČO</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageDataboxSearch.qml" line="152"/>
        <location filename="../../qml/pages/PageDataboxSearch.qml" line="152"/>
        <source>Data-Box ID</source>
        <translation>ID скриньки</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageDataboxSearch.qml" line="241"/>
        <location filename="../../qml/pages/PageDataboxSearch.qml" line="241"/>
        <source>Searching for data boxes ...</source>
        <translation>Пошук у скриньці із даними ...</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageDataboxSearch.qml" line="292"/>
        <location filename="../../qml/pages/PageDataboxSearch.qml" line="292"/>
        <source>Append Selected</source>
        <translation>Підключити вибране</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageDataboxSearch.qml" line="294"/>
        <location filename="../../qml/pages/PageDataboxSearch.qml" line="294"/>
        <source>Append selected data boxes into recipient list.</source>
        <translation>Додайте вибрані скриньки до списку отримувачів.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageDataboxSearch.qml" line="158"/>
        <location filename="../../qml/pages/PageDataboxSearch.qml" line="158"/>
        <source>Enter sought phrase</source>
        <translation>Введіть шукану фразу</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageDataboxSearch.qml" line="181"/>
        <location filename="../../qml/pages/PageDataboxSearch.qml" line="181"/>
        <source>Previous</source>
        <translation>Попередні</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageDataboxSearch.qml" line="213"/>
        <location filename="../../qml/pages/PageDataboxSearch.qml" line="213"/>
        <source>Next</source>
        <translation>Наступні</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageDataboxSearch.qml" line="344"/>
        <location filename="../../qml/pages/PageDataboxSearch.qml" line="344"/>
        <source>Shown</source>
        <translation>Показано</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageDataboxSearch.qml" line="344"/>
        <location filename="../../qml/pages/PageDataboxSearch.qml" line="344"/>
        <source>from</source>
        <translation>від</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageDataboxSearch.qml" line="346"/>
        <location filename="../../qml/pages/PageDataboxSearch.qml" line="346"/>
        <source>Found</source>
        <translation>Знайдено</translation>
    </message>
</context>
<context>
    <name>PageGovService</name>
    <message>
        <location filename="../../qml/pages/PageGovService.qml" line="73"/>
        <location filename="../../qml/pages/PageGovService.qml" line="73"/>
        <source>Service:</source>
        <translation>Сервіс:</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageGovService.qml" line="94"/>
        <location filename="../../qml/pages/PageGovService.qml" line="147"/>
        <location filename="../../qml/pages/PageGovService.qml" line="94"/>
        <location filename="../../qml/pages/PageGovService.qml" line="147"/>
        <source>Request:</source>
        <translation>Запит:</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageGovService.qml" line="145"/>
        <location filename="../../qml/pages/PageGovService.qml" line="145"/>
        <source>Send Request</source>
        <translation>Відправляти запит</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageGovService.qml" line="146"/>
        <location filename="../../qml/pages/PageGovService.qml" line="146"/>
        <source>Do you want to send the e-gov request?</source>
        <translation>Ви хочете надіслати електронний запит?</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageGovService.qml" line="148"/>
        <location filename="../../qml/pages/PageGovService.qml" line="148"/>
        <source>Recipient:</source>
        <translation>Отримувач:</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageGovService.qml" line="106"/>
        <location filename="../../qml/pages/PageGovService.qml" line="106"/>
        <source>Required information</source>
        <translation>Необхідна інформація</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageGovService.qml" line="89"/>
        <location filename="../../qml/pages/PageGovService.qml" line="89"/>
        <source>Data Box:</source>
        <translation>Скринька із даними:</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageGovService.qml" line="110"/>
        <location filename="../../qml/pages/PageGovService.qml" line="110"/>
        <source>Acquired from data box</source>
        <translation>Отримано з датової скриньки</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageGovService.qml" line="119"/>
        <location filename="../../qml/pages/PageGovService.qml" line="119"/>
        <source>No user data needed.</source>
        <translation>Не потрібні дані користувача.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageGovService.qml" line="144"/>
        <location filename="../../qml/pages/PageGovService.qml" line="144"/>
        <source>Send an e-gov request from data box &apos;%1&apos;.</source>
        <translation>Надішліть електронне подання зі скриньки &quot;%1&quot;.</translation>
    </message>
</context>
<context>
    <name>PageGovServiceList</name>
    <message>
        <location filename="../../qml/pages/PageGovServiceList.qml" line="60"/>
        <location filename="../../qml/pages/PageGovServiceList.qml" line="60"/>
        <source>E-Gov Services</source>
        <translation>Послуги електронного уряду</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageGovServiceList.qml" line="68"/>
        <location filename="../../qml/pages/PageGovServiceList.qml" line="68"/>
        <source>Set filter.</source>
        <translation>Встановити фільтр.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageGovServiceList.qml" line="110"/>
        <location filename="../../qml/pages/PageGovServiceList.qml" line="110"/>
        <source>No available e-government service.</source>
        <translation>Немає доступної служби електронного уряду.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageGovServiceList.qml" line="111"/>
        <location filename="../../qml/pages/PageGovServiceList.qml" line="111"/>
        <source>No e-government service found that matches filter text &apos;%1&apos;.</source>
        <translation>Не знайдено жодної служби електронного уряду, яка відповідає тексту фільтра &quot;%1&quot;.</translation>
    </message>
</context>
<context>
    <name>PageHeader</name>
    <message>
        <location filename="../../qml/components/PageHeader.qml" line="45"/>
        <location filename="../../qml/components/PageHeader.qml" line="45"/>
        <source>Back.</source>
        <translation>Назад.</translation>
    </message>
    <message>
        <location filename="../../qml/components/PageHeader.qml" line="52"/>
        <location filename="../../qml/components/PageHeader.qml" line="52"/>
        <source>Application settings.</source>
        <translation>Налаштування програми.</translation>
    </message>
</context>
<context>
    <name>PageImportMessage</name>
    <message>
        <location filename="../../qml/pages/PageImportMessage.qml" line="109"/>
        <location filename="../../qml/pages/PageImportMessage.qml" line="109"/>
        <source>ZFO Message Import</source>
        <translation>Імпорт повідомлень ZFO</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageImportMessage.qml" line="127"/>
        <location filename="../../qml/pages/PageImportMessage.qml" line="127"/>
        <source>Here you can import messages from ZFO files into the local database.</source>
        <translation>Тут ви можете імпортувати повідомлення з файлів ZFO в локальну базу даних.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageImportMessage.qml" line="132"/>
        <location filename="../../qml/pages/PageImportMessage.qml" line="132"/>
        <source>Verify messages on the ISDS server</source>
        <translation>Перевірка повідомлень на сервері ISDS</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageImportMessage.qml" line="150"/>
        <location filename="../../qml/pages/PageImportMessage.qml" line="150"/>
        <source>Imported messages are going to be sent to the ISDS server where they are going to be checked. Messages failing this test won&apos;t be imported. We don&apos;t recommend using this option when you are using a mobile or slow internet connection.</source>
        <translation>Імпортовані повідомлення будуть відправлені на сервер ISDS, де вони будуть перевірені. Повідомлення, які пройшли цей тест, не імпортуються. Ми не рекомендуємо використовувати цю опцію, якщо ви використовуєте мобільне або повільне підключення до Інтернету.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageImportMessage.qml" line="151"/>
        <location filename="../../qml/pages/PageImportMessage.qml" line="151"/>
        <source>Imported messages won&apos;t be validated on the ISDS server.</source>
        <translation>Імпортовані повідомлення не перевірятимуться на сервері ISDS.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageImportMessage.qml" line="157"/>
        <location filename="../../qml/pages/PageImportMessage.qml" line="157"/>
        <source>User must be logged in to the data box to be able to verify the messages.</source>
        <translation>Для перевірки повідомлень користувач повинен увійти до скриньки із даними.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageImportMessage.qml" line="203"/>
        <location filename="../../qml/pages/PageImportMessage.qml" line="203"/>
        <source>User is logged in to ISDS.</source>
        <translation>Користувач увійшов до системи ISDS.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageImportMessage.qml" line="170"/>
        <location filename="../../qml/pages/PageImportMessage.qml" line="170"/>
        <source>Import ZFO Files</source>
        <translation>Імпорт ZFO файлів</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageImportMessage.qml" line="52"/>
        <location filename="../../qml/pages/PageImportMessage.qml" line="52"/>
        <source>Select ZFO Files</source>
        <translation>Виберіть файли ZFO</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageImportMessage.qml" line="181"/>
        <location filename="../../qml/pages/PageImportMessage.qml" line="181"/>
        <source>Import from Directory</source>
        <translation>Імпорт з довідника</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageImportMessage.qml" line="163"/>
        <location filename="../../qml/pages/PageImportMessage.qml" line="163"/>
        <source>Log in now</source>
        <translation>Увійти зараз</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageImportMessage.qml" line="203"/>
        <location filename="../../qml/pages/PageImportMessage.qml" line="203"/>
        <source>Select ZFO files or a directory.</source>
        <translation>Виберіть файли ZFO або папку.</translation>
    </message>
</context>
<context>
    <name>PageLog</name>
    <message>
        <location filename="../../qml/pages/PageLog.qml" line="37"/>
        <location filename="../../qml/pages/PageLog.qml" line="37"/>
        <source>Log from current run:</source>
        <translation>Журнал з поточного запуску:</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageLog.qml" line="76"/>
        <location filename="../../qml/pages/PageLog.qml" line="76"/>
        <source>Choose a log file.</source>
        <translation>Виберіть файл журналу.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageLog.qml" line="78"/>
        <location filename="../../qml/pages/PageLog.qml" line="78"/>
        <source>Choose Log</source>
        <translation>Вибрати журнал</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageLog.qml" line="81"/>
        <location filename="../../qml/pages/PageLog.qml" line="81"/>
        <source>Choose a Log File</source>
        <translation>Виберіть файл журналу</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageLog.qml" line="89"/>
        <location filename="../../qml/pages/PageLog.qml" line="89"/>
        <source>Send the log file via e-mail to developers.</source>
        <translation>Надіслати лог-файл електронною поштою розробникам.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageLog.qml" line="91"/>
        <location filename="../../qml/pages/PageLog.qml" line="91"/>
        <source>Send Log</source>
        <translation>Надіслати журнал</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageLog.qml" line="93"/>
        <location filename="../../qml/pages/PageLog.qml" line="93"/>
        <source>Send Log via E-mail</source>
        <translation>Надіслати журнал електронною поштою</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageLog.qml" line="85"/>
        <location filename="../../qml/pages/PageLog.qml" line="85"/>
        <source>Log file</source>
        <translation>Файл журналу</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageLog.qml" line="48"/>
        <location filename="../../qml/pages/PageLog.qml" line="48"/>
        <source>Log Viewer</source>
        <translation>Перегляд журналу</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageLog.qml" line="94"/>
        <location filename="../../qml/pages/PageLog.qml" line="94"/>
        <source>Do you want to send the log information to developers?</source>
        <translation>Ви хочете надіслати інформацію журналу розробникам?</translation>
    </message>
</context>
<context>
    <name>PageMessageDetail</name>
    <message>
        <location filename="../../qml/pages/PageMessageDetail.qml" line="72"/>
        <location filename="../../qml/pages/PageMessageDetail.qml" line="72"/>
        <source>Hide</source>
        <translation>Приховати</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageMessageDetail.qml" line="77"/>
        <location filename="../../qml/pages/PageMessageDetail.qml" line="330"/>
        <location filename="../../qml/pages/PageMessageDetail.qml" line="77"/>
        <location filename="../../qml/pages/PageMessageDetail.qml" line="330"/>
        <source>Show more</source>
        <translation>Показати більше</translation>
    </message>
    <message numerus="yes">
        <location filename="../../qml/pages/PageMessageDetail.qml" line="89"/>
        <location filename="../../qml/pages/PageMessageDetail.qml" line="89"/>
        <source>The message will be deleted from ISDS in %n day(s).</source>
        <translation>
            <numerusform>Повідомлення буде видалено з ISDS через %n день.</numerusform>
            <numerusform>Повідомлення буде видалено з ISDS через %n дні.</numerusform>
            <numerusform>Повідомлення буде видалено з ISDS через %n днів.</numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <location filename="../../qml/pages/PageMessageDetail.qml" line="92"/>
        <location filename="../../qml/pages/PageMessageDetail.qml" line="92"/>
        <source>The message will be deleted from ISDS in %n day(s) because the long term storage is full.</source>
        <translation>
            <numerusform>Повідомлення буде видалено з ISDS через %n день, оскільки довгострокове сховище заповнено.</numerusform>
            <numerusform>Повідомлення буде видалено з ISDS через %n дні, оскільки довгострокове сховище заповнено.</numerusform>
            <numerusform>Повідомлення буде видалено з ISDS через %n дні, оскільки довгострокове сховище заповнено.</numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <location filename="../../qml/pages/PageMessageDetail.qml" line="94"/>
        <location filename="../../qml/pages/PageMessageDetail.qml" line="94"/>
        <source>The message will be moved to the long term storage in %n day(s) if the storage is not full. The message will be deleted from the ISDS server if the storage is full.</source>
        <translation>
            <numerusform>Повідомлення буде переміщено в довгострокове сховище через %n день, якщо пам’ять не заповнено. Повідомлення буде видалено з сервера ISDS, якщо сховище заповнено.</numerusform>
            <numerusform>Повідомлення буде переміщено в довгострокове сховище через %n дні, якщо пам’ять не заповнено. Повідомлення буде видалено з сервера ISDS, якщо сховище заповнено.</numerusform>
            <numerusform>Повідомлення буде переміщено в довгострокове сховище через %n днів, якщо пам’ять не заповнено. Повідомлення буде видалено з сервера ISDS, якщо сховище заповнено.</numerusform>
        </translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageMessageDetail.qml" line="98"/>
        <location filename="../../qml/pages/PageMessageDetail.qml" line="98"/>
        <source>The message has already been deleted from the ISDS.</source>
        <translation>Повідомлення видалено з ISDS.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageMessageDetail.qml" line="159"/>
        <location filename="../../qml/pages/PageMessageDetail.qml" line="159"/>
        <source>Message</source>
        <translation>Повідомлення</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageMessageDetail.qml" line="161"/>
        <location filename="../../qml/pages/PageMessageDetail.qml" line="161"/>
        <source>Acceptance Info</source>
        <translation>Інформація щодо прийняття</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageMessageDetail.qml" line="163"/>
        <location filename="../../qml/pages/PageMessageDetail.qml" line="163"/>
        <source>Unknown ZFO Content</source>
        <translation>Невідомий вміст ZFO</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageMessageDetail.qml" line="252"/>
        <location filename="../../qml/pages/PageMessageDetail.qml" line="252"/>
        <source>Show menu of available operations.</source>
        <translation>Показати меню доступних операцій.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageMessageDetail.qml" line="365"/>
        <location filename="../../qml/pages/PageMessageDetail.qml" line="365"/>
        <source>Download Message</source>
        <translation>Завантажити повідомлення</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageMessageDetail.qml" line="390"/>
        <location filename="../../qml/pages/PageMessageDetail.qml" line="390"/>
        <source>Downloading message %1 ...</source>
        <translation>Завантаження повідомлення %1 ...</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageMessageDetail.qml" line="519"/>
        <location filename="../../qml/pages/PageMessageDetail.qml" line="519"/>
        <source>Downloading message: %1</source>
        <translation>Завантаження повідомлення: %1</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageMessageDetail.qml" line="520"/>
        <location filename="../../qml/pages/PageMessageDetail.qml" line="520"/>
        <source>Failed to download complete message %1.</source>
        <translation>Не вдалося завантажити повне повідомлення %1.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageMessageDetail.qml" line="547"/>
        <location filename="../../qml/pages/PageMessageDetail.qml" line="547"/>
        <source>Reply on the message.</source>
        <translation>Відповісти на повідомлення.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageMessageDetail.qml" line="549"/>
        <location filename="../../qml/pages/PageMessageDetail.qml" line="549"/>
        <source>Reply</source>
        <translation>Відповісти</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageMessageDetail.qml" line="555"/>
        <location filename="../../qml/pages/PageMessageDetail.qml" line="555"/>
        <source>Forward message.</source>
        <translation>Переслати повідомлення.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageMessageDetail.qml" line="557"/>
        <location filename="../../qml/pages/PageMessageDetail.qml" line="557"/>
        <source>Forward</source>
        <translation>Переслати</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageMessageDetail.qml" line="562"/>
        <location filename="../../qml/pages/PageMessageDetail.qml" line="562"/>
        <source>Send attachments via e-mail.</source>
        <translation>Надіслати вкладення електронною поштою.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageMessageDetail.qml" line="564"/>
        <location filename="../../qml/pages/PageMessageDetail.qml" line="564"/>
        <source>E-mail</source>
        <translation>Е-пошта</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageMessageDetail.qml" line="577"/>
        <location filename="../../qml/pages/PageMessageDetail.qml" line="577"/>
        <source>Save attachments to the local storage.</source>
        <translation>Зберегти вкладення в локальному сховищі.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageMessageDetail.qml" line="579"/>
        <location filename="../../qml/pages/PageMessageDetail.qml" line="579"/>
        <source>Save</source>
        <translation>Зберегти</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageMessageDetail.qml" line="583"/>
        <location filename="../../qml/pages/PageMessageDetail.qml" line="583"/>
        <source>Save Attachments: %1</source>
        <translation>Зберегти вкладення: %1</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageMessageDetail.qml" line="584"/>
        <location filename="../../qml/pages/PageMessageDetail.qml" line="584"/>
        <source>You can export files into iCloud or into device local storage.</source>
        <translation>Ви можете експортувати файли в iCloud або в локальне сховище пристрою.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageMessageDetail.qml" line="585"/>
        <location filename="../../qml/pages/PageMessageDetail.qml" line="585"/>
        <source>Do you want to export files to Datovka iCloud container?</source>
        <translation>Ви хочете завантажити файли в контейнер програми в iCloud?</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageMessageDetail.qml" line="124"/>
        <location filename="../../qml/pages/PageMessageDetail.qml" line="124"/>
        <source>File doesn&apos;t contain a valid message nor does it contain valid delivery information or the file is corrupt.</source>
        <translation>Файл не містить дійсного повідомлення та не містить дійсної інформації про доручення або файл пошкоджений.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageMessageDetail.qml" line="155"/>
        <location filename="../../qml/pages/PageMessageDetail.qml" line="155"/>
        <source>Received</source>
        <translation>Отримані</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageMessageDetail.qml" line="157"/>
        <location filename="../../qml/pages/PageMessageDetail.qml" line="157"/>
        <source>Sent</source>
        <translation>Надіслані</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageMessageDetail.qml" line="352"/>
        <location filename="../../qml/pages/PageMessageDetail.qml" line="352"/>
        <source>Attachments</source>
        <translation>Вкладення</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageMessageDetail.qml" line="366"/>
        <location filename="../../qml/pages/PageMessageDetail.qml" line="366"/>
        <source>Download complete message with attachments.</source>
        <translation>Завантажити повне повідомлення із даними з вкладеннями.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageMessageDetail.qml" line="375"/>
        <location filename="../../qml/pages/PageMessageDetail.qml" line="375"/>
        <source>Download complete signed message %1 with attachments.</source>
        <translation>Завантажте повний звіт про дані %1 з вкладеннями.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageMessageDetail.qml" line="461"/>
        <location filename="../../qml/pages/PageMessageDetail.qml" line="461"/>
        <source>Open attachment &apos;%1&apos;.</source>
        <translation>Відкрити вкладений файл &quot;%1&quot;.</translation>
    </message>
</context>
<context>
    <name>PageMessageEnvelope</name>
    <message>
        <location filename="../../qml/pages/PageMessageEnvelope.qml" line="74"/>
        <location filename="../../qml/pages/PageMessageEnvelope.qml" line="74"/>
        <source>Message Envelope</source>
        <translation>Конверт повідомлення</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageMessageEnvelope.qml" line="74"/>
        <location filename="../../qml/pages/PageMessageEnvelope.qml" line="74"/>
        <source>Acceptance Info</source>
        <translation>Інформація щодо прийняття</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageMessageEnvelope.qml" line="107"/>
        <location filename="../../qml/pages/PageMessageEnvelope.qml" line="107"/>
        <source>Save envelope as PDF.</source>
        <translation>Зберегти конверт у форматі PDF.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageMessageEnvelope.qml" line="107"/>
        <location filename="../../qml/pages/PageMessageEnvelope.qml" line="107"/>
        <source>Save acceptance info as PDF.</source>
        <translation>Зберегти інформацію щодо прийняття у форматі PDF.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageMessageEnvelope.qml" line="109"/>
        <location filename="../../qml/pages/PageMessageEnvelope.qml" line="109"/>
        <source>Save PDF</source>
        <translation>Зберегти PDF</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageMessageEnvelope.qml" line="114"/>
        <location filename="../../qml/pages/PageMessageEnvelope.qml" line="114"/>
        <source>Save PDF: %1</source>
        <translation>Зберегти PDF: %1</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageMessageEnvelope.qml" line="115"/>
        <location filename="../../qml/pages/PageMessageEnvelope.qml" line="132"/>
        <location filename="../../qml/pages/PageMessageEnvelope.qml" line="115"/>
        <location filename="../../qml/pages/PageMessageEnvelope.qml" line="132"/>
        <source>You can export files into iCloud or into device local storage.</source>
        <translation>Ви можете експортувати файли в iCloud або в локальне сховище пристрою.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageMessageEnvelope.qml" line="116"/>
        <location filename="../../qml/pages/PageMessageEnvelope.qml" line="133"/>
        <location filename="../../qml/pages/PageMessageEnvelope.qml" line="116"/>
        <location filename="../../qml/pages/PageMessageEnvelope.qml" line="133"/>
        <source>Do you want to export files to Datovka iCloud container?</source>
        <translation>Ви хочете завантажити файли в контейнер програми в iCloud?</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageMessageEnvelope.qml" line="124"/>
        <location filename="../../qml/pages/PageMessageEnvelope.qml" line="124"/>
        <source>Save message as ZFO.</source>
        <translation>Зберегти повідомлення у форматі ZFO.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageMessageEnvelope.qml" line="124"/>
        <location filename="../../qml/pages/PageMessageEnvelope.qml" line="124"/>
        <source>Save acceptance info as ZFO.</source>
        <translation>Зберегти інформацію щодо прийняття у форматі ZFO.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageMessageEnvelope.qml" line="126"/>
        <location filename="../../qml/pages/PageMessageEnvelope.qml" line="126"/>
        <source>Save ZFO</source>
        <translation>Зберегти ZFO</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageMessageEnvelope.qml" line="131"/>
        <location filename="../../qml/pages/PageMessageEnvelope.qml" line="131"/>
        <source>Save ZFO: %1</source>
        <translation>Зберегти ZFO: %1</translation>
    </message>
</context>
<context>
    <name>PageMessageList</name>
    <message>
        <location filename="../../qml/pages/PageMessageList.qml" line="94"/>
        <location filename="../../qml/pages/PageMessageList.qml" line="94"/>
        <source>Received</source>
        <translation>Отримані</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageMessageList.qml" line="94"/>
        <location filename="../../qml/pages/PageMessageList.qml" line="94"/>
        <source>Sent</source>
        <translation>Надіслані</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageMessageList.qml" line="110"/>
        <location filename="../../qml/pages/PageMessageList.qml" line="110"/>
        <source>Filter messages.</source>
        <translation>Фільтрувати повідомлення.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageMessageList.qml" line="121"/>
        <location filename="../../qml/pages/PageMessageList.qml" line="222"/>
        <location filename="../../qml/pages/PageMessageList.qml" line="121"/>
        <location filename="../../qml/pages/PageMessageList.qml" line="222"/>
        <source>Synchronise received.</source>
        <translation>Синхронізація отримана.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageMessageList.qml" line="121"/>
        <location filename="../../qml/pages/PageMessageList.qml" line="222"/>
        <location filename="../../qml/pages/PageMessageList.qml" line="121"/>
        <location filename="../../qml/pages/PageMessageList.qml" line="222"/>
        <source>Synchronise sent.</source>
        <translation>Синхронізація відправлена.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageMessageList.qml" line="177"/>
        <location filename="../../qml/pages/PageMessageList.qml" line="177"/>
        <source>Delete Message: %1</source>
        <translation>Видалити повідомлення: %1</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageMessageList.qml" line="178"/>
        <location filename="../../qml/pages/PageMessageList.qml" line="178"/>
        <source>Do you want to delete the sent message &apos;%1&apos;?</source>
        <translation>Бажаєте видалити відправлене повідомлення &quot;%1&quot;?</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageMessageList.qml" line="179"/>
        <location filename="../../qml/pages/PageMessageList.qml" line="179"/>
        <source>Note: It will delete all attachments and message information from the local database.</source>
        <translation>Примітка: це видалить усі вкладення та інформацію про повідомлення з локальної бази даних.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageMessageList.qml" line="221"/>
        <location filename="../../qml/pages/PageMessageList.qml" line="221"/>
        <source>Synchronise Received</source>
        <translation>Синхронізація отримана</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageMessageList.qml" line="221"/>
        <location filename="../../qml/pages/PageMessageList.qml" line="221"/>
        <source>Synchronise Sent</source>
        <translation>Синхронізація відправлена</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageMessageList.qml" line="233"/>
        <location filename="../../qml/pages/PageMessageList.qml" line="233"/>
        <source>No %1 messages available or they haven&apos;t been downloaded yet.</source>
        <translation>Поки що немає %1 повідомлень або їх не було завантажено.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageMessageList.qml" line="233"/>
        <location filename="../../qml/pages/PageMessageList.qml" line="233"/>
        <source>received</source>
        <translation>отримані</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageMessageList.qml" line="233"/>
        <location filename="../../qml/pages/PageMessageList.qml" line="233"/>
        <source>sent</source>
        <translation>надіслані</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageMessageList.qml" line="263"/>
        <location filename="../../qml/pages/PageMessageList.qml" line="263"/>
        <source>%1: messages</source>
        <translation>%1: повідомлень</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageMessageList.qml" line="264"/>
        <location filename="../../qml/pages/PageMessageList.qml" line="264"/>
        <source>Failed to download list of messages for user name &apos;%1&apos;.</source>
        <translation>Не вдалося завантажити список повідомлень для імені користувача &quot;%1&quot;.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageMessageList.qml" line="103"/>
        <location filename="../../qml/pages/PageMessageList.qml" line="103"/>
        <source>Show menu of available operations.</source>
        <translation>Показати меню доступних операцій.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageMessageList.qml" line="244"/>
        <location filename="../../qml/pages/PageMessageList.qml" line="244"/>
        <source>No message found that matches filter text &apos;%1&apos;.</source>
        <translation>Не знайдено жодного повідомлення, яке відповідає тексту фільтра &quot;%1&quot;.</translation>
    </message>
</context>
<context>
    <name>PageMessageListSwipeView</name>
    <message>
        <location filename="../../qml/pages/PageMessageListSwipeView.qml" line="131"/>
        <location filename="../../qml/pages/PageMessageListSwipeView.qml" line="131"/>
        <source>Database problem: %1</source>
        <translation>Проблема з базою даних: %1</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageMessageListSwipeView.qml" line="132"/>
        <location filename="../../qml/pages/PageMessageListSwipeView.qml" line="132"/>
        <source>The message database for username &apos;%1&apos; appears to be corrupt. Synchronisation won&apos;t most likely be able to write new data and therefore is going to be aborted.</source>
        <translation>Схоже, що база даних повідомлень для імені користувача &quot;%1&quot; пошкоджена. Синхронізація, швидше за все, не зможе записати нові дані, і тому буде скасована.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageMessageListSwipeView.qml" line="133"/>
        <location filename="../../qml/pages/PageMessageListSwipeView.qml" line="133"/>
        <source>Do you want to restore the damaged database from a local back-up copy?</source>
        <translation>Ви хочете відновити пошкоджену базу даних з локальної резервної копії?</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageMessageListSwipeView.qml" line="152"/>
        <location filename="../../qml/pages/PageMessageListSwipeView.qml" line="152"/>
        <source>Create and send a new message from data box &apos;%1&apos;.</source>
        <translation>Створіть та надішліть нове повідомлення зі скриньки &quot;%1&quot;.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageMessageListSwipeView.qml" line="168"/>
        <location filename="../../qml/pages/PageMessageListSwipeView.qml" line="168"/>
        <source>View received messages of data box &apos;%1&apos;.</source>
        <translation>Перегляд отриманих повідомлень скриньки &quot;%1&quot;.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageMessageListSwipeView.qml" line="169"/>
        <location filename="../../qml/pages/PageMessageListSwipeView.qml" line="169"/>
        <source>Received</source>
        <translation>Отримані</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageMessageListSwipeView.qml" line="175"/>
        <location filename="../../qml/pages/PageMessageListSwipeView.qml" line="175"/>
        <source>View sent messages of data box &apos;%1&apos;.</source>
        <translation>Перегляд отриманих повідомлень скриньки &quot;%1&quot;.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageMessageListSwipeView.qml" line="176"/>
        <location filename="../../qml/pages/PageMessageListSwipeView.qml" line="176"/>
        <source>Sent</source>
        <translation>Надіслані</translation>
    </message>
</context>
<context>
    <name>PageMessageSearch</name>
    <message>
        <location filename="../../qml/pages/PageMessageSearch.qml" line="65"/>
        <location filename="../../qml/pages/PageMessageSearch.qml" line="65"/>
        <source>Search Message</source>
        <translation>Пошук повідомлення</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageMessageSearch.qml" line="73"/>
        <location filename="../../qml/pages/PageMessageSearch.qml" line="73"/>
        <source>Search messages.</source>
        <translation>Пошук повідомлень.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageMessageSearch.qml" line="90"/>
        <location filename="../../qml/pages/PageMessageSearch.qml" line="90"/>
        <source>Select type of sought messages</source>
        <translation>Виберіть тип шуканих повідомлень</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageMessageSearch.qml" line="94"/>
        <location filename="../../qml/pages/PageMessageSearch.qml" line="94"/>
        <source>All messages</source>
        <translation>Усі повідомлення</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageMessageSearch.qml" line="95"/>
        <location filename="../../qml/pages/PageMessageSearch.qml" line="95"/>
        <source>Received messages</source>
        <translation>Отримані повідомлення</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageMessageSearch.qml" line="96"/>
        <location filename="../../qml/pages/PageMessageSearch.qml" line="96"/>
        <source>Sent messages</source>
        <translation>Надіслані повідомлення</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageMessageSearch.qml" line="108"/>
        <location filename="../../qml/pages/PageMessageSearch.qml" line="108"/>
        <source>Enter phrase</source>
        <translation>Введіть фразу</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageMessageSearch.qml" line="127"/>
        <location filename="../../qml/pages/PageMessageSearch.qml" line="127"/>
        <source>Searching for messages ...</source>
        <translation>Пошук скриньки ...</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageMessageSearch.qml" line="159"/>
        <location filename="../../qml/pages/PageMessageSearch.qml" line="159"/>
        <source>No message found for given search phrase.</source>
        <translation>За вказаною пошуковою фразою повідомлення не знайдено.</translation>
    </message>
</context>
<context>
    <name>PagePrefsEditor</name>
    <message>
        <location filename="../../qml/pages/PagePrefsEditor.qml" line="80"/>
        <location filename="../../qml/pages/PagePrefsEditor.qml" line="80"/>
        <source>Enter a Value</source>
        <translation>Введіть значення</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PagePrefsEditor.qml" line="105"/>
        <location filename="../../qml/pages/PagePrefsEditor.qml" line="105"/>
        <source>Close</source>
        <translation>Закрити</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PagePrefsEditor.qml" line="111"/>
        <location filename="../../qml/pages/PagePrefsEditor.qml" line="111"/>
        <source>Reset</source>
        <translation>Перезавантажити</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PagePrefsEditor.qml" line="119"/>
        <location filename="../../qml/pages/PagePrefsEditor.qml" line="119"/>
        <source>Save</source>
        <translation>Зберегти</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PagePrefsEditor.qml" line="156"/>
        <location filename="../../qml/pages/PagePrefsEditor.qml" line="156"/>
        <source>All Preferences</source>
        <translation>Усі параметри</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PagePrefsEditor.qml" line="165"/>
        <location filename="../../qml/pages/PagePrefsEditor.qml" line="165"/>
        <source>Filter the preferences.</source>
        <translation>Фільтрувати параметри.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PagePrefsEditor.qml" line="191"/>
        <location filename="../../qml/pages/PagePrefsEditor.qml" line="191"/>
        <source>Press and hold a list element to edit its value.</source>
        <translation>Натисніть та утримуйте елемент списку, щоб змінити його значення.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PagePrefsEditor.qml" line="264"/>
        <location filename="../../qml/pages/PagePrefsEditor.qml" line="264"/>
        <source>Changing these settings directly can be harmful to the stability, security and performance of this application. You should only continue if you are sure of what you are doing.</source>
        <translation>Зміна цих параметрів безпосередньо може бути шкідливою для стабільності, безпеки та продуктивності.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PagePrefsEditor.qml" line="269"/>
        <location filename="../../qml/pages/PagePrefsEditor.qml" line="269"/>
        <source>I accept the risk.</source>
        <translation>Приймаю ризик.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PagePrefsEditor.qml" line="270"/>
        <location filename="../../qml/pages/PagePrefsEditor.qml" line="270"/>
        <source>Show all preferences.</source>
        <translation>Показати всі параметри.</translation>
    </message>
</context>
<context>
    <name>PageRecordsManagementUpload</name>
    <message>
        <location filename="../../qml/pages/PageRecordsManagementUpload.qml" line="105"/>
        <location filename="../../qml/pages/PageRecordsManagementUpload.qml" line="105"/>
        <source>Upload Message %1</source>
        <translation>Завантажити повідомлення %1</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageRecordsManagementUpload.qml" line="113"/>
        <location filename="../../qml/pages/PageRecordsManagementUpload.qml" line="113"/>
        <source>Filter upload hierarchy.</source>
        <translation>Фільтрувати ієрархію завантажень.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageRecordsManagementUpload.qml" line="138"/>
        <location filename="../../qml/pages/PageRecordsManagementUpload.qml" line="138"/>
        <source>The message can be uploaded into selected locations in the records management hierarchy.</source>
        <translation>Повідомлення можна завантажити у вибрані місця в ієрархії керування записами.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageRecordsManagementUpload.qml" line="146"/>
        <location filename="../../qml/pages/PageRecordsManagementUpload.qml" line="146"/>
        <source>Update upload hierarchy</source>
        <translation>Оновити ієрархію</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageRecordsManagementUpload.qml" line="148"/>
        <location filename="../../qml/pages/PageRecordsManagementUpload.qml" line="148"/>
        <source>Update</source>
        <translation>Оновлення</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageRecordsManagementUpload.qml" line="156"/>
        <location filename="../../qml/pages/PageRecordsManagementUpload.qml" line="156"/>
        <source>Upload message</source>
        <translation>Завантажити повідомлення</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageRecordsManagementUpload.qml" line="158"/>
        <location filename="../../qml/pages/PageRecordsManagementUpload.qml" line="158"/>
        <source>Upload</source>
        <translation>Завантажити повідомлення</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageRecordsManagementUpload.qml" line="188"/>
        <location filename="../../qml/pages/PageRecordsManagementUpload.qml" line="188"/>
        <source>Up</source>
        <translation>Вгору</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageRecordsManagementUpload.qml" line="209"/>
        <location filename="../../qml/pages/PageRecordsManagementUpload.qml" line="209"/>
        <source>Upload hierarchy has not been downloaded yet.</source>
        <translation>Ієрархію завантажень ще не завантажено.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageRecordsManagementUpload.qml" line="210"/>
        <location filename="../../qml/pages/PageRecordsManagementUpload.qml" line="210"/>
        <source>No hierarchy entry found that matches filter text &apos;%1&apos;.</source>
        <translation>Не знайдено запису в ієрархії, який відповідає тексту фільтра &quot;%1&quot;.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageRecordsManagementUpload.qml" line="263"/>
        <location filename="../../qml/pages/PageRecordsManagementUpload.qml" line="263"/>
        <source>View content of %1.</source>
        <translation>Переглянути вміст %1.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageRecordsManagementUpload.qml" line="265"/>
        <location filename="../../qml/pages/PageRecordsManagementUpload.qml" line="265"/>
        <source>Select %1.</source>
        <translation>Виберіть %1.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageRecordsManagementUpload.qml" line="265"/>
        <location filename="../../qml/pages/PageRecordsManagementUpload.qml" line="265"/>
        <source>Deselect %1.</source>
        <translation>Зніміть вибір %1.</translation>
    </message>
</context>
<context>
    <name>PageRepairDatabase</name>
    <message>
        <location filename="../../qml/pages/PageRepairDatabase.qml" line="57"/>
        <location filename="../../qml/pages/PageRepairDatabase.qml" line="57"/>
        <source>Restoration Finished</source>
        <translation>Відновлення закінчено</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageRepairDatabase.qml" line="59"/>
        <location filename="../../qml/pages/PageRepairDatabase.qml" line="59"/>
        <source>Synchronise the data box to download missing data.</source>
        <translation>Синхронізуйте скриньки, щоб завантажити відсутні дані.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageRepairDatabase.qml" line="61"/>
        <location filename="../../qml/pages/PageRepairDatabase.qml" line="61"/>
        <source>Restoration Error</source>
        <translation>Помилка відновлення</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageRepairDatabase.qml" line="62"/>
        <location filename="../../qml/pages/PageRepairDatabase.qml" line="62"/>
        <source>Restoration of the message database failed. The log file contains the description of occurred errors.</source>
        <translation>Не вдалося відновити базу даних повідомлень. Файл журналу містить опис помилок, що виникли.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageRepairDatabase.qml" line="63"/>
        <location filename="../../qml/pages/PageRepairDatabase.qml" line="105"/>
        <location filename="../../qml/pages/PageRepairDatabase.qml" line="63"/>
        <location filename="../../qml/pages/PageRepairDatabase.qml" line="105"/>
        <source>Do you want to delete the corrupted message database and create a new empty one? Downloaded message data will be deleted.</source>
        <translation>Ви хочете видалити пошкоджену базу даних повідомлень і створити нову порожню? Завантажені дані повідомлення буде видалено.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageRepairDatabase.qml" line="70"/>
        <location filename="../../qml/pages/PageRepairDatabase.qml" line="70"/>
        <source>Repair Database</source>
        <translation>Ремонт бази даних</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageRepairDatabase.qml" line="88"/>
        <location filename="../../qml/pages/PageRepairDatabase.qml" line="88"/>
        <source>The action checks the message database integrity and restores damaged files from a local back-up copy.</source>
        <translation>Дія перевіряє цілісність бази даних повідомлень і відновлює пошкоджені файли з локальної резервної копії.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageRepairDatabase.qml" line="91"/>
        <location filename="../../qml/pages/PageRepairDatabase.qml" line="91"/>
        <source>Run Integrity Test</source>
        <translation>Запустити перевірку цілісності</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageRepairDatabase.qml" line="102"/>
        <location filename="../../qml/pages/PageRepairDatabase.qml" line="102"/>
        <source>But there isn&apos;t any local back-up file which can be used to repair the damaged one.</source>
        <translation>Але немає жодного локального файлу резервної копії, який можна було б використати для відновлення пошкодженого.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageRepairDatabase.qml" line="103"/>
        <location filename="../../qml/pages/PageRepairDatabase.qml" line="103"/>
        <source>Database Problem</source>
        <translation>Проблема з базою даних</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageRepairDatabase.qml" line="104"/>
        <location filename="../../qml/pages/PageRepairDatabase.qml" line="104"/>
        <source>The message database file is damaged but there isn&apos;t any local back-up file which can be used to repair the damaged one.</source>
        <translation>Файл бази даних повідомлень пошкоджено, але немає локального файлу резервної копії, який можна було б використати для відновлення пошкодженого.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageRepairDatabase.qml" line="109"/>
        <location filename="../../qml/pages/PageRepairDatabase.qml" line="109"/>
        <source>Select a local back-up database file witch will be used to repair the damaged one.</source>
        <translation>Виберіть файл локальної резервної бази даних, який буде використовуватися для відновлення пошкодженого.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageRepairDatabase.qml" line="155"/>
        <location filename="../../qml/pages/PageRepairDatabase.qml" line="155"/>
        <source>Database Restoration</source>
        <translation>Відновлення бази даних</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageRepairDatabase.qml" line="156"/>
        <location filename="../../qml/pages/PageRepairDatabase.qml" line="156"/>
        <source>The operation restores the currently used message database from the back-up file &apos;%1&apos; created at %2.</source>
        <translation>Операція відновлює поточну базу даних повідомлень із файлу резервної копії &apos;%1&apos;, створеного на %2.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageRepairDatabase.qml" line="157"/>
        <location filename="../../qml/pages/PageRepairDatabase.qml" line="157"/>
        <source>Restore the message database from the selected the back-up file?</source>
        <translation>Відновити базу даних повідомлень із вибраного файлу резервної копії?</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageRepairDatabase.qml" line="101"/>
        <location filename="../../qml/pages/PageRepairDatabase.qml" line="108"/>
        <location filename="../../qml/pages/PageRepairDatabase.qml" line="101"/>
        <location filename="../../qml/pages/PageRepairDatabase.qml" line="108"/>
        <source>The message database file is damaged.</source>
        <translation>Файл бази даних повідомлень пошкоджено.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageRepairDatabase.qml" line="115"/>
        <location filename="../../qml/pages/PageRepairDatabase.qml" line="115"/>
        <source>The message database file is valid and OK.</source>
        <translation>Файл бази даних повідомлень дійсний і в порядку.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageRepairDatabase.qml" line="56"/>
        <location filename="../../qml/pages/PageRepairDatabase.qml" line="58"/>
        <location filename="../../qml/pages/PageRepairDatabase.qml" line="56"/>
        <location filename="../../qml/pages/PageRepairDatabase.qml" line="58"/>
        <source>The message database has successfully been restored.</source>
        <translation>Базу даних повідомлень успішно відновлено.</translation>
    </message>
</context>
<context>
    <name>PageRestoreData</name>
    <message>
        <location filename="../../qml/pages/PageRestoreData.qml" line="151"/>
        <location filename="../../qml/pages/PageRestoreData.qml" line="151"/>
        <source>An error during data restoration has occurred.</source>
        <translation>Під час відновлення даних сталася помилка.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageRestoreData.qml" line="152"/>
        <location filename="../../qml/pages/PageRestoreData.qml" line="152"/>
        <source>Application data restoration has been cancelled.</source>
        <translation>Відновлення даних програми було скасовано.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageRestoreData.qml" line="157"/>
        <location filename="../../qml/pages/PageRestoreData.qml" line="157"/>
        <source>The application will restart in order to load the imported files.</source>
        <translation>Програма перезапуститься, щоб завантажити імпортовані файли.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageRestoreData.qml" line="208"/>
        <location filename="../../qml/pages/PageRestoreData.qml" line="208"/>
        <source>Proceed with restoration.</source>
        <translation>Приступити до відновлення.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageRestoreData.qml" line="150"/>
        <location filename="../../qml/pages/PageRestoreData.qml" line="150"/>
        <source>Restoration Error</source>
        <translation>Помилка відновлення</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageRestoreData.qml" line="155"/>
        <location filename="../../qml/pages/PageRestoreData.qml" line="155"/>
        <source>Restoration Finished</source>
        <translation>Відновлення закінчено</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageRestoreData.qml" line="156"/>
        <location filename="../../qml/pages/PageRestoreData.qml" line="156"/>
        <source>All selected data box data have successfully been restored.</source>
        <translation>Дані вибраних скриньок успішно відновлено.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageRestoreData.qml" line="165"/>
        <location filename="../../qml/pages/PageRestoreData.qml" line="206"/>
        <location filename="../../qml/pages/PageRestoreData.qml" line="165"/>
        <location filename="../../qml/pages/PageRestoreData.qml" line="206"/>
        <source>Restore Data</source>
        <translation>Відновити дані</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageRestoreData.qml" line="182"/>
        <location filename="../../qml/pages/PageRestoreData.qml" line="182"/>
        <source>The action allows to restore data from a back-up or transfer ZIP archive.</source>
        <translation>Дія дозволяє відновити дані з резервної копії або передати ZIP-архів.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageRestoreData.qml" line="186"/>
        <location filename="../../qml/pages/PageRestoreData.qml" line="186"/>
        <source>Choose ZIP Archive</source>
        <translation>Виберіть ZIP-архів</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageRestoreData.qml" line="211"/>
        <location filename="../../qml/pages/PageRestoreData.qml" line="215"/>
        <location filename="../../qml/pages/PageRestoreData.qml" line="211"/>
        <location filename="../../qml/pages/PageRestoreData.qml" line="215"/>
        <source>Proceed?</source>
        <translation>Продовжити?</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageRestoreData.qml" line="212"/>
        <location filename="../../qml/pages/PageRestoreData.qml" line="212"/>
        <source>The action will continue with the restoration of the complete application data. Current application data will be completely rewritten.</source>
        <translation>Дія продовжиться відновленням повних даних програми. Поточні дані програми будуть повністю переписані.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageRestoreData.qml" line="213"/>
        <location filename="../../qml/pages/PageRestoreData.qml" line="213"/>
        <source>Do you wish to restore the application data from the transfer?</source>
        <translation>Бажаєте відновити дані програми?</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageRestoreData.qml" line="216"/>
        <location filename="../../qml/pages/PageRestoreData.qml" line="216"/>
        <source>The action will continue with the restoration of the selected data boxes. Current data of the selected data boxes will be completely rewritten.</source>
        <translation>Дію буде продовжено з оновленням вибраних скриньок. Поточні дані вибраних скриньок буде повністю замінено.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageRestoreData.qml" line="217"/>
        <location filename="../../qml/pages/PageRestoreData.qml" line="217"/>
        <source>Do you wish to restore the selected data boxes from the back-up?</source>
        <translation>Ви хочете відновити вибрані скриньки з резервної копії?</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageRestoreData.qml" line="224"/>
        <location filename="../../qml/pages/PageRestoreData.qml" line="224"/>
        <source>Select all data boxes</source>
        <translation>Виберіть усі скриньки</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageRestoreData.qml" line="91"/>
        <location filename="../../qml/pages/PageRestoreData.qml" line="91"/>
        <source>Select ZIP</source>
        <translation>Виберіть ZIP</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageRestoreData.qml" line="251"/>
        <location filename="../../qml/pages/PageRestoreData.qml" line="251"/>
        <source>Select %1.</source>
        <translation>Вибрати %1.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageRestoreData.qml" line="251"/>
        <location filename="../../qml/pages/PageRestoreData.qml" line="251"/>
        <source>Deselect %1.</source>
        <translation>Зняти вибір %1.</translation>
    </message>
</context>
<context>
    <name>PageSendMessage</name>
    <message>
        <location filename="../../qml/pages/PageSendMessage.qml" line="132"/>
        <location filename="../../qml/pages/PageSendMessage.qml" line="157"/>
        <location filename="../../qml/pages/PageSendMessage.qml" line="169"/>
        <location filename="../../qml/pages/PageSendMessage.qml" line="634"/>
        <location filename="../../qml/pages/PageSendMessage.qml" line="132"/>
        <location filename="../../qml/pages/PageSendMessage.qml" line="157"/>
        <location filename="../../qml/pages/PageSendMessage.qml" line="169"/>
        <location filename="../../qml/pages/PageSendMessage.qml" line="634"/>
        <source>Total size of attachments is %1 B.</source>
        <translation>Загальний розмір вкладень становить %1 B.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSendMessage.qml" line="120"/>
        <location filename="../../qml/pages/PageSendMessage.qml" line="145"/>
        <location filename="../../qml/pages/PageSendMessage.qml" line="120"/>
        <location filename="../../qml/pages/PageSendMessage.qml" line="145"/>
        <source>Total size of attachments exceeds %1 MB.</source>
        <translation>Загальний розмір вкладених файлів перевищує %1 МБ.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSendMessage.qml" line="130"/>
        <location filename="../../qml/pages/PageSendMessage.qml" line="155"/>
        <location filename="../../qml/pages/PageSendMessage.qml" line="130"/>
        <location filename="../../qml/pages/PageSendMessage.qml" line="155"/>
        <source>Total size of attachments is ~%1 kB.</source>
        <translation>Загальний розмір вкладень становить ~%1 КБ.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSendMessage.qml" line="207"/>
        <location filename="../../qml/pages/PageSendMessage.qml" line="207"/>
        <source>Reply %1</source>
        <translation>Відповідь %1</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSendMessage.qml" line="226"/>
        <location filename="../../qml/pages/PageSendMessage.qml" line="226"/>
        <source>Forward %1</source>
        <translation>Переслати %1</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSendMessage.qml" line="249"/>
        <location filename="../../qml/pages/PageSendMessage.qml" line="249"/>
        <source>Forward ZFO %1</source>
        <translation>Переслати ZFO %1</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSendMessage.qml" line="310"/>
        <location filename="../../qml/pages/PageSendMessage.qml" line="310"/>
        <source>New message</source>
        <translation>Нове повідомлення</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSendMessage.qml" line="1021"/>
        <location filename="../../qml/pages/PageSendMessage.qml" line="1021"/>
        <source>message subject</source>
        <translation>тема повідомлення</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSendMessage.qml" line="1024"/>
        <location filename="../../qml/pages/PageSendMessage.qml" line="1024"/>
        <source>recipient data box</source>
        <translation>датова скринька одержувача</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSendMessage.qml" line="1030"/>
        <location filename="../../qml/pages/PageSendMessage.qml" line="1030"/>
        <source>Please fill in the respective fields before attempting to send the message.</source>
        <translation>Будь ласка, заповніть відповідні поля, перш ніж спробувати надіслати повідомлення.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSendMessage.qml" line="1029"/>
        <location filename="../../qml/pages/PageSendMessage.qml" line="1029"/>
        <source>Send Message Error</source>
        <translation>Помилка надсилання повідомлення</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSendMessage.qml" line="448"/>
        <location filename="../../qml/pages/PageSendMessage.qml" line="448"/>
        <source>Error during message creation.</source>
        <translation>Помилка під час створення повідомлення.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSendMessage.qml" line="1106"/>
        <location filename="../../qml/pages/PageSendMessage.qml" line="1106"/>
        <source>Recipient list page.</source>
        <translation>Перелік одержувачів.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSendMessage.qml" line="529"/>
        <location filename="../../qml/pages/PageSendMessage.qml" line="1107"/>
        <location filename="../../qml/pages/PageSendMessage.qml" line="529"/>
        <location filename="../../qml/pages/PageSendMessage.qml" line="1107"/>
        <source>Recipients</source>
        <translation>Отримувачі</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSendMessage.qml" line="306"/>
        <location filename="../../qml/pages/PageSendMessage.qml" line="306"/>
        <source>Data Box: %1 (%2)</source>
        <translation>Датова скринька: %1 (%2)</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSendMessage.qml" line="608"/>
        <location filename="../../qml/pages/PageSendMessage.qml" line="608"/>
        <source>Select a file from the local storage or enter short text message and append it into attachments.</source>
        <translation>Виберіть файл з локального сховища або введіть коротке текстове повідомлення і додайте його до списку вкладень.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSendMessage.qml" line="1112"/>
        <location filename="../../qml/pages/PageSendMessage.qml" line="1112"/>
        <source>Attachment list page.</source>
        <translation>Список вкладень.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSendMessage.qml" line="606"/>
        <location filename="../../qml/pages/PageSendMessage.qml" line="1113"/>
        <location filename="../../qml/pages/PageSendMessage.qml" line="606"/>
        <location filename="../../qml/pages/PageSendMessage.qml" line="1113"/>
        <source>Attachments</source>
        <translation>Вкладення</translation>
    </message>
    <message numerus="yes">
        <location filename="../../qml/pages/PageSendMessage.qml" line="115"/>
        <location filename="../../qml/pages/PageSendMessage.qml" line="140"/>
        <location filename="../../qml/pages/PageSendMessage.qml" line="115"/>
        <location filename="../../qml/pages/PageSendMessage.qml" line="140"/>
        <source>The permitted number of %n attachment(s) has been exceeded.</source>
        <translation>
            <numerusform>Перевищено допустиму кількість %n вкладення.</numerusform>
            <numerusform>Перевищено допустиму кількість %n вкладень.</numerusform>
            <numerusform>Перевищено допустиму кількість %n вкладень.</numerusform>
        </translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSendMessage.qml" line="125"/>
        <location filename="../../qml/pages/PageSendMessage.qml" line="125"/>
        <source>Total size of attachments exceeds %1 MB. Most of the data boxes cannot receive messages larger than %1 MB. However, some OVM data boxes can receive messages up to %2 MB.</source>
        <translation>Загальний розмір вкладень перевищує %1 МБ. Більшість скриньок із даними не можуть приймати повідомлення, розмір яких перевищує %1 МБ. Однак деякі скриньки типу OVM можуть приймати повідомлення розміром до %2 МБ.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSendMessage.qml" line="150"/>
        <location filename="../../qml/pages/PageSendMessage.qml" line="150"/>
        <source>Total size of attachments is ~%1 MB and exceeds the limit of %2 MB for standard messages. Message will be sent as a high-volume message (VoDZ).</source>
        <translation>Загальний розмір вкладень становить ~%1 МБ та перевищив ліміт %2 МБ для стандартного повідомлення. Повідомлення буде надіслано як повідомлення великого обсягу (VoDZ).</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSendMessage.qml" line="266"/>
        <location filename="../../qml/pages/PageSendMessage.qml" line="266"/>
        <source>ZFO file of message %1 is not available. Download the complete message before forwarding.</source>
        <translation>Файл ZFO повідомлення %1 недоступний. Завантажте повне повідомлення, щоб отримати його перед пересиланням.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSendMessage.qml" line="379"/>
        <location filename="../../qml/pages/PageSendMessage.qml" line="379"/>
        <source>Sending message</source>
        <translation>надсилання повідомлення</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSendMessage.qml" line="383"/>
        <location filename="../../qml/pages/PageSendMessage.qml" line="383"/>
        <source>The action may take several seconds.</source>
        <translation>Дія може тривати кілька секунд.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSendMessage.qml" line="435"/>
        <location filename="../../qml/pages/PageSendMessage.qml" line="435"/>
        <source>Create Message</source>
        <translation>Створити повідомлення</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSendMessage.qml" line="465"/>
        <location filename="../../qml/pages/PageSendMessage.qml" line="465"/>
        <source>Sender</source>
        <translation>Відправник</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSendMessage.qml" line="481"/>
        <location filename="../../qml/pages/PageSendMessage.qml" line="481"/>
        <source>Message subject</source>
        <translation>тема повідомлення</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSendMessage.qml" line="485"/>
        <location filename="../../qml/pages/PageSendMessage.qml" line="485"/>
        <source>The subject must be filled in.</source>
        <translation>Тема листа має бути заповнена.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSendMessage.qml" line="532"/>
        <location filename="../../qml/pages/PageSendMessage.qml" line="532"/>
        <source>At least one recipient must be appended.</source>
        <translation>До повідомлення повинен бути додан принаймні один одержувач.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSendMessage.qml" line="553"/>
        <location filename="../../qml/pages/PageSendMessage.qml" line="553"/>
        <source>Find recipient</source>
        <translation>Знайти одержувача</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSendMessage.qml" line="609"/>
        <location filename="../../qml/pages/PageSendMessage.qml" line="609"/>
        <source>At least one attachment must be appended.</source>
        <translation>Повідомлення має містити хоча б одне вкладення.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSendMessage.qml" line="619"/>
        <location filename="../../qml/pages/PageSendMessage.qml" line="619"/>
        <source>Add File</source>
        <translation>Додати файл</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSendMessage.qml" line="280"/>
        <location filename="../../qml/pages/PageSendMessage.qml" line="280"/>
        <source>Choose a File</source>
        <translation>Виберіть файл</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSendMessage.qml" line="624"/>
        <location filename="../../qml/pages/PageSendMessage.qml" line="624"/>
        <source>Add Text</source>
        <translation>Додати текст</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSendMessage.qml" line="1010"/>
        <location filename="../../qml/pages/PageSendMessage.qml" line="1010"/>
        <source>Send a message from data box &apos;%1&apos;.</source>
        <translation>Надіслати повідомлення зі скриньки &quot;%1&quot;.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSendMessage.qml" line="1118"/>
        <location filename="../../qml/pages/PageSendMessage.qml" line="1118"/>
        <source>Message additional fields page.</source>
        <translation>Додаткова інформація про звіт.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSendMessage.qml" line="1119"/>
        <location filename="../../qml/pages/PageSendMessage.qml" line="1119"/>
        <source>Additional</source>
        <translation>Додаткові</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSendMessage.qml" line="1124"/>
        <location filename="../../qml/pages/PageSendMessage.qml" line="1124"/>
        <source>Message author info page.</source>
        <translation>Інформація про автора.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSendMessage.qml" line="1130"/>
        <location filename="../../qml/pages/PageSendMessage.qml" line="1130"/>
        <source>Short text editor page.</source>
        <translation>Коротке текстове повідомлення.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSendMessage.qml" line="1131"/>
        <location filename="../../qml/pages/PageSendMessage.qml" line="1131"/>
        <source>Text</source>
        <translation>Текст</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSendMessage.qml" line="1125"/>
        <location filename="../../qml/pages/PageSendMessage.qml" line="1125"/>
        <source>Author Info</source>
        <translation>Відправник</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSendMessage.qml" line="1101"/>
        <location filename="../../qml/pages/PageSendMessage.qml" line="1101"/>
        <source>Subject</source>
        <translation>Тема</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSendMessage.qml" line="494"/>
        <location filename="../../qml/pages/PageSendMessage.qml" line="494"/>
        <source>Include sender identification</source>
        <translation>Добавити ідентифікацію відправника</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSendMessage.qml" line="500"/>
        <location filename="../../qml/pages/PageSendMessage.qml" line="500"/>
        <source>Personal delivery</source>
        <translation>Особиста доставка</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSendMessage.qml" line="507"/>
        <location filename="../../qml/pages/PageSendMessage.qml" line="507"/>
        <source>Allow acceptance through fiction</source>
        <translation>Дозвольте прийняття через фікцію</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSendMessage.qml" line="513"/>
        <location filename="../../qml/pages/PageSendMessage.qml" line="513"/>
        <source>Immediately download sent message content</source>
        <translation>Одразу ж завантажити вміст надісланого повідомлення</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSendMessage.qml" line="483"/>
        <location filename="../../qml/pages/PageSendMessage.qml" line="483"/>
        <source>Enter message subject. Mandatory</source>
        <translation>Введіть тему. Обов’язковий</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSendMessage.qml" line="484"/>
        <location filename="../../qml/pages/PageSendMessage.qml" line="484"/>
        <source>The message must contain the subject, at least one recipient and attachment.</source>
        <translation>Повідомлення повинне містити тему, принаймні одного одержувача та одне вкладення.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSendMessage.qml" line="531"/>
        <location filename="../../qml/pages/PageSendMessage.qml" line="531"/>
        <source>Select a recipient from local contacts or find a recipient on the ISDS server and append it into recipient list.</source>
        <translation>Виберіть одержувача зі списку контактів або знайдіть його на сервері ISDS, а потім додайте його до списку одержувачів.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSendMessage.qml" line="543"/>
        <location filename="../../qml/pages/PageSendMessage.qml" line="543"/>
        <source>Contacts</source>
        <translation>Контакти</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSendMessage.qml" line="581"/>
        <location filename="../../qml/pages/PageSendMessage.qml" line="581"/>
        <source>Mandatory for initiatory message</source>
        <translation>Обов&apos;язкове для початкового повідомлення</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSendMessage.qml" line="589"/>
        <location filename="../../qml/pages/PageSendMessage.qml" line="589"/>
        <source>Mandatory for reply message</source>
        <translation>Обов&apos;язкове для відповіді</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSendMessage.qml" line="678"/>
        <location filename="../../qml/pages/PageSendMessage.qml" line="678"/>
        <source>Local database</source>
        <translation>Локальна база даних</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSendMessage.qml" line="728"/>
        <location filename="../../qml/pages/PageSendMessage.qml" line="728"/>
        <source>Open file &apos;%1&apos;.</source>
        <translation>Відкрити файл &quot;%1&quot;.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSendMessage.qml" line="753"/>
        <location filename="../../qml/pages/PageSendMessage.qml" line="753"/>
        <source>Remove file &apos;%1&apos; from list.</source>
        <translation>Видалити файл &quot;%1&quot; зі списку.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSendMessage.qml" line="788"/>
        <location filename="../../qml/pages/PageSendMessage.qml" line="788"/>
        <source>Mandate</source>
        <translation>Посвідчення</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSendMessage.qml" line="824"/>
        <location filename="../../qml/pages/PageSendMessage.qml" line="824"/>
        <source>par.</source>
        <translation>пар.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSendMessage.qml" line="834"/>
        <location filename="../../qml/pages/PageSendMessage.qml" line="834"/>
        <source>let.</source>
        <translation>бук.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSendMessage.qml" line="847"/>
        <location filename="../../qml/pages/PageSendMessage.qml" line="847"/>
        <source>Our reference number</source>
        <translation>Наш довідковий номер</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSendMessage.qml" line="848"/>
        <location filename="../../qml/pages/PageSendMessage.qml" line="848"/>
        <source>Enter our reference number</source>
        <translation>Введіть номер довідки</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSendMessage.qml" line="855"/>
        <location filename="../../qml/pages/PageSendMessage.qml" line="855"/>
        <source>Our file mark</source>
        <translation>Позначка нашого файлу</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSendMessage.qml" line="856"/>
        <location filename="../../qml/pages/PageSendMessage.qml" line="856"/>
        <source>Enter our file mark</source>
        <translation>Введіть позначку нашого файлу</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSendMessage.qml" line="863"/>
        <location filename="../../qml/pages/PageSendMessage.qml" line="863"/>
        <source>Your reference number</source>
        <translation>Ваш довідковий номер</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSendMessage.qml" line="864"/>
        <location filename="../../qml/pages/PageSendMessage.qml" line="864"/>
        <source>Enter your reference number</source>
        <translation>Введіть свій довідковий номер</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSendMessage.qml" line="871"/>
        <location filename="../../qml/pages/PageSendMessage.qml" line="871"/>
        <source>Your file mark</source>
        <translation>Позначка вашого файлу</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSendMessage.qml" line="872"/>
        <location filename="../../qml/pages/PageSendMessage.qml" line="872"/>
        <source>Enter your file mark</source>
        <translation>Введіть позначку вашого файлу</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSendMessage.qml" line="879"/>
        <location filename="../../qml/pages/PageSendMessage.qml" line="879"/>
        <source>To hands</source>
        <translation>До рук</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSendMessage.qml" line="880"/>
        <location filename="../../qml/pages/PageSendMessage.qml" line="880"/>
        <source>Enter name</source>
        <translation>Введіть ім&apos;я</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSendMessage.qml" line="900"/>
        <location filename="../../qml/pages/PageSendMessage.qml" line="900"/>
        <source>Select author identification data to be provided to the recipient.</source>
        <translation>Виберіть ідентифікаційні дані відправника, які будуть передані одержувачу.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSendMessage.qml" line="905"/>
        <location filename="../../qml/pages/PageSendMessage.qml" line="905"/>
        <source>full name</source>
        <translation>повне ім&apos;я</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSendMessage.qml" line="911"/>
        <location filename="../../qml/pages/PageSendMessage.qml" line="911"/>
        <source>date of birth</source>
        <translation>дата народження</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSendMessage.qml" line="916"/>
        <location filename="../../qml/pages/PageSendMessage.qml" line="916"/>
        <source>city of birth</source>
        <translation>місто народження</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSendMessage.qml" line="921"/>
        <location filename="../../qml/pages/PageSendMessage.qml" line="921"/>
        <source>county of birth</source>
        <translation>область народження</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSendMessage.qml" line="926"/>
        <location filename="../../qml/pages/PageSendMessage.qml" line="926"/>
        <source>RUIAN address code</source>
        <translation>код адреси RUIAN</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSendMessage.qml" line="931"/>
        <location filename="../../qml/pages/PageSendMessage.qml" line="931"/>
        <source>full address</source>
        <translation>повна адреса</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSendMessage.qml" line="936"/>
        <location filename="../../qml/pages/PageSendMessage.qml" line="936"/>
        <source>ROB identification flag</source>
        <translation>ідентифікаційна ознака ROB</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSendMessage.qml" line="955"/>
        <location filename="../../qml/pages/PageSendMessage.qml" line="955"/>
        <source>Here you can create a short text message and add it to attachments as a PDF file.</source>
        <translation>Тут ви можете створити коротке текстове повідомлення та додати його до вкладень у вигляді файлу PDF.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSendMessage.qml" line="956"/>
        <location filename="../../qml/pages/PageSendMessage.qml" line="956"/>
        <source>Add the message to attachments after you have finished editing the text.</source>
        <translation>Після завершення редагування тексту додайте повідомлення до вкладень.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSendMessage.qml" line="1019"/>
        <location filename="../../qml/pages/PageSendMessage.qml" line="1019"/>
        <source>The following fields must be filled in:</source>
        <translation>Наступні пункти повинні бути заповнені:</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSendMessage.qml" line="1027"/>
        <location filename="../../qml/pages/PageSendMessage.qml" line="1027"/>
        <source>attachment file</source>
        <translation>вкладений файл</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSendMessage.qml" line="1046"/>
        <location filename="../../qml/pages/PageSendMessage.qml" line="1046"/>
        <source>%1: Message has been sent</source>
        <translation>%1: Повідомлення надіслано</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSendMessage.qml" line="1047"/>
        <location filename="../../qml/pages/PageSendMessage.qml" line="1047"/>
        <source>Message has successfully been sent to all recipients.</source>
        <translation>Повідомлення успішно надіслано всім одержувачам.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSendMessage.qml" line="1051"/>
        <location filename="../../qml/pages/PageSendMessage.qml" line="1051"/>
        <source>%1: Error sending message!</source>
        <translation>%1: Помилка під час надсилання повідомлення!</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSendMessage.qml" line="1052"/>
        <location filename="../../qml/pages/PageSendMessage.qml" line="1052"/>
        <source>Message has NOT been sent to all recipients.</source>
        <translation>Повідомлення надіслано НЕ всім одержувачам.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSendMessage.qml" line="1082"/>
        <location filename="../../qml/pages/PageSendMessage.qml" line="1082"/>
        <source>mandatory</source>
        <translation>обов&apos;язковий</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSendMessage.qml" line="1100"/>
        <location filename="../../qml/pages/PageSendMessage.qml" line="1100"/>
        <source>General message settings page.</source>
        <translation>Загальні налаштування повідомлень.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSendMessage.qml" line="964"/>
        <location filename="../../qml/pages/PageSendMessage.qml" line="965"/>
        <location filename="../../qml/pages/PageSendMessage.qml" line="964"/>
        <location filename="../../qml/pages/PageSendMessage.qml" line="965"/>
        <source>View PDF</source>
        <translation>Переглянути PDF</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSendMessage.qml" line="974"/>
        <location filename="../../qml/pages/PageSendMessage.qml" line="974"/>
        <source>Append PDF</source>
        <translation>Додати PDF</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSendMessage.qml" line="975"/>
        <location filename="../../qml/pages/PageSendMessage.qml" line="975"/>
        <source>Append</source>
        <translation>Додати</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSendMessage.qml" line="991"/>
        <location filename="../../qml/pages/PageSendMessage.qml" line="991"/>
        <source>Enter a short text message.</source>
        <translation>Введіть коротке текстове повідомлення.</translation>
    </message>
</context>
<context>
    <name>PageSettingsAccount</name>
    <message>
        <location filename="../../qml/pages/PageSettingsAccount.qml" line="442"/>
        <location filename="../../qml/pages/PageSettingsAccount.qml" line="442"/>
        <source>Change username: %1</source>
        <translation>Змінити ім’я користувача: %1</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsAccount.qml" line="447"/>
        <location filename="../../qml/pages/PageSettingsAccount.qml" line="447"/>
        <source>Username problem: %1</source>
        <translation>Проблема з іменем користувача: %1</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsAccount.qml" line="93"/>
        <location filename="../../qml/pages/PageSettingsAccount.qml" line="93"/>
        <source>Data box &apos;%1&apos; couldn&apos;t be added.</source>
        <translation>Скриньку &quot;%1&quot; не вдалося додати.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsAccount.qml" line="93"/>
        <location filename="../../qml/pages/PageSettingsAccount.qml" line="93"/>
        <source>Data box couldn&apos;t be added.</source>
        <translation>Скриньку не вдалося додати.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsAccount.qml" line="94"/>
        <location filename="../../qml/pages/PageSettingsAccount.qml" line="94"/>
        <source>Add Data Box Error</source>
        <translation>Не вдалося додати скриньку</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsAccount.qml" line="99"/>
        <location filename="../../qml/pages/PageSettingsAccount.qml" line="99"/>
        <source>Change Username: %1</source>
        <translation>Змінити ім’я користувача: %1</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsAccount.qml" line="100"/>
        <location filename="../../qml/pages/PageSettingsAccount.qml" line="100"/>
        <source>Do you want to change the username from &apos;%1&apos; to &apos;%2&apos; for the data box &apos;%3&apos;?</source>
        <translation>Ви хочете змінити ім&apos;я користувача &quot;%1&quot; на &quot;%2&quot; для скриньки &quot;%3&quot;?</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsAccount.qml" line="101"/>
        <location filename="../../qml/pages/PageSettingsAccount.qml" line="101"/>
        <source>Note: It will also change all related local database names and data-box information.</source>
        <translation>Примітка: Назви всіх відповідних баз даних та інформація про скриньки також зміняться.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsAccount.qml" line="206"/>
        <location filename="../../qml/pages/PageSettingsAccount.qml" line="206"/>
        <source>Connection to Data Box: %1</source>
        <translation>Підключеня до скриньки: %1</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsAccount.qml" line="207"/>
        <location filename="../../qml/pages/PageSettingsAccount.qml" line="207"/>
        <source>Data box &apos;%1&apos; could not be added.</source>
        <translation>Скриньку &quot;%1&quot; не вдалося додати.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsAccount.qml" line="215"/>
        <location filename="../../qml/pages/PageSettingsAccount.qml" line="215"/>
        <source>Add Data Box</source>
        <translation>Додати скриньку</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsAccount.qml" line="238"/>
        <location filename="../../qml/pages/PageSettingsAccount.qml" line="238"/>
        <source>Login Method</source>
        <translation>Метод входу</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsAccount.qml" line="240"/>
        <location filename="../../qml/pages/PageSettingsAccount.qml" line="240"/>
        <source>Select the login method which you use to access the data box in the ISDS.</source>
        <translation>Виберіть спосіб входу, який ви використовуєте для доступу до датової криньки у середовищі ISDS.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsAccount.qml" line="246"/>
        <location filename="../../qml/pages/PageSettingsAccount.qml" line="246"/>
        <source>Select login method</source>
        <translation>Виберіть спосіб входу</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsAccount.qml" line="250"/>
        <location filename="../../qml/pages/PageSettingsAccount.qml" line="250"/>
        <source>Username + Password</source>
        <translation>Ім&apos;я користувача + пароль</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsAccount.qml" line="251"/>
        <location filename="../../qml/pages/PageSettingsAccount.qml" line="251"/>
        <source>Certificate + Password</source>
        <translation>Сертифікат + Пароль</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsAccount.qml" line="252"/>
        <location filename="../../qml/pages/PageSettingsAccount.qml" line="252"/>
        <source>Password + Security code</source>
        <translation>Пароль + Код безпеки</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsAccount.qml" line="253"/>
        <location filename="../../qml/pages/PageSettingsAccount.qml" line="253"/>
        <source>Password + Security SMS</source>
        <translation>Пароль + SMS безпеки</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsAccount.qml" line="254"/>
        <location filename="../../qml/pages/PageSettingsAccount.qml" line="254"/>
        <source>Mobile key</source>
        <translation>Мобільний ключ (Mobilní klíč)</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsAccount.qml" line="265"/>
        <location filename="../../qml/pages/PageSettingsAccount.qml" line="265"/>
        <source>Certificate</source>
        <translation>Сертифікат</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsAccount.qml" line="267"/>
        <location filename="../../qml/pages/PageSettingsAccount.qml" line="267"/>
        <source>The certificate file is needed for authentication purposes. The supplied file must contain a certificate and its corresponding private key.</source>
        <translation>Файл сертифіката потрібен для аутентифікації. Наданий файл повинен містити сертифікат і відповідний йому приватний ключ.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsAccount.qml" line="268"/>
        <location filename="../../qml/pages/PageSettingsAccount.qml" line="268"/>
        <source>Only PEM and PFX file formats are accepted.</source>
        <translation>Приймаються лише формати файлів PEM та PFX.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsAccount.qml" line="277"/>
        <location filename="../../qml/pages/PageSettingsAccount.qml" line="277"/>
        <source>Choose File</source>
        <translation>Виберіть файл</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsAccount.qml" line="283"/>
        <location filename="../../qml/pages/PageSettingsAccount.qml" line="283"/>
        <source>Data Box Title</source>
        <translation>Назва скриньки</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsAccount.qml" line="284"/>
        <location filename="../../qml/pages/PageSettingsAccount.qml" line="284"/>
        <source>Enter custom data-box name.</source>
        <translation>Введіть назву скриньки.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsAccount.qml" line="286"/>
        <location filename="../../qml/pages/PageSettingsAccount.qml" line="286"/>
        <source>The data-box title is a user-specified name used for the identification of the data box in the application (e.g. &apos;My Personal Data Box&apos;, &apos;Firm Box&apos;, etc.). The chosen name serves only for your convenience.</source>
        <translation>Назва скриньки - це вибране користувачем ім&apos;я, яке використовується для ідентифікації скриньки в цій програмі (наприклад, &quot;Моя особиста скриньки із даними&quot;, &quot;Скринька компанії&quot; тощо). Обрана назва призначена лише для вашої зручності. Це поле є обов&apos;язковим для заповнення.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsAccount.qml" line="355"/>
        <location filename="../../qml/pages/PageSettingsAccount.qml" line="355"/>
        <source>You have to have the Mobile Key application installed. The Mobile Key application needs to be paired with the corresponding data-box on the ISDS web portal.</source>
        <translation>У вас має бути встановлений додаток Mobile Key. Цей зовнішній додаток має бути з&apos;єднаний з вашою скринька із даними на порталі ISDS.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsAccount.qml" line="365"/>
        <location filename="../../qml/pages/PageSettingsAccount.qml" line="365"/>
        <source>Test data box</source>
        <translation>Тестова скринька</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsAccount.qml" line="368"/>
        <location filename="../../qml/pages/PageSettingsAccount.qml" line="368"/>
        <source>Test data boxes are used to access the ISDS testing environment (www.czebox.cz) while the regular data boxes are used to access the production ISDS environment (www.mojedatovaschranka.cz).</source>
        <translation>Тестова скринька використовується для доступу до тестового середовища ISDS (www.czebox.cz), тоді як звичайна скринька із даними використовується для доступу до виробничого (офіційного) середовища ISDS (www.mojedatovaschranka.cz).</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsAccount.qml" line="385"/>
        <location filename="../../qml/pages/PageSettingsAccount.qml" line="385"/>
        <source>The data box will be included into the synchronisation process of all data boxes.</source>
        <translation>Скринька буде включена до масової синхронізації всіх скриньок із даними.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsAccount.qml" line="386"/>
        <location filename="../../qml/pages/PageSettingsAccount.qml" line="386"/>
        <source>The data box won&apos;t be included into the synchronisation process of all data boxes.</source>
        <translation>Скриньку не буде включено до масової синхронізації всіх скриньок із даними.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsAccount.qml" line="392"/>
        <location filename="../../qml/pages/PageSettingsAccount.qml" line="392"/>
        <source>Login &amp;&amp; Add Data Box</source>
        <translation>Увійти та додати скриньку</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsAccount.qml" line="406"/>
        <location filename="../../qml/pages/PageSettingsAccount.qml" line="406"/>
        <source>Logging into the data box ...</source>
        <translation>Вхід у датову скриньку ...</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsAccount.qml" line="443"/>
        <location filename="../../qml/pages/PageSettingsAccount.qml" line="443"/>
        <source>A new username &apos;%1&apos; for the data box &apos;%2&apos; has been set.</source>
        <translation>Встановлено нове ім&apos;я користувача &quot;%1&quot; скриньки із даними &quot;%2&quot;.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsAccount.qml" line="444"/>
        <location filename="../../qml/pages/PageSettingsAccount.qml" line="444"/>
        <source>The data box will use the new settings.</source>
        <translation>Скринька із даними буде використовувати нові налаштування.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsAccount.qml" line="448"/>
        <location filename="../../qml/pages/PageSettingsAccount.qml" line="448"/>
        <source>The new username &apos;%1&apos; for data box &apos;%2&apos; has not been set</source>
        <translation>Нове ім&apos;я користувача &quot;%1&quot; скриньки із даними &quot;%2&quot; не було встановлено</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsAccount.qml" line="449"/>
        <location filename="../../qml/pages/PageSettingsAccount.qml" line="449"/>
        <source>Data box will use the original settings.</source>
        <translation>Скринька із даними буде використовувати початкові налаштування.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsAccount.qml" line="472"/>
        <location filename="../../qml/pages/PageSettingsAccount.qml" line="472"/>
        <source>Data Box Settings</source>
        <translation>Налаштування скриньки</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsAccount.qml" line="127"/>
        <location filename="../../qml/pages/PageSettingsAccount.qml" line="127"/>
        <source>Choose a certificate</source>
        <translation>Вибрати сертифікат</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsAccount.qml" line="241"/>
        <location filename="../../qml/pages/PageSettingsAccount.qml" line="241"/>
        <source>Note: NIA login methods such as bank ID, mojeID or eCitizen aren&apos;t supported because the ISDS system doesn&apos;t provide such functionality for third-party applications.</source>
        <translation>Примітка: Методи входу в NIA, такі як Bank ID, MojeID або eCitizen, не підтримуються, оскільки система ISDS не підтримує їхню функціональність для сторонніх додатків.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsAccount.qml" line="287"/>
        <location filename="../../qml/pages/PageSettingsAccount.qml" line="287"/>
        <source>The entry must be filled in.</source>
        <translation>Пункт повинен бути заповнений.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsAccount.qml" line="294"/>
        <location filename="../../qml/pages/PageSettingsAccount.qml" line="294"/>
        <source>Enter the login name.</source>
        <translation>Введіть ім&apos;я для входу.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsAccount.qml" line="297"/>
        <location filename="../../qml/pages/PageSettingsAccount.qml" line="297"/>
        <source>The username must consist of at least 6 characters without spaces (only combinations of lower-case letters and digits are allowed). The entry must be filled in.</source>
        <translation>Ім&apos;я користувача має складатися щонайменше з 6 символів без пробілів (допускаються тільки комбінації малих літер та цифр). Поле повинно бути заповнене.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsAccount.qml" line="298"/>
        <location filename="../../qml/pages/PageSettingsAccount.qml" line="298"/>
        <source>Note: The username is not a data-box ID.</source>
        <translation>Примітка: ім&apos;я користувача не є ідентифікатором скриньки із даними.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsAccount.qml" line="315"/>
        <location filename="../../qml/pages/PageSettingsAccount.qml" line="315"/>
        <source>The username should contain only combinations of lower-case letters and digits.</source>
        <translation>Ім&apos;я користувача має складатися тільки з комбінації малих літер та цифр.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsAccount.qml" line="322"/>
        <location filename="../../qml/pages/PageSettingsAccount.qml" line="322"/>
        <source>PIN Required</source>
        <translation>Необхідний PIN-код</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsAccount.qml" line="333"/>
        <location filename="../../qml/pages/PageSettingsAccount.qml" line="333"/>
        <source>Enter the password.</source>
        <translation>Введіть пароль.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsAccount.qml" line="351"/>
        <location filename="../../qml/pages/PageSettingsAccount.qml" line="351"/>
        <source>Enter the communication code.</source>
        <translation>Введіть код зв&apos;язку.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsAccount.qml" line="367"/>
        <location filename="../../qml/pages/PageSettingsAccount.qml" line="367"/>
        <source>Switch on if the data box is in the testing environment.</source>
        <translation>Увімкніть, якщо ця скринька в тестовому середовищі ISDS.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsAccount.qml" line="373"/>
        <location filename="../../qml/pages/PageSettingsAccount.qml" line="373"/>
        <source>Use local storage</source>
        <translation>Використовувати локальне сховище</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsAccount.qml" line="382"/>
        <location filename="../../qml/pages/PageSettingsAccount.qml" line="382"/>
        <source>Synchronise with all</source>
        <translation>Синхронізувати з усіма</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsAccount.qml" line="392"/>
        <location filename="../../qml/pages/PageSettingsAccount.qml" line="392"/>
        <source>Save Changes</source>
        <translation>Зберегти зміни</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsAccount.qml" line="293"/>
        <location filename="../../qml/pages/PageSettingsAccount.qml" line="293"/>
        <source>Username</source>
        <translation>Ім&apos;я користувача</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsAccount.qml" line="329"/>
        <location filename="../../qml/pages/PageSettingsAccount.qml" line="329"/>
        <source>Password</source>
        <translation>Пароль</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsAccount.qml" line="331"/>
        <location filename="../../qml/pages/PageSettingsAccount.qml" line="331"/>
        <source>The password must be valid and non-expired. To check whether you&apos;ve entered the password correctly you may use the icon in the field on the right. The entry must be filled in.</source>
        <translation>Пароль має бути дійсним. Використовуйте іконку з оком праворуч, щоб перевірити та переглянути правильність пароля. Поле повинно бути заповнене.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsAccount.qml" line="332"/>
        <location filename="../../qml/pages/PageSettingsAccount.qml" line="332"/>
        <source>Note: You must first change the password using the ISDS web portal if it is your very first attempt to log into the data box.</source>
        <translation>Примітка: Якщо ви вперше входите до скриньки з паролем, призначеним оператором системи ISDS, вам необхідно спочатку змінити пароль на порталі ISDS.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsAccount.qml" line="322"/>
        <location filename="../../qml/pages/PageSettingsAccount.qml" line="322"/>
        <source>Enter PIN code in order to show the password.</source>
        <translation>Введіть PIN-код, щоб відобразити пароль.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsAccount.qml" line="323"/>
        <location filename="../../qml/pages/PageSettingsAccount.qml" line="323"/>
        <source>Enter PIN</source>
        <translation>Введіть PIN-код</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsAccount.qml" line="350"/>
        <location filename="../../qml/pages/PageSettingsAccount.qml" line="350"/>
        <source>Communication code</source>
        <translation>Код зв&apos;язку</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsAccount.qml" line="354"/>
        <location filename="../../qml/pages/PageSettingsAccount.qml" line="354"/>
        <source>The communication code is a string which can be generated in the ISDS web portal. The entry must be filled in.</source>
        <translation>Код зв&apos;язку - це текстовий рядок, який генерується на порталі ISDS. Поле повинне для заповнення.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsAccount.qml" line="360"/>
        <location filename="../../qml/pages/PageSettingsAccount.qml" line="360"/>
        <source>Remember password</source>
        <translation>Запам&apos;ятати пароль</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsAccount.qml" line="376"/>
        <location filename="../../qml/pages/PageSettingsAccount.qml" line="376"/>
        <source>Messages and attachments will be locally stored. No active internet connection is needed to access locally stored data.</source>
        <translation>Повідомлення та вкладення зберігатимуться локально. Для доступу до локально збережених даних не потрібне активне підключення до Інтернету.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsAccount.qml" line="377"/>
        <location filename="../../qml/pages/PageSettingsAccount.qml" line="377"/>
        <source>Messages and attachments will be stored only temporarily in memory. These data will be lost on application exit.</source>
        <translation>Повідомлення та вкладення зберігатимуться в пам’яті лише тимчасово. Ці дані будуть втрачені під час виходу з програми.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsAccount.qml" line="344"/>
        <location filename="../../qml/pages/PageSettingsAccount.qml" line="344"/>
        <source>Entered PIN is not valid.</source>
        <translation>Введений PIN-код недійсний.</translation>
    </message>
</context>
<context>
    <name>PageSettingsGeneral</name>
    <message>
        <location filename="../../qml/pages/PageSettingsGeneral.qml" line="59"/>
        <location filename="../../qml/pages/PageSettingsGeneral.qml" line="59"/>
        <source>Theme</source>
        <translation>Зовнішній вигляд</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsGeneral.qml" line="64"/>
        <location filename="../../qml/pages/PageSettingsGeneral.qml" line="64"/>
        <source>Dark theme</source>
        <translation>Темний режим</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsGeneral.qml" line="68"/>
        <location filename="../../qml/pages/PageSettingsGeneral.qml" line="68"/>
        <source>Note: Theme will be changed after application restart.</source>
        <translation>Примітка: Зовнішній вигляд буде змінено після перезапуску програми.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsGeneral.qml" line="74"/>
        <location filename="../../qml/pages/PageSettingsGeneral.qml" line="74"/>
        <source>Language</source>
        <translation>Мова</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsGeneral.qml" line="79"/>
        <location filename="../../qml/pages/PageSettingsGeneral.qml" line="79"/>
        <source>Select language</source>
        <translation>Оберіть мову</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsGeneral.qml" line="83"/>
        <location filename="../../qml/pages/PageSettingsGeneral.qml" line="83"/>
        <source>System</source>
        <translation>Система</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsGeneral.qml" line="84"/>
        <location filename="../../qml/pages/PageSettingsGeneral.qml" line="84"/>
        <source>Czech</source>
        <translation>чеський</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsGeneral.qml" line="85"/>
        <location filename="../../qml/pages/PageSettingsGeneral.qml" line="85"/>
        <source>English</source>
        <translation>англійська</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsGeneral.qml" line="86"/>
        <location filename="../../qml/pages/PageSettingsGeneral.qml" line="86"/>
        <source>Ukrainian</source>
        <translation>українська</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsGeneral.qml" line="40"/>
        <location filename="../../qml/pages/PageSettingsGeneral.qml" line="40"/>
        <source>General Settings</source>
        <translation>Загальні налаштування</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsGeneral.qml" line="91"/>
        <location filename="../../qml/pages/PageSettingsGeneral.qml" line="91"/>
        <source>Note: Language will be changed after application restart.</source>
        <translation>Примітка: мова буде змінена після перезапуску програми.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsGeneral.qml" line="97"/>
        <location filename="../../qml/pages/PageSettingsGeneral.qml" line="97"/>
        <source>Font</source>
        <translation>Шрифт</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsGeneral.qml" line="102"/>
        <location filename="../../qml/pages/PageSettingsGeneral.qml" line="102"/>
        <source>Select font</source>
        <translation>Виберіть шрифт</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsGeneral.qml" line="105"/>
        <location filename="../../qml/pages/PageSettingsGeneral.qml" line="105"/>
        <source>System Default</source>
        <translation>Система за замовчуванням</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsGeneral.qml" line="106"/>
        <location filename="../../qml/pages/PageSettingsGeneral.qml" line="106"/>
        <source>Roboto</source>
        <translation>Roboto</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsGeneral.qml" line="107"/>
        <location filename="../../qml/pages/PageSettingsGeneral.qml" line="107"/>
        <source>Source Sans Pro</source>
        <translation>Source Sans Pro</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsGeneral.qml" line="112"/>
        <location filename="../../qml/pages/PageSettingsGeneral.qml" line="112"/>
        <source>Note: Font will be changed after application restart.</source>
        <translation>Примітка: Шрифт буде змінено після перезапуску програми.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsGeneral.qml" line="118"/>
        <location filename="../../qml/pages/PageSettingsGeneral.qml" line="118"/>
        <source>Font size and application scale</source>
        <translation>Розмір шрифту та масштаб застосування</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsGeneral.qml" line="127"/>
        <location filename="../../qml/pages/PageSettingsGeneral.qml" line="127"/>
        <source>Set application font size</source>
        <translation>Встановити розмір шрифту</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsGeneral.qml" line="135"/>
        <location filename="../../qml/pages/PageSettingsGeneral.qml" line="135"/>
        <source>Note: Font size will be changed after application restart. Default is %1.</source>
        <translation>Примітка: Розмір шрифту буде змінено після перезапуску програми. За замовчуванням – %1.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsGeneral.qml" line="141"/>
        <location filename="../../qml/pages/PageSettingsGeneral.qml" line="141"/>
        <source>Debug verbosity level</source>
        <translation>Рівень детальності налагодження</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsGeneral.qml" line="149"/>
        <location filename="../../qml/pages/PageSettingsGeneral.qml" line="149"/>
        <source>Set the amount of logged debugging information</source>
        <translation>Встановіть кількість записаної налагоджувальної інформації</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsGeneral.qml" line="153"/>
        <location filename="../../qml/pages/PageSettingsGeneral.qml" line="153"/>
        <source>Debug verbosity controls the amount of debugging information written to the log file.</source>
        <translation>Детальність налагодження контролює кількість налагоджувальної інформації, записаної у файл журналу.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsGeneral.qml" line="159"/>
        <location filename="../../qml/pages/PageSettingsGeneral.qml" line="159"/>
        <source>Log verbosity level</source>
        <translation>Рівень детальності журналу</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsGeneral.qml" line="167"/>
        <location filename="../../qml/pages/PageSettingsGeneral.qml" line="167"/>
        <source>Set the verbosity of logged entries</source>
        <translation>Встановіть детальність зареєстрованих записів</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsGeneral.qml" line="171"/>
        <location filename="../../qml/pages/PageSettingsGeneral.qml" line="171"/>
        <source>Log verbosity controls the level of detail of the logged entries.</source>
        <translation>Детальність журналу контролює рівень деталізації записів.</translation>
    </message>
</context>
<context>
    <name>PageSettingsPin</name>
    <message>
        <location filename="../../qml/pages/PageSettingsPin.qml" line="42"/>
        <location filename="../../qml/pages/PageSettingsPin.qml" line="42"/>
        <source>Currently there is no PIN code set.</source>
        <translation>Наразі PIN-код не встановлено.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsPin.qml" line="47"/>
        <location filename="../../qml/pages/PageSettingsPin.qml" line="47"/>
        <source>You will be asked to enter a PIN code on application start-up.</source>
        <translation>Під час запуску програми вам буде запропоновано ввести PIN-код.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsPin.qml" line="65"/>
        <location filename="../../qml/pages/PageSettingsPin.qml" line="65"/>
        <source>Enter a new PIN code into both text fields:</source>
        <translation>Введіть новий PIN-код в обидва текстові поля:</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsPin.qml" line="80"/>
        <location filename="../../qml/pages/PageSettingsPin.qml" line="96"/>
        <location filename="../../qml/pages/PageSettingsPin.qml" line="80"/>
        <location filename="../../qml/pages/PageSettingsPin.qml" line="96"/>
        <source>In order to change the PIN code you must enter the current and a new PIN code:</source>
        <translation>Щоб змінити PIN-код, необхідно ввести поточний та новий PIN-код:</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsPin.qml" line="111"/>
        <location filename="../../qml/pages/PageSettingsPin.qml" line="111"/>
        <source>Something went wrong!</source>
        <translation>Щось пішло не так!</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsPin.qml" line="128"/>
        <location filename="../../qml/pages/PageSettingsPin.qml" line="150"/>
        <location filename="../../qml/pages/PageSettingsPin.qml" line="128"/>
        <location filename="../../qml/pages/PageSettingsPin.qml" line="150"/>
        <source>Error: Both new PIN code fields must be filled in.</source>
        <translation>Помилка: необхідно заповнити обидва нові поля PIN-коду.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsPin.qml" line="137"/>
        <location filename="../../qml/pages/PageSettingsPin.qml" line="159"/>
        <location filename="../../qml/pages/PageSettingsPin.qml" line="137"/>
        <location filename="../../qml/pages/PageSettingsPin.qml" line="159"/>
        <source>Error: Newly entered PIN codes are different.</source>
        <translation>Помилка: нові введені PIN-коди відрізняються.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsPin.qml" line="170"/>
        <location filename="../../qml/pages/PageSettingsPin.qml" line="185"/>
        <location filename="../../qml/pages/PageSettingsPin.qml" line="170"/>
        <location filename="../../qml/pages/PageSettingsPin.qml" line="185"/>
        <source>Error: Current PIN code is wrong.</source>
        <translation>Помилка: поточний PIN-код неправильний.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsPin.qml" line="192"/>
        <location filename="../../qml/pages/PageSettingsPin.qml" line="192"/>
        <source>PIN Settings</source>
        <translation>Налаштування PIN-коду</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsPin.qml" line="303"/>
        <location filename="../../qml/pages/PageSettingsPin.qml" line="303"/>
        <source>Save Changes</source>
        <translation>Зберегти зміни</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsPin.qml" line="220"/>
        <location filename="../../qml/pages/PageSettingsPin.qml" line="220"/>
        <source>Set PIN</source>
        <translation>Встановити PIN-код</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsPin.qml" line="229"/>
        <location filename="../../qml/pages/PageSettingsPin.qml" line="229"/>
        <source>Change PIN</source>
        <translation>Змінити PIN-код</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsPin.qml" line="238"/>
        <location filename="../../qml/pages/PageSettingsPin.qml" line="238"/>
        <source>Disable PIN</source>
        <translation>Вимкнути PIN-код</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsPin.qml" line="250"/>
        <location filename="../../qml/pages/PageSettingsPin.qml" line="250"/>
        <source>Current PIN code</source>
        <translation>Поточний PIN-код</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsPin.qml" line="260"/>
        <location filename="../../qml/pages/PageSettingsPin.qml" line="260"/>
        <source>New PIN code</source>
        <translation>Новий PIN-код</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsPin.qml" line="270"/>
        <location filename="../../qml/pages/PageSettingsPin.qml" line="270"/>
        <source>Confirm new PIN code</source>
        <translation>Підтвердьте новий PIN-код</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsPin.qml" line="283"/>
        <location filename="../../qml/pages/PageSettingsPin.qml" line="283"/>
        <source>Lock after seconds of inactivity</source>
        <translation>Блокування після секунд бездіяльності</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsPin.qml" line="291"/>
        <location filename="../../qml/pages/PageSettingsPin.qml" line="291"/>
        <source>don&apos;t lock</source>
        <translation>не замикати</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsPin.qml" line="293"/>
        <location filename="../../qml/pages/PageSettingsPin.qml" line="293"/>
        <source>Select the number of seconds after which the application is going to be locked.</source>
        <translation>Виберіть кількість секунд, через які програма буде заблокована.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsPin.qml" line="304"/>
        <location filename="../../qml/pages/PageSettingsPin.qml" line="304"/>
        <source>Accept changes.</source>
        <translation>Прийняти зміни.</translation>
    </message>
</context>
<context>
    <name>PageSettingsRecordsManagement</name>
    <message>
        <location filename="../../qml/pages/PageSettingsRecordsManagement.qml" line="76"/>
        <location filename="../../qml/pages/PageSettingsRecordsManagement.qml" line="76"/>
        <source>Service URL</source>
        <translation>URL-адреса служби</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsRecordsManagement.qml" line="84"/>
        <location filename="../../qml/pages/PageSettingsRecordsManagement.qml" line="84"/>
        <source>Your token</source>
        <translation>Ваш токен</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsRecordsManagement.qml" line="60"/>
        <location filename="../../qml/pages/PageSettingsRecordsManagement.qml" line="60"/>
        <source>Records Management</source>
        <translation>Управління записами</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsRecordsManagement.qml" line="77"/>
        <location filename="../../qml/pages/PageSettingsRecordsManagement.qml" line="77"/>
        <source>Enter service URL.</source>
        <translation>Введіть URL-адресу служби.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsRecordsManagement.qml" line="79"/>
        <location filename="../../qml/pages/PageSettingsRecordsManagement.qml" line="79"/>
        <source>Please fill in service URL.</source>
        <translation>Введіть URL-адресу файлового сервісу.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsRecordsManagement.qml" line="85"/>
        <location filename="../../qml/pages/PageSettingsRecordsManagement.qml" line="85"/>
        <source>Enter your token.</source>
        <translation>Введіть свій токен.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsRecordsManagement.qml" line="87"/>
        <location filename="../../qml/pages/PageSettingsRecordsManagement.qml" line="87"/>
        <source>Please fill in your identification token.</source>
        <translation>Заповніть комунікаційний код файлового сервісу.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsRecordsManagement.qml" line="97"/>
        <location filename="../../qml/pages/PageSettingsRecordsManagement.qml" line="97"/>
        <source>Save Changes</source>
        <translation>Зберегти зміни</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsRecordsManagement.qml" line="98"/>
        <location filename="../../qml/pages/PageSettingsRecordsManagement.qml" line="98"/>
        <source>Accept changes.</source>
        <translation>Прийняти зміни.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsRecordsManagement.qml" line="108"/>
        <location filename="../../qml/pages/PageSettingsRecordsManagement.qml" line="108"/>
        <source>Get Service Info</source>
        <translation>Отримати сервісну інформацію</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsRecordsManagement.qml" line="119"/>
        <location filename="../../qml/pages/PageSettingsRecordsManagement.qml" line="119"/>
        <source>Clear records management data.</source>
        <translation>Очистити дані управління записами.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsRecordsManagement.qml" line="120"/>
        <location filename="../../qml/pages/PageSettingsRecordsManagement.qml" line="120"/>
        <source>Clear</source>
        <translation>Очистити</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsRecordsManagement.qml" line="130"/>
        <location filename="../../qml/pages/PageSettingsRecordsManagement.qml" line="130"/>
        <source>Communication error. Cannot obtain records management info from server. Internet connection failed or service URL and/or identification token could be wrong.</source>
        <translation>Помилка зв&apos;язку. Не вдається отримати інформацію про керування записами із сервера. Помилка підключення до Інтернету або URL-адреса служби та ідентифікаційний маркер можуть бути неправильними.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsRecordsManagement.qml" line="137"/>
        <location filename="../../qml/pages/PageSettingsRecordsManagement.qml" line="137"/>
        <source>Service name</source>
        <translation>Назва послуги</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsRecordsManagement.qml" line="145"/>
        <location filename="../../qml/pages/PageSettingsRecordsManagement.qml" line="145"/>
        <source>Service token</source>
        <translation>Токен сервісу</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsRecordsManagement.qml" line="154"/>
        <location filename="../../qml/pages/PageSettingsRecordsManagement.qml" line="154"/>
        <source>Service logo</source>
        <translation>Логотип служби</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsRecordsManagement.qml" line="162"/>
        <location filename="../../qml/pages/PageSettingsRecordsManagement.qml" line="162"/>
        <source>Records management logo.</source>
        <translation>Логотип управління записами.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsRecordsManagement.qml" line="170"/>
        <location filename="../../qml/pages/PageSettingsRecordsManagement.qml" line="170"/>
        <source>Update list of uploaded files</source>
        <translation>Оновити список завантажених файлів</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsRecordsManagement.qml" line="176"/>
        <location filename="../../qml/pages/PageSettingsRecordsManagement.qml" line="176"/>
        <source>Update list of uploaded files from records management.</source>
        <translation>Оновлення списку завантажених файлів з діловодства.</translation>
    </message>
</context>
<context>
    <name>PageSettingsSync</name>
    <message>
        <location filename="../../qml/pages/PageSettingsSync.qml" line="37"/>
        <location filename="../../qml/pages/PageSettingsSync.qml" line="37"/>
        <source>Synchronisation Settings</source>
        <translation>Параметри синхронізації</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsSync.qml" line="56"/>
        <location filename="../../qml/pages/PageSettingsSync.qml" line="56"/>
        <source>Download only newer messages</source>
        <translation>Завантажувати лише новіші повідомлення</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsSync.qml" line="62"/>
        <location filename="../../qml/pages/PageSettingsSync.qml" line="62"/>
        <source>Only messages which are not older than 90 days will be downloaded.</source>
        <translation>Будуть завантажені лише повідомлення, які не старші 90 днів.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsSync.qml" line="63"/>
        <location filename="../../qml/pages/PageSettingsSync.qml" line="63"/>
        <source>All available messages (including those in the data vault) will be downloaded.</source>
        <translation>Будуть завантажені всі доступні повідомлення (включаючи ті, що знаходяться в сховищі даних).</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsSync.qml" line="70"/>
        <location filename="../../qml/pages/PageSettingsSync.qml" line="70"/>
        <source>Download complete messages</source>
        <translation>Завантажувати повні повідомлення</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsSync.qml" line="75"/>
        <location filename="../../qml/pages/PageSettingsSync.qml" line="75"/>
        <source>Complete messages will be downloaded during data box synchronisation. This may be slow.</source>
        <translation>Повні повідомлення будуть завантажені під час синхронізації скриньок. Це може бути повільно.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsSync.qml" line="76"/>
        <location filename="../../qml/pages/PageSettingsSync.qml" line="76"/>
        <source>Only message envelopes will be downloaded during data box synchronisation.</source>
        <translation>Під час синхронізації скриньок будуть завантажені лише конверти повідомлень.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsSync.qml" line="83"/>
        <location filename="../../qml/pages/PageSettingsSync.qml" line="83"/>
        <source>Synchronise data box after start</source>
        <translation>Синхронізація скриньок після запуск</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsSync.qml" line="88"/>
        <location filename="../../qml/pages/PageSettingsSync.qml" line="88"/>
        <source>Data boxes will automatically be synchronised a few seconds after application start-up.</source>
        <translation>Скриньки будуть автоматично синхронізовані через кілька секунд після запуску програми.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsSync.qml" line="89"/>
        <location filename="../../qml/pages/PageSettingsSync.qml" line="89"/>
        <source>Data boxes will be synchronised only when the action is manually invoked.</source>
        <translation>Скриньки будуть синхронізовані лише після того, як цю дію буде викликано вручну.</translation>
    </message>
</context>
<context>
    <name>PageTransferData</name>
    <message>
        <location filename="../../qml/pages/PageTransferData.qml" line="63"/>
        <location filename="../../qml/pages/PageTransferData.qml" line="63"/>
        <source>Transfer Error</source>
        <translation>Помилка передачі</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageTransferData.qml" line="64"/>
        <location filename="../../qml/pages/PageTransferData.qml" line="64"/>
        <source>An error occurred when backing up data to target location. Transfer has been cancelled.</source>
        <translation>Під час резервного копіювання даних у цільове розташування сталася помилка. Передачу скасовано.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageTransferData.qml" line="65"/>
        <location filename="../../qml/pages/PageTransferData.qml" line="65"/>
        <source>Application data were not backed up to target location.</source>
        <translation>Дані програми не були створені в цільовому місці.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageTransferData.qml" line="75"/>
        <location filename="../../qml/pages/PageTransferData.qml" line="75"/>
        <source>Application sandbox and iCloud.</source>
        <translation>Робоча область програми та iCloud.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageTransferData.qml" line="76"/>
        <location filename="../../qml/pages/PageTransferData.qml" line="76"/>
        <source>Archive will be stored into the application sandbox and uploaded into the iCloud.</source>
        <translation>Архів буде збережено в робочій області програми, а потім завантажено в iCloud.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageTransferData.qml" line="79"/>
        <location filename="../../qml/pages/PageTransferData.qml" line="79"/>
        <source>Transferred data require to be secured with a PIN.</source>
        <translation>Передача даних вимагає, щоб дані в додатку були захищені PIN-кодом.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageTransferData.qml" line="80"/>
        <location filename="../../qml/pages/PageTransferData.qml" line="80"/>
        <source>Transfer Problem</source>
        <translation>Проблема з перенесенням</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageTransferData.qml" line="105"/>
        <location filename="../../qml/pages/PageTransferData.qml" line="105"/>
        <source>Transfer Application Data</source>
        <translation>Передача даних додатка</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageTransferData.qml" line="124"/>
        <location filename="../../qml/pages/PageTransferData.qml" line="124"/>
        <source>The action allows to transfer complete application data as ZIP archive to another device or to Datovka for desktop. Preserve the ZIP archive in a safe place as it contains login and private data.</source>
        <translation>Ця операція дозволяє перенести всі дані додатку у вигляді ZIP-архіву на інший пристрій або в Datovka для ПК. Зберігайте ZIP-архів у безпечному місці, оскільки він містить облікові дані для входу та особисті дані.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageTransferData.qml" line="152"/>
        <location filename="../../qml/pages/PageTransferData.qml" line="152"/>
        <source>Create ZIP Archive</source>
        <translation>Створіть ZIP-архів</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageTransferData.qml" line="153"/>
        <location filename="../../qml/pages/PageTransferData.qml" line="153"/>
        <source>Proceed with the operation.</source>
        <translation>Дродовжити операцію.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageTransferData.qml" line="81"/>
        <location filename="../../qml/pages/PageTransferData.qml" line="81"/>
        <source>The data transfer requires application data to be secured with a PIN.</source>
        <translation>Для передачі даних потрібно, щоб дані програми були захищені PIN-кодом.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageTransferData.qml" line="82"/>
        <location filename="../../qml/pages/PageTransferData.qml" line="82"/>
        <source>Set an application security PIN in the settings and try again.</source>
        <translation>Встановіть PIN-код у налаштуваннях програми та повторіть спробу.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageTransferData.qml" line="131"/>
        <location filename="../../qml/pages/PageTransferData.qml" line="131"/>
        <source>Set PIN</source>
        <translation>Встановити PIN-код</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageTransferData.qml" line="132"/>
        <location filename="../../qml/pages/PageTransferData.qml" line="132"/>
        <source>Set application security PIN.</source>
        <translation>Встановіть PIN-код безпеки.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageTransferData.qml" line="137"/>
        <location filename="../../qml/pages/PageTransferData.qml" line="137"/>
        <source>Target</source>
        <translation>Ціль</translation>
    </message>
</context>
<context>
    <name>PageWelcome</name>
    <message>
        <location filename="../../qml/pages/PageWelcome.qml" line="62"/>
        <location filename="../../qml/pages/PageWelcome.qml" line="62"/>
        <source>Open application home page.</source>
        <translation>Відкрийте домашню сторінку програми.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageWelcome.qml" line="73"/>
        <location filename="../../qml/pages/PageWelcome.qml" line="73"/>
        <source>Add an existing data box into the application.</source>
        <translation>Додайте існуючу скриньку із даними в додаток.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageWelcome.qml" line="69"/>
        <location filename="../../qml/pages/PageWelcome.qml" line="69"/>
        <source>Welcome in the mobile Datovka.</source>
        <translation>Вітаємо в мобільному додатку Datovka.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageWelcome.qml" line="78"/>
        <location filename="../../qml/pages/PageWelcome.qml" line="78"/>
        <source>Add Data Box - Wizard</source>
        <translation>Майстер додавання скриньки</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageWelcome.qml" line="79"/>
        <location filename="../../qml/pages/PageWelcome.qml" line="79"/>
        <source>Add an existing data box using a wizard.</source>
        <translation>Майстер додавання існуючої скриньки.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageWelcome.qml" line="85"/>
        <location filename="../../qml/pages/PageWelcome.qml" line="85"/>
        <source>Add Data Box - Form</source>
        <translation>Додати скриньку за допомогою форми</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageWelcome.qml" line="86"/>
        <location filename="../../qml/pages/PageWelcome.qml" line="86"/>
        <source>Add an existing data box using a form.</source>
        <translation>Додати існуючу скриньку за допомогою форми.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageWelcome.qml" line="93"/>
        <location filename="../../qml/pages/PageWelcome.qml" line="93"/>
        <source>&lt;a href=&quot;%1&quot;&gt;Learn more...&lt;/a&gt;</source>
        <translation>&lt;a href=&quot;%1&quot;&gt;Дізнайтеся більше...&lt;/a&gt;</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageWelcome.qml" line="93"/>
        <location filename="../../qml/pages/PageWelcome.qml" line="93"/>
        <source>Read the user manual for more information about the application behaviour before first use.</source>
        <translation>Прочитайте посібник користувача про те, як працює додаток, перш ніж використовувати його вперше.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageWelcome.qml" line="102"/>
        <location filename="../../qml/pages/PageWelcome.qml" line="102"/>
        <source>MojeID banner.</source>
        <translation>MojeID банер.</translation>
    </message>
</context>
<context>
    <name>PopupMenuAccount</name>
    <message>
        <location filename="../../qml/popups/PopupMenuAccount.qml" line="60"/>
        <location filename="../../qml/popups/PopupMenuAccount.qml" line="60"/>
        <source>Data-Box Settings</source>
        <translation>Налаштування скриньки</translation>
    </message>
    <message>
        <location filename="../../qml/popups/PopupMenuAccount.qml" line="65"/>
        <location filename="../../qml/popups/PopupMenuAccount.qml" line="65"/>
        <source>View Data-Box info</source>
        <translation>Переглянути інформацію про скриньку</translation>
    </message>
    <message>
        <location filename="../../qml/popups/PopupMenuAccount.qml" line="70"/>
        <location filename="../../qml/popups/PopupMenuAccount.qml" line="70"/>
        <source>Data-Box Logo</source>
        <translation>Логотип скриньки</translation>
    </message>
    <message>
        <location filename="../../qml/popups/PopupMenuAccount.qml" line="75"/>
        <location filename="../../qml/popups/PopupMenuAccount.qml" line="75"/>
        <source>Send E-Gov Request</source>
        <translation>Надіслати електронний запит</translation>
    </message>
    <message>
        <location filename="../../qml/popups/PopupMenuAccount.qml" line="80"/>
        <location filename="../../qml/popups/PopupMenuAccount.qml" line="80"/>
        <source>Import Message</source>
        <translation>Імпортувати повідомлення</translation>
    </message>
    <message>
        <location filename="../../qml/popups/PopupMenuAccount.qml" line="85"/>
        <location filename="../../qml/popups/PopupMenuAccount.qml" line="85"/>
        <source>Find Data Box</source>
        <translation>Знайти датову скриньку</translation>
    </message>
    <message>
        <location filename="../../qml/popups/PopupMenuAccount.qml" line="90"/>
        <location filename="../../qml/popups/PopupMenuAccount.qml" line="90"/>
        <source>Back up Data</source>
        <translation>Резервне копіювання даних</translation>
    </message>
    <message>
        <location filename="../../qml/popups/PopupMenuAccount.qml" line="95"/>
        <location filename="../../qml/popups/PopupMenuAccount.qml" line="95"/>
        <source>Repair Message Database</source>
        <translation>Відновити базу даних повідомлень</translation>
    </message>
    <message>
        <location filename="../../qml/popups/PopupMenuAccount.qml" line="101"/>
        <location filename="../../qml/popups/PopupMenuAccount.qml" line="101"/>
        <source>Change Password</source>
        <translation>Змінити пароль</translation>
    </message>
    <message>
        <location filename="../../qml/popups/PopupMenuAccount.qml" line="106"/>
        <location filename="../../qml/popups/PopupMenuAccount.qml" line="106"/>
        <source>Delete Data Box</source>
        <translation>Видалити скриньку</translation>
    </message>
    <message>
        <location filename="../../qml/popups/PopupMenuAccount.qml" line="107"/>
        <location filename="../../qml/popups/PopupMenuAccount.qml" line="107"/>
        <source>Remove Data Box: %1</source>
        <translation>Видалити скриньку: %1</translation>
    </message>
    <message>
        <location filename="../../qml/popups/PopupMenuAccount.qml" line="108"/>
        <location filename="../../qml/popups/PopupMenuAccount.qml" line="108"/>
        <source>Do you want to remove the data box &apos;%1&apos;?</source>
        <translation>Ви дійсно хочете видалити скриньку &apos;%1&apos;?</translation>
    </message>
    <message>
        <location filename="../../qml/popups/PopupMenuAccount.qml" line="109"/>
        <location filename="../../qml/popups/PopupMenuAccount.qml" line="109"/>
        <source>Note: It will also remove all related local databases and data box information.</source>
        <translation>Примітка: Всі відповідні локальні бази даних та інформація про скриньки також будуть видалені.</translation>
    </message>
</context>
<context>
    <name>PopupMenuMessage</name>
    <message>
        <location filename="../../qml/popups/PopupMenuMessage.qml" line="53"/>
        <location filename="../../qml/popups/PopupMenuMessage.qml" line="53"/>
        <source>Mark as Read</source>
        <translation>Позначити як прочитане</translation>
    </message>
    <message>
        <location filename="../../qml/popups/PopupMenuMessage.qml" line="58"/>
        <location filename="../../qml/popups/PopupMenuMessage.qml" line="58"/>
        <source>Mark as Unread</source>
        <translation>Позначити як непрочитане</translation>
    </message>
    <message>
        <location filename="../../qml/popups/PopupMenuMessage.qml" line="64"/>
        <location filename="../../qml/popups/PopupMenuMessage.qml" line="64"/>
        <source>Delete Message</source>
        <translation>Видалити повідомлення</translation>
    </message>
    <message>
        <location filename="../../qml/popups/PopupMenuMessage.qml" line="65"/>
        <location filename="../../qml/popups/PopupMenuMessage.qml" line="65"/>
        <source>Delete Message: %1</source>
        <translation>Видалити повідомлення: %1</translation>
    </message>
    <message>
        <location filename="../../qml/popups/PopupMenuMessage.qml" line="66"/>
        <location filename="../../qml/popups/PopupMenuMessage.qml" line="66"/>
        <source>Do you want to delete the received message &apos;%1&apos;?</source>
        <translation>Бажаєте видалити отримане повідомлення &quot;%1&quot;?</translation>
    </message>
    <message>
        <location filename="../../qml/popups/PopupMenuMessage.qml" line="67"/>
        <location filename="../../qml/popups/PopupMenuMessage.qml" line="67"/>
        <source>Note: It will delete all attachments and message information from the local database.</source>
        <translation>Примітка: це видалить усі вкладення та інформацію про повідомлення з локальної бази даних.</translation>
    </message>
</context>
<context>
    <name>PopupMenuMessageDetail</name>
    <message>
        <location filename="../../qml/popups/PopupMenuMessageDetail.qml" line="84"/>
        <location filename="../../qml/popups/PopupMenuMessageDetail.qml" line="84"/>
        <source>View Message Envelope</source>
        <translation>Переглянути обкладинку повідомлення</translation>
    </message>
    <message>
        <location filename="../../qml/popups/PopupMenuMessageDetail.qml" line="89"/>
        <location filename="../../qml/popups/PopupMenuMessageDetail.qml" line="89"/>
        <source>View Acceptance Info</source>
        <translation>Переглянути інформацію щодо прийняття</translation>
    </message>
    <message>
        <location filename="../../qml/popups/PopupMenuMessageDetail.qml" line="94"/>
        <location filename="../../qml/popups/PopupMenuMessageDetail.qml" line="94"/>
        <source>Use as Template</source>
        <translation>Використовувати як шаблон</translation>
    </message>
    <message>
        <location filename="../../qml/popups/PopupMenuMessageDetail.qml" line="99"/>
        <location filename="../../qml/popups/PopupMenuMessageDetail.qml" line="99"/>
        <source>Upload to Records Management</source>
        <translation>Завантажити в управління записами</translation>
    </message>
    <message>
        <location filename="../../qml/popups/PopupMenuMessageDetail.qml" line="104"/>
        <location filename="../../qml/popups/PopupMenuMessageDetail.qml" line="104"/>
        <source>Send Message ZFO by E-Mail</source>
        <translation>Надіслати повідомлення ZFO електронною поштою</translation>
    </message>
    <message>
        <location filename="../../qml/popups/PopupMenuMessageDetail.qml" line="117"/>
        <location filename="../../qml/popups/PopupMenuMessageDetail.qml" line="117"/>
        <source>Save Message ZFO</source>
        <translation>Зберегти повідомлення ZFO</translation>
    </message>
    <message>
        <location filename="../../qml/popups/PopupMenuMessageDetail.qml" line="120"/>
        <location filename="../../qml/popups/PopupMenuMessageDetail.qml" line="120"/>
        <source>Save Message ZFO: %1</source>
        <translation>Зберегти повідомлення ZFO: %1</translation>
    </message>
    <message>
        <location filename="../../qml/popups/PopupMenuMessageDetail.qml" line="121"/>
        <location filename="../../qml/popups/PopupMenuMessageDetail.qml" line="121"/>
        <source>You can export files into iCloud or into device local storage.</source>
        <translation>Ви можете експортувати файли в iCloud або в локальне сховище пристрою.</translation>
    </message>
    <message>
        <location filename="../../qml/popups/PopupMenuMessageDetail.qml" line="122"/>
        <location filename="../../qml/popups/PopupMenuMessageDetail.qml" line="122"/>
        <source>Do you want to export files to Datovka iCloud container?</source>
        <translation>Ви хочете завантажити файли в контейнер програми в iCloud?</translation>
    </message>
    <message>
        <location filename="../../qml/popups/PopupMenuMessageDetail.qml" line="131"/>
        <location filename="../../qml/popups/PopupMenuMessageDetail.qml" line="131"/>
        <source>Delete Message</source>
        <translation>Видалити повідомлення</translation>
    </message>
    <message>
        <location filename="../../qml/popups/PopupMenuMessageDetail.qml" line="132"/>
        <location filename="../../qml/popups/PopupMenuMessageDetail.qml" line="132"/>
        <source>Delete Message: %1</source>
        <translation>Видалити повідомлення: %1</translation>
    </message>
    <message>
        <location filename="../../qml/popups/PopupMenuMessageDetail.qml" line="133"/>
        <location filename="../../qml/popups/PopupMenuMessageDetail.qml" line="133"/>
        <source>Do you want to delete the message &apos;%1&apos;?</source>
        <translation>Бажаєте видалити повідомлення &quot;%1&quot;?</translation>
    </message>
    <message>
        <location filename="../../qml/popups/PopupMenuMessageDetail.qml" line="134"/>
        <location filename="../../qml/popups/PopupMenuMessageDetail.qml" line="134"/>
        <source>Note: It will delete all attachments and message information from the local database.</source>
        <translation>Примітка: це видалить усі вкладення та інформацію про повідомлення з локальної бази даних.</translation>
    </message>
</context>
<context>
    <name>PopupMenuMessageList</name>
    <message>
        <location filename="../../qml/popups/PopupMenuMessageList.qml" line="41"/>
        <location filename="../../qml/popups/PopupMenuMessageList.qml" line="41"/>
        <source>Synchronise All</source>
        <translation>Синхронізувати усі</translation>
    </message>
    <message>
        <location filename="../../qml/popups/PopupMenuMessageList.qml" line="46"/>
        <location filename="../../qml/popups/PopupMenuMessageList.qml" line="46"/>
        <source>Mark All as Read</source>
        <translation>Позначити все як прочитане</translation>
    </message>
    <message>
        <location filename="../../qml/popups/PopupMenuMessageList.qml" line="51"/>
        <location filename="../../qml/popups/PopupMenuMessageList.qml" line="51"/>
        <source>Mark All as Unread</source>
        <translation>Позначити все як непрочитане</translation>
    </message>
</context>
<context>
    <name>PrefListModel</name>
    <message>
        <location filename="../../src/models/prefs_model.cpp" line="590"/>
        <source>default</source>
        <translation>за замовчуванням</translation>
    </message>
    <message>
        <location filename="../../src/models/prefs_model.cpp" line="593"/>
        <source>modified</source>
        <translation>змінений</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../../src/auxiliaries/email_helper.cpp" line="110"/>
        <source>ID</source>
        <translation>ID</translation>
    </message>
    <message>
        <location filename="../../src/auxiliaries/email_helper.cpp" line="112"/>
        <source>FROM</source>
        <translation>ВІД</translation>
    </message>
    <message>
        <location filename="../../src/auxiliaries/email_helper.cpp" line="114"/>
        <source>TO</source>
        <translation>КОМУ</translation>
    </message>
    <message>
        <location filename="../../src/auxiliaries/email_helper.cpp" line="116"/>
        <source>DELIVERED</source>
        <translation>ДОСТАВЛЕНО</translation>
    </message>
    <message>
        <location filename="../../src/auxiliaries/email_helper.cpp" line="118"/>
        <source>This email has been generated with Datovka application based on a data message (%1) delivered to databox.</source>
        <translation>Цей електронний лист було створено за допомогою програми Datovka на основі повідомлення даних (%1), доставленого до датової скриньки.</translation>
    </message>
    <message>
        <location filename="../../src/main.cpp" line="259"/>
        <source>Data box application</source>
        <translation>Додаток для датової скриньки</translation>
    </message>
    <message>
        <location filename="../../src/main.cpp" line="264"/>
        <source>ZFO file to be viewed.</source>
        <translation>Файл ZFO для перегляду.</translation>
    </message>
    <message>
        <location filename="../../src/wrap_records_management.cpp" line="346"/>
        <source>Message &apos;%1&apos; could not be uploaded.</source>
        <translation>Не вдалося завантажити повідомлення &quot;%1&quot;.</translation>
    </message>
    <message>
        <location filename="../../src/wrap_records_management.cpp" line="340"/>
        <source>Received error</source>
        <translation>Отримана помилка</translation>
    </message>
    <message>
        <location filename="../../src/wrap_records_management.cpp" line="345"/>
        <source>File Upload Error</source>
        <translation>Помилка завантаження файлу</translation>
    </message>
    <message>
        <location filename="../../src/wrap_records_management.cpp" line="352"/>
        <source>Successful File Upload</source>
        <translation>Успішне завантаження файлу</translation>
    </message>
    <message>
        <location filename="../../src/wrap_records_management.cpp" line="353"/>
        <source>Message &apos;%1&apos; was successfully uploaded into the records management service.</source>
        <translation>Повідомлення &quot;%1&quot; було успішно завантажено в службу керування записами.</translation>
    </message>
    <message>
        <location filename="../../src/wrap_records_management.cpp" line="354"/>
        <source>It can be now found in the records management service in these locations:</source>
        <translation>Тепер його можна знайти в службі управління записами в таких місцях:</translation>
    </message>
    <message>
        <location filename="../../src/sqlite/account_db.cpp" line="753"/>
        <source>Password of username &apos;%1&apos; expires on %2.</source>
        <translation>Термін дії пароля імені користувача &apos;%1&apos; закінчується %2.</translation>
    </message>
    <message>
        <location filename="../../src/sqlite/account_db.cpp" line="755"/>
        <source>Password of username &apos;%1&apos; expired on %2.</source>
        <translation>Термін дії пароля імені користувача «%1» закінчився %2.</translation>
    </message>
    <message>
        <location filename="../../src/worker/task_import_zfo.cpp" line="95"/>
        <source>Cannot open ZFO file &apos;%1&apos;.</source>
        <translation>Не вдається відкрити файл ZFO &quot;%1&quot;.</translation>
    </message>
    <message>
        <location filename="../../src/worker/task_import_zfo.cpp" line="103"/>
        <source>Wrong message format of ZFO file &apos;%1&apos;.</source>
        <translation>Неправильний формат повідомлення файлу ZFO &quot;%1&quot;.</translation>
    </message>
    <message>
        <location filename="../../src/worker/task_import_zfo.cpp" line="107"/>
        <source>Wrong message data of ZFO file &apos;%1&apos;.</source>
        <translation>Неправильні дані повідомлення файлу ZFO &quot;%1&quot;.</translation>
    </message>
    <message>
        <location filename="../../src/worker/task_import_zfo.cpp" line="128"/>
        <source>Relevant data box does not exist for ZFO file &apos;%1&apos;.</source>
        <translation>Відповідна скринька не існує для файлу ZFO &quot;%1&quot;.</translation>
    </message>
    <message>
        <location filename="../../src/worker/task_import_zfo.cpp" line="134"/>
        <source>Import internal error.</source>
        <translation>Внутрішня помилка імпорту.</translation>
    </message>
    <message>
        <location filename="../../src/worker/task_import_zfo.cpp" line="159"/>
        <source>Cannot authenticate the ZFO file &apos;%1&apos;.</source>
        <translation>Не вдається автентифікувати файл ZFO &quot;%1&quot;.</translation>
    </message>
    <message>
        <location filename="../../src/worker/task_import_zfo.cpp" line="164"/>
        <source>ZFO file &apos;%1&apos; is not authentic (returns ISDS)!</source>
        <translation>Файл ZFO &apos;%1&apos; не є автентичним (повертає ISDS)!</translation>
    </message>
    <message>
        <location filename="../../src/worker/task_import_zfo.cpp" line="175"/>
        <source>Cannot store the ZFO file &apos;%1&apos; to local database.</source>
        <translation>Неможливо зберегти файл ZFO &apos;%1&apos; у локальній базі даних.</translation>
    </message>
    <message>
        <location filename="../../src/worker/task_import_zfo.cpp" line="180"/>
        <source>ZFO file &apos;%1&apos; has been imported.</source>
        <translation>Файл ZFO &apos;%1&apos; імпортовано.</translation>
    </message>
    <message>
        <location filename="../../src/worker/task_import_zfo.cpp" line="188"/>
        <source>File has not been imported!</source>
        <translation>Файл не імпортовано!</translation>
    </message>
</context>
<context>
    <name>QuaGzipFile</name>
    <message>
        <location filename="../../src/datovka_shared/3rdparty/quazip-1.4/quazip/quagzipfile.cpp" line="60"/>
        <source>QIODevice::Append is not supported for GZIP</source>
        <translation>QIODevice::Append не підтримується для GZIP</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/3rdparty/quazip-1.4/quazip/quagzipfile.cpp" line="66"/>
        <source>Opening gzip for both reading and writing is not supported</source>
        <translation>Відкриття gzip як для читання, так і для запису не підтримується</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/3rdparty/quazip-1.4/quazip/quagzipfile.cpp" line="75"/>
        <source>You can open a gzip either for reading or for writing. Which is it?</source>
        <translation>Ви можете відкрити gzip як для читання, так і для запису. Що це буде?</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/3rdparty/quazip-1.4/quazip/quagzipfile.cpp" line="81"/>
        <source>Could not gzopen() file</source>
        <translation>Не вдалося провести gzopen() із файлом</translation>
    </message>
</context>
<context>
    <name>QuaZIODevice</name>
    <message>
        <location filename="../../src/datovka_shared/3rdparty/quazip-1.4/quazip/quaziodevice.cpp" line="178"/>
        <source>QIODevice::Append is not supported for QuaZIODevice</source>
        <translation>QIODevice::Append не підтримується для QuaZIODevice</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/3rdparty/quazip-1.4/quazip/quaziodevice.cpp" line="183"/>
        <source>QIODevice::ReadWrite is not supported for QuaZIODevice</source>
        <translation>QIODevice::ReadWrite не підтримується для QuaZIODevice</translation>
    </message>
</context>
<context>
    <name>QuaZipFile</name>
    <message>
        <location filename="../../src/datovka_shared/3rdparty/quazip-1.4/quazip/quazipfile.cpp" line="251"/>
        <source>ZIP/UNZIP API error %1</source>
        <translation>Помилка API ZIP/UNZIP %1</translation>
    </message>
</context>
<context>
    <name>SQLiteTbls</name>
    <message>
        <location filename="../../src/datovka_shared/io/prefs_db_tables.cpp" line="55"/>
        <source>Preference Name</source>
        <translation>Назва налаштування</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/io/prefs_db_tables.cpp" line="56"/>
        <source>Type</source>
        <translation>Тип</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/io/prefs_db_tables.cpp" line="57"/>
        <source>Value</source>
        <translation>Значення</translation>
    </message>
    <message>
        <location filename="../../src/sqlite/account_db_tables.cpp" line="77"/>
        <location filename="../../src/sqlite/account_db_tables.cpp" line="198"/>
        <source>Data box ID</source>
        <translation>ID скриньки</translation>
    </message>
    <message>
        <location filename="../../src/sqlite/account_db_tables.cpp" line="78"/>
        <source>Data box type</source>
        <translation>Тип датової скриньки</translation>
    </message>
    <message>
        <location filename="../../src/sqlite/account_db_tables.cpp" line="79"/>
        <location filename="../../src/sqlite/account_db_tables.cpp" line="158"/>
        <source>Identification number (IČO)</source>
        <translation>IČO</translation>
    </message>
    <message>
        <location filename="../../src/sqlite/account_db_tables.cpp" line="80"/>
        <location filename="../../src/sqlite/account_db_tables.cpp" line="147"/>
        <source>Given name</source>
        <translation>Ім&apos;я</translation>
    </message>
    <message>
        <location filename="../../src/sqlite/account_db_tables.cpp" line="81"/>
        <location filename="../../src/sqlite/account_db_tables.cpp" line="148"/>
        <source>Middle name</source>
        <translation>По батькові</translation>
    </message>
    <message>
        <location filename="../../src/sqlite/account_db_tables.cpp" line="82"/>
        <location filename="../../src/sqlite/account_db_tables.cpp" line="149"/>
        <source>Surname</source>
        <translation>Прізвище</translation>
    </message>
    <message>
        <location filename="../../src/sqlite/account_db_tables.cpp" line="83"/>
        <location filename="../../src/sqlite/account_db_tables.cpp" line="150"/>
        <source>Surname at birth</source>
        <translation>Прізвище при народженні</translation>
    </message>
    <message>
        <location filename="../../src/sqlite/account_db_tables.cpp" line="84"/>
        <location filename="../../src/sqlite/account_db_tables.cpp" line="159"/>
        <source>Company name</source>
        <translation>Назва компанії</translation>
    </message>
    <message>
        <location filename="../../src/sqlite/account_db_tables.cpp" line="85"/>
        <location filename="../../src/sqlite/account_db_tables.cpp" line="157"/>
        <source>Date of birth</source>
        <translation>Дата народження</translation>
    </message>
    <message>
        <location filename="../../src/sqlite/account_db_tables.cpp" line="86"/>
        <source>City of birth</source>
        <translation>Місто народження</translation>
    </message>
    <message>
        <location filename="../../src/sqlite/account_db_tables.cpp" line="87"/>
        <source>County of birth</source>
        <translation>Область народження</translation>
    </message>
    <message>
        <location filename="../../src/sqlite/account_db_tables.cpp" line="88"/>
        <source>State of birth</source>
        <translation>Країна народження</translation>
    </message>
    <message>
        <location filename="../../src/sqlite/account_db_tables.cpp" line="89"/>
        <location filename="../../src/sqlite/account_db_tables.cpp" line="161"/>
        <source>City of residence</source>
        <translation>Місце реєстрації компанії</translation>
    </message>
    <message>
        <location filename="../../src/sqlite/account_db_tables.cpp" line="90"/>
        <location filename="../../src/sqlite/account_db_tables.cpp" line="160"/>
        <source>Street of residence</source>
        <translation>Місце реєстрації компанії</translation>
    </message>
    <message>
        <location filename="../../src/sqlite/account_db_tables.cpp" line="91"/>
        <location filename="../../src/sqlite/account_db_tables.cpp" line="153"/>
        <source>Number in street</source>
        <translation>Номер на вулиці</translation>
    </message>
    <message>
        <location filename="../../src/sqlite/account_db_tables.cpp" line="92"/>
        <location filename="../../src/sqlite/account_db_tables.cpp" line="154"/>
        <source>Number in municipality</source>
        <translation>Номер у кадастрі</translation>
    </message>
    <message>
        <location filename="../../src/sqlite/account_db_tables.cpp" line="93"/>
        <location filename="../../src/sqlite/account_db_tables.cpp" line="155"/>
        <location filename="../../src/sqlite/account_db_tables.cpp" line="162"/>
        <source>Zip code</source>
        <translation>Поштовий індекс</translation>
    </message>
    <message>
        <location filename="../../src/sqlite/account_db_tables.cpp" line="94"/>
        <location filename="../../src/sqlite/account_db_tables.cpp" line="163"/>
        <source>State of residence</source>
        <translation>Країна проживання</translation>
    </message>
    <message>
        <location filename="../../src/sqlite/account_db_tables.cpp" line="95"/>
        <source>Nationality</source>
        <translation>Громадянство</translation>
    </message>
    <message>
        <location filename="../../src/sqlite/account_db_tables.cpp" line="98"/>
        <source>Databox state</source>
        <translation>Стан датовох скриньки</translation>
    </message>
    <message>
        <location filename="../../src/sqlite/account_db_tables.cpp" line="99"/>
        <source>Effective OVM</source>
        <translation>Ефективний OVM</translation>
    </message>
    <message>
        <location filename="../../src/sqlite/account_db_tables.cpp" line="100"/>
        <location filename="../../src/sqlite/account_db_tables.cpp" line="234"/>
        <source>Open addressing</source>
        <translation>Відкрита адресація</translation>
    </message>
    <message>
        <location filename="../../src/sqlite/account_db_tables.cpp" line="145"/>
        <source>User type</source>
        <translation>Тип користувача</translation>
    </message>
    <message>
        <location filename="../../src/sqlite/account_db_tables.cpp" line="146"/>
        <source>Permissions</source>
        <translation>Дозволи</translation>
    </message>
    <message>
        <location filename="../../src/sqlite/account_db_tables.cpp" line="151"/>
        <source>City</source>
        <translation>Місто</translation>
    </message>
    <message>
        <location filename="../../src/sqlite/account_db_tables.cpp" line="152"/>
        <source>Street</source>
        <translation>Вулиця</translation>
    </message>
    <message>
        <location filename="../../src/sqlite/account_db_tables.cpp" line="156"/>
        <source>State</source>
        <translation>Країна</translation>
    </message>
    <message>
        <location filename="../../src/sqlite/account_db_tables.cpp" line="164"/>
        <source>Password expiration</source>
        <translation>Термін дії пароля</translation>
    </message>
    <message>
        <location filename="../../src/sqlite/account_db_tables.cpp" line="199"/>
        <source>Long term storage type</source>
        <translation>Тип довгострокового зберігання</translation>
    </message>
    <message>
        <location filename="../../src/sqlite/account_db_tables.cpp" line="200"/>
        <source>Long term storage capacity</source>
        <translation>Ємність довгострокового зберігання</translation>
    </message>
    <message>
        <location filename="../../src/sqlite/account_db_tables.cpp" line="201"/>
        <location filename="../../src/sqlite/account_db_tables.cpp" line="206"/>
        <source>Active from</source>
        <translation>Активний від</translation>
    </message>
    <message>
        <location filename="../../src/sqlite/account_db_tables.cpp" line="202"/>
        <location filename="../../src/sqlite/account_db_tables.cpp" line="207"/>
        <source>Active to</source>
        <translation>Активний до</translation>
    </message>
    <message>
        <location filename="../../src/sqlite/account_db_tables.cpp" line="203"/>
        <source>Capacity used</source>
        <translation>Використана ємність</translation>
    </message>
    <message>
        <location filename="../../src/sqlite/account_db_tables.cpp" line="204"/>
        <source>Future long term storage type</source>
        <translation>Майбутній вид тривалого зберігання</translation>
    </message>
    <message>
        <location filename="../../src/sqlite/account_db_tables.cpp" line="205"/>
        <source>Future long term storage capacity</source>
        <translation>Ємність майбутнього довгострокового зберігання</translation>
    </message>
    <message>
        <location filename="../../src/sqlite/account_db_tables.cpp" line="208"/>
        <source>Payment state</source>
        <translation>Стан оплати</translation>
    </message>
    <message>
        <location filename="../../src/sqlite/account_db_tables.cpp" line="235"/>
        <source>Last synchronization time</source>
        <translation>Час останньої синхронізації</translation>
    </message>
    <message>
        <location filename="../../src/sqlite/file_db_tables.cpp" line="67"/>
        <location filename="../../src/sqlite/message_db_tables.cpp" line="162"/>
        <source>File name</source>
        <translation>Ім&apos;я файлу</translation>
    </message>
    <message>
        <location filename="../../src/sqlite/file_db_tables.cpp" line="70"/>
        <location filename="../../src/sqlite/message_db_tables.cpp" line="165"/>
        <source>Mime type</source>
        <translation>Тип MIME</translation>
    </message>
    <message>
        <location filename="../../src/sqlite/message_db_tables.cpp" line="91"/>
        <source>ID</source>
        <translation>ID</translation>
    </message>
    <message>
        <location filename="../../src/sqlite/message_db_tables.cpp" line="95"/>
        <source>Sender</source>
        <translation>Відправник</translation>
    </message>
    <message>
        <location filename="../../src/sqlite/message_db_tables.cpp" line="96"/>
        <source>Sender address</source>
        <translation>Адреса відправника</translation>
    </message>
    <message>
        <location filename="../../src/sqlite/message_db_tables.cpp" line="98"/>
        <source>Recipient</source>
        <translation>Отримувач</translation>
    </message>
    <message>
        <location filename="../../src/sqlite/message_db_tables.cpp" line="99"/>
        <source>Recipient address</source>
        <translation>Адреса одержувача</translation>
    </message>
    <message>
        <location filename="../../src/sqlite/message_db_tables.cpp" line="106"/>
        <source>To hands</source>
        <translation>До рук</translation>
    </message>
    <message>
        <location filename="../../src/sqlite/message_db_tables.cpp" line="107"/>
        <source>Subject</source>
        <translation>Тема</translation>
    </message>
    <message>
        <location filename="../../src/sqlite/message_db_tables.cpp" line="108"/>
        <source>Your reference number</source>
        <translation>Ваш довідковий номер</translation>
    </message>
    <message>
        <location filename="../../src/sqlite/message_db_tables.cpp" line="109"/>
        <source>Our reference number</source>
        <translation>Наш довідковий номер</translation>
    </message>
    <message>
        <location filename="../../src/sqlite/message_db_tables.cpp" line="110"/>
        <source>Your file mark</source>
        <translation>Позначка вашого файлу</translation>
    </message>
    <message>
        <location filename="../../src/sqlite/message_db_tables.cpp" line="111"/>
        <source>Our file mark</source>
        <translation>Позначка нашого файлу</translation>
    </message>
    <message>
        <location filename="../../src/sqlite/message_db_tables.cpp" line="112"/>
        <source>Law</source>
        <translation>Закон</translation>
    </message>
    <message>
        <location filename="../../src/sqlite/message_db_tables.cpp" line="113"/>
        <source>Year</source>
        <translation>Рік</translation>
    </message>
    <message>
        <location filename="../../src/sqlite/message_db_tables.cpp" line="114"/>
        <source>Section</source>
        <translation>Розділ</translation>
    </message>
    <message>
        <location filename="../../src/sqlite/message_db_tables.cpp" line="115"/>
        <source>Paragraph</source>
        <translation>Абзац</translation>
    </message>
    <message>
        <location filename="../../src/sqlite/message_db_tables.cpp" line="116"/>
        <source>Letter</source>
        <translation>Буква</translation>
    </message>
    <message>
        <location filename="../../src/sqlite/message_db_tables.cpp" line="120"/>
        <source>Delivered</source>
        <translation>Час доставки</translation>
    </message>
    <message>
        <location filename="../../src/sqlite/message_db_tables.cpp" line="121"/>
        <source>Accepted</source>
        <translation>Прийнято</translation>
    </message>
    <message>
        <location filename="../../src/sqlite/message_db_tables.cpp" line="122"/>
        <source>Status</source>
        <translation>Стан</translation>
    </message>
    <message>
        <location filename="../../src/sqlite/message_db_tables.cpp" line="123"/>
        <source>Attachment size</source>
        <translation>Розмір вкладень</translation>
    </message>
    <message>
        <location filename="../../src/sqlite/message_db_tables.cpp" line="374"/>
        <source>Read locally</source>
        <translation>Прочитано локально</translation>
    </message>
</context>
<context>
    <name>Service</name>
    <message>
        <location filename="../../src/datovka_shared/gov_services/service/gov_service.cpp" line="170"/>
        <source>Cannot access date field.</source>
        <translation>Не вдається отримати дату.</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/gov_services/service/gov_service.cpp" line="179"/>
        <source>The field &apos;%1&apos; contains an invalid date &apos;%2&apos;.</source>
        <translation>Поле &apos;%1&apos; містить недійсну дату &apos;%2&apos;.</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/gov_services/service/gov_service.cpp" line="199"/>
        <source>Cannot access IČO field.</source>
        <translation>Не вдається отримати IČO.</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/gov_services/service/gov_service.cpp" line="208"/>
        <source>The field &apos;%1&apos; contains an invalid value &apos;%2&apos;.</source>
        <translation>Поле &apos;%1&apos; містить недійсне значення &apos;%2&apos;.</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/gov_services/service/gov_service.cpp" line="228"/>
        <location filename="../../src/datovka_shared/gov_services/service/gov_service.cpp" line="256"/>
        <source>Cannot access string.</source>
        <translation>Не вдається отримати доступ до рядка.</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/gov_services/service/gov_service.cpp" line="236"/>
        <location filename="../../src/datovka_shared/gov_services/service/gov_service.cpp" line="264"/>
        <source>The field &apos;%1&apos; contains no value.</source>
        <translation>Поле &apos;%1&apos; не містить значення.</translation>
    </message>
</context>
<context>
    <name>Session</name>
    <message>
        <location filename="../../src/isds/session/isds_session.cpp" line="217"/>
        <source>Cannot open certificate &apos;%1&apos;.</source>
        <translation>Не вдається відкрити сертифікат &quot;%1&quot;.</translation>
    </message>
    <message>
        <location filename="../../src/isds/session/isds_session.cpp" line="243"/>
        <source>Cannot parse certificate &apos;%1&apos;.</source>
        <translation>Не вдається завантажити сертифікат &quot;%1&quot;.</translation>
    </message>
    <message>
        <location filename="../../src/isds/session/isds_session.cpp" line="251"/>
        <source>Unknown format of certificate &apos;%1&apos;.</source>
        <translation>Невідомий формат сертифіката &quot;%1&quot;.</translation>
    </message>
</context>
<context>
    <name>ShowPasswordOverlaidImage</name>
    <message>
        <location filename="../../qml/components/ShowPasswordOverlaidImage.qml" line="89"/>
        <location filename="../../qml/components/ShowPasswordOverlaidImage.qml" line="89"/>
        <source>Hide password</source>
        <translation>Приховати пароль</translation>
    </message>
    <message>
        <location filename="../../qml/components/ShowPasswordOverlaidImage.qml" line="89"/>
        <location filename="../../qml/components/ShowPasswordOverlaidImage.qml" line="89"/>
        <source>Show password</source>
        <translation>Показати пароль</translation>
    </message>
</context>
<context>
    <name>SrvcMvCrrVbh</name>
    <message>
        <location filename="../../src/datovka_shared/gov_services/service/gov_mv_crr_vbh.cpp" line="152"/>
        <source>Printout from the driver penalty point system</source>
        <translation>Витяг балів з центрального реєстру водіїв</translation>
    </message>
</context>
<context>
    <name>SrvcMvIrVp</name>
    <message>
        <location filename="../../src/datovka_shared/gov_services/service/gov_mv_ir_vp.cpp" line="545"/>
        <source>Printout from the insolvency register</source>
        <translation>Витяг з реєстру неплатоспроможності</translation>
    </message>
</context>
<context>
    <name>SrvcMvRtVt</name>
    <message>
        <location filename="../../src/datovka_shared/gov_services/service/gov_mv_rt_vt.cpp" line="175"/>
        <source>Printout from the criminal records</source>
        <translation>Свідотство про несудимість</translation>
    </message>
</context>
<context>
    <name>SrvcMvRtpoVt</name>
    <message>
        <location filename="../../src/datovka_shared/gov_services/service/gov_mv_rtpo_vt.cpp" line="550"/>
        <source>Printout from the criminal records of legal persons</source>
        <translation>Витяг з судимості юридичних осіб</translation>
    </message>
</context>
<context>
    <name>SrvcMvSkdVp</name>
    <message>
        <location filename="../../src/datovka_shared/gov_services/service/gov_mv_skd_vp.cpp" line="546"/>
        <source>Printout from the list of qualified suppliers</source>
        <translation>Витяг зі списку кваліфікованих постачальників</translation>
    </message>
</context>
<context>
    <name>SrvcMvVrVp</name>
    <message>
        <location filename="../../src/datovka_shared/gov_services/service/gov_mv_vr_vp.cpp" line="579"/>
        <source>Printout from the public register</source>
        <translation>Витяг з державного реєстру</translation>
    </message>
</context>
<context>
    <name>SrvcMvZrVp</name>
    <message>
        <location filename="../../src/datovka_shared/gov_services/service/gov_mv_zr_vp.cpp" line="545"/>
        <source>Printout from the company register</source>
        <translation>Витяг з торгового реєстру</translation>
    </message>
</context>
<context>
    <name>SrvcSzrRobVu</name>
    <message>
        <location filename="../../src/datovka_shared/gov_services/service/gov_szr_rob_vu.cpp" line="94"/>
        <source>Printout from the resident register</source>
        <translation>Витяг з реєстру населення</translation>
    </message>
</context>
<context>
    <name>SrvcSzrRobVvu</name>
    <message>
        <location filename="../../src/datovka_shared/gov_services/service/gov_szr_rob_vvu.cpp" line="134"/>
        <source>Printout about the usage of entries from the resident register</source>
        <translation>Роздруківка про використання записів з реєстру резидентів</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/gov_services/service/gov_szr_rob_vvu.cpp" line="234"/>
        <source>The date of start cannot be later than the date of end.</source>
        <translation>Дата початку не може бути пізнішою за дату закінчення.</translation>
    </message>
</context>
<context>
    <name>SrvcSzrRosVv</name>
    <message>
        <location filename="../../src/datovka_shared/gov_services/service/gov_szr_ros_vv.cpp" line="131"/>
        <source>Public printout from the person register</source>
        <translation>Публічний витяг з реєстру осіб</translation>
    </message>
</context>
<context>
    <name>SzrRobVvuData</name>
    <message>
        <location filename="../../src/datovka_shared/gov_services/service/gov_szr_rob_vvu.cpp" line="93"/>
        <source>From</source>
        <translation>Від</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/gov_services/service/gov_szr_rob_vvu.cpp" line="94"/>
        <source>Select start date</source>
        <translation>Виберіть дату початку</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/gov_services/service/gov_szr_rob_vvu.cpp" line="104"/>
        <source>To</source>
        <translation>До</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/gov_services/service/gov_szr_rob_vvu.cpp" line="105"/>
        <source>Select end date</source>
        <translation>Виберіть дату завершення</translation>
    </message>
</context>
<context>
    <name>SzrRosVvData</name>
    <message>
        <location filename="../../src/datovka_shared/gov_services/service/gov_szr_ros_vv.cpp" line="102"/>
        <source>Identification number (IČO)</source>
        <translation>IČO</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/gov_services/service/gov_szr_ros_vv.cpp" line="103"/>
        <source>Enter identification number (IČO)</source>
        <translation>Введіть ідентифікаційний номер (IČO)</translation>
    </message>
</context>
<context>
    <name>Tasks</name>
    <message>
        <location filename="../../src/isds/isds_tasks.cpp" line="79"/>
        <source>Unknown error</source>
        <translation>Невідома помилка</translation>
    </message>
    <message>
        <location filename="../../src/isds/isds_tasks.cpp" line="329"/>
        <source>ZFO import is running... Wait until import will be finished.</source>
        <translation>Імпорт ZFO виконується... Зачекайте, поки імпорт не буде завершено.</translation>
    </message>
</context>
<context>
    <name>TextLineItem</name>
    <message>
        <location filename="../../qml/components/TextLineItem.qml" line="74"/>
        <location filename="../../qml/components/TextLineItem.qml" line="74"/>
        <source>Input text action button.</source>
        <translation>Кнопка дії введення тексту.</translation>
    </message>
</context>
<context>
    <name>TimedPasswordLine</name>
    <message>
        <location filename="../../qml/components/TimedPasswordLine.qml" line="37"/>
        <location filename="../../qml/components/TimedPasswordLine.qml" line="37"/>
        <source>Enter the password</source>
        <translation>Введіть пароль</translation>
    </message>
</context>
<context>
    <name>Utility</name>
    <message>
        <location filename="../../src/datovka_shared/utility/strings.cpp" line="59"/>
        <source>unknown</source>
        <translation>невідомий/а</translation>
    </message>
</context>
<context>
    <name>YesNoDialogue</name>
    <message>
        <location filename="../../qml/dialogues/YesNoDialogue.qml" line="107"/>
        <location filename="../../qml/dialogues/YesNoDialogue.qml" line="107"/>
        <source>Yes</source>
        <translation>Так</translation>
    </message>
    <message>
        <location filename="../../qml/dialogues/YesNoDialogue.qml" line="117"/>
        <location filename="../../qml/dialogues/YesNoDialogue.qml" line="117"/>
        <source>No</source>
        <translation>Ні</translation>
    </message>
</context>
<context>
    <name>main</name>
    <message>
        <location filename="../../qml/main.qml" line="316"/>
        <location filename="../../qml/main.qml" line="316"/>
        <source>Mobile Key Login</source>
        <translation>Увійти через Mobilní Klíč</translation>
    </message>
    <message>
        <location filename="../../qml/main.qml" line="316"/>
        <location filename="../../qml/main.qml" line="316"/>
        <source>Waiting for acknowledgement from the Mobile key application for the data box &apos;%1&apos;.</source>
        <translation>Очікуємо підтвердження від Mobile Key для скриньки &quot;%1&quot;.</translation>
    </message>
    <message>
        <location filename="../../qml/main.qml" line="316"/>
        <location filename="../../qml/main.qml" line="316"/>
        <source>Cancel</source>
        <translation>Скасувати</translation>
    </message>
    <message>
        <location filename="../../qml/main.qml" line="388"/>
        <location filename="../../qml/main.qml" line="393"/>
        <location filename="../../qml/main.qml" line="404"/>
        <location filename="../../qml/main.qml" line="388"/>
        <location filename="../../qml/main.qml" line="393"/>
        <location filename="../../qml/main.qml" line="404"/>
        <source>Enter PIN code</source>
        <translation>Введіть PIN-код</translation>
    </message>
    <message>
        <location filename="../../qml/main.qml" line="411"/>
        <location filename="../../qml/main.qml" line="411"/>
        <source>Verify PIN code.</source>
        <translation>Верифікація PIN-коду.</translation>
    </message>
    <message>
        <location filename="../../qml/main.qml" line="455"/>
        <location filename="../../qml/main.qml" line="455"/>
        <source>Verifying PIN...</source>
        <translation>Верифікація PIN-коду...</translation>
    </message>
    <message>
        <location filename="../../qml/main.qml" line="500"/>
        <location filename="../../qml/main.qml" line="500"/>
        <source>Password Expiration</source>
        <translation>Термін дії пароля</translation>
    </message>
    <message>
        <location filename="../../qml/main.qml" line="501"/>
        <location filename="../../qml/main.qml" line="501"/>
        <source>Passwords of users listed below have expired or are going to expire within few days. You may change the passwords from this application if they haven&apos;t already expired. Expired passwords can only be changed in the ISDS web portal.</source>
        <translation>Термін дії паролів користувачів, перерахованих нижче, закінчився або закінчиться протягом декількох днів. Ви можете змінити паролі з цієї програми, якщо їх термін дії ще не закінчився. Прострочені паролі можна змінити лише на веб-порталі ISDS.</translation>
    </message>
    <message>
        <location filename="../../qml/main.qml" line="509"/>
        <location filename="../../qml/main.qml" line="509"/>
        <source>Donation Request</source>
        <translation>Запит на пожертву</translation>
    </message>
    <message>
        <location filename="../../qml/main.qml" line="510"/>
        <location filename="../../qml/main.qml" line="510"/>
        <source>Thank you for using our application.</source>
        <translation>Дякуємо за використання нашого додатку.</translation>
    </message>
    <message>
        <location filename="../../qml/main.qml" line="511"/>
        <location filename="../../qml/main.qml" line="511"/>
        <source>Please consider giving a donation to support the development of this application.</source>
        <translation>Будь ласка, розгляньте можливість зробити пожертву для підтримки розвитку цього додатку.</translation>
    </message>
    <message>
        <location filename="../../qml/main.qml" line="512"/>
        <location filename="../../qml/main.qml" line="512"/>
        <source>Donate</source>
        <translation>Пожертвуйте</translation>
    </message>
    <message>
        <location filename="../../qml/main.qml" line="524"/>
        <location filename="../../qml/main.qml" line="524"/>
        <source>Cannot Write Configuration File</source>
        <translation>Не вдалося записати файл конфігурації</translation>
    </message>
    <message>
        <location filename="../../qml/main.qml" line="525"/>
        <location filename="../../qml/main.qml" line="525"/>
        <source>An error occurred while attempting to save the configuration.</source>
        <translation>Виникла помилка при спробі зберегти конфігурацію.</translation>
    </message>
    <message>
        <location filename="../../qml/main.qml" line="526"/>
        <location filename="../../qml/main.qml" line="526"/>
        <source>Cannot write file &apos;%1&apos;.</source>
        <translation>Не вдалося записати файл &quot;%1&quot;.</translation>
    </message>
    <message>
        <location filename="../../qml/main.qml" line="438"/>
        <location filename="../../qml/main.qml" line="438"/>
        <source>Wrong PIN code.</source>
        <translation>Неправильний PIN-код.</translation>
    </message>
    <message>
        <location filename="../../qml/main.qml" line="190"/>
        <location filename="../../qml/main.qml" line="190"/>
        <source>Select ZFO Message</source>
        <translation>Вибрати повідомлення ZFO</translation>
    </message>
    <message>
        <location filename="../../qml/main.qml" line="410"/>
        <location filename="../../qml/main.qml" line="410"/>
        <source>Enter</source>
        <translation>Увійти</translation>
    </message>
    <message>
        <location filename="../../qml/main.qml" line="427"/>
        <location filename="../../qml/main.qml" line="427"/>
        <source>Version</source>
        <translation>Версія</translation>
    </message>
</context>
</TS>
