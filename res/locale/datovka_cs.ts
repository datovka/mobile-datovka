<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="cs_CZ">
<context>
    <name>AccessibleSpinBox</name>
    <message>
        <location filename="../../qml/components/AccessibleSpinBox.qml" line="69"/>
        <location filename="../../qml/components/AccessibleSpinBox.qml" line="69"/>
        <source>Decrease value &apos;%1&apos;.</source>
        <translation>Snížit hodnotu &apos;%1&apos;.</translation>
    </message>
    <message>
        <location filename="../../qml/components/AccessibleSpinBox.qml" line="102"/>
        <location filename="../../qml/components/AccessibleSpinBox.qml" line="102"/>
        <source>Increase value &apos;%1&apos;.</source>
        <translation>Zvýšit hodnotu &apos;%1&apos;.</translation>
    </message>
</context>
<context>
    <name>AccessibleSpinBoxZeroMax</name>
    <message>
        <location filename="../../qml/components/AccessibleSpinBoxZeroMax.qml" line="32"/>
        <location filename="../../qml/components/AccessibleSpinBoxZeroMax.qml" line="32"/>
        <source>max</source>
        <translation>max</translation>
    </message>
    <message>
        <location filename="../../qml/components/AccessibleSpinBoxZeroMax.qml" line="92"/>
        <location filename="../../qml/components/AccessibleSpinBoxZeroMax.qml" line="92"/>
        <source>Decrease value &apos;%1&apos;.</source>
        <translation>Snížit hodnotu &apos;%1&apos;.</translation>
    </message>
    <message>
        <location filename="../../qml/components/AccessibleSpinBoxZeroMax.qml" line="125"/>
        <location filename="../../qml/components/AccessibleSpinBoxZeroMax.qml" line="125"/>
        <source>Increase value &apos;%1&apos;.</source>
        <translation>Zvýšit hodnotu &apos;%1&apos;.</translation>
    </message>
</context>
<context>
    <name>AccessibleTextHelp</name>
    <message>
        <location filename="../../qml/components/AccessibleTextHelp.qml" line="89"/>
        <location filename="../../qml/components/AccessibleTextHelp.qml" line="89"/>
        <source>Help</source>
        <translation>Nápověda</translation>
    </message>
</context>
<context>
    <name>AccountList</name>
    <message>
        <location filename="../../qml/components/AccountList.qml" line="85"/>
        <location filename="../../qml/components/AccountList.qml" line="85"/>
        <source>Show messages of data box &apos;%1&apos;. New or unread %2 of %3 received messages.</source>
        <translation>Zobrazit zprávy schránky &apos;%1&apos;. Nových či nepřečtených je %2 z %3 přijatých zpráv.</translation>
    </message>
    <message>
        <location filename="../../qml/components/AccountList.qml" line="141"/>
        <location filename="../../qml/components/AccountList.qml" line="141"/>
        <source>New messages: %1</source>
        <translation>Nových zpráv: %1</translation>
    </message>
    <message>
        <location filename="../../qml/components/AccountList.qml" line="158"/>
        <location filename="../../qml/components/AccountList.qml" line="158"/>
        <source>Last synchronisation</source>
        <translation>Poslední synchronizace</translation>
    </message>
    <message>
        <location filename="../../qml/components/AccountList.qml" line="167"/>
        <location filename="../../qml/components/AccountList.qml" line="167"/>
        <source>Received</source>
        <translation>Přijaté</translation>
    </message>
    <message>
        <location filename="../../qml/components/AccountList.qml" line="167"/>
        <location filename="../../qml/components/AccountList.qml" line="167"/>
        <source>Sent</source>
        <translation>Odeslané</translation>
    </message>
    <message>
        <location filename="../../qml/components/AccountList.qml" line="175"/>
        <location filename="../../qml/components/AccountList.qml" line="175"/>
        <source>Synchronise data box &apos;%1&apos;.</source>
        <translation>Synchronizovat schránku &quot;%1&quot;.</translation>
    </message>
    <message>
        <location filename="../../qml/components/AccountList.qml" line="191"/>
        <location filename="../../qml/components/AccountList.qml" line="191"/>
        <source>Create and send new message from data box &apos;%1&apos;.</source>
        <translation>Vytvořit a odeslat novou zprávu ze schránky &quot;%1&quot;.</translation>
    </message>
    <message>
        <location filename="../../qml/components/AccountList.qml" line="197"/>
        <location filename="../../qml/components/AccountList.qml" line="197"/>
        <source>Data-box properties &apos;%1&apos;.</source>
        <translation>Vlastnosti schránky &quot;%1&quot;.</translation>
    </message>
</context>
<context>
    <name>Accounts</name>
    <message>
        <location filename="../../src/wrap_accounts.cpp" line="261"/>
        <source>Account with username &apos;%1&apos; already exists.</source>
        <translation>Účet s uživatelským jménem &apos;%1&apos; již existuje.</translation>
    </message>
    <message>
        <source>Cannot access data-box model.</source>
        <translation type="vanished">Nelze získat data ze seznamu schránek.</translation>
    </message>
    <message>
        <location filename="../../src/wrap_accounts.cpp" line="237"/>
        <source>Cannot access data-box data.</source>
        <translation>Nelze získat data ze seznamu schránek.</translation>
    </message>
    <message>
        <location filename="../../src/wrap_accounts.cpp" line="243"/>
        <source>Username, communication code or data-box name has not been specified. These fields must be filled in.</source>
        <translation>Uživatelské jméno, komunikační kód nebo jméno schránky nebylo uvedeno. Tyto položky musí být vyplněny.</translation>
    </message>
    <message>
        <location filename="../../src/wrap_accounts.cpp" line="247"/>
        <source>Username, password or data-box name has not been specified. These fields must be filled in.</source>
        <translation>Uživatelské jméno, heslo nebo jméno schránky nebylo uvedeno. Tyto položky musí být vyplněny.</translation>
    </message>
    <message>
        <location filename="../../src/wrap_accounts.cpp" line="514"/>
        <source>Cannot access file database for the username &apos;%1&apos;.</source>
        <translation>Nelze otevřít databázi souborů pro uživatelské jméno &apos;%1&apos;.</translation>
    </message>
    <message>
        <location filename="../../src/wrap_accounts.cpp" line="523"/>
        <source>Cannot change file database to username &apos;%1&apos;.</source>
        <translation>Nelze změnit databázi souborů na uživatelské jméno &apos;%1&apos;.</translation>
    </message>
    <message>
        <location filename="../../src/wrap_accounts.cpp" line="550"/>
        <source>Cannot access message database for username &apos;%1&apos;.</source>
        <translation>Nelze otevřít databázi zpráv pro uživatelské jméno &apos;%1&apos;.</translation>
    </message>
    <message>
        <location filename="../../src/wrap_accounts.cpp" line="559"/>
        <source>Cannot change message database to username &apos;%1&apos;.</source>
        <translation>Nelze změnit databázi zpráv na uživatelské jméno &apos;%1&apos;.</translation>
    </message>
    <message>
        <location filename="../../src/wrap_accounts.cpp" line="583"/>
        <source>Internal error.</source>
        <translation>Interní chyba.</translation>
    </message>
    <message>
        <location filename="../../src/wrap_accounts.cpp" line="589"/>
        <source>Data-box identifier related to the new username &apos;%1&apos; doesn&apos;t correspond with the data-box identifier related to the old username &apos;%2&apos;.</source>
        <translation>Identifikátor schránky pro nové uživatelské jméno &quot;%1&quot; neodpovídá identifikátoru schránky starého uživatelského jména &quot;%2&quot;.</translation>
    </message>
    <message>
        <location filename="../../src/wrap_accounts.cpp" line="595"/>
        <source>Cannot change the file database to match the new username &apos;%1&apos;.</source>
        <translation>Nelze změnit databázi souborů na nové uživatelské jméno &quot;%1&quot;.</translation>
    </message>
    <message>
        <location filename="../../src/wrap_accounts.cpp" line="602"/>
        <source>Cannot change the message database to match the new username &apos;%1&apos;.</source>
        <translation>Nelze změnit databázi zpráv na nové uživatelské jméno &quot;%1&quot;.</translation>
    </message>
</context>
<context>
    <name>AppVersionInfo</name>
    <message>
        <location filename="../../src/app_version_info.cpp" line="35"/>
        <source>Displaying release news in the application.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>BackupRestoreZipData</name>
    <message>
        <location filename="../../src/backup_zip.cpp" line="95"/>
        <source>unknown</source>
        <translation>neznámý/á</translation>
    </message>
    <message>
        <location filename="../../src/backup_zip.cpp" line="581"/>
        <source>From backup</source>
        <translation>Ze zálohy</translation>
    </message>
    <message>
        <location filename="../../src/backup_zip.cpp" line="706"/>
        <source>Cannot open or read file &apos;%1&apos;.</source>
        <translation>Nelze otevřít nebo číst soubor &quot;%1&quot;.</translation>
    </message>
    <message>
        <location filename="../../src/backup_zip.cpp" line="720"/>
        <source>Select accounts from the backup ZIP file which you want to restore. Data of existing accounts will be replaced by data from the backup.</source>
        <translation>Vyberte účty, které chcete obnovit ze ZIP souboru obsahujícího popis zálohy. Data existujících účtů budou nahrazena daty ze zálohy.</translation>
    </message>
    <message>
        <location filename="../../src/backup_zip.cpp" line="818"/>
        <location filename="../../src/backup_zip.cpp" line="936"/>
        <location filename="../../src/backup_zip.cpp" line="1573"/>
        <source>There is not enough space in the selected storage.</source>
        <translation>Na zvoleném úložišti není dostatek volného místa.</translation>
    </message>
    <message>
        <location filename="../../src/backup_zip.cpp" line="896"/>
        <source>JSON file &apos;%1&apos; does not contain valid application information.</source>
        <translation>JSON soubor &quot;%1&quot; neobsahuje platné informace o aplikaci.</translation>
    </message>
    <message>
        <location filename="../../src/backup_zip.cpp" line="924"/>
        <source>Transfer ZIP archive was taken at %1</source>
        <translation>Transferový ZIP archiv byl vytvořen %1</translation>
    </message>
    <message>
        <location filename="../../src/backup_zip.cpp" line="942"/>
        <source>Unknown backup type. JSON file contains no valid backup data.</source>
        <translation>Neznámý typ zálohy. Vybraný ZIP soubor neobsahuje platná data.</translation>
    </message>
    <message>
        <location filename="../../src/backup_zip.cpp" line="912"/>
        <source>Backup was taken at %1</source>
        <translation>Záloha byla provedena %1</translation>
    </message>
    <message numerus="yes">
        <location filename="../../src/backup_zip.cpp" line="913"/>
        <location filename="../../src/backup_zip.cpp" line="925"/>
        <source>and contains %n file(s).</source>
        <translation>
            <numerusform>a obsahuje %n soubor.</numerusform>
            <numerusform>a obsahuje %n soubory.</numerusform>
            <numerusform>a obsahuje %n souborů.</numerusform>
        </translation>
    </message>
    <message>
        <location filename="../../src/backup_zip.cpp" line="932"/>
        <source>Restore all application data from transfer ZIP file. The current application data will be complete rewritten with new data from transfer backup.</source>
        <translation>Obnoví kompletní data aplikace ze ZIP soboru. Současná data v aplikaci budou kompletně přepsána daty z přenosu.</translation>
    </message>
    <message>
        <location filename="../../src/backup_zip.cpp" line="841"/>
        <location filename="../../src/backup_zip.cpp" line="848"/>
        <source>The selected file isn&apos;t a valid ZIP archive containing a backup.</source>
        <translation>Vybraný soubor není validní ZIP archiv obsahující zálohu.</translation>
    </message>
    <message>
        <location filename="../../src/backup_zip.cpp" line="887"/>
        <source>The selected ZIP archive doesn&apos;t contain any JSON file.</source>
        <translation>Vybraný ZIP archiv neobsahuje žádný JSON soubor.</translation>
    </message>
    <message>
        <location filename="../../src/backup_zip.cpp" line="920"/>
        <source>Warning: Application version &apos;%1&apos; does not match the transfer version &apos;%2&apos;. Is is recommended to transfer data between same versions of the application to prevent incompatibility issues.</source>
        <translation>Upozornění: Verze aplikace &quot;%1&quot; neodpovídá verzi deklarované v transferu &quot;%2&quot;. Je doporučeno přenášet data mezi stejnými verzemi aplikace, aby se předešlo problémům s nekompatibilitou.</translation>
    </message>
    <message>
        <location filename="../../src/backup_zip.cpp" line="959"/>
        <location filename="../../src/backup_zip.cpp" line="1049"/>
        <source>Cannot open ZIP archive.</source>
        <translation>Nelze otevřít ZIP archiv.</translation>
    </message>
    <message>
        <location filename="../../src/backup_zip.cpp" line="990"/>
        <location filename="../../src/backup_zip.cpp" line="1119"/>
        <source>Restoring file &apos;%1&apos;.</source>
        <translation>Obnovuji soubor &quot;%1&quot;.</translation>
    </message>
    <message>
        <location filename="../../src/backup_zip.cpp" line="1024"/>
        <location filename="../../src/backup_zip.cpp" line="1158"/>
        <source>Restoration finished.</source>
        <translation>Obnovování dokončeno.</translation>
    </message>
    <message>
        <location filename="../../src/backup_zip.cpp" line="1239"/>
        <source>Transferring file &apos;%1&apos;.</source>
        <translation>Přenáším soubor &quot;%1&quot;.</translation>
    </message>
    <message>
        <location filename="../../src/backup_zip.cpp" line="1274"/>
        <source>Transfer finished.</source>
        <translation>Přenos dokončen.</translation>
    </message>
    <message>
        <location filename="../../src/backup_zip.cpp" line="1342"/>
        <location filename="../../src/backup_zip.cpp" line="1365"/>
        <location filename="../../src/backup_zip.cpp" line="1386"/>
        <location filename="../../src/backup_zip.cpp" line="1465"/>
        <location filename="../../src/backup_zip.cpp" line="1481"/>
        <source>Backing up file &apos;%1&apos;.</source>
        <translation>Zálohuji soubor &quot;%1&quot;.</translation>
    </message>
    <message>
        <location filename="../../src/backup_zip.cpp" line="1503"/>
        <source>Backup finished.</source>
        <translation>Záloha dokončena.</translation>
    </message>
    <message>
        <location filename="../../src/backup_zip.cpp" line="1583"/>
        <source>Required space</source>
        <translation>Vyžadované místo</translation>
    </message>
    <message>
        <location filename="../../src/backup_zip.cpp" line="1584"/>
        <source>Available space</source>
        <translation>Volné místo</translation>
    </message>
</context>
<context>
    <name>ChangeLogBox</name>
    <message>
        <location filename="../../qml/components/ChangeLogBox.qml" line="56"/>
        <location filename="../../qml/components/ChangeLogBox.qml" line="56"/>
        <source>Version</source>
        <translation>Verze</translation>
    </message>
    <message>
        <location filename="../../qml/components/ChangeLogBox.qml" line="63"/>
        <location filename="../../qml/components/ChangeLogBox.qml" line="63"/>
        <source>What&apos;s new?</source>
        <translation>Co je nového?</translation>
    </message>
</context>
<context>
    <name>Connection</name>
    <message>
        <location filename="../../src/isds/io/connection.cpp" line="695"/>
        <source>Success.</source>
        <translation>Úspěch.</translation>
    </message>
    <message>
        <location filename="../../src/isds/io/connection.cpp" line="698"/>
        <source>Successfully finished.</source>
        <translation>Úspěšně dokončeno.</translation>
    </message>
    <message>
        <location filename="../../src/isds/io/connection.cpp" line="701"/>
        <source>Internet connection is probably not available. Check your network settings.</source>
        <translation>Připojení k internetu pravděpodobně není k dispozici. Zkontrolujte nastavení sítě.</translation>
    </message>
    <message>
        <location filename="../../src/isds/io/connection.cpp" line="704"/>
        <source>Authorization failed. Server complains about a bad request.</source>
        <translation>Autorizace selhala. Server vrací upozornění na špatný požadavek.</translation>
    </message>
    <message>
        <location filename="../../src/isds/io/connection.cpp" line="707"/>
        <source>Error reply.</source>
        <translation>Chybná odezva.</translation>
    </message>
    <message>
        <location filename="../../src/isds/io/connection.cpp" line="710"/>
        <source>ISDS server is out of service. Scheduled maintenance in progress. Try again later.</source>
        <translation>Server ISDS je momentálně mimo provoz. Probíhá plánovaná údržba. Zkuste to později.</translation>
    </message>
    <message>
        <location filename="../../src/isds/io/connection.cpp" line="713"/>
        <source>Connection with ISDS server timed out. Request was cancelled.</source>
        <translation>Spojení se serverem ISDS vypršelo. Požadavek byl zrušen.</translation>
    </message>
    <message>
        <location filename="../../src/isds/io/connection.cpp" line="716"/>
        <source>Authorization failed. Check your credentials in the data-box settings whether they are correct and try again. It is also possible that your password expired. Check your credentials validity by logging in to the data box using the ISDS web portal.</source>
        <translation>Autorizace selhala. Zkontrolujte své přihlašovací údaje v nastavení schránky. Také je možné, že vypršela platnost hesla. Ověřte správnost Vašich přihlašovacích údajů přihlášením do webového portálu ISDS.</translation>
    </message>
    <message>
        <location filename="../../src/isds/io/connection.cpp" line="719"/>
        <source>OTP authorization failed. OTP code is wrong or expired.</source>
        <translation>OTP autorizace selhala. Kód OTP je neplatný nebo vypršel.</translation>
    </message>
    <message>
        <location filename="../../src/isds/io/connection.cpp" line="722"/>
        <source>SMS authorization failed. SMS code is wrong or expired.</source>
        <translation>SMS autorizace selhala. Kód SMS je neplatný nebo vypršel.</translation>
    </message>
    <message>
        <location filename="../../src/isds/io/connection.cpp" line="725"/>
        <source>SMS authorization failed. SMS code couldn&apos;t be sent. Your order on premium SMS has been exhausted or cancelled.</source>
        <translation>SMS autorizace selhala. SMS kód nemohl být odeslán. Vaše předplatné prémiových SMS bylo vyčerpáno nebo zrušeno.</translation>
    </message>
    <message>
        <location filename="../../src/isds/io/connection.cpp" line="729"/>
        <source>An communication error. See log for more detail.</source>
        <translation>Chyba komunikace. Více informací naleznete v logu aplikace.</translation>
    </message>
</context>
<context>
    <name>CreateAccountPage1</name>
    <message>
        <location filename="../../qml/wizards/CreateAccountPage1.qml" line="58"/>
        <location filename="../../qml/wizards/CreateAccountPage1.qml" line="58"/>
        <source>Add Data Box</source>
        <translation>Přidat schránku</translation>
    </message>
    <message>
        <location filename="../../qml/wizards/CreateAccountPage1.qml" line="84"/>
        <location filename="../../qml/wizards/CreateAccountPage1.qml" line="84"/>
        <source>The data-box title must be filled in.</source>
        <translation>Název schránky musí být vyplněn.</translation>
    </message>
    <message>
        <location filename="../../qml/wizards/CreateAccountPage1.qml" line="88"/>
        <location filename="../../qml/wizards/CreateAccountPage1.qml" line="88"/>
        <source>Data-Box Title</source>
        <translation>Pojmenování schránky</translation>
    </message>
    <message>
        <location filename="../../qml/wizards/CreateAccountPage1.qml" line="89"/>
        <location filename="../../qml/wizards/CreateAccountPage1.qml" line="89"/>
        <source>Enter custom data-box name.</source>
        <translation>Zadejte název schránky.</translation>
    </message>
    <message>
        <location filename="../../qml/wizards/CreateAccountPage1.qml" line="96"/>
        <location filename="../../qml/wizards/CreateAccountPage1.qml" line="96"/>
        <source>The data-box title is a user-specified name used for the identification of the data box in the application (e.g. &apos;My Personal Data Box&apos;, &apos;Firm Box&apos;, etc.). The chosen name serves only for your convenience. The entry must be filled in.</source>
        <translation>Pojmenování schránky je uživatelem zvolené jméno, které slouží k identifikaci schránky v této aplikaci (např. &quot;Moje osobní datová schránka&quot;, &quot;Firemní schránka&quot;, atd.). Zvolené jméno slouží jen pro Vaši lepší orientaci. Tato položka musí být vyplněna.</translation>
    </message>
    <message>
        <location filename="../../qml/wizards/CreateAccountPage1.qml" line="101"/>
        <location filename="../../qml/wizards/CreateAccountPage1.qml" line="101"/>
        <source>Data-Box Environment</source>
        <translation>Typ datové schránky</translation>
    </message>
    <message>
        <location filename="../../qml/wizards/CreateAccountPage1.qml" line="106"/>
        <location filename="../../qml/wizards/CreateAccountPage1.qml" line="106"/>
        <source>regular</source>
        <translation>běžná</translation>
    </message>
    <message>
        <location filename="../../qml/wizards/CreateAccountPage1.qml" line="111"/>
        <location filename="../../qml/wizards/CreateAccountPage1.qml" line="111"/>
        <source>test</source>
        <translation>testovací</translation>
    </message>
    <message>
        <location filename="../../qml/wizards/CreateAccountPage1.qml" line="116"/>
        <location filename="../../qml/wizards/CreateAccountPage1.qml" line="116"/>
        <source>The regular data box option is used to access the production (official) ISDS environment (&lt;a href=&quot;https://www.mojedatovaschranka.cz&quot;&gt;www.mojedatovaschranka.cz&lt;/a&gt;). Test data box is used to access the ISDS testing environment (&lt;a href=&quot;https://www.czebox.cz&quot;&gt;www.czebox.cz&lt;/a&gt;).</source>
        <translation>Běžná schránka slouží k přístupu do produkčního (oficiálního) prostředí ISDS (&lt;a href=&quot;https://www.mojedatovaschranka.cz&quot;&gt;www.mojedatovaschranka.cz&lt;/a&gt;). Testovací schránka slouží k přístupu do testovacího prostředí ISDS (&lt;a href=&quot;https://www.czebox.cz&quot;&gt;www.czebox.cz&lt;/a&gt;).</translation>
    </message>
    <message>
        <location filename="../../qml/wizards/CreateAccountPage1.qml" line="124"/>
        <location filename="../../qml/wizards/CreateAccountPage1.qml" line="124"/>
        <source>Next</source>
        <translation>Další</translation>
    </message>
    <message>
        <location filename="../../qml/wizards/CreateAccountPage1.qml" line="125"/>
        <location filename="../../qml/wizards/CreateAccountPage1.qml" line="125"/>
        <source>Next step</source>
        <translation>Další krok</translation>
    </message>
</context>
<context>
    <name>CreateAccountPage2</name>
    <message>
        <location filename="../../qml/wizards/CreateAccountPage2.qml" line="131"/>
        <location filename="../../qml/wizards/CreateAccountPage2.qml" line="131"/>
        <source>Next step</source>
        <translation>Další krok</translation>
    </message>
    <message>
        <location filename="../../qml/wizards/CreateAccountPage2.qml" line="50"/>
        <location filename="../../qml/wizards/CreateAccountPage2.qml" line="50"/>
        <source>Add Data Box</source>
        <translation>Přidat schránku</translation>
    </message>
    <message>
        <location filename="../../qml/wizards/CreateAccountPage2.qml" line="73"/>
        <location filename="../../qml/wizards/CreateAccountPage2.qml" line="73"/>
        <source>Data-box title</source>
        <translation>Pojmenování schránky</translation>
    </message>
    <message>
        <location filename="../../qml/wizards/CreateAccountPage2.qml" line="76"/>
        <location filename="../../qml/wizards/CreateAccountPage2.qml" line="76"/>
        <source>Data-box type</source>
        <translation>Typ datové schránky</translation>
    </message>
    <message>
        <location filename="../../qml/wizards/CreateAccountPage2.qml" line="83"/>
        <location filename="../../qml/wizards/CreateAccountPage2.qml" line="83"/>
        <source>Login Method</source>
        <translation>Způsob přihlašování</translation>
    </message>
    <message>
        <location filename="../../qml/wizards/CreateAccountPage2.qml" line="88"/>
        <location filename="../../qml/wizards/CreateAccountPage2.qml" line="88"/>
        <source>username + password</source>
        <translation>jméno + heslo</translation>
    </message>
    <message>
        <location filename="../../qml/wizards/CreateAccountPage2.qml" line="93"/>
        <location filename="../../qml/wizards/CreateAccountPage2.qml" line="93"/>
        <source>username + Mobile Key</source>
        <translation>jméno + Mobilní klíč</translation>
    </message>
    <message>
        <location filename="../../qml/wizards/CreateAccountPage2.qml" line="98"/>
        <location filename="../../qml/wizards/CreateAccountPage2.qml" line="98"/>
        <source>username + password + SMS</source>
        <translation>jméno + heslo + SMS</translation>
    </message>
    <message>
        <location filename="../../qml/wizards/CreateAccountPage2.qml" line="103"/>
        <location filename="../../qml/wizards/CreateAccountPage2.qml" line="103"/>
        <source>username + password + security code</source>
        <translation>jméno + heslo + bezpečnostní kód</translation>
    </message>
    <message>
        <location filename="../../qml/wizards/CreateAccountPage2.qml" line="108"/>
        <location filename="../../qml/wizards/CreateAccountPage2.qml" line="108"/>
        <source>username + password + certificate</source>
        <translation>jméno + heslo + certifikát</translation>
    </message>
    <message>
        <location filename="../../qml/wizards/CreateAccountPage2.qml" line="111"/>
        <location filename="../../qml/wizards/CreateAccountPage2.qml" line="111"/>
        <source>Select the login method which you use to access the data box in the %1 ISDS environment.</source>
        <translation>Zvolte způsob přihlašování, který používáte pro přístup do datové schránky v %1 prostředí ISDS.</translation>
    </message>
    <message>
        <location filename="../../qml/wizards/CreateAccountPage2.qml" line="111"/>
        <location filename="../../qml/wizards/CreateAccountPage2.qml" line="111"/>
        <source>testing</source>
        <translation>testovacím</translation>
    </message>
    <message>
        <location filename="../../qml/wizards/CreateAccountPage2.qml" line="111"/>
        <location filename="../../qml/wizards/CreateAccountPage2.qml" line="111"/>
        <source>production</source>
        <translation>produkčním</translation>
    </message>
    <message>
        <location filename="../../qml/wizards/CreateAccountPage2.qml" line="111"/>
        <location filename="../../qml/wizards/CreateAccountPage2.qml" line="111"/>
        <source>Note: NIA login methods such as bank ID, mojeID or eCitizen aren&apos;t supported because the ISDS system doesn&apos;t provide such functionality for third-party applications.</source>
        <translation>Poznámka: Přihlašovací metody NIA jako bankovní ID, mojeID nebo eObčanka nejsou podporovány, protože systém ISDS takovou funkcionalitu pro aplikace třetích stran neposkytuje.</translation>
    </message>
    <message>
        <location filename="../../qml/wizards/CreateAccountPage2.qml" line="120"/>
        <location filename="../../qml/wizards/CreateAccountPage2.qml" line="120"/>
        <source>Previous step</source>
        <translation>Předchozí krok</translation>
    </message>
    <message>
        <location filename="../../qml/wizards/CreateAccountPage2.qml" line="121"/>
        <location filename="../../qml/wizards/CreateAccountPage2.qml" line="121"/>
        <source>Back</source>
        <translation>Zpět</translation>
    </message>
    <message>
        <location filename="../../qml/wizards/CreateAccountPage2.qml" line="132"/>
        <location filename="../../qml/wizards/CreateAccountPage2.qml" line="132"/>
        <source>Next</source>
        <translation>Další</translation>
    </message>
</context>
<context>
    <name>CreateAccountPage3</name>
    <message>
        <location filename="../../qml/wizards/CreateAccountPage3.qml" line="65"/>
        <location filename="../../qml/wizards/CreateAccountPage3.qml" line="65"/>
        <source>The username must be filled in.</source>
        <translation>Uživatelské jméno musí být vyplněno.</translation>
    </message>
    <message>
        <location filename="../../qml/wizards/CreateAccountPage3.qml" line="71"/>
        <location filename="../../qml/wizards/CreateAccountPage3.qml" line="71"/>
        <source>The communication code must be filled in.</source>
        <translation>Komunikační kód musí být vyplněn.</translation>
    </message>
    <message>
        <location filename="../../qml/wizards/CreateAccountPage3.qml" line="78"/>
        <location filename="../../qml/wizards/CreateAccountPage3.qml" line="78"/>
        <source>The password must be filled in.</source>
        <translation>Heslo musí být vyplněno.</translation>
    </message>
    <message>
        <location filename="../../qml/wizards/CreateAccountPage3.qml" line="86"/>
        <location filename="../../qml/wizards/CreateAccountPage3.qml" line="86"/>
        <source>The certificate file must be specified.</source>
        <translation>Musíte určit soubor s certifikátem.</translation>
    </message>
    <message>
        <location filename="../../qml/wizards/CreateAccountPage3.qml" line="283"/>
        <location filename="../../qml/wizards/CreateAccountPage3.qml" line="283"/>
        <source>Next step</source>
        <translation>Další krok</translation>
    </message>
    <message>
        <location filename="../../qml/wizards/CreateAccountPage3.qml" line="179"/>
        <location filename="../../qml/wizards/CreateAccountPage3.qml" line="179"/>
        <source>Login method</source>
        <translation>Způsob přihlašování</translation>
    </message>
    <message>
        <location filename="../../qml/wizards/CreateAccountPage3.qml" line="193"/>
        <location filename="../../qml/wizards/CreateAccountPage3.qml" line="193"/>
        <source>Username</source>
        <translation>Uživatelské jméno</translation>
    </message>
    <message>
        <location filename="../../qml/wizards/CreateAccountPage3.qml" line="200"/>
        <location filename="../../qml/wizards/CreateAccountPage3.qml" line="200"/>
        <source>Warning: The username should contain only combinations of lower-case letters and digits.</source>
        <translation>Varování: Uživatelské jméno by mělo obsahovat jen kombinaci malých písmen a číslic.</translation>
    </message>
    <message>
        <location filename="../../qml/wizards/CreateAccountPage3.qml" line="210"/>
        <location filename="../../qml/wizards/CreateAccountPage3.qml" line="210"/>
        <source>The username must consist of at least 6 characters without spaces (only combinations of lower-case letters and digits are allowed). Notification: The username is not a data-box ID.</source>
        <translation>Uživatelské jméno se musí skládat alespoň z 6 znaků bez mezer (jsou povoleny jen kombinace malých písmen a číslic). Upozornění: Uživatelské jméno není ID datové schránky.</translation>
    </message>
    <message>
        <location filename="../../qml/wizards/CreateAccountPage3.qml" line="226"/>
        <location filename="../../qml/wizards/CreateAccountPage3.qml" line="226"/>
        <source>The password must be valid and non-expired. To check whether you&apos;ve entered the password correctly you may use the icon in the field on the right. Note: You must fist change the password using the ISDS web portal if it is your very first attempt to log into the data box.</source>
        <translation>Heslo musí být validní a neexspirované. Pro ověření, zda jste heslo zadali správně, můžete použít ikonu v poli napravo. Poznámka: Nejrve si musíte změnit heslo ve webovém portálu ISDS, pokud se jedná o Váš zcela první pokus o přihlášení do datové schránky.</translation>
    </message>
    <message>
        <location filename="../../qml/wizards/CreateAccountPage3.qml" line="219"/>
        <location filename="../../qml/wizards/CreateAccountPage3.qml" line="219"/>
        <source>Password</source>
        <translation>Heslo</translation>
    </message>
    <message>
        <location filename="../../qml/wizards/CreateAccountPage3.qml" line="111"/>
        <location filename="../../qml/wizards/CreateAccountPage3.qml" line="111"/>
        <source>Choose a certificate</source>
        <translation>Vybrat certifikát</translation>
    </message>
    <message>
        <location filename="../../qml/wizards/CreateAccountPage3.qml" line="151"/>
        <location filename="../../qml/wizards/CreateAccountPage3.qml" line="151"/>
        <source>Add Data Box</source>
        <translation>Přidat schránku</translation>
    </message>
    <message>
        <location filename="../../qml/wizards/CreateAccountPage3.qml" line="173"/>
        <location filename="../../qml/wizards/CreateAccountPage3.qml" line="173"/>
        <source>Data-box title</source>
        <translation>Pojmenování schránky</translation>
    </message>
    <message>
        <location filename="../../qml/wizards/CreateAccountPage3.qml" line="176"/>
        <location filename="../../qml/wizards/CreateAccountPage3.qml" line="176"/>
        <source>Data-box type</source>
        <translation>Typ datové schránky</translation>
    </message>
    <message>
        <location filename="../../qml/wizards/CreateAccountPage3.qml" line="194"/>
        <location filename="../../qml/wizards/CreateAccountPage3.qml" line="194"/>
        <source>Enter the login name.</source>
        <translation>Zadejte přihlašovací jméno.</translation>
    </message>
    <message>
        <location filename="../../qml/wizards/CreateAccountPage3.qml" line="220"/>
        <location filename="../../qml/wizards/CreateAccountPage3.qml" line="220"/>
        <source>Enter the password.</source>
        <translation>Zadejte heslo.</translation>
    </message>
    <message>
        <location filename="../../qml/wizards/CreateAccountPage3.qml" line="234"/>
        <location filename="../../qml/wizards/CreateAccountPage3.qml" line="234"/>
        <source>Communication Code</source>
        <translation>Komunikační kód</translation>
    </message>
    <message>
        <location filename="../../qml/wizards/CreateAccountPage3.qml" line="235"/>
        <location filename="../../qml/wizards/CreateAccountPage3.qml" line="235"/>
        <source>Enter the communication code.</source>
        <translation>Zadejte komunikační kód.</translation>
    </message>
    <message>
        <location filename="../../qml/wizards/CreateAccountPage3.qml" line="242"/>
        <location filename="../../qml/wizards/CreateAccountPage3.qml" line="242"/>
        <source>The &lt;a href=&quot;%1&quot;&gt;communication code&lt;/a&gt; is a string which can be generated in the ISDS web portal. You have to have the Mobile Key application installed. The Mobile Key application needs to be paired with the corresponding data-box account on the ISDS web portal.</source>
        <translation>&lt;a href=&quot;%1&quot;&gt;Komunikační kód&lt;/a&gt; je řetězec znaků, který lze vygenerovat ve webovém portálu ISDS. Musíte mít nainstalovanou aplikaci Mobilní klíč. Aplikace Mobilní klíč musí být ve webovém portálu ISDS spárována s odpovídajícím účtem k datové schránce.</translation>
    </message>
    <message>
        <location filename="../../qml/wizards/CreateAccountPage3.qml" line="249"/>
        <location filename="../../qml/wizards/CreateAccountPage3.qml" line="249"/>
        <source>Certificate File</source>
        <translation>Soubor s certifikátem</translation>
    </message>
    <message>
        <location filename="../../qml/wizards/CreateAccountPage3.qml" line="258"/>
        <location filename="../../qml/wizards/CreateAccountPage3.qml" line="258"/>
        <source>Choose certificate</source>
        <translation>Vybrat certifikát</translation>
    </message>
    <message>
        <location filename="../../qml/wizards/CreateAccountPage3.qml" line="271"/>
        <location filename="../../qml/wizards/CreateAccountPage3.qml" line="271"/>
        <source>Previous step</source>
        <translation>Předchozí krok</translation>
    </message>
    <message>
        <location filename="../../qml/wizards/CreateAccountPage3.qml" line="272"/>
        <location filename="../../qml/wizards/CreateAccountPage3.qml" line="272"/>
        <source>Back</source>
        <translation>Zpět</translation>
    </message>
    <message>
        <location filename="../../qml/wizards/CreateAccountPage3.qml" line="284"/>
        <location filename="../../qml/wizards/CreateAccountPage3.qml" line="284"/>
        <source>Next</source>
        <translation>Další</translation>
    </message>
    <message>
        <location filename="../../qml/wizards/CreateAccountPage3.qml" line="262"/>
        <location filename="../../qml/wizards/CreateAccountPage3.qml" line="262"/>
        <source>The certificate file is needed for authentication purposes. The supplied file must contain a certificate and its corresponding private key. Only PEM and PFX file formats are accepted.</source>
        <translation>Soubor s certifikátem je vyžadován za účelem autentizace. Dodaný soubor musí obsahovat jak certifikát, tak i odpovídající soukromý klíč. Jen formáty PEM a PFX jsou podporovány.</translation>
    </message>
</context>
<context>
    <name>CreateAccountPage4</name>
    <message>
        <location filename="../../qml/wizards/CreateAccountPage4.qml" line="38"/>
        <location filename="../../qml/wizards/CreateAccountPage4.qml" line="38"/>
        <source>During the log-in procedure you will be asked to enter a private key password.</source>
        <translation>V průběhu přihlašování budete vyzváni z zadání hesla k soukromému klíči.</translation>
    </message>
    <message>
        <location filename="../../qml/wizards/CreateAccountPage4.qml" line="41"/>
        <location filename="../../qml/wizards/CreateAccountPage4.qml" line="41"/>
        <source>During the log-in procedure you will be asked to confirm a notification in the Mobile Key application.</source>
        <translation>V průběhu přihlašování budete vyzváni k potvrzení notifikace v aplikaci Mobilní klíč.</translation>
    </message>
    <message>
        <location filename="../../qml/wizards/CreateAccountPage4.qml" line="43"/>
        <location filename="../../qml/wizards/CreateAccountPage4.qml" line="43"/>
        <source>During the log-in procedure you will be asked to enter a code which you should obtain via an SMS.</source>
        <translation>V průběhu přihlašování budete vyzváni z zadání kódu, který byste měli obdržet pomocí SMS.</translation>
    </message>
    <message>
        <location filename="../../qml/wizards/CreateAccountPage4.qml" line="45"/>
        <location filename="../../qml/wizards/CreateAccountPage4.qml" line="45"/>
        <source>During the log-in procedure you will be asked to enter a security code.</source>
        <translation>V průběhu přihlašování budete vyzváni z zadání bezpečnostního kódu.</translation>
    </message>
    <message>
        <location filename="../../qml/wizards/CreateAccountPage4.qml" line="70"/>
        <location filename="../../qml/wizards/CreateAccountPage4.qml" line="70"/>
        <source>Creating data box: %1</source>
        <translation>Vytváření schránky: %1</translation>
    </message>
    <message>
        <location filename="../../qml/wizards/CreateAccountPage4.qml" line="71"/>
        <location filename="../../qml/wizards/CreateAccountPage4.qml" line="71"/>
        <source>Data box &apos;%1&apos; could not be created.</source>
        <translation>Schránka &quot;%1&quot; nemohla být vytvořena.</translation>
    </message>
    <message>
        <location filename="../../qml/wizards/CreateAccountPage4.qml" line="100"/>
        <location filename="../../qml/wizards/CreateAccountPage4.qml" line="100"/>
        <source>Add Data Box</source>
        <translation>Přidat schránku</translation>
    </message>
    <message>
        <location filename="../../qml/wizards/CreateAccountPage4.qml" line="122"/>
        <location filename="../../qml/wizards/CreateAccountPage4.qml" line="122"/>
        <source>Data-box title</source>
        <translation>Pojmenování schránky</translation>
    </message>
    <message>
        <location filename="../../qml/wizards/CreateAccountPage4.qml" line="125"/>
        <location filename="../../qml/wizards/CreateAccountPage4.qml" line="125"/>
        <source>Data-box type</source>
        <translation>Typ datové schránky</translation>
    </message>
    <message>
        <location filename="../../qml/wizards/CreateAccountPage4.qml" line="142"/>
        <location filename="../../qml/wizards/CreateAccountPage4.qml" line="142"/>
        <source>Local Data-Box Settings</source>
        <translation>Místní nastavení schránky</translation>
    </message>
    <message>
        <location filename="../../qml/wizards/CreateAccountPage4.qml" line="164"/>
        <location filename="../../qml/wizards/CreateAccountPage4.qml" line="164"/>
        <source>The data box will be included into the synchronisation process of all data boxes.</source>
        <translation>Schránka bude zahrnuta do hromadné synchronizace všech schránek.</translation>
    </message>
    <message>
        <location filename="../../qml/wizards/CreateAccountPage4.qml" line="165"/>
        <location filename="../../qml/wizards/CreateAccountPage4.qml" line="165"/>
        <source>The data box won&apos;t be included into the synchronisation process of all data boxes.</source>
        <translation>Schránka nebude zahrnuta do hromadné synchronizace všech schránek.</translation>
    </message>
    <message>
        <location filename="../../qml/wizards/CreateAccountPage4.qml" line="174"/>
        <location filename="../../qml/wizards/CreateAccountPage4.qml" line="174"/>
        <source>All done. The data box will be added only when the application successfully logs into the data box using the supplied login credentials.</source>
        <translation>Vše hotovo. Scharánka bude přidána pouze, jestli se aplikace pomocí zadaných přihlašovacích údajů úspěšně přihlásí do datové schránky na serveru ISDS.</translation>
    </message>
    <message>
        <location filename="../../qml/wizards/CreateAccountPage4.qml" line="192"/>
        <location filename="../../qml/wizards/CreateAccountPage4.qml" line="192"/>
        <source>Connect to data box.</source>
        <translation>Přihlásit do schránky.</translation>
    </message>
    <message>
        <location filename="../../qml/wizards/CreateAccountPage4.qml" line="193"/>
        <location filename="../../qml/wizards/CreateAccountPage4.qml" line="193"/>
        <source>Connect Data Box</source>
        <translation>Přihlásit do schránky</translation>
    </message>
    <message>
        <location filename="../../qml/wizards/CreateAccountPage4.qml" line="213"/>
        <location filename="../../qml/wizards/CreateAccountPage4.qml" line="213"/>
        <source>Logging into the data box %1 ...</source>
        <translation>Přihlašuji se do schránky %1 ...</translation>
    </message>
    <message>
        <location filename="../../qml/wizards/CreateAccountPage4.qml" line="152"/>
        <location filename="../../qml/wizards/CreateAccountPage4.qml" line="152"/>
        <source>Use local storage</source>
        <translation>Použít lokální úložiště</translation>
    </message>
    <message>
        <location filename="../../qml/wizards/CreateAccountPage4.qml" line="155"/>
        <location filename="../../qml/wizards/CreateAccountPage4.qml" line="155"/>
        <source>Messages and attachments will be locally stored. No active internet connection is needed to access locally stored data.</source>
        <translation>Zprávy a přílohy budou trvale lokálně uloženy v interní databázi. K přístupu k lokálně uloženým datům není potřeba aktivního připojení k internetu.</translation>
    </message>
    <message>
        <location filename="../../qml/wizards/CreateAccountPage4.qml" line="156"/>
        <location filename="../../qml/wizards/CreateAccountPage4.qml" line="156"/>
        <source>Messages and attachments will be stored only temporarily in memory. These data will be lost on application exit.</source>
        <translation>Zprávy a přílohy budou ukládány pouze dočasně v paměti. Tyto data budou při vypnutí aplikace zahozena.</translation>
    </message>
    <message>
        <location filename="../../qml/wizards/CreateAccountPage4.qml" line="161"/>
        <location filename="../../qml/wizards/CreateAccountPage4.qml" line="161"/>
        <source>Synchronise with all</source>
        <translation>Synchronizovat se všemi</translation>
    </message>
    <message>
        <location filename="../../qml/wizards/CreateAccountPage4.qml" line="182"/>
        <location filename="../../qml/wizards/CreateAccountPage4.qml" line="182"/>
        <source>Previous step</source>
        <translation>Předchozí krok</translation>
    </message>
    <message>
        <location filename="../../qml/wizards/CreateAccountPage4.qml" line="183"/>
        <location filename="../../qml/wizards/CreateAccountPage4.qml" line="183"/>
        <source>Back</source>
        <translation>Zpět</translation>
    </message>
    <message>
        <location filename="../../qml/wizards/CreateAccountPage4.qml" line="128"/>
        <location filename="../../qml/wizards/CreateAccountPage4.qml" line="128"/>
        <source>Login method</source>
        <translation>Způsob přihlašování</translation>
    </message>
    <message>
        <location filename="../../qml/wizards/CreateAccountPage4.qml" line="131"/>
        <location filename="../../qml/wizards/CreateAccountPage4.qml" line="131"/>
        <source>Username</source>
        <translation>Uživatelské jméno</translation>
    </message>
    <message>
        <location filename="../../qml/wizards/CreateAccountPage4.qml" line="136"/>
        <location filename="../../qml/wizards/CreateAccountPage4.qml" line="136"/>
        <source>Certificate</source>
        <translation>Certifikát</translation>
    </message>
    <message>
        <location filename="../../qml/wizards/CreateAccountPage4.qml" line="146"/>
        <location filename="../../qml/wizards/CreateAccountPage4.qml" line="146"/>
        <source>Remember password</source>
        <translation>Zapamatovat si heslo</translation>
    </message>
</context>
<context>
    <name>DataboxList</name>
    <message>
        <location filename="../../qml/components/DataboxList.qml" line="54"/>
        <location filename="../../qml/components/DataboxList.qml" line="171"/>
        <location filename="../../qml/components/DataboxList.qml" line="54"/>
        <location filename="../../qml/components/DataboxList.qml" line="171"/>
        <source>Public</source>
        <translation>Veřejná</translation>
    </message>
    <message>
        <location filename="../../qml/components/DataboxList.qml" line="56"/>
        <location filename="../../qml/components/DataboxList.qml" line="56"/>
        <source>Response to initiatory</source>
        <translation>Odpověď na iniciační</translation>
    </message>
    <message>
        <location filename="../../qml/components/DataboxList.qml" line="58"/>
        <location filename="../../qml/components/DataboxList.qml" line="58"/>
        <source>Subsidised</source>
        <translation>Dotovaná</translation>
    </message>
    <message>
        <location filename="../../qml/components/DataboxList.qml" line="60"/>
        <location filename="../../qml/components/DataboxList.qml" line="60"/>
        <source>Contractual</source>
        <translation>Smluvní</translation>
    </message>
    <message>
        <location filename="../../qml/components/DataboxList.qml" line="62"/>
        <location filename="../../qml/components/DataboxList.qml" line="62"/>
        <source>Initiatory</source>
        <translation>Iniciační</translation>
    </message>
    <message>
        <location filename="../../qml/components/DataboxList.qml" line="64"/>
        <location filename="../../qml/components/DataboxList.qml" line="64"/>
        <source>Prepaid credit</source>
        <translation>Předplacený kredit</translation>
    </message>
    <message>
        <location filename="../../qml/components/DataboxList.qml" line="175"/>
        <location filename="../../qml/components/DataboxList.qml" line="175"/>
        <source>Unknown</source>
        <translation>Neznámý</translation>
    </message>
    <message>
        <location filename="../../qml/components/DataboxList.qml" line="180"/>
        <location filename="../../qml/components/DataboxList.qml" line="180"/>
        <source>Message type: %1</source>
        <translation>Typ zprávy: %1</translation>
    </message>
    <message>
        <location filename="../../qml/components/DataboxList.qml" line="189"/>
        <location filename="../../qml/components/DataboxList.qml" line="189"/>
        <source>Payment:</source>
        <translation>Platba:</translation>
    </message>
    <message>
        <location filename="../../qml/components/DataboxList.qml" line="194"/>
        <location filename="../../qml/components/DataboxList.qml" line="194"/>
        <source>Select commercial message payment method.</source>
        <translation>Vyberte způsob platby za PDZ.</translation>
    </message>
</context>
<context>
    <name>DbWrapper</name>
    <message>
        <location filename="../../src/net/db_wrapper.cpp" line="85"/>
        <location filename="../../src/net/db_wrapper.cpp" line="158"/>
        <source>Message %1 envelope update failed!</source>
        <translation>Aktulizace obálky zprávy  %1 selhala!</translation>
    </message>
    <message>
        <location filename="../../src/net/db_wrapper.cpp" line="99"/>
        <source>%1: new messages: %2</source>
        <translation>%1: nových zpráv: %2</translation>
    </message>
    <message>
        <location filename="../../src/net/db_wrapper.cpp" line="102"/>
        <source>%1: No new messages.</source>
        <translation>%1: Žádné nové zprávy.</translation>
    </message>
    <message>
        <location filename="../../src/net/db_wrapper.cpp" line="54"/>
        <location filename="../../src/net/db_wrapper.cpp" line="151"/>
        <location filename="../../src/net/db_wrapper.cpp" line="234"/>
        <location filename="../../src/net/db_wrapper.cpp" line="256"/>
        <source>Cannot open message database!</source>
        <translation>Nepodařilo se otevřít databázi zpráv!</translation>
    </message>
    <message>
        <location filename="../../src/net/db_wrapper.cpp" line="44"/>
        <location filename="../../src/net/db_wrapper.cpp" line="117"/>
        <location filename="../../src/net/db_wrapper.cpp" line="224"/>
        <location filename="../../src/net/db_wrapper.cpp" line="246"/>
        <source>Internal error!</source>
        <translation>Interní chyba!</translation>
    </message>
    <message>
        <location filename="../../src/net/db_wrapper.cpp" line="71"/>
        <source>Message %1 envelope insertion failed!</source>
        <translation>Zpracování obálky zprávy  %1 selhalo!</translation>
    </message>
    <message>
        <location filename="../../src/net/db_wrapper.cpp" line="132"/>
        <source>Cannot open file database!</source>
        <translation>Nepodařilo se otevřít databázi souborů!</translation>
    </message>
    <message>
        <location filename="../../src/net/db_wrapper.cpp" line="139"/>
        <source>File %1 insertion failed!</source>
        <translation>Nepodařilo se zpracovat soubor %1!</translation>
    </message>
</context>
<context>
    <name>Description</name>
    <message>
        <location filename="../../src/datovka_shared/isds/type_description.cpp" line="34"/>
        <source>system box</source>
        <translation>systémová schránka</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/isds/type_description.cpp" line="35"/>
        <source>public authority</source>
        <translation>orgán veřejné moci</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/isds/type_description.cpp" line="36"/>
        <source>public authority - notary</source>
        <translation>orgán veřejné moci - notář</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/isds/type_description.cpp" line="37"/>
        <source>public authority - bailiff</source>
        <translation>orgán veřejné moci - exekutor</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/isds/type_description.cpp" line="38"/>
        <source>public authority - at request</source>
        <translation>public authority - at request</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/isds/type_description.cpp" line="39"/>
        <source>public authority - natural person</source>
        <translation>orgán veřejné moci - fyzická osoba</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/isds/type_description.cpp" line="40"/>
        <source>public authority - self-employed person</source>
        <translation>orgán veřejné moci - podnikající fyzická osoba</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/isds/type_description.cpp" line="41"/>
        <source>public authority - legal person</source>
        <translation>orgán veřejné moci - právnická osoba</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/isds/type_description.cpp" line="42"/>
        <source>legal person</source>
        <translation>právnická osoba</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/isds/type_description.cpp" line="43"/>
        <source>legal person - founded by an act</source>
        <translation>právnická osoba - ze zákona</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/isds/type_description.cpp" line="44"/>
        <source>legal person - at request</source>
        <translation>právnická osoba - na žádost</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/isds/type_description.cpp" line="45"/>
        <source>self-employed person</source>
        <translation>podnikající fyzická osoba</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/isds/type_description.cpp" line="46"/>
        <source>self-employed person - advocate</source>
        <translation>podnikající fyzická osoba - advokát</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/isds/type_description.cpp" line="47"/>
        <source>self-employed person - tax advisor</source>
        <translation>podnikající fyzická osoba - daňový poradce</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/isds/type_description.cpp" line="48"/>
        <source>self-employed person - insolvency administrator</source>
        <translation>podnikající fyzická osoba - insolvenční správce</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/isds/type_description.cpp" line="49"/>
        <source>self-employed person - statutory auditor</source>
        <translation>podnikající fyzická osoba - auditor</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/isds/type_description.cpp" line="50"/>
        <source>self-employed person - expert witness</source>
        <translation>podnikající fyzická osoba - soudní znalec</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/isds/type_description.cpp" line="51"/>
        <source>self-employed person - sworn translator</source>
        <translation>podnikající fyzická osoba - soudní překladatel nebo tlumočník</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/isds/type_description.cpp" line="52"/>
        <source>self-employed person - architect</source>
        <translation>podnikající fyzická osoba - architekt</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/isds/type_description.cpp" line="53"/>
        <source>self-employed person - authorised engineer / technician</source>
        <translation>podnikající fyzická osoba - autorizovaný inženýr / technik</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/isds/type_description.cpp" line="54"/>
        <source>self-employed person - authorised geodetics engineer</source>
        <translation>podnikající fyzická osoba - autorizovaný zeměměřický inženýr</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/isds/type_description.cpp" line="55"/>
        <source>self-employed person - at request</source>
        <translation>podnikající fyzická osoba - na žádost</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/isds/type_description.cpp" line="56"/>
        <source>natural person</source>
        <translation>fyzická osoba</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/isds/type_description.cpp" line="60"/>
        <location filename="../../src/datovka_shared/isds/type_description.cpp" line="341"/>
        <location filename="../../src/datovka_shared/isds/type_description.cpp" line="387"/>
        <source>An error occurred while checking the type.</source>
        <translation>Nastala chyba při ověřování typu.</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/isds/type_description.cpp" line="73"/>
        <source>The data box is accessible. It is possible to send messages into it. It can be looked up on the Portal.</source>
        <translation>Datová schránka je přístupná, lze do ní dodávat zprávy, na Portále lze vyhledat.</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/isds/type_description.cpp" line="78"/>
        <source>The data box is temporarily inaccessible (at own request). It may be made accessible again at some point in the future.</source>
        <translation>Datová schránka je dočasně znepřístupněna (na vlastní žádost), může být později opět zpřístupněna.</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/isds/type_description.cpp" line="83"/>
        <source>The data box is so far inactive. The owner of the box has to log into the web interface at first in order to activate the box.</source>
        <translation>Datová schránka je dosud neaktivní. Vlastník schránky se musí poprvé přihlásit do webového rozhraní, aby došlo k aktivaci schránky.</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/isds/type_description.cpp" line="88"/>
        <source>The data box is permanently inaccessible. It is waiting to be deleted (but it may be made accessible again).</source>
        <translation>Datová schránka je trvale znepřístupněna, čeká na smazání (může být opět zpřístupněna).</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/isds/type_description.cpp" line="93"/>
        <source>The data box has been deleted (none the less it exists in ISDS).</source>
        <translation>Datová schránka byla smazána (přesto existuje v ISDS).</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/isds/type_description.cpp" line="98"/>
        <source>The data box is temporarily inaccessible (because of reasons listed in law). It may be made accessible again at some point in the future.</source>
        <translation>Datová schránka je dočasně znepřístupněna (z důvodů vyjmenovaných v zákoně), může být později opět zpřístupněna.</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/isds/type_description.cpp" line="104"/>
        <location filename="../../src/datovka_shared/isds/type_description.cpp" line="235"/>
        <source>An error occurred while checking the status.</source>
        <translation>Nastala chyba při ověřování stavu.</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/isds/type_description.cpp" line="115"/>
        <source>Full control</source>
        <translation>Plný přístup</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/isds/type_description.cpp" line="117"/>
        <source>Restricted control</source>
        <translation>Omezený přístup</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/isds/type_description.cpp" line="122"/>
        <source>download and read incoming DM</source>
        <translation>stahovat a číst došlé DZ</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/isds/type_description.cpp" line="128"/>
        <source>download and read DM sent into own hands</source>
        <translation>stahovat a číst DZ určené do vlastních rukou</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/isds/type_description.cpp" line="133"/>
        <source>create and send DM, download sent DM</source>
        <translation>vytvářet a odesílat DZ, stahovat odeslané DZ</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/isds/type_description.cpp" line="138"/>
        <source>retrieve DM lists, delivery and acceptance reports</source>
        <translation>načítat seznamy DZ, dodejky a doručenky</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/isds/type_description.cpp" line="143"/>
        <source>search for data boxes</source>
        <translation>vyhledávat DS</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/isds/type_description.cpp" line="147"/>
        <source>manage the data box</source>
        <translation>spravovat DS</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/isds/type_description.cpp" line="151"/>
        <source>read message in data vault</source>
        <translation>číst zprávy v datovém trezoru</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/isds/type_description.cpp" line="155"/>
        <source>erase messages from data vault</source>
        <translation>mazat zprávy v datovém trezoru</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/isds/type_description.cpp" line="169"/>
        <source>Message has been submitted (has been created in ISDS)</source>
        <translation>Zpráva byla podána (vznikla v ISDS)</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/isds/type_description.cpp" line="175"/>
        <source>Data message including its attachments signed with time-stamp.</source>
        <translation>Datová zpráva včetně písemností podepsána časovým razítkem.</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/isds/type_description.cpp" line="182"/>
        <source>Message did not pass through AV check; infected paper deleted; final status before deletion.</source>
        <translation>Zpráva neprošla AV kontrolou; nakažená písemnost je smazána; konečný stav zprávy před smazáním.</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/isds/type_description.cpp" line="187"/>
        <source>Message handed into ISDS (delivery time recorded).</source>
        <translation>Zpráva dodána do ISDS (zapsán čas dodání).</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/isds/type_description.cpp" line="195"/>
        <source>10 days have passed since the delivery of the public message which has not been accepted by logging-in (assumption of acceptance through fiction in non-OVM DS); this state cannot occur for commercial messages.</source>
        <translation>Uplynulo 10 dnů od dodání veřejné zprávy, která dosud nebyla doručena přihlášením (předpoklad doručení fikcí u ne-OVM DS); tento stav nemůže nastat v případě poštovních datových zpráv.</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/isds/type_description.cpp" line="205"/>
        <source>A person authorised to read this message has logged in -- delivered message has been accepted.</source>
        <translation>Osoba oprávněná číst tuto zprávu se přihlásila - dodaná zpráva byla doručena.</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/isds/type_description.cpp" line="210"/>
        <source>Message has been read (on the portal or by ESS action).</source>
        <translation>Zpráva byla přečtena (na portále nebo akcí ESS).</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/isds/type_description.cpp" line="217"/>
        <source>Message marked as undeliverable because the target DS has been made inaccessible.</source>
        <translation>Zpráva byla označena jako nedoručitelná, protože DS adresáta byla zpětně znepřístupněna.</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/isds/type_description.cpp" line="225"/>
        <source>Message content deleted, envelope including hashes has been moved into archive.</source>
        <translation>Obsah zprávy byl smazán, obálka zprávy včetně hashů přesunuta do archivu.</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/isds/type_description.cpp" line="230"/>
        <source>Message resides in data vault.</source>
        <translation>Zpráva je v Datovém trezoru.</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/isds/type_description.cpp" line="256"/>
        <source>Subsidised postal data message, initiating reply postal data message</source>
        <translation>Poštovní datová zpráva dotovaná, iniciuje použití odpovědní poštovní datové zprávy</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/isds/type_description.cpp" line="260"/>
        <source>Subsidised postal data message, initiating reply postal data message - used for sending reply</source>
        <translation>Poštovní datová zpráva dotovaná, iniciuje použití odpovědní poštovní datové zprávy - využitá pro odeslání odpovědi</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/isds/type_description.cpp" line="264"/>
        <source>Subsidised postal data message, initiating reply postal data message - unused for sending reply, expired</source>
        <translation>Poštovní datová zpráva dotovaná, iniciuje použití odpovědní poštovní datové zprávy - nevyužitá a expirovaná</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/isds/type_description.cpp" line="268"/>
        <source>Postal data message sent using a subscription (prepaid credit)</source>
        <translation>Poštovní datová zpráva odeslaná pomocí předplacení (kreditu)</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/isds/type_description.cpp" line="272"/>
        <source>Postal data message sent in endowment mode by another data box to the benefactor account</source>
        <translation>Poštovní datová zpráva zaslaná v režimu dotování jinou schránkou na účet schránky donátora</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/isds/type_description.cpp" line="276"/>
        <source>Postal data message</source>
        <translation>Poštovní datová zpráva</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/isds/type_description.cpp" line="279"/>
        <source>Initiating postal data message</source>
        <translation>Iniciační poštovní datová zpráva</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/isds/type_description.cpp" line="282"/>
        <source>Reply postal data message; sent at the expense of the sender of the initiating postal data message</source>
        <translation>Odpovědní poštovní datová zpráva; zasílaná zdarma na účet odesílatele iniciační poštovní datové zprávy</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/isds/type_description.cpp" line="286"/>
        <source>Public message (recipient or sender is a public authority)</source>
        <translation>Veřejná zpráva (adresát nebo odesílatel je OVM)</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/isds/type_description.cpp" line="289"/>
        <source>Initiating postal data message - unused for sending reply, expired</source>
        <translation>Iniciační poštovní datová zpráva - nevyužitá a expirovaná</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/isds/type_description.cpp" line="292"/>
        <source>Initiating postal data message - used for sending reply</source>
        <translation>Iniciační poštovní datová zpráva - využitá pro odeslání odpovědi</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/isds/type_description.cpp" line="297"/>
        <source>Unrecognised message type</source>
        <translation>Nerozpoznaný typ zprávy</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/isds/type_description.cpp" line="310"/>
        <location filename="../../src/datovka_shared/isds/type_description.cpp" line="354"/>
        <source>Primary user</source>
        <translation>Oprávněná osoba</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/isds/type_description.cpp" line="314"/>
        <location filename="../../src/datovka_shared/isds/type_description.cpp" line="358"/>
        <source>Entrusted user</source>
        <translation>Pověřená osoba</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/isds/type_description.cpp" line="318"/>
        <location filename="../../src/datovka_shared/isds/type_description.cpp" line="362"/>
        <source>Administrator</source>
        <translation>Administrátor</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/isds/type_description.cpp" line="321"/>
        <location filename="../../src/datovka_shared/isds/type_description.cpp" line="366"/>
        <source>Official</source>
        <translation>Systém ISDS</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/isds/type_description.cpp" line="324"/>
        <location filename="../../src/datovka_shared/isds/type_description.cpp" line="373"/>
        <source>???</source>
        <translation>???</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/isds/type_description.cpp" line="328"/>
        <location filename="../../src/datovka_shared/isds/type_description.cpp" line="376"/>
        <source>Liquidator</source>
        <translation>Likvidátor</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/isds/type_description.cpp" line="332"/>
        <location filename="../../src/datovka_shared/isds/type_description.cpp" line="379"/>
        <source>Receiver</source>
        <translation>Nucený správce</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/isds/type_description.cpp" line="336"/>
        <location filename="../../src/datovka_shared/isds/type_description.cpp" line="382"/>
        <source>Guardian</source>
        <translation>Opatrovník</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/isds/type_description.cpp" line="370"/>
        <source>Virtual</source>
        <translation>Virtuální přístup</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/isds/type_description.cpp" line="395"/>
        <source>Success</source>
        <translation>Úspěch</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/isds/type_description.cpp" line="396"/>
        <source>Unspecified error</source>
        <translation>Nespecifikovaná chyba</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/isds/type_description.cpp" line="397"/>
        <source>Not supported</source>
        <translation>Není podporováno</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/isds/type_description.cpp" line="398"/>
        <source>Invalid value</source>
        <translation>Neplatná hodnota</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/isds/type_description.cpp" line="399"/>
        <source>Invalid context</source>
        <translation>Neplatný kontext</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/isds/type_description.cpp" line="400"/>
        <source>Not logged in</source>
        <translation>Nepřihlášen</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/isds/type_description.cpp" line="401"/>
        <source>Connection closed</source>
        <translation>Spojení ukončeno</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/isds/type_description.cpp" line="404"/>
        <source>Out of memory</source>
        <translation>Nedostatek paměti</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/isds/type_description.cpp" line="405"/>
        <source>Network problem</source>
        <translation>Chyba sítě</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/isds/type_description.cpp" line="406"/>
        <source>HTTP problem</source>
        <translation>Chyba HTTP</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/isds/type_description.cpp" line="407"/>
        <source>SOAP problem</source>
        <translation>Chyba SOAP</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/isds/type_description.cpp" line="408"/>
        <source>XML problem</source>
        <translation>Chyba XML</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/isds/type_description.cpp" line="409"/>
        <source>ISDS server problem</source>
        <translation>Chyba serveru ISDS</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/isds/type_description.cpp" line="410"/>
        <source>Invalid enumeration value</source>
        <translation>Neplatná výčtová hodnota</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/isds/type_description.cpp" line="411"/>
        <source>Invalid date value</source>
        <translation>Neplatné datum</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/isds/type_description.cpp" line="412"/>
        <source>Too big</source>
        <translation>Příliš velké</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/isds/type_description.cpp" line="413"/>
        <source>Too small</source>
        <translation>Příliš malé</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/isds/type_description.cpp" line="414"/>
        <source>Value not unique</source>
        <translation>Hodnota není unikátní</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/isds/type_description.cpp" line="415"/>
        <source>Values not equal</source>
        <translation>Hodnota se nerovná</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/isds/type_description.cpp" line="416"/>
        <source>Some suboperations failed</source>
        <translation>Selhaly některé podoperace</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/isds/type_description.cpp" line="417"/>
        <source>Operation aborted</source>
        <translation>Operace přerušena</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/isds/type_description.cpp" line="418"/>
        <source>Security problem</source>
        <translation>Bezpečnostní problém</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/isds/type_description.cpp" line="421"/>
        <source>Unknown error</source>
        <translation>Neznámá chyba</translation>
    </message>
</context>
<context>
    <name>DrawerMenuDatovka</name>
    <message>
        <location filename="../../qml/popups/DrawerMenuDatovka.qml" line="83"/>
        <location filename="../../qml/popups/DrawerMenuDatovka.qml" line="107"/>
        <location filename="../../qml/popups/DrawerMenuDatovka.qml" line="83"/>
        <location filename="../../qml/popups/DrawerMenuDatovka.qml" line="107"/>
        <source>Messages</source>
        <translation>Zprávy</translation>
    </message>
    <message>
        <location filename="../../qml/popups/DrawerMenuDatovka.qml" line="87"/>
        <location filename="../../qml/popups/DrawerMenuDatovka.qml" line="109"/>
        <location filename="../../qml/popups/DrawerMenuDatovka.qml" line="87"/>
        <location filename="../../qml/popups/DrawerMenuDatovka.qml" line="109"/>
        <source>Settings</source>
        <translation>Nastavení</translation>
    </message>
    <message>
        <location filename="../../qml/popups/DrawerMenuDatovka.qml" line="88"/>
        <location filename="../../qml/popups/DrawerMenuDatovka.qml" line="110"/>
        <location filename="../../qml/popups/DrawerMenuDatovka.qml" line="88"/>
        <location filename="../../qml/popups/DrawerMenuDatovka.qml" line="110"/>
        <source>General</source>
        <translation>Obecné</translation>
    </message>
    <message>
        <location filename="../../qml/popups/DrawerMenuDatovka.qml" line="91"/>
        <location filename="../../qml/popups/DrawerMenuDatovka.qml" line="113"/>
        <location filename="../../qml/popups/DrawerMenuDatovka.qml" line="91"/>
        <location filename="../../qml/popups/DrawerMenuDatovka.qml" line="113"/>
        <source>Records Management</source>
        <translation>Spisová služba</translation>
    </message>
    <message>
        <location filename="../../qml/popups/DrawerMenuDatovka.qml" line="50"/>
        <location filename="../../qml/popups/DrawerMenuDatovka.qml" line="68"/>
        <location filename="../../qml/popups/DrawerMenuDatovka.qml" line="50"/>
        <location filename="../../qml/popups/DrawerMenuDatovka.qml" line="68"/>
        <source>Data-box properties</source>
        <translation>Vlastnosti schránky</translation>
    </message>
    <message>
        <location filename="../../qml/popups/DrawerMenuDatovka.qml" line="81"/>
        <location filename="../../qml/popups/DrawerMenuDatovka.qml" line="105"/>
        <location filename="../../qml/popups/DrawerMenuDatovka.qml" line="81"/>
        <location filename="../../qml/popups/DrawerMenuDatovka.qml" line="105"/>
        <source>Data Boxes</source>
        <translation>Schránky</translation>
    </message>
    <message>
        <location filename="../../qml/popups/DrawerMenuDatovka.qml" line="82"/>
        <location filename="../../qml/popups/DrawerMenuDatovka.qml" line="106"/>
        <location filename="../../qml/popups/DrawerMenuDatovka.qml" line="82"/>
        <location filename="../../qml/popups/DrawerMenuDatovka.qml" line="106"/>
        <source>Add Data Box</source>
        <translation>Přidat schránku</translation>
    </message>
    <message>
        <location filename="../../qml/popups/DrawerMenuDatovka.qml" line="84"/>
        <location filename="../../qml/popups/DrawerMenuDatovka.qml" line="108"/>
        <location filename="../../qml/popups/DrawerMenuDatovka.qml" line="84"/>
        <location filename="../../qml/popups/DrawerMenuDatovka.qml" line="108"/>
        <source>View Message</source>
        <translation>Zobrazit zprávu</translation>
    </message>
    <message>
        <location filename="../../qml/popups/DrawerMenuDatovka.qml" line="85"/>
        <location filename="../../qml/popups/DrawerMenuDatovka.qml" line="85"/>
        <source>Search Message</source>
        <translation>Hledat zprávu</translation>
    </message>
    <message>
        <location filename="../../qml/popups/DrawerMenuDatovka.qml" line="86"/>
        <location filename="../../qml/popups/DrawerMenuDatovka.qml" line="86"/>
        <source>Import Message</source>
        <translation>Importovat zprávu</translation>
    </message>
    <message>
        <location filename="../../qml/popups/DrawerMenuDatovka.qml" line="89"/>
        <location filename="../../qml/popups/DrawerMenuDatovka.qml" line="111"/>
        <location filename="../../qml/popups/DrawerMenuDatovka.qml" line="89"/>
        <location filename="../../qml/popups/DrawerMenuDatovka.qml" line="111"/>
        <source>Synchronisation</source>
        <translation>Synchronizace</translation>
    </message>
    <message>
        <location filename="../../qml/popups/DrawerMenuDatovka.qml" line="90"/>
        <location filename="../../qml/popups/DrawerMenuDatovka.qml" line="112"/>
        <location filename="../../qml/popups/DrawerMenuDatovka.qml" line="90"/>
        <location filename="../../qml/popups/DrawerMenuDatovka.qml" line="112"/>
        <source>Security &amp;&amp; PIN</source>
        <translation>Zabezpečení a PIN</translation>
    </message>
    <message>
        <location filename="../../qml/popups/DrawerMenuDatovka.qml" line="92"/>
        <location filename="../../qml/popups/DrawerMenuDatovka.qml" line="114"/>
        <location filename="../../qml/popups/DrawerMenuDatovka.qml" line="92"/>
        <location filename="../../qml/popups/DrawerMenuDatovka.qml" line="114"/>
        <source>View All Settings</source>
        <translation>Zobrazit všechny předvolby</translation>
    </message>
    <message>
        <location filename="../../qml/popups/DrawerMenuDatovka.qml" line="93"/>
        <location filename="../../qml/popups/DrawerMenuDatovka.qml" line="115"/>
        <location filename="../../qml/popups/DrawerMenuDatovka.qml" line="93"/>
        <location filename="../../qml/popups/DrawerMenuDatovka.qml" line="115"/>
        <source>Back up &amp;&amp; Restore</source>
        <translation>Záloha a obnova</translation>
    </message>
    <message>
        <location filename="../../qml/popups/DrawerMenuDatovka.qml" line="94"/>
        <location filename="../../qml/popups/DrawerMenuDatovka.qml" line="94"/>
        <source>Transfer Data</source>
        <translation>Přenos dat</translation>
    </message>
    <message>
        <location filename="../../qml/popups/DrawerMenuDatovka.qml" line="95"/>
        <location filename="../../qml/popups/DrawerMenuDatovka.qml" line="95"/>
        <source>Back up Data</source>
        <translation>Záloha dat</translation>
    </message>
    <message>
        <location filename="../../qml/popups/DrawerMenuDatovka.qml" line="96"/>
        <location filename="../../qml/popups/DrawerMenuDatovka.qml" line="116"/>
        <location filename="../../qml/popups/DrawerMenuDatovka.qml" line="96"/>
        <location filename="../../qml/popups/DrawerMenuDatovka.qml" line="116"/>
        <source>Restore Data</source>
        <translation>Obnova dat</translation>
    </message>
    <message>
        <location filename="../../qml/popups/DrawerMenuDatovka.qml" line="97"/>
        <location filename="../../qml/popups/DrawerMenuDatovka.qml" line="117"/>
        <location filename="../../qml/popups/DrawerMenuDatovka.qml" line="97"/>
        <location filename="../../qml/popups/DrawerMenuDatovka.qml" line="117"/>
        <source>Help</source>
        <translation>Nápověda</translation>
    </message>
    <message>
        <location filename="../../qml/popups/DrawerMenuDatovka.qml" line="98"/>
        <location filename="../../qml/popups/DrawerMenuDatovka.qml" line="118"/>
        <location filename="../../qml/popups/DrawerMenuDatovka.qml" line="98"/>
        <location filename="../../qml/popups/DrawerMenuDatovka.qml" line="118"/>
        <source>Log Viewer</source>
        <translation>Zobrazení logu</translation>
    </message>
    <message>
        <location filename="../../qml/popups/DrawerMenuDatovka.qml" line="99"/>
        <location filename="../../qml/popups/DrawerMenuDatovka.qml" line="119"/>
        <location filename="../../qml/popups/DrawerMenuDatovka.qml" line="99"/>
        <location filename="../../qml/popups/DrawerMenuDatovka.qml" line="119"/>
        <source>User Guide</source>
        <translation>Uživatelská příručka</translation>
    </message>
    <message>
        <location filename="../../qml/popups/DrawerMenuDatovka.qml" line="100"/>
        <location filename="../../qml/popups/DrawerMenuDatovka.qml" line="120"/>
        <location filename="../../qml/popups/DrawerMenuDatovka.qml" line="100"/>
        <location filename="../../qml/popups/DrawerMenuDatovka.qml" line="120"/>
        <source>About Datovka</source>
        <translation>O Datovce</translation>
    </message>
</context>
<context>
    <name>ErrorEntry</name>
    <message>
        <location filename="../../src/datovka_shared/records_management/json/entry_error.cpp" line="312"/>
        <source>No error occurred</source>
        <translation>Žádná chyba se nevyskytla</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/records_management/json/entry_error.cpp" line="315"/>
        <source>Request was malformed</source>
        <translation>Požadavek byl poškozený</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/records_management/json/entry_error.cpp" line="318"/>
        <source>Identifier is missing</source>
        <translation>Chybějící identifikátor</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/records_management/json/entry_error.cpp" line="321"/>
        <source>Supplied identifier is wrong</source>
        <translation>Neplatný identifikátor</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/records_management/json/entry_error.cpp" line="324"/>
        <source>File format is not supported</source>
        <translation>Nepodporovaný formát souboru</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/records_management/json/entry_error.cpp" line="327"/>
        <source>Data are already present</source>
        <translation>Data již existují</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/records_management/json/entry_error.cpp" line="330"/>
        <source>Service limit was exceeded</source>
        <translation>Překročen limit služby</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/records_management/json/entry_error.cpp" line="333"/>
        <source>Unspecified error</source>
        <translation>Blíže neurčená chyba</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/records_management/json/entry_error.cpp" line="337"/>
        <source>Unknown error</source>
        <translation>Neznámá chyba</translation>
    </message>
</context>
<context>
    <name>Export</name>
    <message>
        <location filename="../../src/datovka_shared/html/html_export.cpp" line="95"/>
        <source>at</source>
        <translation>v</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/html/html_export.cpp" line="162"/>
        <location filename="../../src/datovka_shared/html/html_export.cpp" line="922"/>
        <source>User Type</source>
        <translation>Typ uživatele</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/html/html_export.cpp" line="169"/>
        <source>Full Name</source>
        <translation>Plné jméno</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/html/html_export.cpp" line="277"/>
        <source>paragraph</source>
        <translation>dstavec</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/html/html_export.cpp" line="281"/>
        <source>letter</source>
        <translation>písmeno</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/html/html_export.cpp" line="303"/>
        <location filename="../../src/datovka_shared/html/html_export.cpp" line="489"/>
        <source>Our Reference Number</source>
        <translation>Naše číslo jednací</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/html/html_export.cpp" line="307"/>
        <location filename="../../src/datovka_shared/html/html_export.cpp" line="492"/>
        <source>Our File Mark</source>
        <translation>Naše spisová značka</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/html/html_export.cpp" line="311"/>
        <location filename="../../src/datovka_shared/html/html_export.cpp" line="495"/>
        <source>Your Reference Number</source>
        <translation>Vaše číslo jednací</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/html/html_export.cpp" line="315"/>
        <location filename="../../src/datovka_shared/html/html_export.cpp" line="498"/>
        <source>Your File Mark</source>
        <translation>Vaše spisová značka</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/html/html_export.cpp" line="354"/>
        <location filename="../../src/datovka_shared/html/html_export.cpp" line="501"/>
        <source>To Hands</source>
        <translation>K rukám</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/html/html_export.cpp" line="349"/>
        <location filename="../../src/datovka_shared/html/html_export.cpp" line="487"/>
        <source>Mandate</source>
        <translation>Zmocnění</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/html/html_export.cpp" line="358"/>
        <location filename="../../src/datovka_shared/html/html_export.cpp" line="503"/>
        <source>Personal Delivery</source>
        <translation>Do vlastních rukou</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/html/html_export.cpp" line="359"/>
        <location filename="../../src/datovka_shared/html/html_export.cpp" line="505"/>
        <location filename="../../src/datovka_shared/html/html_export.cpp" line="508"/>
        <location filename="../../src/datovka_shared/html/html_export.cpp" line="628"/>
        <source>yes</source>
        <translation>ano</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/html/html_export.cpp" line="380"/>
        <location filename="../../src/datovka_shared/html/html_export.cpp" line="454"/>
        <location filename="../../src/datovka_shared/html/html_export.cpp" line="718"/>
        <source>Sender</source>
        <translation>Odesílatel</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/html/html_export.cpp" line="381"/>
        <location filename="../../src/datovka_shared/html/html_export.cpp" line="470"/>
        <location filename="../../src/datovka_shared/html/html_export.cpp" line="718"/>
        <source>Recipient</source>
        <translation>Adresát</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/html/html_export.cpp" line="382"/>
        <source>Name</source>
        <translation>Jméno</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/html/html_export.cpp" line="383"/>
        <source>Address</source>
        <translation>Adresa</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/html/html_export.cpp" line="384"/>
        <location filename="../../src/datovka_shared/html/html_export.cpp" line="458"/>
        <location filename="../../src/datovka_shared/html/html_export.cpp" line="474"/>
        <location filename="../../src/datovka_shared/html/html_export.cpp" line="741"/>
        <source>Data-Box ID</source>
        <translation>ID schránky</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/html/html_export.cpp" line="386"/>
        <location filename="../../src/datovka_shared/html/html_export.cpp" line="460"/>
        <location filename="../../src/datovka_shared/html/html_export.cpp" line="476"/>
        <location filename="../../src/datovka_shared/html/html_export.cpp" line="742"/>
        <source>Data-Box Type</source>
        <translation>Typ datové schránky</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/html/html_export.cpp" line="389"/>
        <location filename="../../src/datovka_shared/html/html_export.cpp" line="463"/>
        <source>Message Author</source>
        <translation>Odesílající osoba</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/html/html_export.cpp" line="405"/>
        <source>Delivery Info</source>
        <translation>Dodejka</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/html/html_export.cpp" line="408"/>
        <source>Acceptance Info</source>
        <translation>Doručenka</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/html/html_export.cpp" line="412"/>
        <location filename="../../src/datovka_shared/html/html_export.cpp" line="425"/>
        <source>Message Envelope</source>
        <translation>Obálka datové zprávy</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/html/html_export.cpp" line="420"/>
        <source>Message Delivery Info</source>
        <translation>Dodejka datové zprávy</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/html/html_export.cpp" line="422"/>
        <source>Message Acceptance Info</source>
        <translation>Doručenka datové zprávy</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/html/html_export.cpp" line="429"/>
        <location filename="../../src/datovka_shared/html/html_export.cpp" line="559"/>
        <location filename="../../src/datovka_shared/html/html_export.cpp" line="619"/>
        <location filename="../../src/datovka_shared/html/html_export.cpp" line="717"/>
        <source>Subject</source>
        <translation>Předmět</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/html/html_export.cpp" line="430"/>
        <location filename="../../src/datovka_shared/html/html_export.cpp" line="558"/>
        <location filename="../../src/datovka_shared/html/html_export.cpp" line="618"/>
        <location filename="../../src/datovka_shared/html/html_export.cpp" line="716"/>
        <source>Message ID</source>
        <translation>ID zprávy</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/html/html_export.cpp" line="432"/>
        <location filename="../../src/datovka_shared/html/html_export.cpp" line="561"/>
        <location filename="../../src/datovka_shared/html/html_export.cpp" line="623"/>
        <source>Message Type</source>
        <translation>Typ zprávy</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/html/html_export.cpp" line="436"/>
        <location filename="../../src/datovka_shared/html/html_export.cpp" line="528"/>
        <location filename="../../src/datovka_shared/html/html_export.cpp" line="564"/>
        <location filename="../../src/datovka_shared/html/html_export.cpp" line="667"/>
        <location filename="../../src/datovka_shared/html/html_export.cpp" line="682"/>
        <source>Message State</source>
        <translation>Stav zprávy</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/html/html_export.cpp" line="440"/>
        <location filename="../../src/datovka_shared/html/html_export.cpp" line="526"/>
        <location filename="../../src/datovka_shared/html/html_export.cpp" line="568"/>
        <location filename="../../src/datovka_shared/html/html_export.cpp" line="670"/>
        <location filename="../../src/datovka_shared/html/html_export.cpp" line="679"/>
        <location filename="../../src/datovka_shared/html/html_export.cpp" line="721"/>
        <source>Acceptance Time</source>
        <translation>Čas doručení</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/html/html_export.cpp" line="443"/>
        <location filename="../../src/datovka_shared/html/html_export.cpp" line="524"/>
        <location filename="../../src/datovka_shared/html/html_export.cpp" line="571"/>
        <location filename="../../src/datovka_shared/html/html_export.cpp" line="673"/>
        <location filename="../../src/datovka_shared/html/html_export.cpp" line="677"/>
        <source>Delivery Time</source>
        <translation>Čas dodání</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/html/html_export.cpp" line="447"/>
        <location filename="../../src/datovka_shared/html/html_export.cpp" line="620"/>
        <location filename="../../src/datovka_shared/html/html_export.cpp" line="723"/>
        <source>Attachment Size</source>
        <translation>Velikost příloh</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/html/html_export.cpp" line="740"/>
        <source>Data-Box Info</source>
        <translation>Informace o schránce</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/html/html_export.cpp" line="744"/>
        <source>Open Addressing</source>
        <translation>Otevřené adresování</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/html/html_export.cpp" line="746"/>
        <source>Yes</source>
        <translation>Ano</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/html/html_export.cpp" line="746"/>
        <source>No</source>
        <translation>Ne</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/html/html_export.cpp" line="747"/>
        <source>Data-Box State</source>
        <translation>Stav schránky</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/html/html_export.cpp" line="750"/>
        <source>Upper Data-Box ID</source>
        <translation>ID nadřízené schránky</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/html/html_export.cpp" line="754"/>
        <source>OVM ID</source>
        <translation>OVM ID</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/html/html_export.cpp" line="758"/>
        <location filename="../../src/datovka_shared/html/html_export.cpp" line="931"/>
        <source>ROB Identification</source>
        <translation>Ztotožnění s ROB</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/html/html_export.cpp" line="760"/>
        <location filename="../../src/datovka_shared/html/html_export.cpp" line="933"/>
        <source>identified</source>
        <translation>ztotožněn</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/html/html_export.cpp" line="760"/>
        <location filename="../../src/datovka_shared/html/html_export.cpp" line="933"/>
        <source>not identified</source>
        <translation>není ztotožněn</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/html/html_export.cpp" line="764"/>
        <source>Data-Box Owner Info</source>
        <translation>Informace o majiteli datové schránky</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/html/html_export.cpp" line="766"/>
        <location filename="../../src/datovka_shared/html/html_export.cpp" line="850"/>
        <source>Company Name</source>
        <translation>Název firmy</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/html/html_export.cpp" line="770"/>
        <location filename="../../src/datovka_shared/html/html_export.cpp" line="854"/>
        <source>IČ</source>
        <translation>IČ</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/html/html_export.cpp" line="773"/>
        <location filename="../../src/datovka_shared/html/html_export.cpp" line="857"/>
        <source>Given Names</source>
        <translation>Jména</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/html/html_export.cpp" line="777"/>
        <location filename="../../src/datovka_shared/html/html_export.cpp" line="861"/>
        <source>Last Name</source>
        <translation>Příjmení</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/html/html_export.cpp" line="783"/>
        <location filename="../../src/datovka_shared/html/html_export.cpp" line="865"/>
        <source>Date of Birth</source>
        <translation>Datum narození</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/html/html_export.cpp" line="787"/>
        <source>City of Birth</source>
        <translation>Místo narození</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/html/html_export.cpp" line="791"/>
        <source>County of Birth</source>
        <translation>Okres narození</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/html/html_export.cpp" line="795"/>
        <source>State of Birth</source>
        <translation>Stát narození</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/html/html_export.cpp" line="801"/>
        <location filename="../../src/datovka_shared/html/html_export.cpp" line="905"/>
        <source>Street of Residence</source>
        <translation>Sídlo - ulice</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/html/html_export.cpp" line="805"/>
        <location filename="../../src/datovka_shared/html/html_export.cpp" line="875"/>
        <source>Number in Street</source>
        <translation>Číslo orientační</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/html/html_export.cpp" line="809"/>
        <location filename="../../src/datovka_shared/html/html_export.cpp" line="879"/>
        <source>Number in Municipality</source>
        <translation>Číslo popisné</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/html/html_export.cpp" line="813"/>
        <location filename="../../src/datovka_shared/html/html_export.cpp" line="891"/>
        <location filename="../../src/datovka_shared/html/html_export.cpp" line="913"/>
        <source>Zip Code</source>
        <translation>PSČ</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/html/html_export.cpp" line="817"/>
        <source>District of Residence</source>
        <translation>Sídlo - část obce</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/html/html_export.cpp" line="821"/>
        <location filename="../../src/datovka_shared/html/html_export.cpp" line="909"/>
        <source>City of Residence</source>
        <translation>Sídlo - město</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/html/html_export.cpp" line="825"/>
        <location filename="../../src/datovka_shared/html/html_export.cpp" line="917"/>
        <source>State of Residence</source>
        <translation>Sídlo - stát</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/html/html_export.cpp" line="829"/>
        <source>Nationality</source>
        <translation>Státní občanství</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/html/html_export.cpp" line="833"/>
        <location filename="../../src/datovka_shared/html/html_export.cpp" line="899"/>
        <source>RUIAN Address Code</source>
        <translation>Adresní kód RUIAN</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/html/html_export.cpp" line="848"/>
        <source>User Info</source>
        <translation>Informace o uživateli</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/html/html_export.cpp" line="871"/>
        <source>Street</source>
        <translation>Ulice</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/html/html_export.cpp" line="883"/>
        <source>District</source>
        <translation>Část obce</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/html/html_export.cpp" line="887"/>
        <source>City</source>
        <translation>Město</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/html/html_export.cpp" line="895"/>
        <source>State</source>
        <translation>Stát</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/html/html_export.cpp" line="924"/>
        <source>Permissions</source>
        <translation>Oprávnění</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/html/html_export.cpp" line="927"/>
        <source>Unique User ISDS ID</source>
        <translation>Unikátní ID uživatele ISDS</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/html/html_export.cpp" line="952"/>
        <source>Local Database Location</source>
        <translation>Umístění místní databáze</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/html/html_export.cpp" line="955"/>
        <source>Data box has no own message database associated.</source>
        <translation>Schránka nemá přiřazenou žádnou vlastní databázi zpráv.</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/html/html_export.cpp" line="958"/>
        <source>local database file</source>
        <translation>místní databázový soubor</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/html/html_export.cpp" line="983"/>
        <source>All Messages</source>
        <translation>Všechny zprávy</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/html/html_export.cpp" line="984"/>
        <source>Received</source>
        <translation>Přijaté</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/html/html_export.cpp" line="985"/>
        <source>Sent</source>
        <translation>Odeslané</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/html/html_export.cpp" line="994"/>
        <source>Signature</source>
        <translation>Podpis</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/html/html_export.cpp" line="996"/>
        <source>Message Signature</source>
        <translation>Podpis zprávy</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/html/html_export.cpp" line="999"/>
        <source>Signing Certificate</source>
        <translation>Podepisující certifikát</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/html/html_export.cpp" line="1002"/>
        <source>Time Stamp</source>
        <translation>Časové razítko</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/html/html_export.cpp" line="946"/>
        <source>Password Expiration</source>
        <translation>Expirace hesla</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/html/html_export.cpp" line="505"/>
        <location filename="../../src/datovka_shared/html/html_export.cpp" line="508"/>
        <location filename="../../src/datovka_shared/html/html_export.cpp" line="628"/>
        <source>no</source>
        <translation>ne</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/html/html_export.cpp" line="485"/>
        <location filename="../../src/datovka_shared/html/html_export.cpp" line="488"/>
        <source>not specified</source>
        <translation>nezadáno</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/html/html_export.cpp" line="506"/>
        <location filename="../../src/datovka_shared/html/html_export.cpp" line="626"/>
        <source>Prohibited Acceptance through Fiction</source>
        <translation>Zakázáno doručení fikcí</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/html/html_export.cpp" line="513"/>
        <source>Message Events</source>
        <translation>Události zprávy</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/html/html_export.cpp" line="557"/>
        <location filename="../../src/datovka_shared/html/html_export.cpp" line="617"/>
        <location filename="../../src/datovka_shared/html/html_export.cpp" line="715"/>
        <source>General</source>
        <translation>Obecné</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/html/html_export.cpp" line="593"/>
        <location filename="../../src/datovka_shared/html/html_export.cpp" line="662"/>
        <source>Additional</source>
        <translation>Doplňující</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/html/html_export.cpp" line="598"/>
        <location filename="../../src/datovka_shared/html/html_export.cpp" line="688"/>
        <source>Events</source>
        <translation>Události</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/html/html_export.cpp" line="536"/>
        <source>Message Attachments</source>
        <translation>Přílohy zprávy</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/html/html_export.cpp" line="698"/>
        <source>Records Management Location</source>
        <translation>Umístění ve spisové službě</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/html/html_export.cpp" line="700"/>
        <source>Location</source>
        <translation>Umístění</translation>
    </message>
</context>
<context>
    <name>FileDialogue</name>
    <message>
        <location filename="../../qml/components/FileDialogue.qml" line="36"/>
        <location filename="../../qml/components/FileDialogue.qml" line="36"/>
        <source>Choose a File</source>
        <translation>Vyberte soubor</translation>
    </message>
</context>
<context>
    <name>Files</name>
    <message>
        <location filename="../../src/files.cpp" line="288"/>
        <location filename="../../src/files.cpp" line="317"/>
        <location filename="../../src/files.cpp" line="326"/>
        <source>Open attachment error</source>
        <translation>Chyba otevření přílohy</translation>
    </message>
    <message>
        <location filename="../../src/files.cpp" line="318"/>
        <location filename="../../src/files.cpp" line="327"/>
        <source>There is no application to open this file format.</source>
        <translation>Neexistuje aplikace, která pracuje s tímto formátem souborů.</translation>
    </message>
    <message>
        <location filename="../../src/files.cpp" line="319"/>
        <location filename="../../src/files.cpp" line="328"/>
        <source>File: &apos;%1&apos;</source>
        <translation>Soubor: &apos;%1&apos;</translation>
    </message>
    <message>
        <location filename="../../src/files.cpp" line="289"/>
        <source>Cannot save selected file to disk for opening.</source>
        <translation>Nelze uložit zvolený soubor pro otevření.</translation>
    </message>
    <message>
        <location filename="../../src/files.cpp" line="685"/>
        <source>Path: &apos;%1&apos;</source>
        <translation>Cesta: &apos;%1&apos;</translation>
    </message>
    <message>
        <location filename="../../src/files.cpp" line="401"/>
        <location filename="../../src/files.cpp" line="763"/>
        <source>ZFO missing</source>
        <translation>Chybějící ZFO</translation>
    </message>
    <message>
        <location filename="../../src/files.cpp" line="402"/>
        <location filename="../../src/files.cpp" line="764"/>
        <source>ZFO message is not present in local database.</source>
        <translation>ZFO se zprávou není uloženo v lokální databázi.</translation>
    </message>
    <message>
        <location filename="../../src/files.cpp" line="403"/>
        <location filename="../../src/files.cpp" line="765"/>
        <source>Download complete message again to obtain it.</source>
        <translation>Stáhněte znovu celou datovou zprávu.</translation>
    </message>
    <message>
        <location filename="../../src/files.cpp" line="683"/>
        <source>Saving Successful</source>
        <translation>Úspěšně uloženo</translation>
    </message>
    <message>
        <location filename="../../src/files.cpp" line="684"/>
        <source>Files have been saved.</source>
        <translation>Soubory byly úspěšně uloženy.</translation>
    </message>
    <message>
        <location filename="../../src/files.cpp" line="687"/>
        <source>Saving Failed</source>
        <translation>Ukládání selhalo</translation>
    </message>
    <message>
        <location filename="../../src/files.cpp" line="688"/>
        <source>Files have not been saved.</source>
        <translation>Soubory nebyly uloženy.</translation>
    </message>
    <message>
        <location filename="../../src/files.cpp" line="689"/>
        <source>Please check whether the application has permissions to access the storage.</source>
        <translation>Ověřte prosím, zda aplikace má oprávnění k přístupu do úložiště.</translation>
    </message>
</context>
<context>
    <name>FilterBar</name>
    <message>
        <location filename="../../qml/components/FilterBar.qml" line="51"/>
        <location filename="../../qml/components/FilterBar.qml" line="51"/>
        <source>Enter filter text</source>
        <translation>Zadejte vyhledávaný text</translation>
    </message>
</context>
<context>
    <name>InputDialogue</name>
    <message>
        <location filename="../../qml/dialogues/InputDialogue.qml" line="114"/>
        <location filename="../../qml/dialogues/InputDialogue.qml" line="114"/>
        <source>Close</source>
        <translation>Zavřít</translation>
    </message>
    <message>
        <location filename="../../qml/dialogues/InputDialogue.qml" line="123"/>
        <location filename="../../qml/dialogues/InputDialogue.qml" line="123"/>
        <source>OK</source>
        <translation>OK</translation>
    </message>
</context>
<context>
    <name>IosHelper</name>
    <message>
        <location filename="../../src/auxiliaries/ios_helper.cpp" line="59"/>
        <source>iCloud error</source>
        <translation>Chyba iCloudu</translation>
    </message>
    <message>
        <location filename="../../src/auxiliaries/ios_helper.cpp" line="60"/>
        <source>Unable to access iCloud account. Open the settings and check your iCloud settings.</source>
        <translation>Nelze použít účet pro iCloud. Ověřte nastavení iCloudu na Vašem zařízení.</translation>
    </message>
    <message>
        <location filename="../../src/auxiliaries/ios_helper.cpp" line="80"/>
        <source>Unable to access iCloud!</source>
        <translation>Nelze použít iCloud!</translation>
    </message>
    <message>
        <location filename="../../src/auxiliaries/ios_helper.cpp" line="85"/>
        <source>Cannot create subdirectory &apos;%1&apos; in iCloud.</source>
        <translation>V iCloudu nelze vytvořit podadresář &apos;%1&apos;.</translation>
    </message>
    <message>
        <location filename="../../src/auxiliaries/ios_helper.cpp" line="90"/>
        <source>File &apos;%1&apos; already exists in iCloud.</source>
        <translation>Soubor &apos;%1&apos; již v iCloudu existuje.</translation>
    </message>
    <message>
        <location filename="../../src/auxiliaries/ios_helper.cpp" line="94"/>
        <source>File &apos;%1&apos; upload failed.</source>
        <translation>Nahrávání souboru &apos;%1&apos; selhalo.</translation>
    </message>
    <message>
        <location filename="../../src/auxiliaries/ios_helper.cpp" line="97"/>
        <source>File &apos;%1&apos; has been stored into iCloud.</source>
        <translation>Soubor &apos;%1&apos; byl uložen do iCloudu.</translation>
    </message>
    <message>
        <location filename="../../src/auxiliaries/ios_helper.cpp" line="113"/>
        <source>Saved to iCloud</source>
        <translation>Uloženo do iCloudu</translation>
    </message>
    <message>
        <location filename="../../src/auxiliaries/ios_helper.cpp" line="114"/>
        <source>Files have been stored into iCloud.</source>
        <translation>Soubory byly uloženy do iCloudu.</translation>
    </message>
    <message>
        <location filename="../../src/auxiliaries/ios_helper.cpp" line="115"/>
        <source>Path: &apos;%1&apos;</source>
        <translation>Cesta: &apos;%1&apos;</translation>
    </message>
    <message>
        <location filename="../../src/auxiliaries/ios_helper.cpp" line="121"/>
        <source>iCloud Problem</source>
        <translation>Problém s iCloudem</translation>
    </message>
    <message>
        <location filename="../../src/auxiliaries/ios_helper.cpp" line="122"/>
        <source>Files have not been saved!</source>
        <translation>Soubory nebyly uloženy!</translation>
    </message>
</context>
<context>
    <name>Isds</name>
    <message>
        <location filename="../../src/datovka_shared/isds/to_text_conversion.cpp" line="64"/>
        <source>User type</source>
        <translation>Typ uživatele</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/isds/to_text_conversion.cpp" line="70"/>
        <source>Full name</source>
        <translation>Plné jméno</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/isds/to_text_conversion.cpp" line="77"/>
        <source>Date of birth</source>
        <translation>Datum narození</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/isds/to_text_conversion.cpp" line="83"/>
        <source>City of birth</source>
        <translation>Místo narození</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/isds/to_text_conversion.cpp" line="87"/>
        <source>County of birth</source>
        <translation>Okres narození</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/isds/to_text_conversion.cpp" line="91"/>
        <source>RUIAN address code</source>
        <translation>Adresní kód RUIAN</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/isds/to_text_conversion.cpp" line="95"/>
        <source>Full address</source>
        <translation>Adresa pobytu</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/isds/to_text_conversion.cpp" line="100"/>
        <source>ROB identification</source>
        <translation>Ztotožnění s ROB</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/isds/to_text_conversion.cpp" line="101"/>
        <source>identified</source>
        <translation>ztotožněn</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/isds/to_text_conversion.cpp" line="101"/>
        <source>not identified</source>
        <translation>není ztotožněn</translation>
    </message>
</context>
<context>
    <name>IsdsConversion</name>
    <message>
        <location filename="../../src/isds/conversion/isds_conversion.cpp" line="35"/>
        <source>Primary user</source>
        <translation>Oprávněná osoba</translation>
    </message>
    <message>
        <location filename="../../src/isds/conversion/isds_conversion.cpp" line="38"/>
        <source>Entrusted user</source>
        <translation>Pověřená osoba</translation>
    </message>
    <message>
        <location filename="../../src/isds/conversion/isds_conversion.cpp" line="41"/>
        <source>Administrator</source>
        <translation>Administrátor</translation>
    </message>
    <message>
        <location filename="../../src/isds/conversion/isds_conversion.cpp" line="43"/>
        <source>Official</source>
        <translation>Systém ISDS</translation>
    </message>
    <message>
        <location filename="../../src/isds/conversion/isds_conversion.cpp" line="45"/>
        <source>Virtual</source>
        <translation>Virtuální osoba</translation>
    </message>
    <message>
        <location filename="../../src/isds/conversion/isds_conversion.cpp" line="47"/>
        <source>Liquidator</source>
        <translation>Likvidátor</translation>
    </message>
    <message>
        <location filename="../../src/isds/conversion/isds_conversion.cpp" line="49"/>
        <source>Receiver</source>
        <translation>Nucený správce</translation>
    </message>
    <message>
        <location filename="../../src/isds/conversion/isds_conversion.cpp" line="51"/>
        <source>Guardian</source>
        <translation>Opatrovník</translation>
    </message>
</context>
<context>
    <name>IsdsWrapper</name>
    <message>
        <location filename="../../src/isds/isds_wrapper.cpp" line="263"/>
        <source>No data box for zfo import.</source>
        <translation>Není žádná schránka pro import zfo.</translation>
    </message>
    <message>
        <location filename="../../src/isds/isds_wrapper.cpp" line="291"/>
        <source>ZFO messages cannot be imported because there is no active data box for their verification.</source>
        <translation>ZFO zprávy nemohou být importovány, protože není k dispozici žádná aktivní schránka pro jejich verifikaci.</translation>
    </message>
    <message>
        <location filename="../../src/isds/isds_wrapper.cpp" line="401"/>
        <source>SMS code sending request has been cancelled.</source>
        <translation>Požadavek na zaslání SMS kódu byl zrušen.</translation>
    </message>
    <message>
        <location filename="../../src/isds/isds_wrapper.cpp" line="500"/>
        <location filename="../../src/isds/isds_wrapper.cpp" line="506"/>
        <source>Error sending message</source>
        <translation>Chyba při odesílání zprávy</translation>
    </message>
    <message>
        <location filename="../../src/isds/isds_wrapper.cpp" line="501"/>
        <source>Pre-paid transfer charges for message reply have been enabled.</source>
        <translation>Bylo povoleno předplatit poplatky za odpověď na tuto zprávu.</translation>
    </message>
    <message>
        <location filename="../../src/isds/isds_wrapper.cpp" line="502"/>
        <source>The sender reference number must be specified in the additional section in order to send the message.</source>
        <translation>Pro odeslání inicializační poštovní zprávy musí být zadáno referenční číslo odesílatele.</translation>
    </message>
    <message>
        <location filename="../../src/isds/isds_wrapper.cpp" line="507"/>
        <source>Message uses the offered payment of transfer charges by the recipient.</source>
        <translation>Využít nabízenou platbu za zasílací poplatky adresátem.</translation>
    </message>
    <message>
        <location filename="../../src/isds/isds_wrapper.cpp" line="508"/>
        <source>The recipient reference number must be specified in the additional section in order to send the message.</source>
        <translation>Pro odeslání předplacené odpovědní poštovní zprávy musí být zadáno referenční číslo adresáta.</translation>
    </message>
    <message>
        <location filename="../../src/isds/isds_wrapper.cpp" line="655"/>
        <source>Recipient data-box ID &apos;%1&apos; isn&apos;t active.</source>
        <translation>Datová schránka adresáta &quot;%1&quot; není aktivní.</translation>
    </message>
    <message>
        <location filename="../../src/isds/isds_wrapper.cpp" line="662"/>
        <source>Recipient with data-box ID &apos;%1&apos; doesn&apos;t exist.</source>
        <translation>Adresát s datovou schránkou &quot;%1&quot; neexistuje.</translation>
    </message>
    <message>
        <location filename="../../src/isds/isds_wrapper.cpp" line="926"/>
        <source>Communication code for data box &apos;%1&apos; is required.</source>
        <translation>Je vyžadován kód pro schránku &quot;%1&quot;.</translation>
    </message>
    <message>
        <location filename="../../src/isds/isds_wrapper.cpp" line="940"/>
        <source>Password for data box &apos;%1&apos; missing.</source>
        <translation>Chybí heslo pro schránku &quot;%1&quot;.</translation>
    </message>
    <message>
        <location filename="../../src/isds/isds_wrapper.cpp" line="954"/>
        <source>Certificate password for data box &apos;%1&apos; is required.</source>
        <translation>Je vyžadováno heslo k certifikátu pro schránku &quot;%1&quot;.</translation>
    </message>
    <message>
        <location filename="../../src/isds/isds_wrapper.cpp" line="970"/>
        <source>New data box problem: %1</source>
        <translation>Chyba vytváření schránky: %1</translation>
    </message>
    <message>
        <location filename="../../src/isds/isds_wrapper.cpp" line="971"/>
        <source>New data box could not be created for username &apos;%1&apos;.</source>
        <translation>Nebylo možné vytvořit schránku pro uživatelské jméno &quot;%1&quot;.</translation>
    </message>
    <message>
        <location filename="../../src/isds/isds_wrapper.cpp" line="993"/>
        <source>Security code for data box &apos;%1&apos; required</source>
        <translation>Je vyžadován bezpečnostní kód pro schránku &quot;%1&quot;</translation>
    </message>
    <message>
        <location filename="../../src/isds/isds_wrapper.cpp" line="1006"/>
        <source>SMS code for data box &apos;%1&apos; required</source>
        <translation>Je vyžadován SMS kód pro schránku &quot;%1&quot;</translation>
    </message>
    <message>
        <location filename="../../src/isds/isds_wrapper.cpp" line="942"/>
        <source>Enter password</source>
        <translation>Zadejte heslo</translation>
    </message>
    <message>
        <location filename="../../src/isds/isds_wrapper.cpp" line="251"/>
        <source>No zfo files or selected folder.</source>
        <translation>Žádný zfo soubor nebo vybraná složka.</translation>
    </message>
    <message>
        <location filename="../../src/isds/isds_wrapper.cpp" line="192"/>
        <source>Find data box: %1</source>
        <translation>Vyhledat schránku: %1</translation>
    </message>
    <message>
        <location filename="../../src/isds/isds_wrapper.cpp" line="193"/>
        <source>Failed finding data box to phrase &apos;%1&apos;.</source>
        <translation>Vyhledávání schránky k řetězci &quot;%1&quot; selhalo.</translation>
    </message>
    <message>
        <location filename="../../src/isds/isds_wrapper.cpp" line="272"/>
        <source>No zfo files in the selected directory.</source>
        <translation>Vybraná složka neobsahuje zfo soubory.</translation>
    </message>
    <message>
        <location filename="../../src/isds/isds_wrapper.cpp" line="278"/>
        <source>No zfo file for import.</source>
        <translation>Žádný zfo soubor pro import.</translation>
    </message>
    <message>
        <location filename="../../src/isds/isds_wrapper.cpp" line="632"/>
        <location filename="../../src/isds/isds_wrapper.cpp" line="637"/>
        <location filename="../../src/isds/isds_wrapper.cpp" line="642"/>
        <source>Unknown Message Type</source>
        <translation>Neznámý typ zprávy</translation>
    </message>
    <message>
        <location filename="../../src/isds/isds_wrapper.cpp" line="633"/>
        <source>No information about the recipient data box &apos;%1&apos; could be obtained. It is unknown whether public or commercial messages can be sent to this recipient.</source>
        <translation>Nebylo možné obdržet žádné informace o datové schránce adresáta &quot;%1&quot;. Není známo, zda tomuto adresátu lze poslat veřejnou či poštovní datovou zprávu.</translation>
    </message>
    <message>
        <location filename="../../src/isds/isds_wrapper.cpp" line="638"/>
        <source>No commercial message to the recipient data box &apos;%1&apos; can be sent. It is unknown whether a public messages can be sent to this recipient.</source>
        <translation>Do datové schránky adresáta &quot;%1&quot; není možné zaslat poštovní datovou zprávu. Není známo, zda tomuto adresátu lze poslat veřejnou datovou zprávu.</translation>
    </message>
    <message>
        <location filename="../../src/isds/isds_wrapper.cpp" line="643"/>
        <source>No public message to the recipient data box &apos;%1&apos; can be sent. It is unknown whether a commercial messages can be sent to this recipient.</source>
        <translation>Do datové schránky adresáta &quot;%1&quot; není možné zaslat veřejnou datovou zprávu. Není známo, zda tomuto adresátu lze poslat poštovní datovou zprávu.</translation>
    </message>
    <message>
        <location filename="../../src/isds/isds_wrapper.cpp" line="647"/>
        <source>Cannot send message</source>
        <translation>Zprávu nelze odeslat</translation>
    </message>
    <message>
        <location filename="../../src/isds/isds_wrapper.cpp" line="648"/>
        <source>No public data message nor a commercial data message (PDZ) can be sent to the recipient data box &apos;%1&apos;.</source>
        <translation>Do datové schránky adresáta &quot;%1&quot; nelze zaslat veřejnou ani poštovní datovou zprávu.</translation>
    </message>
    <message>
        <location filename="../../src/isds/isds_wrapper.cpp" line="649"/>
        <source>Receiving of PDZs has been disabled in the recipient data box or there are no available payment methods for PDZs.</source>
        <translation>Příjem PDZ byl v datové schránce adresáta zakázán nebo nejsou dostupné platební metody pro PDZ.</translation>
    </message>
    <message>
        <location filename="../../src/isds/isds_wrapper.cpp" line="654"/>
        <source>Data box is not active</source>
        <translation>Datová schránka není aktivní</translation>
    </message>
    <message>
        <location filename="../../src/isds/isds_wrapper.cpp" line="656"/>
        <location filename="../../src/isds/isds_wrapper.cpp" line="663"/>
        <source>The message cannot be delivered.</source>
        <translation>Zprávu nelze dodat.</translation>
    </message>
    <message>
        <location filename="../../src/isds/isds_wrapper.cpp" line="661"/>
        <source>Wrong Recipient</source>
        <translation>Chybný adresát</translation>
    </message>
    <message>
        <location filename="../../src/isds/isds_wrapper.cpp" line="807"/>
        <source>Internal error.</source>
        <translation>Interní chyba.</translation>
    </message>
    <message>
        <location filename="../../src/isds/isds_wrapper.cpp" line="925"/>
        <source>Enter communication code: %1</source>
        <translation>Zadejte komunikační kód: %1</translation>
    </message>
    <message>
        <location filename="../../src/isds/isds_wrapper.cpp" line="928"/>
        <source>Enter communication code</source>
        <translation>Zadejte komunikační kód</translation>
    </message>
    <message>
        <location filename="../../src/isds/isds_wrapper.cpp" line="972"/>
        <source>Note: You must change your password via the ISDS web portal if this is your first login to the data box. You can start using mobile Datovka to access the data box after changing.</source>
        <translation>Poznámka: Pokud se přihlašujete do datové schránky poprvé, tak musíte použit webový portál ISDS, kde si po prvním přihlášení změníte heslo. Po změně hesla pak můžete začít používat Datovku pro přístup do datové schránky.</translation>
    </message>
    <message>
        <location filename="../../src/isds/isds_wrapper.cpp" line="975"/>
        <source>Username could not be changed on the new username &apos;%1&apos;.</source>
        <translation>Uživatelské jméno se nepodařilo změnit na nové &quot;%1&quot;.</translation>
    </message>
    <message>
        <location filename="../../src/isds/isds_wrapper.cpp" line="1039"/>
        <source>ZFO import has been finished with the result:</source>
        <translation>ZFO import byl dokončen s výsledkem:</translation>
    </message>
    <message>
        <location filename="../../src/isds/isds_wrapper.cpp" line="1075"/>
        <source>Message has been sent to &lt;b&gt;&apos;%1&apos;&lt;/b&gt; &lt;i&gt;(%2)&lt;/i&gt; as message number &lt;b&gt;%3&lt;/b&gt;.</source>
        <translation>Zpráva byla odeslána adresátu &lt;b&gt;&apos;%1&apos;&lt;/b&gt; &lt;i&gt;(%2)&lt;/i&gt; pod číslem &lt;b&gt;%3&lt;/b&gt;.</translation>
    </message>
    <message>
        <location filename="../../src/isds/isds_wrapper.cpp" line="1086"/>
        <source>Message has NOT been sent to &lt;b&gt;&apos;%1&apos;&lt;/b&gt; &lt;i&gt;(%2)&lt;/i&gt;. ISDS returns: %3</source>
        <translation>Zpráva nebyla odeslána adresátu &lt;b&gt;&apos;%1&apos;&lt;/b&gt; &lt;i&gt;(%2)&lt;/i&gt;. ISDS vrací: %3</translation>
    </message>
    <message>
        <location filename="../../src/isds/isds_wrapper.cpp" line="1128"/>
        <source>PDZ sending: unavailable</source>
        <translation>Odesílání PDZ: nedostupné</translation>
    </message>
    <message>
        <location filename="../../src/isds/isds_wrapper.cpp" line="1143"/>
        <source>PDZ: active</source>
        <translation>PDZ: aktivní</translation>
    </message>
    <message>
        <location filename="../../src/isds/isds_wrapper.cpp" line="1146"/>
        <source>Subsidised by data box &apos;%1&apos;.</source>
        <translation>Dotovaná datovou schránkou &quot;%1&quot;.</translation>
    </message>
    <message>
        <location filename="../../src/isds/isds_wrapper.cpp" line="1149"/>
        <source>Contract with Czech post</source>
        <translation>Smlouva s Českou poštou</translation>
    </message>
    <message>
        <location filename="../../src/isds/isds_wrapper.cpp" line="1153"/>
        <source>Remaining credit: %1 Kč</source>
        <translation>Zbývající kredit: %1 Kč</translation>
    </message>
    <message>
        <location filename="../../src/isds/isds_wrapper.cpp" line="974"/>
        <source>Username change: %1</source>
        <translation>Změna uživatelského jména: %1</translation>
    </message>
    <message>
        <location filename="../../src/isds/isds_wrapper.cpp" line="939"/>
        <source>Enter password: %1</source>
        <translation>Zadejte heslo: %1</translation>
    </message>
    <message>
        <location filename="../../src/isds/isds_wrapper.cpp" line="992"/>
        <source>Enter security code: %1</source>
        <translation>Zadejte bezpečnostní kód: %1</translation>
    </message>
    <message>
        <location filename="../../src/isds/isds_wrapper.cpp" line="953"/>
        <source>Enter certificate password: %1</source>
        <translation>Zadejte heslo k certifikátu: %1</translation>
    </message>
    <message>
        <location filename="../../src/isds/isds_wrapper.cpp" line="1005"/>
        <source>Enter SMS code: %1</source>
        <translation>Zadejte SMS kód: %1</translation>
    </message>
    <message>
        <location filename="../../src/isds/isds_wrapper.cpp" line="1007"/>
        <source>Enter SMS code</source>
        <translation>Zadejte SMS kód</translation>
    </message>
    <message>
        <location filename="../../src/isds/isds_wrapper.cpp" line="994"/>
        <source>Enter security code</source>
        <translation>Zadejte bezpečnostní kód</translation>
    </message>
    <message>
        <location filename="../../src/isds/isds_wrapper.cpp" line="668"/>
        <source>Recipient Search Failed</source>
        <translation>Chyba vyhledávání adresáta</translation>
    </message>
    <message>
        <location filename="../../src/isds/isds_wrapper.cpp" line="669"/>
        <source>Information about recipient data box &apos;%1&apos; couldn&apos;t be obtained.</source>
        <translation>Nebylo možné obdržet informace o datové schránce adresáta &quot;%1&quot;.</translation>
    </message>
    <message>
        <location filename="../../src/isds/isds_wrapper.cpp" line="670"/>
        <source>The message may not be delivered.</source>
        <translation>Zpráva nemusí být dodána.</translation>
    </message>
    <message>
        <location filename="../../src/isds/isds_wrapper.cpp" line="764"/>
        <source>SMS code: %1</source>
        <translation>SMS kód: %1</translation>
    </message>
    <message>
        <location filename="../../src/isds/isds_wrapper.cpp" line="765"/>
        <source>Account &apos;%1&apos; requires authentication with an SMS code.</source>
        <translation>Účet &quot;%1&quot; vyžaduje přihlášení SMS kódem.</translation>
    </message>
    <message>
        <location filename="../../src/isds/isds_wrapper.cpp" line="767"/>
        <source>Do you want to request the SMS code now?</source>
        <translation>Chcete zaslat SMS kód?</translation>
    </message>
    <message>
        <location filename="../../src/isds/isds_wrapper.cpp" line="956"/>
        <source>Enter certificate password</source>
        <translation>Zadejte heslo k certifikátu</translation>
    </message>
    <message>
        <location filename="../../src/isds/isds_wrapper.cpp" line="978"/>
        <source>Login problem: %1</source>
        <translation>Problém s přihlášením: %1</translation>
    </message>
    <message>
        <location filename="../../src/isds/isds_wrapper.cpp" line="979"/>
        <source>Error while logging in with username &apos;%1&apos;.</source>
        <translation>Chyba při přihlášení uživatelským jménem &apos;%1&apos;.</translation>
    </message>
</context>
<context>
    <name>Login</name>
    <message>
        <location filename="../../src/isds/isds_login.cpp" line="328"/>
        <source>MEP confirmation timeout.</source>
        <translation>Vypršela délka MEP požadavku.</translation>
    </message>
    <message>
        <location filename="../../src/isds/isds_login.cpp" line="333"/>
        <source>MEP login was cancelled.</source>
        <translation>Přihlašování pomocí MEP přerušeno.</translation>
    </message>
    <message>
        <location filename="../../src/isds/isds_login.cpp" line="338"/>
        <source>Wrong MEP login credentials. Invalid username or communication code.</source>
        <translation>Nesprávné údaje pro přihlášení Mobilním klíčem. Neplatné přihlašovací jméno nebo komunikační kód.</translation>
    </message>
    <message>
        <location filename="../../src/isds/isds_login.cpp" line="343"/>
        <source>MEP login failed.</source>
        <translation>Přihlašování pomocí MEP selhalo.</translation>
    </message>
</context>
<context>
    <name>MessageBox</name>
    <message>
        <location filename="../../qml/components/MessageBox.qml" line="147"/>
        <location filename="../../qml/components/MessageBox.qml" line="147"/>
        <source>Yes</source>
        <translation>Ano</translation>
    </message>
    <message>
        <location filename="../../qml/components/MessageBox.qml" line="156"/>
        <location filename="../../qml/components/MessageBox.qml" line="156"/>
        <source>No</source>
        <translation>Ne</translation>
    </message>
</context>
<context>
    <name>MessageList</name>
    <message>
        <location filename="../../qml/components/MessageList.qml" line="93"/>
        <location filename="../../qml/components/MessageList.qml" line="93"/>
        <source>Delivered</source>
        <translation>Čas dodání</translation>
    </message>
    <message>
        <location filename="../../qml/components/MessageList.qml" line="99"/>
        <location filename="../../qml/components/MessageList.qml" line="99"/>
        <source>Accepted</source>
        <translation>Čas doručení</translation>
    </message>
    <message>
        <location filename="../../qml/components/MessageList.qml" line="127"/>
        <location filename="../../qml/components/MessageList.qml" line="127"/>
        <source>ID</source>
        <translation>ID</translation>
    </message>
    <message>
        <location filename="../../qml/components/MessageList.qml" line="140"/>
        <location filename="../../qml/components/MessageList.qml" line="140"/>
        <source>Unread message from sender</source>
        <translation>Nepřečtená zpráva od odesílatele</translation>
    </message>
    <message>
        <location filename="../../qml/components/MessageList.qml" line="140"/>
        <location filename="../../qml/components/MessageList.qml" line="140"/>
        <source>Read message from sender</source>
        <translation>Přečtená zpráva od odesílatele</translation>
    </message>
    <message>
        <location filename="../../qml/components/MessageList.qml" line="142"/>
        <location filename="../../qml/components/MessageList.qml" line="142"/>
        <source>Message to receiver</source>
        <translation>Zpráva adresátu</translation>
    </message>
    <message>
        <location filename="../../qml/components/MessageList.qml" line="145"/>
        <location filename="../../qml/components/MessageList.qml" line="145"/>
        <source>Subject</source>
        <translation>Předmět</translation>
    </message>
</context>
<context>
    <name>Messages</name>
    <message>
        <location filename="../../src/messages.cpp" line="921"/>
        <source>The message database for username &apos;%1&apos; couldn&apos;t be converted into the new format. A new empty database file has been created.</source>
        <translation>Databáze zpráv k uživatelskému jménu &apos;%1&apos; nemohla být převedena do nového formátu. Byla vytvořena nová prázdná databáze.</translation>
    </message>
    <message>
        <location filename="../../src/messages.cpp" line="492"/>
        <source>Move</source>
        <translation>Přesunout</translation>
    </message>
    <message>
        <location filename="../../src/messages.cpp" line="493"/>
        <source>Create new</source>
        <translation>Nově vytvořit</translation>
    </message>
</context>
<context>
    <name>MvCrrVbhData</name>
    <message>
        <location filename="../../src/datovka_shared/gov_services/service/gov_mv_crr_vbh.cpp" line="91"/>
        <source>Driving licence ID</source>
        <translation>Číslo řidičského oprávnění</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/gov_services/service/gov_mv_crr_vbh.cpp" line="92"/>
        <source>Enter driving licence ID without spaces</source>
        <translation>Zadejte číslo řidičského průkazu bez mezer</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/gov_services/service/gov_mv_crr_vbh.cpp" line="102"/>
        <source>Data-box owner name</source>
        <translation>Jméno vlastníka datové schránky</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/gov_services/service/gov_mv_crr_vbh.cpp" line="112"/>
        <source>Data-box owner surname</source>
        <translation>Příjmení vlastníka datové schránky</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/gov_services/service/gov_mv_crr_vbh.cpp" line="122"/>
        <source>Birth date</source>
        <translation>Datum narození</translation>
    </message>
</context>
<context>
    <name>MvIrVpData</name>
    <message>
        <location filename="../../src/datovka_shared/gov_services/service/gov_mv_ir_vp.cpp" line="516"/>
        <source>Identification number (IČO)</source>
        <translation>IČO</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/gov_services/service/gov_mv_ir_vp.cpp" line="517"/>
        <source>Enter identification number (IČO)</source>
        <translation>Zadejte IČO</translation>
    </message>
</context>
<context>
    <name>MvRtVtData</name>
    <message>
        <location filename="../../src/datovka_shared/gov_services/service/gov_mv_rt_vt.cpp" line="106"/>
        <source>Data-box owner name</source>
        <translation>Jméno vlastníka datové schránky</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/gov_services/service/gov_mv_rt_vt.cpp" line="116"/>
        <source>Data-box owner surname</source>
        <translation>Příjmení vlastníka datové schránky</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/gov_services/service/gov_mv_rt_vt.cpp" line="126"/>
        <source>Birth date</source>
        <translation>Datum narození</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/gov_services/service/gov_mv_rt_vt.cpp" line="136"/>
        <source>Birth region</source>
        <translation>Okres (narození)</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/gov_services/service/gov_mv_rt_vt.cpp" line="146"/>
        <source>City</source>
        <translation>Město</translation>
    </message>
</context>
<context>
    <name>MvRtpoVtData</name>
    <message>
        <location filename="../../src/datovka_shared/gov_services/service/gov_mv_rtpo_vt.cpp" line="521"/>
        <source>Identification number (IČO)</source>
        <translation>IČO</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/gov_services/service/gov_mv_rtpo_vt.cpp" line="522"/>
        <source>Enter identification number (IČO)</source>
        <translation>Zadejte IČO</translation>
    </message>
</context>
<context>
    <name>MvSkdVpData</name>
    <message>
        <location filename="../../src/datovka_shared/gov_services/service/gov_mv_skd_vp.cpp" line="517"/>
        <source>Identification number (IČO)</source>
        <translation>IČO</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/gov_services/service/gov_mv_skd_vp.cpp" line="518"/>
        <source>Enter identification number (IČO)</source>
        <translation>Zadejte IČO</translation>
    </message>
</context>
<context>
    <name>MvVrVpData</name>
    <message>
        <location filename="../../src/datovka_shared/gov_services/service/gov_mv_vr_vp.cpp" line="550"/>
        <source>Identification number (IČO)</source>
        <translation>IČO</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/gov_services/service/gov_mv_vr_vp.cpp" line="551"/>
        <source>Enter identification number (IČO)</source>
        <translation>Zadejte IČO</translation>
    </message>
</context>
<context>
    <name>MvZrVpData</name>
    <message>
        <location filename="../../src/datovka_shared/gov_services/service/gov_mv_zr_vp.cpp" line="516"/>
        <source>Identification number (IČO)</source>
        <translation>IČO</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/gov_services/service/gov_mv_zr_vp.cpp" line="517"/>
        <source>Enter identification number (IČO)</source>
        <translation>Zadejte IČO</translation>
    </message>
</context>
<context>
    <name>PageAboutApp</name>
    <message>
        <location filename="../../qml/pages/PageAboutApp.qml" line="32"/>
        <location filename="../../qml/pages/PageAboutApp.qml" line="32"/>
        <source>About Datovka</source>
        <translation>O Datovce</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageAboutApp.qml" line="54"/>
        <location filename="../../qml/pages/PageAboutApp.qml" line="54"/>
        <source>Open application home page.</source>
        <translation>Otevřít domovskou stránku aplikace.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageAboutApp.qml" line="65"/>
        <location filename="../../qml/pages/PageAboutApp.qml" line="65"/>
        <source>Datovka</source>
        <translation>Datovka</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageAboutApp.qml" line="65"/>
        <location filename="../../qml/pages/PageAboutApp.qml" line="65"/>
        <source>Free mobile data-box client.</source>
        <translation>Svobodný mobilní klient pro Datové schránky.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageAboutApp.qml" line="73"/>
        <location filename="../../qml/pages/PageAboutApp.qml" line="73"/>
        <source>Version: %1</source>
        <translation>Verze: %1</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageAboutApp.qml" line="108"/>
        <location filename="../../qml/pages/PageAboutApp.qml" line="108"/>
        <source>Learn more...</source>
        <translation>Dozvědět se více...</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageAboutApp.qml" line="81"/>
        <location filename="../../qml/pages/PageAboutApp.qml" line="142"/>
        <location filename="../../qml/pages/PageAboutApp.qml" line="81"/>
        <location filename="../../qml/pages/PageAboutApp.qml" line="142"/>
        <source>This application is being developed by %1 as open source.</source>
        <translation>Tuto aplikaci vyvíjí %1 jako open source.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageAboutApp.qml" line="83"/>
        <location filename="../../qml/pages/PageAboutApp.qml" line="83"/>
        <source>It&apos;s distributed free of charge. It doesn&apos;t contain advertisements or in-app purchases.</source>
        <translation>Je distribuována zdarma. Neobsahuje reklamy ani nákupy v aplikaci.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageAboutApp.qml" line="85"/>
        <location filename="../../qml/pages/PageAboutApp.qml" line="85"/>
        <source>We want to provide an easy-to-use data-box client.</source>
        <translation>Chceme poskytovat snadno použitelného klienta pro datové schránky.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageAboutApp.qml" line="87"/>
        <location filename="../../qml/pages/PageAboutApp.qml" line="87"/>
        <source>Do you want to support us in this effort?</source>
        <translation>Chcete nás v tomto záměru podpořit?</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageAboutApp.qml" line="89"/>
        <location filename="../../qml/pages/PageAboutApp.qml" line="89"/>
        <source>Make a donation.</source>
        <translation>Darujte příspěvek.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageAboutApp.qml" line="96"/>
        <location filename="../../qml/pages/PageAboutApp.qml" line="96"/>
        <source>The behaviour of the ISDS environment and this application is different from the behaviour of an e-mail client. Delivery of data messages is governed by law.</source>
        <translation>Chování systému datových schránek ISDS a této aplikace je rozdílné od chování běžných e-mailových klientů. Doručovaní datových zpráv se řídí zákonem.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageAboutApp.qml" line="150"/>
        <location filename="../../qml/pages/PageAboutApp.qml" line="150"/>
        <source>If you have any problems with the application or you have found any errors or you just have an idea how to improve this application please contact us using the following e-mail address:</source>
        <translation>Máte-li s touto aplikací nějaký problém, našli jste nějakou chybu nebo máte nápad, jak tuto aplikaci zlepšit, prosím, napište nám na uvedenou e-mailovou adresu:</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageAboutApp.qml" line="156"/>
        <location filename="../../qml/pages/PageAboutApp.qml" line="156"/>
        <source>Show News</source>
        <translation>Ukázat novinky</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageAboutApp.qml" line="157"/>
        <location filename="../../qml/pages/PageAboutApp.qml" line="157"/>
        <source>What&apos;s new?</source>
        <translation>Co je nového?</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageAboutApp.qml" line="178"/>
        <location filename="../../qml/pages/PageAboutApp.qml" line="178"/>
        <source>This application is available as it is. Usage of this application is at own risk. The association CZ.NIC is in no circumstances liable for any damage directly or indirectly caused by using or not using this application.</source>
        <translation>Tato aplikace je k dispozici tak, jak je. Použití této aplikace je na vlastní nebezpečí. Sdružení CZ.NIC není v žádném případě odpovědno za škody přímo či nepřímo způsobené použitím nebo nepoužitím této aplikace.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageAboutApp.qml" line="180"/>
        <location filename="../../qml/pages/PageAboutApp.qml" line="180"/>
        <source>CZ.NIC isn&apos;t the operator nor the administrator of the Data Box Information System (ISDS). The operation of the ISDS is regulated by Czech law and regulations.</source>
        <translation>CZ.NIC není provozovatelem ani správcem Informačního systému datových schránek (ISDS). Provoz ISDS je řízen zákony a dalšími předpisy České republiky.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageAboutApp.qml" line="189"/>
        <location filename="../../qml/pages/PageAboutApp.qml" line="189"/>
        <source>This software is distributed under the %1 licence.</source>
        <translation>Tento software je distribuován pod licencí %1.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageAboutApp.qml" line="189"/>
        <location filename="../../qml/pages/PageAboutApp.qml" line="189"/>
        <source>GNU GPL version 3</source>
        <translation>GNU GPL verze 3</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageAboutApp.qml" line="211"/>
        <location filename="../../qml/pages/PageAboutApp.qml" line="211"/>
        <source>About Datovka.</source>
        <translation>O Datovce.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageAboutApp.qml" line="212"/>
        <location filename="../../qml/pages/PageAboutApp.qml" line="212"/>
        <source>About</source>
        <translation>O aplikaci</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageAboutApp.qml" line="217"/>
        <location filename="../../qml/pages/PageAboutApp.qml" line="217"/>
        <source>Datovka development.</source>
        <translation>Vývoj Datovky.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageAboutApp.qml" line="218"/>
        <location filename="../../qml/pages/PageAboutApp.qml" line="218"/>
        <source>Development</source>
        <translation>Vývoj</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageAboutApp.qml" line="223"/>
        <location filename="../../qml/pages/PageAboutApp.qml" line="223"/>
        <source>Licence.</source>
        <translation>Licence.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageAboutApp.qml" line="224"/>
        <location filename="../../qml/pages/PageAboutApp.qml" line="224"/>
        <source>Licence</source>
        <translation>Licence</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageAboutApp.qml" line="98"/>
        <location filename="../../qml/pages/PageAboutApp.qml" line="98"/>
        <source>Read the &lt;a href=&quot;%1&quot;&gt;user manual&lt;/a&gt; for more information about the application behaviour.</source>
        <translation>Přečtěte si &lt;a href=&quot;%1&quot;&gt;uživatelskou příručku&lt;/a&gt; o chování aplikace.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageAboutApp.qml" line="131"/>
        <location filename="../../qml/pages/PageAboutApp.qml" line="131"/>
        <source>Open the home page of the CZ.NIC association.</source>
        <translation>Otevřít domovskou stránku sdružení CZ.NIC.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageAboutApp.qml" line="106"/>
        <location filename="../../qml/pages/PageAboutApp.qml" line="106"/>
        <source>Did you know that there is a desktop version of Datovka?</source>
        <translation>Věděli jste, že existuje i verze Datovky pro desktop?</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageAboutApp.qml" line="126"/>
        <location filename="../../qml/pages/PageAboutApp.qml" line="126"/>
        <source>Powered by</source>
        <translation>Vyvíjeno</translation>
    </message>
</context>
<context>
    <name>PageAccountDetail</name>
    <message>
        <location filename="../../qml/pages/PageAccountDetail.qml" line="55"/>
        <location filename="../../qml/pages/PageAccountDetail.qml" line="55"/>
        <source>Data-Box Info</source>
        <translation>Informace o schránce</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageAccountDetail.qml" line="63"/>
        <location filename="../../qml/pages/PageAccountDetail.qml" line="63"/>
        <source>Refresh information about the data box.</source>
        <translation>Znovu načíst informace o schránce.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageAccountDetail.qml" line="89"/>
        <location filename="../../qml/pages/PageAccountDetail.qml" line="89"/>
        <source>Information about the data box haven&apos;t been downloaded yet.</source>
        <translation>Žádné informace o schránce doposud nebyly staženy.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageAccountDetail.qml" line="111"/>
        <location filename="../../qml/pages/PageAccountDetail.qml" line="111"/>
        <source>Data-Box Info: %1</source>
        <translation>Informace o schránce: %1</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageAccountDetail.qml" line="112"/>
        <location filename="../../qml/pages/PageAccountDetail.qml" line="112"/>
        <source>Failed to get data-box info for username &apos;%1&apos;.</source>
        <translation>Nezdařilo se stáhnout informace o schránce pro uživatelské jméno &quot;%1&quot;.</translation>
    </message>
</context>
<context>
    <name>PageAccountList</name>
    <message>
        <location filename="../../qml/pages/PageAccountList.qml" line="227"/>
        <location filename="../../qml/pages/PageAccountList.qml" line="227"/>
        <source>Database problem: %1</source>
        <translation>Problém s databází: %1</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageAccountList.qml" line="69"/>
        <location filename="../../qml/pages/PageAccountList.qml" line="69"/>
        <source>Synchronising...</source>
        <translation>Synchronizuji...</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageAccountList.qml" line="69"/>
        <location filename="../../qml/pages/PageAccountList.qml" line="69"/>
        <source>Last synchronisation</source>
        <translation>Poslední synchronizace</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageAccountList.qml" line="127"/>
        <location filename="../../qml/pages/PageAccountList.qml" line="127"/>
        <source>Data Boxes</source>
        <translation>Schránky</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageAccountList.qml" line="134"/>
        <location filename="../../qml/pages/PageAccountList.qml" line="134"/>
        <source>Synchronise all data boxes.</source>
        <translation>Synchronizovat všechny schránky.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageAccountList.qml" line="213"/>
        <location filename="../../qml/pages/PageAccountList.qml" line="213"/>
        <source>%1: messages</source>
        <translation>%1: zpráv</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageAccountList.qml" line="214"/>
        <location filename="../../qml/pages/PageAccountList.qml" line="214"/>
        <source>Failed to download list of messages for username &apos;%1&apos;.</source>
        <translation>Chyba při stahování seznamu zpráv pro uživatelské jméno &quot;%1&quot;.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageAccountList.qml" line="228"/>
        <location filename="../../qml/pages/PageAccountList.qml" line="228"/>
        <source>The message database for username &apos;%1&apos; appears to be corrupt. Synchronisation won&apos;t most likely be able to write new data and therefore is going to be aborted.</source>
        <translation>Databáze zpráv pro uživatele &apos;%1&apos; se zdá být poškozená. Synchronizace nebude pravděpodobně schopná zapsat nová data a bude proto přerušena.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageAccountList.qml" line="229"/>
        <location filename="../../qml/pages/PageAccountList.qml" line="229"/>
        <source>Do you want to restore the damaged database from a local back-up copy?</source>
        <translation>Přejete si obnovit poškozenou databázi z lokální záložní kopie?</translation>
    </message>
</context>
<context>
    <name>PageAccountLogo</name>
    <message>
        <source>Choose a image file</source>
        <translation type="obsolete">Vyberte soubor s obrázkem</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageAccountLogo.qml" line="66"/>
        <location filename="../../qml/pages/PageAccountLogo.qml" line="66"/>
        <source>Choose an Image File</source>
        <translation>Vyberte soubor s obrázkem</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageAccountLogo.qml" line="105"/>
        <location filename="../../qml/pages/PageAccountLogo.qml" line="105"/>
        <source>Data-Box Logo</source>
        <translation>Logo datové schránky</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageAccountLogo.qml" line="121"/>
        <location filename="../../qml/pages/PageAccountLogo.qml" line="121"/>
        <source>Here you can change the default data-box logo to a custom image.</source>
        <translation>Zde si můžete změnit výchozí logo datové schránky na vlastní obrázek.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageAccountLogo.qml" line="123"/>
        <location filename="../../qml/pages/PageAccountLogo.qml" line="123"/>
        <source>The set logo will be used only locally. Logos aren&apos;t shared between application installations nor the ISDS.</source>
        <translation>Nastavené logo bude použito pouze lokálně. Loga nejsou sdílena mezi instalacemi aplikace ani ISDS.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageAccountLogo.qml" line="125"/>
        <location filename="../../qml/pages/PageAccountLogo.qml" line="125"/>
        <source>Allowed image formats are: %1</source>
        <translation>Dovolené formáty obrázků jsou: %1</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageAccountLogo.qml" line="127"/>
        <location filename="../../qml/pages/PageAccountLogo.qml" line="127"/>
        <source>The recommended image size is %1x%1 pixels.</source>
        <translation>Doporučená velikost obrázku je %1x%1 bodů.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageAccountLogo.qml" line="145"/>
        <location filename="../../qml/pages/PageAccountLogo.qml" line="145"/>
        <source>Set New Logo</source>
        <translation>Nastavit nové logo</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageAccountLogo.qml" line="150"/>
        <location filename="../../qml/pages/PageAccountLogo.qml" line="150"/>
        <source>Reset Logo</source>
        <translation>Resetovat logo</translation>
    </message>
</context>
<context>
    <name>PageBackupData</name>
    <message>
        <location filename="../../qml/pages/PageBackupData.qml" line="128"/>
        <location filename="../../qml/pages/PageBackupData.qml" line="128"/>
        <source>Target</source>
        <translation>Cíl</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageBackupData.qml" line="64"/>
        <location filename="../../qml/pages/PageBackupData.qml" line="64"/>
        <source>An error occurred when backing up data to target location. Backup has been cancelled.</source>
        <translation>Nastala chyba při zálohování dat do cílového umístění. Zálohování bylo přerušeno.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageBackupData.qml" line="63"/>
        <location filename="../../qml/pages/PageBackupData.qml" line="63"/>
        <source>Backup Error</source>
        <translation>Chyba zálohování</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageBackupData.qml" line="65"/>
        <location filename="../../qml/pages/PageBackupData.qml" line="65"/>
        <source>Application data haven&apos;t been backed up to target location.</source>
        <translation>Data nebyla zálohována do cílového umístění.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageBackupData.qml" line="74"/>
        <location filename="../../qml/pages/PageBackupData.qml" line="107"/>
        <location filename="../../qml/pages/PageBackupData.qml" line="74"/>
        <location filename="../../qml/pages/PageBackupData.qml" line="107"/>
        <source>Back up Data Boxes</source>
        <translation>Záloha schránek</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageBackupData.qml" line="74"/>
        <location filename="../../qml/pages/PageBackupData.qml" line="74"/>
        <source>Back up Data Box</source>
        <translation>Záloha schránky</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageBackupData.qml" line="75"/>
        <location filename="../../qml/pages/PageBackupData.qml" line="75"/>
        <source>The action allows to back up data of selected data boxes into a ZIP archive.</source>
        <translation>Tato akce umožňuje zálohovat data vybraných schránek do ZIP archivu.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageBackupData.qml" line="75"/>
        <location filename="../../qml/pages/PageBackupData.qml" line="75"/>
        <source>The action allows to back up data of the data box &apos;%1&apos; into a ZIP archive.</source>
        <translation>Tato akce umožňuje zálohovat data schrány &quot;%1&quot; do ZIP archivu.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageBackupData.qml" line="77"/>
        <location filename="../../qml/pages/PageBackupData.qml" line="77"/>
        <source>Archive will be stored into the application sandbox and uploaded into the iCloud.</source>
        <translation>Archiv bude uložen do pracovního prostoru aplikace a následně nahrán do iCloud.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageBackupData.qml" line="78"/>
        <location filename="../../qml/pages/PageBackupData.qml" line="78"/>
        <source>Application sandbox and iCloud.</source>
        <translation>Pracovní prostor aplikace a iCloud.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageBackupData.qml" line="143"/>
        <location filename="../../qml/pages/PageBackupData.qml" line="143"/>
        <source>Create ZIP Archive</source>
        <translation>Vytvořit ZIP archiv</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageBackupData.qml" line="150"/>
        <location filename="../../qml/pages/PageBackupData.qml" line="150"/>
        <source>Select all data boxes</source>
        <translation>Vybrat všechny schránky</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageBackupData.qml" line="144"/>
        <location filename="../../qml/pages/PageBackupData.qml" line="144"/>
        <source>Proceed with operation.</source>
        <translation>Pokračovat v operaci.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageBackupData.qml" line="177"/>
        <location filename="../../qml/pages/PageBackupData.qml" line="177"/>
        <source>Select %1.</source>
        <translation>Vybrat %1.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageBackupData.qml" line="177"/>
        <location filename="../../qml/pages/PageBackupData.qml" line="177"/>
        <source>Deselect %1.</source>
        <translation>Zrušit výběr %1.</translation>
    </message>
</context>
<context>
    <name>PageChangePassword</name>
    <message>
        <location filename="../../qml/pages/PageChangePassword.qml" line="45"/>
        <location filename="../../qml/pages/PageChangePassword.qml" line="45"/>
        <source>Enter the current password, twice the new password and the SMS code.</source>
        <translation>Zadejte aktuální heslo, dvakrát nové heslo a kód SMS.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageChangePassword.qml" line="45"/>
        <location filename="../../qml/pages/PageChangePassword.qml" line="51"/>
        <location filename="../../qml/pages/PageChangePassword.qml" line="56"/>
        <location filename="../../qml/pages/PageChangePassword.qml" line="45"/>
        <location filename="../../qml/pages/PageChangePassword.qml" line="51"/>
        <location filename="../../qml/pages/PageChangePassword.qml" line="56"/>
        <source>User is logged in to ISDS.</source>
        <translation>Uživatel je přihlášen do ISDS.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageChangePassword.qml" line="46"/>
        <location filename="../../qml/pages/PageChangePassword.qml" line="46"/>
        <source>Enter SMS code</source>
        <translation>Zadejte SMS kód</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageChangePassword.qml" line="52"/>
        <location filename="../../qml/pages/PageChangePassword.qml" line="52"/>
        <source>Enter security code</source>
        <translation>Zadejte bezpečnostní kód</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageChangePassword.qml" line="101"/>
        <location filename="../../qml/pages/PageChangePassword.qml" line="101"/>
        <source>Change password</source>
        <translation>Změnit heslo</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageChangePassword.qml" line="139"/>
        <location filename="../../qml/pages/PageChangePassword.qml" line="139"/>
        <source>Log in now into data box.</source>
        <translation>Přihlásit se do schránky.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageChangePassword.qml" line="219"/>
        <location filename="../../qml/pages/PageChangePassword.qml" line="219"/>
        <source>Old and new password must both be filled in.</source>
        <translation>Staré a nové heslo musí být obě vyplněna.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageChangePassword.qml" line="229"/>
        <location filename="../../qml/pages/PageChangePassword.qml" line="229"/>
        <source>Wrong password format. The new password must contain at least 8 characters including at least 1 number and at least 1 upper-case letter.</source>
        <translation>Špatný formát hesla. Nové heslo musí být alepoň 8 znaků dlouhé včetně nejméně 1 čísla a nejméně 1 velkého písmene.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageChangePassword.qml" line="235"/>
        <location filename="../../qml/pages/PageChangePassword.qml" line="235"/>
        <source>Security/SMS code must be filled in.</source>
        <translation>Bezpečnostní/SMS kód musí být vyplněn.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageChangePassword.qml" line="248"/>
        <location filename="../../qml/pages/PageChangePassword.qml" line="248"/>
        <source>Password change failed.</source>
        <translation>Změna hesla selhala.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageChangePassword.qml" line="187"/>
        <location filename="../../qml/pages/PageChangePassword.qml" line="187"/>
        <source>Enter the current password to be able to send the SMS code.</source>
        <translation>Zadejte současné heslo, aby bylo možné zaslat SMS kód.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageChangePassword.qml" line="153"/>
        <location filename="../../qml/pages/PageChangePassword.qml" line="153"/>
        <source>Current password</source>
        <translation>Aktuální heslo</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageChangePassword.qml" line="164"/>
        <location filename="../../qml/pages/PageChangePassword.qml" line="164"/>
        <source>New password</source>
        <translation>Nové heslo</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageChangePassword.qml" line="175"/>
        <location filename="../../qml/pages/PageChangePassword.qml" line="175"/>
        <source>Confirm the new password</source>
        <translation>Potvrďte nové heslo</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageChangePassword.qml" line="51"/>
        <location filename="../../qml/pages/PageChangePassword.qml" line="51"/>
        <source>Enter the current password, twice the new password and the security code.</source>
        <translation>Zadejte aktuální heslo, dvakrát nové heslo a bezpečnostní kód.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageChangePassword.qml" line="56"/>
        <location filename="../../qml/pages/PageChangePassword.qml" line="56"/>
        <source>Enter the current password and twice the new password.</source>
        <translation>Zadejte aktuální heslo a dvakrát nové heslo.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageChangePassword.qml" line="76"/>
        <location filename="../../qml/pages/PageChangePassword.qml" line="76"/>
        <source>User must be logged in to the data box to be able to change the password.</source>
        <translation>Uživatel musí být přihlášen do datové schránky, aby bylo možné změnit heslo.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageChangePassword.qml" line="86"/>
        <location filename="../../qml/pages/PageChangePassword.qml" line="86"/>
        <source>SMS code has been sent...</source>
        <translation>SMS kód byl odeslán...</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageChangePassword.qml" line="91"/>
        <location filename="../../qml/pages/PageChangePassword.qml" line="91"/>
        <source>SMS code cannot be sent. Entered wrong current password or the ISDS server is out of order.</source>
        <translation>Nelze zaslat SMS kód. Bylo zadáno špatné současné heslo nebo ISDS server je mimo provoz.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageChangePassword.qml" line="110"/>
        <location filename="../../qml/pages/PageChangePassword.qml" line="110"/>
        <source>Hide Passwords</source>
        <translation>Skrýt hesla</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageChangePassword.qml" line="110"/>
        <location filename="../../qml/pages/PageChangePassword.qml" line="110"/>
        <source>Show Passwords</source>
        <translation>Zobrazit hesla</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageChangePassword.qml" line="131"/>
        <location filename="../../qml/pages/PageChangePassword.qml" line="131"/>
        <source>Enter the current password and the new password.</source>
        <translation>Zadejte aktuální heslo a dvakrát nové heslo.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageChangePassword.qml" line="137"/>
        <location filename="../../qml/pages/PageChangePassword.qml" line="137"/>
        <source>Log in Now</source>
        <translation>Přihlásit se nyní</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageChangePassword.qml" line="184"/>
        <location filename="../../qml/pages/PageChangePassword.qml" line="184"/>
        <source>Send SMS Code</source>
        <translation>Zaslat kód SMS</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageChangePassword.qml" line="192"/>
        <location filename="../../qml/pages/PageChangePassword.qml" line="192"/>
        <source>SMS Code: %1</source>
        <translation>SMS kód: %1</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageChangePassword.qml" line="193"/>
        <location filename="../../qml/pages/PageChangePassword.qml" line="193"/>
        <source>Data box &apos;%1&apos; requires a SMS code for password changing.</source>
        <translation>Schránka &quot;%1&quot; vyžaduje SMS kód pro změnu hesla.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageChangePassword.qml" line="243"/>
        <location filename="../../qml/pages/PageChangePassword.qml" line="243"/>
        <source>Successful Password Change</source>
        <translation>Úspěšná změna hesla</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageChangePassword.qml" line="244"/>
        <location filename="../../qml/pages/PageChangePassword.qml" line="244"/>
        <source>Password for username &apos;%1&apos; has been changed.</source>
        <translation>Heslo pro uživatelské jméno &quot;%1&quot; bylo změněno.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageChangePassword.qml" line="245"/>
        <location filename="../../qml/pages/PageChangePassword.qml" line="245"/>
        <source>The new password has been saved.</source>
        <translation>Nové heslo bylo uloženo.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageChangePassword.qml" line="266"/>
        <location filename="../../qml/pages/PageChangePassword.qml" line="266"/>
        <source>Data box settings</source>
        <translation>Nastavení schránky</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageChangePassword.qml" line="194"/>
        <location filename="../../qml/pages/PageChangePassword.qml" line="194"/>
        <source>Do you want to send the SMS code now?</source>
        <translation>Chcete zaslat SMS kód?</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageChangePassword.qml" line="206"/>
        <location filename="../../qml/pages/PageChangePassword.qml" line="206"/>
        <source>Enter OTP code</source>
        <translation>Zadejte OTP kód</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageChangePassword.qml" line="214"/>
        <location filename="../../qml/pages/PageChangePassword.qml" line="214"/>
        <source>Accept Changes</source>
        <translation>Přijmout změny</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageChangePassword.qml" line="216"/>
        <location filename="../../qml/pages/PageChangePassword.qml" line="216"/>
        <source>Accept all changes.</source>
        <translation>Přijmout všechny změny.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageChangePassword.qml" line="224"/>
        <location filename="../../qml/pages/PageChangePassword.qml" line="224"/>
        <source>The new password fields don&apos;t match.</source>
        <translation>Obsah polí s novým heslem si neodpovídá.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageChangePassword.qml" line="262"/>
        <location filename="../../qml/pages/PageChangePassword.qml" line="262"/>
        <source>This will change the password on the ISDS server. You won&apos;t be able to log in to ISDS without the new password. If your current password has already expired then you must log into the ISDS web portal to change it.</source>
        <translation>Tato operace změní heslo na serveru ISDS. Bez nového hesla se nebudete moci přihlásit do ISDS. Pokud již vypršela platnost vašeho aktuálního hesla, tak se pro jeho změnu musíte přihlásit do webového portálu ISDS.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageChangePassword.qml" line="266"/>
        <location filename="../../qml/pages/PageChangePassword.qml" line="266"/>
        <source>If you just want to update the password because it has been changed elsewhere then go to the </source>
        <translation>Pokud chcete jen opravit nastavení hesla, protože bylo změněno odjinud, tak použijte </translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageChangePassword.qml" line="270"/>
        <location filename="../../qml/pages/PageChangePassword.qml" line="270"/>
        <source>Keep a copy of your login credentials and store it in a safe place where only authorised persons have access to.</source>
        <translation>Udržujte si kopii přihlašovacích údajů a uchovávejte ji na bezpečném místě, kam má přístup jen oprávněná osoba.</translation>
    </message>
</context>
<context>
    <name>PageContactList</name>
    <message>
        <location filename="../../qml/pages/PageContactList.qml" line="69"/>
        <location filename="../../qml/pages/PageContactList.qml" line="69"/>
        <source>Contacts</source>
        <translation>Kontakty</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageContactList.qml" line="77"/>
        <location filename="../../qml/pages/PageContactList.qml" line="77"/>
        <source>Filter data boxes.</source>
        <translation>Filtrovat datové schránky.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageContactList.qml" line="124"/>
        <location filename="../../qml/pages/PageContactList.qml" line="124"/>
        <source>No data box found in contacts.</source>
        <translation>Nenalezeny žádné schránky.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageContactList.qml" line="135"/>
        <location filename="../../qml/pages/PageContactList.qml" line="135"/>
        <source>Append selected</source>
        <translation>Připojit vybrané</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageContactList.qml" line="137"/>
        <location filename="../../qml/pages/PageContactList.qml" line="137"/>
        <source>Append selected data boxes into recipient list.</source>
        <translation>Připojit vybrané schránky do seznamu adresátů.</translation>
    </message>
</context>
<context>
    <name>PageConvertDatabase</name>
    <message>
        <location filename="../../qml/pages/PageConvertDatabase.qml" line="63"/>
        <location filename="../../qml/pages/PageConvertDatabase.qml" line="63"/>
        <source>The application requires the conversion of database files into a new format. Don&apos;t close or shut down the application until the conversion process finishes. The application needs to be restarted in order to load the changes after conversion.</source>
        <translation>Aplikace vyžaduje převod databází do nového formátu. Nezavírejte nebo neukončujte aplikaci dříve, než se převod ukončí. Aplikace vyžaduje restart pro načtení změn po konverzi.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageConvertDatabase.qml" line="48"/>
        <location filename="../../qml/pages/PageConvertDatabase.qml" line="48"/>
        <source>Database Conversion</source>
        <translation>Konverze databáze</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageConvertDatabase.qml" line="67"/>
        <location filename="../../qml/pages/PageConvertDatabase.qml" line="67"/>
        <source>Start Conversion</source>
        <translation>Začít s převodem</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageConvertDatabase.qml" line="74"/>
        <location filename="../../qml/pages/PageConvertDatabase.qml" line="74"/>
        <source>Database conversion finished. Some message databases couldn&apos;t be converted. The application needs to be restarted in order to load the changes.</source>
        <translation>Převod databází ukončen. Některé databáze zpráv nemohly být převedeny. Aplikace vyžaduje restart pro načtení změn.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageConvertDatabase.qml" line="94"/>
        <location filename="../../qml/pages/PageConvertDatabase.qml" line="94"/>
        <source>Database conversion finished. All message databases have successfully been converted. The application needs to be restarted in order to load the changes.</source>
        <translation>Převod databází ukončen. Všechny databáze zpráv byly úspěšně převedeny. Aplikace vyžaduje restart pro načtení změn.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageConvertDatabase.qml" line="84"/>
        <location filename="../../qml/pages/PageConvertDatabase.qml" line="84"/>
        <source>Restart</source>
        <translation>Restartovat</translation>
    </message>
</context>
<context>
    <name>PageDataboxDetail</name>
    <message>
        <location filename="../../qml/pages/PageDataboxDetail.qml" line="43"/>
        <location filename="../../qml/pages/PageDataboxDetail.qml" line="43"/>
        <source>Data-Box Info</source>
        <translation>Informace o schránce</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageDataboxDetail.qml" line="77"/>
        <location filename="../../qml/pages/PageDataboxDetail.qml" line="77"/>
        <source>Information about the data box &apos;%1&apos; aren&apos;t available. The data box may have been temporarily disabled. It is likely that you cannot send data message to this data box.</source>
        <translation>Informace o datové schránce &quot;%1&quot; nejsou dostupné. Datová schránka mohla být dočasně znepřístupněna. Je možné, že do této datové schránky nebudete moci odeslat datovou zprávu.</translation>
    </message>
</context>
<context>
    <name>PageDataboxSearch</name>
    <message>
        <location filename="../../qml/pages/PageDataboxSearch.qml" line="151"/>
        <location filename="../../qml/pages/PageDataboxSearch.qml" line="151"/>
        <source>IČO</source>
        <translation>IČO</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageDataboxSearch.qml" line="152"/>
        <location filename="../../qml/pages/PageDataboxSearch.qml" line="152"/>
        <source>Data-Box ID</source>
        <translation>ID schránky</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageDataboxSearch.qml" line="158"/>
        <location filename="../../qml/pages/PageDataboxSearch.qml" line="158"/>
        <source>Enter sought phrase</source>
        <translation>Zadejte hledaný text</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageDataboxSearch.qml" line="181"/>
        <location filename="../../qml/pages/PageDataboxSearch.qml" line="181"/>
        <source>Previous</source>
        <translation>Předchozí</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageDataboxSearch.qml" line="213"/>
        <location filename="../../qml/pages/PageDataboxSearch.qml" line="213"/>
        <source>Next</source>
        <translation>Následující</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageDataboxSearch.qml" line="100"/>
        <location filename="../../qml/pages/PageDataboxSearch.qml" line="100"/>
        <source>Filter data boxes.</source>
        <translation>Filtrovat datové schránky.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageDataboxSearch.qml" line="88"/>
        <location filename="../../qml/pages/PageDataboxSearch.qml" line="88"/>
        <source>Search data boxes.</source>
        <translation>Vyhledat datové schránky.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageDataboxSearch.qml" line="80"/>
        <location filename="../../qml/pages/PageDataboxSearch.qml" line="80"/>
        <source>Find Data Box</source>
        <translation>Vyhledat schránku</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageDataboxSearch.qml" line="132"/>
        <location filename="../../qml/pages/PageDataboxSearch.qml" line="132"/>
        <source>Select type of sought data box</source>
        <translation>Vyberte typ hledané datové schránky</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageDataboxSearch.qml" line="135"/>
        <location filename="../../qml/pages/PageDataboxSearch.qml" line="135"/>
        <source>All data boxes</source>
        <translation>Všechny schránky</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageDataboxSearch.qml" line="136"/>
        <location filename="../../qml/pages/PageDataboxSearch.qml" line="136"/>
        <source>OVM</source>
        <translation>OVM</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageDataboxSearch.qml" line="137"/>
        <location filename="../../qml/pages/PageDataboxSearch.qml" line="137"/>
        <source>PO</source>
        <translation>PO</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageDataboxSearch.qml" line="138"/>
        <location filename="../../qml/pages/PageDataboxSearch.qml" line="138"/>
        <source>PFO</source>
        <translation>PFO</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageDataboxSearch.qml" line="139"/>
        <location filename="../../qml/pages/PageDataboxSearch.qml" line="139"/>
        <source>FO</source>
        <translation>FO</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageDataboxSearch.qml" line="146"/>
        <location filename="../../qml/pages/PageDataboxSearch.qml" line="146"/>
        <source>Select entries to search in</source>
        <translation>Vyberte položky, ve kterých se bude vyhledávat</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageDataboxSearch.qml" line="149"/>
        <location filename="../../qml/pages/PageDataboxSearch.qml" line="149"/>
        <source>All fields</source>
        <translation>Všechny položky</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageDataboxSearch.qml" line="150"/>
        <location filename="../../qml/pages/PageDataboxSearch.qml" line="150"/>
        <source>Address</source>
        <translation>Adresa</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageDataboxSearch.qml" line="241"/>
        <location filename="../../qml/pages/PageDataboxSearch.qml" line="241"/>
        <source>Searching for data boxes ...</source>
        <translation>Vyhledávám schránky ...</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageDataboxSearch.qml" line="292"/>
        <location filename="../../qml/pages/PageDataboxSearch.qml" line="292"/>
        <source>Append Selected</source>
        <translation>Připojit vybrané</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageDataboxSearch.qml" line="294"/>
        <location filename="../../qml/pages/PageDataboxSearch.qml" line="294"/>
        <source>Append selected data boxes into recipient list.</source>
        <translation>Připojit vybrané schránky do seznamu adresátů.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageDataboxSearch.qml" line="344"/>
        <location filename="../../qml/pages/PageDataboxSearch.qml" line="344"/>
        <source>from</source>
        <translation>z</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageDataboxSearch.qml" line="346"/>
        <location filename="../../qml/pages/PageDataboxSearch.qml" line="346"/>
        <source>Found</source>
        <translation>Nalezeno</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageDataboxSearch.qml" line="344"/>
        <location filename="../../qml/pages/PageDataboxSearch.qml" line="344"/>
        <source>Shown</source>
        <translation>Zobrazeno</translation>
    </message>
</context>
<context>
    <name>PageGovService</name>
    <message>
        <location filename="../../qml/pages/PageGovService.qml" line="73"/>
        <location filename="../../qml/pages/PageGovService.qml" line="73"/>
        <source>Service:</source>
        <translation>Služba:</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageGovService.qml" line="94"/>
        <location filename="../../qml/pages/PageGovService.qml" line="147"/>
        <location filename="../../qml/pages/PageGovService.qml" line="94"/>
        <location filename="../../qml/pages/PageGovService.qml" line="147"/>
        <source>Request:</source>
        <translation>Podání:</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageGovService.qml" line="89"/>
        <location filename="../../qml/pages/PageGovService.qml" line="89"/>
        <source>Data Box:</source>
        <translation>Schránka:</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageGovService.qml" line="144"/>
        <location filename="../../qml/pages/PageGovService.qml" line="144"/>
        <source>Send an e-gov request from data box &apos;%1&apos;.</source>
        <translation>Odeslat elektronické podání ze schránky &quot;%1&quot;.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageGovService.qml" line="145"/>
        <location filename="../../qml/pages/PageGovService.qml" line="145"/>
        <source>Send Request</source>
        <translation>Odeslat podání</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageGovService.qml" line="146"/>
        <location filename="../../qml/pages/PageGovService.qml" line="146"/>
        <source>Do you want to send the e-gov request?</source>
        <translation>Přejete si odeslat elektronické podání?</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageGovService.qml" line="148"/>
        <location filename="../../qml/pages/PageGovService.qml" line="148"/>
        <source>Recipient:</source>
        <translation>Adresát:</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageGovService.qml" line="106"/>
        <location filename="../../qml/pages/PageGovService.qml" line="106"/>
        <source>Required information</source>
        <translation>Požadované informace</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageGovService.qml" line="110"/>
        <location filename="../../qml/pages/PageGovService.qml" line="110"/>
        <source>Acquired from data box</source>
        <translation>Obdrženo z datové schránky</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageGovService.qml" line="119"/>
        <location filename="../../qml/pages/PageGovService.qml" line="119"/>
        <source>No user data needed.</source>
        <translation>Nejsou vyžadována uživatelská data.</translation>
    </message>
</context>
<context>
    <name>PageGovServiceList</name>
    <message>
        <location filename="../../qml/pages/PageGovServiceList.qml" line="60"/>
        <location filename="../../qml/pages/PageGovServiceList.qml" line="60"/>
        <source>E-Gov Services</source>
        <translation>Elektronická podání</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageGovServiceList.qml" line="68"/>
        <location filename="../../qml/pages/PageGovServiceList.qml" line="68"/>
        <source>Set filter.</source>
        <translation>Nastavit filtr.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageGovServiceList.qml" line="110"/>
        <location filename="../../qml/pages/PageGovServiceList.qml" line="110"/>
        <source>No available e-government service.</source>
        <translation>Žádné dostupné elektronické podání.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageGovServiceList.qml" line="111"/>
        <location filename="../../qml/pages/PageGovServiceList.qml" line="111"/>
        <source>No e-government service found that matches filter text &apos;%1&apos;.</source>
        <translation>Nenalezeno žádné elektronické podání odpovídající textu &apos;%1&apos;.</translation>
    </message>
</context>
<context>
    <name>PageHeader</name>
    <message>
        <location filename="../../qml/components/PageHeader.qml" line="45"/>
        <location filename="../../qml/components/PageHeader.qml" line="45"/>
        <source>Back.</source>
        <translation>Zpět.</translation>
    </message>
    <message>
        <location filename="../../qml/components/PageHeader.qml" line="52"/>
        <location filename="../../qml/components/PageHeader.qml" line="52"/>
        <source>Application settings.</source>
        <translation>Nastavení aplikace.</translation>
    </message>
</context>
<context>
    <name>PageImportMessage</name>
    <message>
        <location filename="../../qml/pages/PageImportMessage.qml" line="52"/>
        <location filename="../../qml/pages/PageImportMessage.qml" line="52"/>
        <source>Select ZFO Files</source>
        <translation>Vyberte soubory ZFO</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageImportMessage.qml" line="109"/>
        <location filename="../../qml/pages/PageImportMessage.qml" line="109"/>
        <source>ZFO Message Import</source>
        <translation>Import ZFO zpráv</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageImportMessage.qml" line="127"/>
        <location filename="../../qml/pages/PageImportMessage.qml" line="127"/>
        <source>Here you can import messages from ZFO files into the local database.</source>
        <translation>Zde můžete importovat zprávy ze souborů ZFO do místní databáze.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageImportMessage.qml" line="132"/>
        <location filename="../../qml/pages/PageImportMessage.qml" line="132"/>
        <source>Verify messages on the ISDS server</source>
        <translation>Verifikovat zprávy na serveru ISDS</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageImportMessage.qml" line="150"/>
        <location filename="../../qml/pages/PageImportMessage.qml" line="150"/>
        <source>Imported messages are going to be sent to the ISDS server where they are going to be checked. Messages failing this test won&apos;t be imported. We don&apos;t recommend using this option when you are using a mobile or slow internet connection.</source>
        <translation>Importované zprávy budou nejdříve zaslány na ISDS server, kde budou ověřeny. Zprávy, které se nepodaří ověřit, nebudou importovány. Nedoporučujeme používat tuto možnost, když používáte mobilní nebo pomalé internetové připojení.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageImportMessage.qml" line="157"/>
        <location filename="../../qml/pages/PageImportMessage.qml" line="157"/>
        <source>User must be logged in to the data box to be able to verify the messages.</source>
        <translation>Uživatel musí být přihlášen do datové schránky, aby bylo možné ověřit zprávy.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageImportMessage.qml" line="203"/>
        <location filename="../../qml/pages/PageImportMessage.qml" line="203"/>
        <source>User is logged in to ISDS.</source>
        <translation>Uživatel je přihlášen do ISDS.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageImportMessage.qml" line="170"/>
        <location filename="../../qml/pages/PageImportMessage.qml" line="170"/>
        <source>Import ZFO Files</source>
        <translation>Importovat ZFO soubory</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageImportMessage.qml" line="181"/>
        <location filename="../../qml/pages/PageImportMessage.qml" line="181"/>
        <source>Import from Directory</source>
        <translation>Importovat z adresáře</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageImportMessage.qml" line="163"/>
        <location filename="../../qml/pages/PageImportMessage.qml" line="163"/>
        <source>Log in now</source>
        <translation>Přihlásit se nyní</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageImportMessage.qml" line="203"/>
        <location filename="../../qml/pages/PageImportMessage.qml" line="203"/>
        <source>Select ZFO files or a directory.</source>
        <translation>Vyberte soubory ZFO nebo adresář.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageImportMessage.qml" line="151"/>
        <location filename="../../qml/pages/PageImportMessage.qml" line="151"/>
        <source>Imported messages won&apos;t be validated on the ISDS server.</source>
        <translation>Importované zprávy nebudou ověřeny na serveru ISDS.</translation>
    </message>
</context>
<context>
    <name>PageLog</name>
    <message>
        <location filename="../../qml/pages/PageLog.qml" line="37"/>
        <location filename="../../qml/pages/PageLog.qml" line="37"/>
        <source>Log from current run:</source>
        <translation>Log současného běhu:</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageLog.qml" line="76"/>
        <location filename="../../qml/pages/PageLog.qml" line="76"/>
        <source>Choose a log file.</source>
        <translation>Vybrat soubor s logem.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageLog.qml" line="78"/>
        <location filename="../../qml/pages/PageLog.qml" line="78"/>
        <source>Choose Log</source>
        <translation>Vybrat log</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageLog.qml" line="81"/>
        <location filename="../../qml/pages/PageLog.qml" line="81"/>
        <source>Choose a Log File</source>
        <translation>Vyberte soubor s logem</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageLog.qml" line="89"/>
        <location filename="../../qml/pages/PageLog.qml" line="89"/>
        <source>Send the log file via e-mail to developers.</source>
        <translation>Odesla log soubor přes e-mail vývojářům.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageLog.qml" line="91"/>
        <location filename="../../qml/pages/PageLog.qml" line="91"/>
        <source>Send Log</source>
        <translation>Odeslat log</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageLog.qml" line="93"/>
        <location filename="../../qml/pages/PageLog.qml" line="93"/>
        <source>Send Log via E-mail</source>
        <translation>Odeslat log emailem</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageLog.qml" line="85"/>
        <location filename="../../qml/pages/PageLog.qml" line="85"/>
        <source>Log file</source>
        <translation>Soubor s logem</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageLog.qml" line="48"/>
        <location filename="../../qml/pages/PageLog.qml" line="48"/>
        <source>Log Viewer</source>
        <translation>Zobrazení logu</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageLog.qml" line="94"/>
        <location filename="../../qml/pages/PageLog.qml" line="94"/>
        <source>Do you want to send the log information to developers?</source>
        <translation>Chcete odeslat log vývojářům?</translation>
    </message>
</context>
<context>
    <name>PageMessageDetail</name>
    <message>
        <location filename="../../qml/pages/PageMessageDetail.qml" line="124"/>
        <location filename="../../qml/pages/PageMessageDetail.qml" line="124"/>
        <source>File doesn&apos;t contain a valid message nor does it contain valid delivery information or the file is corrupt.</source>
        <translation>Soubor neobsahuje platnou datovou zprávu ani platné údaje o dodání a nebo je poškozen.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageMessageDetail.qml" line="155"/>
        <location filename="../../qml/pages/PageMessageDetail.qml" line="155"/>
        <source>Received</source>
        <translation>Přijatá</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageMessageDetail.qml" line="157"/>
        <location filename="../../qml/pages/PageMessageDetail.qml" line="157"/>
        <source>Sent</source>
        <translation>Odeslaná</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageMessageDetail.qml" line="72"/>
        <location filename="../../qml/pages/PageMessageDetail.qml" line="72"/>
        <source>Hide</source>
        <translation>Skrýt</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageMessageDetail.qml" line="77"/>
        <location filename="../../qml/pages/PageMessageDetail.qml" line="330"/>
        <location filename="../../qml/pages/PageMessageDetail.qml" line="77"/>
        <location filename="../../qml/pages/PageMessageDetail.qml" line="330"/>
        <source>Show more</source>
        <translation>Zobrazit více</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageMessageDetail.qml" line="159"/>
        <location filename="../../qml/pages/PageMessageDetail.qml" line="159"/>
        <source>Message</source>
        <translation>Zpráva</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageMessageDetail.qml" line="161"/>
        <location filename="../../qml/pages/PageMessageDetail.qml" line="161"/>
        <source>Acceptance Info</source>
        <translation>Doručenka</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageMessageDetail.qml" line="163"/>
        <location filename="../../qml/pages/PageMessageDetail.qml" line="163"/>
        <source>Unknown ZFO Content</source>
        <translation>Neznámy obsah souboru ZFO</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageMessageDetail.qml" line="252"/>
        <location filename="../../qml/pages/PageMessageDetail.qml" line="252"/>
        <source>Show menu of available operations.</source>
        <translation>Zobrazit menu s dostupnými operacemi.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageMessageDetail.qml" line="365"/>
        <location filename="../../qml/pages/PageMessageDetail.qml" line="365"/>
        <source>Download Message</source>
        <translation>Stáhnout zprávu</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageMessageDetail.qml" line="390"/>
        <location filename="../../qml/pages/PageMessageDetail.qml" line="390"/>
        <source>Downloading message %1 ...</source>
        <translation>Stahování zprávy %1 ...</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageMessageDetail.qml" line="519"/>
        <location filename="../../qml/pages/PageMessageDetail.qml" line="519"/>
        <source>Downloading message: %1</source>
        <translation>Stahuji zprávu: %1</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageMessageDetail.qml" line="520"/>
        <location filename="../../qml/pages/PageMessageDetail.qml" line="520"/>
        <source>Failed to download complete message %1.</source>
        <translation>Chyba při stahování kompletní zprávy %1.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageMessageDetail.qml" line="547"/>
        <location filename="../../qml/pages/PageMessageDetail.qml" line="547"/>
        <source>Reply on the message.</source>
        <translation>Odpovědět na zprávu.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageMessageDetail.qml" line="549"/>
        <location filename="../../qml/pages/PageMessageDetail.qml" line="549"/>
        <source>Reply</source>
        <translation>Odpověď</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageMessageDetail.qml" line="555"/>
        <location filename="../../qml/pages/PageMessageDetail.qml" line="555"/>
        <source>Forward message.</source>
        <translation>Přeposlat zprávu.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageMessageDetail.qml" line="557"/>
        <location filename="../../qml/pages/PageMessageDetail.qml" line="557"/>
        <source>Forward</source>
        <translation>Přeposlat</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageMessageDetail.qml" line="562"/>
        <location filename="../../qml/pages/PageMessageDetail.qml" line="562"/>
        <source>Send attachments via e-mail.</source>
        <translation>Přeposlat přílohy emailem.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageMessageDetail.qml" line="564"/>
        <location filename="../../qml/pages/PageMessageDetail.qml" line="564"/>
        <source>E-mail</source>
        <translation>E-mail</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageMessageDetail.qml" line="583"/>
        <location filename="../../qml/pages/PageMessageDetail.qml" line="583"/>
        <source>Save Attachments: %1</source>
        <translation>Uložit přílohy: %1</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageMessageDetail.qml" line="584"/>
        <location filename="../../qml/pages/PageMessageDetail.qml" line="584"/>
        <source>You can export files into iCloud or into device local storage.</source>
        <translation>Můžete exportovat soubory do iCloudu nebo do úložiště v zařízení.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageMessageDetail.qml" line="585"/>
        <location filename="../../qml/pages/PageMessageDetail.qml" line="585"/>
        <source>Do you want to export files to Datovka iCloud container?</source>
        <translation>Přejete si exportovat soubory do kontejneru Datovky v iCloudu?</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageMessageDetail.qml" line="577"/>
        <location filename="../../qml/pages/PageMessageDetail.qml" line="577"/>
        <source>Save attachments to the local storage.</source>
        <translation>Uložit přílohy do lokálního uložiště.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageMessageDetail.qml" line="579"/>
        <location filename="../../qml/pages/PageMessageDetail.qml" line="579"/>
        <source>Save</source>
        <translation>Uložit</translation>
    </message>
    <message numerus="yes">
        <location filename="../../qml/pages/PageMessageDetail.qml" line="89"/>
        <location filename="../../qml/pages/PageMessageDetail.qml" line="89"/>
        <source>The message will be deleted from ISDS in %n day(s).</source>
        <translation>
            <numerusform>Zpráva bude smazána z ISDS za %n den.</numerusform>
            <numerusform>Zpráva bude smazána z ISDS za %n dny.</numerusform>
            <numerusform>Zpráva bude smazána z ISDS za %n dnů.</numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <location filename="../../qml/pages/PageMessageDetail.qml" line="92"/>
        <location filename="../../qml/pages/PageMessageDetail.qml" line="92"/>
        <source>The message will be deleted from ISDS in %n day(s) because the long term storage is full.</source>
        <translation>
            <numerusform>Zpráva bude smazána z ISDS za %n den, protože datový trezor je plný.</numerusform>
            <numerusform>Zpráva bude smazána z ISDS za %n dny, protože datový trezor je plný.</numerusform>
            <numerusform>Zpráva bude smazána z ISDS za %n dnů, protože datový trezor je plný.</numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <location filename="../../qml/pages/PageMessageDetail.qml" line="94"/>
        <location filename="../../qml/pages/PageMessageDetail.qml" line="94"/>
        <source>The message will be moved to the long term storage in %n day(s) if the storage is not full. The message will be deleted from the ISDS server if the storage is full.</source>
        <translation>
            <numerusform>Zpráva bude za %n den přesunuta do datového trezoru pokud není trezor zaplněn. Zpráva bude smazána ze serveru ISDS pokud je trezor plný.</numerusform>
            <numerusform>Zpráva bude za %n dny přesunuta do datového trezoru pokud není trezor zaplněn. Zpráva bude smazána ze serveru ISDS pokud je trezor plný.</numerusform>
            <numerusform>Zpráva bude za %n dnů přesunuta do datového trezoru pokud není trezor zaplněn. Zpráva bude smazána ze serveru ISDS pokud je trezor plný.</numerusform>
        </translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageMessageDetail.qml" line="98"/>
        <location filename="../../qml/pages/PageMessageDetail.qml" line="98"/>
        <source>The message has already been deleted from the ISDS.</source>
        <translation>Zpráva byla smazána z ISDS.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageMessageDetail.qml" line="352"/>
        <location filename="../../qml/pages/PageMessageDetail.qml" line="352"/>
        <source>Attachments</source>
        <translation>Přílohy</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageMessageDetail.qml" line="366"/>
        <location filename="../../qml/pages/PageMessageDetail.qml" line="366"/>
        <source>Download complete message with attachments.</source>
        <translation>Stáhnout kompletní datovou zprávu s přílohami.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageMessageDetail.qml" line="375"/>
        <location filename="../../qml/pages/PageMessageDetail.qml" line="375"/>
        <source>Download complete signed message %1 with attachments.</source>
        <translation>Stáhnout kompletní datovou zprávu %1 s přílohami.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageMessageDetail.qml" line="461"/>
        <location filename="../../qml/pages/PageMessageDetail.qml" line="461"/>
        <source>Open attachment &apos;%1&apos;.</source>
        <translation>Otevřít přílohu &apos;%1&apos;.</translation>
    </message>
</context>
<context>
    <name>PageMessageEnvelope</name>
    <message>
        <location filename="../../qml/pages/PageMessageEnvelope.qml" line="74"/>
        <location filename="../../qml/pages/PageMessageEnvelope.qml" line="74"/>
        <source>Message Envelope</source>
        <translation>Obálka datové zprávy</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageMessageEnvelope.qml" line="74"/>
        <location filename="../../qml/pages/PageMessageEnvelope.qml" line="74"/>
        <source>Acceptance Info</source>
        <translation>Doručenka</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageMessageEnvelope.qml" line="107"/>
        <location filename="../../qml/pages/PageMessageEnvelope.qml" line="107"/>
        <source>Save envelope as PDF.</source>
        <translation>Uložit obálku jako PDF.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageMessageEnvelope.qml" line="107"/>
        <location filename="../../qml/pages/PageMessageEnvelope.qml" line="107"/>
        <source>Save acceptance info as PDF.</source>
        <translation>Uložit doručenku jako PDF.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageMessageEnvelope.qml" line="109"/>
        <location filename="../../qml/pages/PageMessageEnvelope.qml" line="109"/>
        <source>Save PDF</source>
        <translation>Uložit PDF</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageMessageEnvelope.qml" line="114"/>
        <location filename="../../qml/pages/PageMessageEnvelope.qml" line="114"/>
        <source>Save PDF: %1</source>
        <translation>Uložit PDF: %1</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageMessageEnvelope.qml" line="115"/>
        <location filename="../../qml/pages/PageMessageEnvelope.qml" line="132"/>
        <location filename="../../qml/pages/PageMessageEnvelope.qml" line="115"/>
        <location filename="../../qml/pages/PageMessageEnvelope.qml" line="132"/>
        <source>You can export files into iCloud or into device local storage.</source>
        <translation>Můžete exportovat soubory do iCloudu nebo do úložiště v zařízení.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageMessageEnvelope.qml" line="116"/>
        <location filename="../../qml/pages/PageMessageEnvelope.qml" line="133"/>
        <location filename="../../qml/pages/PageMessageEnvelope.qml" line="116"/>
        <location filename="../../qml/pages/PageMessageEnvelope.qml" line="133"/>
        <source>Do you want to export files to Datovka iCloud container?</source>
        <translation>Přejete si exportovat soubory do kontejneru Datovky v iCloudu?</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageMessageEnvelope.qml" line="124"/>
        <location filename="../../qml/pages/PageMessageEnvelope.qml" line="124"/>
        <source>Save message as ZFO.</source>
        <translation>Uložit zprávu jako ZFO.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageMessageEnvelope.qml" line="124"/>
        <location filename="../../qml/pages/PageMessageEnvelope.qml" line="124"/>
        <source>Save acceptance info as ZFO.</source>
        <translation>Uložit doručenku jako ZFO.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageMessageEnvelope.qml" line="126"/>
        <location filename="../../qml/pages/PageMessageEnvelope.qml" line="126"/>
        <source>Save ZFO</source>
        <translation>Uložit ZFO</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageMessageEnvelope.qml" line="131"/>
        <location filename="../../qml/pages/PageMessageEnvelope.qml" line="131"/>
        <source>Save ZFO: %1</source>
        <translation>Uložit ZFO: %1</translation>
    </message>
</context>
<context>
    <name>PageMessageList</name>
    <message>
        <location filename="../../qml/pages/PageMessageList.qml" line="94"/>
        <location filename="../../qml/pages/PageMessageList.qml" line="94"/>
        <source>Received</source>
        <translation>Přijaté</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageMessageList.qml" line="94"/>
        <location filename="../../qml/pages/PageMessageList.qml" line="94"/>
        <source>Sent</source>
        <translation>Odeslané</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageMessageList.qml" line="110"/>
        <location filename="../../qml/pages/PageMessageList.qml" line="110"/>
        <source>Filter messages.</source>
        <translation>Filtrovat zprávy.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageMessageList.qml" line="121"/>
        <location filename="../../qml/pages/PageMessageList.qml" line="222"/>
        <location filename="../../qml/pages/PageMessageList.qml" line="121"/>
        <location filename="../../qml/pages/PageMessageList.qml" line="222"/>
        <source>Synchronise received.</source>
        <translation>Synchronizovat přijaté.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageMessageList.qml" line="121"/>
        <location filename="../../qml/pages/PageMessageList.qml" line="222"/>
        <location filename="../../qml/pages/PageMessageList.qml" line="121"/>
        <location filename="../../qml/pages/PageMessageList.qml" line="222"/>
        <source>Synchronise sent.</source>
        <translation>Synchronizovat odeslané.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageMessageList.qml" line="177"/>
        <location filename="../../qml/pages/PageMessageList.qml" line="177"/>
        <source>Delete Message: %1</source>
        <translation>Smazat zprávu: %1</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageMessageList.qml" line="178"/>
        <location filename="../../qml/pages/PageMessageList.qml" line="178"/>
        <source>Do you want to delete the sent message &apos;%1&apos;?</source>
        <translation>Přejete si smazat odeslanou zprávu &quot;%1&quot;?</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageMessageList.qml" line="179"/>
        <location filename="../../qml/pages/PageMessageList.qml" line="179"/>
        <source>Note: It will delete all attachments and message information from the local database.</source>
        <translation>Poznámka: Z místní databáze budou smazány všechny přílohy a informace o zprávě.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageMessageList.qml" line="221"/>
        <location filename="../../qml/pages/PageMessageList.qml" line="221"/>
        <source>Synchronise Received</source>
        <translation>Synchronizovat přijaté</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageMessageList.qml" line="221"/>
        <location filename="../../qml/pages/PageMessageList.qml" line="221"/>
        <source>Synchronise Sent</source>
        <translation>Synchronizovat odeslané</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageMessageList.qml" line="233"/>
        <location filename="../../qml/pages/PageMessageList.qml" line="233"/>
        <source>No %1 messages available or they haven&apos;t been downloaded yet.</source>
        <translation>Žádné %1 zprávy nebo doposud nebyly žádné staženy.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageMessageList.qml" line="263"/>
        <location filename="../../qml/pages/PageMessageList.qml" line="263"/>
        <source>%1: messages</source>
        <translation>%1: zpráv</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageMessageList.qml" line="264"/>
        <location filename="../../qml/pages/PageMessageList.qml" line="264"/>
        <source>Failed to download list of messages for user name &apos;%1&apos;.</source>
        <translation>Chyba při stahování seznamu zpráv pro uživatelské jméno &apos;%1&apos;.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageMessageList.qml" line="233"/>
        <location filename="../../qml/pages/PageMessageList.qml" line="233"/>
        <source>received</source>
        <translation>přijaté</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageMessageList.qml" line="233"/>
        <location filename="../../qml/pages/PageMessageList.qml" line="233"/>
        <source>sent</source>
        <translation>odeslané</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageMessageList.qml" line="103"/>
        <location filename="../../qml/pages/PageMessageList.qml" line="103"/>
        <source>Show menu of available operations.</source>
        <translation>Zobrazit menu s dostupnými operacemi.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageMessageList.qml" line="244"/>
        <location filename="../../qml/pages/PageMessageList.qml" line="244"/>
        <source>No message found that matches filter text &apos;%1&apos;.</source>
        <translation>Nenalezena žádná zpráva odpovídající textu filtru &apos;%1&apos;.</translation>
    </message>
</context>
<context>
    <name>PageMessageListSwipeView</name>
    <message>
        <location filename="../../qml/pages/PageMessageListSwipeView.qml" line="131"/>
        <location filename="../../qml/pages/PageMessageListSwipeView.qml" line="131"/>
        <source>Database problem: %1</source>
        <translation>Problém s databází: %1</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageMessageListSwipeView.qml" line="132"/>
        <location filename="../../qml/pages/PageMessageListSwipeView.qml" line="132"/>
        <source>The message database for username &apos;%1&apos; appears to be corrupt. Synchronisation won&apos;t most likely be able to write new data and therefore is going to be aborted.</source>
        <translation>Databáze zpráv pro uživatele &apos;%1&apos; se zdá být poškozená. Synchronizace nebude pravděpodobně schopná zapsat nová data a bude proto přerušena.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageMessageListSwipeView.qml" line="133"/>
        <location filename="../../qml/pages/PageMessageListSwipeView.qml" line="133"/>
        <source>Do you want to restore the damaged database from a local back-up copy?</source>
        <translation>Přejete si obnovit poškozenou databázi z lokální záložní kopie?</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageMessageListSwipeView.qml" line="152"/>
        <location filename="../../qml/pages/PageMessageListSwipeView.qml" line="152"/>
        <source>Create and send a new message from data box &apos;%1&apos;.</source>
        <translation>Vytvořit a odeslat novou zprávu ze schránky &quot;%1&quot;.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageMessageListSwipeView.qml" line="168"/>
        <location filename="../../qml/pages/PageMessageListSwipeView.qml" line="168"/>
        <source>View received messages of data box &apos;%1&apos;.</source>
        <translation>Zobrazit přijaté zprávy schránky &quot;%1&quot;.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageMessageListSwipeView.qml" line="169"/>
        <location filename="../../qml/pages/PageMessageListSwipeView.qml" line="169"/>
        <source>Received</source>
        <translation>Přijaté</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageMessageListSwipeView.qml" line="175"/>
        <location filename="../../qml/pages/PageMessageListSwipeView.qml" line="175"/>
        <source>View sent messages of data box &apos;%1&apos;.</source>
        <translation>Zobrazit odeslané zprávy schránky &apos;%1&apos;.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageMessageListSwipeView.qml" line="176"/>
        <location filename="../../qml/pages/PageMessageListSwipeView.qml" line="176"/>
        <source>Sent</source>
        <translation>Odeslané</translation>
    </message>
</context>
<context>
    <name>PageMessageSearch</name>
    <message>
        <location filename="../../qml/pages/PageMessageSearch.qml" line="65"/>
        <location filename="../../qml/pages/PageMessageSearch.qml" line="65"/>
        <source>Search Message</source>
        <translation>Hledat zprávu</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageMessageSearch.qml" line="73"/>
        <location filename="../../qml/pages/PageMessageSearch.qml" line="73"/>
        <source>Search messages.</source>
        <translation>Hledat zprávy.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageMessageSearch.qml" line="90"/>
        <location filename="../../qml/pages/PageMessageSearch.qml" line="90"/>
        <source>Select type of sought messages</source>
        <translation>Vyberte typ hledaných zpráv</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageMessageSearch.qml" line="94"/>
        <location filename="../../qml/pages/PageMessageSearch.qml" line="94"/>
        <source>All messages</source>
        <translation>Všechny zprávy</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageMessageSearch.qml" line="95"/>
        <location filename="../../qml/pages/PageMessageSearch.qml" line="95"/>
        <source>Received messages</source>
        <translation>Přijaté zprávy</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageMessageSearch.qml" line="96"/>
        <location filename="../../qml/pages/PageMessageSearch.qml" line="96"/>
        <source>Sent messages</source>
        <translation>Odeslané zprávy</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageMessageSearch.qml" line="108"/>
        <location filename="../../qml/pages/PageMessageSearch.qml" line="108"/>
        <source>Enter phrase</source>
        <translation>Zadejte text</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageMessageSearch.qml" line="127"/>
        <location filename="../../qml/pages/PageMessageSearch.qml" line="127"/>
        <source>Searching for messages ...</source>
        <translation>Vyhledávám zprávy ...</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageMessageSearch.qml" line="159"/>
        <location filename="../../qml/pages/PageMessageSearch.qml" line="159"/>
        <source>No message found for given search phrase.</source>
        <translation>Nenalezena žádná zpráva pro uvedený hledaný text.</translation>
    </message>
</context>
<context>
    <name>PagePrefsEditor</name>
    <message>
        <location filename="../../qml/pages/PagePrefsEditor.qml" line="80"/>
        <location filename="../../qml/pages/PagePrefsEditor.qml" line="80"/>
        <source>Enter a Value</source>
        <translation>Zadejte hodnotu</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PagePrefsEditor.qml" line="105"/>
        <location filename="../../qml/pages/PagePrefsEditor.qml" line="105"/>
        <source>Close</source>
        <translation>Zavřít</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PagePrefsEditor.qml" line="111"/>
        <location filename="../../qml/pages/PagePrefsEditor.qml" line="111"/>
        <source>Reset</source>
        <translation>Resetovat</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PagePrefsEditor.qml" line="119"/>
        <location filename="../../qml/pages/PagePrefsEditor.qml" line="119"/>
        <source>Save</source>
        <translation>Uložit</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PagePrefsEditor.qml" line="156"/>
        <location filename="../../qml/pages/PagePrefsEditor.qml" line="156"/>
        <source>All Preferences</source>
        <translation>Všechny předvolby</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PagePrefsEditor.qml" line="165"/>
        <location filename="../../qml/pages/PagePrefsEditor.qml" line="165"/>
        <source>Filter the preferences.</source>
        <translation>Filtrovat předvolby.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PagePrefsEditor.qml" line="191"/>
        <location filename="../../qml/pages/PagePrefsEditor.qml" line="191"/>
        <source>Press and hold a list element to edit its value.</source>
        <translation>Stiskněte a držte položku seznamu za účelem editace její hodnoty.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PagePrefsEditor.qml" line="264"/>
        <location filename="../../qml/pages/PagePrefsEditor.qml" line="264"/>
        <source>Changing these settings directly can be harmful to the stability, security and performance of this application. You should only continue if you are sure of what you are doing.</source>
        <translation>Přímé nastavení těchto hodnot může mít špatný vliv na stabilitu, bezpečnost a výkonnost této aplikace. Pokračujte pouze pokud víte, co děláte.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PagePrefsEditor.qml" line="269"/>
        <location filename="../../qml/pages/PagePrefsEditor.qml" line="269"/>
        <source>I accept the risk.</source>
        <translation>Beru na vědomí.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PagePrefsEditor.qml" line="270"/>
        <location filename="../../qml/pages/PagePrefsEditor.qml" line="270"/>
        <source>Show all preferences.</source>
        <translation>Ukázat všechny předvolby.</translation>
    </message>
</context>
<context>
    <name>PageRecordsManagementUpload</name>
    <message>
        <location filename="../../qml/pages/PageRecordsManagementUpload.qml" line="105"/>
        <location filename="../../qml/pages/PageRecordsManagementUpload.qml" line="105"/>
        <source>Upload Message %1</source>
        <translation>Nahrát zprávu %1</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageRecordsManagementUpload.qml" line="113"/>
        <location filename="../../qml/pages/PageRecordsManagementUpload.qml" line="113"/>
        <source>Filter upload hierarchy.</source>
        <translation>Filtrovat hierarchii pro nahrávání.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageRecordsManagementUpload.qml" line="138"/>
        <location filename="../../qml/pages/PageRecordsManagementUpload.qml" line="138"/>
        <source>The message can be uploaded into selected locations in the records management hierarchy.</source>
        <translation>Zprávu můžete nahrát do zvolených umístění v hierarchii spisové služby.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageRecordsManagementUpload.qml" line="146"/>
        <location filename="../../qml/pages/PageRecordsManagementUpload.qml" line="146"/>
        <source>Update upload hierarchy</source>
        <translation>Aktualizovat hierarchii</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageRecordsManagementUpload.qml" line="148"/>
        <location filename="../../qml/pages/PageRecordsManagementUpload.qml" line="148"/>
        <source>Update</source>
        <translation>Aktualizovat</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageRecordsManagementUpload.qml" line="156"/>
        <location filename="../../qml/pages/PageRecordsManagementUpload.qml" line="156"/>
        <source>Upload message</source>
        <translation>Nahrát zprávu</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageRecordsManagementUpload.qml" line="158"/>
        <location filename="../../qml/pages/PageRecordsManagementUpload.qml" line="158"/>
        <source>Upload</source>
        <translation>Nahrát zprávu</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageRecordsManagementUpload.qml" line="188"/>
        <location filename="../../qml/pages/PageRecordsManagementUpload.qml" line="188"/>
        <source>Up</source>
        <translation>Nahoru</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageRecordsManagementUpload.qml" line="209"/>
        <location filename="../../qml/pages/PageRecordsManagementUpload.qml" line="209"/>
        <source>Upload hierarchy has not been downloaded yet.</source>
        <translation>Hierarchie spisové služby nebyla doposud stažena.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageRecordsManagementUpload.qml" line="210"/>
        <location filename="../../qml/pages/PageRecordsManagementUpload.qml" line="210"/>
        <source>No hierarchy entry found that matches filter text &apos;%1&apos;.</source>
        <translation>Nenalezena žádná položka hierarchie odpovídající textu &apos;%1&apos;.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageRecordsManagementUpload.qml" line="263"/>
        <location filename="../../qml/pages/PageRecordsManagementUpload.qml" line="263"/>
        <source>View content of %1.</source>
        <translation>Zobrazit obsah %1.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageRecordsManagementUpload.qml" line="265"/>
        <location filename="../../qml/pages/PageRecordsManagementUpload.qml" line="265"/>
        <source>Select %1.</source>
        <translation>Vybrat %1.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageRecordsManagementUpload.qml" line="265"/>
        <location filename="../../qml/pages/PageRecordsManagementUpload.qml" line="265"/>
        <source>Deselect %1.</source>
        <translation>Zrušit výběr %1.</translation>
    </message>
</context>
<context>
    <name>PageRepairDatabase</name>
    <message>
        <location filename="../../qml/pages/PageRepairDatabase.qml" line="57"/>
        <location filename="../../qml/pages/PageRepairDatabase.qml" line="57"/>
        <source>Restoration Finished</source>
        <translation>Obnovování dokončeno</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageRepairDatabase.qml" line="59"/>
        <location filename="../../qml/pages/PageRepairDatabase.qml" line="59"/>
        <source>Synchronise the data box to download missing data.</source>
        <translation>Synchronizujte schránky pro stažení chybějících dat.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageRepairDatabase.qml" line="61"/>
        <location filename="../../qml/pages/PageRepairDatabase.qml" line="61"/>
        <source>Restoration Error</source>
        <translation>Chyba obnovování</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageRepairDatabase.qml" line="62"/>
        <location filename="../../qml/pages/PageRepairDatabase.qml" line="62"/>
        <source>Restoration of the message database failed. The log file contains the description of occurred errors.</source>
        <translation>Obnovování databáze zpráv selhalo. Soubor s logem obsahuje popisy chyb, které se vyskytly.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageRepairDatabase.qml" line="63"/>
        <location filename="../../qml/pages/PageRepairDatabase.qml" line="105"/>
        <location filename="../../qml/pages/PageRepairDatabase.qml" line="63"/>
        <location filename="../../qml/pages/PageRepairDatabase.qml" line="105"/>
        <source>Do you want to delete the corrupted message database and create a new empty one? Downloaded message data will be deleted.</source>
        <translation>Přejete si smazat poškozenou databázi zpráv a vytvořit novou prázdnou? Stažená data zpráv budou smazána.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageRepairDatabase.qml" line="70"/>
        <location filename="../../qml/pages/PageRepairDatabase.qml" line="70"/>
        <source>Repair Database</source>
        <translation>Opravit databázi</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageRepairDatabase.qml" line="88"/>
        <location filename="../../qml/pages/PageRepairDatabase.qml" line="88"/>
        <source>The action checks the message database integrity and restores damaged files from a local back-up copy.</source>
        <translation>Akce ověří integritu databáze zpráv a obnoví poškozené soubory z lokální záložní kopie.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageRepairDatabase.qml" line="91"/>
        <location filename="../../qml/pages/PageRepairDatabase.qml" line="91"/>
        <source>Run Integrity Test</source>
        <translation>Spustit test integrity</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageRepairDatabase.qml" line="102"/>
        <location filename="../../qml/pages/PageRepairDatabase.qml" line="102"/>
        <source>But there isn&apos;t any local back-up file which can be used to repair the damaged one.</source>
        <translation>Ale není k dispozici žádný záložní soubor, který by se dal použít k opravě poškozeného.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageRepairDatabase.qml" line="103"/>
        <location filename="../../qml/pages/PageRepairDatabase.qml" line="103"/>
        <source>Database Problem</source>
        <translation>Problém s databází</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageRepairDatabase.qml" line="104"/>
        <location filename="../../qml/pages/PageRepairDatabase.qml" line="104"/>
        <source>The message database file is damaged but there isn&apos;t any local back-up file which can be used to repair the damaged one.</source>
        <translation>Soubor databáze zpráv je poškozen, ale neni dostupný žádný záložní soubor, který by se dal použít k opravě poškozeného.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageRepairDatabase.qml" line="109"/>
        <location filename="../../qml/pages/PageRepairDatabase.qml" line="109"/>
        <source>Select a local back-up database file witch will be used to repair the damaged one.</source>
        <translation>Vyberte lokální soubor se zálohou databáze, který bude použit k opravě poškozeného.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageRepairDatabase.qml" line="155"/>
        <location filename="../../qml/pages/PageRepairDatabase.qml" line="155"/>
        <source>Database Restoration</source>
        <translation>Obnovení databáze</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageRepairDatabase.qml" line="156"/>
        <location filename="../../qml/pages/PageRepairDatabase.qml" line="156"/>
        <source>The operation restores the currently used message database from the back-up file &apos;%1&apos; created at %2.</source>
        <translation>Operace obnoví současně používanou databázi zpráv ze záložního souboru &apos;%1&apos; vytvořeného %2.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageRepairDatabase.qml" line="157"/>
        <location filename="../../qml/pages/PageRepairDatabase.qml" line="157"/>
        <source>Restore the message database from the selected the back-up file?</source>
        <translation>Obnovit databázi zpráv z vybraného záložního souboru?</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageRepairDatabase.qml" line="101"/>
        <location filename="../../qml/pages/PageRepairDatabase.qml" line="108"/>
        <location filename="../../qml/pages/PageRepairDatabase.qml" line="101"/>
        <location filename="../../qml/pages/PageRepairDatabase.qml" line="108"/>
        <source>The message database file is damaged.</source>
        <translation>Soubor databáze zpráv je poškozen.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageRepairDatabase.qml" line="115"/>
        <location filename="../../qml/pages/PageRepairDatabase.qml" line="115"/>
        <source>The message database file is valid and OK.</source>
        <translation>Soubor databáze zpráv je validní a v pořádku.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageRepairDatabase.qml" line="56"/>
        <location filename="../../qml/pages/PageRepairDatabase.qml" line="58"/>
        <location filename="../../qml/pages/PageRepairDatabase.qml" line="56"/>
        <location filename="../../qml/pages/PageRepairDatabase.qml" line="58"/>
        <source>The message database has successfully been restored.</source>
        <translation>Databáze zpráv byla úspěšně obnovena.</translation>
    </message>
</context>
<context>
    <name>PageRestoreData</name>
    <message>
        <location filename="../../qml/pages/PageRestoreData.qml" line="151"/>
        <location filename="../../qml/pages/PageRestoreData.qml" line="151"/>
        <source>An error during data restoration has occurred.</source>
        <translation>Nastala chyba v průběhu obnovování.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageRestoreData.qml" line="152"/>
        <location filename="../../qml/pages/PageRestoreData.qml" line="152"/>
        <source>Application data restoration has been cancelled.</source>
        <translation>Obnovování bylo přerušeno.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageRestoreData.qml" line="157"/>
        <location filename="../../qml/pages/PageRestoreData.qml" line="157"/>
        <source>The application will restart in order to load the imported files.</source>
        <translation>Aplikace se restartuje, aby načetla importované soubory.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageRestoreData.qml" line="208"/>
        <location filename="../../qml/pages/PageRestoreData.qml" line="208"/>
        <source>Proceed with restoration.</source>
        <translation>Pokračovat s obnovením.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageRestoreData.qml" line="150"/>
        <location filename="../../qml/pages/PageRestoreData.qml" line="150"/>
        <source>Restoration Error</source>
        <translation>Chyba obnovování</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageRestoreData.qml" line="155"/>
        <location filename="../../qml/pages/PageRestoreData.qml" line="155"/>
        <source>Restoration Finished</source>
        <translation>Obnovování dokončeno</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageRestoreData.qml" line="156"/>
        <location filename="../../qml/pages/PageRestoreData.qml" line="156"/>
        <source>All selected data box data have successfully been restored.</source>
        <translation>Data vybraných schránek byla úspěšně obnovena.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageRestoreData.qml" line="165"/>
        <location filename="../../qml/pages/PageRestoreData.qml" line="206"/>
        <location filename="../../qml/pages/PageRestoreData.qml" line="165"/>
        <location filename="../../qml/pages/PageRestoreData.qml" line="206"/>
        <source>Restore Data</source>
        <translation>Obnova dat</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageRestoreData.qml" line="182"/>
        <location filename="../../qml/pages/PageRestoreData.qml" line="182"/>
        <source>The action allows to restore data from a back-up or transfer ZIP archive.</source>
        <translation>Akce umožňuje obnovit data ze ZIP archivu zálohy nebo přenosu.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageRestoreData.qml" line="186"/>
        <location filename="../../qml/pages/PageRestoreData.qml" line="186"/>
        <source>Choose ZIP Archive</source>
        <translation>Vybrat ZIP archiv</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageRestoreData.qml" line="211"/>
        <location filename="../../qml/pages/PageRestoreData.qml" line="215"/>
        <location filename="../../qml/pages/PageRestoreData.qml" line="211"/>
        <location filename="../../qml/pages/PageRestoreData.qml" line="215"/>
        <source>Proceed?</source>
        <translation>Pokračovat?</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageRestoreData.qml" line="212"/>
        <location filename="../../qml/pages/PageRestoreData.qml" line="212"/>
        <source>The action will continue with the restoration of the complete application data. Current application data will be completely rewritten.</source>
        <translation>Akce bude pokračovat obnovením kompletních dat aplikace. Současná data aplikace budou kompletně přepsána.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageRestoreData.qml" line="213"/>
        <location filename="../../qml/pages/PageRestoreData.qml" line="213"/>
        <source>Do you wish to restore the application data from the transfer?</source>
        <translation>Přejete si obnovit data aplikace?</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageRestoreData.qml" line="216"/>
        <location filename="../../qml/pages/PageRestoreData.qml" line="216"/>
        <source>The action will continue with the restoration of the selected data boxes. Current data of the selected data boxes will be completely rewritten.</source>
        <translation>Akce bude pokračovat obnovením vybraných schránek. Současná data vybraných schránek budou kompletně přepsána.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageRestoreData.qml" line="217"/>
        <location filename="../../qml/pages/PageRestoreData.qml" line="217"/>
        <source>Do you wish to restore the selected data boxes from the back-up?</source>
        <translation>Přejete si obnovit vybrané schránky ze zálohy?</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageRestoreData.qml" line="224"/>
        <location filename="../../qml/pages/PageRestoreData.qml" line="224"/>
        <source>Select all data boxes</source>
        <translation>Vybrat všechny schránky</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageRestoreData.qml" line="91"/>
        <location filename="../../qml/pages/PageRestoreData.qml" line="91"/>
        <source>Select ZIP</source>
        <translation>Vyberte ZIP</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageRestoreData.qml" line="251"/>
        <location filename="../../qml/pages/PageRestoreData.qml" line="251"/>
        <source>Select %1.</source>
        <translation>Vybrat %1.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageRestoreData.qml" line="251"/>
        <location filename="../../qml/pages/PageRestoreData.qml" line="251"/>
        <source>Deselect %1.</source>
        <translation>Zrušit výběr %1.</translation>
    </message>
</context>
<context>
    <name>PageSendMessage</name>
    <message>
        <location filename="../../qml/pages/PageSendMessage.qml" line="207"/>
        <location filename="../../qml/pages/PageSendMessage.qml" line="207"/>
        <source>Reply %1</source>
        <translation>Odpověď %1</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSendMessage.qml" line="226"/>
        <location filename="../../qml/pages/PageSendMessage.qml" line="226"/>
        <source>Forward %1</source>
        <translation>Přeposlat %1</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSendMessage.qml" line="249"/>
        <location filename="../../qml/pages/PageSendMessage.qml" line="249"/>
        <source>Forward ZFO %1</source>
        <translation>Přeposlat ZFO %1</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSendMessage.qml" line="310"/>
        <location filename="../../qml/pages/PageSendMessage.qml" line="310"/>
        <source>New message</source>
        <translation>Nová zpráva</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSendMessage.qml" line="1021"/>
        <location filename="../../qml/pages/PageSendMessage.qml" line="1021"/>
        <source>message subject</source>
        <translation>předmět zprávy</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSendMessage.qml" line="1024"/>
        <location filename="../../qml/pages/PageSendMessage.qml" line="1024"/>
        <source>recipient data box</source>
        <translation>datová schránka adresáta</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSendMessage.qml" line="1030"/>
        <location filename="../../qml/pages/PageSendMessage.qml" line="1030"/>
        <source>Please fill in the respective fields before attempting to send the message.</source>
        <translation>Před odesláním zprávy vyplňte prosím odpovídající položky.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSendMessage.qml" line="1029"/>
        <location filename="../../qml/pages/PageSendMessage.qml" line="1029"/>
        <source>Send Message Error</source>
        <translation>Chyba odesílání zprávy</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSendMessage.qml" line="448"/>
        <location filename="../../qml/pages/PageSendMessage.qml" line="448"/>
        <source>Error during message creation.</source>
        <translation>Chyba při vytváření zprávy.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSendMessage.qml" line="483"/>
        <location filename="../../qml/pages/PageSendMessage.qml" line="483"/>
        <source>Enter message subject. Mandatory</source>
        <translation>Zadejte předmět zprávy. Povinné</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSendMessage.qml" line="532"/>
        <location filename="../../qml/pages/PageSendMessage.qml" line="532"/>
        <source>At least one recipient must be appended.</source>
        <translation>Alespoň jeden adresát musí být připojen ke zprávě.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSendMessage.qml" line="553"/>
        <location filename="../../qml/pages/PageSendMessage.qml" line="553"/>
        <source>Find recipient</source>
        <translation>Najít adresáta</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSendMessage.qml" line="608"/>
        <location filename="../../qml/pages/PageSendMessage.qml" line="608"/>
        <source>Select a file from the local storage or enter short text message and append it into attachments.</source>
        <translation>Vyberte soubor z lokálního úložiště nebo zadejte krátkou textovou zprávu a připojte ji do seznamu příloh.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSendMessage.qml" line="975"/>
        <location filename="../../qml/pages/PageSendMessage.qml" line="975"/>
        <source>Append</source>
        <translation>Přiložit</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSendMessage.qml" line="1100"/>
        <location filename="../../qml/pages/PageSendMessage.qml" line="1100"/>
        <source>General message settings page.</source>
        <translation>Obecné nastavení zprávy.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSendMessage.qml" line="1106"/>
        <location filename="../../qml/pages/PageSendMessage.qml" line="1106"/>
        <source>Recipient list page.</source>
        <translation>Seznam adresátů.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSendMessage.qml" line="529"/>
        <location filename="../../qml/pages/PageSendMessage.qml" line="1107"/>
        <location filename="../../qml/pages/PageSendMessage.qml" line="529"/>
        <location filename="../../qml/pages/PageSendMessage.qml" line="1107"/>
        <source>Recipients</source>
        <translation>Adresáti</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSendMessage.qml" line="306"/>
        <location filename="../../qml/pages/PageSendMessage.qml" line="306"/>
        <source>Data Box: %1 (%2)</source>
        <translation>Datová schránka: %1 (%2)</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSendMessage.qml" line="484"/>
        <location filename="../../qml/pages/PageSendMessage.qml" line="484"/>
        <source>The message must contain the subject, at least one recipient and attachment.</source>
        <translation>Zpráva musí obsahovat předmět, alespoň jednoho adresáta a nejméně jednu přílohu.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSendMessage.qml" line="531"/>
        <location filename="../../qml/pages/PageSendMessage.qml" line="531"/>
        <source>Select a recipient from local contacts or find a recipient on the ISDS server and append it into recipient list.</source>
        <translation>Vyberte adresáta z kontaktů nebo jej vyhledejte na serveru ISDS a pak jej připojte do seznamu adresátů.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSendMessage.qml" line="1112"/>
        <location filename="../../qml/pages/PageSendMessage.qml" line="1112"/>
        <source>Attachment list page.</source>
        <translation>Seznam příloh.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSendMessage.qml" line="606"/>
        <location filename="../../qml/pages/PageSendMessage.qml" line="1113"/>
        <location filename="../../qml/pages/PageSendMessage.qml" line="606"/>
        <location filename="../../qml/pages/PageSendMessage.qml" line="1113"/>
        <source>Attachments</source>
        <translation>Přílohy</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSendMessage.qml" line="1101"/>
        <location filename="../../qml/pages/PageSendMessage.qml" line="1101"/>
        <source>Subject</source>
        <translation>Předmět</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSendMessage.qml" line="494"/>
        <location filename="../../qml/pages/PageSendMessage.qml" line="494"/>
        <source>Include sender identification</source>
        <translation>Přidat identifikaci odesílatele</translation>
    </message>
    <message numerus="yes">
        <location filename="../../qml/pages/PageSendMessage.qml" line="115"/>
        <location filename="../../qml/pages/PageSendMessage.qml" line="140"/>
        <location filename="../../qml/pages/PageSendMessage.qml" line="115"/>
        <location filename="../../qml/pages/PageSendMessage.qml" line="140"/>
        <source>The permitted number of %n attachment(s) has been exceeded.</source>
        <translation>
            <numerusform>Povolený počet %n přílohy byl překročen.</numerusform>
            <numerusform>Povolený počet %n příloh byl překročen.</numerusform>
            <numerusform>Povolený počet %n příloh byl překročen.</numerusform>
        </translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSendMessage.qml" line="125"/>
        <location filename="../../qml/pages/PageSendMessage.qml" line="125"/>
        <source>Total size of attachments exceeds %1 MB. Most of the data boxes cannot receive messages larger than %1 MB. However, some OVM data boxes can receive messages up to %2 MB.</source>
        <translation>Celková velikost příloh přesahuje %1 MB. Většina datových schránek nemůže přijímat zprávy větší než %1 MB. Nicméně některé datové schránky typu OVM mohou přijímat zprávy až do %2 MB.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSendMessage.qml" line="150"/>
        <location filename="../../qml/pages/PageSendMessage.qml" line="150"/>
        <source>Total size of attachments is ~%1 MB and exceeds the limit of %2 MB for standard messages. Message will be sent as a high-volume message (VoDZ).</source>
        <translation>Celková velikost příloh je ~%1 MB a překročila limit %2 MB pro standardní zprávu. Zpráva bude odesláná jako velkoobjemová zpráva (VoDZ).</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSendMessage.qml" line="266"/>
        <location filename="../../qml/pages/PageSendMessage.qml" line="266"/>
        <source>ZFO file of message %1 is not available. Download the complete message before forwarding.</source>
        <translation>ZFO zpráva %1 není k dispozici. Stáhněte znovu kompletní zprávu.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSendMessage.qml" line="435"/>
        <location filename="../../qml/pages/PageSendMessage.qml" line="435"/>
        <source>Create Message</source>
        <translation>Vytvořit zprávu</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSendMessage.qml" line="465"/>
        <location filename="../../qml/pages/PageSendMessage.qml" line="465"/>
        <source>Sender</source>
        <translation>Odesílatel</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSendMessage.qml" line="481"/>
        <location filename="../../qml/pages/PageSendMessage.qml" line="481"/>
        <source>Message subject</source>
        <translation>Předmět zprávy</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSendMessage.qml" line="485"/>
        <location filename="../../qml/pages/PageSendMessage.qml" line="485"/>
        <source>The subject must be filled in.</source>
        <translation>Předmět zprávy musí být vyplněn.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSendMessage.qml" line="500"/>
        <location filename="../../qml/pages/PageSendMessage.qml" line="500"/>
        <source>Personal delivery</source>
        <translation>Doručení do vlastních rukou</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSendMessage.qml" line="507"/>
        <location filename="../../qml/pages/PageSendMessage.qml" line="507"/>
        <source>Allow acceptance through fiction</source>
        <translation>Povolit doručení fikcí</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSendMessage.qml" line="728"/>
        <location filename="../../qml/pages/PageSendMessage.qml" line="728"/>
        <source>Open file &apos;%1&apos;.</source>
        <translation>Otevřít soubor &apos;%1&apos;.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSendMessage.qml" line="678"/>
        <location filename="../../qml/pages/PageSendMessage.qml" line="678"/>
        <source>Local database</source>
        <translation>Lokální databáze</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSendMessage.qml" line="581"/>
        <location filename="../../qml/pages/PageSendMessage.qml" line="581"/>
        <source>Mandatory for initiatory message</source>
        <translation>Povinné pro iniciační datovou zprávu</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSendMessage.qml" line="589"/>
        <location filename="../../qml/pages/PageSendMessage.qml" line="589"/>
        <source>Mandatory for reply message</source>
        <translation>Povinné pro odpovědní datovou zprávu</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSendMessage.qml" line="609"/>
        <location filename="../../qml/pages/PageSendMessage.qml" line="609"/>
        <source>At least one attachment must be appended.</source>
        <translation>Alespoň jedna příloha musí být připojena ke zprávě.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSendMessage.qml" line="619"/>
        <location filename="../../qml/pages/PageSendMessage.qml" line="619"/>
        <source>Add File</source>
        <translation>Přidat soubor</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSendMessage.qml" line="280"/>
        <location filename="../../qml/pages/PageSendMessage.qml" line="280"/>
        <source>Choose a File</source>
        <translation>Vyberte soubor</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSendMessage.qml" line="379"/>
        <location filename="../../qml/pages/PageSendMessage.qml" line="379"/>
        <source>Sending message</source>
        <translation>Odesílání zprávy</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSendMessage.qml" line="383"/>
        <location filename="../../qml/pages/PageSendMessage.qml" line="383"/>
        <source>The action may take several seconds.</source>
        <translation>Akce může trvat několik sekund.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSendMessage.qml" line="624"/>
        <location filename="../../qml/pages/PageSendMessage.qml" line="624"/>
        <source>Add Text</source>
        <translation>Přidat text</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSendMessage.qml" line="753"/>
        <location filename="../../qml/pages/PageSendMessage.qml" line="753"/>
        <source>Remove file &apos;%1&apos; from list.</source>
        <translation>Odebrat soubor &apos;%1&apos; ze seznamu.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSendMessage.qml" line="788"/>
        <location filename="../../qml/pages/PageSendMessage.qml" line="788"/>
        <source>Mandate</source>
        <translation>Zmocnění</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSendMessage.qml" line="847"/>
        <location filename="../../qml/pages/PageSendMessage.qml" line="847"/>
        <source>Our reference number</source>
        <translation>Naše číslo jednací</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSendMessage.qml" line="855"/>
        <location filename="../../qml/pages/PageSendMessage.qml" line="855"/>
        <source>Our file mark</source>
        <translation>Naše spisová značka</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSendMessage.qml" line="863"/>
        <location filename="../../qml/pages/PageSendMessage.qml" line="863"/>
        <source>Your reference number</source>
        <translation>Vaše číslo jednací</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSendMessage.qml" line="871"/>
        <location filename="../../qml/pages/PageSendMessage.qml" line="871"/>
        <source>Your file mark</source>
        <translation>Vaše spisová značka</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSendMessage.qml" line="879"/>
        <location filename="../../qml/pages/PageSendMessage.qml" line="879"/>
        <source>To hands</source>
        <translation>K rukám</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSendMessage.qml" line="824"/>
        <location filename="../../qml/pages/PageSendMessage.qml" line="824"/>
        <source>par.</source>
        <translation>odst.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSendMessage.qml" line="132"/>
        <location filename="../../qml/pages/PageSendMessage.qml" line="157"/>
        <location filename="../../qml/pages/PageSendMessage.qml" line="169"/>
        <location filename="../../qml/pages/PageSendMessage.qml" line="634"/>
        <location filename="../../qml/pages/PageSendMessage.qml" line="132"/>
        <location filename="../../qml/pages/PageSendMessage.qml" line="157"/>
        <location filename="../../qml/pages/PageSendMessage.qml" line="169"/>
        <location filename="../../qml/pages/PageSendMessage.qml" line="634"/>
        <source>Total size of attachments is %1 B.</source>
        <translation>Celková velikost příloh je %1 B.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSendMessage.qml" line="120"/>
        <location filename="../../qml/pages/PageSendMessage.qml" line="145"/>
        <location filename="../../qml/pages/PageSendMessage.qml" line="120"/>
        <location filename="../../qml/pages/PageSendMessage.qml" line="145"/>
        <source>Total size of attachments exceeds %1 MB.</source>
        <translation>Celková velikost příloh přesahuje %1 MB.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSendMessage.qml" line="130"/>
        <location filename="../../qml/pages/PageSendMessage.qml" line="155"/>
        <location filename="../../qml/pages/PageSendMessage.qml" line="130"/>
        <location filename="../../qml/pages/PageSendMessage.qml" line="155"/>
        <source>Total size of attachments is ~%1 kB.</source>
        <translation>Celková velikost příloh je ~%1 kB.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSendMessage.qml" line="1119"/>
        <location filename="../../qml/pages/PageSendMessage.qml" line="1119"/>
        <source>Additional</source>
        <translation>Doplňující</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSendMessage.qml" line="1124"/>
        <location filename="../../qml/pages/PageSendMessage.qml" line="1124"/>
        <source>Message author info page.</source>
        <translation>Informace o autorovi.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSendMessage.qml" line="1130"/>
        <location filename="../../qml/pages/PageSendMessage.qml" line="1130"/>
        <source>Short text editor page.</source>
        <translation>Krátka textová zpráva.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSendMessage.qml" line="1131"/>
        <location filename="../../qml/pages/PageSendMessage.qml" line="1131"/>
        <source>Text</source>
        <translation>Text</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSendMessage.qml" line="1125"/>
        <location filename="../../qml/pages/PageSendMessage.qml" line="1125"/>
        <source>Author Info</source>
        <translation>Odesílatel</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSendMessage.qml" line="513"/>
        <location filename="../../qml/pages/PageSendMessage.qml" line="513"/>
        <source>Immediately download sent message content</source>
        <translation>Ihned stáhnout obsah odeslané zprávy</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSendMessage.qml" line="543"/>
        <location filename="../../qml/pages/PageSendMessage.qml" line="543"/>
        <source>Contacts</source>
        <translation>Kontakty</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSendMessage.qml" line="834"/>
        <location filename="../../qml/pages/PageSendMessage.qml" line="834"/>
        <source>let.</source>
        <translation>písm.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSendMessage.qml" line="848"/>
        <location filename="../../qml/pages/PageSendMessage.qml" line="848"/>
        <source>Enter our reference number</source>
        <translation>Zadejte jednací číslo</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSendMessage.qml" line="856"/>
        <location filename="../../qml/pages/PageSendMessage.qml" line="856"/>
        <source>Enter our file mark</source>
        <translation>Zadejte spisovou značku</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSendMessage.qml" line="864"/>
        <location filename="../../qml/pages/PageSendMessage.qml" line="864"/>
        <source>Enter your reference number</source>
        <translation>Zadejte jednací číslo</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSendMessage.qml" line="872"/>
        <location filename="../../qml/pages/PageSendMessage.qml" line="872"/>
        <source>Enter your file mark</source>
        <translation>Zadejte spisovou značku</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSendMessage.qml" line="880"/>
        <location filename="../../qml/pages/PageSendMessage.qml" line="880"/>
        <source>Enter name</source>
        <translation>Zadejte jméno</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSendMessage.qml" line="900"/>
        <location filename="../../qml/pages/PageSendMessage.qml" line="900"/>
        <source>Select author identification data to be provided to the recipient.</source>
        <translation>Vyberte identifikační údaje o autorovi, které se poskytnou adresátu.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSendMessage.qml" line="905"/>
        <location filename="../../qml/pages/PageSendMessage.qml" line="905"/>
        <source>full name</source>
        <translation>plné jméno</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSendMessage.qml" line="911"/>
        <location filename="../../qml/pages/PageSendMessage.qml" line="911"/>
        <source>date of birth</source>
        <translation>datum narození</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSendMessage.qml" line="916"/>
        <location filename="../../qml/pages/PageSendMessage.qml" line="916"/>
        <source>city of birth</source>
        <translation>místo narození</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSendMessage.qml" line="921"/>
        <location filename="../../qml/pages/PageSendMessage.qml" line="921"/>
        <source>county of birth</source>
        <translation>okres narození</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSendMessage.qml" line="926"/>
        <location filename="../../qml/pages/PageSendMessage.qml" line="926"/>
        <source>RUIAN address code</source>
        <translation>adresní kód RUIAN</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSendMessage.qml" line="931"/>
        <location filename="../../qml/pages/PageSendMessage.qml" line="931"/>
        <source>full address</source>
        <translation>adresa pobytu</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSendMessage.qml" line="936"/>
        <location filename="../../qml/pages/PageSendMessage.qml" line="936"/>
        <source>ROB identification flag</source>
        <translation>příznak ztotožnění s ROB</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSendMessage.qml" line="955"/>
        <location filename="../../qml/pages/PageSendMessage.qml" line="955"/>
        <source>Here you can create a short text message and add it to attachments as a PDF file.</source>
        <translation>Zde můžete napsat krátkou textovou zprávu a vložit ji do příloh jako PDF soubor.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSendMessage.qml" line="956"/>
        <location filename="../../qml/pages/PageSendMessage.qml" line="956"/>
        <source>Add the message to attachments after you have finished editing the text.</source>
        <translation>Přidejte zprávu do příloh poté, co jste ukončili editaci textu.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSendMessage.qml" line="1010"/>
        <location filename="../../qml/pages/PageSendMessage.qml" line="1010"/>
        <source>Send a message from data box &apos;%1&apos;.</source>
        <translation>Odeslat zprávu ze schránky &quot;%1&quot;.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSendMessage.qml" line="1019"/>
        <location filename="../../qml/pages/PageSendMessage.qml" line="1019"/>
        <source>The following fields must be filled in:</source>
        <translation>Následující položky musí být vyplněny:</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSendMessage.qml" line="1027"/>
        <location filename="../../qml/pages/PageSendMessage.qml" line="1027"/>
        <source>attachment file</source>
        <translation>soubor přílohy</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSendMessage.qml" line="1046"/>
        <location filename="../../qml/pages/PageSendMessage.qml" line="1046"/>
        <source>%1: Message has been sent</source>
        <translation>%1: Zpráva byla odeslána</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSendMessage.qml" line="1047"/>
        <location filename="../../qml/pages/PageSendMessage.qml" line="1047"/>
        <source>Message has successfully been sent to all recipients.</source>
        <translation>Zpráva byla úspěšně odeslána všem adresátům.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSendMessage.qml" line="1051"/>
        <location filename="../../qml/pages/PageSendMessage.qml" line="1051"/>
        <source>%1: Error sending message!</source>
        <translation>%1: Chyba při odeslání zprávy!</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSendMessage.qml" line="1052"/>
        <location filename="../../qml/pages/PageSendMessage.qml" line="1052"/>
        <source>Message has NOT been sent to all recipients.</source>
        <translation>Zpráva nebyla odeslána všem adresátům.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSendMessage.qml" line="1082"/>
        <location filename="../../qml/pages/PageSendMessage.qml" line="1082"/>
        <source>mandatory</source>
        <translation>povinné</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSendMessage.qml" line="1118"/>
        <location filename="../../qml/pages/PageSendMessage.qml" line="1118"/>
        <source>Message additional fields page.</source>
        <translation>Doplňující informace o zprávě.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSendMessage.qml" line="964"/>
        <location filename="../../qml/pages/PageSendMessage.qml" line="965"/>
        <location filename="../../qml/pages/PageSendMessage.qml" line="964"/>
        <location filename="../../qml/pages/PageSendMessage.qml" line="965"/>
        <source>View PDF</source>
        <translation>Zobrazit PDF</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSendMessage.qml" line="974"/>
        <location filename="../../qml/pages/PageSendMessage.qml" line="974"/>
        <source>Append PDF</source>
        <translation>Připojit PDF</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSendMessage.qml" line="991"/>
        <location filename="../../qml/pages/PageSendMessage.qml" line="991"/>
        <source>Enter a short text message.</source>
        <translation>Zadejte krátkou textovou zprávu.</translation>
    </message>
</context>
<context>
    <name>PageSettingsAccount</name>
    <message>
        <location filename="../../qml/pages/PageSettingsAccount.qml" line="265"/>
        <location filename="../../qml/pages/PageSettingsAccount.qml" line="265"/>
        <source>Certificate</source>
        <translation>Certifikát</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsAccount.qml" line="250"/>
        <location filename="../../qml/pages/PageSettingsAccount.qml" line="250"/>
        <source>Username + Password</source>
        <translation>Jméno + Heslo</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsAccount.qml" line="251"/>
        <location filename="../../qml/pages/PageSettingsAccount.qml" line="251"/>
        <source>Certificate + Password</source>
        <translation>Certifikát + Heslo</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsAccount.qml" line="252"/>
        <location filename="../../qml/pages/PageSettingsAccount.qml" line="252"/>
        <source>Password + Security code</source>
        <translation>Heslo + Bezpečnostní kód</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsAccount.qml" line="253"/>
        <location filename="../../qml/pages/PageSettingsAccount.qml" line="253"/>
        <source>Password + Security SMS</source>
        <translation>Heslo + SMS kód</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsAccount.qml" line="293"/>
        <location filename="../../qml/pages/PageSettingsAccount.qml" line="293"/>
        <source>Username</source>
        <translation>Uživatelské jméno</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsAccount.qml" line="329"/>
        <location filename="../../qml/pages/PageSettingsAccount.qml" line="329"/>
        <source>Password</source>
        <translation>Heslo</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsAccount.qml" line="442"/>
        <location filename="../../qml/pages/PageSettingsAccount.qml" line="442"/>
        <source>Change username: %1</source>
        <translation>Změna uživatelského jména: %1</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsAccount.qml" line="447"/>
        <location filename="../../qml/pages/PageSettingsAccount.qml" line="447"/>
        <source>Username problem: %1</source>
        <translation>Problém s uživatelským jménem: %1</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsAccount.qml" line="246"/>
        <location filename="../../qml/pages/PageSettingsAccount.qml" line="246"/>
        <source>Select login method</source>
        <translation>Vyberte přihlašovací metodu</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsAccount.qml" line="254"/>
        <location filename="../../qml/pages/PageSettingsAccount.qml" line="254"/>
        <source>Mobile key</source>
        <translation>Mobilní klíč</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsAccount.qml" line="207"/>
        <location filename="../../qml/pages/PageSettingsAccount.qml" line="207"/>
        <source>Data box &apos;%1&apos; could not be added.</source>
        <translation>Schránka &quot;%1&quot; nemohla být přidána.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsAccount.qml" line="240"/>
        <location filename="../../qml/pages/PageSettingsAccount.qml" line="240"/>
        <source>Select the login method which you use to access the data box in the ISDS.</source>
        <translation>Zvolte způsob přihlašování, který používáte pro přístup do datové schránky v prostředí ISDS.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsAccount.qml" line="267"/>
        <location filename="../../qml/pages/PageSettingsAccount.qml" line="267"/>
        <source>The certificate file is needed for authentication purposes. The supplied file must contain a certificate and its corresponding private key.</source>
        <translation>Soubor s certifikátem je vyžadován za účelem autentizace. Dodaný soubor musí obsahovat jak certifikát, tak i odpovídající soukromý klíč.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsAccount.qml" line="268"/>
        <location filename="../../qml/pages/PageSettingsAccount.qml" line="268"/>
        <source>Only PEM and PFX file formats are accepted.</source>
        <translation>Jen formáty PEM a PFX jsou podporovány.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsAccount.qml" line="127"/>
        <location filename="../../qml/pages/PageSettingsAccount.qml" line="127"/>
        <source>Choose a certificate</source>
        <translation>Vybrat certifikát</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsAccount.qml" line="93"/>
        <location filename="../../qml/pages/PageSettingsAccount.qml" line="93"/>
        <source>Data box &apos;%1&apos; couldn&apos;t be added.</source>
        <translation>Schránka &quot;%1&quot; nemohla být přidána.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsAccount.qml" line="93"/>
        <location filename="../../qml/pages/PageSettingsAccount.qml" line="93"/>
        <source>Data box couldn&apos;t be added.</source>
        <translation>Schránka nemohla být přidána.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsAccount.qml" line="94"/>
        <location filename="../../qml/pages/PageSettingsAccount.qml" line="94"/>
        <source>Add Data Box Error</source>
        <translation>Přidání schránky selhalo</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsAccount.qml" line="99"/>
        <location filename="../../qml/pages/PageSettingsAccount.qml" line="99"/>
        <source>Change Username: %1</source>
        <translation>Změna uživatelského jména: %1</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsAccount.qml" line="100"/>
        <location filename="../../qml/pages/PageSettingsAccount.qml" line="100"/>
        <source>Do you want to change the username from &apos;%1&apos; to &apos;%2&apos; for the data box &apos;%3&apos;?</source>
        <translation>Chcete změnit uživatelské jméno z &quot;%1&quot; na &quot;%2&quot; pro schránku &quot;%3&quot;?</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsAccount.qml" line="101"/>
        <location filename="../../qml/pages/PageSettingsAccount.qml" line="101"/>
        <source>Note: It will also change all related local database names and data-box information.</source>
        <translation>Poznámka: Změní se také jména všech odpovídajících databází a informace o schránce.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsAccount.qml" line="206"/>
        <location filename="../../qml/pages/PageSettingsAccount.qml" line="206"/>
        <source>Connection to Data Box: %1</source>
        <translation>Přihlášení do schránky: %1</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsAccount.qml" line="215"/>
        <location filename="../../qml/pages/PageSettingsAccount.qml" line="215"/>
        <source>Add Data Box</source>
        <translation>Přidat schránku</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsAccount.qml" line="238"/>
        <location filename="../../qml/pages/PageSettingsAccount.qml" line="238"/>
        <source>Login Method</source>
        <translation>Způsob přihlašování</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsAccount.qml" line="241"/>
        <location filename="../../qml/pages/PageSettingsAccount.qml" line="241"/>
        <source>Note: NIA login methods such as bank ID, mojeID or eCitizen aren&apos;t supported because the ISDS system doesn&apos;t provide such functionality for third-party applications.</source>
        <translation>Poznámka: Přihlašovací metody NIA jako bankovní ID, mojeID nebo eObčanka nejsou podporovány, protože systém ISDS takovou funkcionalitu pro aplikace třetích stran neposkytuje.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsAccount.qml" line="277"/>
        <location filename="../../qml/pages/PageSettingsAccount.qml" line="277"/>
        <source>Choose File</source>
        <translation>Vybrat soubor</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsAccount.qml" line="283"/>
        <location filename="../../qml/pages/PageSettingsAccount.qml" line="283"/>
        <source>Data Box Title</source>
        <translation>Pojmenování schránky</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsAccount.qml" line="284"/>
        <location filename="../../qml/pages/PageSettingsAccount.qml" line="284"/>
        <source>Enter custom data-box name.</source>
        <translation>Zadejte název schránky.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsAccount.qml" line="286"/>
        <location filename="../../qml/pages/PageSettingsAccount.qml" line="286"/>
        <source>The data-box title is a user-specified name used for the identification of the data box in the application (e.g. &apos;My Personal Data Box&apos;, &apos;Firm Box&apos;, etc.). The chosen name serves only for your convenience.</source>
        <translation>Pojmenování schránky je uživatelem zvolené jméno, které slouží k identifikaci schránky v této aplikaci (např. &quot;Moje osobní datová schránka&quot;, &quot;Firemní schránka&quot;, atd.). Zvolené jméno slouží jen pro Vaši lepší orientaci. Tato položka musí být vyplněna.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsAccount.qml" line="287"/>
        <location filename="../../qml/pages/PageSettingsAccount.qml" line="287"/>
        <source>The entry must be filled in.</source>
        <translation>Položka musí být vyplněna.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsAccount.qml" line="294"/>
        <location filename="../../qml/pages/PageSettingsAccount.qml" line="294"/>
        <source>Enter the login name.</source>
        <translation>Zadejte přihlašovací jméno.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsAccount.qml" line="297"/>
        <location filename="../../qml/pages/PageSettingsAccount.qml" line="297"/>
        <source>The username must consist of at least 6 characters without spaces (only combinations of lower-case letters and digits are allowed). The entry must be filled in.</source>
        <translation>Uživatelské jméno se musí skládat alespoň z 6 znaků bez mezer (jsou povoleny jen kombinace malých písmen a číslic). Polžka musí být vyplněna.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsAccount.qml" line="298"/>
        <location filename="../../qml/pages/PageSettingsAccount.qml" line="298"/>
        <source>Note: The username is not a data-box ID.</source>
        <translation>Upozornění: Uživatelské jméno není ID datové schránky.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsAccount.qml" line="315"/>
        <location filename="../../qml/pages/PageSettingsAccount.qml" line="315"/>
        <source>The username should contain only combinations of lower-case letters and digits.</source>
        <translation>Uživatelské jméno by mělo obsahovat jen kombinaci malých písmen a číslic.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsAccount.qml" line="331"/>
        <location filename="../../qml/pages/PageSettingsAccount.qml" line="331"/>
        <source>The password must be valid and non-expired. To check whether you&apos;ve entered the password correctly you may use the icon in the field on the right. The entry must be filled in.</source>
        <translation>Heslo musí být platné. Pro kontrolu a náhled správnosti zadaného hesla použijte ikonu oka vpravo. Položka musí být vyplněna.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsAccount.qml" line="332"/>
        <location filename="../../qml/pages/PageSettingsAccount.qml" line="332"/>
        <source>Note: You must first change the password using the ISDS web portal if it is your very first attempt to log into the data box.</source>
        <translation>Poznámka: Musíte si nejprve změnit heslo na porálu ISDS, pokud se přihlašujete do schránky úplně poprvé s přiděleným heslem od provozovatele systému ISDS.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsAccount.qml" line="355"/>
        <location filename="../../qml/pages/PageSettingsAccount.qml" line="355"/>
        <source>You have to have the Mobile Key application installed. The Mobile Key application needs to be paired with the corresponding data-box on the ISDS web portal.</source>
        <translation>Měli byste mít naistalovanou a funkční aplikaci eGov mobilní klíč. Tuto externí aplikaci je potřeba mít spárovanou se schránkou na portálu ISDS.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsAccount.qml" line="365"/>
        <location filename="../../qml/pages/PageSettingsAccount.qml" line="365"/>
        <source>Test data box</source>
        <translation>Testovací schránka</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsAccount.qml" line="368"/>
        <location filename="../../qml/pages/PageSettingsAccount.qml" line="368"/>
        <source>Test data boxes are used to access the ISDS testing environment (www.czebox.cz) while the regular data boxes are used to access the production ISDS environment (www.mojedatovaschranka.cz).</source>
        <translation>Testovací schránka slouží k přístupu do testovacího prostředí ISDS (www.czebox.cz), zatím co běžná schránka slouží k přístupu do produkčního (oficiálního) prostředí ISDS (www.mojedatovaschranka.cz).</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsAccount.qml" line="385"/>
        <location filename="../../qml/pages/PageSettingsAccount.qml" line="385"/>
        <source>The data box will be included into the synchronisation process of all data boxes.</source>
        <translation>Schránka bude zahrnuta do hromadné synchronizace všech schránek.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsAccount.qml" line="386"/>
        <location filename="../../qml/pages/PageSettingsAccount.qml" line="386"/>
        <source>The data box won&apos;t be included into the synchronisation process of all data boxes.</source>
        <translation>Schránka nebude zahrnuta do hromadné synchronizace všech schránek.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsAccount.qml" line="392"/>
        <location filename="../../qml/pages/PageSettingsAccount.qml" line="392"/>
        <source>Login &amp;&amp; Add Data Box</source>
        <translation>Přihlásit a přidat schránku</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsAccount.qml" line="406"/>
        <location filename="../../qml/pages/PageSettingsAccount.qml" line="406"/>
        <source>Logging into the data box ...</source>
        <translation>Přihlašuji se do schránky ...</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsAccount.qml" line="443"/>
        <location filename="../../qml/pages/PageSettingsAccount.qml" line="443"/>
        <source>A new username &apos;%1&apos; for the data box &apos;%2&apos; has been set.</source>
        <translation>Nové uživatelské jméno &quot;%1&quot; pro schránku &quot;%2&quot; bylo nastaveno.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsAccount.qml" line="444"/>
        <location filename="../../qml/pages/PageSettingsAccount.qml" line="444"/>
        <source>The data box will use the new settings.</source>
        <translation>Schránka použije nové nastavení.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsAccount.qml" line="448"/>
        <location filename="../../qml/pages/PageSettingsAccount.qml" line="448"/>
        <source>The new username &apos;%1&apos; for data box &apos;%2&apos; has not been set</source>
        <translation>Nové uživatelské jméno &quot;%1&quot; pro schránku &quot;%2&quot; nebylo nastaveno</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsAccount.qml" line="449"/>
        <location filename="../../qml/pages/PageSettingsAccount.qml" line="449"/>
        <source>Data box will use the original settings.</source>
        <translation>Schránka použije původní nastavení.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsAccount.qml" line="472"/>
        <location filename="../../qml/pages/PageSettingsAccount.qml" line="472"/>
        <source>Data Box Settings</source>
        <translation>Nastavení schránky</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsAccount.qml" line="322"/>
        <location filename="../../qml/pages/PageSettingsAccount.qml" line="322"/>
        <source>Enter PIN code in order to show the password.</source>
        <translation>Zadejte PIN kód, aby bylo možné zobrazit heslo.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsAccount.qml" line="322"/>
        <location filename="../../qml/pages/PageSettingsAccount.qml" line="322"/>
        <source>PIN Required</source>
        <translation>Vyžadován PIN</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsAccount.qml" line="323"/>
        <location filename="../../qml/pages/PageSettingsAccount.qml" line="323"/>
        <source>Enter PIN</source>
        <translation>Zadejte PIN</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsAccount.qml" line="350"/>
        <location filename="../../qml/pages/PageSettingsAccount.qml" line="350"/>
        <source>Communication code</source>
        <translation>Komunikační kód</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsAccount.qml" line="333"/>
        <location filename="../../qml/pages/PageSettingsAccount.qml" line="333"/>
        <source>Enter the password.</source>
        <translation>Zadejte heslo.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsAccount.qml" line="351"/>
        <location filename="../../qml/pages/PageSettingsAccount.qml" line="351"/>
        <source>Enter the communication code.</source>
        <translation>Zadejte komunikační kód.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsAccount.qml" line="354"/>
        <location filename="../../qml/pages/PageSettingsAccount.qml" line="354"/>
        <source>The communication code is a string which can be generated in the ISDS web portal. The entry must be filled in.</source>
        <translation>Komunikační kód je textový řetězec, který se vygeneruje na portálu ISDS při párování zařízení s aplikací eGov mobilní klíč. Položka musí být vyplněna.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsAccount.qml" line="360"/>
        <location filename="../../qml/pages/PageSettingsAccount.qml" line="360"/>
        <source>Remember password</source>
        <translation>Zapamatovat si heslo</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsAccount.qml" line="367"/>
        <location filename="../../qml/pages/PageSettingsAccount.qml" line="367"/>
        <source>Switch on if the data box is in the testing environment.</source>
        <translation>Povolte, jestliže se jedná o schránku v testovacím prostředí ISDS.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsAccount.qml" line="373"/>
        <location filename="../../qml/pages/PageSettingsAccount.qml" line="373"/>
        <source>Use local storage</source>
        <translation>Použít lokální úložiště</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsAccount.qml" line="382"/>
        <location filename="../../qml/pages/PageSettingsAccount.qml" line="382"/>
        <source>Synchronise with all</source>
        <translation>Synchronizovat se všemi</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsAccount.qml" line="392"/>
        <location filename="../../qml/pages/PageSettingsAccount.qml" line="392"/>
        <source>Save Changes</source>
        <translation>Uložit změny</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsAccount.qml" line="376"/>
        <location filename="../../qml/pages/PageSettingsAccount.qml" line="376"/>
        <source>Messages and attachments will be locally stored. No active internet connection is needed to access locally stored data.</source>
        <translation>Zprávy a přílohy budou trvale lokálně uloženy v interní databázi. K přístupu k lokálně uloženým datům není potřeba aktivního připojení k internetu.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsAccount.qml" line="377"/>
        <location filename="../../qml/pages/PageSettingsAccount.qml" line="377"/>
        <source>Messages and attachments will be stored only temporarily in memory. These data will be lost on application exit.</source>
        <translation>Zprávy a přílohy budou ukládány pouze dočasně v paměti. Tyto data budou při vypnutí aplikace zahozena.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsAccount.qml" line="344"/>
        <location filename="../../qml/pages/PageSettingsAccount.qml" line="344"/>
        <source>Entered PIN is not valid.</source>
        <translation>Zadaný PIN je neplatný.</translation>
    </message>
</context>
<context>
    <name>PageSettingsGeneral</name>
    <message>
        <location filename="../../qml/pages/PageSettingsGeneral.qml" line="59"/>
        <location filename="../../qml/pages/PageSettingsGeneral.qml" line="59"/>
        <source>Theme</source>
        <translation>Vzhled</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsGeneral.qml" line="64"/>
        <location filename="../../qml/pages/PageSettingsGeneral.qml" line="64"/>
        <source>Dark theme</source>
        <translation>Tmavý režim</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsGeneral.qml" line="68"/>
        <location filename="../../qml/pages/PageSettingsGeneral.qml" line="68"/>
        <source>Note: Theme will be changed after application restart.</source>
        <translation>Poznámka: Vzhled bude změněn až po restartu aplikace.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsGeneral.qml" line="74"/>
        <location filename="../../qml/pages/PageSettingsGeneral.qml" line="74"/>
        <source>Language</source>
        <translation>Jazyk</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsGeneral.qml" line="79"/>
        <location filename="../../qml/pages/PageSettingsGeneral.qml" line="79"/>
        <source>Select language</source>
        <translation>Vyberte jazyk</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsGeneral.qml" line="83"/>
        <location filename="../../qml/pages/PageSettingsGeneral.qml" line="83"/>
        <source>System</source>
        <translation>Ze systému</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsGeneral.qml" line="84"/>
        <location filename="../../qml/pages/PageSettingsGeneral.qml" line="84"/>
        <source>Czech</source>
        <translation>Český</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsGeneral.qml" line="85"/>
        <location filename="../../qml/pages/PageSettingsGeneral.qml" line="85"/>
        <source>English</source>
        <translation>Anglický</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsGeneral.qml" line="86"/>
        <location filename="../../qml/pages/PageSettingsGeneral.qml" line="86"/>
        <source>Ukrainian</source>
        <translation>Ukrajinský</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsGeneral.qml" line="91"/>
        <location filename="../../qml/pages/PageSettingsGeneral.qml" line="91"/>
        <source>Note: Language will be changed after application restart.</source>
        <translation>Poznámka: Jazyk bude změněn až po restartu aplikace.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsGeneral.qml" line="97"/>
        <location filename="../../qml/pages/PageSettingsGeneral.qml" line="97"/>
        <source>Font</source>
        <translation>Font</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsGeneral.qml" line="102"/>
        <location filename="../../qml/pages/PageSettingsGeneral.qml" line="102"/>
        <source>Select font</source>
        <translation>Vyberte font</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsGeneral.qml" line="105"/>
        <location filename="../../qml/pages/PageSettingsGeneral.qml" line="105"/>
        <source>System Default</source>
        <translation>Systémový výchozí</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsGeneral.qml" line="106"/>
        <location filename="../../qml/pages/PageSettingsGeneral.qml" line="106"/>
        <source>Roboto</source>
        <translation>Roboto</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsGeneral.qml" line="107"/>
        <location filename="../../qml/pages/PageSettingsGeneral.qml" line="107"/>
        <source>Source Sans Pro</source>
        <translation>Source Sans Pro</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsGeneral.qml" line="112"/>
        <location filename="../../qml/pages/PageSettingsGeneral.qml" line="112"/>
        <source>Note: Font will be changed after application restart.</source>
        <translation>Poznámka: Font bude změněn až po restartu aplikace.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsGeneral.qml" line="118"/>
        <location filename="../../qml/pages/PageSettingsGeneral.qml" line="118"/>
        <source>Font size and application scale</source>
        <translation>Velikost písma a měřítko</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsGeneral.qml" line="127"/>
        <location filename="../../qml/pages/PageSettingsGeneral.qml" line="127"/>
        <source>Set application font size</source>
        <translation>Nastavit velikost fontu</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsGeneral.qml" line="135"/>
        <location filename="../../qml/pages/PageSettingsGeneral.qml" line="135"/>
        <source>Note: Font size will be changed after application restart. Default is %1.</source>
        <translation>Poznámka: Změna velikosti se projeví až po restartu aplikace. Výchozí hodnota je %1.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsGeneral.qml" line="141"/>
        <location filename="../../qml/pages/PageSettingsGeneral.qml" line="141"/>
        <source>Debug verbosity level</source>
        <translation>Úroveň ladících výpisů</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsGeneral.qml" line="149"/>
        <location filename="../../qml/pages/PageSettingsGeneral.qml" line="149"/>
        <source>Set the amount of logged debugging information</source>
        <translation>Nastavit množství zaznamenávaných ladících informací</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsGeneral.qml" line="167"/>
        <location filename="../../qml/pages/PageSettingsGeneral.qml" line="167"/>
        <source>Set the verbosity of logged entries</source>
        <translation>Nastavit podrobnost zaznamenávaných položek</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsGeneral.qml" line="171"/>
        <location filename="../../qml/pages/PageSettingsGeneral.qml" line="171"/>
        <source>Log verbosity controls the level of detail of the logged entries.</source>
        <translation>Podrobnost záznamů určuje úroveň detailu zaznamenávaných informací.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsGeneral.qml" line="153"/>
        <location filename="../../qml/pages/PageSettingsGeneral.qml" line="153"/>
        <source>Debug verbosity controls the amount of debugging information written to the log file.</source>
        <translation>Úroveň ladících výpisů řídí množství zaznamenávaných ladících informací.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsGeneral.qml" line="40"/>
        <location filename="../../qml/pages/PageSettingsGeneral.qml" line="40"/>
        <source>General Settings</source>
        <translation>Obecné nastavení</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsGeneral.qml" line="159"/>
        <location filename="../../qml/pages/PageSettingsGeneral.qml" line="159"/>
        <source>Log verbosity level</source>
        <translation>Podrobnost záznamů</translation>
    </message>
</context>
<context>
    <name>PageSettingsPin</name>
    <message>
        <location filename="../../qml/pages/PageSettingsPin.qml" line="42"/>
        <location filename="../../qml/pages/PageSettingsPin.qml" line="42"/>
        <source>Currently there is no PIN code set.</source>
        <translation>Momentálně není nastaven žádný PIN kód.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsPin.qml" line="47"/>
        <location filename="../../qml/pages/PageSettingsPin.qml" line="47"/>
        <source>You will be asked to enter a PIN code on application start-up.</source>
        <translation>Při startu aplikace budete vyzváni k zadání PIN kódu.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsPin.qml" line="65"/>
        <location filename="../../qml/pages/PageSettingsPin.qml" line="65"/>
        <source>Enter a new PIN code into both text fields:</source>
        <translation>Zadejte nový PIN kód do obou textových polí:</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsPin.qml" line="80"/>
        <location filename="../../qml/pages/PageSettingsPin.qml" line="96"/>
        <location filename="../../qml/pages/PageSettingsPin.qml" line="80"/>
        <location filename="../../qml/pages/PageSettingsPin.qml" line="96"/>
        <source>In order to change the PIN code you must enter the current and a new PIN code:</source>
        <translation>Pro změnu PIN kódu musíte zadat současný a nový PIN kód:</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsPin.qml" line="111"/>
        <location filename="../../qml/pages/PageSettingsPin.qml" line="111"/>
        <source>Something went wrong!</source>
        <translation>Někde se stala chyba!</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsPin.qml" line="128"/>
        <location filename="../../qml/pages/PageSettingsPin.qml" line="150"/>
        <location filename="../../qml/pages/PageSettingsPin.qml" line="128"/>
        <location filename="../../qml/pages/PageSettingsPin.qml" line="150"/>
        <source>Error: Both new PIN code fields must be filled in.</source>
        <translation>Chyba: Obě pole s novým PIN kódem musí být vyplněna.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsPin.qml" line="137"/>
        <location filename="../../qml/pages/PageSettingsPin.qml" line="159"/>
        <location filename="../../qml/pages/PageSettingsPin.qml" line="137"/>
        <location filename="../../qml/pages/PageSettingsPin.qml" line="159"/>
        <source>Error: Newly entered PIN codes are different.</source>
        <translation>Chyba: Nově zadané PIN kódy se liší.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsPin.qml" line="170"/>
        <location filename="../../qml/pages/PageSettingsPin.qml" line="185"/>
        <location filename="../../qml/pages/PageSettingsPin.qml" line="170"/>
        <location filename="../../qml/pages/PageSettingsPin.qml" line="185"/>
        <source>Error: Current PIN code is wrong.</source>
        <translation>Chyba: Současný PIN kód je nesprávný.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsPin.qml" line="192"/>
        <location filename="../../qml/pages/PageSettingsPin.qml" line="192"/>
        <source>PIN Settings</source>
        <translation>Nastavení PINu</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsPin.qml" line="303"/>
        <location filename="../../qml/pages/PageSettingsPin.qml" line="303"/>
        <source>Save Changes</source>
        <translation>Uložit změny</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsPin.qml" line="220"/>
        <location filename="../../qml/pages/PageSettingsPin.qml" line="220"/>
        <source>Set PIN</source>
        <translation>Nastavit PIN</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsPin.qml" line="229"/>
        <location filename="../../qml/pages/PageSettingsPin.qml" line="229"/>
        <source>Change PIN</source>
        <translation>Změnit PIN</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsPin.qml" line="238"/>
        <location filename="../../qml/pages/PageSettingsPin.qml" line="238"/>
        <source>Disable PIN</source>
        <translation>Zrušit PIN</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsPin.qml" line="250"/>
        <location filename="../../qml/pages/PageSettingsPin.qml" line="250"/>
        <source>Current PIN code</source>
        <translation>Současný PIN kód</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsPin.qml" line="260"/>
        <location filename="../../qml/pages/PageSettingsPin.qml" line="260"/>
        <source>New PIN code</source>
        <translation>Nový PIN kód</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsPin.qml" line="270"/>
        <location filename="../../qml/pages/PageSettingsPin.qml" line="270"/>
        <source>Confirm new PIN code</source>
        <translation>Potvrdit nový PIN kód</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsPin.qml" line="283"/>
        <location filename="../../qml/pages/PageSettingsPin.qml" line="283"/>
        <source>Lock after seconds of inactivity</source>
        <translation>Zamknout po sekundách neaktivity</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsPin.qml" line="291"/>
        <location filename="../../qml/pages/PageSettingsPin.qml" line="291"/>
        <source>don&apos;t lock</source>
        <translation>nezamykat</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsPin.qml" line="293"/>
        <location filename="../../qml/pages/PageSettingsPin.qml" line="293"/>
        <source>Select the number of seconds after which the application is going to be locked.</source>
        <translation>Vybrat dobu (v sekundách), za jak dlouho se aplikace uzamkne.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsPin.qml" line="304"/>
        <location filename="../../qml/pages/PageSettingsPin.qml" line="304"/>
        <source>Accept changes.</source>
        <translation>Přijmout změny.</translation>
    </message>
</context>
<context>
    <name>PageSettingsRecordsManagement</name>
    <message>
        <location filename="../../qml/pages/PageSettingsRecordsManagement.qml" line="76"/>
        <location filename="../../qml/pages/PageSettingsRecordsManagement.qml" line="76"/>
        <source>Service URL</source>
        <translation>URL služby</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsRecordsManagement.qml" line="84"/>
        <location filename="../../qml/pages/PageSettingsRecordsManagement.qml" line="84"/>
        <source>Your token</source>
        <translation>Token</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsRecordsManagement.qml" line="60"/>
        <location filename="../../qml/pages/PageSettingsRecordsManagement.qml" line="60"/>
        <source>Records Management</source>
        <translation>Spisová služba</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsRecordsManagement.qml" line="77"/>
        <location filename="../../qml/pages/PageSettingsRecordsManagement.qml" line="77"/>
        <source>Enter service URL.</source>
        <translation>Zadejte URL služby.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsRecordsManagement.qml" line="79"/>
        <location filename="../../qml/pages/PageSettingsRecordsManagement.qml" line="79"/>
        <source>Please fill in service URL.</source>
        <translation>Vyplňte URL spisové služby.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsRecordsManagement.qml" line="85"/>
        <location filename="../../qml/pages/PageSettingsRecordsManagement.qml" line="85"/>
        <source>Enter your token.</source>
        <translation>Zadejte token.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsRecordsManagement.qml" line="87"/>
        <location filename="../../qml/pages/PageSettingsRecordsManagement.qml" line="87"/>
        <source>Please fill in your identification token.</source>
        <translation>Vyplňte komunikační kód spisové služby.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsRecordsManagement.qml" line="97"/>
        <location filename="../../qml/pages/PageSettingsRecordsManagement.qml" line="97"/>
        <source>Save Changes</source>
        <translation>Uložit změny</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsRecordsManagement.qml" line="98"/>
        <location filename="../../qml/pages/PageSettingsRecordsManagement.qml" line="98"/>
        <source>Accept changes.</source>
        <translation>Přijmout změny.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsRecordsManagement.qml" line="108"/>
        <location filename="../../qml/pages/PageSettingsRecordsManagement.qml" line="108"/>
        <source>Get Service Info</source>
        <translation>Stáhnout info</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsRecordsManagement.qml" line="119"/>
        <location filename="../../qml/pages/PageSettingsRecordsManagement.qml" line="119"/>
        <source>Clear records management data.</source>
        <translation>Vymazat data o spisové službe.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsRecordsManagement.qml" line="120"/>
        <location filename="../../qml/pages/PageSettingsRecordsManagement.qml" line="120"/>
        <source>Clear</source>
        <translation>Vymazat</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsRecordsManagement.qml" line="130"/>
        <location filename="../../qml/pages/PageSettingsRecordsManagement.qml" line="130"/>
        <source>Communication error. Cannot obtain records management info from server. Internet connection failed or service URL and/or identification token could be wrong.</source>
        <translation>Chyba komunikace. Nelze obdržet informace o spisové službě. Připojení k internetu nefunguje nebo URL služby a/nebo identifikační token jsou neplatné.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsRecordsManagement.qml" line="137"/>
        <location filename="../../qml/pages/PageSettingsRecordsManagement.qml" line="137"/>
        <source>Service name</source>
        <translation>Název služby</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsRecordsManagement.qml" line="145"/>
        <location filename="../../qml/pages/PageSettingsRecordsManagement.qml" line="145"/>
        <source>Service token</source>
        <translation>Token služby</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsRecordsManagement.qml" line="154"/>
        <location filename="../../qml/pages/PageSettingsRecordsManagement.qml" line="154"/>
        <source>Service logo</source>
        <translation>Logo služby</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsRecordsManagement.qml" line="162"/>
        <location filename="../../qml/pages/PageSettingsRecordsManagement.qml" line="162"/>
        <source>Records management logo.</source>
        <translation>Logo spisové služby.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsRecordsManagement.qml" line="170"/>
        <location filename="../../qml/pages/PageSettingsRecordsManagement.qml" line="170"/>
        <source>Update list of uploaded files</source>
        <translation>Aktualizovat seznam nahraných zpráv</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsRecordsManagement.qml" line="176"/>
        <location filename="../../qml/pages/PageSettingsRecordsManagement.qml" line="176"/>
        <source>Update list of uploaded files from records management.</source>
        <translation>Aktualizovat seznam nahraných souborů ze spisové služby.</translation>
    </message>
</context>
<context>
    <name>PageSettingsSync</name>
    <message>
        <location filename="../../qml/pages/PageSettingsSync.qml" line="37"/>
        <location filename="../../qml/pages/PageSettingsSync.qml" line="37"/>
        <source>Synchronisation Settings</source>
        <translation>Nastavení synchronizace</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsSync.qml" line="56"/>
        <location filename="../../qml/pages/PageSettingsSync.qml" line="56"/>
        <source>Download only newer messages</source>
        <translation>Stahovat pouze novější zprávy</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsSync.qml" line="62"/>
        <location filename="../../qml/pages/PageSettingsSync.qml" line="62"/>
        <source>Only messages which are not older than 90 days will be downloaded.</source>
        <translation>Budou stahovány jen zprávy, které nejsou starší 90 dnů.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsSync.qml" line="63"/>
        <location filename="../../qml/pages/PageSettingsSync.qml" line="63"/>
        <source>All available messages (including those in the data vault) will be downloaded.</source>
        <translation>Budou stahovány všechny zprávy (včetně těch v datovém trezoru).</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsSync.qml" line="70"/>
        <location filename="../../qml/pages/PageSettingsSync.qml" line="70"/>
        <source>Download complete messages</source>
        <translation>Stahovat kompletní zprávy</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsSync.qml" line="75"/>
        <location filename="../../qml/pages/PageSettingsSync.qml" line="75"/>
        <source>Complete messages will be downloaded during data box synchronisation. This may be slow.</source>
        <translation>V průběhu synchronizace schránek budou stahovány kompletní zprávy. To může být pomalé.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsSync.qml" line="76"/>
        <location filename="../../qml/pages/PageSettingsSync.qml" line="76"/>
        <source>Only message envelopes will be downloaded during data box synchronisation.</source>
        <translation>V průběhu synchronizace schránek budou stahovány pouze obálky zpráv.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsSync.qml" line="83"/>
        <location filename="../../qml/pages/PageSettingsSync.qml" line="83"/>
        <source>Synchronise data box after start</source>
        <translation>Po startu synchronizovat schránky</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsSync.qml" line="88"/>
        <location filename="../../qml/pages/PageSettingsSync.qml" line="88"/>
        <source>Data boxes will automatically be synchronised a few seconds after application start-up.</source>
        <translation>Schránky budou automaticky synchronizovány několik sekund po startu aplikace.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsSync.qml" line="89"/>
        <location filename="../../qml/pages/PageSettingsSync.qml" line="89"/>
        <source>Data boxes will be synchronised only when the action is manually invoked.</source>
        <translation>Schránky budou synchronizovány jen po ručním vyvolání akce.</translation>
    </message>
</context>
<context>
    <name>PageTransferData</name>
    <message>
        <location filename="../../qml/pages/PageTransferData.qml" line="63"/>
        <location filename="../../qml/pages/PageTransferData.qml" line="63"/>
        <source>Transfer Error</source>
        <translation>Chyba přenosu</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageTransferData.qml" line="64"/>
        <location filename="../../qml/pages/PageTransferData.qml" line="64"/>
        <source>An error occurred when backing up data to target location. Transfer has been cancelled.</source>
        <translation>Nastala chyba při zálohování dat do cílového umístění. Přenos byl přerušen.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageTransferData.qml" line="65"/>
        <location filename="../../qml/pages/PageTransferData.qml" line="65"/>
        <source>Application data were not backed up to target location.</source>
        <translation>Data nebyla zálohována do cílového umístění.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageTransferData.qml" line="75"/>
        <location filename="../../qml/pages/PageTransferData.qml" line="75"/>
        <source>Application sandbox and iCloud.</source>
        <translation>Pracovní prostor aplikace a iCloud.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageTransferData.qml" line="76"/>
        <location filename="../../qml/pages/PageTransferData.qml" line="76"/>
        <source>Archive will be stored into the application sandbox and uploaded into the iCloud.</source>
        <translation>Archiv bude uložen do pracovního prostoru aplikace a následně nahrán do iCloud.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageTransferData.qml" line="79"/>
        <location filename="../../qml/pages/PageTransferData.qml" line="79"/>
        <source>Transferred data require to be secured with a PIN.</source>
        <translation>Přenesení dat vyžaduje, aby byla data v aplikaci zabezpečena PINem.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageTransferData.qml" line="80"/>
        <location filename="../../qml/pages/PageTransferData.qml" line="80"/>
        <source>Transfer Problem</source>
        <translation>Problém přenosu</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageTransferData.qml" line="105"/>
        <location filename="../../qml/pages/PageTransferData.qml" line="105"/>
        <source>Transfer Application Data</source>
        <translation>Přenos dat aplikace</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageTransferData.qml" line="124"/>
        <location filename="../../qml/pages/PageTransferData.qml" line="124"/>
        <source>The action allows to transfer complete application data as ZIP archive to another device or to Datovka for desktop. Preserve the ZIP archive in a safe place as it contains login and private data.</source>
        <translation>Tato operace umožňuje přenést kompletní data aplikace jako ZIP archiv do jiného zařízení či Datovky pro PC. Uchovávejte ZIP archiv na bezpečném místě, protože obsahuje přihlašovací údaje a soukromá data.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageTransferData.qml" line="152"/>
        <location filename="../../qml/pages/PageTransferData.qml" line="152"/>
        <source>Create ZIP Archive</source>
        <translation>Vytvořit ZIP archiv</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageTransferData.qml" line="153"/>
        <location filename="../../qml/pages/PageTransferData.qml" line="153"/>
        <source>Proceed with the operation.</source>
        <translation>Pokračovat v operaci.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageTransferData.qml" line="81"/>
        <location filename="../../qml/pages/PageTransferData.qml" line="81"/>
        <source>The data transfer requires application data to be secured with a PIN.</source>
        <translation>Přenesení dat vyžaduje, aby byla data v aplikaci zabezpečena PINem.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageTransferData.qml" line="82"/>
        <location filename="../../qml/pages/PageTransferData.qml" line="82"/>
        <source>Set an application security PIN in the settings and try again.</source>
        <translation>Nastavte PIN v nastavení aplikace a zkuste to znovu.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageTransferData.qml" line="131"/>
        <location filename="../../qml/pages/PageTransferData.qml" line="131"/>
        <source>Set PIN</source>
        <translation>Nastavit PIN</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageTransferData.qml" line="132"/>
        <location filename="../../qml/pages/PageTransferData.qml" line="132"/>
        <source>Set application security PIN.</source>
        <translation>Nastavit bezpečnostní PIN.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageTransferData.qml" line="137"/>
        <location filename="../../qml/pages/PageTransferData.qml" line="137"/>
        <source>Target</source>
        <translation>Cíl</translation>
    </message>
</context>
<context>
    <name>PageWelcome</name>
    <message>
        <location filename="../../qml/pages/PageWelcome.qml" line="62"/>
        <location filename="../../qml/pages/PageWelcome.qml" line="62"/>
        <source>Open application home page.</source>
        <translation>Otevřít domovskou stránku aplikace.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageWelcome.qml" line="73"/>
        <location filename="../../qml/pages/PageWelcome.qml" line="73"/>
        <source>Add an existing data box into the application.</source>
        <translation>Přidejte existující datovou schránku do aplikace.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageWelcome.qml" line="69"/>
        <location filename="../../qml/pages/PageWelcome.qml" line="69"/>
        <source>Welcome in the mobile Datovka.</source>
        <translation>Vítejte v aplikaci mobilní Datovka.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageWelcome.qml" line="78"/>
        <location filename="../../qml/pages/PageWelcome.qml" line="78"/>
        <source>Add Data Box - Wizard</source>
        <translation>Přidat schránku průvodcem</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageWelcome.qml" line="79"/>
        <location filename="../../qml/pages/PageWelcome.qml" line="79"/>
        <source>Add an existing data box using a wizard.</source>
        <translation>Přidat existují schránku průvodcem.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageWelcome.qml" line="85"/>
        <location filename="../../qml/pages/PageWelcome.qml" line="85"/>
        <source>Add Data Box - Form</source>
        <translation>Přidat schránku formulářem</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageWelcome.qml" line="86"/>
        <location filename="../../qml/pages/PageWelcome.qml" line="86"/>
        <source>Add an existing data box using a form.</source>
        <translation>Přidat existující schránku formulářem.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageWelcome.qml" line="93"/>
        <location filename="../../qml/pages/PageWelcome.qml" line="93"/>
        <source>&lt;a href=&quot;%1&quot;&gt;Learn more...&lt;/a&gt;</source>
        <translation>&lt;a href=&quot;%1&quot;&gt;Zjistit více...&lt;/a&gt;</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageWelcome.qml" line="93"/>
        <location filename="../../qml/pages/PageWelcome.qml" line="93"/>
        <source>Read the user manual for more information about the application behaviour before first use.</source>
        <translation>Přečtěte si uživatelskou příručku o chování aplikace před jejím prvním použití.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageWelcome.qml" line="102"/>
        <location filename="../../qml/pages/PageWelcome.qml" line="102"/>
        <source>MojeID banner.</source>
        <translation>Moje ID banner.</translation>
    </message>
</context>
<context>
    <name>PopupMenuAccount</name>
    <message>
        <location filename="../../qml/popups/PopupMenuAccount.qml" line="60"/>
        <location filename="../../qml/popups/PopupMenuAccount.qml" line="60"/>
        <source>Data-Box Settings</source>
        <translation>Nastavení schránky</translation>
    </message>
    <message>
        <location filename="../../qml/popups/PopupMenuAccount.qml" line="65"/>
        <location filename="../../qml/popups/PopupMenuAccount.qml" line="65"/>
        <source>View Data-Box info</source>
        <translation>Zobrazit informace o schránce</translation>
    </message>
    <message>
        <location filename="../../qml/popups/PopupMenuAccount.qml" line="70"/>
        <location filename="../../qml/popups/PopupMenuAccount.qml" line="70"/>
        <source>Data-Box Logo</source>
        <translation>Logo datové schránky</translation>
    </message>
    <message>
        <location filename="../../qml/popups/PopupMenuAccount.qml" line="75"/>
        <location filename="../../qml/popups/PopupMenuAccount.qml" line="75"/>
        <source>Send E-Gov Request</source>
        <translation>Odeslat elektronické podání</translation>
    </message>
    <message>
        <location filename="../../qml/popups/PopupMenuAccount.qml" line="80"/>
        <location filename="../../qml/popups/PopupMenuAccount.qml" line="80"/>
        <source>Import Message</source>
        <translation>Importovat zprávu</translation>
    </message>
    <message>
        <location filename="../../qml/popups/PopupMenuAccount.qml" line="85"/>
        <location filename="../../qml/popups/PopupMenuAccount.qml" line="85"/>
        <source>Find Data Box</source>
        <translation>Vyhledat schránku</translation>
    </message>
    <message>
        <location filename="../../qml/popups/PopupMenuAccount.qml" line="90"/>
        <location filename="../../qml/popups/PopupMenuAccount.qml" line="90"/>
        <source>Back up Data</source>
        <translation>Záloha dat</translation>
    </message>
    <message>
        <location filename="../../qml/popups/PopupMenuAccount.qml" line="95"/>
        <location filename="../../qml/popups/PopupMenuAccount.qml" line="95"/>
        <source>Repair Message Database</source>
        <translation>Opravit databázi zpráv</translation>
    </message>
    <message>
        <location filename="../../qml/popups/PopupMenuAccount.qml" line="101"/>
        <location filename="../../qml/popups/PopupMenuAccount.qml" line="101"/>
        <source>Change Password</source>
        <translation>Změnit heslo</translation>
    </message>
    <message>
        <location filename="../../qml/popups/PopupMenuAccount.qml" line="106"/>
        <location filename="../../qml/popups/PopupMenuAccount.qml" line="106"/>
        <source>Delete Data Box</source>
        <translation>Odstranit schránku</translation>
    </message>
    <message>
        <location filename="../../qml/popups/PopupMenuAccount.qml" line="107"/>
        <location filename="../../qml/popups/PopupMenuAccount.qml" line="107"/>
        <source>Remove Data Box: %1</source>
        <translation>Odstranit schránku: %1</translation>
    </message>
    <message>
        <location filename="../../qml/popups/PopupMenuAccount.qml" line="108"/>
        <location filename="../../qml/popups/PopupMenuAccount.qml" line="108"/>
        <source>Do you want to remove the data box &apos;%1&apos;?</source>
        <translation>Opravdu chcete odstranit schránku &apos;%1&apos;?</translation>
    </message>
    <message>
        <location filename="../../qml/popups/PopupMenuAccount.qml" line="109"/>
        <location filename="../../qml/popups/PopupMenuAccount.qml" line="109"/>
        <source>Note: It will also remove all related local databases and data box information.</source>
        <translation>Poznámka: Budou odstraněny také všechny odpovídající lokální databáze a informace o schránce.</translation>
    </message>
</context>
<context>
    <name>PopupMenuMessage</name>
    <message>
        <location filename="../../qml/popups/PopupMenuMessage.qml" line="53"/>
        <location filename="../../qml/popups/PopupMenuMessage.qml" line="53"/>
        <source>Mark as Read</source>
        <translation>Označit jako přečtené</translation>
    </message>
    <message>
        <location filename="../../qml/popups/PopupMenuMessage.qml" line="58"/>
        <location filename="../../qml/popups/PopupMenuMessage.qml" line="58"/>
        <source>Mark as Unread</source>
        <translation>Označit jako nepřečtené</translation>
    </message>
    <message>
        <location filename="../../qml/popups/PopupMenuMessage.qml" line="64"/>
        <location filename="../../qml/popups/PopupMenuMessage.qml" line="64"/>
        <source>Delete Message</source>
        <translation>Smazat zprávu</translation>
    </message>
    <message>
        <location filename="../../qml/popups/PopupMenuMessage.qml" line="65"/>
        <location filename="../../qml/popups/PopupMenuMessage.qml" line="65"/>
        <source>Delete Message: %1</source>
        <translation>Smazat zprávu: %1</translation>
    </message>
    <message>
        <location filename="../../qml/popups/PopupMenuMessage.qml" line="66"/>
        <location filename="../../qml/popups/PopupMenuMessage.qml" line="66"/>
        <source>Do you want to delete the received message &apos;%1&apos;?</source>
        <translation>Přejete si smazat přijatou zprávu &quot;%1&quot;?</translation>
    </message>
    <message>
        <location filename="../../qml/popups/PopupMenuMessage.qml" line="67"/>
        <location filename="../../qml/popups/PopupMenuMessage.qml" line="67"/>
        <source>Note: It will delete all attachments and message information from the local database.</source>
        <translation>Poznámka: Z místní databáze budou smazány všechny přílohy a informace o zprávě.</translation>
    </message>
</context>
<context>
    <name>PopupMenuMessageDetail</name>
    <message>
        <location filename="../../qml/popups/PopupMenuMessageDetail.qml" line="84"/>
        <location filename="../../qml/popups/PopupMenuMessageDetail.qml" line="84"/>
        <source>View Message Envelope</source>
        <translation>Zobrazit obálku zprávy</translation>
    </message>
    <message>
        <location filename="../../qml/popups/PopupMenuMessageDetail.qml" line="89"/>
        <location filename="../../qml/popups/PopupMenuMessageDetail.qml" line="89"/>
        <source>View Acceptance Info</source>
        <translation>Zobrazit doručenku</translation>
    </message>
    <message>
        <location filename="../../qml/popups/PopupMenuMessageDetail.qml" line="94"/>
        <location filename="../../qml/popups/PopupMenuMessageDetail.qml" line="94"/>
        <source>Use as Template</source>
        <translation>Použít jako šablonu</translation>
    </message>
    <message>
        <location filename="../../qml/popups/PopupMenuMessageDetail.qml" line="99"/>
        <location filename="../../qml/popups/PopupMenuMessageDetail.qml" line="99"/>
        <source>Upload to Records Management</source>
        <translation>Nahrát do spisové služby</translation>
    </message>
    <message>
        <location filename="../../qml/popups/PopupMenuMessageDetail.qml" line="104"/>
        <location filename="../../qml/popups/PopupMenuMessageDetail.qml" line="104"/>
        <source>Send Message ZFO by E-Mail</source>
        <translation>Odeslat ZFO zprávu emailem</translation>
    </message>
    <message>
        <location filename="../../qml/popups/PopupMenuMessageDetail.qml" line="117"/>
        <location filename="../../qml/popups/PopupMenuMessageDetail.qml" line="117"/>
        <source>Save Message ZFO</source>
        <translation>Uložit ZFO zprávu</translation>
    </message>
    <message>
        <location filename="../../qml/popups/PopupMenuMessageDetail.qml" line="120"/>
        <location filename="../../qml/popups/PopupMenuMessageDetail.qml" line="120"/>
        <source>Save Message ZFO: %1</source>
        <translation>Uložit ZFO zprávu: %1</translation>
    </message>
    <message>
        <location filename="../../qml/popups/PopupMenuMessageDetail.qml" line="121"/>
        <location filename="../../qml/popups/PopupMenuMessageDetail.qml" line="121"/>
        <source>You can export files into iCloud or into device local storage.</source>
        <translation>Můžete exportovat soubory do iCloudu nebo do úložiště v zařízení.</translation>
    </message>
    <message>
        <location filename="../../qml/popups/PopupMenuMessageDetail.qml" line="122"/>
        <location filename="../../qml/popups/PopupMenuMessageDetail.qml" line="122"/>
        <source>Do you want to export files to Datovka iCloud container?</source>
        <translation>Přejete si exportovat soubory do kontejneru Datovky v iCloudu?</translation>
    </message>
    <message>
        <location filename="../../qml/popups/PopupMenuMessageDetail.qml" line="131"/>
        <location filename="../../qml/popups/PopupMenuMessageDetail.qml" line="131"/>
        <source>Delete Message</source>
        <translation>Smazat zprávu</translation>
    </message>
    <message>
        <location filename="../../qml/popups/PopupMenuMessageDetail.qml" line="132"/>
        <location filename="../../qml/popups/PopupMenuMessageDetail.qml" line="132"/>
        <source>Delete Message: %1</source>
        <translation>Smazat zprávu: %1</translation>
    </message>
    <message>
        <location filename="../../qml/popups/PopupMenuMessageDetail.qml" line="133"/>
        <location filename="../../qml/popups/PopupMenuMessageDetail.qml" line="133"/>
        <source>Do you want to delete the message &apos;%1&apos;?</source>
        <translation>Přejete si smazat zprávu &apos;%1&apos;?</translation>
    </message>
    <message>
        <location filename="../../qml/popups/PopupMenuMessageDetail.qml" line="134"/>
        <location filename="../../qml/popups/PopupMenuMessageDetail.qml" line="134"/>
        <source>Note: It will delete all attachments and message information from the local database.</source>
        <translation>Poznámka: Z místní databáze budou smazány všechny přílohy a informace o zprávě.</translation>
    </message>
</context>
<context>
    <name>PopupMenuMessageList</name>
    <message>
        <location filename="../../qml/popups/PopupMenuMessageList.qml" line="41"/>
        <location filename="../../qml/popups/PopupMenuMessageList.qml" line="41"/>
        <source>Synchronise All</source>
        <translation>Synchronizovat všechny</translation>
    </message>
    <message>
        <location filename="../../qml/popups/PopupMenuMessageList.qml" line="46"/>
        <location filename="../../qml/popups/PopupMenuMessageList.qml" line="46"/>
        <source>Mark All as Read</source>
        <translation>Označit vše jako přečtené</translation>
    </message>
    <message>
        <location filename="../../qml/popups/PopupMenuMessageList.qml" line="51"/>
        <location filename="../../qml/popups/PopupMenuMessageList.qml" line="51"/>
        <source>Mark All as Unread</source>
        <translation>Označit vše jako nové</translation>
    </message>
</context>
<context>
    <name>PrefListModel</name>
    <message>
        <location filename="../../src/models/prefs_model.cpp" line="590"/>
        <source>default</source>
        <translation>výchozí</translation>
    </message>
    <message>
        <location filename="../../src/models/prefs_model.cpp" line="593"/>
        <source>modified</source>
        <translation>změněno</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../../src/auxiliaries/email_helper.cpp" line="112"/>
        <source>FROM</source>
        <translation>OD</translation>
    </message>
    <message>
        <location filename="../../src/auxiliaries/email_helper.cpp" line="114"/>
        <source>TO</source>
        <translation>KOMU</translation>
    </message>
    <message>
        <location filename="../../src/auxiliaries/email_helper.cpp" line="116"/>
        <source>DELIVERED</source>
        <translation>DORUČENO</translation>
    </message>
    <message>
        <location filename="../../src/sqlite/account_db.cpp" line="753"/>
        <source>Password of username &apos;%1&apos; expires on %2.</source>
        <translation>Heslo uživatele &quot;%1&quot; vyprší %2.</translation>
    </message>
    <message>
        <location filename="../../src/sqlite/account_db.cpp" line="755"/>
        <source>Password of username &apos;%1&apos; expired on %2.</source>
        <translation>Heslo uživatele &quot;%1&quot; vypršelo %2.</translation>
    </message>
    <message>
        <location filename="../../src/auxiliaries/email_helper.cpp" line="110"/>
        <source>ID</source>
        <translation>ID</translation>
    </message>
    <message>
        <location filename="../../src/auxiliaries/email_helper.cpp" line="118"/>
        <source>This email has been generated with Datovka application based on a data message (%1) delivered to databox.</source>
        <translation>Tento email byl vytvořen aplikací Datovka na základě datové zprávy (%1) doručené do datové schránky.</translation>
    </message>
    <message>
        <location filename="../../src/main.cpp" line="259"/>
        <source>Data box application</source>
        <translation>Aplikace pro datové schránky</translation>
    </message>
    <message>
        <location filename="../../src/main.cpp" line="264"/>
        <source>ZFO file to be viewed.</source>
        <translation>Soubor zfo, který má být zobrazen.</translation>
    </message>
    <message>
        <location filename="../../src/worker/task_import_zfo.cpp" line="95"/>
        <source>Cannot open ZFO file &apos;%1&apos;.</source>
        <translation>Nelze otevřít ZFO soubor &apos;%1&apos;.</translation>
    </message>
    <message>
        <location filename="../../src/worker/task_import_zfo.cpp" line="103"/>
        <source>Wrong message format of ZFO file &apos;%1&apos;.</source>
        <translation>Špatný formát ZFO souboru &apos;%1&apos;.</translation>
    </message>
    <message>
        <location filename="../../src/worker/task_import_zfo.cpp" line="107"/>
        <source>Wrong message data of ZFO file &apos;%1&apos;.</source>
        <translation>Špatný obsah ZFO souboru &apos;%1&apos;.</translation>
    </message>
    <message>
        <location filename="../../src/worker/task_import_zfo.cpp" line="128"/>
        <source>Relevant data box does not exist for ZFO file &apos;%1&apos;.</source>
        <translation>Neexistuje příslušná schránka pro import ZFO souboru &quot;&apos;%1&quot;.</translation>
    </message>
    <message>
        <location filename="../../src/worker/task_import_zfo.cpp" line="134"/>
        <source>Import internal error.</source>
        <translation>Interní chyba během importu.</translation>
    </message>
    <message>
        <location filename="../../src/worker/task_import_zfo.cpp" line="159"/>
        <source>Cannot authenticate the ZFO file &apos;%1&apos;.</source>
        <translation>Nelze ověřit pravost ZFO souboru &apos;%1&apos;.</translation>
    </message>
    <message>
        <location filename="../../src/worker/task_import_zfo.cpp" line="164"/>
        <source>ZFO file &apos;%1&apos; is not authentic (returns ISDS)!</source>
        <translation>ZFO Soubor &apos;%1&apos; není autentický (vrací ISDS)!</translation>
    </message>
    <message>
        <location filename="../../src/worker/task_import_zfo.cpp" line="175"/>
        <source>Cannot store the ZFO file &apos;%1&apos; to local database.</source>
        <translation>Nepodařilo se uložit ZFO soubor &apos;%1&apos; do místní databáze.</translation>
    </message>
    <message>
        <location filename="../../src/worker/task_import_zfo.cpp" line="180"/>
        <source>ZFO file &apos;%1&apos; has been imported.</source>
        <translation>ZFO soubor &apos;%1&apos; byl importován.</translation>
    </message>
    <message>
        <location filename="../../src/worker/task_import_zfo.cpp" line="188"/>
        <source>File has not been imported!</source>
        <translation>Soubor nebyl importován!</translation>
    </message>
    <message>
        <location filename="../../src/wrap_records_management.cpp" line="346"/>
        <source>Message &apos;%1&apos; could not be uploaded.</source>
        <translation>Zpráva &apos;%1&apos; nemohla být nahrána.</translation>
    </message>
    <message>
        <location filename="../../src/wrap_records_management.cpp" line="340"/>
        <source>Received error</source>
        <translation>Obržena chyba</translation>
    </message>
    <message>
        <location filename="../../src/wrap_records_management.cpp" line="345"/>
        <source>File Upload Error</source>
        <translation>Chyba nahrávání souboru</translation>
    </message>
    <message>
        <location filename="../../src/wrap_records_management.cpp" line="352"/>
        <source>Successful File Upload</source>
        <translation>Úspěšné nahrání souboru</translation>
    </message>
    <message>
        <location filename="../../src/wrap_records_management.cpp" line="353"/>
        <source>Message &apos;%1&apos; was successfully uploaded into the records management service.</source>
        <translation>Zpráva &apos;%1&apos; byla úspěšně nahrána do spisové služby.</translation>
    </message>
    <message>
        <location filename="../../src/wrap_records_management.cpp" line="354"/>
        <source>It can be now found in the records management service in these locations:</source>
        <translation>Lze ji teď nalézt ve spisové službě v těchto položkách:</translation>
    </message>
</context>
<context>
    <name>QuaGzipFile</name>
    <message>
        <location filename="../../src/datovka_shared/3rdparty/quazip-1.4/quazip/quagzipfile.cpp" line="60"/>
        <source>QIODevice::Append is not supported for GZIP</source>
        <translation>QIODevice::Append není podporován pro GZIP</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/3rdparty/quazip-1.4/quazip/quagzipfile.cpp" line="66"/>
        <source>Opening gzip for both reading and writing is not supported</source>
        <translation>Otevření gzip pro čtení a zápis současně není podporováno</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/3rdparty/quazip-1.4/quazip/quagzipfile.cpp" line="75"/>
        <source>You can open a gzip either for reading or for writing. Which is it?</source>
        <translation>Gzip můžete otevřít jen pro čtení nebo pro zápis. Co to bude?</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/3rdparty/quazip-1.4/quazip/quagzipfile.cpp" line="81"/>
        <source>Could not gzopen() file</source>
        <translation>Nelze provést gzopen() se souborem</translation>
    </message>
</context>
<context>
    <name>QuaZIODevice</name>
    <message>
        <location filename="../../src/datovka_shared/3rdparty/quazip-1.4/quazip/quaziodevice.cpp" line="178"/>
        <source>QIODevice::Append is not supported for QuaZIODevice</source>
        <translation>QIODevice::Append není podporován pro QuaZIODevice</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/3rdparty/quazip-1.4/quazip/quaziodevice.cpp" line="183"/>
        <source>QIODevice::ReadWrite is not supported for QuaZIODevice</source>
        <translation>QIODevice::ReadWrite není podporován pro QuaZIODevice</translation>
    </message>
</context>
<context>
    <name>QuaZipFile</name>
    <message>
        <location filename="../../src/datovka_shared/3rdparty/quazip-1.4/quazip/quazipfile.cpp" line="251"/>
        <source>ZIP/UNZIP API error %1</source>
        <translation>ZIP/UNZIP API chyba %1</translation>
    </message>
</context>
<context>
    <name>SQLiteTbls</name>
    <message>
        <location filename="../../src/sqlite/account_db_tables.cpp" line="77"/>
        <location filename="../../src/sqlite/account_db_tables.cpp" line="198"/>
        <source>Data box ID</source>
        <translation>ID datové schránky</translation>
    </message>
    <message>
        <location filename="../../src/sqlite/account_db_tables.cpp" line="78"/>
        <source>Data box type</source>
        <translation>Typ datové schránky</translation>
    </message>
    <message>
        <location filename="../../src/sqlite/account_db_tables.cpp" line="79"/>
        <location filename="../../src/sqlite/account_db_tables.cpp" line="158"/>
        <source>Identification number (IČO)</source>
        <translation>IČO</translation>
    </message>
    <message>
        <location filename="../../src/sqlite/account_db_tables.cpp" line="80"/>
        <location filename="../../src/sqlite/account_db_tables.cpp" line="147"/>
        <source>Given name</source>
        <translation>Jméno</translation>
    </message>
    <message>
        <location filename="../../src/sqlite/account_db_tables.cpp" line="81"/>
        <location filename="../../src/sqlite/account_db_tables.cpp" line="148"/>
        <source>Middle name</source>
        <translation>Prostření jméno</translation>
    </message>
    <message>
        <location filename="../../src/sqlite/account_db_tables.cpp" line="82"/>
        <location filename="../../src/sqlite/account_db_tables.cpp" line="149"/>
        <source>Surname</source>
        <translation>Příjmení</translation>
    </message>
    <message>
        <location filename="../../src/sqlite/account_db_tables.cpp" line="83"/>
        <location filename="../../src/sqlite/account_db_tables.cpp" line="150"/>
        <source>Surname at birth</source>
        <translation>Rodné příjmení</translation>
    </message>
    <message>
        <location filename="../../src/sqlite/account_db_tables.cpp" line="84"/>
        <location filename="../../src/sqlite/account_db_tables.cpp" line="159"/>
        <source>Company name</source>
        <translation>Název firmy</translation>
    </message>
    <message>
        <location filename="../../src/sqlite/account_db_tables.cpp" line="85"/>
        <location filename="../../src/sqlite/account_db_tables.cpp" line="157"/>
        <source>Date of birth</source>
        <translation>Datum narození</translation>
    </message>
    <message>
        <location filename="../../src/sqlite/account_db_tables.cpp" line="86"/>
        <source>City of birth</source>
        <translation>Místo narození</translation>
    </message>
    <message>
        <location filename="../../src/sqlite/account_db_tables.cpp" line="87"/>
        <source>County of birth</source>
        <translation>Okres narození</translation>
    </message>
    <message>
        <location filename="../../src/sqlite/account_db_tables.cpp" line="88"/>
        <source>State of birth</source>
        <translation>Stát narození</translation>
    </message>
    <message>
        <location filename="../../src/sqlite/account_db_tables.cpp" line="89"/>
        <location filename="../../src/sqlite/account_db_tables.cpp" line="161"/>
        <source>City of residence</source>
        <translation>Sídlo - město</translation>
    </message>
    <message>
        <location filename="../../src/sqlite/account_db_tables.cpp" line="90"/>
        <location filename="../../src/sqlite/account_db_tables.cpp" line="160"/>
        <source>Street of residence</source>
        <translation>Sídlo - ulice</translation>
    </message>
    <message>
        <location filename="../../src/sqlite/account_db_tables.cpp" line="91"/>
        <location filename="../../src/sqlite/account_db_tables.cpp" line="153"/>
        <source>Number in street</source>
        <translation>Číslo orientační</translation>
    </message>
    <message>
        <location filename="../../src/sqlite/account_db_tables.cpp" line="92"/>
        <location filename="../../src/sqlite/account_db_tables.cpp" line="154"/>
        <source>Number in municipality</source>
        <translation>Číslo popisné</translation>
    </message>
    <message>
        <location filename="../../src/sqlite/account_db_tables.cpp" line="93"/>
        <location filename="../../src/sqlite/account_db_tables.cpp" line="155"/>
        <location filename="../../src/sqlite/account_db_tables.cpp" line="162"/>
        <source>Zip code</source>
        <translation>PSČ</translation>
    </message>
    <message>
        <location filename="../../src/sqlite/account_db_tables.cpp" line="94"/>
        <location filename="../../src/sqlite/account_db_tables.cpp" line="163"/>
        <source>State of residence</source>
        <translation>Sídlo - stát</translation>
    </message>
    <message>
        <location filename="../../src/sqlite/account_db_tables.cpp" line="95"/>
        <source>Nationality</source>
        <translation>Státní občanství</translation>
    </message>
    <message>
        <location filename="../../src/sqlite/account_db_tables.cpp" line="98"/>
        <source>Databox state</source>
        <translation>Stav schránky</translation>
    </message>
    <message>
        <location filename="../../src/sqlite/account_db_tables.cpp" line="99"/>
        <source>Effective OVM</source>
        <translation>Efektivní OVM</translation>
    </message>
    <message>
        <location filename="../../src/sqlite/account_db_tables.cpp" line="100"/>
        <location filename="../../src/sqlite/account_db_tables.cpp" line="234"/>
        <source>Open addressing</source>
        <translation>Otevřené adresování</translation>
    </message>
    <message>
        <location filename="../../src/sqlite/account_db_tables.cpp" line="145"/>
        <source>User type</source>
        <translation>Typ uživatele</translation>
    </message>
    <message>
        <location filename="../../src/sqlite/account_db_tables.cpp" line="146"/>
        <source>Permissions</source>
        <translation>Oprávnění</translation>
    </message>
    <message>
        <location filename="../../src/sqlite/account_db_tables.cpp" line="151"/>
        <source>City</source>
        <translation>Město</translation>
    </message>
    <message>
        <location filename="../../src/sqlite/account_db_tables.cpp" line="152"/>
        <source>Street</source>
        <translation>Ulice</translation>
    </message>
    <message>
        <location filename="../../src/sqlite/account_db_tables.cpp" line="156"/>
        <source>State</source>
        <translation>Stát</translation>
    </message>
    <message>
        <location filename="../../src/sqlite/account_db_tables.cpp" line="164"/>
        <source>Password expiration</source>
        <translation>Expirace hesla</translation>
    </message>
    <message>
        <location filename="../../src/sqlite/account_db_tables.cpp" line="199"/>
        <source>Long term storage type</source>
        <translation>Typ datového trezoru</translation>
    </message>
    <message>
        <location filename="../../src/sqlite/account_db_tables.cpp" line="200"/>
        <source>Long term storage capacity</source>
        <translation>Kapacita datového trezoru</translation>
    </message>
    <message>
        <location filename="../../src/sqlite/account_db_tables.cpp" line="201"/>
        <location filename="../../src/sqlite/account_db_tables.cpp" line="206"/>
        <source>Active from</source>
        <translation>Aktivní od</translation>
    </message>
    <message>
        <location filename="../../src/sqlite/account_db_tables.cpp" line="202"/>
        <location filename="../../src/sqlite/account_db_tables.cpp" line="207"/>
        <source>Active to</source>
        <translation>Aktivní do</translation>
    </message>
    <message>
        <location filename="../../src/sqlite/account_db_tables.cpp" line="203"/>
        <source>Capacity used</source>
        <translation>Použitá kapacita</translation>
    </message>
    <message>
        <location filename="../../src/sqlite/account_db_tables.cpp" line="204"/>
        <source>Future long term storage type</source>
        <translation>Budoucí typ datového trezoru</translation>
    </message>
    <message>
        <location filename="../../src/sqlite/account_db_tables.cpp" line="205"/>
        <source>Future long term storage capacity</source>
        <translation>Budoucí kapacita datového trezoru</translation>
    </message>
    <message>
        <location filename="../../src/sqlite/account_db_tables.cpp" line="208"/>
        <source>Payment state</source>
        <translation>Stav platby</translation>
    </message>
    <message>
        <location filename="../../src/sqlite/account_db_tables.cpp" line="235"/>
        <source>Last synchronization time</source>
        <translation>Čas poslední synchronizace</translation>
    </message>
    <message>
        <location filename="../../src/sqlite/file_db_tables.cpp" line="67"/>
        <location filename="../../src/sqlite/message_db_tables.cpp" line="162"/>
        <source>File name</source>
        <translation>Název souboru</translation>
    </message>
    <message>
        <location filename="../../src/sqlite/file_db_tables.cpp" line="70"/>
        <location filename="../../src/sqlite/message_db_tables.cpp" line="165"/>
        <source>Mime type</source>
        <translation>Typ MIME</translation>
    </message>
    <message>
        <location filename="../../src/sqlite/message_db_tables.cpp" line="91"/>
        <source>ID</source>
        <translation>ID</translation>
    </message>
    <message>
        <location filename="../../src/sqlite/message_db_tables.cpp" line="95"/>
        <source>Sender</source>
        <translation>Odesílatel</translation>
    </message>
    <message>
        <location filename="../../src/sqlite/message_db_tables.cpp" line="96"/>
        <source>Sender address</source>
        <translation>Adresa odesílatele</translation>
    </message>
    <message>
        <location filename="../../src/sqlite/message_db_tables.cpp" line="98"/>
        <source>Recipient</source>
        <translation>Adresát</translation>
    </message>
    <message>
        <location filename="../../src/sqlite/message_db_tables.cpp" line="99"/>
        <source>Recipient address</source>
        <translation>Adresa adresáta</translation>
    </message>
    <message>
        <location filename="../../src/sqlite/message_db_tables.cpp" line="106"/>
        <source>To hands</source>
        <translation>K rukám</translation>
    </message>
    <message>
        <location filename="../../src/sqlite/message_db_tables.cpp" line="107"/>
        <source>Subject</source>
        <translation>Předmět</translation>
    </message>
    <message>
        <location filename="../../src/sqlite/message_db_tables.cpp" line="108"/>
        <source>Your reference number</source>
        <translation>Vaše číslo jednací</translation>
    </message>
    <message>
        <location filename="../../src/sqlite/message_db_tables.cpp" line="109"/>
        <source>Our reference number</source>
        <translation>Naše číslo jednací</translation>
    </message>
    <message>
        <location filename="../../src/sqlite/message_db_tables.cpp" line="110"/>
        <source>Your file mark</source>
        <translation>Vaše spisová značka</translation>
    </message>
    <message>
        <location filename="../../src/sqlite/message_db_tables.cpp" line="111"/>
        <source>Our file mark</source>
        <translation>Naše spisová značka</translation>
    </message>
    <message>
        <location filename="../../src/sqlite/message_db_tables.cpp" line="112"/>
        <source>Law</source>
        <translation>Zákon</translation>
    </message>
    <message>
        <location filename="../../src/sqlite/message_db_tables.cpp" line="113"/>
        <source>Year</source>
        <translation>Rok</translation>
    </message>
    <message>
        <location filename="../../src/sqlite/message_db_tables.cpp" line="114"/>
        <source>Section</source>
        <translation>Sekce</translation>
    </message>
    <message>
        <location filename="../../src/sqlite/message_db_tables.cpp" line="115"/>
        <source>Paragraph</source>
        <translation>Odstavec</translation>
    </message>
    <message>
        <location filename="../../src/sqlite/message_db_tables.cpp" line="116"/>
        <source>Letter</source>
        <translation>Písmeno</translation>
    </message>
    <message>
        <location filename="../../src/sqlite/message_db_tables.cpp" line="120"/>
        <source>Delivered</source>
        <translation>Čas dodání</translation>
    </message>
    <message>
        <location filename="../../src/sqlite/message_db_tables.cpp" line="121"/>
        <source>Accepted</source>
        <translation>Čas doručení</translation>
    </message>
    <message>
        <location filename="../../src/sqlite/message_db_tables.cpp" line="122"/>
        <source>Status</source>
        <translation>Stav</translation>
    </message>
    <message>
        <location filename="../../src/sqlite/message_db_tables.cpp" line="123"/>
        <source>Attachment size</source>
        <translation>Velikost příloh</translation>
    </message>
    <message>
        <location filename="../../src/sqlite/message_db_tables.cpp" line="374"/>
        <source>Read locally</source>
        <translation>Přečteno lokálně</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/io/prefs_db_tables.cpp" line="55"/>
        <source>Preference Name</source>
        <translation>Název předvolby</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/io/prefs_db_tables.cpp" line="56"/>
        <source>Type</source>
        <translation>Typ</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/io/prefs_db_tables.cpp" line="57"/>
        <source>Value</source>
        <translation>Hodnota</translation>
    </message>
</context>
<context>
    <name>Service</name>
    <message>
        <location filename="../../src/datovka_shared/gov_services/service/gov_service.cpp" line="170"/>
        <source>Cannot access date field.</source>
        <translation>Nelze získat datum.</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/gov_services/service/gov_service.cpp" line="179"/>
        <source>The field &apos;%1&apos; contains an invalid date &apos;%2&apos;.</source>
        <translation>Pole &apos;%1&apos; obsahuje neplatné datum &apos;%2&apos;.</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/gov_services/service/gov_service.cpp" line="199"/>
        <source>Cannot access IČO field.</source>
        <translation>Nelze získat IČO.</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/gov_services/service/gov_service.cpp" line="208"/>
        <source>The field &apos;%1&apos; contains an invalid value &apos;%2&apos;.</source>
        <translation>Pole &apos;%1&apos; obsahuje neplatnou hodnotu &apos;%2&apos;.</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/gov_services/service/gov_service.cpp" line="228"/>
        <location filename="../../src/datovka_shared/gov_services/service/gov_service.cpp" line="256"/>
        <source>Cannot access string.</source>
        <translation>Nelze získat řetězec.</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/gov_services/service/gov_service.cpp" line="236"/>
        <location filename="../../src/datovka_shared/gov_services/service/gov_service.cpp" line="264"/>
        <source>The field &apos;%1&apos; contains no value.</source>
        <translation>Pole &apos;%1&apos; neobsahuje žádnou hodnotu.</translation>
    </message>
</context>
<context>
    <name>Session</name>
    <message>
        <location filename="../../src/isds/session/isds_session.cpp" line="217"/>
        <source>Cannot open certificate &apos;%1&apos;.</source>
        <translation>Nelze otevřít certifikát &quot;%1&quot;.</translation>
    </message>
    <message>
        <location filename="../../src/isds/session/isds_session.cpp" line="243"/>
        <source>Cannot parse certificate &apos;%1&apos;.</source>
        <translation>Nelze načíst certifikát &quot;%1&quot;.</translation>
    </message>
    <message>
        <location filename="../../src/isds/session/isds_session.cpp" line="251"/>
        <source>Unknown format of certificate &apos;%1&apos;.</source>
        <translation>Neznámý formát certifikátu &quot;%1&quot;.</translation>
    </message>
</context>
<context>
    <name>ShowPasswordOverlaidImage</name>
    <message>
        <location filename="../../qml/components/ShowPasswordOverlaidImage.qml" line="89"/>
        <location filename="../../qml/components/ShowPasswordOverlaidImage.qml" line="89"/>
        <source>Hide password</source>
        <translation>Skrýt heslo</translation>
    </message>
    <message>
        <location filename="../../qml/components/ShowPasswordOverlaidImage.qml" line="89"/>
        <location filename="../../qml/components/ShowPasswordOverlaidImage.qml" line="89"/>
        <source>Show password</source>
        <translation>Zobrazit heslo</translation>
    </message>
</context>
<context>
    <name>SrvcMvCrrVbh</name>
    <message>
        <location filename="../../src/datovka_shared/gov_services/service/gov_mv_crr_vbh.cpp" line="152"/>
        <source>Printout from the driver penalty point system</source>
        <translation>Výpis bodového hodnocení z centrálního registru řidičů</translation>
    </message>
</context>
<context>
    <name>SrvcMvIrVp</name>
    <message>
        <location filename="../../src/datovka_shared/gov_services/service/gov_mv_ir_vp.cpp" line="545"/>
        <source>Printout from the insolvency register</source>
        <translation>Výpis z insolvenčního rejstříku</translation>
    </message>
</context>
<context>
    <name>SrvcMvRtVt</name>
    <message>
        <location filename="../../src/datovka_shared/gov_services/service/gov_mv_rt_vt.cpp" line="175"/>
        <source>Printout from the criminal records</source>
        <translation>Výpis z rejstříku trestů</translation>
    </message>
</context>
<context>
    <name>SrvcMvRtpoVt</name>
    <message>
        <location filename="../../src/datovka_shared/gov_services/service/gov_mv_rtpo_vt.cpp" line="550"/>
        <source>Printout from the criminal records of legal persons</source>
        <translation>Výpis z rejstříku trestů právnických osob</translation>
    </message>
</context>
<context>
    <name>SrvcMvSkdVp</name>
    <message>
        <location filename="../../src/datovka_shared/gov_services/service/gov_mv_skd_vp.cpp" line="546"/>
        <source>Printout from the list of qualified suppliers</source>
        <translation>Výpis ze seznamu kvalifikovaných dodavatelů</translation>
    </message>
</context>
<context>
    <name>SrvcMvVrVp</name>
    <message>
        <location filename="../../src/datovka_shared/gov_services/service/gov_mv_vr_vp.cpp" line="579"/>
        <source>Printout from the public register</source>
        <translation>Výpis z veřejného rejstříku</translation>
    </message>
</context>
<context>
    <name>SrvcMvZrVp</name>
    <message>
        <location filename="../../src/datovka_shared/gov_services/service/gov_mv_zr_vp.cpp" line="545"/>
        <source>Printout from the company register</source>
        <translation>Výpis z živnostenského rejstříku</translation>
    </message>
</context>
<context>
    <name>SrvcSzrRobVu</name>
    <message>
        <location filename="../../src/datovka_shared/gov_services/service/gov_szr_rob_vu.cpp" line="94"/>
        <source>Printout from the resident register</source>
        <translation>Výpis z registru obyvatel</translation>
    </message>
</context>
<context>
    <name>SrvcSzrRobVvu</name>
    <message>
        <location filename="../../src/datovka_shared/gov_services/service/gov_szr_rob_vvu.cpp" line="134"/>
        <source>Printout about the usage of entries from the resident register</source>
        <translation>Výpis o využití údajů z registru obyvatel</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/gov_services/service/gov_szr_rob_vvu.cpp" line="234"/>
        <source>The date of start cannot be later than the date of end.</source>
        <translation>Datum začátku nemůže být později než datum konce.</translation>
    </message>
</context>
<context>
    <name>SrvcSzrRosVv</name>
    <message>
        <location filename="../../src/datovka_shared/gov_services/service/gov_szr_ros_vv.cpp" line="131"/>
        <source>Public printout from the person register</source>
        <translation>Veřejný výpis z registru osob</translation>
    </message>
</context>
<context>
    <name>SzrRobVvuData</name>
    <message>
        <location filename="../../src/datovka_shared/gov_services/service/gov_szr_rob_vvu.cpp" line="93"/>
        <source>From</source>
        <translation>Od</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/gov_services/service/gov_szr_rob_vvu.cpp" line="94"/>
        <source>Select start date</source>
        <translation>Zvolte počáteční datum</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/gov_services/service/gov_szr_rob_vvu.cpp" line="105"/>
        <source>Select end date</source>
        <translation>Zvolte koncové datum</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/gov_services/service/gov_szr_rob_vvu.cpp" line="104"/>
        <source>To</source>
        <translation>Do</translation>
    </message>
</context>
<context>
    <name>SzrRosVvData</name>
    <message>
        <location filename="../../src/datovka_shared/gov_services/service/gov_szr_ros_vv.cpp" line="102"/>
        <source>Identification number (IČO)</source>
        <translation>IČO</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/gov_services/service/gov_szr_ros_vv.cpp" line="103"/>
        <source>Enter identification number (IČO)</source>
        <translation>Zadejte IČO</translation>
    </message>
</context>
<context>
    <name>Tasks</name>
    <message>
        <location filename="../../src/isds/isds_tasks.cpp" line="79"/>
        <source>Unknown error</source>
        <translation>Neznámá chyba</translation>
    </message>
    <message>
        <location filename="../../src/isds/isds_tasks.cpp" line="329"/>
        <source>ZFO import is running... Wait until import will be finished.</source>
        <translation>ZFO import běží... Počkejte, dokud import neskončí.</translation>
    </message>
</context>
<context>
    <name>TextLineItem</name>
    <message>
        <location filename="../../qml/components/TextLineItem.qml" line="74"/>
        <location filename="../../qml/components/TextLineItem.qml" line="74"/>
        <source>Input text action button.</source>
        <translation>Tlačítko akce vkládání textu.</translation>
    </message>
</context>
<context>
    <name>TimedPasswordLine</name>
    <message>
        <location filename="../../qml/components/TimedPasswordLine.qml" line="37"/>
        <location filename="../../qml/components/TimedPasswordLine.qml" line="37"/>
        <source>Enter the password</source>
        <translation>Zadejte heslo</translation>
    </message>
</context>
<context>
    <name>Utility</name>
    <message>
        <location filename="../../src/datovka_shared/utility/strings.cpp" line="59"/>
        <source>unknown</source>
        <translation>neznámý</translation>
    </message>
</context>
<context>
    <name>YesNoDialogue</name>
    <message>
        <location filename="../../qml/dialogues/YesNoDialogue.qml" line="107"/>
        <location filename="../../qml/dialogues/YesNoDialogue.qml" line="107"/>
        <source>Yes</source>
        <translation>Ano</translation>
    </message>
    <message>
        <location filename="../../qml/dialogues/YesNoDialogue.qml" line="117"/>
        <location filename="../../qml/dialogues/YesNoDialogue.qml" line="117"/>
        <source>No</source>
        <translation>Ne</translation>
    </message>
</context>
<context>
    <name>main</name>
    <message>
        <location filename="../../qml/main.qml" line="438"/>
        <location filename="../../qml/main.qml" line="438"/>
        <source>Wrong PIN code.</source>
        <translation>Špatný PIN kód.</translation>
    </message>
    <message>
        <location filename="../../qml/main.qml" line="316"/>
        <location filename="../../qml/main.qml" line="316"/>
        <source>Waiting for acknowledgement from the Mobile key application for the data box &apos;%1&apos;.</source>
        <translation>Čekání na potvrzení z Mobilního klíče pro schránku &quot;%1&quot;.</translation>
    </message>
    <message>
        <location filename="../../qml/main.qml" line="190"/>
        <location filename="../../qml/main.qml" line="190"/>
        <source>Select ZFO Message</source>
        <translation>Vyberte ZFO zprávu</translation>
    </message>
    <message>
        <location filename="../../qml/main.qml" line="427"/>
        <location filename="../../qml/main.qml" line="427"/>
        <source>Version</source>
        <translation>Verze</translation>
    </message>
    <message>
        <location filename="../../qml/main.qml" line="388"/>
        <location filename="../../qml/main.qml" line="393"/>
        <location filename="../../qml/main.qml" line="404"/>
        <location filename="../../qml/main.qml" line="388"/>
        <location filename="../../qml/main.qml" line="393"/>
        <location filename="../../qml/main.qml" line="404"/>
        <source>Enter PIN code</source>
        <translation>Zadejte PIN kód</translation>
    </message>
    <message>
        <location filename="../../qml/main.qml" line="316"/>
        <location filename="../../qml/main.qml" line="316"/>
        <source>Cancel</source>
        <translation>Zrušit</translation>
    </message>
    <message>
        <location filename="../../qml/main.qml" line="411"/>
        <location filename="../../qml/main.qml" line="411"/>
        <source>Verify PIN code.</source>
        <translation>Ověřit PIN kód.</translation>
    </message>
    <message>
        <location filename="../../qml/main.qml" line="500"/>
        <location filename="../../qml/main.qml" line="500"/>
        <source>Password Expiration</source>
        <translation>Expirace hesla</translation>
    </message>
    <message>
        <location filename="../../qml/main.qml" line="501"/>
        <location filename="../../qml/main.qml" line="501"/>
        <source>Passwords of users listed below have expired or are going to expire within few days. You may change the passwords from this application if they haven&apos;t already expired. Expired passwords can only be changed in the ISDS web portal.</source>
        <translation>Uvedená uživatelská hesla expirovala nebo expirují v následujících dnech. Tato hesla, pokud ještě nevypršela, můžete změnit z této aplikace. Expirovaná hesla mohou být změněna pouze na webovém portálu ISDS.</translation>
    </message>
    <message>
        <location filename="../../qml/main.qml" line="509"/>
        <location filename="../../qml/main.qml" line="509"/>
        <source>Donation Request</source>
        <translation>Žádost o dar</translation>
    </message>
    <message>
        <location filename="../../qml/main.qml" line="510"/>
        <location filename="../../qml/main.qml" line="510"/>
        <source>Thank you for using our application.</source>
        <translation>Děkujeme, že používáte naši aplikaci.</translation>
    </message>
    <message>
        <location filename="../../qml/main.qml" line="511"/>
        <location filename="../../qml/main.qml" line="511"/>
        <source>Please consider giving a donation to support the development of this application.</source>
        <translation>Zvažte prosím poskytnutí daru na podporu vývoje této aplikace.</translation>
    </message>
    <message>
        <location filename="../../qml/main.qml" line="512"/>
        <location filename="../../qml/main.qml" line="512"/>
        <source>Donate</source>
        <translation>Darovat</translation>
    </message>
    <message>
        <location filename="../../qml/main.qml" line="524"/>
        <location filename="../../qml/main.qml" line="524"/>
        <source>Cannot Write Configuration File</source>
        <translation>Nemohu zapsat konfigurační soubor</translation>
    </message>
    <message>
        <location filename="../../qml/main.qml" line="525"/>
        <location filename="../../qml/main.qml" line="525"/>
        <source>An error occurred while attempting to save the configuration.</source>
        <translation>Nastala chyba v průběhu pokusu o uložení konfigurace.</translation>
    </message>
    <message>
        <location filename="../../qml/main.qml" line="526"/>
        <location filename="../../qml/main.qml" line="526"/>
        <source>Cannot write file &apos;%1&apos;.</source>
        <translation>Nemohu uložit soubor &quot;%1&quot;.</translation>
    </message>
    <message>
        <location filename="../../qml/main.qml" line="455"/>
        <location filename="../../qml/main.qml" line="455"/>
        <source>Verifying PIN...</source>
        <translation>Ověřuji PIN...</translation>
    </message>
    <message>
        <location filename="../../qml/main.qml" line="316"/>
        <location filename="../../qml/main.qml" line="316"/>
        <source>Mobile Key Login</source>
        <translation>Přihlášení Mobilním klíčem</translation>
    </message>
    <message>
        <location filename="../../qml/main.qml" line="410"/>
        <location filename="../../qml/main.qml" line="410"/>
        <source>Enter</source>
        <translation>Vstoupit</translation>
    </message>
</context>
</TS>
