#!/bin/sh

#  Automatic build script for libssl and libcrypto
#  for iPhoneOS and iPhoneSimulator
              
VERSION="1.0.2u"
IOS_SDKVERSION=`xcrun -sdk iphoneos --show-sdk-version`
CONFIG_OPTIONS=""
ARCHS="x86_64 arm64"

spinner()
{
  local pid=$!
  local delay=0.75
  local spinstr='|/-\'
  while [ "$(ps a | awk '{print $1}' | grep $pid)" ]; do
    local temp=${spinstr#?}
    printf " [%c]  " "$spinstr"
    local spinstr=$temp${spinstr%"$temp"}
    sleep $delay
    printf "\b\b\b\b\b\b"
  done
  printf "    \b\b\b\b"
}

CURRENTPATH=`pwd`
DEVELOPER=`xcode-select -print-path`
IOS_MIN_SDK_VERSION="7.0"

if [ ! -d "$DEVELOPER" ]; then
  echo "xcode path is not set correctly $DEVELOPER does not exist"
  echo "run"
  echo "sudo xcode-select -switch <xcode path>"
  echo "for default installation:"
  echo "sudo xcode-select -switch /Applications/Xcode.app/Contents/Developer"
  exit 1
fi

case $DEVELOPER in
  *\ * )
    echo "Your Xcode path contains whitespaces, which is not supported."
    exit 1
  ;;
esac

case $CURRENTPATH in
  *\ * )
    echo "Your path contains whitespaces, which is not supported by 'make install'."
    exit 1
  ;;
esac

set -e
OPENSSL_ARCHIVE_BASE_NAME=openssl-${VERSION}
OPENSSL_ARCHIVE_FILE_NAME=${OPENSSL_ARCHIVE_BASE_NAME}.tar.gz
if [ ! -e ${OPENSSL_ARCHIVE_FILE_NAME} ]; then
  echo "Downloading ${OPENSSL_ARCHIVE_FILE_NAME}"
  curl -O https://www.openssl.org/source/${OPENSSL_ARCHIVE_FILE_NAME}
else
  echo "Using ${OPENSSL_ARCHIVE_FILE_NAME}"
fi

mkdir -p "${CURRENTPATH}/src"
mkdir -p "${CURRENTPATH}/bin"
mkdir -p "${CURRENTPATH}/lib"

for ARCH in ${ARCHS}
do

  SDKVERSION=$IOS_SDKVERSION
  MIN_SDK_VERSION=$IOS_MIN_SDK_VERSION
 
  if [[ "${ARCH}" == "x86_64" ]]; then
    PLATFORM="iPhoneSimulator"
  else
    PLATFORM="iPhoneOS"
  fi

  export $PLATFORM
  export CROSS_TOP="${DEVELOPER}/Platforms/${PLATFORM}.platform/Developer"
  export CROSS_SDK="${PLATFORM}${SDKVERSION}.sdk"
  export BUILD_TOOLS="${DEVELOPER}"

  mkdir -p "${CURRENTPATH}/bin/${PLATFORM}${SDKVERSION}-${ARCH}.sdk"
  LOG="${CURRENTPATH}/bin/${PLATFORM}${SDKVERSION}-${ARCH}.sdk/build-openssl-${VERSION}.log"

  echo "Building openssl-${VERSION} for ${PLATFORM} ${SDKVERSION} ${ARCH}"
  echo "  Logfile: $LOG"

  LOCAL_CONFIG_OPTIONS="${CONFIG_OPTIONS}"
  export CC="${BUILD_TOOLS}/usr/bin/gcc -arch ${ARCH} -fembed-bitcode"

  echo "  Patch source code..."

  src_work_dir="${CURRENTPATH}/src/${PLATFORM}-${ARCH}"
  mkdir -p "$src_work_dir"
  tar zxf "${CURRENTPATH}/${OPENSSL_ARCHIVE_FILE_NAME}" -C "$src_work_dir"
  cd "${src_work_dir}/${OPENSSL_ARCHIVE_BASE_NAME}"

  chmod u+x ./Configure
  if [[ "$PLATFORM" == "iPhoneOS" ]]; then
    sed -ie "s!static volatile sig_atomic_t intr_signal;!static volatile intr_signal;!" "crypto/ui/ui_openssl.c"
  fi

  echo "  Configure...\c"
  set +e
  if [ "$1" == "verbose" ]; then
    if [ "${ARCH}" == "x86_64" ]; then
      ./Configure no-asm darwin64-x86_64-cc --openssldir="${CURRENTPATH}/bin/${PLATFORM}${SDKVERSION}-${ARCH}.sdk" ${LOCAL_CONFIG_OPTIONS}
    else
      ./Configure iphoneos-cross --openssldir="${CURRENTPATH}/bin/${PLATFORM}${SDKVERSION}-${ARCH}.sdk" ${LOCAL_CONFIG_OPTIONS}
    fi
  else
    if [ "${ARCH}" == "x86_64" ]; then
      (./Configure no-asm darwin64-x86_64-cc --openssldir="${CURRENTPATH}/bin/${PLATFORM}${SDKVERSION}-${ARCH}.sdk" ${LOCAL_CONFIG_OPTIONS} > "${LOG}" 2>&1) & spinner
    else
      (./Configure iphoneos-cross --openssldir="${CURRENTPATH}/bin/${PLATFORM}${SDKVERSION}-${ARCH}.sdk" ${LOCAL_CONFIG_OPTIONS} > "${LOG}" 2>&1) & spinner
    fi
  fi

  if [ $? != 0 ]; then
    echo "Problem while configure - Please check ${LOG}"
    exit 1
  fi

  echo "\n  Patch Makefile..."
  # add -isysroot to CC=
  sed -ie "s!^CFLAG=!CFLAG=-isysroot ${CROSS_TOP}/SDKs/${CROSS_SDK} -miphoneos-version-min=${MIN_SDK_VERSION} !" "Makefile"

  echo "  Make...\c"

  if [ "$1" == "verbose" ]; then
    if [[ ! -z $CONFIG_OPTIONS ]]; then
      make depend
    fi
    make
  else
    if [[ ! -z $CONFIG_OPTIONS ]]; then
      make depend >> "${LOG}" 2>&1
    fi
    (make >> "${LOG}" 2>&1) & spinner
  fi
  echo "\n"

  if [ $? != 0 ]; then
    echo "Problem while make - Please check ${LOG}"
    exit 1
  fi

  set -e
  if [ "$1" == "verbose" ]; then
    make install_sw
    make clean
  else
    make install_sw >> "${LOG}" 2>&1
    make clean >> "${LOG}" 2>&1
  fi

  rm -rf "$src_work_dir"
done

echo "Create fat binaries with lipo tool..."

lipo -create \
  ${CURRENTPATH}/bin/iPhoneOS${IOS_SDKVERSION}-arm64.sdk/lib/libssl.a \
  ${CURRENTPATH}/bin/iPhoneSimulator${IOS_SDKVERSION}-x86_64.sdk/lib/libssl.a \
  -output ${CURRENTPATH}/lib/libssl.a
lipo -create \
  ${CURRENTPATH}/bin/iPhoneOS${IOS_SDKVERSION}-arm64.sdk/lib/libcrypto.a \
  ${CURRENTPATH}/bin/iPhoneSimulator${IOS_SDKVERSION}-x86_64.sdk/lib/libcrypto.a \
  -output ${CURRENTPATH}/lib/libcrypto.a

echo "Libraries are created in ${CURRENTPATH}/lib/"

echo "Create openssl include folder..."

mkdir -p "${CURRENTPATH}/include"
cp -R ${CURRENTPATH}/bin/iPhoneOS${IOS_SDKVERSION}-arm64.sdk/include/openssl ${CURRENTPATH}/include/

echo "All Done."
