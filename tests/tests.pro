
QT += core
QT += testlib

top_srcdir = ../

TEMPLATE = app
TARGET = tests

include($${top_srcdir}pri/version.pri)

isEmpty(MOC_DIR) {
	MOC_DIR = gen_moc
}
isEmpty(OBJECTS_DIR) {
	OBJECTS_DIR = gen_objects
}
isEmpty(UI_DIR) {
	UI_DIR = gen_ui
}
CONFIG += object_parallel_to_source

DEFINES += \
	DEBUG=1 \
	VERSION=\\\"$${VERSION}\\\"

QMAKE_CXXFLAGS += \
    -g -O0 -std=c++11 \
    -Wall -Wextra -pedantic \
    -Wdate-time -Wformat -Werror=format-security

INCLUDEPATH += \
	$${top_srcdir}

# MOC files are generated only directly from *.cpp files when using testlib.
# Adding a custom compiler rule does not help.

SOURCES = \
	$${top_srcdir}tests/tests.cpp

#HEADERS = \
#

include(test_isds_xml_box.pri)
include(test_isds_xml_message.pri)
include(test_isds_xml_response_status.pri)

# Replace possible double slashes with a single slash. Also remove duplicated
# entries.
TMP = ""
for(src, SOURCES): TMP += $$replace(src, //, /)
SOURCES = $$unique(TMP)
TMP = ""
for(hdr, HEADERS): TMP += $$replace(hdr, //, /)
HEADERS = $$unique(TMP)
