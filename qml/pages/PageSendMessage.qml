/*
 * Copyright (C) 2014-2025 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

import QtQuick 2.15
import QtQuick.Controls 2.15
import cz.nic.mobileDatovka 1.0
import cz.nic.mobileDatovka.files 1.0
import cz.nic.mobileDatovka.iOsHelper 1.0
import cz.nic.mobileDatovka.messages 1.0
import cz.nic.mobileDatovka.models 1.0
import cz.nic.mobileDatovka.qmlIdentifiers 1.0

Page {
    id: pageSendMessage

    /* These properties must be set by caller. */
    property var pageView
    property string acntName
    property AcntId acntId: null
    property string msgId
    property int msgType
    property string action: "new"

    /*
     * These properties are constants and specifies some attachment size limits.
     * Do not change these! See ISDS documentation about more message limit info.
     */
    property int max_ATTACHMENT_FILES: 900
    /*
     * Since 1.1.2024 will be 50 files only.
     * property int max_ATTACHMENT_FILES: 50
     */
    property int max_ATTACHMENT_SIZE_BYTES: 20971520 // 20MB = 20 * 1024 * 1024.
    property int max_ATTACHMENT_SIZE_MB: 20
    property int max_OVM_ATTACHMENT_SIZE_BYTES: 52428800 // 50MB = 50 * 1024 * 1024.
    property int max_OVM_ATTACHMENT_SIZE_MB: 50
    property int max_BIG_ATTACHMENT_FILES: 50
    property int max_BIG_ATTACHMENT_SIZE_BYTES: 104857600 // 100MB = 100 * 1024 * 1024.
    property int max_BIG_ATTACHMENT_SIZE_MB: 100

    property string text_COLOR_RED: "#ff0000"
    property string redAsterisk: " <font color=\"red\">*</font>"
    property color sendMsgButtonColor: backButtonBgColor

    property bool boxOVM: false
    property bool vodz: false
    property bool vodzEnabled: false

    /* This property holds total attachment size in bytes */
    property int totalAttachmentSizeBytes: 0

    /*
     * Set sender/author identification level.
     * See src/datovka_shared/isds/types.h for all possible idLevel values
     * from enum IdLevelValue.
     */
    function setSenderIdentIdLevel() {
        // Isds::Type::IDLEV_PUBLISH_USERTYPE
        var idLevel = 0
        if (!dmPublishOwnID.checked) return idLevel
        if (dmPublishOwnName.checked) {
            // Isds::Type::IDLEV_PUBLISH_PERSONNAME
            idLevel += 1
        }
        if (dmPublishOwnBiDate.checked) {
            // Isds::Type::IDLEV_PUBLISH_BIDATE
            idLevel += 2
        }
        if (dmPublishOwnBiCity.checked) {
            // Isds::Type::IDLEV_PUBLISH_BICITY
            idLevel += 4
        }
        if (dmPublishOwnBiCounty.checked) {
            // Isds::Type::IDLEV_PUBLISH_BICOUNTY
            idLevel += 8
        }
        if (dmPublishOwnAdcode.checked) {
            // Isds::Type::IDLEV_PUBLISH_ADCODE
            idLevel += 16
        }
        if (dmPublishOwnFullAddress.checked) {
            // Isds::Type::IDLEV_PUBLISH_FULLADDRESS
            idLevel += 32
        }
        if (dmPublishOwnRobIdent.checked) {
            // Isds::Type::IDLEV_PUBLISH_ROBIDENT
            idLevel += 64
        }
        return idLevel
    }

    /* Check attachment number and size for non-vodz message. */
    function attachmentSizeNonVodz(attachmentNumber) {
        if (attachmentNumber >= max_ATTACHMENT_FILES) {
            attachmentsSizeLabel.text = qsTr("The permitted number of %n attachment(s) has been exceeded.", "", max_ATTACHMENT_FILES)
            attachmentsSizeLabel.color = text_COLOR_RED
            return false
        }
        if (totalAttachmentSizeBytes >= max_OVM_ATTACHMENT_SIZE_BYTES) {
            attachmentsSizeLabel.text = qsTr("Total size of attachments exceeds %1 MB.").arg(max_OVM_ATTACHMENT_SIZE_MB)
            attachmentsSizeLabel.color = text_COLOR_RED
            return false
        }
        if (totalAttachmentSizeBytes >= max_ATTACHMENT_SIZE_BYTES) {
            attachmentsSizeLabel.text = qsTr("Total size of attachments exceeds %1 MB. Most of the data boxes cannot receive messages larger than %1 MB. However, some OVM data boxes can receive messages up to %2 MB.").arg(max_ATTACHMENT_SIZE_MB).arg(max_OVM_ATTACHMENT_SIZE_MB)
            attachmentsSizeLabel.color = text_COLOR_RED
            return true
        }
        if (totalAttachmentSizeBytes >= 1024) {
            attachmentsSizeLabel.text = qsTr("Total size of attachments is ~%1 kB.").arg(Math.ceil(totalAttachmentSizeBytes/1024))
        } else {
            attachmentsSizeLabel.text = qsTr("Total size of attachments is %1 B.").arg(totalAttachmentSizeBytes)
        }
        return true
    }

    /* Check attachment number and size for vodz message. */
    function attachmentSizeVodz(attachmentNumber) {
        if (attachmentNumber >= max_BIG_ATTACHMENT_FILES) {
            attachmentsSizeLabel.text = qsTr("The permitted number of %n attachment(s) has been exceeded.", "", max_BIG_ATTACHMENT_FILES)
            attachmentsSizeLabel.color = text_COLOR_RED
            return false
        }
        if (totalAttachmentSizeBytes >= max_BIG_ATTACHMENT_SIZE_BYTES) {
            attachmentsSizeLabel.text = qsTr("Total size of attachments exceeds %1 MB.").arg(max_BIG_ATTACHMENT_SIZE_MB)
            attachmentsSizeLabel.color = text_COLOR_RED
            return false
        }
        if (totalAttachmentSizeBytes >= max_ATTACHMENT_SIZE_BYTES) {
            attachmentsSizeLabel.text = qsTr("Total size of attachments is ~%1 MB and exceeds the limit of %2 MB for standard messages. Message will be sent as a high-volume message (VoDZ).").arg(Math.ceil(totalAttachmentSizeBytes/1048576)).arg(max_ATTACHMENT_SIZE_MB)
            vodz = true
            return true
        }
        if (totalAttachmentSizeBytes >= 1024) {
            attachmentsSizeLabel.text = qsTr("Total size of attachments is ~%1 kB.").arg(Math.ceil(totalAttachmentSizeBytes/1024))
        } else {
            attachmentsSizeLabel.text = qsTr("Total size of attachments is %1 B.").arg(totalAttachmentSizeBytes)
        }
        return true
    }

    /* Check attachment number and size. */
    function attachmentSizeOk() {
        vodz = false
        attachmentsSizeLabel.color = datovkaPalette.text
        var attNumber = sendMsgAttachmentModel.rowCount()
        totalAttachmentSizeBytes = sendMsgAttachmentModel.dataSizeSum()
        if (attNumber === 0) {
            attachmentsSizeLabel.text = qsTr("Total size of attachments is %1 B.").arg(0)
            return false
        }
        if (vodzEnabled) {
            return attachmentSizeVodz(attNumber)
        } else {
            return attachmentSizeNonVodz(attNumber)
        }
    }

    /* Append one file into send model */
    function addFileToModel(filePath) {
        if (filePath === "") {
            return
        }
        for (var i = 0; i < sendMsgAttachmentModel.rowCount(); i++) {
            if (sendMsgAttachmentModel.filePathFromRow(i) === filePath) {
                return
            }
        }
        var fileName = files.getRealFileNameFromPath(filePath)
        var fileSizeBytes = files.getAttachmentSizeInBytes(filePath)
        sendMsgAttachmentModel.appendFileFromPath(FileIdType.NO_FILE_ID,
            fileName, filePath, fileSizeBytes)
        attachmentSizeOk()
    }

    /* Set mandate fields in QML */
    function setMandate(isdsEnvelope) {
        dmLegalTitleLaw.text = isdsEnvelope.dmLegalTitleLawStr
        dmLegalTitleYear.text = isdsEnvelope.dmLegalTitleYearStr
        dmLegalTitleSect.text = isdsEnvelope.dmLegalTitleSect
        dmLegalTitlePar.text = isdsEnvelope.dmLegalTitlePar
        dmLegalTitlePoint.text = isdsEnvelope.dmLegalTitlePoint
    }

    /* Set reply message data and add recipient to model */
    function setReplyData(acntId, msgId) {
        headerBar.title = qsTr("Reply %1").arg(msgId)
        // get some message envelope data and add recipient to recipient model
        var isdsEnvelope = messages.getMsgEnvelopeDataAndSetRecipient(acntId, msgId)
        dmAnnotation.textLine.text = "Re: " + isdsEnvelope.dmAnnotation
        // swap sender ref and ident data to recipient (reply)
        dmSenderRefNumber.textLine.text = isdsEnvelope.dmRecipientRefNumber
        dmSenderIdent.textLine.text = isdsEnvelope.dmRecipientIdent
        dmRecipientRefNumber.textLine.text = isdsEnvelope.dmSenderRefNumber
        dmRecipientIdent.textLine.text = isdsEnvelope.dmSenderIdent
        setMandate(isdsEnvelope)
        var canUseInitReply = false
        if (isdsEnvelope.dmType === "I" || isdsEnvelope.dmType === "A") {
            canUseInitReply = true
        }
        isds.addRecipient(acntId, isdsEnvelope.dbIDSender, isdsEnvelope.dmSender, isdsEnvelope.dmSenderAddress, canUseInitReply, recipBoxModel);
    }

    /* Set message envelope data and files to attachment model*/
    function setTemplateData(acntId, msgId, msgType) {
        headerBar.title = qsTr("Forward %1").arg(msgId)
        // get some message envelope data, recipient model must be null (no recipient)
        var isdsEnvelope = messages.getMsgEnvelopeDataAndSetRecipient(acntId, msgId)
        dmAnnotation.textLine.text = isdsEnvelope.dmAnnotation
        dmSenderRefNumber.textLine.text = isdsEnvelope.dmSenderRefNumber
        dmSenderIdent.textLine.text = isdsEnvelope.dmSenderIdent
        dmRecipientRefNumber.textLine.text = isdsEnvelope.dmRecipientRefNumber
        dmRecipientIdent.textLine.text = isdsEnvelope.dmRecipientIdent
        setMandate(isdsEnvelope)
        var filePath = messages.getZfoFilePath(acntId, msgId, msgType, false)
        var rawZfoContent = files.rawFileContent(filePath)
        if (rawZfoContent != "") {
            console.log("Loading attachments from ZFO file.")
            files.zfoData(sendMsgAttachmentModel, rawZfoContent)
        } else {
            console.log("Loading attachments from database.")
            sendMsgAttachmentModel.setFromDb(acntId, msgId)
        }
        attachmentSizeOk()
    }

    /* Set message forward data and add ZFO file to model */
    function setForwardZfoData(acntId, msgId, msgType) {
        headerBar.title = qsTr("Forward ZFO %1").arg(msgId)
        var isdsEnvelope = messages.getMsgEnvelopeDataAndSetRecipient(acntId, msgId)
        dmAnnotation.textLine.text = "Fwd: " + isdsEnvelope.dmAnnotation
        var fileName = (msgType === MessageType.TYPE_SENT) ? "ODZ_" + msgId + ".zfo" : "DDZ_" + msgId + ".zfo"
        var filePath = messages.getZfoFilePath(acntId, msgId, msgType, false)
        var rawZfoContent = files.rawFileContent(filePath)
        if (rawZfoContent != "") {
            console.log("Loading ZFO file from storage.")
            addFileToModel(filePath)
        } else {
            console.log("Loading ZFO file from database.")
            var zfoSize = zfo.getZfoSizeFromDb(acntId, msgId)
            if (zfoSize > 0) {
                sendMsgAttachmentModel.appendFileFromPath(FileIdType.DB_ZFO_ID, fileName, "", zfoSize)
                attachmentSizeOk()
            } else {
                mainPanel.visible = false
                errorText.text = qsTr("ZFO file of message %1 is not available. Download the complete message before forwarding.").arg(msgId)
                errorText.visible = true
            }
        }
    }

    /* Enable send message button if all required fields are filled */
    function areReguiredFieldsFilled() {
        var res = (attachmentSizeOk() && (dmAnnotation.textLine.text.toString() !== "") && (recipBoxModel.rowCount() > 0))
        sendMsgButtonColor = (res) ? datovkaPalette.highlightedText : backButtonBgColor
        return res
    }

    function openFileDialog() {
        var title = qsTr("Choose a File")
        var filters = "*.*"
        if (settings.useQmlFileDialog()) {
            console.log("Use QML File Dialog")
            fileDialogue.openFileDialog(title, filters)
            return
        }
        if (Qt.platform.os == "ios") {
            if (settings.useIosDocumentPicker()) {
                console.log("Use Native Ios Document Picker")
                iOSHelper.openDocumentPickerControllerForImport(IosImportAction.IMPORT_SEND,[])
            } else {
                console.log("Use Qt Core File Dialog")
                addFileToModel(files.openFileDialog(title, null, filters))
            }
        } else {
            console.log("Use Qt Core File Dialog")
            addFileToModel(files.openFileDialog(title, null, filters))
        }
    }

    Component.onCompleted: {
        vodzEnabled = settings.isVodzEnbaled()
        boxOVM = accounts.isOvm(acntId)
        var dbID = accounts.dbId(acntId);
        var dbType = accounts.dbType(acntId);
        databoxInfo.text = qsTr("Data Box: %1 (%2)").arg(dbID).arg(dbType)
        dmAllowSubstDelivery.visible = boxOVM
        isds.doIsdsAction("initSendMsgDlg", acntId);
        if (action === "new") {
            headerBar.title = qsTr("New message")
        } else if (action === "reply") {
            setReplyData(acntId, msgId)
        } else if (action === "template") {
            setTemplateData(acntId, msgId, msgType)
        } else if (action == "fwdzfo") {
            setForwardZfoData(acntId, msgId, msgType)
        }
    }

    MessageBox {
        id: sendMsgpopup
    }

    /* Holds send message recipient list model */
    DataboxListModel {
        id: recipBoxModel
        Component.onCompleted: {
        }
    }

    /* Holds send message attachment list model */
    FileListModel {
        id: sendMsgAttachmentModel
        Component.onCompleted: {
        }
    }

    FileDialogue {
        id: fileDialogue
        onAccepted: {
            for (var i = 0; i < fileDialogue.files.length; i++) {
                addFileToModel(removeFilePrefix(decodeURIComponent(fileDialogue.files[i])))
            }
        }
        Connections {
            target: iOSHelper
            function onSendFilesSelectedSig(sendFilePaths) {
                for (var j = 0; j < sendFilePaths.length; ++j) {
                    addFileToModel(sendFilePaths[j])
                }
            }
        }
    }

    Popup {
        id: progressBar
        anchors.centerIn: parent
        modal: true
        width: 260
        closePolicy: Popup.NoAutoClose

        background: Rectangle {
            color: datovkaPalette.base
            border.color: datovkaPalette.text
            radius: defaultMargin
        }

        ControlGroupItem {
            id: busyElementSection
            anchors.centerIn: parent
            width: parent.width
            BusyIndicator {
                id: busyIndicator
                anchors.horizontalCenter: parent.horizontalCenter
                running: progressBar.visible
            }
            AccessibleTextInfo {
                horizontalAlignment: Text.AlignHCenter
                text: qsTr("Sending message") + "..."
            }
            AccessibleTextInfo {
                horizontalAlignment: Text.AlignHCenter
                text: qsTr("The action may take several seconds.")
            }
        }
    }

    Timer {
        id: sendMsgTimer
        interval: 300
        running: false
        repeat: false
        onTriggered: isds.doIsdsAction("sendMessage", acntId)
    }

    function showProgressBar(visible) {
        sendMsgButton.enabled = !visible
        if (visible) {
            progressBar.open()
        } else {
            progressBar.close()
        }
    }

    function doSendRequest() {
        showProgressBar(true)
        sendMsgTimer.start()
    }

    AcntId {
        id: actionAcntId
    }
    Connections {
        target: isds
        function onRunSendMessageSig(userName, testing) {
            actionAcntId.username = userName
            actionAcntId.testing = testing
            if (!isds.sendMessage(actionAcntId, msgId, dmAnnotation.textLine.text.toString(),
                recipBoxModel, sendMsgAttachmentModel,
                dmLegalTitleLaw.text.toString(), dmLegalTitleYear.text.toString(),
                dmLegalTitleSect.text.toString(), dmLegalTitlePar.text.toString(),
                dmLegalTitlePoint.text.toString(), dmToHands.textLine.text.toString(),
                dmRecipientRefNumber.textLine.text.toString(), dmRecipientIdent.textLine.text.toString(),
                dmSenderRefNumber.textLine.text.toString(), dmSenderIdent.textLine.text.toString(),
                false, dmPublishOwnID.checked, dmAllowSubstDelivery.checked,
                dmPersonalDelivery.checked, setSenderIdentIdLevel(),
                downloadSentMsg.checked, vodz)) {
                    showProgressBar(false)
            }
        }
    }

    header: PageHeader {
        id: headerBar
        title: qsTr("Create Message")
        onBackClicked: {
            pageView.pop(StackView.Immediate)
        }
    }

    AccessibleText {
        id: errorText
        visible: false
        anchors.centerIn: parent
        width: parent.width
        horizontalAlignment: Text.AlignHCenter
        wrapMode: Text.Wrap
        text: qsTr("Error during message creation.")
    }

    SwipeView {
        id: swipeView
        anchors.fill: parent
        currentIndex: tabBar.currentIndex
            Flickable {
                id: tabGeneral
                contentHeight: generalContent.implicitHeight
                Pane {
                    id: generalContent
                    anchors.fill: parent
                    ControlGroupItem {
                        spacing: formItemVerticalSpacing
                        ControlGroupItem {
                            AccessibleLabel {
                                text: qsTr("Sender")
                            }
                            AccessibleTextInfo {
                                font.bold: true
                                text: acntName + " (" + acntId.username + ")"
                            }
                            AccessibleTextInfo {
                                id: databoxInfo
                            }
                            AccessibleTextInfoSmall {
                                id: pdzMsgInfo
                            }
                        }
                        ControlGroupItem {
                            TextLineItem {
                                id: dmAnnotation
                                titleText: qsTr("Message subject")
                                textLineTitle: titleText + redAsterisk
                                placeholderText: qsTr("Enter message subject. Mandatory")
                                infoText: qsTr("The message must contain the subject, at least one recipient and attachment.")
                                detailText: qsTr("The subject must be filled in.")
                                isPassword: false
                                textLine.onTextChanged: areReguiredFieldsFilled()
                            }
                        }
                        ControlGroupItem {
                            AccessibleSwitch {
                                id: dmPublishOwnID
                                width: parent.width
                                text: qsTr("Include sender identification")
                                checked: true
                            }
                            AccessibleSwitch {
                                id: dmPersonalDelivery
                                width: parent.width
                                text: qsTr("Personal delivery")
                                checked: false
                            }
                            AccessibleSwitch {
                                id: dmAllowSubstDelivery
                                visible: false
                                width: parent.width
                                text: qsTr("Allow acceptance through fiction")
                                checked: true
                            }
                            AccessibleSwitch {
                                id: downloadSentMsg
                                width: parent.width
                                text: qsTr("Immediately download sent message content")
                                checked: true
                            }
                        }
                    } // Column
                } // Pane
            } // Flickable
//----RECIPIENTS SECTION------------
            Item {
                id: tabRecipients
                Pane {
                    anchors.fill: parent
                    AccessibleTextHelp {
                        id: recipientsLabel
                        anchors.top: parent.top
                        anchors.topMargin: defaultMargin
                        titleText: qsTr("Recipients")
                        labelText: titleText + redAsterisk
                        infoText: qsTr("Select a recipient from local contacts or find a recipient on the ISDS server and append it into recipient list.")
                        detailText: qsTr("At least one recipient must be appended.")
                    }
                    Row {
                        id: buttonMenu
                        spacing: formItemVerticalSpacing
                        anchors.top: recipientsLabel.bottom
                        anchors.topMargin: defaultMargin
                        anchors.horizontalCenter: parent.horizontalCenter
                        AccessibleButton {
                            id: addContact
                            icon.source: "qrc:/ui/account-plus.svg"
                            text: qsTr("Contacts")
                            onClicked: pageView.push(pageContactList, {
                                        "pageView": pageView,
                                        "acntId": acntId,
                                        "recipBoxModel": recipBoxModel
                                        }, StackView.Immediate)
                        }
                        AccessibleButton {
                            id: findDS
                            icon.source: "qrc:/ui/account-search.svg"
                            text: qsTr("Find recipient")
                            onClicked: pageView.push(pageDataboxSearch, {
                                        "pageView": pageView,
                                        "acntId": acntId,
                                        "recipBoxModel": recipBoxModel
                                        }, StackView.Immediate)
                        }
                    } // Row
                    DataboxList {
                        anchors.top: buttonMenu.bottom
                        anchors.topMargin: defaultMargin
                        anchors.bottom: parent.bottom
                        width: parent.width
                        model: recipBoxModel
                        isSendMsgRecipList: true
                        canRemoveBoxes: true
                        localContactFormat: true
                        onCountChanged: areReguiredFieldsFilled()
                        onBoxRemove: {
                            recipBoxModel.removeEntry(boxId)
                        }
                        onPdzDmType: {
                            recipBoxModel.setPdzDmType(boxId, dmType)
                            /* Commercial message is initiatory - dmType 73. */
                            if (dmType === 73) {
                                /* Sender reference number must be filled. */
                                if (dmSenderRefNumber.textLine.text === "") {
                                    tabBar.setCurrentIndex(3)
                                    dmSenderRefNumber.placeholderText = qsTr("Mandatory for initiatory message")
                                    dmSenderRefNumber.textLine.forceActiveFocus()
                                }
                            /* Commercial message is reply on initiatory - dmType 79. */
                            } else if (dmType === 79) {
                                /* Recipient reference number must be filled. */
                                if (dmRecipientRefNumber.textLine.text === "") {
                                    tabBar.setCurrentIndex(3)
                                    dmRecipientRefNumber.placeholderText = qsTr("Mandatory for reply message")
                                    dmRecipientRefNumber.textLine.forceActiveFocus()
                                }
                            }
                        }
                    } // DataboxList
                } // Pane
            } // Item
//----ATTACHMENT SECTION------------
            Item {
                id: tabAttachments
                Pane {
                    anchors.fill: parent
                    AccessibleTextHelp {
                        id: attachmentsLabel
                        anchors.top: parent.top
                        anchors.topMargin: defaultMargin
                        titleText: qsTr("Attachments")
                        labelText: titleText + redAsterisk
                        infoText: qsTr("Select a file from the local storage or enter short text message and append it into attachments.")
                        detailText: qsTr("At least one attachment must be appended.")
                    }
                    Row {
                        id: buttonMenu2
                        spacing: formItemVerticalSpacing
                        anchors.top: attachmentsLabel.bottom
                        anchors.topMargin: defaultMargin
                        anchors.horizontalCenter: parent.horizontalCenter
                        AccessibleButton {
                            icon.source: "qrc:/ui/paper-clip-plus.svg"
                            text: qsTr("Add File")
                            onClicked: openFileDialog()
                        }
                        AccessibleButton {
                            icon.source: "qrc:/ui/document-text.svg"
                            text: qsTr("Add Text")
                            onClicked: tabBar.setCurrentIndex(5)
                        }
                    } // Row
                    AccessibleTextInfoSmall {
                        id: attachmentsSizeLabel
                        anchors.top: buttonMenu2.bottom
                        anchors.topMargin: defaultMargin
                        horizontalAlignment: Text.AlignHCenter
                        wrapMode: Text.Wrap
                        text: qsTr("Total size of attachments is %1 B.").arg(totalAttachmentSizeBytes)
                    }
                    ScrollableListView {
                        id: attachmentListSend
                        delegateHeight: listViewDelegateHeight
                        anchors.top: attachmentsSizeLabel.bottom
                        anchors.topMargin: defaultMargin
                        anchors.bottom: parent.bottom
                        visible: true
                        width: parent.width
                        model: sendMsgAttachmentModel
                        onCountChanged: areReguiredFieldsFilled()
                        delegate: Item {
                            id: attachmentItem
                            width: attachmentListSend.width
                            height: attachmentListSend.delegateHeight
                            SeparatorLine {
                                anchors.top: parent.top
                                visible: index === 0
                            }
                            Item {
                                anchors.fill: parent
                                anchors.margins: defaultMargin
                                Image {
                                    id: imageAttachment
                                    anchors.verticalCenter: parent.verticalCenter
                                    sourceSize.height: parent.height * 0.8
                                    source: files.getAttachmentFileIcon(rFileName)
                                }
                                Column {
                                    id: databoxColumn
                                    anchors.left: imageAttachment.right
                                    anchors.leftMargin: defaultMargin
                                    anchors.right: parent.right
                                    anchors.verticalCenter: parent.verticalCenter
                                    spacing: defaultMargin
                                    AccessibleText {
                                        id: fileNameId
                                        width: parent.width - fileActionButton.width
                                        elide: Text.ElideRight
                                        font.bold: true
                                        text: rFileName
                                    }
                                    AccessibleTextInfoSmall {
                                        text: (rFilePath !== "") ? rFilePath : qsTr("Local database")
                                        width: parent.width - fileActionButton.width
                                        elide: Text.ElideRight
                                        wrapMode: Text.NoWrap
                                    }
                                    AccessibleTextInfoSmall {
                                        id: fileSizeId
                                        width: parent.width - fileActionButton.width
                                        text: rFileSizeStr
                                    }
                                }
                                MouseArea {
                                    function handleClick() {
                                        // fileId is set and is valid, use files from database
                                        if (rFileId > 0) {
                                            if (files.isZfoFile(rFileName)) {
                                                pageView.push(pageMessageDetail, {
                                                    "pageView": pageView,
                                                    "fromLocalDb": false,
                                                    "rawZfoContent": files.getFileRawContentFromDb(acntId, rFileId)})
                                            } else {
                                                files.openAttachmentFromDb(acntId, rFileId)
                                            }
                                        } else if (rFileId === FileIdType.DB_ZFO_ID)  {
                                                pageView.push(pageMessageDetail, {
                                                    "pageView": pageView,
                                                    "fromLocalDb": false,
                                                    "rawZfoContent": zfo.getZfoContentFromDb(acntId, msgId)})
                                        } else if (rFileId === FileIdType.NO_FILE_ID && rFilePath === "")  {
                                          if (files.isZfoFile(rFileName)) {
                                                pageView.push(pageMessageDetail, {
                                                    "pageView": pageView,
                                                    "fromLocalDb": false,
                                                    "rawZfoContent": rBinaryContent})
                                            } else {
                                                files.openAttachment(rFileName, rBinaryContent)
                                            }
                                        } else {
                                            if (files.isZfoFile(getFileNameFromPath(rFilePath))) {
                                                pageView.push(pageMessageDetail, {
                                                    "pageView": pageView,
                                                    "fromLocalDb": false,
                                                    "rawZfoContent": files.rawFileContent(rFilePath)})
                                            } else {
                                                files.openAttachment(getFileNameFromPath(rFileName), files.rawFileContent(rFilePath))
                                            }
                                        }
                                    }
                                    anchors.fill: parent
                                    Accessible.role: Accessible.Button
                                    Accessible.name: qsTr("Open file '%1'.").arg(rFileName)
                                    Accessible.onPressAction: {
                                        handleClick()
                                    }
                                    onClicked: {
                                        handleClick()
                                    }
                                }
                                Item {
                                    id: fileActionButton
                                    z: 1
                                    anchors.right: parent.right
                                    height: parent.height
                                    width: baseDelegateHeight
                                    AccessibleRoundButton {
                                        anchors.centerIn: parent
                                        icon.source: "qrc:/ui/remove.svg"
                                    }
                                    MouseArea {
                                        function handleClick() {
                                            sendMsgAttachmentModel.removeItem(index)
                                            attachmentSizeOk()
                                        }
                                        anchors.fill: parent
                                        Accessible.role: Accessible.Button
                                        Accessible.name: qsTr("Remove file '%1' from list.").arg(rFileName)
                                        Accessible.onScrollDownAction: attachmentListSend.scrollDown()
                                        Accessible.onScrollUpAction: attachmentListSend.scrollUp()
                                        Accessible.onPressAction: {
                                            handleClick()
                                        }
                                        onClicked: {
                                            handleClick()
                                        }
                                    }
                                }
                            }
                            SeparatorLine {
                                anchors.bottom: parent.bottom
                            }
                        } // Rectangle
                        ScrollIndicator.vertical: ScrollIndicator {}
                    } // Listview
                }
            }
//----ADDITIONALS SECTION------------
            Flickable {
                id: tabAdditional
                contentHeight: additonalsContent.implicitHeight
                Pane {
                    id: additonalsContent
                    anchors.fill: parent
                    Column {
                        anchors.right: parent.right
                        anchors.left: parent.left
                        spacing: formItemVerticalSpacing
    //---Mandate---------------------
                        ControlGroupItem {
                            AccessibleLabel {
                                id: mandate
                                text: qsTr("Mandate")
                            }
                            Row {
                                width: parent.width
                                spacing: 3
                                AccessibleTextField {
                                    id: dmLegalTitleLaw
                                    maximumLength: 4
                                    width: mandate.font.pixelSize * 5
                                    inputMethodHints: Qt.ImhDigitsOnly
                                }
                                AccessibleText {
                                    anchors.verticalCenter: parent.verticalCenter
                                    text: "/"
                                }
                                AccessibleTextField {
                                    id: dmLegalTitleYear
                                    maximumLength: 4
                                    width: mandate.font.pixelSize * 5
                                    inputMethodHints: Qt.ImhDigitsOnly
                                }
                                AccessibleText {
                                    anchors.verticalCenter: parent.verticalCenter
                                    text: "§"
                                }
                                AccessibleTextField {
                                    id: dmLegalTitleSect
                                    maximumLength: 4
                                    width: mandate.font.pixelSize * 5
                                }
                            }
                            Row {
                                width: parent.width
                                spacing: 3
                                AccessibleText {
                                    anchors.verticalCenter: parent.verticalCenter
                                    text: qsTr("par.")
                                }
                                AccessibleTextField {
                                    id: dmLegalTitlePar
                                    maximumLength: 2
                                    width: mandate.font.pixelSize * 4
                                    inputMethodHints: Qt.ImhDigitsOnly
                                }
                                AccessibleText {
                                    anchors.verticalCenter: parent.verticalCenter
                                    text: qsTr("let.")
                                }
                                AccessibleTextField {
                                    id: dmLegalTitlePoint
                                    maximumLength: 2
                                    width: mandate.font.pixelSize * 4
                                }
                            }
                        }
    //---Our ref.number---------------------
                        ControlGroupItem {
                            TextLineItem {
                                id: dmSenderRefNumber
                                textLineTitle: qsTr("Our reference number")
                                placeholderText: qsTr("Enter our reference number")
                            }
                        }
    //---Our file mark---------------------
                        ControlGroupItem {
                            TextLineItem {
                                id: dmSenderIdent
                                textLineTitle: qsTr("Our file mark")
                                placeholderText: qsTr("Enter our file mark")
                            }
                        }
    //---Your ref.number---------------------
                        ControlGroupItem {
                            TextLineItem {
                                id: dmRecipientRefNumber
                                textLineTitle: qsTr("Your reference number")
                                placeholderText: qsTr("Enter your reference number")
                            }
                        }
    //---Your file mark---------------------
                        ControlGroupItem {
                            TextLineItem {
                                id: dmRecipientIdent
                                textLineTitle: qsTr("Your file mark")
                                placeholderText: qsTr("Enter your file mark")
                            }
                        }
    //---To hands---------------------
                        ControlGroupItem {
                            TextLineItem {
                                id: dmToHands
                                textLineTitle:qsTr("To hands")
                                placeholderText: qsTr("Enter name")
                            }
                        }
                    } // Column
                 } // Pane
            } // Flickable
//----MESSAGE SENDER INFO SECTION------------
            Flickable {
                id: tabSenderInfo
                contentHeight: additonalsContent.implicitHeight
                Pane {
                    id: senderInfoContent
                    anchors.fill: parent
                    Column {
                        anchors.right: parent.right
                        anchors.left: parent.left
                        spacing: formItemVerticalSpacing
                        visible: dmPublishOwnID.checked
                        AccessibleTextInfo {
                            horizontalAlignment: Text.AlignHCenter
                            text: qsTr("Select author identification data to be provided to the recipient.")
                        }
                        AccessibleSwitch {
                            id: dmPublishOwnName
                            width: parent.width
                            text: qsTr("full name")
                            checked: true
                        }
                        AccessibleSwitch {
                            id: dmPublishOwnBiDate
                            width: parent.width
                            text: qsTr("date of birth")
                        }
                        AccessibleSwitch {
                            id: dmPublishOwnBiCity
                            width: parent.width
                            text: qsTr("city of birth")
                        }
                        AccessibleSwitch {
                            id: dmPublishOwnBiCounty
                            width: parent.width
                            text: qsTr("county of birth")
                        }
                        AccessibleSwitch {
                            id: dmPublishOwnAdcode
                            width: parent.width
                            text: qsTr("RUIAN address code")
                        }
                        AccessibleSwitch {
                            id: dmPublishOwnFullAddress
                            width: parent.width
                            text: qsTr("full address")
                        }
                        AccessibleSwitch {
                            id: dmPublishOwnRobIdent
                            width: parent.width
                            text: qsTr("ROB identification flag")
                        }
                    }
                }
            }
//----TEXT MESSAGE SECTION------------
            Flickable {
                id: tabTextMessage
                contentHeight: textMessageContent.implicitHeight
                Pane {
                    id: textMessageContent
                    anchors.fill: parent
                    Column {
                        id: panelTop
                        anchors.right: parent.right
                        anchors.left: parent.left
                        spacing: formItemVerticalSpacing
                        AccessibleTextInfo {
                            horizontalAlignment: Text.AlignHCenter
                            text: qsTr("Here you can create a short text message and add it to attachments as a PDF file.")
                                + " " + qsTr("Add the message to attachments after you have finished editing the text.")
                        }
                        Row {
                            anchors.horizontalCenter: parent.horizontalCenter
                            spacing: formItemVerticalSpacing * 2
                            AccessibleButton {
                                enabled: textMessage.text.length > 0
                                icon.source: "qrc:/ui/eye.svg"
                                accessibleName: qsTr("View PDF")
                                text: qsTr("View PDF")
                                onClicked: {
                                    files.viewPDF(textMessage.text)
                                }
                            }
                            AccessibleButton {
                                id: appendPdfImage
                                enabled: textMessage.text.length > 0
                                icon.source: "qrc:/ui/paper-clip-plus.svg"
                                accessibleName: qsTr("Append PDF")
                                text: qsTr("Append")
                                onClicked: {
                                    var filePath = files.appendPDF(textMessage.text)
                                    if (filePath !== "") {
                                        addFileToModel(filePath)
                                        tabBar.setCurrentIndex(2)
                                        Qt.inputMethod.hide()
                                    }
                                }
                            }
                        }
                        TextArea {
                            id: textMessage
                            width: parent.width
                            wrapMode: Text.Wrap
                            padding: defaultMargin
                            placeholderText: qsTr("Enter a short text message.")
                            implicitWidth: parent.width
                            implicitHeight: parent.width
                        }
                    } // Column
                 } // Pane
            } // Flickable
    }

    AccessibleRoundButton {
        id: sendMsgButton
        z: 1
        anchors.right: parent.right
        anchors.rightMargin: defaultMargin
        anchors.bottom: parent.bottom
        anchors.bottomMargin: defaultMargin
        icon.source: "qrc:/ui/send-msg.svg"
        icon.width: roundButtonSize
        icon.height: roundButtonSize
        accessibleName: qsTr("Send a message from data box '%1'.").arg(acntName)
        background: Rectangle {
            radius: sendMsgButton.radius
            color: sendMsgButtonColor
        }
        onClicked: {
           if (areReguiredFieldsFilled()) {
                doSendRequest()
            } else {
                var pText = qsTr("The following fields must be filled in:") + "\n"
                if (dmAnnotation.textLine.text.toString() === "") {
                    pText += "\n" + qsTr("message subject")
                }
                if (recipBoxModel.rowCount() <= 0) {
                    pText += "\n" + qsTr("recipient data box")
                }
                if (!attachmentSizeOk()) {
                    pText += "\n" + qsTr("attachment file")
                }
                sendMsgpopup.error(qsTr("Send Message Error"), pText,
                    qsTr("Please fill in the respective fields before attempting to send the message."))
            }
        }
    }

    Connections {
        target: isds
        function onLoginFailed(userName, testing) {
            if (acntId.username === userName && acntId.testing === testing) {
                showProgressBar(false)
            }
        }
        function onSentMessageFinished(userName, testing, success, infoTxt) {
            accounts.loadModelCounters(accountModel)
            showProgressBar(false)
            if (success) {
                okMsgBox.info(qsTr("%1: Message has been sent").arg(userName),
                qsTr("Message has successfully been sent to all recipients."),
                infoTxt);
                pageView.pop(StackView.Immediate)
            } else {
                okMsgBox.error(qsTr("%1: Error sending message!").arg(userName),
                qsTr("Message has NOT been sent to all recipients."),
                infoTxt);
            }
        }
        function onInitSendMsgDlgSig(userName, testing) {
            actionAcntId.username = userName
            actionAcntId.testing = testing
            var boxOVM = accounts.isOvm(actionAcntId)
            if (boxOVM) {
                return
            }
            var dbID = accounts.dbId(actionAcntId);
            isds.getPdzInfo(actionAcntId, dbID);
        }
        function onGetPdzInfoSig(pdzInfo) {
            pdzMsgInfo.text = pdzInfo
        }
        function onShowMessageBox(title, text, detailText) {
            showProgressBar(false)
            okMsgBox.error(title, text, detailText)
        }
    } // Connections

    AccessibleTextInfoSmall {
        visible: (swipeView.currentIndex < 3)
        anchors.left: parent.left
        anchors.leftMargin: defaultMargin
        anchors.bottom: parent.bottom
        anchors.bottomMargin: defaultMargin + defaultMargin
        font.bold: true
        text: redAsterisk + " " + qsTr("mandatory")
    }

    PageIndicator {
        currentIndex: tabBar.currentIndex
        count: tabBar.count
        anchors.bottom: parent.bottom
        anchors.bottomMargin: defaultMargin
        anchors.horizontalCenter: parent.horizontalCenter
    }

    footer: TabBar {
        id: tabBar
        currentIndex: swipeView.currentIndex
        property int buttonWidth: Math.max(100, tabBar.width / 6)
        TabButton {
            width: tabBar.buttonWidth
            Accessible.role: Accessible.Button
            Accessible.name: qsTr("General message settings page.")
            text: qsTr("Subject")
        }
        TabButton {
            width: tabBar.buttonWidth
            Accessible.role: Accessible.Button
            Accessible.name: qsTr("Recipient list page.")
            text: qsTr("Recipients")
        }
        TabButton {
            width: tabBar.buttonWidth
            Accessible.role: Accessible.Button
            Accessible.name: qsTr("Attachment list page.")
            text: qsTr("Attachments")
        }
        TabButton {
            width: tabBar.buttonWidth
            Accessible.role: Accessible.Button
            Accessible.name: qsTr("Message additional fields page.")
            text: qsTr("Additional")
        }
        TabButton {
            width: tabBar.buttonWidth
            Accessible.role: Accessible.Button
            Accessible.name: qsTr("Message author info page.")
            text: qsTr("Author Info")
        }
        TabButton {
            width: tabBar.buttonWidth
            Accessible.role: Accessible.Button
            Accessible.name: qsTr("Short text editor page.")
            text: qsTr("Text")
        }
    } // TabBar
} // Page
