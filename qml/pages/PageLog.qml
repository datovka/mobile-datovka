/*
 * Copyright (C) 2014-2025 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

import QtQuick 2.7
import QtQuick.Controls 2.15
import QtQuick.Layouts 1.3
import QtQuick.Controls.Material 2.15
import cz.nic.mobileDatovka 1.0

Page {

    property string logFileLocation: ""
    property string logFilePath: ""

    Component.onCompleted: {
        var logTxt = log.loadLogContent(logFilePath)
        logContent.text = qsTr("Log from current run:") + "\n\n" + logTxt
        logFileLocation = log.getLogFileLocation()
    }

    MessageBox {
        id: yesNoMsgBox
        onAcceptAndClosed: log.sendLogViaEmail(logContent.text)
    }

    header: PageHeader {
        id: headerBar
        title: qsTr("Log Viewer")
        onBackClicked: {
            pageView.pop()
        }
    }

    Flickable {
        anchors.fill: parent
        contentHeight: flickContent.implicitHeight
        clip: true
        Pane {
            id: flickContent
            anchors.fill: parent
            AccessibleTextInfoSmall {
                id: logContent
                anchors.fill: parent
            }
        }
        ScrollIndicator.vertical: ScrollIndicator {}
    }

    footer: ToolBar {
        id: toolBar
        spacing: formItemVerticalSpacing
        Material.foreground: buttonBgColor
        RowLayout {
            anchors.centerIn : parent
            AccessibleToolButton {
                accessibleName: qsTr("Choose a log file.")
                icon.source: "qrc:/ui/folder.svg"
                text: qsTr("Choose Log")
                display: AbstractButton.TextUnderIcon
                onClicked: {
                    var title = qsTr("Choose a Log File")
                    var filters = "*.*"
                    var logFile = files.openFileDialog(title, null, filters)
                    var logTxt = log.loadLogContent(logFile)
                    logContent.text = qsTr("Log file") + ": " + getFileNameFromPath(removeFilePrefix(decodeURIComponent(logFile))) + "\n\n" + logTxt
                }
            }
            AccessibleToolButton {
                accessibleName: qsTr("Send the log file via e-mail to developers.")
                icon.source: "qrc:/ui/send-msg.svg"
                text: qsTr("Send Log")
                display: AbstractButton.TextUnderIcon
                onClicked: yesNoMsgBox.question(qsTr("Send Log via E-mail"),
                    qsTr("Do you want to send the log information to developers?"),
                    "")
            }
        }
    }
} // Page
