/*
 * Copyright (C) 2014-2025 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

import QtQuick 2.7
import QtQuick.Controls 2.15
import cz.nic.mobileDatovka 1.0
import cz.nic.mobileDatovka.models 1.0
import cz.nic.mobileDatovka.qmlIdentifiers 1.0
import cz.nic.mobileDatovka.qmlInteraction 1.0

Page {

    property string transferZipPath: "Storage/Documents/Datovka/Transfer"

    onFocusChanged: {
        canTransferData()
    }

    function showBusyIndicator(visible) {
        busyElement.visible = visible
        headerBar.enabled = !visible
        createTransferZipButton.visible = !visible
    }

    function canTransferData() {
        if (backup_zip.isSetPin()) {
            setPinButton.visible = false
            backupPathLabel.visible = true
            createTransferZipButton.visible = backup_zip.canBackupSelectedAccounts(backupSelectionModel, transferZipPath, true)
            if (createTransferZipButton.visible) {
                actionInfoText.color = datovkaPalette.text
            } else {
                actionInfoText.color = "red"
            }
        }
    }

    function createZipArchive() {
        showBusyIndicator(true)
        var success = backup_zip.transferAppDataToZip(transferZipPath)
        showBusyIndicator(false)
        if (!success) {
            okMsgBox.info(qsTr("Transfer Error"),
                qsTr("An error occurred when backing up data to target location. Transfer has been cancelled."),
                qsTr("Application data were not backed up to target location."))
        } else {
            createTransferZipButton.visible = false
        }
    }

    Component.onCompleted: {
        backup_zip.stopWorker(true)
        if (iOS) {
            backupPathLabel.visible = true
            transferZipPath = qsTr("Application sandbox and iCloud.")
            infoText.text = infoText.text + " " + qsTr("Archive will be stored into the application sandbox and uploaded into the iCloud.")
        }
        if (!backup_zip.isSetPin()) {
            infoText.text = infoText.text + " " + qsTr("Transferred data require to be secured with a PIN.")
            okMsgBox.error(qsTr("Transfer Problem"),
                qsTr("The data transfer requires application data to be secured with a PIN."),
                qsTr("Set an application security PIN in the settings and try again."))
            setPinButton.visible = true
            createTransferZipButton.visible = false
            backupPathLabel.visible = false
        } else {
            canTransferData()
        }
    }

    Component.onDestruction: {
        backup_zip.stopWorker(false)
    }

    BackupRestoreZipData {
        id: backup_zip
    }

    BackupRestoreSelectionModel {
        id: backupSelectionModel
    }

    header: PageHeader {
        id: headerBar
        title: qsTr("Transfer Application Data")
        onBackClicked: {
            pageView.pop()
        }
    }

    Flickable {
        anchors.fill: parent
        contentHeight: flickContent.implicitHeight
        Pane {
            id: flickContent
            anchors.fill: parent
            Column {
                anchors.right: parent.right
                anchors.left: parent.left
                spacing: formItemVerticalSpacing
                AccessibleTextInfo {
                    id: infoText
                    horizontalAlignment: Text.AlignHCenter
                    text: qsTr("The action allows to transfer complete application data as ZIP archive to another device or to Datovka for desktop. Preserve the ZIP archive in a safe place as it contains login and private data.")
                }
                AccessibleButton {
                    id: setPinButton
                    visible: false
                    anchors.horizontalCenter: parent.horizontalCenter
                    icon.source: "qrc:/ui/key-variant.svg"
                    text: qsTr("Set PIN")
                    accessibleName: qsTr("Set application security PIN.")
                    onClicked: pageView.push("qrc:/qml/pages/PageSettingsPin.qml")
                }
                AccessibleTextInfo {
                    id: backupPathLabel
                    text: qsTr("Target") + ": " + transferZipPath
                }
                AccessibleTextInfo {
                    id: sizeInfoText
                    width: parent.width
                }
                AccessibleTextInfo {
                    id: actionInfoText
                    font.bold: true
                }
                AccessibleButton {
                    id: createTransferZipButton
                    visible: false
                    anchors.horizontalCenter: parent.horizontalCenter
                    icon.source: "qrc:/ui/checkbox-marked-circle.svg"
                    text: qsTr("Create ZIP Archive")
                    accessibleName: qsTr("Proceed with the operation.")
                    onClicked: createZipArchive()
                }
            }
            Connections {
                target: backup_zip
                function onActionTextSig(actionInfo) {
                    actionInfoText.text = actionInfo
                }
                function onSizeTextSig(sizeInfo) {
                    sizeInfoText.text = sizeInfo
                }
                function onShowMessageBox(title, text, detailText) {
                    okMsgBox.error(title, text, detailText)
                }
            }
        }
    }
    Rectangle {
        id: busyElement
        visible: false
        anchors.fill: parent
        color: datovkaPalette.text
        opacity: 0.2
        z: 2
        BusyIndicator {
            anchors.centerIn: parent
            running: true
        }
    }
}
