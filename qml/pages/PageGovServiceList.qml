/*
 * Copyright (C) 2014-2025 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

import QtQuick 2.7
import QtQuick.Controls 2.15
import cz.nic.mobileDatovka 1.0
import cz.nic.mobileDatovka.models 1.0
import cz.nic.mobileDatovka.qmlIdentifiers 1.0

Page {

    /* These properties must be set by caller. */
    property var pageView
    property string acntName
    property AcntId acntId: null

    property string gsInternIdSelected

    Component.onCompleted: {
        gov.loadServicesToModel(acntId, govServiceModel)
        proxyGovServiceModel.setSourceModel(govServiceModel)
    }

    GovServiceListModel {
        id: govServiceModel
    }

    ListSortFilterProxyModel {
        id: proxyGovServiceModel

        Component.onCompleted: {
            setFilterRoles([GovServiceListModel.ROLE_GOV_SRVC_FULL_NAME,
                GovServiceListModel.ROLE_GOV_SRVC_INST_NAME,
                GovServiceListModel.ROLE_GOV_SRVC_BOXID])
        }
    }

    header: PageHeader {
        id: headerBar
        title: qsTr("E-Gov Services")
        onBackClicked: {
            pageView.pop()
        }
        AccessibleToolButton {
            id: searchButton
            anchors.right: parent.right
            icon.source: "qrc:/ui/magnify.svg"
            accessibleName: qsTr("Set filter.")
            onClicked: {
                filterBar.open()
                filterBar.filterField.forceActiveFocus()
                Qt.inputMethod.show()
            }
        }
    }

    FilterBar {
        id: filterBar
        y: header.height
        onTextChanged: text => proxyGovServiceModel.setFilterRegExpStr(text)
        onClearClicked: close()
    }

    GovServiceList {
        id: govServiceList
        visible: true
        anchors.fill: parent
        model: proxyGovServiceModel
        onCountChanged: {
            emptyList.visible = (govServiceList.count === 0)
        }
        onGovServiceClicked: {
            pageView.push(pageGovService, {
                "pageView": pageView,
                "acntName" : acntName,
                "acntId": acntId,
                "gsInternId": gsInternId,
                "gsFullName": gsFullName,
                "gsInstName": gsInstName
            }, StackView.Immediate)
        }
    }
    AccessibleTextInfo {
        id: emptyList
        visible: false
        anchors.centerIn: parent
        anchors.margins: defaultMargin
        horizontalAlignment: Text.AlignHCenter
        text: (filterBar.filterField.displayText.length === 0) ?
            qsTr("No available e-government service.") :
            qsTr("No e-government service found that matches filter text '%1'.").arg(filterBar.filterField.displayText)
    }
}
