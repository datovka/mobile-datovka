/*
 * Copyright (C) 2014-2025 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

import QtQuick 2.7
import QtQuick.Controls 2.15
import cz.nic.mobileDatovka 1.0

Page {

    /* These properties must be set by caller. */
    property var pageView

    Component.onCompleted: {
        messages.stopWorker(true)
    }

    function showBusyIndicator(visible) {
        busyElement.visible = visible
        convertButton.enabled = !visible
        quitButton.enabled = !visible
        resultText.visible = !visible
    }

    header: PageHeader {
        id: headerBar
        isFirstPage: true
        disableNavigaion: true
        title: qsTr("Database Conversion")
    }

    Flickable {
        anchors.fill: parent
        contentHeight: flickContent.implicitHeight
        Pane {
            id: flickContent
            anchors.fill: parent
            Column {
                anchors.right: parent.right
                anchors.left: parent.left
                spacing: formItemVerticalSpacing
                AccessibleTextInfo {
                    horizontalAlignment: Text.AlignHCenter
                    text: qsTr("The application requires the conversion of database files into a new format. Don't close or shut down the application until the conversion process finishes. The application needs to be restarted in order to load the changes after conversion.")
                }
                AccessibleButton {
                    id: convertButton
                    text: qsTr("Start Conversion")
                    anchors.horizontalCenter: parent.horizontalCenter
                    onClicked: {
                        showBusyIndicator(true)
                        console.log("The conversion of databases is being processed.")
                        var ret = messages.convertDatabases()
                        if (!ret) {
                            resultText.text = qsTr("Database conversion finished. Some message databases couldn't be converted. The application needs to be restarted in order to load the changes.")
                        }
                        showBusyIndicator(false)
                        convertButton.visible = false
                        quitButton.visible = true
                    }
                }
                AccessibleButton {
                    id: quitButton
                    visible: false
                    text: qsTr("Restart")
                    anchors.horizontalCenter: parent.horizontalCenter
                    onClicked: messages.quitApp()
                }
                AccessibleTextInfo {
                    id: resultText
                    visible: false
                    horizontalAlignment: Text.AlignHCenter
                    width: parent.width
                    font.bold: true
                    text: qsTr("Database conversion finished. All message databases have successfully been converted. The application needs to be restarted in order to load the changes.")
                }
                AccessibleTextInfo {
                    id: convertInfo
                    visible: false
                    color: "red"
                    horizontalAlignment: Text.AlignHCenter
                    width: parent.width
                    font.bold: true
                }
            }
        }
    }
    Rectangle {
        id: busyElement
        visible: false
        anchors.fill: parent
        color: datovkaPalette.text
        opacity: 0.2
        z: 2
        BusyIndicator {
            anchors.centerIn: parent
            running: true
        }
    }
    Connections {
        target: messages
        function onSendMsgDbConvertInfo(info) {
            convertInfo.visible = true
            convertInfo.text = convertInfo.text + "\n" + info
        }
    }
}
