/*
 * Copyright (C) 2014-2025 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

import QtQuick 2.7
import QtQuick.Controls 2.15
import cz.nic.mobileDatovka 1.0
import cz.nic.mobileDatovka.models 1.0
import cz.nic.mobileDatovka.qmlIdentifiers 1.0
import cz.nic.mobileDatovka.qmlInteraction 1.0

Page {

    /* These properties must be set by caller. */
    property AcntId acntId: null

    property string backupTargetPath: "Storage/Documents/Datovka/Backup"

    function showBusyIndicator(visible) {
        busyElement.visible = visible
        headerBar.enabled = !visible
    }

    function canBackup() {
        backUpButton.enabled = (acntId == null ) ? backup_zip.canBackupSelectedAccounts(backupSelectionModel, backupTargetPath, false) : backup_zip.canBackUpAccount(acntId, backupTargetPath, false)
        if (backUpButton.enabled) {
            actionInfoText.color = datovkaPalette.text
        } else {
            actionInfoText.color = "red"
        }
    }

    function createZipArchive() {
        var success = false
        showBusyIndicator(true)
        if (acntId != null) {
            success = backup_zip.backUpAccount(acntId, backupTargetPath)
        } else {
            success = backup_zip.backUpSelectedAccounts(backupSelectionModel, backupTargetPath)
        }
        showBusyIndicator(false)
        if (!success) {
           actionInfoText.visible = false
           okMsgBox.info(qsTr("Backup Error"),
                qsTr("An error occurred when backing up data to target location. Backup has been cancelled."),
                qsTr("Application data haven't been backed up to target location."))
        } else {
            backUpButton.enabled = false
        }
    }

    Component.onCompleted: {
        backup_zip.stopWorker(true)
        backupAccountList.visible = false
        headerBar.title = (acntId == null ) ? qsTr("Back up Data Boxes") : qsTr("Back up Data Box")
        infoText.text = (acntId == null ) ? qsTr("The action allows to back up data of selected data boxes into a ZIP archive.") : qsTr("The action allows to back up data of the data box '%1' into a ZIP archive.").arg(acntId.username)
        if (iOS) {
            infoText.text = infoText.text + " " + qsTr("Archive will be stored into the application sandbox and uploaded into the iCloud.")
            backupTargetPath = qsTr("Application sandbox and iCloud.")
        }
        includeAllAccounts.visible = (acntId == null )
        if (acntId == null ) {
            backupAccountList.visible = true
            backup_zip.fillBackupModel(backupSelectionModel)
        }
        canBackup()
    }

    Component.onDestruction: {
        backup_zip.stopWorker(false)
    }

    BackupRestoreZipData {
        id: backup_zip
    }

    BackupRestoreSelectionModel {
        id: backupSelectionModel
        Component.onCompleted: {
        }
        onDataChanged: {
            includeAllAccounts.checked = (backupSelectionModel.rowCount() > 0) && (backupSelectionModel.numSelected() === backupSelectionModel.rowCount())
        }
    }

    header: PageHeader {
        id: headerBar
        title: qsTr("Back up Data Boxes")
        onBackClicked: {
            pageView.pop()
        }
    }

    Flickable {
        anchors.fill: parent
        contentHeight: flickContent.implicitHeight
        Pane {
            id: flickContent
            anchors.fill: parent
            ControlGroupItem {
                id: contentPart
                spacing: formItemVerticalSpacing
                AccessibleTextInfo {
                    id: infoText
                    horizontalAlignment: Text.AlignHCenter
                }
                AccessibleTextInfo {
                    id: backupPathLabel
                    text: qsTr("Target") + ": " + backupTargetPath
                }
                AccessibleTextInfo {
                    id: sizeInfoText
                    width: parent.width
                }
                AccessibleTextInfo {
                    id: actionInfoText
                    font.bold: true
                }
                AccessibleButton {
                    id: backUpButton
                    enabled: false
                    anchors.horizontalCenter: parent.horizontalCenter
                    icon.source: "qrc:/ui/checkbox-marked-circle.svg"
                    text: qsTr("Create ZIP Archive")
                    accessibleName: qsTr("Proceed with operation.")
                    onClicked: createZipArchive()
                }
                AccessibleSwitch {
                    id: includeAllAccounts
                    visible: false
                    text: qsTr("Select all data boxes")
                    checked: false
                    onClicked: {
                        backupSelectionModel.setAllSelected(includeAllAccounts.checked)
                        canBackup()
                    }
                }
            }
            ScrollableListView {
                id: backupAccountList
                visible: false
                delegateHeight: baseDelegateHeight * 1.4
                width: parent.width
                anchors.top: contentPart.bottom
                height: mainWindow.height - contentPart.height
                model: backupSelectionModel
                delegate: Rectangle {
                    width: parent.width
                    height: backupAccountList.delegateHeight
                    color: (!rSelected) ? "transparent" : datovkaPalette.selected
                    MouseArea {
                        function handleClick() {
                            backupSelectionModel.setSelected(index, !rSelected)
                            canBackup()
                        }
                        anchors.fill: parent
                        Accessible.role: Accessible.Button
                        Accessible.name: (!rSelected) ? qsTr("Select %1.").arg(rAcntName) : qsTr("Deselect %1.").arg(rAcntName)
                        Accessible.onScrollDownAction: backupAccountList.scrollDown()
                        Accessible.onScrollUpAction: backupAccountList.scrollUp()
                        Accessible.onPressAction: {
                            handleClick()
                        }
                        onClicked: {
                            handleClick()
                        }
                    }
                    SeparatorLine {
                        anchors.top: parent.top
                        visible: index === 0
                    }
                    Image {
                        id: accountImg
                        source: (rTestAccount) ? "qrc:/ui/databox-test.png" : "qrc:/ui/databox-regular.png"
                        sourceSize.height: baseDelegateHeight
                        anchors.left: parent.left
                        anchors.leftMargin: defaultMargin
                        anchors.verticalCenter: parent.verticalCenter
                    }
                    Column {
                        anchors.left: accountImg.right
                        anchors.leftMargin: defaultMargin * 2
                        anchors.verticalCenter: parent.verticalCenter
                        anchors.right: parent.right
                        spacing: defaultMargin
                        AccessibleLabel {
                            width: parent.width
                            elide: Text.ElideRight
                            text: rAcntName
                        }
                        AccessibleLabel {
                            font.bold: false
                            color: datovkaPalette.greyText
                            text: rUserName + "     boxID: " + rBoxId
                        }
                    }
                    SeparatorLine {
                        anchors.bottom: parent.bottom
                    }
                }
            }
            Connections {
                target: backup_zip
                function onActionTextSig(actionInfo) {
                    actionInfoText.text = actionInfo
                }
                function onSizeTextSig(sizeInfo) {
                    sizeInfoText.text = sizeInfo
                }
                function onShowMessageBox(title, text, detailText) {
                    okMsgBox.error(title, text, detailText)
                }
            }
        }
    }
    Rectangle {
        id: busyElement
        visible: false
        anchors.fill: parent
        color: datovkaPalette.text
        opacity: 0.2
        z: 2
        BusyIndicator {
            anchors.centerIn: parent
            running: true
        }
    }
}
