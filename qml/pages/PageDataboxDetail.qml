/*
 * Copyright (C) 2014-2025 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

import QtQuick 2.7
import QtQuick.Controls 2.15
import cz.nic.mobileDatovka 1.0
import cz.nic.mobileDatovka.qmlIdentifiers 1.0

Page {

    /* These properties must be set by caller. */
    property var pageView
    property AcntId acntId: null
    property string dbID
    property string dbType

    Component.onCompleted: {
        isds.doIsdsAction("findDatabox", acntId)
    }

    header: PageHeader {
        id: headerBar
        title: qsTr("Data-Box Info")
        onBackClicked: {
             pageView.pop(StackView.Immediate)
        }
    }

    Flickable {
        anchors.fill: parent
        contentHeight: flickContent.implicitHeight
        Pane {
            id: flickContent
            anchors.fill: parent
            background: Rectangle {
                color: datovkaPalette.base
            }
            AccessibleText {
                id: databoxDetailText
                width: parent.width
                textFormat: TextEdit.RichText
                wrapMode: Text.WordWrap
            }
        }
        ScrollIndicator.vertical: ScrollIndicator {}
    }
    AcntId {
        id: actionAcntId
    }
    Connections {
        target: isds
        function onRunFindDataboxSig(userName, testing) {
            actionAcntId.username = userName
            actionAcntId.testing = testing
            databoxDetailText.text = isds.findDatabox(actionAcntId, dbID, dbType)
            if (databoxDetailText.text === "") {
                databoxDetailText.text = qsTr("Information about the data box '%1' aren't available. The data box may have been temporarily disabled. It is likely that you cannot send data message to this data box.").arg(dbID)
            }
        }
    }
}
