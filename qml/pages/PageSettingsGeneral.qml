/*
 * Copyright (C) 2014-2025 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

import QtQuick 2.7
import QtQuick.Controls 2.15
import cz.nic.mobileDatovka 1.0

Page {

    Component.onCompleted: {
        languageComboBox.selectCurrentKey(settings.language())
        fontComboBox.selectCurrentKey(settings.font())
        fontSizeSpinBox.setVal(settings.fontSize())
        debugVerbosityLevelSpinBox.setVal(settings.debugVerbosityLevel())
        logVerbosityLevelSpinBox.setVal(settings.logVerbosityLevel())
        useDarkTheme.checked = settings.darkTheme()
    }

    header: PageHeader {
        title: qsTr("General Settings")
        onBackClicked: {
            pageView.pop()
        }
    }

    Flickable {
        anchors.fill: parent
        contentHeight: flickContent.implicitHeight
        Pane {
            id: flickContent
            anchors.fill: parent
            Column {
                anchors.right: parent.right
                anchors.left: parent.left
                spacing: formItemVerticalSpacing
                ControlGroupItem {
                    visible: !iOS
                    AccessibleLabel {
                        text: qsTr("Theme")
                    }
                    AccessibleSwitch {
                        id: useDarkTheme
                        checked: true
                        text: qsTr("Dark theme")
                        onClicked: settings.setDarkTheme(useDarkTheme.checked)
                    }
                    AccessibleTextInfoSmall {
                        text: qsTr("Note: Theme will be changed after application restart.")
                    }
                }
                SeparatorLine {}
                ControlGroupItem {
                    AccessibleLabel {
                        text: qsTr("Language")
                    }
                    AccessibleComboBox {
                        id: languageComboBox
                        width: parent.width
                        accessibleDescription: qsTr("Select language")
                        model: ListModel {
                            id: langMethodModel
                            /* Don't change keys and labels. */
                            ListElement { label: qsTr("System"); key: "system" }
                            ListElement { label: qsTr("Czech"); key: "cs" }
                            ListElement { label: qsTr("English"); key: "en" }
                            ListElement { label: qsTr("Ukrainian"); key: "uk" }
                        }
                        onCurrentIndexChanged: settings.setLanguage(currentKey())
                    }
                    AccessibleTextInfoSmall {
                        text: qsTr("Note: Language will be changed after application restart.")
                    }
                }
                SeparatorLine {}
                ControlGroupItem {
                    AccessibleLabel {
                        text: qsTr("Font")
                    }
                    AccessibleComboBox {
                        id: fontComboBox
                        width: parent.width
                        accessibleDescription: qsTr("Select font")
                        model: ListModel {
                            /* Don't change key values. */
                            ListElement { label: qsTr("System Default"); key: "system" }
                            ListElement { label: qsTr("Roboto"); key: "roboto" }
                            ListElement { label: qsTr("Source Sans Pro"); key: "source_sans_pro" }
                        }
                        onCurrentIndexChanged: settings.setFont(currentKey())
                    }
                    AccessibleTextInfoSmall {
                        text: qsTr("Note: Font will be changed after application restart.")
                    }
                }
                SeparatorLine {}
                ControlGroupItem {
                    AccessibleLabel {
                        text: qsTr("Font size and application scale")
                    }
                    AccessibleSpinBox {
                        /* Actually holds font pixel size. */
                        id: fontSizeSpinBox
                        property int dflt: 16
                        from: 14
                        to: 20
                        stepSize: 1
                        accessibleDescription: qsTr("Set application font size")
                        /*
                         * Cannot use onValueChanged here because this resets
                         * the font size to minimal value on spin box creation.
                         */
                        onValueModified: settings.setFontSize(fontSizeSpinBox.val())
                    }
                    AccessibleTextInfoSmall {
                        text: qsTr("Note: Font size will be changed after application restart. Default is %1.").arg(fontSizeSpinBox.dflt)
                    }
                }
                SeparatorLine {}
                ControlGroupItem {
                    AccessibleLabel {
                        text: qsTr("Debug verbosity level")
                    }
                    AccessibleSpinBox {
                        id: debugVerbosityLevelSpinBox
                        property int dflt: 1
                        from: 0
                        to: 3
                        stepSize: 1
                        accessibleDescription: qsTr("Set the amount of logged debugging information")
                        onValueModified: settings.setDebugVerbosityLevel(debugVerbosityLevelSpinBox.val())
                    }
                    AccessibleTextInfoSmall {
                        text: qsTr("Debug verbosity controls the amount of debugging information written to the log file.")
                    }
                }
                SeparatorLine {}
                ControlGroupItem {
                    AccessibleLabel {
                        text: qsTr("Log verbosity level")
                    }
                    AccessibleSpinBox {
                        id: logVerbosityLevelSpinBox
                        property int dflt: 1
                        from: 0
                        to: 3
                        stepSize: 1
                        accessibleDescription: qsTr("Set the verbosity of logged entries")
                        onValueModified: settings.setLogVerbosityLevel(logVerbosityLevelSpinBox.val())
                    }
                    AccessibleTextInfoSmall {
                        text: qsTr("Log verbosity controls the level of detail of the logged entries.")
                    }
                }
                SeparatorLine {}
            } // Column layout
        } // Pane
        ScrollIndicator.vertical: ScrollIndicator {}
    } // Flickable
} // Page
