/*
 * Copyright (C) 2014-2025 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

import Qt.labs.platform 1.1
import QtQuick 2.7
import QtQuick.Controls 2.15
import cz.nic.mobileDatovka 1.0
import cz.nic.mobileDatovka.iOsHelper 1.0
import cz.nic.mobileDatovka.qmlIdentifiers 1.0

Page {

    property string acntName
    property AcntId acntId: null

    /* Clear import info and import results */
    function clearInfo() {
        infoText.text = ""
        progressText.text = ""
    }

    Component.onCompleted: {
        clearInfo()
    }

    function importFiles(files) {
        clearInfo()
        infoText.text = isds.importZfoMessages(acntId, files, verifyMessage.checked)
    }

    function openFileDialog() {
        var title = qsTr("Select ZFO Files")
        var filters = "*.zfo"
        if (settings.useQmlFileDialog()) {
            console.log("Use QML File Dialog")
            fileDialogue.openFileDialog(title, filters)
            return
        }
        var fs = []
        if (Qt.platform.os == "ios") {
            if (settings.useIosDocumentPicker()) {
                console.log("Use Native Ios Document Picker")
                iOSHelper.openDocumentPickerControllerForImport(IosImportAction.IMPORT_ZFOS,[])
            } else {
                console.log("Use Qt Core File Dialog")
                fs.push(files.openFileDialog(title, null, filters))
                importFiles(fs)
            }
        } else {
            console.log("Use Qt Core File Dialog")
            fs.push(files.openFileDialog(title, null, filters))
            importFiles(fs)
        }
    }

    FileDialogue {
        id: fileDialogue
        onAccepted: {
            var files = []
            for (var i = 0; i < fileDialogue.files.length; i++) {
                files.push(removeFilePrefix(decodeURIComponent(fileDialogue.files[i])))
            }
            importFiles(files)
        }
        Connections {
            target: iOSHelper
            function onZfoFilesSelectedSig(filePaths) {
                var files = []
                for (var j = 0; j < filePaths.length; ++j) {
                    files.push(removeFilePrefix(decodeURIComponent(filePaths[j])))
                }
                importFiles(files)
            }
        }
    }

    FolderDialog {
        id: fileDialogueDir
        title: "Select Import Directory"
        folder: StandardPaths.standardLocations(StandardPaths.DocumentsLocation)[0]
        onAccepted: {
            var files = [removeFilePrefix(decodeURIComponent(fileDialogueDir.folder))]
            infoText.text = isds.importZfoMessages(acntId, files, verifyMessage.checked)
        }
    }

    header: PageHeader {
        id: headerBar
        title: qsTr("ZFO Message Import")
        onBackClicked: {
            pageView.pop()
        }
    }

    Flickable {
        anchors.fill: parent
        contentHeight: flickContent.implicitHeight
        Pane {
            id: flickContent
            anchors.fill: parent
            Column {
                anchors.right: parent.right
                anchors.left: parent.left
                spacing: formItemVerticalSpacing
                AccessibleTextInfo {
                    horizontalAlignment: Text.AlignHCenter
                    text: qsTr("Here you can import messages from ZFO files into the local database.")
                }
                ControlGroupItem {
                    AccessibleSwitch {
                        id: verifyMessage
                        text: qsTr("Verify messages on the ISDS server")
                        checked: false
                        onClicked: {
                            clearInfo()
                            if (acntId === null) {
                                return
                            }
                            if (verifyMessage.checked && !isds.isLogged(acntId)) {
                                loginButton.visible = true;
                                loginText.visible = loginButton.visible
                            } else {
                                loginButton.visible = false;
                                loginText.visible = loginButton.visible
                            }
                        }
                    }
                    AccessibleTextInfo {
                        text: (verifyMessage.checked) ?
                            qsTr("Imported messages are going to be sent to the ISDS server where they are going to be checked. Messages failing this test won't be imported. We don't recommend using this option when you are using a mobile or slow internet connection.") :
                            qsTr("Imported messages won't be validated on the ISDS server.")
                    }
                }
                AccessibleTextInfo {
                    id: loginText
                    visible: false
                    text: qsTr("User must be logged in to the data box to be able to verify the messages.")
                }
                AccessibleButton {
                    id: loginButton
                    visible: false
                    anchors.horizontalCenter: parent.horizontalCenter
                    text: qsTr("Log in now")
                    onClicked: isds.doIsdsAction("importZfoMessage", acntId)
                }
                AccessibleButton {
                    id: importZfo
                    anchors.horizontalCenter: parent.horizontalCenter
                    icon.source: "qrc:/ui/file-import.svg"
                    text: qsTr("Import ZFO Files")
                    onClicked: {
                        clearInfo()
                        openFileDialog()
                    }
                }
                AccessibleButton {
                    id: importZfoDir
                    visible: !iOS
                    anchors.horizontalCenter: parent.horizontalCenter
                    icon.source: "qrc:/ui/folder.svg"
                    text: qsTr("Import from Directory")
                    onClicked: {
                        clearInfo()
                        fileDialogueDir.open()
                    }
                }
                AccessibleTextInfo {
                    id: infoText
                    horizontalAlignment: Text.AlignHCenter
                }
                AccessibleTextInfo {
                    id: progressText
                    textFormat: TextEdit.RichText
                }
            } // Column layout
        } // Pane
        ScrollIndicator.vertical: ScrollIndicator {}
    } // Flickable
    Connections {
        target: isds
        function onRunImportZfoMessageSig(userName, testing) {
            loginButton.visible = false;
            loginText.text = qsTr("User is logged in to ISDS.") + " " + qsTr("Select ZFO files or a directory.")
            clearInfo()
        }
        function onZfoImportFinishedSig(txt) {
            infoText.text = txt
            accounts.loadModelCounters(accountModel)
        }
        function onZfoImportProgressSig(txt) {
            progressText.text = progressText.text + "<br/>" + txt
        }
    }
}
