/*
 * Copyright (C) 2014-2025 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

import QtQuick 2.7
import QtQuick.Controls 2.15
import cz.nic.mobileDatovka 1.0
import cz.nic.mobileDatovka.models 1.0
import cz.nic.mobileDatovka.qmlIdentifiers 1.0

Page {

    /* These properties must be set by caller. */
    property var pageView
    property string acntName
    property AcntId acntId: null
    property string gsInternId
    property string gsFullName
    property string gsInstName

    Component.onCompleted: {
        gov.loadFormToModel(acntId, gsInternId, govFormModel)
        actionButton.enabled = govFormModel.haveAllMandatory()
        userMandatoryText.visible = govFormModel.containsMandatoryUser()
        fromDataBoxText.visible = govFormModel.containsBoxOwnerData()
    }

    GovFormListModel {
        id: govFormModel

        onDataChanged: {
            actionButton.enabled = govFormModel.haveAllMandatory()
            userMandatoryText.visible = govFormModel.containsMandatoryUser()
            fromDataBoxText.visible = govFormModel.containsBoxOwnerData()
        }

        onValidityNotification: {
            validityNotification.text = message
            actionButton.enabled = govFormModel.haveAllMandatory()
        }
    }

    MessageBox {
        id: yesNoMsgBox
        onAcceptAndClosed: {
            if (govFormModel.haveAllValid()) {
                isds.doIsdsAction("sendGovMessage", acntId)
            }
        }
    }

    header: PageHeader {
        id: headerBar
        title: qsTr("Service:") + " " + gsInternId
        onBackClicked: {
            pageView.pop()
        }
    }

    Pane {
        id: formPane
        anchors.top: parent.top
        anchors.right: parent.right
        anchors.left: parent.left
        Column {
            anchors.left: parent.left
            anchors.right: parent.right
            spacing: defaultMargin
            AccessibleLabelDark {
                text: qsTr("Data Box:") + " " + acntName
            }
            AccessibleText {
                width: parent.width
                wrapMode: Text.Wrap
                text: qsTr("Request:") + " " + gsFullName
            }
            AccessibleLabelDark {
                text: gsInstName
            }
            AccessibleTextInfo {
                id: validityNotification
                color: datovkaPalette.errorText
                visible: validityNotification.text !== ""
            }
            AccessibleLabelDark {
                id: userMandatoryText
                text: "(*) " + qsTr("Required information")
            }
            AccessibleLabelDark {
                id: fromDataBoxText
                text: "(**) " + qsTr("Acquired from data box")
            }
        }
    }
    AccessibleText {
        id: emptyList
        visible: false
        anchors.centerIn: parent
        wrapMode: Text.Wrap
        text: qsTr("No user data needed.")
    }
    GovFormList {
        id: formList
        anchors.top: formPane.bottom
        anchors.topMargin: defaultMargin
        anchors.bottom: parent.bottom
        visible: true
        width: parent.width
        model: govFormModel
        onCountChanged: {
            emptyList.visible = (formList.count === 0)
        }
    }

    AccessibleRoundButton {
        id: actionButton
        z: 1
        anchors.right: parent.right
        anchors.rightMargin: defaultMargin
        anchors.bottom: parent.bottom
        anchors.bottomMargin: defaultMargin
        icon.source: "qrc:/ui/send-msg.svg"
        icon.width: roundButtonSize
        icon.height: roundButtonSize
        accessibleName: qsTr("Send an e-gov request from data box '%1'.").arg(acntName)
        onClicked: yesNoMsgBox.question(qsTr("Send Request"),
                qsTr("Do you want to send the e-gov request?"),
                qsTr("Request:") + " " + gsFullName
                + "\n" + qsTr("Recipient:") + " " + gsInstName)
    }

    AcntId {
        id: actionAcntId
    }
    Connections {
        target: isds
        function onRunSendGovMessageSig(userName, testing) {
            actionButton.enabled = false
            actionAcntId.username = userName
            actionAcntId.testing = testing
            if (gov.sendGovRequest(actionAcntId, gsInternId, govFormModel)) {
                pageView.pop(null, StackView.Immediate)
            } else {
                actionButton.enabled = govFormModel.haveAllMandatory()
            }
        }
    }
} // Item
