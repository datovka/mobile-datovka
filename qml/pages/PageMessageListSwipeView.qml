/*
 * Copyright (C) 2014-2025 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

import QtQuick 2.15
import QtQuick.Controls 2.15
import cz.nic.mobileDatovka 1.0
import cz.nic.mobileDatovka.messages 1.0
import cz.nic.mobileDatovka.models 1.0
import cz.nic.mobileDatovka.qmlIdentifiers 1.0

Page {
    id: msgsListSwipeView

    /* These properties must be set by caller. */
    property var pageViewSv: pageView
    property string acntNameSv
    property AcntId acntIdSv: null
    property bool isMainPageSv: true

    Component.onDestruction: {
        accounts.updateOneAccountCounters(accountModel, acntIdSv)
    }

    Component.onCompleted: {
        if (accountModel.rowCount() === 1) {
            oneAccountId.username = accountModel.userName()
            oneAccountId.testing = accountModel.testing()
            currentAcntId = oneAccountId
            acntIdSv = currentAcntId
            currentAcntName = accountModel.accountName()
            acntNameSv = currentAcntName
        }
    }

    AcntId {
        id: oneAccountId
    }

    MessageListModel {
        id: receivedMsgModel
    }

    MessageListModel {
        id: sentMsgModel
    }

    MessageBox {
        id: yesNoMsgBox
        property string userName: ""
        property bool testing: false
        AcntId {
            id: anctIdBox
        }
        onAcceptAndClosed: {
            anctIdBox.username = yesNoMsgBox.userName
            anctIdBox.testing = yesNoMsgBox.testing
            pageView.push(pageRepairDatabase, {
                "pageView": pageView,
                "acntId": anctIdBox
            }, StackView.Immediate)
        }
    }

    SwipeView {
        id: swipeView
        anchors.fill: parent
        currentIndex: tabBar.currentIndex
        PageMessageList {
            id: receivedMsgList
            pageView: pageViewSv
            acntName: acntNameSv
            acntId: acntIdSv
            msgType: MessageType.TYPE_RECEIVED
            isMainPage: isMainPageSv
            messageModel: receivedMsgModel
        }
        PageMessageList {
            id: sentMsgList
            pageView: pageViewSv
            acntName: acntNameSv
            acntId: acntIdSv
            msgType: MessageType.TYPE_SENT
            isMainPage: isMainPageSv
            messageModel: sentMsgModel
        }
    }

    AcntId {
        id: actionAcntId
    }
    Connections {
        target: isds
        function onRunSyncSingleAccountReceivedSig(userName, testing) {
            actionAcntId.username = userName
            actionAcntId.testing = testing
            isds.syncSingleAccountReceived(actionAcntId)
        }
        function onRunSyncSingleAccountSentSig(userName, testing) {
            actionAcntId.username = userName
            actionAcntId.testing = testing
            isds.syncSingleAccountSent(actionAcntId)
        }
        function onRunSyncOneAccountSig(userName, testing) {
            actionAcntId.username = userName
            actionAcntId.testing = testing
            if (messages.testMsgDbIntegrity(actionAcntId)) {
                isds.syncOneAccount(actionAcntId)
            } else {
                yesNoMsgBox.userName = userName
                yesNoMsgBox.testing = testing
                yesNoMsgBox.question(qsTr("Database problem: %1").arg(userName),
                    qsTr("The message database for username '%1' appears to be corrupt. Synchronisation won't most likely be able to write new data and therefore is going to be aborted.").arg(userName),
                    qsTr("Do you want to restore the damaged database from a local back-up copy?"))
            }
        }
        function onSentMessageFinished(userName, testing, success, infoTxt) {
            if (success) {
                messages.fillMessageList(sentMsgModel, acntIdSv, MessageType.TYPE_SENT)
            }
        }
    }

    AccessibleRoundButton {
        z: 1
        anchors.right: parent.right
        anchors.rightMargin: defaultMargin
        anchors.bottom: parent.bottom
        anchors.bottomMargin: defaultMargin
        icon.source: "qrc:/ui/pencil-box-outline.svg"
        icon.width: roundButtonSize
        icon.height: roundButtonSize
        accessibleName: qsTr("Create and send a new message from data box '%1'.").arg(acntNameSv)
        onClicked: {
            pageView.push(pageSendMessage, {
                "pageView": pageView,
                "acntName": acntNameSv,
                "acntId": acntIdSv,
                "action": "new"
            }, StackView.Immediate)
        }
    }

    footer: TabBar {
        id: tabBar
        currentIndex: swipeView.currentIndex
        TabButton {
            Accessible.role: Accessible.Button
            Accessible.name: qsTr("View received messages of data box '%1'.").arg(acntNameSv)
            text: qsTr("Received") + "  [" + receivedMsgList.msgList.count + "]"
            icon.source: "qrc:/ui/received-msgs.svg"
            display: AbstractButton.TextUnderIcon
        }
        TabButton {
            Accessible.role: Accessible.Button
            Accessible.name: qsTr("View sent messages of data box '%1'.").arg(acntNameSv)
            text: qsTr("Sent") + "  [" + sentMsgList.msgList.count + "]"
            icon.source: "qrc:/ui/sent-msgs.svg"
            display: AbstractButton.TextUnderIcon
        }
    }
}
