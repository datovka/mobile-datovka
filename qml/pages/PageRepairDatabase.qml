/*
 * Copyright (C) 2014-2025 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

import QtQuick 2.7
import QtQuick.Controls 2.15
import cz.nic.mobileDatovka 1.0
import cz.nic.mobileDatovka.qmlIdentifiers 1.0

Page {

    /* These properties must be set by caller. */
    property var pageView
    property AcntId acntId: null
    property string restoreFilePath: ""

    ListModel {
        id: backupModel
    }

    MessageBox {
        id: msgBox
        onClosed: pageView.pop(StackView.Immediate)
    }

    MessageBox {
        id: yesNoMsgBoxDelDb
        onAcceptAndClosed: messages.removeAndDeleteMsgDb(acntId)
    }

    MessageBox {
        id: yesNoMsgBoxRestoreDb
        onAcceptAndClosed: {
            if (messages.restoreMsgDbFromLocalBackup(acntId, restoreFilePath)) {
                accounts.updateOneAccountCounters(accountModel, acntId)
                resultText.visible = true
                resultText.text = qsTr("The message database has successfully been restored.")
                msgBox.info(qsTr("Restoration Finished"),
                    qsTr("The message database has successfully been restored."),
                    qsTr("Synchronise the data box to download missing data."))
            } else {
                yesNoMsgBoxDelDb.question(qsTr("Restoration Error"),
                    qsTr("Restoration of the message database failed. The log file contains the description of occurred errors."),
                    qsTr("Do you want to delete the corrupted message database and create a new empty one? Downloaded message data will be deleted."))
            }
        }
    }

    header: PageHeader {
        id: headerBar
        title: qsTr("Repair Database")
        onBackClicked: {
            pageView.pop(StackView.Immediate)
        }
    }

    Flickable {
        anchors.fill: parent
        contentHeight: flickContent.implicitHeight
        Pane {
            id: flickContent
            anchors.fill: parent
            Column {
                anchors.right: parent.right
                anchors.left: parent.left
                spacing: formItemVerticalSpacing
                AccessibleTextInfo {
                    horizontalAlignment: Text.AlignHCenter
                    text: qsTr("The action checks the message database integrity and restores damaged files from a local back-up copy.")
                }
                AccessibleButton {
                    text: qsTr("Run Integrity Test")
                    icon.source: "qrc:/ui/database.svg"
                    anchors.horizontalCenter: parent.horizontalCenter
                    onClicked: {
                        backupModel.clear()
                        var ret = messages.testMsgDbIntegrity(acntId)
                        resultText.visible = true
                        if (!ret) {
                            var bFiles = messages.getLocalBackupFiles(acntId)
                            if (bFiles.length === 0) {
                                resultText.text = qsTr("The message database file is damaged.")
                                    + " " + qsTr("But there isn't any local back-up file which can be used to repair the damaged one.")
                                yesNoMsgBoxDelDb.question(qsTr("Database Problem"),
                                    qsTr("The message database file is damaged but there isn't any local back-up file which can be used to repair the damaged one."),
                                    qsTr("Do you want to delete the corrupted message database and create a new empty one? Downloaded message data will be deleted."))
                                return
                            }
                            resultText.text = qsTr("The message database file is damaged.")
                                + " " + qsTr("Select a local back-up database file witch will be used to repair the damaged one.")
                            for (var i = 0; i<bFiles.length; i++ ) {
                                var fDate = messages.getFileDateFromLocalBackup(bFiles[i])
                                backupModel.append({"filePath": bFiles[i], "fileName": getFileNameFromPath(bFiles[i]), "date": fDate})
                             }
                        } else {
                            resultText.text = qsTr("The message database file is valid and OK.")
                        }
                    }
                }
                AccessibleTextInfo {
                    id: resultText
                    visible: false
                    horizontalAlignment: Text.AlignHCenter
                    font.bold: true
                }
                ScrollableListView {
                    id: backupList
                    delegateHeight: listViewDelegateHeight
                    width: parent.width
                    height: 300
                    model: backupModel
                    delegate: Rectangle {
                        color: datovkaPalette.base
                        width: parent.width
                        height: backupList.delegateHeight
                        Rectangle {
                            visible: (0 === index)
                            anchors.top: parent.top
                            anchors.right: parent.right
                            height: 1
                            width: parent.width
                            color: datovkaPalette.greyText
                        }
                        AccessibleText {
                            id: backupFile
                            anchors.verticalCenter: parent.verticalCenter
                            anchors.left: parent.left
                            anchors.leftMargin: defaultMargin
                            width: parent.width - defaultMargin
                            elide: Text.ElideRight
                            text: date + ":  " + fileName
                        }
                        MouseArea {
                            function handleClick() {
                                restoreFilePath = backupModel.get(index).filePath
                                yesNoMsgBoxRestoreDb.question(qsTr("Database Restoration"),
                                    qsTr("The operation restores the currently used message database from the back-up file '%1' created at %2.").arg(getFileNameFromPath(restoreFilePath)).arg(backupModel.get(index).date),
                                    qsTr("Restore the message database from the selected the back-up file?"))
                            }
                            anchors.fill: parent
                            Accessible.role: Accessible.Button
                            Accessible.onScrollDownAction: backupList.scrollDown()
                            Accessible.onScrollUpAction: backupList.scrollUp()
                            Accessible.onPressAction: {
                                handleClick()
                            }
                            onClicked: {
                                handleClick()
                            }
                        }
                        Rectangle {
                            anchors.bottom: parent.bottom
                            anchors.right: parent.right
                            height: 1
                            width: parent.width
                            color: (backupList.count-1 === index) ? datovkaPalette.greyText : datovkaPalette.mid
                        }
                    }
                }
            }
        }
    }
}
