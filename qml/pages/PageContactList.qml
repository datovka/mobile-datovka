/*
 * Copyright (C) 2014-2025 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

import QtQuick 2.7
import QtQuick.Controls 2.15
import cz.nic.mobileDatovka 1.0
import cz.nic.mobileDatovka.models 1.0
import cz.nic.mobileDatovka.qmlIdentifiers 1.0

/*
 * Roles are defined in DataboxListModel::roleNames() and are accessed directly
 * via their names.
 */

Page {

    /* These properties must be set by caller. */
    property var pageView
    property AcntId acntId: null
    property var recipBoxModel: null

    Component.onCompleted: {
        var currentBoxId = accounts.dbId(acntId)
        messages.fillContactList(foundBoxModel, acntId, currentBoxId)
        if (foundBoxModel.rowCount() === 0) {
            emptyList.visible = true
        }
        if (recipBoxModel != null) {
            foundBoxModel.selectEntries(recipBoxModel.boxIds(), true)
        }
        proxyDataboxModel.setSourceModel(foundBoxModel)
    }

    DataboxListModel {
        id: foundBoxModel
        Component.onCompleted: {
        }
    }
    ListSortFilterProxyModel {
        id: proxyDataboxModel
        Component.onCompleted: {
            setFilterRoles([DataboxListModel.ROLE_DB_NAME,
                DataboxListModel.ROLE_DB_ADDRESS, DataboxListModel.ROLE_DB_ID])
        }
    }

    header: PageHeader {
        id: headerBar
        title: qsTr("Contacts")
        onBackClicked: {
            pageView.pop(StackView.Immediate)
        }
        AccessibleToolButton {
            id: searchButton
            anchors.right: parent.right
            icon.source: "qrc:/ui/magnify.svg"
            accessibleName: qsTr("Filter data boxes.")
            onClicked: {
                filterBar.open()
                filterBar.filterField.forceActiveFocus()
                Qt.inputMethod.show()
            }
        }
    }

    FilterBar {
        id: filterBar
        y: header.height
        onTextChanged: text => proxyDataboxModel.setFilterRegExpStr(text)
        onClearClicked: close()
    }

    DataboxList {
        id: databoxList
        anchors.fill: parent
        visible: true
        model: proxyDataboxModel
        canDetailBoxes: recipBoxModel == null
        canSelectBoxes: recipBoxModel != null
        canDeselectBoxes: recipBoxModel != null
        localContactFormat: true
        onBoxSelect: {
            if (recipBoxModel != null) {
                var boxEntry = foundBoxModel.entry(boxId)
                foundBoxModel.selectEntry(boxEntry.dbID, true)
                isds.addRecipient(acntId, boxEntry.dbID, boxEntry.dbName, boxEntry.dbAddress, false, recipBoxModel)
                appendSelectedDb.visible = recipBoxModel.rowCount() > 0
            }
        }
        onBoxDeselect: {
            if (recipBoxModel != null) {
                foundBoxModel.selectEntry(boxId, false)
                recipBoxModel.removeEntry(boxId)
                appendSelectedDb.visible = recipBoxModel.rowCount() > 0
            }
        }
    } // DataboxList
    AccessibleTextInfo {
        id: emptyList
        visible: false
        anchors.centerIn: parent
        anchors.margins: defaultMargin
        horizontalAlignment: Text.AlignHCenter
        text: qsTr("No data box found in contacts.")
    }

    AccessibleRoundButton {
        id: appendSelectedDb
        z: 1
        visible: false
        anchors.right: parent.right
        anchors.rightMargin: defaultMargin
        anchors.bottom: parent.bottom
        anchors.bottomMargin: defaultMargin
        text: qsTr("Append selected")
        icon.source: "qrc:/ui/account-plus.svg"
        accessibleName: qsTr("Append selected data boxes into recipient list.")
        onClicked: {
            pageView.pop(StackView.Immediate)
        }
    }
} // Item
