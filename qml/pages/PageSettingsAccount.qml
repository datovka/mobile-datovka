/*
 * Copyright (C) 2014-2025 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

import QtQuick 2.7
import QtQuick.Controls 2.15
import cz.nic.mobileDatovka 1.0
import cz.nic.mobileDatovka.account 1.0
import cz.nic.mobileDatovka.iOsHelper 1.0
import cz.nic.mobileDatovka.qmlIdentifiers 1.0

Page {
    id: pageSettingsAccount

    property string acntName
    property AcntId acntId: null
    property bool isNewAccount: true

    property int sLoginMethod: AcntData.LIM_UNAME_PWD
    property string oldUserName
    property string  certFile: ""
    property bool closePage: false

    property bool acntRemembersPwd: false /* Set only once when page content loaded. */

    /* Set some elements on the page based on selected login method */
    function setPageElements(loginMethod) {
        sLoginMethod = loginMethod
        // set default properties
        passwordItem.visible = true
        mepCodeItem.visible = !passwordItem.visible
        rememberPassword.visible = passwordItem.visible
        certificateSection.visible = false
        if (loginMethod === AcntData.LIM_UNAME_PWD_CRT) {
            certificateSection.visible = true
            certPathLabelId.visible = (certPathLabelId.text !== "")
        } else if (loginMethod === AcntData.LIM_UNAME_MEP) {
            mepCodeItem.visible = true
            passwordItem.visible = !mepCodeItem.visible
            rememberPassword.visible = !mepCodeItem.visible
        }
    }

    function setPageElementsInvisible(visible) {
        headerBar.enabled = !visible
        flickContent.enabled = !visible
        createAccountSection.visible = !visible
        busyElementSection.visible = visible
        busyIndicator.running = visible
    }

    function addDatabox() {
        if (Qt.inputMethod.visible) {
            Qt.inputMethod.commit()
            Qt.inputMethod.hide()
        }
        if (isNewAccount) {
            // Create a new account context data and add to model.
            var errText = accounts.createAccount(sLoginMethod,
                accountNameItem.textLine.displayText.toString(),
                userNameItem.textLine.displayText.toString(),
                passwordItem.pwdTextLine.text.toString(),
                mepCodeItem.textLine.displayText.toString(),
                testAccount.sw.checked, rememberPassword.checked,
                useLS.sw.checked, useSyncWithAll.sw.checked,
                certFile)
            if (errText === "") {
                // Login to new account.
                actionAcntId.username = userNameItem.textLine.displayText.toString()
                actionAcntId.testing = testAccount.sw.checked
                setPageElementsInvisible(true)
                isds.doIsdsAction("addNewAccount", actionAcntId)
            } else {
                var infoText = (accountNameItem.textLine.displayText.toString() !== "") ? qsTr("Data box '%1' couldn't be added.").arg(accountNameItem.textLine.displayText.toString()) : qsTr("Data box couldn't be added.")
                okMsgBox.error(qsTr("Add Data Box Error"), infoText, errText);
            }
        } else {
            // Change account user name.
            if (userNameItem.textLine.displayText.toString() !== oldUserName) {
                yesNoMsgBox.question(qsTr("Change Username: %1").arg(oldUserName),
                    qsTr("Do you want to change the username from '%1' to '%2' for the data box '%3'?").arg(oldUserName).arg(userNameItem.textLine.displayText).arg(acntName),
                    qsTr("Note: It will also change all related local database names and data-box information."))
            } else {
                // Update account context data.
                accounts.updateAccount(accountModel, sLoginMethod,
                  accountNameItem.textLine.displayText.toString(),
                  userNameItem.textLine.displayText.toString(),
                  passwordItem.pwdTextLine.text.toString(),
                  mepCodeItem.textLine.displayText.toString(),
                  testAccount.sw.checked, rememberPassword.checked,
                  useLS.sw.checked, useSyncWithAll.sw.checked,
                  certFile)
                /* INI settings are saved automatically. */
                pageView.pop()
            }
        }
    }

    function openCert(certPath) {
        if (certPath !== "") {
            certFile = removeFilePrefix(decodeURIComponent(certPath))
            certPathLabelId.text = getFileNameFromPath(certFile)
            certPathLabelId.visible = true
        }
    }

    function openFileDialog() {
        var title = qsTr("Choose a certificate")
        var filters = "*.pem"
        if (settings.useQmlFileDialog()) {
            console.log("Use QML File Dialog")
            fileDialogue.openFileDialog(title, filters)
            return
        }
        if (Qt.platform.os == "ios") {
            if (settings.useIosDocumentPicker()) {
                console.log("Use Native Ios Document Picker")
                iOSHelper.openDocumentPickerControllerForImport(IosImportAction.IMPORT_CERT,[])
            } else {
                console.log("Use Qt Core File Dialog")
                openCert(files.openFileDialog(title, null, filters))
            }
        } else {
            console.log("Use Qt Core File Dialog")
            openCert(files.openFileDialog(title, null, filters))
        }
    }

    FileDialogue {
        id: fileDialogue
        onAccepted: {
            if (fileDialogue.files.length > 0) {
                openCert(fileDialogue.files[0])
            }
        }
        Connections {
            target: iOSHelper
            function onCertFilesSelectedSig(filePaths) {
                if (filePaths.length > 0) {
                    openCert(filePaths[0])
                }
            }
        }
    }

    AcntId {
        id: actionAcntId
    }

    Component.onCompleted: {
        if (!isNewAccount) {
            accounts.getAccountData(acntId)
        }
    }

    Component.onDestruction: {
        isds.loginMepCancel()
    }

    MessageBox {
        id: okMsgBox
        onClosed: {
            if (closePage) {
                pageView.pop()
            }
        }
    }

    MessageBox {
        id: yesNoMsgBox
        onAcceptAndClosed: {
            // Create temporary account with a new user name.
            var errText = accounts.prepareChangeUserName(sLoginMethod,
                accountNameItem.textLine.displayText.toString(),
                userNameItem.textLine.displayText.toString(),
                passwordItem.pwdTextLine.text.toString(),
                mepCodeItem.textLine.displayText.toString(),
                testAccount.checked, rememberPassword.checked,
                useLS.checked, useSyncWithAll.checked,
                certFile)
            if (errText === "") {
                // Login to isds with the new user name.
                actionAcntId.username = userNameItem.textLine.displayText.toString()
                actionAcntId.testing = testAccount.checked
                isds.doIsdsAction("changeUserName", actionAcntId)
             } else {
                okMsgBox.error(qsTr("Connection to Data Box: %1").arg(acntId.username),
                    qsTr("Data box '%1' could not be added.").arg(accountName),
                    errText);
             }
        }
    }

    header: PageHeader {
        id: headerBar
        title: qsTr("Add Data Box")
        onBackClicked: {
            if (isds.loginMepRunning()) {
                isds.loginMepCancel()
                closePage = true
            } else {
                pageView.pop()
            }
        }
    } // PageHeader

    Flickable {
        anchors.fill: parent
        contentHeight: flickContent.implicitHeight
        Pane {
            id: flickContent
            anchors.fill: parent
            Column {
                anchors.right: parent.right
                anchors.left: parent.left
                spacing: formItemVerticalSpacing
                ControlGroupItem {
                    AccessibleTextHelp {
                        labelText: qsTr("Login Method")
                        titleText: labelText
                        infoText: qsTr("Select the login method which you use to access the data box in the ISDS.")
                        detailText: qsTr("Note: NIA login methods such as bank ID, mojeID or eCitizen aren't supported because the ISDS system doesn't provide such functionality for third-party applications.")
                    }
                    AccessibleComboBox {
                        id: loginMethodComboBox
                        width: parent.width
                        accessibleDescription: qsTr("Select login method")
                        model: ListModel {
                            id: loginMethodModel
                            /* Key values must be equivalent to constants defined in C++ code! */
                            ListElement { label: qsTr("Username + Password"); key: AcntData.LIM_UNAME_PWD }
                            ListElement { label: qsTr("Certificate + Password"); key: AcntData.LIM_UNAME_PWD_CRT }
                            ListElement { label: qsTr("Password + Security code"); key: AcntData.LIM_UNAME_PWD_HOTP }
                            ListElement { label: qsTr("Password + Security SMS"); key: AcntData.LIM_UNAME_PWD_TOTP }
                            ListElement { label: qsTr("Mobile key"); key: AcntData.LIM_UNAME_MEP }
                        }
                        onCurrentIndexChanged: {
                            setPageElements(currentKey())
                        }
                    }
                }
                ControlGroupItem {
                    id: certificateSection
                    visible: false
                    AccessibleTextHelp {
                        labelText: qsTr("Certificate")
                        titleText: labelText
                        infoText: qsTr("The certificate file is needed for authentication purposes. The supplied file must contain a certificate and its corresponding private key.")
                        detailText: qsTr("Only PEM and PFX file formats are accepted.")
                    }
                    AccessibleText {
                        id: certPathLabelId
                        visible: false
                        width: parent.width
                        wrapMode: Text.WrapAnywhere
                    }
                    AccessibleButton {
                        text: qsTr("Choose File")
                        onClicked: openFileDialog()
                    }
                }
                TextLineItem {
                    id: accountNameItem
                    textLineTitle: qsTr("Data Box Title")
                    placeholderText: qsTr("Enter custom data-box name.")
                    titleText: textLineTitle
                    infoText: qsTr("The data-box title is a user-specified name used for the identification of the data box in the application (e.g. 'My Personal Data Box', 'Firm Box', etc.). The chosen name serves only for your convenience.")
                    detailText: qsTr("The entry must be filled in.")
                    isPassword: false
                }
                ControlGroupItem {
                    TextLineItem {
                        id: userNameItem
                        textLineTitle: qsTr("Username")
                        placeholderText: qsTr("Enter the login name.")
                        inputMethodHints: Qt.ImhLowercaseOnly | Qt.ImhPreferLowercase | Qt.ImhNoPredictiveText
                        titleText: textLineTitle
                        infoText: qsTr("The username must consist of at least 6 characters without spaces (only combinations of lower-case letters and digits are allowed). The entry must be filled in.")
                        detailText: qsTr("Note: The username is not a data-box ID.")
                        isPassword: false
                        textLine.onTextChanged: {
                            if (!isLowerCaseWithoutNonPrintableChars(userNameItem.textLine.text)) {
                                userNameItem.textLine.color = datovkaPalette.errorText
                                errorTextUserName.visible = true
                            } else {
                                errorTextUserName.visible = false
                                userNameItem.textLine.color = datovkaPalette.text
                            }
                        }
                    }
                    AccessibleTextInfo {
                        id: errorTextUserName
                        visible: false
                        horizontalAlignment: Text.AlignHCenter
                        color: datovkaPalette.errorText
                        text: qsTr("The username should contain only combinations of lower-case letters and digits.")
                    }
                }
                ControlGroupItem {
                    TimedPasswordLine {
                        function askPassword() {
                           pinInputDlg.openInputDialog("showPinRequest", 0, acntId,
                               qsTr("PIN Required"), qsTr("Enter PIN code in order to show the password."),
                               qsTr("Enter PIN"), true)
                           return false
                        }
                        id: passwordItem
                        anchors.left: parent.left
                        anchors.right: parent.right
                        textLineTitle: qsTr("Password")
                        titleText: textLineTitle
                        infoText: qsTr("The password must be valid and non-expired. To check whether you've entered the password correctly you may use the icon in the field on the right. The entry must be filled in.")
                        detailText: qsTr("Note: You must first change the password using the ISDS web portal if it is your very first attempt to log into the data box.")
                        placeholderText: qsTr("Enter the password.")
                        /* Able to show password: when creating new account, when not remembering password or when PIN is configured. */
                        pwdEyeIconVisible: isNewAccount || (!acntRemembersPwd) || settings.pinConfigured()
                        checkBeforeShowing: (isNewAccount || (!acntRemembersPwd)) ? null : askPassword
                        hideAfterMs: isNewAccount ? 0 : 5000
                    }
                    AccessibleTextInfo {
                        id: errorTextPwd
                        visible: false
                        horizontalAlignment: Text.AlignHCenter
                        color: datovkaPalette.errorText
                        text: qsTr("Entered PIN is not valid.")
                    }
                }
                TextLineItem {
                    id: mepCodeItem
                    visible: false
                    textLineTitle: qsTr("Communication code")
                    placeholderText: qsTr("Enter the communication code.")
                    isPassword: false
                    titleText: textLineTitle
                    infoText: qsTr("The communication code is a string which can be generated in the ISDS web portal. The entry must be filled in.")
                    detailText: qsTr("You have to have the Mobile Key application installed. The Mobile Key application needs to be paired with the corresponding data-box on the ISDS web portal.")
                }
                ControlGroupItem {
                    AccessibleSwitch {
                        id: rememberPassword
                        text: qsTr("Remember password")
                        checked: true
                    }
                    AccessibleSwitchInfo {
                        id: testAccount
                        switchText: qsTr("Test data box")
                        titleText: switchText
                        infoText: qsTr("Switch on if the data box is in the testing environment.")
                        detailText: qsTr("Test data boxes are used to access the ISDS testing environment (www.czebox.cz) while the regular data boxes are used to access the production ISDS environment (www.mojedatovaschranka.cz).")
                    }
                    AccessibleSwitchInfo {
                        id: useLS
                        checked: true
                        switchText: qsTr("Use local storage")
                        titleText: switchText
                        infoText: (useLS.sw.checked) ?
                            qsTr("Messages and attachments will be locally stored. No active internet connection is needed to access locally stored data.") :
                            qsTr("Messages and attachments will be stored only temporarily in memory. These data will be lost on application exit.")
                    }
                    AccessibleSwitchInfo {
                        id: useSyncWithAll
                        checked: true
                        switchText: qsTr("Synchronise with all")
                        titleText: switchText
                        infoText: useSyncWithAll.sw.checked ?
                            qsTr("The data box will be included into the synchronisation process of all data boxes.") :
                            qsTr("The data box won't be included into the synchronisation process of all data boxes.")
                    }
                }
                AccessibleButton {
                    id: createAccountSection
                    icon.source: (isNewAccount) ? "qrc:/ui/databox-add.svg" : "qrc:/ui/checkbox-marked-circle.svg"
                    text: (isNewAccount) ? qsTr("Login && Add Data Box") : qsTr("Save Changes")
                    anchors.horizontalCenter: parent.horizontalCenter
                    onClicked: addDatabox()
                }
                ControlGroupItem {
                    id: busyElementSection
                    visible: false
                    BusyIndicator {
                        id: busyIndicator
                        anchors.horizontalCenter: parent.horizontalCenter
                        running: false
                    }
                    AccessibleTextInfo {
                        horizontalAlignment: Text.AlignHCenter
                        text: qsTr("Logging into the data box ...")
                    }
                }

                Connections {
                    target: isds
                    function onLoginFailed(userName, testing) {
                        pageSettingsAccount.setPageElementsInvisible(false)
                    }
                    function onDownloadAccountInfoFinishedSig(userName, testing, success, errTxt) {
                        /* INI settings are saved automatically. */
                        if (pageSettingsAccount.isNewAccount) {
                            pageSettingsAccount.setPageElementsInvisible(false)
                            onAddNewAccount(userName, testing, accountNameItem.textLine.displayText.toString())
                        }
                    }
                    function onRunGetAccountInfoSig(userName, testing) {
                         actionAcntId.username = userName
                         actionAcntId.testing = testing
                         /* INI settings are saved automatically. */
                         isds.getAccountInfo(actionAcntId)
                    }
                    function onRunChangeUserNameSig(userName, testing) {
                        // Login to isds with new user name has been succeeded.
                         actionAcntId.username = userName
                         actionAcntId.testing = testing
                        // Get data box ID of new user name.
                        var dbId = isds.getAccountDbId(actionAcntId)
                        // Change username and rename databases.
                        var errText = accounts.changeAccountUserName(
                            userNameItem.textLine.text.toString(),
                            oldUserName, useLS.sw.checked, dbId)
                        if (errText === "") {
                            pageSettingsAccount.closePage = true
                            /* INI settings are saved automatically. */
                            isds.getAccountInfo(actionAcntId)
                            okMsgBox.info(qsTr("Change username: %1").arg(oldUserName),
                                qsTr("A new username '%1' for the data box '%2' has been set.").arg(userNameItem.textLine.text.toString()).arg(accountNameItem.textLine.displayText.toString()),
                                qsTr("The data box will use the new settings."))
                        } else {
                            pageSettingsAccount.closePage = false
                            okMsgBox.error(qsTr("Username problem: %1").arg(oldUserName),
                                qsTr("The new username '%1' for data box '%2' has not been set").arg(userNameItem.textLine.text.toString()).arg(accountNameItem.textLine.displayText.toString()),
                                errText + " " + qsTr("Data box will use the original settings."))
                        }
                    }
                }

                Connections {
                    // Connection is activated when settings of exists account is shown.
                    target: accounts
                    function onSendAccountData(acntName, userName, loginMethod, password,
                        mepToken, isTestAccount, rememberPwd, storeToDisk, syncWithAll, certPath) {
                        accountNameItem.textLine.text = acntName
                        userNameItem.textLine.text = userName
                        passwordItem.pwdTextLine.text = password
                        mepCodeItem.textLine.text = mepToken
                        rememberPassword.checked = rememberPwd
                        testAccount.checked = isTestAccount
                        useLS.checked = storeToDisk
                        useSyncWithAll.checked = syncWithAll
                        certFile = certPath
                        certPathLabelId.text = getFileNameFromPath(removeFilePrefix(decodeURIComponent(certFile)))
                        loginMethodComboBox.selectCurrentKey(loginMethod)
                        oldUserName = userName
                        testAccount.enabled = false
                        headerBar.title = qsTr("Data Box Settings")
                        setPageElements(loginMethod)
                        acntRemembersPwd = rememberPwd
                    }
                } // Connection
            } // Column
        } // Pane
        ScrollIndicator.vertical: ScrollIndicator {}
    } // Flickable

    InputDialogue {
        id: pinInputDlg
        onAcceptAndClosed: function checkPinInput() {
            /* Check entered pin and if valid, then show password. */
            if (settings.pinValid(pinInputDlg.inputTextField.text.toString())) {
                passwordItem.show()
                errorTextPwd.visible = false
            } else {
                errorTextPwd.visible = true
            }
        }
        onRejectAndClosed: function doNothing() {
            errorTextPwd.visible = false
        }
    }
} // Page
