/*
 * Copyright (C) 2014-2025 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

import QtQuick 2.7
import QtQuick.Controls 2.15
import cz.nic.mobileDatovka 1.0
import cz.nic.mobileDatovka.iOsHelper 1.0
import cz.nic.mobileDatovka.models 1.0
import cz.nic.mobileDatovka.qmlIdentifiers 1.0
import cz.nic.mobileDatovka.qmlInteraction 1.0

Page {

    property int backupType: BackupRestoreZipData.BRT_UNKNOWN
    property bool quitApp: false

    Component.onCompleted: {
        backup_zip.stopWorker(true)
    }

    Component.onDestruction: {
        backup_zip.stopWorker(false)
    }

    BackupRestoreZipData {
        id: backup_zip
    }

    BackupRestoreSelectionModel {
        id: restoreSelectionModel
        Component.onCompleted: {
        }
        onDataChanged: {
            includeAllAccounts.checked = (restoreSelectionModel.rowCount() > 0) && (restoreSelectionModel.numSelected() == restoreSelectionModel.rowCount())
        }
    }

    function showBusyIndicator(visible) {
        busyElement.visible = visible
        headerBar.enabled = !visible
        selectJsonButton.enabled = !visible
    }

    function canRestore() {
        restoreButton.visible = backup_zip.canRestoreSelectedAccounts(restoreSelectionModel, true)
    }

    function parseZipFile(zipFilePath, zipFileName) {
        actionInfoText.text = ""
        sizeInfoText.text = ""
        restoreAccountList.visible = false
        includeAllAccounts.visible = false
        restoreButton.visible = false
        restoreSelectionModel.clearAll()
        backupType = backup_zip.loadBackupJsonFromZip(zipFilePath, zipFileName)
        if (backupType === BackupRestoreZipData.BRT_BACKUP) {
            backup_zip.fillRestoreModel(restoreSelectionModel)
            restoreAccountList.visible = true
            includeAllAccounts.visible = true
        }
    }

    function openZip(zipPath) {
        if (zipPath !== "") {
            var fileName = files.getRealFileNameFromPath(zipPath)
            parseZipFile(zipPath, fileName)
        }
    }

    function openFileDialog() {
        var title = qsTr("Select ZIP")
        var filters = "*.zip"
        if (settings.useQmlFileDialog()) {
            console.log("Use QML File Dialog")
            fileDialogue.openFileDialog(title, filters)
            return
        }
        if (Qt.platform.os == "ios") {
            if (settings.useIosDocumentPicker()) {
                console.log("Use Native Ios Document Picker")
                iOSHelper.openDocumentPickerControllerForImport(IosImportAction.IMPORT_JSON,[])
            } else {
                console.log("Use Qt Core File Dialog")
                openZip(files.openFileDialog(title, null, filters))
            }
        } else {
            console.log("Use Qt Core File Dialog")
            openZip(files.openFileDialog(title, null, filters))
        }
    }

    FileDialogue {
        id: fileDialogue
        onAccepted: {
            if (fileDialogue.files.length > 0) {
                openZip(removeFilePrefix(decodeURIComponent(fileDialogue.files[0])))
            }
        }
        Connections {
            target: iOSHelper
            function onJsonFilesSelectedSig(filePaths) {
                if (filePaths.length > 0) {
                    openZip(filePaths[0])
                }
            }
        }
    }

    MessageBox {
        id: okMsgBox
        onClosed: {
            if (quitApp) {
                Qt.quit()
            }
        }
    }

    MessageBox {
        id: yesNoMsgBox
        onAcceptAndClosed: {
            var success = false
            showBusyIndicator(true)
            if (backupType === BackupRestoreZipData.BRT_TRANSFER) {
                success = backup_zip.restoreDataFromTransferZiP()
            } else if (backupType === BackupRestoreZipData.BRT_BACKUP) {
               success = backup_zip.restoreSelectedAccountsFromZip(restoreSelectionModel, true)
            }
            showBusyIndicator(false)
            if (!success) {
                okMsgBox.error(qsTr("Restoration Error"),
                    qsTr("An error during data restoration has occurred."),
                    qsTr("Application data restoration has been cancelled."))
                quitApp = false
            } else {
                okMsgBox.info(qsTr("Restoration Finished"),
                    qsTr("All selected data box data have successfully been restored."),
                    qsTr("The application will restart in order to load the imported files."))
                quitApp = true
            }
        }
    }

    header: PageHeader {
        id: headerBar
        title: qsTr("Restore Data")
        onBackClicked: {
            pageView.pop()
        }
    }

    Flickable {
        anchors.fill: parent
        contentHeight: flickContent.implicitHeight
        Pane {
            id: flickContent
            anchors.fill: parent
            ControlGroupItem {
                id: contentPart
                spacing: formItemVerticalSpacing
                AccessibleTextInfo {
                    horizontalAlignment: Text.AlignHCenter
                    text: qsTr("The action allows to restore data from a back-up or transfer ZIP archive.")
                }
                AccessibleButton {
                    id: selectJsonButton
                    text: qsTr("Choose ZIP Archive")
                    icon.source: "qrc:/ui/folder.svg"
                    anchors.horizontalCenter: parent.horizontalCenter
                    onClicked: openFileDialog()
                }
                AccessibleTextInfo {
                    id: backupDateText
                }
                AccessibleTextInfo {
                    id: sizeInfoText
                    width: parent.width
                }
                AccessibleTextInfo {
                    id: actionInfoText
                    font.bold: true
                }
                AccessibleButton {
                    id: restoreButton
                    visible: false
                    anchors.horizontalCenter: parent.horizontalCenter
                    text: qsTr("Restore Data")
                    icon.source: "qrc:/ui/checkbox-marked-circle.svg"
                    accessibleName: qsTr("Proceed with restoration.")
                    onClicked: {
                        if (backupType === BackupRestoreZipData.BRT_TRANSFER) {
                            yesNoMsgBox.question(qsTr("Proceed?"),
                            qsTr("The action will continue with the restoration of the complete application data. Current application data will be completely rewritten."),
                            qsTr("Do you wish to restore the application data from the transfer?"))
                        } else if (backupType === BackupRestoreZipData.BRT_BACKUP) {
                            yesNoMsgBox.question(qsTr("Proceed?"),
                            qsTr("The action will continue with the restoration of the selected data boxes. Current data of the selected data boxes will be completely rewritten."),
                            qsTr("Do you wish to restore the selected data boxes from the back-up?"))
                        }
                    }
                }
                AccessibleSwitch {
                    id: includeAllAccounts
                    visible: false
                    text: qsTr("Select all data boxes")
                    checked: false
                    onClicked: {
                        restoreSelectionModel.setAllSelected(includeAllAccounts.checked)
                        canRestore()
                    }
                }
            }
            ScrollableListView {
                id: restoreAccountList
                visible: false
                delegateHeight: baseDelegateHeight * 1.4
                width: parent.width
                anchors.top: contentPart.bottom
                height: mainWindow.height - contentPart.height
                model: restoreSelectionModel
                delegate: Rectangle {
                    width: parent.width
                    height: restoreAccountList.delegateHeight
                    color: (!rSelected) ? "transparent" : datovkaPalette.selected
                    MouseArea {
                        function handleClick() {
                            restoreSelectionModel.setSelected(index, !rSelected)
                            canRestore()
                        }
                        anchors.fill: parent
                        Accessible.role: Accessible.Button
                        Accessible.name: (!rSelected) ? qsTr("Select %1.").arg(rAcntName) : qsTr("Deselect %1.").arg(rAcntName)
                        Accessible.onScrollDownAction: restoreAccountList.scrollDown()
                        Accessible.onScrollUpAction: restoreAccountList.scrollUp()
                        Accessible.onPressAction: {
                            handleClick()
                        }
                        onClicked: {
                            handleClick()
                        }
                    }
                    SeparatorLine {
                        anchors.top: parent.top
                        visible: index === 0
                    }
                    Image {
                        id: accountImg
                        source: (rTestAccount) ? "qrc:/ui/databox-test.png" : "qrc:/ui/databox-regular.png"
                        sourceSize.height: baseDelegateHeight
                        anchors.left: parent.left
                        anchors.leftMargin: defaultMargin
                        anchors.verticalCenter: parent.verticalCenter
                    }
                    Column {
                        anchors.left: accountImg.right
                        anchors.leftMargin: defaultMargin * 2
                        anchors.verticalCenter: parent.verticalCenter
                        anchors.right: parent.right
                        spacing: defaultMargin
                        AccessibleLabel {
                            width: parent.width
                            elide: Text.ElideRight
                            text: rAcntName
                        }
                        AccessibleLabel {
                            font.bold: false
                            color: datovkaPalette.greyText
                            text: rUserName + "     boxID: " + rBoxId
                        }
                    }
                    SeparatorLine {
                        anchors.bottom: parent.bottom
                    }
                }
            }
            Connections {
                target: backup_zip
                function onBacupDateTextSig(backupDateTime) {
                    backupDateText.text = backupDateTime
                }
                function onActionTextSig(actionInfo) {
                    actionInfoText.text = actionInfo
                }
                function onSizeTextSig(sizeInfo) {
                    sizeInfoText.text = sizeInfo
                }
                function onVisibleZfoSwitchSig(visible) {
                }
                function onCanRestoreFromTransferSig(restore) {
                    restoreButton.visible = restore
                }
            }
        }
    }
    Rectangle {
        id: busyElement
        visible: false
        anchors.fill: parent
        color: datovkaPalette.text
        opacity: 0.2
        z: 2
        BusyIndicator {
            anchors.centerIn: parent
            running: true
        }
    }
}
