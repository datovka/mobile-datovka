/*
 * Copyright (C) 2014-2025 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

import QtQuick 2.15
import QtQuick.Controls 2.15
import QtQuick.Layouts 1.3
import cz.nic.mobileDatovka 1.0
import cz.nic.mobileDatovka.messages 1.0
import cz.nic.mobileDatovka.models 1.0
import cz.nic.mobileDatovka.qmlIdentifiers 1.0

/*
 * Roles are defined in AccountListModel::roleNames() and are accessed directly
 * via their names.
 */

Page {
    id: welcomePage

    header: PageHeader {
        id: headerBar
        title: "Datovka"
        isFirstPage: true
        isWelcomePage: true
    }

    Flickable {
        anchors.fill: parent
        contentHeight: flickContent.implicitHeight
        Pane {
            id: flickContent
            anchors.fill: parent
            Column {
                anchors.right: parent.right
                anchors.left: parent.left
                spacing: formItemVerticalSpacing
                AccessibleImageButton {
                    anchors.horizontalCenter: parent.horizontalCenter
                    source: "qrc:/datovka.png"
                    sourceSize.width: baseDelegateHeight
                    sourceSize.height: baseDelegateHeight
                    accessibleName: qsTr("Open application home page.")
                    onClicked: Qt.openUrlExternally("http://www.datovka.cz/")
                }
                AccessibleTextInfo {
                    color: datovkaPalette.text
                    horizontalAlignment: Text.AlignHCenter
                    font.bold: true
                    text: qsTr("Welcome in the mobile Datovka.")
                }
                AccessibleTextInfo {
                    horizontalAlignment: Text.AlignHCenter
                    text: qsTr("Add an existing data box into the application.")
                }
                AccessibleButton {
                    anchors.horizontalCenter: parent.horizontalCenter
                    icon.source: "qrc:/ui/databox-add.svg"
                    text: qsTr("Add Data Box - Wizard")
                    accessibleName: qsTr("Add an existing data box using a wizard.")
                    onClicked: pageView.push(pageAccountWizard, {}, StackView.Immediate)
                }
                AccessibleButton {
                    anchors.horizontalCenter: parent.horizontalCenter
                    icon.source: "qrc:/ui/databox-add.svg"
                    text: qsTr("Add Data Box - Form")
                    accessibleName: qsTr("Add an existing data box using a form.")
                    onClicked: pageView.push(pageSettingsAccount, {})
                }
                AccessibleTextInfo {
                    horizontalAlignment: Text.AlignHCenter
                    textFormat: TextEdit.RichText
                    onLinkActivated: Qt.openUrlExternally(link)
                    text: qsTr("Read the user manual for more information about the application behaviour before first use.") + "<br/><style>a:link { color: " + datovkaPalette.highlightedText + "; }</style>" + qsTr("<a href=\"%1\">Learn more...</a>").arg("https://datovka.nic.cz/redirect/mobile-manual-ver-2.html")
                }
                Item {
                    width: parent.width
                    height: formItemVerticalSpacing
                }
                AccessibleImageButton {
                    anchors.horizontalCenter: parent.horizontalCenter
                    source: (appArea.width < appArea.height) ? "qrc:/banners/mojeID_banner_portrait.svg" : "qrc:/banners/mojeID_banner_landscape.svg"
                    accessibleName: qsTr("MojeID banner.")
                    onClicked: Qt.openUrlExternally("https://www.mojeid.cz/cs/proc-mojeid/pristup-ke-sluzbam-verejne-spravy/?utm_source=Aplikace%20Datovka&utm_medium=banner%20v%20aplikaci%20Datovka&utm_campaign=mojeID%20banner%20v%20aplikaci%20Datovka")
                }
            }
        }
    }
}
