/*
 * Copyright (C) 2014-2025 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

import QtQuick 2.7
import QtQuick.Controls 2.15
import cz.nic.mobileDatovka 1.0
import cz.nic.mobileDatovka.messages 1.0
import cz.nic.mobileDatovka.models 1.0
import cz.nic.mobileDatovka.qmlIdentifiers 1.0

/*
 * Roles are defined in MessageListModel::roleNames() and are accessed directly
 * via their names.
 */
Page {

    property int soughtMsgType: MessageType.TYPE_RECEIVED | MessageType.TYPE_SENT

   function showBusyIndicator(visible) {
        headerBar.enabled = !visible
        searchPanel.enabled = !visible
        messageList.visible = !visible
        messageList.enabled = !visible
        busyElementSection.visible = visible
        busyIndicator.running = visible
    }

    function doSearchRequest() {
        emptyList.visible = false
        showBusyIndicator(true)
        emptyList.visible = (messages.searchMsg(messageModel, searchPhraseText.text, soughtMsgType) <= 0)
        showBusyIndicator(false)
    }

    Component.onCompleted: {
         searchPhraseText.forceActiveFocus()
    }

    MessageListModel {
        id: messageModel
    }

    header: PageHeader {
        id: headerBar
        title: qsTr("Search Message")
        onBackClicked: {
            pageView.pop()
        }
        AccessibleToolButton {
            id: actionButton
            anchors.right: parent.right
            icon.source: "qrc:/ui/magnify.svg"
            accessibleName: qsTr("Search messages.")
            onClicked: {
                searchPhraseText.focus = false
                doSearchRequest()
            }
        }
    }

    Pane {
        id: searchPanel
        anchors.top: parent.top
        width: parent.width
        ControlGroupItem {
            spacing: formItemVerticalSpacing
            AccessibleComboBox {
                id: searchOptionComboBox
                width: parent.width
                accessibleDescription: qsTr("Select type of sought messages")
                model: ListModel {
                    id: searchOptionComboBoxModel
                    /* Cannot have an expression here because QML complains that it cannot use script for property key. */
                    ListElement { label: qsTr("All messages"); key: -1 }
                    ListElement { label: qsTr("Received messages"); key: MessageType.TYPE_RECEIVED }
                    ListElement { label: qsTr("Sent messages"); key: MessageType.TYPE_SENT }
                }
                onCurrentIndexChanged: {
                    if (currentKey() === -1) {
                        soughtMsgType = MessageType.TYPE_RECEIVED | MessageType.TYPE_SENT
                    } else {
                        soughtMsgType = currentKey()
                    }
                }
            }
            AccessibleTextField {
                id: searchPhraseText
                placeholderText: qsTr("Enter phrase")
                focus: true
                width: parent.width
                onAccepted: doSearchRequest()
            }
        }
    } // Pane

    ControlGroupItem {
        id: busyElementSection
        visible: false
        anchors.verticalCenter: parent.verticalCenter
        BusyIndicator {
            id: busyIndicator
            anchors.horizontalCenter: parent.horizontalCenter
            running: false
        }
        AccessibleTextInfo {
            horizontalAlignment: Text.AlignHCenter
            text: qsTr("Searching for messages ...")
        }
    }

    MessageList {
        id: messageList
        anchors.top: searchPanel.bottom
        anchors.bottom: parent.bottom
        visible: true
        width: parent.width
        model: messageModel
        onMsgClicked: {
            messages.markMessageAsLocallyRead(messageModel, acntId, msgId, true)
            pageView.push(pageMessageDetail, {
                "pageView": pageView,
                "fromLocalDb": true,
                "acntName": "",
                "acntId": acntId,
                "msgDaysToDeletion": msgDaysToDeletion,
                "msgType": msgType,
                "msgId": msgId,
                "messageModel": messageModel
            })
        }
    } // MessageList

    AccessibleTextInfo {
        id: emptyList
        visible: false
        anchors.centerIn: parent
        anchors.margins: defaultMargin
        horizontalAlignment: Text.AlignHCenter
        text: qsTr("No message found for given search phrase.")
    }
} // Page
