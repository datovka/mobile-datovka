/*
 * Copyright (C) 2014-2025 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

import QtQuick 2.7
import QtQuick.Controls 2.15
import QtQuick.Layouts 1.3
import QtQuick.Controls.Material 2.15
import cz.nic.mobileDatovka 1.0
import cz.nic.mobileDatovka.messages 1.0
import cz.nic.mobileDatovka.files 1.0
import cz.nic.mobileDatovka.models 1.0
import cz.nic.mobileDatovka.msgInfo 1.0
import cz.nic.mobileDatovka.qmlIdentifiers 1.0

Page {
    id: msgDetailPage

    /* These properties must be set by caller. */
    property var pageView
    property bool fromLocalDb: false
    property var rawZfoContent: null /* Raw ZFO file content. */
    property string acntName: ""
    property AcntId acntId: null
    property int msgDaysToDeletion
    property int msgType: MessageType.TYPE_RECEIVED
    property string msgId: ""
    property var messageModel: null

    /* These properties are set from within the page. */
    property string zfoId /*Prevents infinite loop that would be caused by setting msgId from within the page. */
    property string msgAnnotation
    property string msgDescrHtml /* Message HTML description. */
    property string msgDescrHtmlShort /* Message HTML description. */
    property string emailBody
    property string deleteInfo: ""
    property int zfoType: MsgInfo.TYPE_UNKNOWN
    property bool loadFromZfo: false
    property bool loadFromExternalZfo: false
    property bool showCompleteInfo: false

    function setTopButtonVisibility() {
        replyButton.visible = fromLocalDb
        forwardButton.visible = fromLocalDb
        attachmentMenuButon.visible = fromLocalDb && (attachmentList.count > 0)
    }

    function showCompleteMsgInfo(msgDaysToDeletion) {
        if (!showCompleteInfo) {
            doOpen.start()
            messageEnvelope.text = deleteInfo + msgDescrHtml
            showCompleteInfo = true
            expandInfo.text = qsTr("Hide")
        } else {
            doClose.start()
            messageEnvelope.text = deleteInfo + msgDescrHtmlShort
            showCompleteInfo = false
            expandInfo.text = qsTr("Show more")
        }
    }

    function showMsgDeletionInfo(msgDaysToDeletion) {
        var text = ""
        if (msgDaysToDeletion <= settings.msgDeletionNotifyAheadDays() && msgDaysToDeletion >0) {
            /*
             * actDTType 0 == Long term storage is not active.
             * -1 means DTInfo is unknown.
             */
            if (accounts.actDTType(acntId) <= 0) {
                text = qsTr("The message will be deleted from ISDS in %n day(s).", "", msgDaysToDeletion)
            } else {
                if (accounts.isDTCapacityFull(acntId)) {
                    text = qsTr("The message will be deleted from ISDS in %n day(s) because the long term storage is full.", "", msgDaysToDeletion)
                } else {
                    text = qsTr("The message will be moved to the long term storage in %n day(s) if the storage is not full. The message will be deleted from the ISDS server if the storage is full.", "", msgDaysToDeletion)
                }
            }
        } else if (msgDaysToDeletion === 0) {
            text = qsTr("The message has already been deleted from the ISDS.")
        }
        if (text === "") {
            return text
        } else {
            return textColorRed(text)
        }
    }

    function fillContentFromDb(rZfoContent) {
        if (!fromLocalDb && (rZfoContent != null)) {
            /* This method also sets the attachment model. */
            var msgInfo = files.zfoData(attachmentModel, rZfoContent)
            zfoId = msgInfo.idStr
            msgAnnotation = msgInfo.annotation
            msgDescrHtml = msgInfo.descrHtml
            msgDescrHtmlShort = msgDescrHtml
            emailBody = msgInfo.emailBody
            zfoType = msgInfo.type
            attachmentArea.visible = false
            if (zfoType == MsgInfo.TYPE_MESSAGE) {
                attachmentArea.visible = true
            } else if (zfoType == MsgInfo.TYPE_DELIVERY_INFO) {
                flickable.height = mainWindow.height
                showAll.start()
            } else {
                messageEnvelope.text = qsTr("File doesn't contain a valid message nor does it contain valid delivery information or the file is corrupt.")
            }
            downloadMsgButton.visible = attachmentArea.visible
            expandInfo.visible = downloadMsgButton.visible
            rawZfoContent = null /* There is no need to remember this. */
            loadFromExternalZfo = true
        } else if (fromLocalDb && (acntId.username.length !== 0) && (msgId.length !== 0)) {
            zfoId = msgId
            msgDescrHtml = messages.getMessageDetail(acntId, zfoId, msgType, true)
            msgDescrHtmlShort = messages.getShortMessageDetail(acntId, zfoId, msgType)
            var filePath = messages.getZfoFilePath(acntId, zfoId, msgType, false)
            var zfoContent = files.rawFileContent(filePath)
            if (zfoContent != "") {
                console.log("Loading attachments from ZFO file.")
                loadFromZfo = true
                files.zfoData(attachmentModel, zfoContent)
            } else {
                console.log("Loading attachments from database.")
                loadFromZfo = false
                attachmentModel.setFromDb(acntId, zfoId)
            }
            zfoType = MsgInfo.TYPE_MESSAGE
            deleteInfo = showMsgDeletionInfo(msgDaysToDeletion)
        }
        downloadMsgButton.visible = (attachmentList.count === 0)
        setTopButtonVisibility()
    }

   function composePageTitle() {
            var str = "";
            if (fromLocalDb && (msgType === MessageType.TYPE_RECEIVED)) {
               str += qsTr("Received")
            } else if (fromLocalDb && (msgType === MessageType.TYPE_SENT)) {
               str += qsTr("Sent")
            } else if (zfoType == MsgInfo.TYPE_MESSAGE) {
               str += qsTr("Message")  + " " + zfoId
            } else if (zfoType == MsgInfo.TYPE_DELIVERY_INFO) {
               str += qsTr("Acceptance Info")  + " " + zfoId
            } else {
               str += qsTr("Unknown ZFO Content")
            }
            return str
       }

    function openMessageDetailPopup() {
        if (fromLocalDb) {
            popupMenuMessageDetail.acntName = acntName
            popupMenuMessageDetail.acntId = acntId
            popupMenuMessageDetail.msgType = msgType
            popupMenuMessageDetail.msgId = zfoId
            popupMenuMessageDetail.messageModel = messageModel
            popupMenuMessageDetail.attachmentModel = attachmentModel
            popupMenuMessageDetail.loadFromZfo = loadFromZfo
            popupMenuMessageDetail.loadFromExternalZfo = loadFromExternalZfo
            popupMenuMessageDetail.canDelete = (msgDaysToDeletion === 0)
            popupMenuMessageDetail.open()
        }
    }

   function showBusyIndicator(visible) {
        headerBar.enabled = !visible
        downloadMsgButton.visible = !visible
        busyElementSection.visible = visible
        busyIndicator.running = visible
    }

    function doIsdsAction(action) {
        showBusyIndicator(true)
        isds.doIsdsAction(action, acntId)
    }

    onRawZfoContentChanged: {
        fillContentFromDb(rawZfoContent)
    }

    onAcntIdChanged: {
        fillContentFromDb(null)
    }

    onMsgIdChanged: {
        fillContentFromDb(null)
    }

    FileListModel {
        id: attachmentModel
        Component.onCompleted: {
        }
    }

    Connections {
        target: messages
        function onUpdateMessageDetail(msgHtmlInfo) {
            msgDescrHtml = deleteInfo + msgHtmlInfo
        }
    }

    PopupMenuMessageDetail {
        id: popupMenuMessageDetail
    }

    MessageBox {
        id: yesNoMsgBoxiOS
        onAcceptAndClosed: {
            if (loadFromZfo || loadFromExternalZfo) {
                files.saveAttachmentsToDiskZfo(attachmentModel, acntId, zfoId, true)
            } else {
                files.saveMsgFilesToDisk(acntId, zfoId, msgType, MsgAttachFlag.MSG_ATTACHS, true)
            }
        }
        onRejectAndClosed: {
            if (loadFromZfo || loadFromExternalZfo) {
                files.saveAttachmentsToDiskZfo(attachmentModel, acntId, zfoId, false)
            } else {
                files.saveMsgFilesToDisk(acntId, zfoId, msgType, MsgAttachFlag.MSG_ATTACHS, false)
            }
        }
    }

    header: PageHeader {
        id: headerBar
        title: composePageTitle()
        onBackClicked: {
            pageView.pop()
        }
        AccessibleToolButton {
            id: attachmentMenuButon
            anchors.right: parent.right
            icon.source: "qrc:/ui/android-more-vertical.svg"
            accessibleName: qsTr("Show menu of available operations.")
            onClicked: openMessageDetailPopup()
        }
    }

    Item {
        anchors.fill: parent
        Flickable {
            id: flickable
            clip: true
            anchors.top: parent.top
            width: parent.width
            height: mainWindow.height * 0.25
            contentHeight: flickContent.implicitHeight
            Pane {
                id: flickContent
                anchors.fill: parent
                background: Rectangle {
                    color: datovkaPalette.base
                }
                AccessibleText {
                    id: messageEnvelope
                    width: parent.width
                    textFormat: TextEdit.RichText
                    wrapMode: Text.WordWrap
                    text: deleteInfo + msgDescrHtmlShort
                    MouseArea {
                        anchors.fill: parent
                        onClicked: {
                            msgDetailPage.showCompleteMsgInfo(msgDaysToDeletion)
                        }
                    }
                }
            } // Pane
            ScrollIndicator.vertical: ScrollIndicator {}
        } // Flickable

        ParallelAnimation {
            id: showAll
            running: false
            NumberAnimation {
                target: flickable
                easing.type: Easing.OutSine
                property: "height"
                to: mainWindow.height
                duration: 10
            }
        }
        ParallelAnimation {
            id: doOpen
            running: false
            NumberAnimation {
                target: flickable
                easing.type: Easing.OutSine
                property: "height"
                to: mainWindow.height * 0.6
                duration: 1000
            }
        }
        ParallelAnimation {
            id: doClose
            running: false
            NumberAnimation {
                target: flickable
                easing.type: Easing.OutSine
                property: "height"
                to: mainWindow.height * 0.25
                duration: 1000
            }
        }

        AccessibleTextInfo {
            id: expandInfo
            anchors.top: flickable.bottom
            anchors.topMargin: defaultMargin
            horizontalAlignment: Text.AlignHCenter
            color: datovkaPalette.highlightedText
            font.bold: true
            text: qsTr("Show more")
            MouseArea {
                anchors.fill: parent
                onClicked: msgDetailPage.showCompleteMsgInfo(msgDaysToDeletion)
            }
        }

        Item {
            id: attachmentArea
            anchors.top: expandInfo.bottom
            anchors.bottom: parent.bottom
            anchors.right: parent.right
            anchors.left: parent.left
            Item {
                id: attachmentLabel
                width: parent.width
                height: baseDelegateHeight
                AccessibleText {
                    anchors.verticalCenter: parent.verticalCenter
                    anchors.left: parent.left
                    anchors.leftMargin: defaultMargin * 2
                    textFormat: TextEdit.RichText
                    text: qsTr("Attachments")
                    font.bold: true
                    font.pixelSize: textFontPixelSizeLarge
                }
            }
            ControlGroupItem {
                id: downloadMsgButton
                anchors.centerIn: parent
                visible: false
                z: 1
                AccessibleButton {
                    z: 2
                    icon.source: "qrc:/ui/attach-download.svg"
                    text: qsTr("Download Message")
                    Accessible.name: qsTr("Download complete message with attachments.")
                    onClicked: {
                        if (fromLocalDb) {
                            doIsdsAction("downloadMessage")
                        }
                    }
                }
                AccessibleTextInfo {
                    horizontalAlignment: Text.AlignHCenter
                    text: qsTr("Download complete signed message %1 with attachments.").arg(msgId)
                }
            }
            ControlGroupItem {
                id: busyElementSection
                visible: false
                anchors.verticalCenter: parent.verticalCenter
                z: 1
                BusyIndicator {
                    id: busyIndicator
                    anchors.horizontalCenter: parent.horizontalCenter
                    running: false
                }
                AccessibleTextInfo {
                    horizontalAlignment: Text.AlignHCenter
                    text: qsTr("Downloading message %1 ...").arg(msgId)
                }
            }
            ScrollableListView {
                id: attachmentList
                delegateHeight: listViewDelegateHeight * 0.7
                anchors.top: attachmentLabel.bottom
                anchors.bottom: parent.bottom
                visible: true
                width: parent.width
                model: attachmentModel
                onCountChanged: {
                    downloadMsgButton.visible = (attachmentList.count === 0)
                    setTopButtonVisibility()
                }
                delegate: Rectangle {
                    id: attachmentItem
                    width: attachmentList.width
                    height: attachmentList.delegateHeight
                    color: datovkaPalette.base
                    Item {
                        anchors.fill: parent
                        anchors.topMargin: defaultMargin
                        anchors.bottomMargin: defaultMargin
                        Image {
                            id: imageAttachment
                            anchors.verticalCenter: parent.verticalCenter
                            sourceSize.height: parent.height
                            source: files.getAttachmentFileIcon(rFileName)
                        }
                        Column {
                            anchors.left: imageAttachment.right
                            anchors.leftMargin: defaultMargin
                            anchors.right: parent.right
                            anchors.verticalCenter: parent.verticalCenter
                            spacing: defaultMargin
                            AccessibleText {
                                width: parent.width
                                elide: Text.ElideRight
                                font.bold: true
                                text: rFileName
                            }
                            AccessibleTextInfo {
                                text: rFileSizeStr
                            }
                        }
                        MouseArea {
                            function handleClick() {
                                locker.ignoreNextSuspension()
                                if (files.isZfoFile(rFileName)) {
                                    console.log("Attachment is ZFO: " + rFileName)
                                    var fileContent = null
                                    if (!fromLocalDb || loadFromZfo) {
                                        fileContent = rBinaryContent
                                    } else {
                                        fileContent = files.getFileRawContentFromDb(acntId, rFileId)
                                    }
                                    pageView.push(pageMessageDetail, {
                                            "pageView": pageView,
                                            "rawZfoContent": fileContent
                                        })
                                } else {
                                    if (!fromLocalDb || loadFromZfo) {
                                        files.openAttachment(rFileName, rBinaryContent)
                                    } else {
                                        files.openAttachmentFromDb(acntId, rFileId)
                                    }
                                }
                            }
                            anchors.fill: parent
                            Accessible.role: Accessible.Button
                            Accessible.name: qsTr("Open attachment '%1'.").arg(rFileName)
                            Accessible.onScrollDownAction: attachmentList.scrollDown()
                            Accessible.onScrollUpAction: attachmentList.scrollUp()
                            Accessible.onPressAction: {
                                handleClick()
                            }
                            onClicked: {
                                handleClick()
                            }
                        }
                    }
                }
                property bool downloadStart: false
                Timer {
                    /*
                     * The MEP dialogue is not shown when the movement animation
                     * is active. This timer ensures that the login procedure starts
                     * with a delay.
                     * TODO -- Find a better solution.
                     */
                    id: dwnldTimer
                    interval: 500
                    running: false
                    repeat: false
                    onTriggered: doIsdsAction("downloadMessage")
                }
                onMovementEnded: {
                    if (downloadStart) {
                        downloadStart = false
                        if (fromLocalDb) {
                            dwnldTimer.start()
                        }
                    }
                }
                onDragEnded: {
                    downloadStart = contentY < -120
                }
                ScrollIndicator.vertical: ScrollIndicator {}
            } // Listview
        } // Item
    } // Item

    AcntId {
        id: actionAcntId
    }
    Connections {
        target: isds
        function onLoginFailed(userName, testing) {
            if (acntId.username === userName && acntId.testing === testing) {
                showBusyIndicator(false)
                downloadMsgButton.visible = (attachmentList.count === 0)
            }
        }
        function onDownloadMessageFinishedSig(userName, testing, msgId, msgIdStr, success, errTxt) {
            showBusyIndicator(false)
            fillContentFromDb()
            messages.overrideDownloaded(messageModel, msgId, true);
            if (!success) {
                okMsgBox.error(qsTr("Downloading message: %1").arg(userName),
                   qsTr("Failed to download complete message %1.").arg(msgIdStr),
                   errTxt)
            }
        }
        function onRunDownloadMessageSig(userName, testing) {
            if (fromLocalDb) {
                actionAcntId.username = userName
                actionAcntId.testing = testing
                var vodz = messages.isMessageVodz(actionAcntId, zfoId)
                if (vodz) {
                    isds.downloadBigMessage(actionAcntId, msgType, zfoId)
                } else {
                    isds.downloadMessage(actionAcntId, msgType, zfoId)
                }
            }
        }
    }

    footer: ToolBar {
        id: toolBar
        Material.foreground: buttonBgColor
        visible: (attachmentList.count > 0)
        RowLayout {
            anchors.centerIn : parent
            spacing: formItemVerticalSpacing
            AccessibleToolButton {
                id: replyButton
                accessibleName: qsTr("Reply on the message.")
                icon.source: "qrc:/ui/reply.svg"
                text: qsTr("Reply")
                display: AbstractButton.TextUnderIcon
                onClicked: pageView.push(pageSendMessage, {"pageView": pageView, "acntName" : acntName, "acntId": acntId, "msgId": msgId, "msgType": msgType, "action": "reply"}, StackView.Immediate)
            }
            AccessibleToolButton {
                id: forwardButton
                accessibleName: qsTr("Forward message.")
                icon.source: "qrc:/ui/forward.svg"
                text: qsTr("Forward")
                display: AbstractButton.TextUnderIcon
                onClicked: pageView.push(pageSendMessage, {"pageView": pageView, "acntName" : acntName, "acntId": acntId, "msgId": msgId, "msgType": msgType, "action": "fwdzfo"}, StackView.Immediate)
            }
            AccessibleToolButton {
                accessibleName: qsTr("Send attachments via e-mail.")
                icon.source: "qrc:/ui/email-attachment.svg"
                text: qsTr("E-mail")
                display: AbstractButton.TextUnderIcon
                onClicked: {
                    if (loadFromZfo) {
                        files.sendAttachmentsWithEmail(attachmentModel, acntId, msgId, msgType, MsgAttachFlag.MSG_ATTACHS)
                    } else if (loadFromExternalZfo) {
                        files.sendAttachmentEmailZfo(attachmentModel, acntId, zfoId, msgType, msgAnnotation, emailBody, MsgAttachFlag.MSG_ATTACHS)
                    } else {
                        files.sendMsgFilesWithEmail(acntId, msgId, msgType, MsgAttachFlag.MSG_ATTACHS)
                    }
                }
            }
            AccessibleToolButton {
                accessibleName: qsTr("Save attachments to the local storage.")
                icon.source: "qrc:/ui/save-to-disk-attachment.svg"
                text: qsTr("Save")
                display: AbstractButton.TextUnderIcon
                onClicked: {
                    if (Qt.platform.os == "ios") {
                        yesNoMsgBoxiOS.question(qsTr("Save Attachments: %1").arg(msgId),
                        qsTr("You can export files into iCloud or into device local storage."),
                        qsTr("Do you want to export files to Datovka iCloud container?"))
                    } else  {
                        if (loadFromZfo || loadFromExternalZfo) {
                            files.saveAttachmentsToDiskZfo(attachmentModel, acntId, zfoId, false)
                        } else {
                            files.saveMsgFilesToDisk(acntId, zfoId, msgType, MsgAttachFlag.MSG_ATTACHS, false)
                        }
                    }
                }
            }
        }
    }
}
