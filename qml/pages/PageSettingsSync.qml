/*
 * Copyright (C) 2014-2025 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

import QtQuick 2.7
import QtQuick.Controls 2.15
import cz.nic.mobileDatovka 1.0

Page {

    Component.onCompleted: {
        downloadOnlyNewMsgs.checked = settings.downloadOnlyNewMsgs()
        downloadCompleteMsgs.checked = settings.downloadCompleteMsgs()
        syncAfterAppStart.checked = settings.syncAfterCleanAppStart()
    }

    header: PageHeader {
        title: qsTr("Synchronisation Settings")
        onBackClicked: {
            pageView.pop()
        }
    }

    Flickable {
        anchors.fill: parent
        contentHeight: flickContent.implicitHeight
        Pane {
            id: flickContent
            anchors.fill: parent
            Column {
                anchors.right: parent.right
                anchors.left: parent.left
                spacing: formItemVerticalSpacing
                ControlGroupItem {
                    AccessibleSwitch {
                        id: downloadOnlyNewMsgs
                        text: qsTr("Download only newer messages")
                        checked: true
                        onClicked: settings.setDownloadOnlyNewMsgs(downloadOnlyNewMsgs.checked)
                    }
                    AccessibleTextInfoSmall {
                        text: (downloadOnlyNewMsgs.checked ?
                           qsTr("Only messages which are not older than 90 days will be downloaded.") :
                           qsTr("All available messages (including those in the data vault) will be downloaded."))
                    }
                }
                SeparatorLine {}
                ControlGroupItem {
                    AccessibleSwitch {
                        id: downloadCompleteMsgs
                        text: qsTr("Download complete messages")
                        onClicked: settings.setDownloadCompleteMsgs(downloadCompleteMsgs.checked)
                    }
                    AccessibleTextInfoSmall {
                        text: (downloadCompleteMsgs.checked ?
                           qsTr("Complete messages will be downloaded during data box synchronisation. This may be slow.") :
                           qsTr("Only message envelopes will be downloaded during data box synchronisation."))
                    }
                }
                SeparatorLine {}
                ControlGroupItem {
                    AccessibleSwitch {
                        id: syncAfterAppStart
                        text: qsTr("Synchronise data box after start")
                        onClicked: settings.setSyncAfterCleanAppStart(syncAfterAppStart.checked)
                    }
                    AccessibleTextInfoSmall {
                        text: (syncAfterAppStart.checked ?
                           qsTr("Data boxes will automatically be synchronised a few seconds after application start-up.") :
                           qsTr("Data boxes will be synchronised only when the action is manually invoked."))
                    }
                }
                SeparatorLine {}
            } // Column layout
        } // Pane
        ScrollIndicator.vertical: ScrollIndicator {}
    } // Flickable
} // Page
