/*
 * Copyright (C) 2014-2025 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

import QtQuick 2.7
import QtQuick.Controls 2.15
import cz.nic.mobileDatovka 1.0

Page {

    /* Remember last service URL from settings */
    property string lastUrlFromSettings: ""

    /* Enable info and clear buttons if URL and token fields are filled */
    function areUrlandTokenFilled() {
        infoButton.enabled = (urlTextField.textLine.displayText.toString() !== "" && tokenTextField.textLine.displayText.toString() !== "")
        clearButton.enabled = infoButton.enabled
        updateFileList.visible = infoButton.enabled
    }

    /* Clear all data and info */
    function clearAll() {
        urlTextField.textLine.clear()
        tokenTextField.textLine.clear()
        serviceInfoSection.visible = false
        areUrlandTokenFilled()
        acceptElement.visible = true
        updateFileList.visible = false
    }

    Component.onCompleted: {
        serviceInfoSection.visible = false
        urlTextField.textLine.text = settings.rmUrl()
        lastUrlFromSettings = settings.rmUrl()
        tokenTextField.textLine.text = settings.rmToken()
        areUrlandTokenFilled()
        recordsManagement.loadStoredServiceInfo()
    }

    header: PageHeader {
        title: qsTr("Records Management")
        onBackClicked: {
            pageView.pop()
        }
    }

    Flickable {
        anchors.fill: parent
        contentHeight: flickContent.implicitHeight
        Pane {
            id: flickContent
            anchors.fill: parent
            ControlGroupItem {
                spacing: formItemVerticalSpacing
                TextLineItem {
                    id: urlTextField
                    textLineTitle: qsTr("Service URL")
                    placeholderText: qsTr("Enter service URL.")
                    titleText: textLineTitle
                    infoText: qsTr("Please fill in service URL.")
                    textLine.onTextChanged: areUrlandTokenFilled()
                }
                TextLineItem {
                    id: tokenTextField
                    textLineTitle: qsTr("Your token")
                    placeholderText: qsTr("Enter your token.")
                    titleText: textLineTitle
                    infoText: qsTr("Please fill in your identification token.")
                    textLine.onTextChanged: areUrlandTokenFilled()
                }
                Row {
                    spacing: formItemVerticalSpacing
                    anchors.horizontalCenter: parent.horizontalCenter
                    AccessibleButton {
                        id: acceptElement
                        visible: false
                        icon.source: "qrc:/ui/checkbox-marked-circle.svg"
                        text: qsTr("Save Changes")
                        accessibleName: qsTr("Accept changes.")
                        onClicked: {
                            settings.setRmUrl(urlTextField.textLine.displayText.toString())
                            settings.setRmToken(tokenTextField.textLine.displayText.toString())
                            recordsManagement.updateServiceInfo(urlTextField.textLine.displayText.toString(), lastUrlFromSettings, serviceName.text, tokenName.text)
                            pageView.pop()
                        }
                    }
                    AccessibleButton {
                        id: infoButton
                        text: qsTr("Get Service Info")
                        icon.source: "qrc:/ui/briefcase.svg"
                        visible: !acceptElement.visible
                        onClicked: {
                            serviceInfoSection.visible = recordsManagement.callServiceInfo(urlTextField.textLine.displayText.toString(), tokenTextField.textLine.displayText.toString())
                            serviceInfoError.visible = !serviceInfoSection.visible
                            acceptElement.visible = serviceInfoSection.visible
                        }
                    }
                    AccessibleButton {
                        id: clearButton
                        accessibleName: qsTr("Clear records management data.")
                        text: qsTr("Clear")
                        icon.source: "qrc:/ui/close-octagon.svg"
                        onClicked: clearAll()
                    }
                } // Row
                AccessibleTextInfo {
                    id: serviceInfoError
                    visible: false
                    horizontalAlignment: Text.AlignHCenter
                    color: datovkaPalette.errorText
                    text: qsTr("Communication error. Cannot obtain records management info from server. Internet connection failed or service URL and/or identification token could be wrong.")
                }
                Grid {
                    id: serviceInfoSection
                    columns: 2
                    spacing: 2
                    AccessibleText {
                        text: qsTr("Service name") + ":"
                    }
                    AccessibleText {
                        id: serviceName
                        font.bold: true
                        text: ""
                    }
                    AccessibleText {
                        text: qsTr("Service token") + ":"
                    }
                    AccessibleText {
                        id: tokenName
                        font.bold: true
                        text: ""
                    }
                    AccessibleText {
                        verticalAlignment: Text.AlignVCenter
                        text: qsTr("Service logo") + ":"
                    }
                    AccessibleImageButton {
                        id: serviceLogo
                        width: baseDelegateHeight * 1.5
                        height: width
                        source: "qrc:/ui/briefcase.svg"
                        cache: false
                        accessibleName: qsTr("Records management logo.")
                    }
                } // Grid
                ControlGroupItem {
                    id: updateFileList
                    visible: false
                    AccessibleButton {
                        anchors.horizontalCenter: parent.horizontalCenter
                        text: qsTr("Update list of uploaded files")
                        icon.source: "qrc:/ui/sync.svg"
                        onClicked: recordsManagement.getStoredMsgInfoFromRecordsManagement(settings.rmUrl(), settings.rmToken())
                    }
                    AccessibleTextInfo {
                        horizontalAlignment: Text.AlignHCenter
                        text: qsTr("Update list of uploaded files from records management.")
                    }
                }
            } // Column
        } // Pane
        ScrollIndicator.vertical: ScrollIndicator {}
    } // Flickable
    Connections {
        target: recordsManagement
        function onServiceInfo(srName, srToken) {
            if (srName !== "" && srToken !== "") {
                serviceInfoSection.visible = true
                acceptElement.visible = true
                serviceName.text = srName
                tokenName.text = srToken
                serviceInfoError.visible = false
                serviceLogo.source = "image://images/rmlogo.svg"
                updateFileList.visible = true
            } else {
                serviceInfoSection.visible = false
                updateFileList.visible = false
            }
        }
    }
} // Page
