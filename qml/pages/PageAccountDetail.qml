/*
 * Copyright (C) 2014-2025 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

import QtQuick 2.7
import QtQuick.Controls 2.15
import cz.nic.mobileDatovka 1.0
import cz.nic.mobileDatovka.qmlIdentifiers 1.0

Page {

    /* These properties must be set by caller. */
    property var pageView
    property AcntId acntId: null

    function setAccountDetailInfo(acntId) {
        accountDetailText.text = accounts.fillAccountInfo(acntId)
    }

    function disableHeader(visible) {
        headerBar.enabled = !visible
        actionButtonRotation.running = visible
    }

    function doIsdsAction(action) {
        disableHeader(true)
        isds.doIsdsAction(action, acntId)
    }

    Component.onCompleted: {
        setAccountDetailInfo(acntId)
    }

    header: PageHeader {
        id: headerBar
        title: qsTr("Data-Box Info")
        onBackClicked: {
            pageView.pop(StackView.Immediate)
        }
        AccessibleToolButton {
            id: actionButton
            anchors.right: parent.right
            icon.source: "qrc:/ui/sync.svg"
            accessibleName: qsTr("Refresh information about the data box.")
            onClicked: doIsdsAction("getAccountInfo")
            RotationAnimator {
                id: actionButtonRotation
                target: actionButton
                alwaysRunToEnd:  true
                from: 0;
                to: 360;
                duration: 2000
                loops: Animation.Infinite
                running: false
            }
        }
    }

    Flickable {
        anchors.fill: parent
        contentHeight: flickContent.implicitHeight
        Pane {
            id: flickContent
            anchors.fill: parent
            AccessibleText {
                id: accountDetailText
                width: parent.width
                textFormat: TextEdit.RichText
                wrapMode: Text.WordWrap
                text: qsTr("Information about the data box haven't been downloaded yet.")
            }
        }
        ScrollIndicator.vertical: ScrollIndicator {}
    }

    AcntId {
        id: actionAcntId
    }
    Connections {
        target: isds
        function onLoginFailed(userName, testing) {
            if (acntId.username === userName && acntId.testing === testing) {
                disableHeader(false)
            }
        }
        function onDownloadAccountInfoFinishedSig(userName, testing, success, errTxt) {
            actionAcntId.username = userName
            actionAcntId.testing = testing
            disableHeader(false)
            setAccountDetailInfo(actionAcntId)
            if (!success) {
                okMsgBox.error(qsTr("Data-Box Info: %1").arg(userName),
                    qsTr("Failed to get data-box info for username '%1'.").arg(userName),
                    errTxt)
            }
        }
    }
}
