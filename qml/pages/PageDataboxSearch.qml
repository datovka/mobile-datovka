/*
 * Copyright (C) 2014-2025 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

import QtQuick 2.7
import QtQuick.Controls 2.15
import cz.nic.mobileDatovka 1.0
import cz.nic.mobileDatovka.models 1.0
import cz.nic.mobileDatovka.qmlIdentifiers 1.0

/*
 * Roles are defined in DataboxListModel::roleNames() and are accessed directly
 * via their names.
 */

Page {

    /* These properties must be set by caller. */
    property var pageView
    property AcntId acntId: null
    property var recipBoxModel: null

    /* These properties remember choice of ComboBoxes */
    property string searchType: "GENERAL"
    property string searchScope: "ALL"

    /* These properties remember origin search settings during pagination */
    property int page: 0
    property string searchTextTmp
    property string searchTypeTmp
    property string searchScopeTmp

   function showBusyIndicator(visible) {
        headerBar.enabled = !visible
        searchPanel.enabled = !visible
        databoxList.visible = !visible
        databoxList.enabled = !visible
        busyElementSection.visible = visible
        busyIndicator.running = visible
    }

    Component.onCompleted: {
         searchPhraseText.forceActiveFocus()
         proxyDataboxModel.setSourceModel(foundBoxModel)
    }

    DataboxListModel {
        id: foundBoxModel
    }
    ListSortFilterProxyModel {
        id: proxyDataboxModel
        Component.onCompleted: {
            setFilterRoles([DataboxListModel.ROLE_DB_NAME,
                DataboxListModel.ROLE_DB_ADDRESS, DataboxListModel.ROLE_DB_IC,
                DataboxListModel.ROLE_DB_ID])
        }
    }

    header: PageHeader {
        id: headerBar
        title: qsTr("Find Data Box")
        onBackClicked: {
            pageView.pop(StackView.Immediate)
        }
        AccessibleToolButton {
            id: actionButton
            anchors.right: parent.right
            icon.source: "qrc:/ui/account-search.svg"
            accessibleName: qsTr("Search data boxes.")
            enabled: searchPhraseText.text.length > 0
            onClicked: {
                searchPhraseText.focus = false
                searchPanel.doSearchRequest()
            }
        }
        AccessibleToolButton {
            id: filterButton
            visible: false
            anchors.right: actionButton.left
            icon.source: "qrc:/ui/magnify.svg"
            accessibleName: qsTr("Filter data boxes.")
            onClicked: {
                filterBar.open()
                filterBar.filterField.forceActiveFocus()
                Qt.inputMethod.show()
            }
        }
    }

    FilterBar {
        id: filterBar
        y: header.height
        onTextChanged: text => proxyDataboxModel.setFilterRegExpStr(text)
        onClearClicked: close()
    }

    Pane {
        id: searchPanel
        anchors.top: parent.top
        width: parent.width
        function doSearchRequest() {
            if (searchPhraseText.text.length > 0) {
                showBusyIndicator(true)
                isds.doIsdsAction("findDataboxFulltext", acntId)
            }
        }

        ControlGroupItem {
            spacing: formItemVerticalSpacing
            AccessibleComboBox {
                id: searchScopeComboBox
                width: parent.width
                accessibleDescription: qsTr("Select type of sought data box")
                model: ListModel {
                    id: searchScopeComboBoxModel
                    ListElement { label: qsTr("All data boxes"); key: "ALL" }
                    ListElement { label: qsTr("OVM"); key: "OVM" }
                    ListElement { label: qsTr("PO"); key: "PO" }
                    ListElement { label: qsTr("PFO"); key: "PFO" }
                    ListElement { label: qsTr("FO"); key: "FO" }
                }
                onCurrentIndexChanged: searchScope = currentKey()
            }
            AccessibleComboBox {
                id: searchTypeComboBox
                width: parent.width
                accessibleDescription: qsTr("Select entries to search in")
                model: ListModel {
                    id: searchTypeComboBoxModel
                    ListElement { label: qsTr("All fields"); key: "GENERAL" }
                    ListElement { label: qsTr("Address"); key: "ADDRESS" }
                    ListElement { label: qsTr("IČO"); key: "ICO" }
                    ListElement { label: qsTr("Data-Box ID"); key: "DBID" }
                }
                onCurrentIndexChanged: searchType = currentKey()
            }
            AccessibleTextField {
                id: searchPhraseText
                placeholderText: qsTr("Enter sought phrase")
                focus: true
                width: parent.width
                onAccepted: searchPanel.doSearchRequest()
            }
            Item {
                id: searchResults
                height: baseDelegateHeight * 0.6
                width: parent.width
                visible: false
                Item {
                    id: searchResultPrevious
                    anchors.top: parent.top
                    anchors.bottom: parent.bottom
                    anchors.left: parent.left
                    width: searchResults.height
                    AccessibleTextButton {
                        anchors.fill: parent
                        horizontalAlignment: Text.AlignHCenter
                        verticalAlignment: Text.AlignVCenter
                        color: (page > 0) ? datovkaPalette.text : datovkaPalette.greyText
                        font.bold: true
                        text: "<"
                        accessibleName: qsTr("Previous")
                        onClicked: {
                            if (page > 0) {
                                page = page-1
                            }
                            if (searchTextTmp.length > 0) {
                                showBusyIndicator(true)
                                isds.findDataboxFulltext(acntId, foundBoxModel, searchTextTmp, searchTypeTmp, searchScopeTmp, page)
                            }
                            if (recipBoxModel != null) {
                                foundBoxModel.selectEntries(recipBoxModel.boxIds(), true)
                            }
                        }
                    }
                }
                AccessibleTextInfo {
                    id: searchResultText
                    anchors.centerIn: parent
                    horizontalAlignment: Text.AlignHCenter
                }
                Item {
                    id: searchResultNext
                    anchors.top: parent.top
                    anchors.bottom: parent.bottom
                    anchors.right: parent.right
                    width: searchResults.height
                    AccessibleTextButton {
                        anchors.fill: parent
                        horizontalAlignment: Text.AlignHCenter
                        verticalAlignment: Text.AlignVCenter
                        font.bold: true
                        text: ">"
                        accessibleName: qsTr("Next")
                        onClicked: {
                            page = page+1
                            if (searchTextTmp.length > 0) {
                                showBusyIndicator(true)
                                isds.findDataboxFulltext(acntId, foundBoxModel, searchTextTmp, searchTypeTmp, searchScopeTmp, page)
                            }
                            if (recipBoxModel != null) {
                                foundBoxModel.selectEntries(recipBoxModel.boxIds(), true)
                            }
                        }
                    }
                }
            }
        }
    } // Pane

    ControlGroupItem {
        id: busyElementSection
        visible: false
        anchors.verticalCenter: parent.verticalCenter
        BusyIndicator {
            id: busyIndicator
            anchors.horizontalCenter: parent.horizontalCenter
            running: false
        }
        AccessibleTextInfo {
            horizontalAlignment: Text.AlignHCenter
            text: qsTr("Searching for data boxes ...")
        }
    }

    DataboxList {
        id: databoxList
        anchors.top: searchPanel.bottom
        anchors.bottom: parent.bottom
        visible: true
        width: parent.width
        model: proxyDataboxModel
        canDetailBoxes: recipBoxModel == null
        canSelectBoxes: recipBoxModel != null
        canDeselectBoxes: recipBoxModel != null
        onBoxSelect: {
            if (recipBoxModel != null) {
                var boxEntry = foundBoxModel.entry(boxId)
                foundBoxModel.selectEntry(boxEntry.dbID, true)
                isds.addRecipient(acntId, boxEntry.dbID, boxEntry.dbName, boxEntry.dbAddress, false, recipBoxModel)
                appendSelectedDb.visible = recipBoxModel.rowCount() > 0
            }
        }
        onBoxDeselect: {
            if (recipBoxModel != null) {
                foundBoxModel.selectEntry(boxId, false)
                recipBoxModel.removeEntry(boxId)
                appendSelectedDb.visible = recipBoxModel.rowCount() > 0
            }
        }
        onBoxDetail: {
            var boxEntry = foundBoxModel.entry(boxId)
            if (recipBoxModel == null) {
                Qt.inputMethod.hide()
                pageView.push(pageDataboxDetail, {
                    "pageView": pageView,
                    "acntId": acntId,
                    "dbID": boxEntry.dbID,
                    "dbType": boxEntry.dbType
                }, StackView.Immediate)
            }
        }
    } // DataboxList

    AccessibleRoundButton {
        id: appendSelectedDb
        z: 1
        visible: false
        anchors.right: parent.right
        anchors.rightMargin: defaultMargin
        anchors.bottom: parent.bottom
        anchors.bottomMargin: defaultMargin
        text: qsTr("Append Selected")
        icon.source: "qrc:/ui/account-plus.svg"
        accessibleName: qsTr("Append selected data boxes into recipient list.")
        onClicked: {
            pageView.pop(StackView.Immediate)
        }
    }

    AcntId {
        id: actionAcntId
    }
    Connections {
        target: isds
        function onLoginFailed(userName, testing) {
            if (acntId.username === userName && acntId.testing === testing) {
                showBusyIndicator(false)
            }
        }
        function onShowMessageBox(title, text, detailText) {
            showBusyIndicator(false)
            okMsgBox.error(title, text, detailText)
        }
        function onRunFindDataboxFulltextSig(userName, testing) {
            searchResults.visible = false
            searchResultPrevious.enabled = false
            searchResultNext.enabled = false
            searchResultText.text = ""
            page = 0
            searchTextTmp = searchPhraseText.text
            searchTypeTmp = searchType
            searchScopeTmp = searchScope
            actionAcntId.username = userName
            actionAcntId.testing = testing
            if (searchPhraseText.text.length > 0) {
                isds.findDataboxFulltext(actionAcntId, foundBoxModel, searchPhraseText.text, searchType, searchScope, page)
            }
            if (foundBoxModel.rowCount() <= 0) {
                filterButton.visible = false
            } else {
                filterButton.visible = true
            }
            if (recipBoxModel != null) {
                foundBoxModel.selectEntries(recipBoxModel.boxIds(), true)
            }
        }
        function onSendSearchResultsSig(totalCount, currentCount, position, lastPage) {
            searchResults.visible = true
            searchResultNext.visible = (totalCount > 0) && (totalCount > currentCount)
            searchResultPrevious.visible = (totalCount > 0) && (totalCount > currentCount)
            searchResultNext.enabled = !lastPage
            searchResultPrevious.enabled = (position > 0)
            if ((totalCount > 0) && (totalCount > currentCount)) {
                searchResultText.text = qsTr("Shown") + ": " + (position + 1) + "-" + (position + currentCount) + " " + qsTr("from") + " " + totalCount
            } else {
                searchResultText.text = qsTr("Found") + ": " + totalCount
            }
            showBusyIndicator(false)
        }
    }
} // Page
