/*
 * Copyright (C) 2014-2025 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

import QtQml 2.2
import QtQuick 2.7
import QtQuick.Controls 2.15
import cz.nic.mobileDatovka 1.0
import cz.nic.mobileDatovka.models 1.0
import cz.nic.mobileDatovka.qmlIdentifiers 1.0

Page {

    /* These properties must be set by caller. */
    property var pageView
    property string acntName
    property AcntId acntId: null
    property int msgType
    property string msgId
    property var messageModel: null

    Component.onCompleted: {
        uploadHierarchyProxyModel.setSourceModel(uploadHierarchyListModel)
        uploadHierarchyProxyModel.sortContent()
        timer.start()
    }

    function downloadUploadHierarchy() {
        hierarchyButton.enabled = false
        recordsManagement.callUploadHierarchy(uploadHierarchyListModel)
        hierarchyButton.enabled = true
    }

    Timer {
        id: timer
        interval: 500 // milliseconds
        running: false
        repeat: false
        onTriggered: {
            downloadUploadHierarchy()
        }
    }

    FontMetrics {
        id: fontMetrics
        font: selectedLocations.text
    }

    function viewSelectedNodeNames() {
        var nameList = uploadHierarchyListModel.selectedFullNames(true)
        selectedLocations.text = nameList.join("\n")

        var vertPos = 0.0
        if (selectedLocationsView.contentHeight > selectedLocationsView.height) {
            /* Navigate to bottom. */
            vertPos = 1.0 - (selectedLocationsView.height / selectedLocationsView.contentHeight);
        }
        selectedLocationsView.ScrollBar.vertical.position = vertPos
    }

    UploadHierarchyListModel {
        id: uploadHierarchyListModel
        onDataChanged: {
            uploadButton.enabled = (uploadHierarchyListModel.selectedIds().length > 0)
            viewSelectedNodeNames()
        }
        onModelReset: {
            uploadButton.enabled = (uploadHierarchyListModel.selectedIds().length > 0)
            uploadHierarchyProxyModel.sortContent()
            locationLabel.text = uploadHierarchyListModel.navigatedRootName(true, "/")
            viewSelectedNodeNames()
        }
        Component.onCompleted: {
        }
    }

    UploadHierarchyQmlProxyModel {
        id: uploadHierarchyProxyModel
        Component.onCompleted: {
            setFilterRole(UploadHierarchyListModel.ROLE_FILTER_DATA_RECURSIVE)
        }
    }

    header: PageHeader {
        id: headerBar
        title: qsTr("Upload Message %1").arg(msgId)
        onBackClicked: {
            pageView.pop(StackView.Immediate)
        }
        AccessibleToolButton {
            id: searchButton
            anchors.right: parent.right
            icon.source: "qrc:/ui/magnify.svg"
            accessibleName: qsTr("Filter upload hierarchy.")
            onClicked: {
                filterBar.open()
                filterBar.filterField.forceActiveFocus()
                Qt.inputMethod.show()
            }
        }
    }

    FilterBar {
        id: filterBar
        y: header.height
        onTextChanged: text => uploadHierarchyProxyModel.setFilterRegExpStr(text)
        onClearClicked: close()
    }

    Pane {
        id: controlPane
        anchors.top: parent.top
        anchors.left: parent.left
        anchors.right: parent.right
        ControlGroupItem {
            spacing: formItemVerticalSpacing
            AccessibleTextInfo {
                horizontalAlignment: Text.AlignHCenter
                text: qsTr("The message can be uploaded into selected locations in the records management hierarchy.")
            }
            Row {
                id: buttonRow
                spacing: formItemVerticalSpacing * 2
                anchors.horizontalCenter: parent.horizontalCenter
                AccessibleButton {
                    id: hierarchyButton
                    accessibleName: qsTr("Update upload hierarchy")
                    icon.source: "qrc:/ui/sync.svg"
                    text: qsTr("Update")
                    onClicked: {
                        downloadUploadHierarchy()
                    }
                }
                AccessibleButton {
                    id: uploadButton
                    enabled: false
                    accessibleName: qsTr("Upload message")
                    icon.source: "qrc:/ui/upload.svg"
                    text: qsTr("Upload")
                    onClicked: {
                        if (recordsManagement.uploadMessage(acntId, msgId, msgType, uploadHierarchyListModel)) {
                            messages.updateRmStatus(messageModel, acntId, msgId, msgType, true)
                            pageView.pop(StackView.Immediate)
                        }
                    }
                }
            }
            ScrollView {
                id: selectedLocationsView
                anchors.left: parent.left
                anchors.right: parent.right
                clip: true
                height: fontMetrics.height * 5
                ScrollBar.horizontal.policy: ScrollBar.AsNeeded
                ScrollBar.vertical.policy: ScrollBar.AsNeeded
                AccessibleText {
                    id: selectedLocations
                }
            }
            Row {
                id: navigateUpRow
                spacing: formItemVerticalSpacing
                anchors.left: parent.left
                anchors.right: parent.right
                AccessibleButton {
                    id: upButton
                    anchors.verticalCenter: parent.verticalCenter
                    text: "<"
                    accessibleName: qsTr("Up")
                    onClicked: uploadHierarchyListModel.navigateSuper()
                }
                AccessibleText {
                    id: locationLabel
                    anchors.verticalCenter: parent.verticalCenter
                    text: "/"
                }
            }
        }
    }
    Item {
        anchors.top: controlPane.bottom
        anchors.bottom: parent.bottom
        width: parent.width
        AccessibleTextInfo {
            id: emptyHierarchy
            visible: uploadHierarchyList.count <= 0
            anchors.centerIn: parent
            horizontalAlignment: Text.AlignHCenter
            text: (filterBar.filterField.displayText.length === 0) ?
                qsTr("Upload hierarchy has not been downloaded yet.") :
                qsTr("No hierarchy entry found that matches filter text '%1'.").arg(filterBar.filterField.displayText)
        }
        ScrollableListView {
            id: uploadHierarchyList
            delegateHeight: baseDelegateHeight
            anchors.top: parent.top
            anchors.bottom: parent.bottom
            width: parent.width
            model: uploadHierarchyProxyModel
            delegate: Rectangle {
                id: uploadItem
                color: (rSelected) ? datovkaPalette.selected : "transparent"
                height: uploadHierarchyList.delegateHeight
                width: uploadHierarchyList.width
                SeparatorLine {
                    visible: (0 === index)
                    anchors.top: parent.top
                }
                Text {
                    anchors.verticalCenter: parent.verticalCenter
                    anchors.left: parent.left
                    anchors.leftMargin: defaultMargin
                    width: parent.width - defaultMargin - nextNavElement.width
                    elide: Text.ElideRight
                    color: (rSelected || (!rSelectedRecursive)) ? datovkaPalette.text : datovkaPalette.greyText
                    text: rName
                }
                Image {
                    id: nextNavElement
                    visible: !rLeaf
                    anchors.verticalCenter: parent.verticalCenter
                    anchors.right: parent.right
                    anchors.rightMargin: defaultMargin
                    sourceSize.height: baseDelegateHeight * 0.3
                    source: getImgThemeFilePath("next.svg")
                }
                SeparatorLine {
                    anchors.bottom: parent.bottom
                }
                MouseArea {
                    function handleClick() {
                        if (!rLeaf) {
                            uploadHierarchyListModel.navigateSub(
                                uploadHierarchyProxyModel.mapToSourceContent(index))
                        } else if (rSelectable) {
                            uploadHierarchyListModel.toggleNodeSelection(
                                uploadHierarchyProxyModel.mapToSourceContent(index))
                        }
                    }
                    anchors.fill: parent
                    Accessible.role: Accessible.Button
                    Accessible.name: {
                        if (!rLeaf) {
                            qsTr("View content of %1.").arg(rName)
                        } else if (rSelectable) {
                            (!rSelected) ? qsTr("Select %1.").arg(rName) : qsTr("Deselect %1.").arg(rName)
                        } else {
                            ""
                        }
                    }
                    Accessible.onScrollDownAction: uploadHierarchyList.scrollDown()
                    Accessible.onScrollUpAction: uploadHierarchyList.scrollUp()
                    Accessible.onPressAction: {
                        handleClick()
                    }
                    onClicked: {
                        handleClick()
                    }
                }
            }
        }
    }
    Connections {
        target: recordsManagement
        function onShowMessageBox(title, text, detailText) {
            okMsgBox.error(title, text, detailText)
        }
    }
}
