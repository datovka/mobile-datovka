/*
 * Copyright (C) 2014-2025 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

import QtQuick 2.7
import QtQuick.Controls 2.15
import cz.nic.mobileDatovka 1.0

Page {

    header: PageHeader {
        id: headerBar
        title: qsTr("About Datovka")
        onBackClicked: {
            pageView.pop()
        }
    }

    SwipeView {
        id: swipeView
        anchors.fill: parent
        currentIndex: tabBar.currentIndex
        // Tab About
        Flickable {
            id: tabAbout
            contentHeight: aboutContent.implicitHeight
            Pane {
                id: aboutContent
                anchors.fill: parent
                ControlGroupItem {
                    spacing: formItemVerticalSpacing * 1.5
                    AccessibleImageButton {
                        anchors.horizontalCenter: parent.horizontalCenter
                        source: "qrc:/datovka.png"
                        accessibleName: qsTr("Open application home page.")
                        onClicked: {
                            Qt.openUrlExternally("http://www.datovka.cz/")
                        }
                    }
                    AccessibleText {
                        anchors.horizontalCenter: parent.horizontalCenter
                        width: parent.width
                        font.bold: true
                        horizontalAlignment: Text.AlignHCenter
                        wrapMode: Text.WordWrap
                        text: qsTr("Datovka") + " - " + qsTr("Free mobile data-box client.")
                    }
                    AccessibleText {
                        anchors.horizontalCenter: parent.horizontalCenter
                        width: parent.width
                        font.bold: true
                        horizontalAlignment: Text.AlignHCenter
                        wrapMode: Text.WordWrap
                        text: qsTr("Version: %1").arg(settings.appVersion())
                    }
                    AccessibleTextInfo {
                        anchors.horizontalCenter: parent.horizontalCenter
                        horizontalAlignment: Text.AlignHCenter
                        textFormat: TextEdit.RichText
                        onLinkActivated: Qt.openUrlExternally(link)
                        text: "<style>a:link{color:" + datovkaPalette.highlightedText + ";}</style>"
                            + qsTr("This application is being developed by %1 as open source.").arg("<a href=\"http://www.nic.cz/\">CZ.NIC</a>")
                            + " "
                            + qsTr("It's distributed free of charge. It doesn't contain advertisements or in-app purchases.")
                            + "<br/><br/>"
                            + qsTr("We want to provide an easy-to-use data-box client.")
                            + " "
                            + qsTr("Do you want to support us in this effort?")
                            + "<br/>"
                            + "<a href=\"%1\">%2</a>".arg("https://datovka.nic.cz/redirect/donation-mobile.html").arg(qsTr("Make a donation."))
                    }
                    AccessibleTextInfo {
                        horizontalAlignment: Text.AlignHCenter
                        textFormat: TextEdit.RichText
                        onLinkActivated: Qt.openUrlExternally(link)
                        text: "<style>a:link{color:" + datovkaPalette.highlightedText + ";}</style>"
                            + qsTr("The behaviour of the ISDS environment and this application is different from the behaviour of an e-mail client. Delivery of data messages is governed by law.")
                            + " "
                            + qsTr("Read the <a href=\"%1\">user manual</a> for more information about the application behaviour.").arg("https://datovka.nic.cz/redirect/mobile-manual-ver-2.html")
                    }
                    AccessibleTextInfo {
                        anchors.horizontalCenter: parent.horizontalCenter
                        horizontalAlignment: Text.AlignHCenter
                        textFormat: TextEdit.RichText
                        onLinkActivated: Qt.openUrlExternally(link)
                        text: "<style>a:link{color:" + datovkaPalette.highlightedText + ";}</style>"
                            + qsTr("Did you know that there is a desktop version of Datovka?")
                            + "<br/>"
                            + "<a href=\"%1\">%2</a>".arg("https://www.datovka.cz/").arg(qsTr("Learn more..."))
                    }
                } // Column
            } // Pane
        } // Flickable

        // Tab development and support
        Flickable {
            id: tabDevel
            contentHeight: develContent.implicitHeight
            Pane {
                id: develContent
                anchors.fill: parent
                ControlGroupItem {
                    spacing: formItemVerticalSpacing * 2
                    AccessibleText {
                        anchors.horizontalCenter: parent.horizontalCenter
                        font.bold: true
                        text: qsTr("Powered by")
                    }
                    AccessibleImageButton {
                        anchors.horizontalCenter: parent.horizontalCenter
                        source: darkMode ? "qrc:/cznicneg.png" : "qrc:/cznic.png"
                        accessibleName: qsTr("Open the home page of the CZ.NIC association.")
                        onClicked: {
                            Qt.openUrlExternally("http://www.nic.cz/")
                        }
                    }
                    AccessibleTextInfo {
                        anchors.horizontalCenter: parent.horizontalCenter
                        horizontalAlignment: Text.AlignHCenter
                        textFormat: TextEdit.RichText
                        onLinkActivated: Qt.openUrlExternally(link)
                        text: "<style>a:link{color:" + datovkaPalette.highlightedText + ";}</style>"
                            + qsTr("This application is being developed by %1 as open source." ).arg("<a href=\"http://www.nic.cz/\">CZ.NIC, z. s. p. o.</a>")
                    }
                    AccessibleTextInfo {
                        anchors.horizontalCenter: parent.horizontalCenter
                        horizontalAlignment: Text.AlignHCenter
                        textFormat: TextEdit.RichText
                        onLinkActivated: Qt.openUrlExternally(link)
                        text: "<style>a:link{color:" + datovkaPalette.highlightedText + ";}</style>"
                            + qsTr("If you have any problems with the application or you have found any errors or you just have an idea how to improve this application please contact us using the following e-mail address:")
                            + "<br/>"
                            + "<a href=\"mailto:datovka@labs.nic.cz?Subject=[Mobile Datovka%20" + settings.appVersion() + "]\">datovka@labs.nic.cz</a>"
                    }
                    AccessibleButton {
                        anchors.horizontalCenter: parent.horizontalCenter
                        text: qsTr("Show News")
                        accessibleName: qsTr("What's new?")
                        onClicked: changeLogBox.showChangeLog()
                    }
                } // Column
            } // Pane
        } // Flickable

        // Tab Licence
        Flickable {
            id: tabLicense
            contentHeight: licenseContent.implicitHeight
            Pane {
                id: licenseContent
                anchors.fill: parent
                ControlGroupItem {
                    spacing: formItemVerticalSpacing * 2
                    AccessibleTextInfo {
                        anchors.horizontalCenter: parent.horizontalCenter
                        horizontalAlignment: Text.AlignHCenter
                        textFormat: TextEdit.RichText
                        text: "<br/>"
                            + qsTr("This application is available as it is. Usage of this application is at own risk. The association CZ.NIC is in no circumstances liable for any damage directly or indirectly caused by using or not using this application.")
                            + "<br/><br/>"
                            + qsTr("CZ.NIC isn't the operator nor the administrator of the Data Box Information System (ISDS). The operation of the ISDS is regulated by Czech law and regulations.")
                            + "<br/>"
                    }
                    AccessibleTextInfo {
                        anchors.horizontalCenter: parent.horizontalCenter
                        horizontalAlignment: Text.AlignHCenter
                        textFormat: TextEdit.RichText
                        onLinkActivated: Qt.openUrlExternally(link)
                        text: "<style>a:link{color:"+datovkaPalette.highlightedText+";}</style>"
                            + qsTr("This software is distributed under the %1 licence.").arg("<a href=\"%1\">%2</a>".arg("https://www.gnu.org/licenses/gpl-3.0-standalone.html").arg(qsTr("GNU GPL version 3")))
                    }
                } // Column
            } // Pane
        } // Flickable
    }

    PageIndicator {
        currentIndex: tabBar.currentIndex
        count: tabBar.count
        anchors.bottom: parent.bottom
        anchors.bottomMargin: defaultMargin
        anchors.horizontalCenter: parent.horizontalCenter
    }

    footer: TabBar {
        id: tabBar
        currentIndex: swipeView.currentIndex
        property int buttonWidth: Math.max(100, tabBar.width / 3)
        TabButton {
            width: tabBar.buttonWidth
            Accessible.role: Accessible.Button
            Accessible.name: qsTr("About Datovka.")
            text: qsTr("About")
        }
        TabButton {
            width: tabBar.buttonWidth
            Accessible.role: Accessible.Button
            Accessible.name: qsTr("Datovka development.")
            text: qsTr("Development")
        }
        TabButton {
            width: tabBar.buttonWidth
            Accessible.role: Accessible.Button
            Accessible.name: qsTr("Licence.")
            text: qsTr("Licence")
        }
    } // TabBar
}
