/*
 * Copyright (C) 2014-2025 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

import QtQuick 2.15
import QtQuick.Controls 2.15
import cz.nic.mobileDatovka 1.0
import cz.nic.mobileDatovka.messages 1.0
import cz.nic.mobileDatovka.models 1.0
import cz.nic.mobileDatovka.qmlIdentifiers 1.0

/*
 * Roles are defined in MessageListModel::roleNames() and are accessed directly
 * via their names.
 */
Page {
    id: msgListPage

    property alias msgList: messageList

    /* These properties must be set by caller. */
    property var pageView
    property string acntName
    property AcntId acntId: null
    property int msgType
    property bool isMainPage: false
    property var messageModel: null

    property string messageId: ""

   function disabledPageComponents(disable) {
        headerBar.enabled = !disable
        messageList.enabled = headerBar.enabled
        syncButtonRotation.running = disable
    }

    function doIsdsAction(action, acntId) {
        disabledPageComponents(true)
        isds.doIsdsAction(action, acntId)
    }

    Component.onCompleted: {
        messages.fillMessageList(messageModel, acntId, msgType)
        proxyMessageModel.setSourceModel(messageModel)
    }

    MessageBox {
        id: yesNoMsgBox
        onAcceptAndClosed: {
            messages.deleteMessageFromDbs(accountModel, messageModel, acntId, messageId)
            messages.fillMessageList(messageModel, acntId, msgType)
        }
    }

    ListSortFilterProxyModel {
        id: proxyMessageModel
        Component.onCompleted: {
            setFilterRoles([MessageListModel.ROLE_MSG_ID,
                MessageListModel.ROLE_FROM, MessageListModel.ROLE_TO,
                MessageListModel.ROLE_ANNOTATION])
        }
    }

    PopupMenuMessageList {
        id: popupMenuMessageList
    }

    PopupMenuMessage {
        id: popupMenuMessage
    }

    header: PageHeader {
        id: headerBar
        isFirstPage: isMainPage
        title: (msgType == MessageType.TYPE_RECEIVED) ? qsTr("Received") : qsTr("Sent");
        onBackClicked: {
            pageView.pop()
        }
        AccessibleToolButton {
            id: actionButton
            anchors.right: parent.right
            visible: ((msgType == MessageType.TYPE_RECEIVED) && (messageList.count > 0))
            icon.source: "qrc:/ui/android-more-vertical.svg"
            accessibleName: qsTr("Show menu of available operations.")
            onClicked: popupMenuMessageList.open()
        }
        AccessibleToolButton {
            id: searchButton
            anchors.right: actionButton.visible ? actionButton.left : parent.right
            icon.source: "qrc:/ui/magnify.svg"
            accessibleName: qsTr("Filter messages.")
            onClicked: {
                filterBar.open()
                filterBar.filterField.forceActiveFocus()
                Qt.inputMethod.show()
            }
        }
        AccessibleToolButton {
            id: syncButton
            anchors.right: searchButton.visible ? searchButton.left : parent.right
            icon.source: "qrc:/ui/sync.svg"
            accessibleName: (msgType == MessageType.TYPE_RECEIVED) ? qsTr("Synchronise received.") : qsTr("Synchronise sent.")
            onClicked: {
                if (msgType == MessageType.TYPE_RECEIVED) {
                    doIsdsAction("syncSingleAccountReceived", acntId)
                } else if (msgType == MessageType.TYPE_SENT) {
                    doIsdsAction("syncSingleAccountSent", acntId)
                }
            }
            RotationAnimator {
                id: syncButtonRotation
                target: syncButton
                alwaysRunToEnd:  true
                from: 0;
                to: 360;
                duration: 2000
                loops: Animation.Infinite
                running: false
            }
        }
    }

    FilterBar {
        id: filterBar
        onTextChanged: text => proxyMessageModel.setFilterRegExpStr(text)
        onClearClicked: close()
    }

    MessageList {
        id: messageList
        anchors.fill: parent
        visible: true
        model: proxyMessageModel
        onMsgClicked: function onMsgClicked(acntId, msgType, msgId, msgDaysToDeletion) {
            Qt.inputMethod.hide()
            messages.markMessageAsLocallyRead(messageModel, acntId, msgId, true)
            pageView.push(pageMessageDetail, {
                    "pageView": pageView,
                    "fromLocalDb": true,
                    "acntName": acntName,
                    "acntId": acntId,
                    "msgDaysToDeletion": msgDaysToDeletion,
                    "msgType": msgType,
                    "msgId": msgId,
                    "messageModel": messageModel
                })
        }
        onMsgPressAndHold: function onMsgPressAndHold(acntId, msgType, msgId, canDelete) {
            Qt.inputMethod.hide()
            /* Use message settings for received message only */
            if (msgType === MessageType.TYPE_RECEIVED) {
                popupMenuMessage.msgType = msgType
                popupMenuMessage.msgId = msgId
                popupMenuMessage.canDelete = canDelete
                popupMenuMessage.open()
            } else if (msgType === MessageType.TYPE_SENT && canDelete) {
                messageId = msgId
                yesNoMsgBox.question(qsTr("Delete Message: %1").arg(msgId),
                   qsTr("Do you want to delete the sent message '%1'?").arg(msgId),
                   qsTr("Note: It will delete all attachments and message information from the local database."))
            }
        }
        property bool downloadStart: false
        Timer {
            /*
             * The MEP dialogue is not shown when the movement animation
             * is active. This timer ensures that the login procedure starts
             * with a delay.
             * TODO -- Find a better solution.
             */
            id: syncTimer
            interval: 500
            running: false
            repeat: false
            onTriggered: {
                if (msgType == MessageType.TYPE_RECEIVED) {
                    doIsdsAction("syncSingleAccountReceived", acntId)
                } else if (msgType == MessageType.TYPE_SENT) {
                    doIsdsAction("syncSingleAccountSent", acntId)
                }
            }
        }
        onMovementEnded: {
            if (downloadStart) {
                downloadStart = false
                syncTimer.start()
            }
        }
        onDragEnded: {
            downloadStart = contentY < -120
        }
    }

    ControlGroupItem {
        id: downloadMsgButton
        anchors.centerIn: parent
        visible: (messageList.count <= 0) && (filterBar.filterField.displayText.length <= 0)
        z: 1
        AccessibleButton {
            z: 2
            icon.source: "qrc:/ui/sync.svg"
            text: (msgType == MessageType.TYPE_RECEIVED) ?  qsTr("Synchronise Received") : qsTr("Synchronise Sent")
            accessibleName: (msgType == MessageType.TYPE_RECEIVED) ?  qsTr("Synchronise received.") : qsTr("Synchronise sent.")
            onClicked: {
                if (msgType == MessageType.TYPE_RECEIVED) {
                    doIsdsAction("syncSingleAccountReceived", acntId)
                } else if (msgType == MessageType.TYPE_SENT) {
                    doIsdsAction("syncSingleAccountSent", acntId)
                }
            }
        }
        AccessibleTextInfo {
            horizontalAlignment: Text.AlignHCenter
            text: qsTr("No %1 messages available or they haven't been downloaded yet.").arg((msgType == MessageType.TYPE_RECEIVED) ? qsTr("received") : qsTr("sent"))
        }
    }

    AccessibleTextInfo {
        visible: (messageList.count === 0) && (filterBar.filterField.displayText.length > 0)
        anchors.fill: parent
        anchors.margins: defaultMargin
        horizontalAlignment: Text.AlignHCenter
        verticalAlignment: Text.AlignVCenter
        wrapMode: Text.Wrap
        text: qsTr("No message found that matches filter text '%1'.").arg(filterBar.filterField.displayText)
    }

    Connections {
        target: isds
        function onLoginFailed(userName, testing) {
            if (acntId.username === userName && acntId.testing === testing) {
                disabledPageComponents(false)
            }
        }
        function onDownloadMessageListFinishedSig(isMsgReceived, userName, testing, success, errTxt) {
            if (msgType == MessageType.TYPE_RECEIVED && isMsgReceived) {
                messages.fillMessageList(messageModel, acntId, msgType)
            } else if (msgType == MessageType.TYPE_SENT && !isMsgReceived) {
                messages.fillMessageList(messageModel, acntId, msgType)
            }
            accounts.updateLastSyncTime(acntId)
            disabledPageComponents(false)
            if (!success) {
                okMsgBox.error(qsTr("%1: messages").arg(userName),
                    qsTr("Failed to download list of messages for user name '%1'.").arg(username),
                    errTxt)
            }
        }
    }
}
