/*
 * Copyright (C) 2014-2025 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

import QtQuick 2.15
import QtQuick.Controls 2.15
import QtQuick.Layouts 1.3
import cz.nic.mobileDatovka 1.0
import cz.nic.mobileDatovka.models 1.0

Page {

    Popup {
        id: inputPrefDlg
        anchors.centerIn: parent
        modal: true
        closePolicy: Popup.NoAutoClose

        property alias inputTextField: prefValueString
        property int preferredMinWidth: 260
        property int prefIndex: 0

        property int spinBoxMinInt: -2147483648
        property int spinBoxMaxInt: 2147483647

        function openInputDialog(index, name, value, status) {
            prefIndex = index
            prefName.text = name
            resetButton.visible = (status === PrefListModel.STAT_MODIFIED)
            if (mainPrefListModel.type(prefIndex) === PrefListModel.VAL_INTEGER) {
                prefValueString.visible = false
                prefValueInt.from = spinBoxMinInt
                prefValueInt.to = spinBoxMaxInt
                prefValueInt.setVal(value)
                prefValueInt.visible = true
            } else {
                /* ValueType.Others */
                prefValueInt.visible = false
                prefValueString.text = value
                prefValueString.visible = true
            }
            inputPrefDlg.open()
            inputPrefDlg.forceActiveFocus()
        }

        signal acceptAndClosed()
        signal resetAndClosed()

        background: Rectangle {
            color: datovkaPalette.base
            border.color: datovkaPalette.text
            radius: defaultMargin
        }

        ColumnLayout {
            spacing: defaultMargin * 2
            AccessibleText {
                id: titleText
                font.bold: true
                Layout.alignment: Qt.AlignCenter
                text: qsTr("Enter a Value")
            }
            AccessibleText {
                id: prefName
                Layout.preferredWidth: inputPrefDlg.preferredMinWidth
                horizontalAlignment : Text.Center
                wrapMode: Text.Wrap
            }
            AccessibleTextField {
                id: prefValueString
                Layout.fillWidth: true
                Layout.alignment: Qt.AlignLeft | Qt.AlignBaseline
                echoMode: TextInput.Normal
                horizontalAlignment: TextInput.AlignHCenter
            }
            AccessibleSpinBox {
                id: prefValueInt
                Layout.fillWidth: true
                Layout.alignment: Qt.AlignLeft | Qt.AlignBaseline
            }
            RowLayout {
                Layout.alignment: Qt.AlignCenter
                spacing: defaultMargin * 2
                AccessibleButton {
                    Layout.alignment: Qt.AlignCenter
                    text: qsTr("Close")
                    onClicked: inputPrefDlg.close()
                }
                AccessibleButton {
                    id: resetButton
                    Layout.alignment: Qt.AlignCenter
                    text: qsTr("Reset")
                    onClicked: {
                        inputPrefDlg.close()
                        inputPrefDlg.resetAndClosed()
                    }
                }
                AccessibleButton {
                    Layout.alignment: Qt.AlignCenter
                    text: qsTr("Save")
                    onClicked: {
                        inputPrefDlg.close()
                        inputPrefDlg.acceptAndClosed()
                    }
                }
            }
        }

        onAcceptAndClosed: function saveEntry() {
            if (mainPrefListModel.type(prefIndex) === PrefListModel.VAL_INTEGER) {
                mainPrefListModel.modifyInt(prefIndex, prefValueInt.value)
            } else {
                mainPrefListModel.modifyValue(prefIndex, prefValueString.text)
            }
        }
        onResetAndClosed: function resetEntry() {
            mainPrefListModel.resetEntry(prefIndex)
        }
    }

    Component.onCompleted: {
        proxyPrefsModel.setSourceModel(mainPrefListModel)
        proxyPrefsModel.sortContent(ListSortFilterProxyModel.ASCENDING_ORDER)
    }

    ListSortFilterProxyModel {
        id: proxyPrefsModel
        Component.onCompleted: {
            setFilterRoles([PrefListModel.ROLE_PREF_NAME])
            sortRole = PrefListModel.ROLE_PREF_NAME
        }
    }

    header: PageHeader {
        id: headerBar
        isFirstPage: false
        title: qsTr("All Preferences")
        onBackClicked: {
            pageView.pop()
        }
        AccessibleToolButton {
            id: searchButton
            visible: false
            anchors.right: parent.right
            icon.source: "qrc:/ui/magnify.svg"
            accessibleName: qsTr("Filter the preferences.")
            onClicked: {
                filterBar.open()
                filterBar.filterField.forceActiveFocus()
                Qt.inputMethod.show()
            }
        }
    }

    FilterBar {
        id: filterBar
        onTextChanged: text => proxyPrefsModel.setFilterRegExpStr(text)
        onClearClicked: close()
    }

    Item {
        id: prefsGroup
        visible: false
        anchors.fill: parent
        ControlGroupItem {
            id: infoText
            anchors.top: parent.top
            anchors.topMargin: defaultMargin
            width: parent.width
            AccessibleTextInfo {
                horizontalAlignment: Text.AlignHCenter
                text: qsTr("Press and hold a list element to edit its value.")
            }
            SeparatorLine {}
        }
        ListView {
            id: prefsList
            anchors.top: infoText.bottom
            anchors.bottom: parent.bottom
            clip: true
            width: parent.width
            model: proxyPrefsModel
            delegate: Item {
                property bool textBold: (rPrefStatus === PrefListModel.STAT_MODIFIED)
                width: prefsList.width
                height: listViewDelegateHeight
                Column {
                    anchors.fill: parent
                    anchors.margins: defaultMargin
                    anchors.verticalCenter: parent.verticalCenter
                    spacing: defaultMargin
                    AccessibleText {
                        width: parent.width
                        font.bold: true
                        elide: Text.ElideMiddle
                        text: rPrefName
                    }
                    AccessibleText {
                        width: parent.width
                        font.bold: textBold
                        elide: Text.ElideMiddle
                        text: rPrefValue
                    }
                    Row {
                        width: parent.width
                        spacing: defaultMargin
                        AccessibleLabelDark {
                            font.bold: textBold
                            text: rPrefType
                        }
                        AccessibleLabelDark {
                            font.bold: textBold
                            text: mainPrefListModel.statusString(rPrefStatus)
                        }
                    }
                }
                SeparatorLine {
                    anchors.bottom: parent.bottom
                }
                MouseArea {
                    anchors.fill: parent
                    /*
                     * Intentionally not using onClicked to prevent unintentional modifications.
                     */
                    onPressAndHold: {
                        var pIndex = mainPrefListModel.findRow(rPrefName)
                        if (mainPrefListModel.type(pIndex) === PrefListModel.VAL_BOOLEAN) {
                            mainPrefListModel.alterBool(pIndex)
                        } else {
                            inputPrefDlg.openInputDialog(pIndex, rPrefName, rPrefValue, rPrefStatus)
                        }
                    }
                }
            }
        }
    }

    ControlGroupItem {
        id: riskInfo
        anchors.centerIn: parent
        visible: true
        z: 1
        AccessibleTextInfo {
            horizontalAlignment: Text.AlignHCenter
            text: qsTr("Changing these settings directly can be harmful to the stability, security and performance of this application. You should only continue if you are sure of what you are doing.")
        }
        AccessibleButton {
            z: 2
            icon.source: "qrc:/ui/forward.svg"
            text: qsTr("I accept the risk.")
            accessibleName: qsTr("Show all preferences.")
            onClicked: {
                riskInfo.visible = false
                prefsGroup.visible = !riskInfo.visible
                searchButton.visible = prefsGroup.visible
            }
        }
    }
}
