/*
 * Copyright (C) 2014-2025 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

import QtQuick 2.7
import QtQuick.Controls 2.15
import cz.nic.mobileDatovka 1.0
import cz.nic.mobileDatovka.account 1.0
import cz.nic.mobileDatovka.iOsHelper 1.0
import cz.nic.mobileDatovka.qmlIdentifiers 1.0

Page {

    Connections {
        target: acntLogos
        function onContentReset() {
            logoImage.reloadImage()
        }
        function onDataInserted(aId) {
            /* The strId property is compared because in compares strings. */
            if (aId.strId === acntId.strId) {
                logoImage.reloadImage()
            }
        }
        function onDataRemoved(aId) {
            if (aId.strId === acntId.strId) {
                logoImage.reloadImage()
            }
        }
        function onDataChanged(aId) {
            if (aId.strId === acntId.strId) {
                logoImage.reloadImage()
            }
        }
    }

    property string acntName
    property AcntId acntId

    function storeLogo(filePath) {
        if (accounts.setDataboxLogo(acntId, filePath)) {
            pageView.pop()
        }
    }

    function openFileDialog() {
        var title = qsTr("Choose an Image File")
        var filters = "(*.png *.jpg *.jpeg *.bmp *.svg *.gif)"
        if (settings.useQmlFileDialog()) {
            console.log("Use QML File Dialog")
            fileDialogue.openFileDialog(title, filters)
            return
        }
        if (Qt.platform.os == "ios") {
            if (settings.useIosDocumentPicker()) {
                console.log("Use Native Ios Document Picker")
                iOSHelper.openDocumentPickerControllerForImport(IosImportAction.IMPORT_SEND,[])
            } else {
                console.log("Use Qt Core File Dialog")
                storeLogo(files.openFileDialog(title, null, filters))
            }
        } else {
            console.log("Use Qt Core File Dialog")
            storeLogo(files.openFileDialog(title, null, filters))
        }
    }

    FileDialogue {
        id: fileDialogue
        onAccepted: {
            if (fileDialogue.files.length > 0) {
                storeLogo(fileDialogue.files[0])
            }
        }
        Connections {
            target: iOSHelper
            function onSendFilesSelectedSig(filePaths) {
                if (filePaths.length > 0) {
                    storeLogo(filePaths[0])
                }
            }
        }
    }

    header: PageHeader {
        title: qsTr("Data-Box Logo")
        onBackClicked: {
            pageView.pop()
        }
    }

    Flickable {
        anchors.fill: parent
        contentHeight: flickContent.implicitHeight
        Pane {
            id: flickContent
            anchors.fill: parent
            ControlGroupItem {
                spacing: formItemVerticalSpacing
                AccessibleTextInfo {
                    horizontalAlignment: Text.AlignHCenter
                    text: qsTr("Here you can change the default data-box logo to a custom image.")
                        + " "
                        + qsTr("The set logo will be used only locally. Logos aren't shared between application installations nor the ISDS.")
                        + "\n\n"
                        + qsTr("Allowed image formats are: %1").arg("PNG, JPG, SVG, BMP")
                        + "\n"
                        + qsTr("The recommended image size is %1x%1 pixels.").arg(128)
                }
                Image {
                    id: logoImage
                    function reloadImage() {
                        /* Refresh image content after source changing. */
                        var oldSource = accounts.databoxLogo(acntId)
                        source = "qrc:/ui/databox-regular.png"
                        source = oldSource
                    }
                    cache: false
                    source: accounts.databoxLogo(acntId)
                    anchors.horizontalCenter: parent.horizontalCenter
                    sourceSize.height: baseDelegateHeight * 2
                    sourceSize.width: baseDelegateHeight * 2
                }
                AccessibleButton {
                    anchors.horizontalCenter: parent.horizontalCenter
                    text: qsTr("Set New Logo")
                    onClicked: openFileDialog()
                }
                AccessibleButton {
                    anchors.horizontalCenter: parent.horizontalCenter
                    text: qsTr("Reset Logo")
                    onClicked: {
                        if (accounts.resetDataboxLogo(acntId)) {
                            pageView.pop()
                        }
                    }
                }
            } // Column
        } // Pane
        ScrollIndicator.vertical: ScrollIndicator {}
    } // Flickable
} // Page
