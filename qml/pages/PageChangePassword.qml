/*
 * Copyright (C) 2014-2025 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

import QtQuick 2.7
import QtQuick.Controls 2.15
import cz.nic.mobileDatovka 1.0
import cz.nic.mobileDatovka.account 1.0
import cz.nic.mobileDatovka.qmlIdentifiers 1.0

Page {

    /* These properties must be set by caller. */
    property var pageView
    property string acntName
    property AcntId acntId: null
    property var loginMethod
    property bool showPwd: false

    function changePwdSettings() {
            loginMethod = isds.getAccountLoginMethod(acntId)
            errLineText.visible = false
            actionButton.enabled = true
            loginButton.visible = false;
            if (loginMethod === AcntData.LIM_UNAME_PWD_TOTP) {
                topLineText.text = qsTr("User is logged in to ISDS.") + " " + qsTr("Enter the current password, twice the new password and the SMS code.")
                otpCode.placeholderText =  qsTr("Enter SMS code")
                otpCode.visible = true
                sendSmsButton.visible = true
                sendSmsButton.enabled = true
            } else if (loginMethod === AcntData.LIM_UNAME_PWD_HOTP) {
                topLineText.text = qsTr("User is logged in to ISDS.") + " " + qsTr("Enter the current password, twice the new password and the security code.")
                otpCode.placeholderText =  qsTr("Enter security code")
                otpCode.visible = true
                sendSmsButton.visible = false
            } else {
                topLineText.text = qsTr("User is logged in to ISDS.") + " " + qsTr("Enter the current password and twice the new password.")
                otpCode.visible = false
                sendSmsButton.visible = false
            }
    }

    function showHidePasswords(showPwd) {
        oldPwd.echoMode = showPwd ? TextInput.Normal : TextInput.Password
        newPwd.echoMode = oldPwd.echoMode
        newPwdAgain.echoMode = oldPwd.echoMode
    }

    function showHidePwdButton() {
        pwdButton.visible = (oldPwd.text.length > 0 || newPwd.text.length > 0 || newPwdAgain.text.length > 0)
    }

    Component.onCompleted: {
        if (!isds.isLogged(acntId)) {
            actionButton.enabled = false
            loginButton.visible = true;
            topLineText.text = qsTr("User must be logged in to the data box to be able to change the password.")
        } else {
            changePwdSettings()
        }
    }

    MessageBox {
        id: yesNoMsgBox
        onAcceptAndClosed: {
            if (isds.sendSMS(acntId, oldPwd.text.toString())) {
                errLineText.text = qsTr("SMS code has been sent...")
                errLineText.visible = true
                sendSmsButton.enabled = false
                sendSmsButton.visible = false
            } else {
                errLineText.text = qsTr("SMS code cannot be sent. Entered wrong current password or the ISDS server is out of order.")
                errLineText.visible = true
                sendSmsButton.enabled = true
                sendSmsButton.visible = true
            }
        }
    }

    header: PageHeader {
        id: headerBar
        title: qsTr("Change password")
        onBackClicked: {
            pageView.pop(StackView.Immediate)
        }
        AccessibleToolButton {
            id: pwdButton
            visible: false
            anchors.right: parent.right
            icon.source: showPwd ? "qrc:/ui/eye-pwd-hide.svg" : "qrc:/ui/eye-pwd-show.svg"
            accessibleName: showPwd ?  qsTr("Hide Passwords") : qsTr("Show Passwords")
            onClicked: {
                showPwd = !showPwd
                showHidePasswords(showPwd)
            }
        }
    }

    Flickable {
        anchors.fill: parent
        contentHeight: flickContent.implicitHeight
        Pane {
            id: flickContent
            anchors.fill: parent
            Column {
                anchors.right: parent.right
                anchors.left: parent.left
                spacing: formItemVerticalSpacing
                AccessibleTextInfo {
                    id: topLineText
                    horizontalAlignment: Text.AlignHCenter
                    text: qsTr("Enter the current password and the new password.")
                }
                AccessibleButton {
                    id: loginButton
                    visible: false
                    anchors.horizontalCenter: parent.horizontalCenter
                    text: qsTr("Log in Now")
                    icon.source: "qrc:/ui/send-msg.svg"
                    accessibleName: qsTr("Log in now into data box.")
                    onClicked: isds.doIsdsAction("changePassword", acntId)
                }
                Column {
                    anchors.horizontalCenter: parent.horizontalCenter
                    width: parent.width / 2
                    spacing: formItemVerticalSpacing
                    AccessibleTextField {
                        id: oldPwd
                        anchors.left: parent.left
                        anchors.right: parent.right
                        echoMode: TextInput.Password
                        passwordMaskDelay: 500 // milliseconds
                        inputMethodHints: Qt.ImhNone
                        placeholderText: qsTr("Current password")
                        horizontalAlignment: TextInput.AlignHCenter
                        onTextChanged: showHidePwdButton()
                    }
                    AccessibleTextField {
                        id: newPwd
                        anchors.left: parent.left
                        anchors.right: parent.right
                        echoMode: TextInput.Password
                        passwordMaskDelay: 500 // milliseconds
                        inputMethodHints: Qt.ImhNone
                        placeholderText: qsTr("New password")
                        horizontalAlignment: TextInput.AlignHCenter
                        onTextChanged: showHidePwdButton()
                    }
                    AccessibleTextField {
                        id: newPwdAgain
                        anchors.left: parent.left
                        anchors.right: parent.right
                        echoMode: TextInput.Password
                        passwordMaskDelay: 500 // milliseconds
                        inputMethodHints: Qt.ImhNone
                        placeholderText: qsTr("Confirm the new password")
                        horizontalAlignment: TextInput.AlignHCenter
                        onTextChanged: showHidePwdButton()
                    }
                    AccessibleButton {
                        id: sendSmsButton
                        visible: false
                        enabled: false
                        anchors.horizontalCenter: parent.horizontalCenter
                        text: qsTr("Send SMS Code")
                        onClicked: {
                            if (oldPwd.text === "") {
                                errLineText.text = qsTr("Enter the current password to be able to send the SMS code.")
                                errLineText.visible = true
                                sendSmsButton.enabled = true
                                sendSmsButton.visible = true
                            } else {
                                yesNoMsgBox.question(qsTr("SMS Code: %1").arg(acntId.username),
                                    qsTr("Data box '%1' requires a SMS code for password changing.").arg(acntId.username),
                                    qsTr("Do you want to send the SMS code now?"))
                            }
                        }
                    }
                    AccessibleTextField {
                        id: otpCode
                        visible: false
                        anchors.left: parent.left
                        anchors.right: parent.right
                        echoMode: TextInput.Normal
                        passwordMaskDelay: 500 // milliseconds
                        inputMethodHints: Qt.ImhPreferNumbers
                        placeholderText: qsTr("Enter OTP code")
                        horizontalAlignment: TextInput.AlignHCenter
                        text: ""
                    }
                }
                AccessibleButton {
                    id: actionButton
                    anchors.horizontalCenter: parent.horizontalCenter
                    text: qsTr("Accept Changes")
                    icon.source: "qrc:/ui/checkbox-marked-circle.svg"
                    accessibleName: qsTr("Accept all changes.")
                    onClicked: {
                        if (oldPwd.text === "" || newPwd.text === "") {
                            errLineText.text = qsTr("Old and new password must both be filled in.")
                            errLineText.visible = true
                            return
                        }
                        if (newPwd.text !== newPwdAgain.text) {
                            errLineText.text = qsTr("The new password fields don't match.")
                            errLineText.visible = true
                            return
                        }
                        if (!isds.isCorrectPassword(newPwd.text.toString())) {
                            errLineText.text = qsTr("Wrong password format. The new password must contain at least 8 characters including at least 1 number and at least 1 upper-case letter.")
                            errLineText.visible = true
                            return
                        }
                        if (loginMethod === AcntData.LIM_UNAME_PWD_TOTP || loginMethod === AcntData.LIM_UNAME_PWD_HOTP)  {
                            if (otpCode.text === "") {
                                errLineText.text = qsTr("Security/SMS code must be filled in.")
                                errLineText.visible = true
                                return
                            }
                        }
                        errLineText.visible = false
                        if (isds.changePassword(acntId, oldPwd.text.toString(), newPwd.text.toString(), otpCode.text.toString())) {
                            /* INI settings are saved automatically. */
                            okMsgBox.info(qsTr("Successful Password Change"),
                                qsTr("Password for username '%1' has been changed.").arg(acntId.username),
                                qsTr("The new password has been saved."))
                            pageView.pop(StackView.Immediate)
                        } else {
                            errLineText.text = qsTr("Password change failed.")
                            errLineText.visible = true
                        }
                    }
                }
                AccessibleTextInfo {
                    id: errLineText
                    visible: false
                    horizontalAlignment: Text.AlignHCenter
                    color: datovkaPalette.errorText
                    text: ""
                }
                AccessibleTextInfo {
                    horizontalAlignment: Text.AlignHCenter
                    text: qsTr("This will change the password on the ISDS server. You won't be able to log in to ISDS without the new password. If your current password has already expired then you must log into the ISDS web portal to change it.")
                }
                AccessibleTextInfo {
                    horizontalAlignment: Text.AlignHCenter
                    text: qsTr("If you just want to update the password because it has been changed elsewhere then go to the ") + qsTr("Data box settings")+ "."
                }
                AccessibleTextInfo {
                    horizontalAlignment: Text.AlignHCenter
                    text: qsTr("Keep a copy of your login credentials and store it in a safe place where only authorised persons have access to.")
                }
            } // Column layout
            Connections {
                target: isds
                function onRunChangePasswordSig() {
                    changePwdSettings()
                }
            }
        } // Pane
        ScrollIndicator.vertical: ScrollIndicator {}
    } // Flickable
} // Page
