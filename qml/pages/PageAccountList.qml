/*
 * Copyright (C) 2014-2025 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

import QtQuick 2.15
import QtQuick.Controls 2.15
import QtQuick.Layouts 1.3
import cz.nic.mobileDatovka 1.0
import cz.nic.mobileDatovka.messages 1.0
import cz.nic.mobileDatovka.models 1.0
import cz.nic.mobileDatovka.qmlIdentifiers 1.0

/*
 * Roles are defined in AccountListModel::roleNames() and are accessed directly
 * via their names.
 */

Page {
    id: appArea

    /* Threshold when the dialogue for banner closing will be shown. */
    property int bannerDlgVisibilityThreshold: 4

    /* Set visibility of page's elements.  */
    function setPageElementVisibility(acntCnt) {
        syncAllButton.visible = true
        syncAllButton.enabled = true
        syncAllButtonRotation.running = false
        if (acntCnt < 1) {
            syncAllButton.visible = false
            syncAllButton.enabled = false
        }
    }

    function syncAllAccounts() {
        disabledPageComponents(true)
        syncAllButtonRotation.running = true
        syncAllTimer.start()
        isds.syncAllAccounts()
    }

    function setSyncButtonRotation(userName, testing, isRunning) {
        for (var i = 0; i<accountList.count; i++) {
            if (!accountList.itemAtIndex(i)) {
                continue
            }
            if (accountList.itemAtIndex(i).acntId.username === userName &&
                accountList.itemAtIndex(i).acntId.testing === testing) {
                accountList.itemAtIndex(i).syncButtonRotation.running = isRunning
                accountList.itemAtIndex(i).syncLabelText.text = isRunning ? qsTr("Synchronising...") : qsTr("Last synchronisation") + ":"
                return
            }
        }
    }

   function disabledPageComponents(disable) {
        headerBar.enabled = !disable
        accountList.enabled = headerBar.enabled
    }

    function doIsdsAction(action, acntId) {
        disabledPageComponents(true)
        isds.doIsdsAction(action, acntId)
    }

    Component.onCompleted: {
        setPageElementVisibility(accountList.count)
    }

    /*
     * Timer enables sync button after 10s. It is temporary solution.
     * TODO - Better sync all accounts solution must be implemented
     *        in the future.
     */
    Timer {
        id: syncAllTimer
        interval: 10000 // milliseconds
        running: false
        repeat: false
        onTriggered: {
            disabledPageComponents(false)
            syncAllButtonRotation.running = false
        }
    }

    MessageBox {
        id: yesNoMsgBox
        property string userName: ""
        property bool testing: false
        AcntId {
            id: anctIdBox
        }
        onAcceptAndClosed: {
            anctIdBox.username = yesNoMsgBox.userName
            anctIdBox.testing = yesNoMsgBox.testing
            pageView.push(pageRepairDatabase, {
                "pageView": pageView,
                "acntId": anctIdBox
            }, StackView.Immediate)
        }
        onClosed: {
            disabledPageComponents(false)
        }
    }

    header: PageHeader {
        id: headerBar
        title: qsTr("Data Boxes")
        isFirstPage: true
        AccessibleToolButton {
            id: syncAllButton
            visible: false
            anchors.right: parent.right
            icon.source: "qrc:/ui/sync-all.svg"
            accessibleName: qsTr("Synchronise all data boxes.")
            onClicked: syncAllAccounts()
            RotationAnimator {
                id: syncAllButtonRotation
                target: syncAllButton
                alwaysRunToEnd:  true
                from: 0;
                to: 360;
                duration: 2000
                loops: Animation.Infinite
                running: false
            }
        }
    }

    AccountList {
        id: accountList
        anchors.fill: parent
        visible: true
        model: accountModel
        onCountChanged: {
            setPageElementVisibility(accountList.count)
        }
        property bool downloadStart: false
        Timer {
            /*
             * The MEP dialogue is not shown when the movement animation
             * is active. This timer ensures that the login procedure starts
             * with a delay.
             * TODO -- Find a better solution.
             */
            id: syncTimer
            interval: 500
            running: false
            repeat: false
            onTriggered: syncAllAccounts()
        }
        onMovementEnded: {
            if (downloadStart) {
                downloadStart = false
                syncTimer.start()
            }
        }
        onDragEnded: {
            downloadStart = contentY < -120
        }
        onSyncClicked: function onSyncClicked(acntId, index) {
            doIsdsAction("syncOneAccount", acntId)
        }
        onAcntMenuClicked: function onAcntMenuClicked(acntId, acntName) {
            showPopupAccountMenu(acntName, acntId)
        }
        onMoveClicked: function onMoveClicked(src, trg) {
            accountModel.move(src, trg);
            /* INI settings are saved automatically. */
        }
    }

    AcntId {
        id: actionInAcntId
    }
    AcntId {
        id: actionOutAcntId
    }
    Connections {
        target: isds
        function onLoginFailed(userName, testing) {
            disabledPageComponents(false)
        }
        function onDownloadMessageListFinishedSig(isMsgReceived, userName, testing, success, errTxt) {
            actionOutAcntId.username = userName
            actionOutAcntId.testing = testing
            accounts.updateOneAccountCounters(accountModel, actionOutAcntId)
            if (isMsgReceived) {
                setSyncButtonRotation(userName, testing, false)
                accounts.updateLastSyncTime(actionOutAcntId)
            }
            disabledPageComponents(false)
            if (!success) {
                okMsgBox.error(qsTr("%1: messages").arg(userName),
                    qsTr("Failed to download list of messages for username '%1'.").arg(userName),
                    errTxt)
            }
        }
        function onRunSyncOneAccountSig(userName, testing) {
            actionInAcntId.username = userName
            actionInAcntId.testing = testing
            if (messages.testMsgDbIntegrity(actionInAcntId)) {
                setSyncButtonRotation(userName, testing, true)
                isds.syncOneAccount(actionInAcntId)
            } else {
                yesNoMsgBox.userName = userName
                yesNoMsgBox.testing = testing
                yesNoMsgBox.question(qsTr("Database problem: %1").arg(userName),
                    qsTr("The message database for username '%1' appears to be corrupt. Synchronisation won't most likely be able to write new data and therefore is going to be aborted.").arg(userName),
                    qsTr("Do you want to restore the damaged database from a local back-up copy?"))
            }
        }
    }
}
