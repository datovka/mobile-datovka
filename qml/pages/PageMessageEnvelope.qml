/*
 * Copyright (C) 2014-2025 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

import QtQuick 2.7
import QtQuick.Layouts 1.3
import QtQuick.Controls 2.15
import QtQuick.Controls.Material 2.15
import cz.nic.mobileDatovka 1.0
import cz.nic.mobileDatovka.qmlIdentifiers 1.0

Page {
    id: msgEnvelopePage

    /* These properties must be set by caller. */
    property var pageView
    property AcntId acntId: null
    property int msgType
    property string msgId: ""
    property bool loadFromZfo: false
    property bool loadFromExternalZfo: false
    property bool isDeliveryInfo: false
    property bool saveAsPdf: false

    property string msgDescrHtml

    function saveFile(iCloudUpload) {
        if (saveAsPdf) {
            if (isDeliveryInfo) {
                files.saveDeliveryInfoPdfToDisk(acntId, msgId, iCloudUpload)
            } else {
                files.saveMsgEnvelopePdfToDisk(acntId, msgId, msgType, iCloudUpload)
            }
        } else {
            if (loadFromZfo || loadFromExternalZfo) {
                files.saveZfoFilesToDisk(acntId, msgId, msgType, iCloudUpload)
            } else {
                files.saveMsgFilesToDisk(acntId, msgId, msgType, MsgAttachFlag.MSG_ZFO, iCloudUpload)
            }
        }
    }

    MessageBox {
        id: yesNoMsgBoxiOS
        onAcceptAndClosed: saveFile(true)
        onRejectAndClosed: saveFile(false)
    }

    Component.onCompleted: {
        msgDescrHtml = isDeliveryInfo ? messages.getDeliveryInfoDetail(acntId, msgId) : messages.getMessageDetail(acntId, msgId, msgType, false)
    }

    header: PageHeader {
        id: headerBar
        title: isDeliveryInfo ? qsTr("Acceptance Info") : qsTr("Message Envelope")
        onBackClicked: {
            pageView.pop()
        }
    }

    Flickable {
        anchors.fill: parent
        contentHeight: flickContent.implicitHeight
        Pane {
            id: flickContent
            anchors.fill: parent
            background: Rectangle {
                color: datovkaPalette.base
            }
            AccessibleText {
                width: parent.width
                textFormat: TextEdit.RichText
                wrapMode: Text.WordWrap
                text: msgDescrHtml
            }
        } // Pane
        ScrollIndicator.vertical: ScrollIndicator {}
    } // Flickable

    footer: ToolBar {
        id: toolBar
        Material.foreground: buttonBgColor
        RowLayout {
            anchors.centerIn : parent
            spacing: formItemVerticalSpacing
            AccessibleToolButton {
                id: savePdf
                accessibleName: isDeliveryInfo ? qsTr("Save acceptance info as PDF.") : qsTr("Save envelope as PDF.")
                icon.source: "qrc:/ui/save-to-disk-pdf.svg"
                text: qsTr("Save PDF")
                display: AbstractButton.TextUnderIcon
                onClicked: {
                    saveAsPdf = true
                    if (Qt.platform.os == "ios") {
                        yesNoMsgBoxiOS.question(qsTr("Save PDF: %1").arg(msgId),
                        qsTr("You can export files into iCloud or into device local storage."),
                        qsTr("Do you want to export files to Datovka iCloud container?"))
                    } else  {
                        saveFile(false)
                    }
                }
            }
            AccessibleToolButton {
                id: saveZfo
                accessibleName: isDeliveryInfo ? qsTr("Save acceptance info as ZFO.") : qsTr("Save message as ZFO.")
                icon.source: "qrc:/ui/save-to-disk-zfo.svg"
                text: qsTr("Save ZFO")
                display: AbstractButton.TextUnderIcon
                onClicked: {
                    saveAsPdf = false
                    if (Qt.platform.os == "ios") {
                        yesNoMsgBoxiOS.question(qsTr("Save ZFO: %1").arg(msgId),
                        qsTr("You can export files into iCloud or into device local storage."),
                        qsTr("Do you want to export files to Datovka iCloud container?"))
                    } else  {
                        saveFile(false)
                    }
                }
            }
        }
    }
}
