/*
 * Copyright (C) 2014-2025 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

import QtQuick 2.7

/*
 * Accessible text component.
 *
 * This component exposes the its text to the accessibility interface.
 * If accessibleName is specified then its content is used instead of the text
 * property value.
 */
Row {
    id: root

    /* These properties must be set by caller. */
    property string accessibleName: ""
    property string labelText: ""
    property string titleText: ""
    property string infoText: ""
    property string detailText: ""

    signal clicked()

    spacing: defaultMargin

    function handleClick() {
        if (root.infoText !== "") {
            okMsgBox.info(root.titleText, root.infoText, root.detailText);
        }
        root.clicked()
    }

    Text {
        id: lText
        font.bold: true
        color: datovkaPalette.text
        text: labelText
        visible: (root.labelText !== "")
        MouseArea {
            anchors.fill: parent
            Accessible.role: Accessible.Button
            Accessible.name: (root.accessibleName !== "") ? root.accessibleName : ((lText.textFormat !== TextEdit.RichText) ? lText.text : strManipulation.removeRich(lText.text))
            Accessible.onPressAction: {
                handleClick()
            }
            onClicked: {
                handleClick()
            }
        }
    }

    Rectangle {
        width: lText.font.pixelSize
        height: width
        color: datovkaPalette.highlightedText
        radius: width * 0.5
        visible: (root.infoText !== "")
        Text {
           anchors.centerIn: parent
           font.pixelSize: textFontPixelSizeSmall
           font.bold: true
           color: buttonTextColor
           text: "?"
        }
        MouseArea {
            anchors.fill: parent
            Accessible.role: Accessible.Button
            Accessible.name: qsTr("Help")
            Accessible.onPressAction: {
                handleClick()
            }
            onClicked: {
                handleClick()
            }
        }
    }
}
