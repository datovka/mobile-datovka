/*
 * Copyright (C) 2014-2025 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

import QtQuick 2.7
import QtQuick.Controls 2.15
import QtQuick.Layouts 1.15
import cz.nic.mobileDatovka.messages 1.0
import cz.nic.mobileDatovka.qmlIdentifiers 1.0

ScrollableListView {
    id: root

    property int msgDeletionNotifyAheadDays: settings.msgDeletionNotifyAheadDays()

    /* These signals should be captured to implement message interaction. */
    signal msgClicked(AcntId acntId, int msgType, string msgId, int msgDaysToDeletion)
    signal msgPressAndHold(AcntId acntId, int msgType, string msgId, bool canDelete)

    delegate: Rectangle {
        id: messageItem
        property bool isReceivedMsg : (rMsgType === MessageType.TYPE_RECEIVED)
        width: root.width
        height: listViewDelegateHeight
        color: (isReceivedMsg && !rReadLocally) ? datovkaPalette.selected : "transparent"
        GridLayout {
            anchors.fill: parent
            anchors.margins: defaultMargin
            anchors.verticalCenter: parent.verticalCenter
            rows: 3
            columns: 2
            rowSpacing: 0
            flow: Grid.TopToBottom

            AccessibleRoundButton {
                visible: (isReceivedMsg && !rReadLocally)
            }
            Pictogram {
                visible: !(isReceivedMsg && !rReadLocally)
                source: getImgThemeFilePath("email-open-outline.svg")
            }
            Pictogram {
                source: (rAttachmentsDownloaded) ? getImgThemeFilePath("paper-clip.svg") : "qrc:/ui/datovka-msg-blank.png"
            }
            Pictogram {
                source: rRecordsManagement ? getImgThemeFilePath("briefcase.svg") : "qrc:/ui/datovka-msg-blank.png"
            }

            /* Second column */
            AccessibleText {
                Layout.fillWidth: true
                elide: Text.ElideRight
                font.bold: (isReceivedMsg && !rReadLocally)
                text: (isReceivedMsg) ? rFrom : rTo
            }
            AccessibleText {
                Layout.fillWidth: true
                elide: Text.ElideRight
                font.bold: (isReceivedMsg && !rReadLocally)
                text: rAnnotation
            }
            Item {
                Layout.fillWidth: true
                Layout.fillHeight: true
                GridLayout {
                    width: parent.width
                    rows: 2
                    columns: 2
                    rowSpacing: 0
                    flow: Grid.TopToBottom
                    AccessibleTextInfoSmall {
                        Layout.fillWidth: true
                        wrapMode: Text.NoWrap
                        text: qsTr("Delivered") + ": " + rDelivTime
                    }
                    AccessibleTextInfoSmall {
                        Layout.fillWidth: true
                        wrapMode: Text.NoWrap
                        elide: Text.ElideRight
                        text: qsTr("Accepted") + ": " + rAcceptTime
                    }
                    AccessibleTextInfoSmall {
                        Layout.fillWidth: true
                        Row {
                            anchors.top: parent.top
                            anchors.right: parent.right
                            spacing: defaultMargin
                            Pictogram {
                                id: deletionIcon
                                visible: false
                                Component.onCompleted: {
                                    if ((rMsgDaysToDeletion <= msgDeletionNotifyAheadDays) && (rMsgDaysToDeletion > 0)) {
                                        deletionIcon.source = "qrc:/ui/danger-cross-orange.svg"
                                        deletionIcon.visible = true
                                    } else if (rMsgDaysToDeletion === 0) {
                                        deletionIcon.source = "qrc:/ui/danger-cross-red.svg"
                                        deletionIcon.visible = true
                                    }
                                }
                            }
                            /* Add another pictograms here if needed. */
                        }
                    }
                    AccessibleTextInfoSmall {
                        Layout.fillWidth: true
                        horizontalAlignment: Text.AlignRight
                        wrapMode: Text.NoWrap
                        text: qsTr("ID") + ": " + rMsgId
                    }
                } // GridLayout
            } // Item
        } // GridLayout
        SeparatorLine {
            anchors.bottom: parent.bottom
        }
        MouseArea {
            /* Construct string to be presented to accessibility interface. */
            function accessibleText() {
                var aText = "";
                if (isReceivedMsg) {
                    aText += !rReadLocally ? qsTr("Unread message from sender") : qsTr("Read message from sender")
                } else {
                    aText += qsTr("Message to receiver")
                }
                aText += " '" + ((isReceivedMsg) ? rFrom : rTo) + "'. "
                aText += qsTr("Subject") + ": '" + rAnnotation + "'."
                return aText;
            }

            AcntId {
                id: actionAcntId
            }
            function handleClick() {
                actionAcntId.username = rUserName
                actionAcntId.testing = rTesting
                root.msgClicked(actionAcntId, rMsgType, rMsgId, rMsgDaysToDeletion)
            }

            anchors.fill: parent

            Accessible.role: Accessible.Button
            Accessible.name: accessibleText()
            Accessible.onScrollDownAction: {
                root.scrollDown()
            }
            Accessible.onScrollUpAction: {
                root.scrollUp()
            }
            Accessible.onPressAction: {
                handleClick()
            }
            onClicked: {
                handleClick()
            }
            onPressAndHold: {
                /* Accessible doesn't support this type of action. */
                root.msgPressAndHold(acntId, rMsgType, rMsgId, (rMsgDaysToDeletion <= 0))
            }
        }
    } // Rectangle
    ScrollIndicator.vertical: ScrollIndicator {}
} // ScrollableListView
