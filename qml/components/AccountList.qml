/*
 * Copyright (C) 2014-2025 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

import QtQuick 2.15
import QtQuick.Layouts 1.3
import QtQuick.Controls 2.2
import cz.nic.mobileDatovka 1.0
import cz.nic.mobileDatovka.messages 1.0
import cz.nic.mobileDatovka.qmlIdentifiers 1.0

ScrollableListView {
    id: root

    clip: true
    spacing: defaultMargin

    /* These signals should be captured to implement interaction. */
    signal syncClicked(AcntId acntId, int index)
    signal acntMenuClicked(AcntId acntId, string acntName)
    signal moveClicked(int src, int trg)

    property bool showArrows: false

    function restartArrowsTimer() {
        showArrows = true
        showPositionArrowsInterval.restart()
    }

    Timer {
        id: showPositionArrowsInterval
        interval: 10000
        running: false
        repeat: false
        onTriggered: showArrows = false
    }

    delegate: Rectangle {
        id: dragRect
        property alias syncButtonRotation: syncButtonRotation
        property alias syncLabelText: syncLabelText
        property alias acntId: acntId
        property alias acntName: accountName
        width: root.width
        height: baseDelegateHeight * 3.7
        color: datovkaPalette.account
        border.color: datovkaPalette.greyText
        border.width: 1
        radius: 10
        AcntId {
            id: acntId
            username: rUserName
            testing: rTestAccount
        }
        MouseArea {
            function handleClick() {
                pageView.push(pageMessageListSwipeView, {
                        "pageViewSv": pageView,
                        "acntNameSv": rAcntName,
                        "acntIdSv": acntId,
                        "isMainPageSv": false
                    })
            }
            anchors.fill: parent
            Accessible.role: Accessible.Button
            Accessible.name: qsTr("Show messages of data box '%1'. New or unread %2 of %3 received messages.").arg(rAcntName).arg(rRcvdUnread).arg(rRcvdTotal)
            Accessible.onScrollDownAction: root.scrollDown()
            Accessible.onScrollUpAction: root.scrollUp()
            Accessible.onPressAction: {
                handleClick()
            }
            onClicked: {
                handleClick()
            }
            onPressAndHold: {
                showArrows = true
                showPositionArrowsInterval.start()
            }
        }
        ColumnLayout {
            anchors.fill: parent
            anchors.margins: defaultMargin * 2
            spacing: defaultMargin

            Label {
                id: accountName
                Layout.fillWidth: true
                width: parent.width
                font.bold: true
                font.italic: !rStoreIntoDb
                font.capitalization: Font.AllUppercase
                font.pixelSize: textFontPixelSizeLarge
                elide: Text.ElideRight
                text: rAcntName
            }

            RowLayout {
                Layout.fillWidth: true
                spacing: defaultMargin * 4
                Column {
                    spacing: defaultMargin
                    Item {
                        height: baseDelegateHeight * 1.7
                        width: baseDelegateHeight * 1.7
                        anchors.horizontalCenter: parent.horizontalCenter
                        Image {
                            source: rDataboxLogo
                            cache: false
                            anchors.centerIn: parent
                            sourceSize.height: parent.height
                            sourceSize.width: parent.width
                        }
                        AccessibleRoundButton {
                            id: newMessages
                            visible: (rRcvdUnread > 0)
                            anchors.right: parent.right
                            anchors.top: parent.top
                            text: rRcvdUnread
                            font.pixelSize: textFontPixelSizeSmall
                            font.bold: true
                            Accessible.role: Accessible.StaticText
                            accessibleName: qsTr("New messages: %1").arg(rRcvdUnread)
                            background: Rectangle {
                                radius: newMessages.radius
                                color: "#FF5D00"
                            }
                        }
                    }
                    AccessibleLabelDark {
                        anchors.horizontalCenter: parent.horizontalCenter
                        font.bold: true
                        text: "    ID: " + rDataboxId + "    "
                    }
                }
                Column {
                    spacing: defaultMargin
                    AccessibleLabelDark {
                        id: syncLabelText
                        text: qsTr("Last synchronisation") + ":"
                    }
                    AccessibleLabel {
                        font.pixelSize: textFontPixelSizeSmall
                        text: rLastSyncTime
                    }
                    AccessibleLabelDark {
                        id: counters
                        font.bold: true
                        text: qsTr("Received") + ": " + rRcvdTotal + "   " +  qsTr("Sent") + ": " + rSntTotal
                    }
                    Row {
                        spacing: defaultMargin
                        AccessibleRoundButton {
                            id: syncButton
                            z: 1
                            icon.source: "qrc:/ui/sync.svg"
                            accessibleName: qsTr("Synchronise data box '%1'.").arg(rAcntName)
                            onClicked: root.syncClicked(acntId, index)
                            RotationAnimator {
                                id: syncButtonRotation
                                target: syncButton
                                alwaysRunToEnd:  true
                                from: 0;
                                to: 360;
                                duration: 2000
                                loops: Animation.Infinite
                                running: false
                            }
                        }
                        AccessibleRoundButton {
                            z: 1
                            icon.source: "qrc:/ui/pencil-box-outline.svg"
                            accessibleDescription: qsTr("Create and send new message from data box '%1'.").arg(rAcntName)
                            onClicked: pageView.push(pageSendMessage, {"pageView": pageView, "acntName": rAcntName, "acntId": acntId, "action": "new"}, StackView.Immediate)
                        }
                        AccessibleRoundButton {
                            z: 1
                            icon.source: "qrc:/ui/databox-settings.svg"
                            accessibleDescription: qsTr("Data-box properties '%1'.").arg(rAcntName)
                            onClicked: root.acntMenuClicked(acntId, rAcntName)
                        }
                    } //Row
                } //Column
            } //Row
        } //ColumnLayout
        Label {
            visible: showArrows && (index > 0)
            anchors.top: parent.top
            anchors.right: parent.right
            anchors.margins: defaultMargin * 2
            color: buttonBgColor
            font.bold: true
            font.pixelSize: appPixelSize * 2
            z: 1
            text: "↑"
            MouseArea {
                anchors.fill: parent
                onClicked: {
                    root.restartArrowsTimer()
                    root.moveClicked(index, index-1)
                }
            }
        }
        Label {
            visible: showArrows && (index < root.count-1)
            anchors.right: parent.right
            anchors.bottom: parent.bottom
            anchors.margins: defaultMargin * 2
            color: buttonBgColor
            font.bold: true
            font.pixelSize: appPixelSize * 2
            z: 1
            text: "↓"
            MouseArea {
                anchors.fill: parent
                onClicked: {
                    root.restartArrowsTimer()
                    root.moveClicked(index, index+1)
                }
            }
        }
    } //Rectangle
    ScrollIndicator.vertical: ScrollIndicator {}
}
