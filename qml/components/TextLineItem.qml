/*
 * Copyright (C) 2014-2025 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

import QtQuick 2.7
import cz.nic.mobileDatovka 1.0

/*
 * TextLineItem component.
 */
Item {
    id: root

    property alias textLine: textField
    property alias label: label
    property alias actionButton: actionButton

    /* These properties must be set by caller. */
    property string textLineTitle: ""
    property string placeholderText: ""
    property string titleText: ""
    property string infoText: ""
    property string detailText: ""
    property int inputMethodHints: Qt.ImhNone
    property bool isPassword: false

    width: parent.width
    height: textField.height + label.height

    AccessibleTextHelp {
        id: label
        labelText: root.textLineTitle
        titleText: root.titleText
        infoText: root.infoText
        detailText: root.detailText
    }
    AccessibleTextField {
        id: textField
        anchors.top: label.bottom
        anchors.topMargin: defaultMargin
        width: actionButton.visible ? parent.width - actionButton.width : parent.width
        inputMethodHints: inputMethodHints
        echoMode: isPassword ? TextInput.Password : TextInput.Normal
        passwordMaskDelay: isPassword ? 500 : 0
        placeholderText: root.placeholderText
    }
    AccessibleImageButton {
        id: actionButton
        visible: false
        anchors.verticalCenter: textField.verticalCenter
        anchors.left: textField.right
        anchors.leftMargin: defaultMargin
        sourceSize.height: parent.height * 0.4
        source: "qrc:/ui/alert-red.svg"
        accessibleName: qsTr("Input text action button.")
    }
}
