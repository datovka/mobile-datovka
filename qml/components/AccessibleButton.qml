/*
 * Copyright (C) 2014-2025 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

import QtQuick 2.7
import QtQuick.Controls 2.15
import QtQuick.Controls.Material 2.15

/*
 * Accessible button component.
 */
RoundButton {
    id: root

    /* These properties must be set by caller. */
    property string accessibleDescription: ""
    property string accessibleName: ""

    Accessible.role: Accessible.Button
    Accessible.checkable: root.checkable
    Accessible.checked: root.checked
    Accessible.description: root.accessibleDescription
    Accessible.name: (root.accessibleName !== "") ? root.accessibleName : root.text
    Accessible.pressed: root.pressed
    Accessible.onPressAction: {
        root.clicked()
    }

    radius: 10
    leftPadding: 20
    rightPadding: 20

    Material.foreground: buttonTextColor

    background: Rectangle {
        radius: root.radius
        color: datovkaPalette.highlightedText
    }
}
