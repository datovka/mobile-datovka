/*
 * Copyright (C) 2014-2025 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

import QtQuick 2.7
import QtQuick.Controls 2.15
import QtQuick.Layouts 1.15
import cz.nic.mobileDatovka 1.0
import cz.nic.mobileDatovka.modelEntries 1.0

ScrollableListView {
    id: root

    /* These properties must be set by caller. */
    property bool isSendMsgRecipList: false
    property bool canDetailBoxes: false // enables viewing of box details
    property bool canSelectBoxes: false // enables selecting of entries
    property bool canDeselectBoxes: false // enables deselecting of entries
    property bool canRemoveBoxes: false // enables removing of entries
    property bool localContactFormat: false // false - format of contacts obtained from ISDS server
                                            // true - contacts gathered from local storage

    /* This signal should be captured to implement model interaction. */
    signal boxDetail(string boxId)
    signal boxSelect(string boxId)
    signal boxDeselect(string boxId)
    signal boxRemove(string boxId)
    signal pdzDmType(string boxId, int dmType)

    delegateHeight: (isSendMsgRecipList) ? listViewDelegateHeight * 1.3 : listViewDelegateHeight

    function descrDmType(dmType) {
        switch (dmType) {
        case 86: // 'V'
            return qsTr("Public");
        case 79: // 'O'
            return qsTr("Response to initiatory");
        case 71: // 'G'
            return qsTr("Subsidised");
        case 75: // 'K'
            return qsTr("Contractual");
        case 73: // 'I'
            return qsTr("Initiatory");
        case 69: // 'E'
            return qsTr("Prepaid credit");
        default:
            return "N/A";
        }
    }
    delegate: Rectangle {

        /*
         * Returns specified value:
         * -1 - none ov below
         *  0 - show box detail
         *  1 - deselect box
         *  2 - select box
         */
        function actionOnClick() {
            var ret = -1;
            if (canDetailBoxes && !(canSelectBoxes || canDeselectBoxes)) {
                ret = 0;
            } else if (!canDetailBoxes && (canSelectBoxes || canDeselectBoxes)) {
                if (rDbSelected && canDeselectBoxes) {
                    ret = 1;
                } else if (!rDbSelected && canSelectBoxes) {
                    ret = 2;
                }
            }
            return ret;
        }

        function handleClick() {
            switch (actionOnClick()) {
            case 0:
                root.boxDetail(rDbID)
                break;
            case 1:
                root.boxDeselect(rDbID)
                break;
            case 2:
                root.boxSelect(rDbID)
                break;
            default:
                break;
            }
        }

        id: dbItem
        width: root.width
        height: (rPdz === 1  && rPdzDmTypeList.length > 0) ? root.delegateHeight * 1.4 : root.delegateHeight
        color: rDbSelected ? datovkaPalette.selected : "transparent"
        SeparatorLine {
            anchors.top: parent.top
            visible: index === 0
        }
        MouseArea {
            anchors.fill: parent
            Accessible.role: Accessible.Button
            Accessible.onScrollDownAction: {
                root.scrollDown()
            }
            Accessible.onScrollUpAction: {
                root.scrollUp()
            }
            Accessible.onPressAction: {
                handleClick()
            }
            onClicked: {
                handleClick()
            }
        }

        ColumnLayout {
            anchors.fill: parent
            anchors.margins: defaultMargin
            anchors.verticalCenter: parent.verticalCenter
            spacing: defaultMargin
            Column {
                Layout.fillWidth: true
                spacing: defaultMargin
                AccessibleText {
                    id: boxOwnerName
                    width: parent.width - actionButton.width
                    elide: Text.ElideRight
                    wrapMode: Text.NoWrap
                    font.bold: true
                    text: rDbName
                }
                AccessibleTextInfo {
                    id: boxOwnerAddress
                    width: parent.width - actionButton.width
                    elide: Text.ElideRight
                    wrapMode: Text.NoWrap
                    text: rDbAddress
                }
                AccessibleTextInfo {
                    id: boxInfo
                    text: if (rDbIc !== "" && rDbType !== "") {
                            "ID: " + rDbID + "   (" + rDbType + ")   IČO: " + rDbIc + "   " + rDbSendOption
                        } else if (rDbIc === "" && rDbType !== "") {
                            "ID: " + rDbID + "   (" + rDbType + ")" + "   " + rDbSendOption
                        } else if (rDbIc !== "" && rDbType === "") {
                            "ID: " + rDbID + "   IČO: " + rDbIc + "   " + rDbSendOption
                        } else  {
                            "ID: " + rDbID + "   " + rDbSendOption
                        }
                }
                AccessibleTextInfo {
                    function setMessageTypeText(msgType) {
                        if (msgType === 0) {
                            return qsTr("Public")
                        } else if (msgType === 1) {
                            return "PDZ"
                        } else {
                            return qsTr("Unknown")
                        }
                    }
                    visible: isSendMsgRecipList
                    width: parent.width
                    text: qsTr("Message type: %1").arg(setMessageTypeText(rPdz))
                }
                Row {
                    visible: (isSendMsgRecipList && (rPdz === 1))
                    spacing: defaultMargin
                    width: parent.width
                    Label {
                        anchors.verticalCenter: parent.verticalCenter
                        color: datovkaPalette.greyText
                        text: qsTr("Payment:")
                    }
                    AccessibleComboBox {
                        z: 1
                        width: parent.width * 0.7
                        accessibleDescription: qsTr("Select commercial message payment method.")
                        model: ListModel {
                            id: pdzPaymentsModel
                        }
                        Component.onCompleted: {
                            for (var i = 0; i < rPdzDmTypeList.length; i++ ) {
                                pdzPaymentsModel.append({label:descrDmType(rPdzDmTypeList[i]), key:rPdzDmTypeList[i]})
                            }
                        }
                        onCurrentIndexChanged: root.pdzDmType(rDbID, currentKey())
                    }
                }
            } // Column
        } // ColumnLayout

        Rectangle {
            id: actionButton
            visible: !canDetailBoxes
            z: 1
            anchors.right: parent.right
            anchors.rightMargin: defaultMargin
            height: parent.height
            width: baseDelegateHeight
            color: parent.color
            AccessibleRoundButton {
                anchors.centerIn: parent
                icon.source: if (canDetailBoxes && !(canSelectBoxes || canDeselectBoxes) && !canRemoveBoxes) {
                    "qrc:/ui/datovka-msg-blank.png"
                } else if (!canDetailBoxes && (canSelectBoxes || canDeselectBoxes) && !canRemoveBoxes) {
                    if (rDbSelected && canDeselectBoxes) {
                        "qrc:/ui/subtract.svg"
                    } else if (!rDbSelected && canSelectBoxes) {
                        "qrc:/ui/add.svg"
                    }
                } else if (!canDetailBoxes && !(canSelectBoxes || canDeselectBoxes) && canRemoveBoxes) {
                    "qrc:/ui/remove.svg"
                }
            }
            MouseArea {
                anchors.fill: parent
                onClicked: {
                    handleClick()
                    root.boxRemove(rDbID)
                }
            }
        }
        SeparatorLine {
            anchors.bottom: parent.bottom
        }
    } // Rectangle
    ScrollIndicator.vertical: ScrollIndicator {}
} // ListView
