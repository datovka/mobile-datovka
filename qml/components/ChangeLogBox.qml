/*
 * Copyright (C) 2014-2025 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

import QtQuick 2.7
import QtQuick.Controls 2.1
import QtQuick.Layouts 1.2

/*
 * Provides a simple change log dialog.
 */
Popup {
    id: root
    anchors.centerIn: parent
    modal: true

    property int preferredMinWidth: 280

    /* Public interface. */
    function showChangeLog() {
        changeLogText.text = settings.loadChangeLogText()
        root.open()
    }

    background: Rectangle {
        color: datovkaPalette.base
        border.color: datovkaPalette.text
        radius: defaultMargin
    }

    ColumnLayout {
        spacing: defaultMargin * 2
        AccessibleText {
            font.bold: true
            Layout.alignment: Qt.AlignCenter
            color: datovkaPalette.highlightedText
            text: qsTr("Version") + ": " + Qt.application.version
        }
        AccessibleText {
            Layout.preferredWidth: root.preferredMinWidth
            horizontalAlignment : Text.Center
            wrapMode: Text.Wrap
            font.bold: true
            text: qsTr("What's new?")
        }
        Rectangle {
            Layout.preferredWidth: root.preferredMinWidth
            implicitHeight: preferredMinWidth
            color: "transparent"
            border.color: datovkaPalette.line
            border.width: 1
            Flickable {
                anchors.fill: parent
                anchors.margins: 1
                contentHeight: flickContent.implicitHeight
                clip: true
                Pane {
                    id: flickContent
                    anchors.fill: parent
                    AccessibleText {
                        id: changeLogText
                        width: parent.width
                        wrapMode: Text.WordWrap
                        textFormat: TextEdit.RichText
                    }
                }
                ScrollIndicator.vertical: ScrollIndicator {}
            }
        }
        AccessibleButton {
            Layout.alignment: Qt.AlignCenter
            text: "OK"
            onClicked: root.close()
        }
    }
}
