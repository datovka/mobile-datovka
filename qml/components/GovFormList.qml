/*
 * Copyright (C) 2014-2025 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

import QtQuick 2.7
import QtQuick.Controls 2.15
import cz.nic.mobileDatovka 1.0

ScrollableListView {
    id: root
    /*
     * Return asterisks.
     */
    function asterisks(mandatory, userInput, boxInput) {
        if (mandatory && userInput) {
            return " *"
        } else if (boxInput) {
            return " **"
        } else {
            return ""
        }
    }

    delegate: Rectangle {
        id: formItem
        width: parent.width
        height: listViewDelegateHeight * 0.8
        enabled: gsUserInput
        color: enabled ? datovkaPalette.base : datovkaPalette.selected
        Item {
            anchors.fill: parent
            anchors.margins: defaultMargin
            ControlGroupItem {
                spacing: formItemVerticalSpacing
                AccessibleLabel {
                    width: parent.width
                    wrapMode: Text.Wrap
                    text: gsDescr + asterisks(gsMandatory, gsUserInput, gsBoxInput)
                }
                AccessibleTextField {
                    id: textField
                    width: parent.width
                    text: gsVal
                    placeholderText: gsPlacehold
                    readOnly: !formItem.enabled
                    onTextEdited: {
                        root.model.setProperty(index, "gsVal", textField.text)
                    }
                }
            } // Column
        } // Item
    } // Rectangle

    ScrollIndicator.vertical: ScrollIndicator {}
}
