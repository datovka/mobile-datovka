/*
 * Copyright (C) 2014-2025 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

import QtQuick 2.7
import QtQuick.Controls 2.2

/*
 * Accessible switch component.
 */
Item {
    id: root

    property alias sw: swtch

    /* These properties must be set by caller. */
    property string accessibleDescription: ""
    property string accessibleName: ""
    property string switchText: ""
    property string titleText: ""
    property string infoText: ""
    property string detailText: ""
    property bool checked: false

    width: parent.width
    height: sw.height

    Row {
        //spacing: defaultMargin
        Switch {
            id: swtch
            text: root.switchText
            checked: root.checked
            Accessible.role: Accessible.CheckBox
            Accessible.checkStateMixed: false
            Accessible.checkable: true
            Accessible.checked: swtch.checked
            Accessible.description: root.accessibleDescription
            Accessible.name: (root.accessibleName !== "") ? root.accessibleName : swtch.text
            Accessible.onPressAction: {
                swtch.toggle()
            }
            Accessible.onToggleAction: {
                swtch.toggle()
            }
        }
        AccessibleTextHelp {
            anchors.verticalCenter: parent.verticalCenter
            titleText: root.titleText
            infoText: root.infoText
            detailText: root.detailText
        }
    }
}
