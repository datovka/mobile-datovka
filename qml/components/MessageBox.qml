/*
 * Copyright (C) 2014-2025 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

import QtQuick 2.7
import QtQuick.Controls 2.1
import QtQuick.Layouts 1.2

/*
 * Provides a simple message box dialog.
 */
Popup {
    id: root
    anchors.centerIn: parent
    modal: true

    /* Private function. */
    function setPopupText(msgBoxTitle, msgBoxText, msgBoxDetail) {
        titleText.text = msgBoxTitle
        mainText.text = msgBoxText
        detailText.visible = (msgBoxDetail !== "")
        detailText.text = msgBoxDetail
    }

    /* Public interface. */
    function error(msgBoxTitle, msgBoxText, msgBoxDetail) {
        yesButton.visible = false
        noButton.visible = false
        okButton.visible = true
        customButton.visible = false
        titleText.color = datovkaPalette.errorText
        root.setPopupText(msgBoxTitle, msgBoxText, msgBoxDetail)
        root.open()
    }

    /* Public interface. */
    function info(msgBoxTitle, msgBoxText, msgBoxDetail) {
        yesButton.visible = false
        noButton.visible = false
        okButton.visible = true
        customButton.visible = false
        root.setPopupText(msgBoxTitle, msgBoxText, msgBoxDetail)
        root.open()
    }

     /* Public interface. */
    function question(msgBoxTitle, msgBoxText, msgBoxDetail) {
        yesButton.visible = true
        noButton.visible = true
        okButton.visible = false
        customButton.visible = false
        closePolicy = Popup.NoAutoClose
        root.setPopupText(msgBoxTitle, msgBoxText, msgBoxDetail)
        root.open()
    }

     /* Public interface. */
    function customBox(msgBoxTitle, msgBoxText, msgBoxDetail, btnText) {
        yesButton.visible = false
        noButton.visible = false
        okButton.visible = false
        customButton.text = btnText
        customButton.visible = true
        root.setPopupText(msgBoxTitle, msgBoxText, msgBoxDetail)
        root.open()
    }

     /* Public MEP wait interface. */
    function showMepWaitBox(msgBoxTitle, msgBoxText, btnText) {
        modal = false
        closePolicy = Popup.NoAutoClose
        yesButton.visible = false
        noButton.visible = false
        okButton.visible = false
        detailText.visible = false
        customButton.text = btnText
        customButton.visible = true
        root.setPopupText(msgBoxTitle, msgBoxText, "")
        root.open()
    }

    signal acceptAndClosed()
    signal rejectAndClosed()

    property int preferredMinWidth: 260
    property bool accept: false

    background: Rectangle {
        color: datovkaPalette.base
        border.color: datovkaPalette.text
        radius: defaultMargin
    }

    onClosed: {
        if (root.accept) {
            acceptAndClosed()
        } else {
            rejectAndClosed()
        }
        root.accept = false
    }

    ColumnLayout {
        spacing: defaultMargin * 2
        AccessibleText {
            id: titleText
            font.bold: true
            Layout.alignment: Qt.AlignCenter
        }
        AccessibleText {
            id: mainText
            Layout.preferredWidth: root.preferredMinWidth
            horizontalAlignment : Text.Center
            wrapMode: Text.Wrap
        }
        AccessibleText {
            id: detailText
            Layout.preferredWidth: root.preferredMinWidth
            horizontalAlignment : Text.Center
            wrapMode: Text.Wrap
        }
        RowLayout {
            Layout.alignment: Qt.AlignCenter
            spacing: defaultMargin * 2
            AccessibleButton {
                id: yesButton
                Layout.alignment: Qt.AlignCenter
                text: qsTr("Yes")
                onClicked: {
                    root.accept = true
                    root.close()
                }
            }
            AccessibleButton {
                id: noButton
                Layout.alignment: Qt.AlignCenter
                text: qsTr("No")
                onClicked: {
                    root.accept = false
                    root.close()
                }
            }
            AccessibleButton {
                id: okButton
                Layout.alignment: Qt.AlignCenter
                text: "OK"
                onClicked: {
                    root.accept = false
                    root.close()
                }
            }
            AccessibleButton {
                id: customButton
                Layout.alignment: Qt.AlignCenter
                text: "OK"
                onClicked: {
                    root.accept = true
                    root.close()
                }
            }
        }
    }
}
