/*
 * Copyright (C) 2014-2025 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

import QtQuick 2.7
import QtQuick.Controls 2.15

ScrollableListView {
    id: root

    /* These signals should be captured to implement message interaction. */
    signal govServiceClicked(string gsInternId, string gsFullName, string gsInstName)

    delegate: Item {
        width: root.width
        height: listViewDelegateHeight * 0.8
        Item {
            anchors.fill: parent
            anchors.margins: defaultMargin
            Column {
                width: parent.width
                anchors.verticalCenter: parent.verticalCenter
                spacing: formItemVerticalSpacing
                AccessibleText {
                    width: parent.width
                    wrapMode: Text.Wrap
                    font.bold: true
                    text: gsFullName
                }
                AccessibleTextInfoSmall {
                    id: instName
                    text: "DS: " + gsBoxId + " -- " + gsInstName
                    elide: Text.ElideRight
                }
            }
        }
        SeparatorLine {
            anchors.bottom: parent.bottom
        }
        MouseArea {
            anchors.fill: parent
            onClicked: govServiceClicked(gsInternId, gsFullName, instName.text.toString())
        }
    }

    ScrollIndicator.vertical: ScrollIndicator {}
}
