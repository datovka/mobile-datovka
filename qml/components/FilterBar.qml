/*
 * Copyright (C) 2014-2025 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

import QtQuick 2.7
import QtQuick.Controls 2.15

/*
 * Filter bar component.
 */
Drawer {
    id: root
    modal: false
    width: parent.width
    height: listViewDelegateHeight * 0.8
    edge: Qt.TopEdge

    signal textChanged(string text)
    signal clearClicked()

    property alias filterField: filterField2

    Item {
        anchors.fill: parent
        Row {
            anchors.fill: parent
            anchors.margins: defaultMargin * 2
            AccessibleTextField {
                id: filterField2
                width: !iOS ? parent.width : parent.width - clearButton.width
                height: listViewDelegateHeight * 0.5
                placeholderText: qsTr("Enter filter text")
                inputMethodHints: Qt.ImhNoPredictiveText
                Accessible.searchEdit: true
                onDisplayTextChanged: root.textChanged(filterField2.displayText)
                Item {
                    visible: !iOS
                    anchors.verticalCenter: parent.verticalCenter
                    anchors.right: parent.right
                    width: parent.height
                    height: parent.height
                    Label {
                        anchors.centerIn: parent
                        font.bold: true
                        text: "X"
                    }
                    MouseArea {
                        anchors.fill: parent
                        onClicked: {
                            filterField2.clear()
                            root.clearClicked()
                            Qt.inputMethod.hide()
                        }
                    }
                }
            }
            Item {
                id: clearButton
                visible: iOS
                anchors.verticalCenter: parent.verticalCenter
                width: root.height * 0.5
                height: root.height
                Label {
                    anchors.centerIn: parent
                    font.bold: true
                    text: "X"
                }
                MouseArea {
                    anchors.fill: parent
                    onClicked: {
                        filterField2.clear()
                        root.clearClicked()
                        Qt.inputMethod.hide()
                    }
                }
            }
        }
    }
}
