/*
 * Copyright (C) 2014-2025 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

import QtQuick 2.7
import cz.nic.mobileDatovka 1.0

/*
 * TextLineItem component.
 */
Item {
    id: root

    property alias pwdTextLine: textField

    /* These properties must be set by caller. */
    property string textLineTitle: ""
    property string placeholderText: qsTr("Enter the password")
    property string titleText: ""
    property string infoText: ""
    property string detailText: ""
    property int horizontalAlignment: TextInput.AlignLeft
    property bool pwdEyeIconVisible: true
    property var checkBeforeShowing: null /* Pass this callback to eye icon. */

    property int hideAfterMs: 0 /* Negative and zero values are ignored. */

    width: parent.width
    height: textField.height + label.height

    function hide() {
        return iOS ? showPwdEyeIcon2.hide() : showPwdEyeIcon.hide()
    }

    function show() {
        return iOS ? showPwdEyeIcon2.show() : showPwdEyeIcon.show()
    }

    AccessibleTextHelp {
        id: label
        labelText: root.textLineTitle
        titleText: root.titleText
        infoText: root.infoText
        detailText: root.detailText
    }
    AccessibleTextField {
        id: textField
        anchors.top: label.bottom
        anchors.topMargin: defaultMargin
        width: !iOS ? parent.width : showPwdEyeIcon2.visible ? parent.width - showPwdEyeIcon2.width - formItemVerticalSpacing : parent.width
        echoMode: if (iOS) {
                showPwdEyeIcon2.showPwd ? TextInput.Normal : TextInput.Password
            } else {
                showPwdEyeIcon.showPwd ? TextInput.Normal : TextInput.Password
            }
        passwordMaskDelay: 500 // milliseconds
        inputMethodHints: Qt.ImhNone
        placeholderText: root.placeholderText
        horizontalAlignment: root.horizontalAlignment
        ShowPasswordOverlaidImage {
            id: showPwdEyeIcon
            visible: !iOS && pwdEyeIconVisible && (textField.text.length > 0)
            anchors.right: textField.right
            anchors.verticalCenter: parent.verticalCenter
            anchors.rightMargin: formItemVerticalSpacing
            checkBeforeShowing: root.checkBeforeShowing
            onShown: {
                if (hideAfterMs > 0) {
                    timer.running = true
                }
            }
        }
    }
    ShowPasswordOverlaidImage {
        id: showPwdEyeIcon2
        visible: iOS && pwdEyeIconVisible && (textField.text.length > 0)
        anchors.left: textField.right
        anchors.leftMargin: formItemVerticalSpacing
        checkBeforeShowing: root.checkBeforeShowing
        onShown: {
            if (hideAfterMs > 0) {
                timer.running = true
            }
        }
    }
    Timer {
        id: timer
        interval: hideAfterMs
        running: false
        repeat: false
        onTriggered: iOS ? showPwdEyeIcon2.hide() : showPwdEyeIcon.hide()
    }
}
