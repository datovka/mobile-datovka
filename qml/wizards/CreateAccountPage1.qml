/*
 * Copyright (C) 2014-2025 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

import QtQuick 2.7
import QtQuick.Controls 2.15
import cz.nic.mobileDatovka 1.0
import cz.nic.mobileDatovka.qmlIdentifiers 1.0

Page {

    signal handlerLoader(string pageFile)

    Component.onCompleted: {
        if (accountName !== "") {
            accountNameItem.textLine.text = accountName
        }
        for (var i = 0; i < accountTypeRadioGroup.buttons.length; ++i) {
            if (accountTypeRadioGroup.buttons[i].testing === acntId.testing) {
                accountTypeRadioGroup.buttons[i].checked = true
            }
        }
    }

    function canNextStep() {
        if (accountNameItem.textLine.displayText === "") {
            errorText.visible = true
            accountNameItem.textLine.focus = true
            return
        }
        accountName = accountNameItem.textLine.displayText
        acntId.testing = accountTypeRadioGroup.checkedButton.testing
        accountTypeName = accountTypeRadioGroup.checkedButton.text
        handlerLoader("CreateAccountPage2.qml")
    }

    header: PageHeader {
        id: headerBar
        title: qsTr("Add Data Box")
        onBackClicked: {
            pageView.pop()
        }
    }

    Flickable {
        anchors.fill: parent
        contentHeight: flickContent.implicitHeight
        Pane {
            id: flickContent
            anchors.fill: parent
            ButtonGroup {
                id: accountTypeRadioGroup
                onClicked: canNextStep()
            }
            Column {
                anchors.right: parent.right
                anchors.left: parent.left
                spacing: formItemVerticalSpacing
                ControlGroupItem {
                    AccessibleTextInfo {
                        id: errorText
                        visible: false
                        horizontalAlignment: Text.AlignHCenter
                        color: datovkaPalette.errorText
                        text: qsTr("The data-box title must be filled in.")
                    }
                    TextLineItem {
                        id: accountNameItem
                        textLineTitle: qsTr("Data-Box Title")
                        placeholderText: qsTr("Enter custom data-box name.")
                        isPassword: false
                        textLine.onTextChanged: {
                            nextButton.enabled = (accountNameItem.textLine.text.length > 0)
                        }
                    }
                    AccessibleTextInfo {
                        text: qsTr("The data-box title is a user-specified name used for the identification of the data box in the application (e.g. 'My Personal Data Box', 'Firm Box', etc.). The chosen name serves only for your convenience. The entry must be filled in.")
                    }
                }
                ControlGroupItem {
                    AccessibleLabel {
                        text: qsTr("Data-Box Environment")
                    }
                    RadioButton {
                        ButtonGroup.group: accountTypeRadioGroup
                        property bool testing: false
                        text: qsTr("regular")
                    }
                    RadioButton {
                        ButtonGroup.group: accountTypeRadioGroup
                        property bool testing: true
                        text: qsTr("test")
                    }
                    AccessibleTextInfo {
                        textFormat: TextEdit.RichText
                        onLinkActivated: Qt.openUrlExternally(link)
                        text: "<style>a:link { color: " + datovkaPalette.highlightedText + "; }</style>" + qsTr("The regular data box option is used to access the production (official) ISDS environment (<a href=\"https://www.mojedatovaschranka.cz\">www.mojedatovaschranka.cz</a>). Test data box is used to access the ISDS testing environment (<a href=\"https://www.czebox.cz\">www.czebox.cz</a>).")
                    }
                }
                AccessibleButton {
                    id: nextButton
                    enabled: false
                    anchors.horizontalCenter: parent.horizontalCenter
                    icon.source: "qrc:/ui/arrow-right.svg"
                    text: qsTr("Next")
                    accessibleName: qsTr("Next step")
                    onClicked: canNextStep()
                }
                PageIndicator {
                    currentIndex: 0
                    count: 4
                    anchors.horizontalCenter: parent.horizontalCenter
                }
            } // Column
        } // Pane
        ScrollIndicator.vertical: ScrollIndicator {}
    } // Flickable
} // Page
