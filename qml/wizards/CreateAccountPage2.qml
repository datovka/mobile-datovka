/*
 * Copyright (C) 2014-2025 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

import QtQuick 2.7
import QtQuick.Controls 2.15
import cz.nic.mobileDatovka 1.0
import cz.nic.mobileDatovka.account 1.0
import cz.nic.mobileDatovka.qmlIdentifiers 1.0

Page {

    signal handlerLoader(string pageFile)

    Component.onCompleted: {
        for (var i = 0; i < loginMethodRadioGroup.buttons.length; ++i) {
            if (loginMethodRadioGroup.buttons[i].loginMethod === loginMethod) {
                loginMethodRadioGroup.buttons[i].checked = true
            }
        }
    }

    function canNextStep() {
        loginMethod = loginMethodRadioGroup.checkedButton.loginMethod
        loginMethodName = loginMethodRadioGroup.checkedButton.text
        handlerLoader("CreateAccountPage3.qml")
    }

    header: PageHeader {
        id: headerBar
        title: qsTr("Add Data Box")
        onBackClicked: {
            pageView.pop()
        }
    }

    Flickable {
        anchors.fill: parent
        contentHeight: flickContent.implicitHeight
        Pane {
            id: flickContent
            anchors.fill: parent
            ButtonGroup {
                id: loginMethodRadioGroup
                onClicked: canNextStep()
            }
            Column {
                anchors.right: parent.right
                anchors.left: parent.left
                spacing: formItemVerticalSpacing
                ControlGroupItem {
                    spacing: 5
                    AccessibleLabelDark {
                        text: qsTr("Data-box title") + ": <b>" + accountName + "</b>"
                    }
                    AccessibleLabelDark {
                        text: qsTr("Data-box type") + ": <b>" + accountTypeName + "</b>"
                    }
                    SeparatorLine {
                    }
                }
                ControlGroupItem {
                    AccessibleLabel {
                       text: qsTr("Login Method")
                    }
                    RadioButton {
                        ButtonGroup.group: loginMethodRadioGroup
                        property int loginMethod: AcntData.LIM_UNAME_PWD
                        text: qsTr("username + password")
                    }
                    RadioButton {
                        ButtonGroup.group: loginMethodRadioGroup
                        property int loginMethod: AcntData.LIM_UNAME_MEP
                        text: qsTr("username + Mobile Key")
                    }
                    RadioButton {
                        ButtonGroup.group: loginMethodRadioGroup
                        property int loginMethod: AcntData.LIM_UNAME_PWD_TOTP
                        text: qsTr("username + password + SMS")
                    }
                    RadioButton {
                        ButtonGroup.group: loginMethodRadioGroup
                        property int loginMethod: AcntData.LIM_UNAME_PWD_HOTP
                        text: qsTr("username + password + security code")
                    }
                    RadioButton {
                        ButtonGroup.group: loginMethodRadioGroup
                        property int loginMethod: AcntData.LIM_UNAME_PWD_CRT
                        text: qsTr("username + password + certificate")
                    }
                    AccessibleTextInfo {
                        text: qsTr("Select the login method which you use to access the data box in the %1 ISDS environment.").arg((acntId.testing) ? qsTr("testing") : qsTr("production")) + " " + qsTr("Note: NIA login methods such as bank ID, mojeID or eCitizen aren't supported because the ISDS system doesn't provide such functionality for third-party applications.")
                    }
                }
                Row {
                    anchors.horizontalCenter: parent.horizontalCenter
                    spacing: formItemVerticalSpacing
                    AccessibleButton {
                        id: backButton
                        icon.source: "qrc:/ui/arrow-left.svg"
                        accessibleName: qsTr("Previous step")
                        text: qsTr("Back")
                        background: Rectangle {
                            radius: backButton.radius
                            color: backButtonBgColor
                        }
                        onClicked: handlerLoader("CreateAccountPage1.qml")
                    }
                    AccessibleButton {
                        id: nextButton
                        icon.source: "qrc:/ui/arrow-right.svg"
                        accessibleName: qsTr("Next step")
                        text: qsTr("Next")
                        onClicked: canNextStep()
                    }
                }
                PageIndicator {
                    currentIndex: 1
                    count: 4
                    anchors.horizontalCenter: parent.horizontalCenter
                }
            } // Column
        } // Pane
        ScrollIndicator.vertical: ScrollIndicator {}
    } // Flickable
} // Page
