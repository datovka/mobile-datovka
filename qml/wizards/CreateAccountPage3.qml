/*
 * Copyright (C) 2014-2025 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

import QtQuick 2.7
import QtQuick.Controls 2.15
import cz.nic.mobileDatovka 1.0
import cz.nic.mobileDatovka.account 1.0
import cz.nic.mobileDatovka.iOsHelper 1.0
import cz.nic.mobileDatovka.qmlIdentifiers 1.0

Page {

    signal handlerLoader(string pageFile)

    Component.onCompleted: {
        if (loginMethod === AcntData.LIM_UNAME_PWD_CRT) {
            certificateSection.visible = true
            certPathLabel.text = getFileNameFromPath(certFilePath)
            certPathLabel.visible = (certPathLabel.text !== "")
        } else if (loginMethod === AcntData.LIM_UNAME_MEP) {
            mepSection.visible = true
            pwdItem.visible = !mepSection.visible
            pwdSection.visible = pwdItem.visible
            mepCodeItem.textLine.text = mepCode
            pwdStr = ""
        }
        if (acntId.username !== "") {
            userNameItem.textLine.text = acntId.username
        }
        pwdItem.pwdTextLine.text = pwdStr
    }

    function setErrorLabel(errTxt) {
            errorText.text = errTxt
            errorText.color = datovkaPalette.errorText
            errorText.visible = true
    }

    function canNextStep() {
         if (Qt.inputMethod.visible) {
            Qt.inputMethod.commit()
            Qt.inputMethod.hide()
        }
        if (userNameItem.textLine.displayText === "") {
            setErrorLabel(qsTr("The username must be filled in."))
            userNameItem.textLine.focus = true
            return
        }
        if (loginMethod === AcntData.LIM_UNAME_MEP) {
            if (mepCodeItem.textLine.displayText === "") {
                setErrorLabel(qsTr("The communication code must be filled in."))
                mepCodeItem.textLine.focus = true
                return
             } else {
                mepCode = mepCodeItem.textLine.displayText
             }
        } else if (pwdItem.pwdTextLine.text === "") {
            setErrorLabel(qsTr("The password must be filled in."))
            pwdItem.pwdTextLine.focus = true
            return
        } else {
            pwdStr = pwdItem.pwdTextLine.text
        }
        if (loginMethod === AcntData.LIM_UNAME_PWD_CRT) {
            if (certFilePath === "") {
                setErrorLabel(qsTr("The certificate file must be specified."))
                return
             }
        }
        acntId.username = userNameItem.textLine.displayText
        handlerLoader("CreateAccountPage4.qml")
    }

    function checkMandatoryItems() {
        if (loginMethod === AcntData.LIM_UNAME_MEP) {
            nextButton.enabled = (userNameItem.textLine.text.length > 0) && (mepCodeItem.textLine.text.length > 0)
        } else {
            nextButton.enabled = (userNameItem.textLine.text.length > 0) && (pwdItem.pwdTextLine.text.length > 0)
        }
    }

    function openCert(certPath) {
        if (certPath !== "") {
            certFilePath = removeFilePrefix(decodeURIComponent(certPath))
            certPathLabel.text = getFileNameFromPath(certFilePath)
            certPathLabel.visible = true
        }
    }

    function openFileDialog() {
        var title = qsTr("Choose a certificate")
        var filters = "*.pem"
        if (settings.useQmlFileDialog()) {
            console.log("Use QML File Dialog")
            fileDialogue.openFileDialog(title, filters)
            return
        }
        if (Qt.platform.os == "ios") {
            if (settings.useIosDocumentPicker()) {
                console.log("Use Native Ios Document Picker")
                iOSHelper.openDocumentPickerControllerForImport(IosImportAction.IMPORT_CERT,[])
            } else {
                console.log("Use Qt Core File Dialog")
                openCert(files.openFileDialog(title, null, filters))
            }
        } else {
            console.log("Use Qt Core File Dialog")
            openCert(files.openFileDialog(title, null, filters))
        }
    }

    FileDialogue {
        id: fileDialogue
        onAccepted: {
            if (fileDialogue.files.length > 0) {
                openCert(fileDialogue.files[0])
            }
        }
        Connections {
            target: iOSHelper
            function onCertFilesSelectedSig(filePaths) {
                if (filePaths.length > 0) {
                    openCert(filePaths[0])
                }
            }
        }
    }

    header: PageHeader {
        id: headerBar
        title: qsTr("Add Data Box")
        onBackClicked: {
            pageView.pop()
        }
    }

    Flickable {
        anchors.fill: parent
        contentHeight: flickContent.implicitHeight
        Pane {
            id: flickContent
            anchors.fill: parent
            ButtonGroup {
                id: loginMethodRadioGroup
            }
            Column {
                anchors.right: parent.right
                anchors.left: parent.left
                spacing: formItemVerticalSpacing
                ControlGroupItem {
                    spacing: 5
                    AccessibleLabelDark {
                        text: qsTr("Data-box title") + ": <b>" + accountName + "</b>"
                    }
                    AccessibleLabelDark {
                        text: qsTr("Data-box type") + ": <b>" + accountTypeName + "</b>"
                    }
                    AccessibleLabelDark {
                        text: qsTr("Login method") + ": <b>" + loginMethodName + "</b>"
                    }
                    SeparatorLine {
                    }
                }
                AccessibleTextInfo {
                    id: errorText
                    visible: false
                    horizontalAlignment: Text.AlignHCenter
                    color: datovkaPalette.errorText
                }
                ControlGroupItem {
                    TextLineItem {
                        id: userNameItem
                        textLineTitle: qsTr("Username")
                        placeholderText: qsTr("Enter the login name.")
                        inputMethodHints: Qt.ImhLowercaseOnly | Qt.ImhPreferLowercase | Qt.ImhNoPredictiveText
                        isPassword: false
                        textLine.onTextChanged: {
                            if (!isLowerCaseWithoutNonPrintableChars(userNameItem.textLine.text)) {
                                userNameItem.textLine.color = datovkaPalette.errorText
                                setErrorLabel(qsTr("Warning: The username should contain only combinations of lower-case letters and digits."))
                            } else {
                                userNameItem.textLine.color = datovkaPalette.text
                                errorText.color = userNameItem.textLine.color
                                errorText.visible = false
                            }
                            checkMandatoryItems()
                        }
                    }
                    AccessibleTextInfo {
                        text: qsTr("The username must consist of at least 6 characters without spaces (only combinations of lower-case letters and digits are allowed). Notification: The username is not a data-box ID.")
                    }
                }
                ControlGroupItem {
                    id: pwdSection
                    TimedPasswordLine {
                        id: pwdItem
                        anchors.left: parent.left
                        anchors.right: parent.right
                        textLineTitle: qsTr("Password")
                        placeholderText: qsTr("Enter the password.")
                        pwdEyeIconVisible: true
                        hideAfterMs: 0
                        pwdTextLine.onTextChanged: checkMandatoryItems()
                    }
                    AccessibleTextInfo {
                        text: qsTr("The password must be valid and non-expired. To check whether you've entered the password correctly you may use the icon in the field on the right. Note: You must fist change the password using the ISDS web portal if it is your very first attempt to log into the data box.")
                    }
                }
                ControlGroupItem {
                    id: mepSection
                    visible: false
                    TextLineItem {
                        id: mepCodeItem
                        textLineTitle: qsTr("Communication Code")
                        placeholderText: qsTr("Enter the communication code.")
                        isPassword: false
                        textLine.onTextChanged: checkMandatoryItems()
                    }
                    AccessibleTextInfo {
                        textFormat: TextEdit.RichText
                        onLinkActivated: Qt.openUrlExternally(link)
                        text: qsTr("The <a href=\"%1\">communication code</a> is a string which can be generated in the ISDS web portal. You have to have the Mobile Key application installed. The Mobile Key application needs to be paired with the corresponding data-box account on the ISDS web portal.").arg("https://datovka.nic.cz/redirect/mobile-manual-mep.html")
                    }
                }
                ControlGroupItem {
                    id: certificateSection
                    visible: false
                    AccessibleLabel {
                        text: qsTr("Certificate File")
                    }
                    AccessibleText {
                        id: certPathLabel
                        visible: false
                        width: parent.width
                        wrapMode: Text.WrapAnywhere
                    }
                    AccessibleButton {
                        text: qsTr("Choose certificate")
                        onClicked: openFileDialog()
                    }
                    AccessibleTextInfo {
                        text: qsTr("The certificate file is needed for authentication purposes. The supplied file must contain a certificate and its corresponding private key. Only PEM and PFX file formats are accepted.")
                    }
                }
                Row {
                    anchors.horizontalCenter: parent.horizontalCenter
                    spacing: formItemVerticalSpacing
                    AccessibleButton {
                        id: backButton
                        icon.source: "qrc:/ui/arrow-left.svg"
                        accessibleName: qsTr("Previous step")
                        text: qsTr("Back")
                        background: Rectangle {
                            radius: backButton.radius
                            color: backButtonBgColor
                        }
                        onClicked: handlerLoader("CreateAccountPage2.qml")
                    }
                    AccessibleButton {
                        id: nextButton
                        enabled: false
                        icon.source: "qrc:/ui/arrow-right.svg"
                        accessibleName: qsTr("Next step")
                        text: qsTr("Next")
                        onClicked: canNextStep()
                    }
                }
                PageIndicator {
                    currentIndex: 2
                    count: 4
                    anchors.horizontalCenter: parent.horizontalCenter
                }
            } // Column
        } // Pane
        ScrollIndicator.vertical: ScrollIndicator {}
    } // Flickable
} // Page
