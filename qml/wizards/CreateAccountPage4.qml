/*
 * Copyright (C) 2014-2025 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

import QtQuick 2.7
import QtQuick.Controls 2.15
import cz.nic.mobileDatovka 1.0
import cz.nic.mobileDatovka.account 1.0
import cz.nic.mobileDatovka.qmlIdentifiers 1.0

Page {

    signal handlerLoader(string pageFile)
    property string loginInfoText: ""

    Component.onCompleted: {
        if (loginMethod === AcntData.LIM_UNAME_PWD_CRT) {
            crtFilePath.visible = true
            loginInfoText = qsTr("During the log-in procedure you will be asked to enter a private key password.")
        } else if (loginMethod === AcntData.LIM_UNAME_MEP) {
            rememberPassword.visible = false
            loginInfoText = qsTr("During the log-in procedure you will be asked to confirm a notification in the Mobile Key application.")
        } else if (loginMethod === AcntData.LIM_UNAME_PWD_TOTP) {
            loginInfoText = qsTr("During the log-in procedure you will be asked to enter a code which you should obtain via an SMS.")
        } else if (loginMethod === AcntData.LIM_UNAME_PWD_HOTP) {
            loginInfoText = qsTr("During the log-in procedure you will be asked to enter a security code.")
        }
    }

    function setPageElementsInvisible(visible) {
        headerBar.enabled = !visible
        flickContent.enabled = !visible
        createAccountSection.visible = !visible
        busyElementSection.visible = visible
        busyIndicator.running = visible
    }

    function doIsdsAction(action, acntId) {
        setPageElementsInvisible(true)
        isds.doIsdsAction(action, acntId)
    }

    function createAccount() {
        var errText = accounts.createAccount(loginMethod,
            accountName, acntId.username, pwdStr, mepCode,
            acntId.testing, rememberPassword.checked,
            useLS.checked, useSyncWithAll.checked, certFilePath)
        if (errText === "") {
            doIsdsAction("addNewAccount", acntId)
        } else {
            okMsgBox.error(qsTr("Creating data box: %1").arg(acntId.username),
                qsTr("Data box '%1' could not be created.").arg(accountName),
                errText);
        }
    }

    AcntId {
        id: actionAcntId
    }

    Connections {
        target: isds
        function onLoginFailed(userName, testing) {
            setPageElementsInvisible(false)
        }
        function onDownloadAccountInfoFinishedSig(userName, testing, success, errTxt) {
            /* INI settings are saved automatically. */
            setPageElementsInvisible(false)
            onAddNewAccount(userName, testing, accountName)
        }
        function onRunGetAccountInfoSig(userName, testing) {
             actionAcntId.username = userName
             actionAcntId.testing = testing
             /* INI settings are saved automatically. */
             isds.getAccountInfo(actionAcntId)
        }
    }

    header: PageHeader {
        id: headerBar
        title: qsTr("Add Data Box")
        onBackClicked: {
            pageView.pop()
        }
    }

    Flickable {
        anchors.fill: parent
        contentHeight: flickContent.implicitHeight
        Pane {
            id: flickContent
            anchors.fill: parent
            ButtonGroup {
                id: loginMethodRadioGroup
            }
            Column {
                anchors.right: parent.right
                anchors.left: parent.left
                spacing: formItemVerticalSpacing
                ControlGroupItem {
                    spacing: 5
                    AccessibleLabelDark {
                        text: qsTr("Data-box title") + ": <b>" + accountName + "</b>"
                    }
                    AccessibleLabelDark {
                        text: qsTr("Data-box type") + ": <b>" + accountTypeName + "</b>"
                    }
                    AccessibleLabelDark {
                        text: qsTr("Login method") + ": <b>" + loginMethodName + "</b>"
                    }
                    AccessibleLabelDark {
                        text: qsTr("Username") + ": <b>" + acntId.username + "</b>"
                    }
                    AccessibleTextInfo {
                        id: crtFilePath
                        visible: false
                        text: qsTr("Certificate") + ": <b>" + getFileNameFromPath(certFilePath) + "</b>"
                    }
                    SeparatorLine {
                    }
                }
                AccessibleLabel {
                    text: qsTr("Local Data-Box Settings")
                }
                AccessibleSwitch {
                    id: rememberPassword
                    text: qsTr("Remember password")
                    checked: true
                }
                AccessibleSwitchInfo {
                    id: useLS
                    checked: true
                    switchText: qsTr("Use local storage")
                    titleText: switchText
                    infoText: (useLS.sw.checked) ?
                        qsTr("Messages and attachments will be locally stored. No active internet connection is needed to access locally stored data.") :
                        qsTr("Messages and attachments will be stored only temporarily in memory. These data will be lost on application exit.")
                }
                AccessibleSwitchInfo {
                    id: useSyncWithAll
                    checked: true
                    switchText: qsTr("Synchronise with all")
                    titleText: switchText
                    infoText: useSyncWithAll.sw.checked ?
                        qsTr("The data box will be included into the synchronisation process of all data boxes.") :
                        qsTr("The data box won't be included into the synchronisation process of all data boxes.")
                }
                SeparatorLine {
                }
                ControlGroupItem {
                    id: createAccountSection
                    spacing: formItemVerticalSpacing
                    AccessibleTextInfo {
                        horizontalAlignment: Text.AlignHCenter
                        text: qsTr("All done. The data box will be added only when the application successfully logs into the data box using the supplied login credentials.") + " " + loginInfoText
                    }
                    Row {
                        anchors.horizontalCenter: parent.horizontalCenter
                        spacing: formItemVerticalSpacing
                        AccessibleButton {
                            id: backButton
                            icon.source: "qrc:/ui/arrow-left.svg"
                            accessibleName: qsTr("Previous step")
                            text: qsTr("Back")
                            background: Rectangle {
                                radius: backButton.radius
                                color: backButtonBgColor
                            }
                            onClicked: handlerLoader("CreateAccountPage3.qml")
                        }
                        AccessibleButton {
                            icon.source: "qrc:/ui/databox-add.svg"
                            accessibleName: qsTr("Connect to data box.")
                            text: qsTr("Connect Data Box")
                            onClicked: createAccount()
                        }
                    }
                    PageIndicator {
                        currentIndex: 3
                        count: 4
                        anchors.horizontalCenter: parent.horizontalCenter
                    }
                }
                ControlGroupItem {
                    id: busyElementSection
                    visible: false
                    BusyIndicator {
                        id: busyIndicator
                        anchors.horizontalCenter: parent.horizontalCenter
                        running: false
                    }
                    AccessibleTextInfo {
                        horizontalAlignment: Text.AlignHCenter
                        text: qsTr("Logging into the data box %1 ...").arg(acntId.username)
                    }
                }
            } // Column
        } // Pane
        ScrollIndicator.vertical: ScrollIndicator {}
    } // Flickable
} // Page
