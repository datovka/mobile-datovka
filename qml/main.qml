/*
 * Copyright (C) 2014-2025 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

import QtQuick 2.15
import QtQuick.Controls 2.15
import QtQuick.Window 2.1
import QtQuick.Controls.Material 2.15
import cz.nic.mobileDatovka 1.0
import cz.nic.mobileDatovka.iOsHelper 1.0
import cz.nic.mobileDatovka.qmlIdentifiers 1.0

ApplicationWindow {

    id: mainWindow
    visible: true
    title: "Datovka"

    width: 375
    minimumWidth: 320
    height: 667
    minimumHeight: 480

    /* Used for iOS only. Test if iOS system theme is dark or light mode. */
    SystemPalette { id: systemPalette; colorGroup: SystemPalette.Active }
    function iOSDarkTheme() {
        return Qt.colorEqual(systemPalette.text, "#ffffff")
    }

    property bool iOS: settings.isIos()
    property bool darkMode: iOS ? iOSDarkTheme() : settings.darkTheme()

    /* Do not change these. */
    property color iosButtonBgColor: "#007AFF"
    property color aBtnBgColorLight: "#007AFF"
    property color aBtnBgColorDark: "#F6C315"
    property color darkGrey: "#202020"
    property color darkGreySel: "#303030"
    property color lightGrey: "#F0F0F0"
    property color lightGreySel: "#DCDCDC"

    /* Button background and text colors. */
    property color buttonBgColor: iOS ? iosButtonBgColor : darkMode ? aBtnBgColorDark : aBtnBgColorLight
    property color buttonTextColor: iOS ? "white" : darkMode ? "black" : "white"
    property color backButtonBgColor: darkMode ? "#A9A9A9" : "#808080"

    Material.theme: darkMode ? Material.Dark : Material.Light
    Material.background: darkMode ? "black" : "white"
    Material.foreground: darkMode ? "white" : "black"
    Material.accent: buttonBgColor
    Material.primary: darkMode ? darkGrey : lightGrey

    /* Set datovka color palette for dark and light theme. */
    QtObject {
        id: datovkaPalette
        property string theme: darkMode ? "dark" : "light"
        property color base: darkMode ? "black" : "white"
        property color selected: darkMode ? darkGreySel : lightGreySel
        property color greyText: darkMode ? "#BEBEBE" : "#404040"
        property color text: darkMode ? "white" : "black"
        property color line: darkMode ? darkGreySel : lightGreySel
        property color account: darkMode ? darkGrey : lightGrey
        property color errorText: "red"
        property color highlightedText: buttonBgColor
        property color popupIconColor: greyText
    }

    /* Set material UI theme and controls dimensions. */
    property int appPixelSize: Qt.application.font.pixelSize
    font.pixelSize: appPixelSize
    property int textFontPixelSizeSmall: appPixelSize - 3
    property int textFontPixelSizeLarge: appPixelSize + 3
    property int defaultMargin: Math.round(Screen.pixelDensity)
    property int baseDelegateHeight: font.pixelSize * 3
    property int roundButtonSize: font.pixelSize * 2.5
    property int listViewDelegateHeight: baseDelegateHeight * 2
    property int formItemVerticalSpacing: defaultMargin * 2
    property var nestedStack: null
    property var accountModel: mainAccountModel

    /* Define all pages for stackview. */
    property Component pageAccountList: PageAccountList {}
    property Component pageAccountDetail: PageAccountDetail {}
    property Component pageBackupData: PageBackupData {}
    property Component pageChangePassword: PageChangePassword {}
    property Component pageContactList: PageContactList {}
    property Component pageConvertDatabase: PageConvertDatabase {}
    property Component pageDataboxDetail: PageDataboxDetail {}
    property Component pageDataboxSearch: PageDataboxSearch {}
    property Component pageGovService: PageGovService {}
    property Component pageGovServiceList: PageGovServiceList {}
    property Component pageImportMessage: PageImportMessage {}
    property Component pageMessageListSwipeView: PageMessageListSwipeView {}
    property Component pageMessageDetail: PageMessageDetail {}
    property Component pageMessageEnvelope: PageMessageEnvelope {}
    property Component pageMessageList: PageMessageList {}
    property Component pagePrefsEditor: PagePrefsEditor {}
    property Component pageRecordsManagementUpload: PageRecordsManagementUpload {}
    property Component pageRepairDatabase: PageRepairDatabase {}
    property Component pageSendMessage: PageSendMessage {}
    property Component pageSettingsAccount: PageSettingsAccount {}
    property Component pageAccountWizard: CreateAccountLoader {}
    property Component pageAccountLogo: PageAccountLogo {}

    property string currentAcntName
    property AcntId currentAcntId: null

    function textColorRed(txt) {
        return "<div style=\"color:red\">" + txt + "</div>"
    }

    /* Test if string is lowercase without non-printable characters. */
    function isLowerCaseWithoutNonPrintableChars(str) {
        return (str === str.toLowerCase()) && (!(RegExp("\\s").test(str)))
    }

    /* Return resource file path based on theme. */
    function getImgThemeFilePath(filename) {
        return ("qrc:/ui/" + datovkaPalette.theme + "/" +filename)
    }

    /* Return file name from file path */
    function getFileNameFromPath(filePath) {
        return filePath.replace(/^.*[\\\/]/, '')
    }

    /* Return file name path without file prefix */
    function removeFilePrefix(filePath) {
        return filePath.replace("file://", '')
    }

    function showPopupAccountMenu(acntName, acntId) {
        popupMenuAccount.acntName = acntName
        popupMenuAccount.acntId = acntId
        popupMenuAccount.open()
    }

    /* Set account list page as main page if an account has added. */
    function onAddNewAccount(userName, testing, accountName) {
        if (accountModel.rowCount() === 1) {
            pageView.clear()
            newAccountId.username = userName
            newAccountId.testing = testing
            pageView.push(pageMessageListSwipeView, {
                "pageViewSv": pageView,
                "acntNameSv": accountName,
                "acntIdSv": newAccountId
            })
            currentAcntName = accountName
            currentAcntId = newAccountId
            drawerMenuDatovka.insertItem()
        } else if (accountModel.rowCount() > 1) {
            pageView.clear()
            pageView.push(pageAccountList, {})
            drawerMenuDatovka.removeItem()
        }
    }

    function loadZfo(filePath) {
        if (filePath === "") {
            return
        }
        var fileContent = files.rawFileContent(filePath)
        if (fileContent === "") {
            return
        }
        pageView.push(pageMessageDetail, { "pageView": pageView, "rawZfoContent": fileContent }, StackView.Immediate)
    }

    function openFileDialog() {
        var title = qsTr("Select ZFO Message")
        var filters = "*.zfo"
        if (settings.useQmlFileDialog()) {
            console.log("Use QML File Dialog")
            fD.openFileDialog(title, filters)
            return
        }
        if (Qt.platform.os == "ios") {
            if (settings.useIosDocumentPicker()) {
                console.log("Use Native Ios Document Picker")
                iOSHelper.openDocumentPickerControllerForImport(IosImportAction.IMPORT_VIEW_ZFO,[])
            } else {
                console.log("Use Qt Core File Dialog")
                loadZfo(files.openFileDialog(title, null, filters))
            }
        } else {
            console.log("Use Qt Core File Dialog")
            loadZfo(files.openFileDialog(title, null, filters))
        }
    }

    FileDialogue {
        id: fD
        onAccepted: {
            if (fD.files.length > 0) {
                loadZfo(fD.files[0])
            }
        }
        Connections {
            target: iOSHelper
            function onViewZfoFilesSelectedSig(filePaths) {
                if (filePaths.length > 0) {
                    loadZfo(filePaths[0])
                }
            }
        }
    }

    AcntId {
        id: newAccountId
    }

    PopupMenuAccount {
        id: popupMenuAccount
    }

    Timer {
        id: syncAllAfterStartup
        interval: 1000;
        running: false;
        repeat: false
        onTriggered: isds.syncAllAccounts()
    }

    MessageBox {
        id: mepWaitBox
        onAcceptAndClosed: {
            isds.loginMepCancel()
        }
    }

    MessageBox {
        id: donationMsgBox
        onAcceptAndClosed: {
            Qt.openUrlExternally("https://datovka.nic.cz/redirect/donation-mobile.html")
        }
        onRejectAndClosed: {
            settings.planNextDonationViewTime(true)
        }
    }

    MessageBox {
        id: okMsgBox
    }

    ChangeLogBox {
        id: changeLogBox
    }

    DrawerMenuDatovka {
        id: drawerMenuDatovka
    }

    AcntId {
        id: actionAcntId
    }

    Connections {
        target: files
        function onShowMessageBox(title, text, detailText) {
            okMsgBox.error(title, text, detailText)
        }
    }

    Connections {
        target: isds
        function onOpenDialogRequest(isdsAction, loginMethod, userName, testing, title, text, placeholderText, hidePwd) {
            var component = Qt.createComponent("qrc:/qml/dialogues/InputDialogue.qml");
            if (component !== null ) {
                var inputDialog = component.createObject(mainWindow, {});
                if (inputDialog !== null ) {
                    actionAcntId.username = userName
                    actionAcntId.testing = testing
                    inputDialog.openInputDialog(isdsAction, loginMethod, actionAcntId, title, text, placeholderText, hidePwd)
                }
            }
        }
        function onOpenYesNoDialogue(isdsAction, loginMethod, userName, testing, title, text, detailText) {
            var component = Qt.createComponent("qrc:/qml/dialogues/YesNoDialogue.qml");
            if (component !== null ) {
                var yesNoDialog = component.createObject(mainWindow, {});
                if (yesNoDialog !== null ) {
                    actionAcntId.username = userName
                    actionAcntId.testing = testing
                    yesNoDialog.question(isdsAction, loginMethod, actionAcntId, title, text, detailText)
                }
            }
        }
        function onUnsuccessfulLoginToIsdsSig(userName, testing) {
            actionAcntId.username = userName
            actionAcntId.testing = testing
            accounts.removeAccount(actionAcntId)
            /* INI settings are saved automatically. */
        }
        function onShowMepWaitDialog(userName, show) {
            if (show) {
                mepWaitBox.showMepWaitBox(qsTr("Mobile Key Login"), qsTr("Waiting for acknowledgement from the Mobile key application for the data box '%1'.").arg(userName), qsTr("Cancel"))
            } else {
                mepWaitBox.close()
            }
        }
        function onShowLoginErrMsgBox(title, text, detailText) {
            okMsgBox.error(title, text, detailText)
        }
    }

    Component.onCompleted: {
        if (settings.pinConfigured()) {
            if (mainStack.currentItem.objectName === "appArea") {
                mainStack.push(lockScreen, StackView.Immediate)
                pinCodeInput.forceActiveFocus()
                Qt.inputMethod.show()
            }
        } else {
            pageView.visible = true
        }
    }

    StackView {
        id: mainStack
        anchors.fill: parent
        initialItem: appArea
        Item {
            id: appArea
            objectName: "appArea"
            anchors.fill: parent
            StackView.onActivated: {
                mainStack.forceActiveFocus()
            }
            StackView {
                id: pageView
                anchors.fill: parent
                visible: false
                initialItem: if (accountModel.rowCount() > 1) {
                        "qrc:/qml/pages/PageAccountList.qml"
                    } else if (accountModel.rowCount() === 1) {
                        "qrc:/qml/pages/PageMessageListSwipeView.qml"
                    } else {
                        "qrc:/qml/pages/PageWelcome.qml"
                    }
                Component.onCompleted: {
                    nestedStack = pageView
                }
            }
        }

        Page { /* Pin lock screen. */
            id: lockScreen
            objectName: "lockScreen"
            visible: false
            Rectangle  {
                anchors.fill: parent
                anchors.margins: defaultMargin
                color: datovkaPalette.base
                ControlGroupItem {
                    spacing: formItemVerticalSpacing
                    Item {
                        width: parent.width
                        height: parent.height / 4
                    }
                    ControlGroupItem {
                        spacing: defaultMargin
                        AccessibleTextField {
                            id: pinCodeInput
                            width: parent.width / 2.5
                            echoMode: TextInput.Password
                            passwordMaskDelay: 500
                            inputMethodHints: Qt.ImhNoPredictiveText | Qt.ImhDigitsOnly | Qt.ImhSensitiveData | Qt.ImhNoAutoUppercase
                            placeholderText: qsTr("Enter PIN code")
                            anchors.horizontalCenter: parent.horizontalCenter
                            horizontalAlignment: TextInput.AlignHCenter
                            onDisplayTextChanged: {
                                pinCodeText.color = datovkaPalette.greyText
                                pinCodeText.text = qsTr("Enter PIN code")
                            }
                            onEditingFinished: {
                                if (pinCodeInput.text.length > 0) {
                                    settings.verifyPin(pinCodeInput.text.toString())
                                }
                            }
                        }
                        AccessibleTextInfo {
                            id: pinCodeText
                            horizontalAlignment: Text.AlignHCenter
                            text: qsTr("Enter PIN code")
                        }
                        AccessibleButton {
                            anchors.horizontalCenter: parent.horizontalCenter
                            icon.source: "qrc:/ui/checkmark.svg"
                            height: datovkaLogo.height
                            text: qsTr("Enter")
                            accessibleName: qsTr("Verify PIN code.")
                            enabled: (pinCodeInput.displayText.length > 0)
                            onClicked: settings.verifyPin(pinCodeInput.text.toString())
                        }
                    }
                    Item {
                        width: parent.width
                        height: formItemVerticalSpacing
                    }
                    Image {
                        id: datovkaLogo
                        anchors.horizontalCenter: parent.horizontalCenter
                        source: "qrc:/datovka.png"
                    }
                    AccessibleTextInfo {
                        horizontalAlignment: Text.AlignHCenter
                        text: qsTr("Version") + ": " + settings.appVersion()
                    }
                }
                Timer {
                    id: pinTimer
                    interval: 1000;
                    running: false;
                    repeat: false
                    onTriggered: {
                        pinCodeInput.clear()
                        pinCodeText.color = datovkaPalette.errorText
                        pinCodeText.text = qsTr("Wrong PIN code.")
                        pinCodeInput.forceActiveFocus()
                        Qt.inputMethod.show()
                    }
                }
                Connections {
                    target: settings
                    function onSendPinReply(success) {
                        if (success) {
                            Qt.inputMethod.hide()
                            if (mainStack.currentItem.objectName === "lockScreen") {
                                mainStack.pop(StackView.Immediate)
                                pageView.visible = true
                            }
                            pinCodeInput.clear()
                        } else {
                            pinTimer.start()
                            pinCodeText.text = qsTr("Verifying PIN...")
                        }
                    }
                } // Connections
            } // Rectangle
        } // Component

        Connections {
            target: locker
            function onLockApp() {
                if (mainStack.currentItem.objectName === "appArea") {
                    mainStack.push(lockScreen, StackView.Immediate)
                }
            }
        }
        Connections {
            target: interactionZfoFile
            function onDisplayZfoFileContent(filePath) {
                console.log("Showing ZFO file: " + filePath)
                var fileContent = files.rawFileContent(filePath)
                mainStack.push(pageMessageDetail, {
                        "pageView": mainStack,
                        "rawZfoContent": fileContent
                    }, StackView.Immediate)
                files.deleteTmpFileFromStorage(filePath)
            }
        }

        Connections {
            target: settings
            function onRunOnAppStartUpSig() {
                console.log("Running actions after application start-up.")
                var isNewVersion = settings.isNewVersion()
                if (isNewVersion) {
                    changeLogBox.showChangeLog()
                }
                var areNews = messages.checkNewDatabasesFormat()
                if (!areNews) {
                    pageView.push(pageConvertDatabase, {
                        "pageView": pageView
                    }, StackView.Immediate)
                } else {
                    if (settings.pwdExpirationDays() > 0) {
                        var exprPwd = accounts.getPasswordExpirationInfo()
                        if (exprPwd !== "") {
                            okMsgBox.error(qsTr("Password Expiration"),
                                qsTr("Passwords of users listed below have expired or are going to expire within few days. You may change the passwords from this application if they haven't already expired. Expired passwords can only be changed in the ISDS web portal."),
                                exprPwd)
                        }
                    }
                    if (settings.syncAfterCleanAppStart()) {
                        syncAllAfterStartup.start()
                    }
                    if (settings.isTimeToShowDonationDlg()) {
                        donationMsgBox.customBox(qsTr("Donation Request"),
                            qsTr("Thank you for using our application."),
                            qsTr("Please consider giving a donation to support the development of this application."),
                            qsTr("Donate"))
                    }
                    if (settings.isTimeToShowRatingDlg()) {
                        settings.openRatingDlg()
                    }
                }
            }
        }

        Connections {
            target: preferencesWatcher
            function onFileWriteError(filePath) {
                okMsgBox.error(qsTr("Cannot Write Configuration File"),
                    qsTr("An error occurred while attempting to save the configuration."),
                    qsTr("Cannot write file '%1'.").arg(filePath))
            }
        }

        function navigateBack(mainStack, nestedStack, event) {
            if (event.key === Qt.Key_Back) {
                event.accepted = true
                if (nestedStack === null) {
                    return
                }
                if (mainStack.currentItem.objectName === "lockScreen") {
                    console.log("Ignoring back button on lock screen.")
                } else if (mainStack.depth > 1) {
                    mainStack.pop(StackView.Immediate)
                } else if (nestedStack.depth > 1) {
                    nestedStack.pop()
                } else {
                    event.accepted = false
                    Qt.quit()
                }
            }
        }

        /* Android back button. */
        focus: true
        Keys.onReleased: event => navigateBack(mainStack, nestedStack, event)
    } // StackView
}
