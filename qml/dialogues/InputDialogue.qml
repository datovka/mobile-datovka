/*
 * Copyright (C) 2014-2025 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

import QtQuick 2.7
import QtQuick.Layouts 1.3
import QtQuick.Controls 2.15
import cz.nic.mobileDatovka 1.0
import cz.nic.mobileDatovka.account 1.0
import cz.nic.mobileDatovka.qmlIdentifiers 1.0


/*
 * Provides a simple input dialog.
 */
Popup {
    id: root
    anchors.centerIn: parent
    modal: true
    closePolicy: Popup.NoAutoClose

    property alias inputTextField: rootPwd
    property int preferredMinWidth: 260
    property string dIsdsAction
    property var dPwdType

    AcntId {
        id: actionAcntId
    }

    function openInputDialog(isdsAction, pwdType, acntId, title, text, placeholderText, hidePwd) {
        dIsdsAction = isdsAction
        dPwdType = pwdType
        actionAcntId.username = acntId.username
        actionAcntId.testing = acntId.testing
        titleText.text = title
        rootText.text = text
        rootPwd.clear()
        rootPwd.placeholderText = placeholderText
        rootPwd.echoMode = (hidePwd) ? TextInput.Password : TextInput.Normal
        if (pwdType === AcntData.LIM_UNAME_PWD_TOTP || pwdType === AcntData.LIM_UNAME_PWD_HOTP) {
            rootPwd.inputMethodHints = Qt.ImhPreferNumbers
        } else {
            rootPwd.inputMethodHints = Qt.ImhNone
        }
        root.open()
        rootPwd.forceActiveFocus()
    }

    /* Default code to be run when user accepts the dialogue. */
    function dfltAccepted() {
        isds.inputDialogReturnPassword(dIsdsAction, dPwdType, actionAcntId, rootPwd.text.toString())
    }

    /* Default code to be run when user rejects the dialogue. */
    function dfltRejected() {
        isds.inputDialogCancel(dIsdsAction, actionAcntId)
    }

    signal acceptAndClosed()
    signal rejectAndClosed()

    background: Rectangle {
        color: datovkaPalette.base
        border.color: datovkaPalette.text
        radius: defaultMargin
    }

    ColumnLayout {
        spacing: defaultMargin * 2
        AccessibleText {
            id: titleText
            font.bold: true
            Layout.alignment: Qt.AlignCenter
        }
        AccessibleText {
            id: rootText
            Layout.preferredWidth: root.preferredMinWidth
            horizontalAlignment : Text.Center
            wrapMode: Text.Wrap
        }
        AccessibleTextField {
            id: rootPwd
            Layout.fillWidth: true
            Layout.alignment: Qt.AlignLeft | Qt.AlignBaseline
            echoMode: TextInput.Normal
            horizontalAlignment: TextInput.AlignHCenter
            passwordMaskDelay: 500
        }
        RowLayout {
            Layout.alignment: Qt.AlignCenter
            spacing: defaultMargin * 2
            AccessibleButton {
                Layout.alignment: Qt.AlignCenter
                text: qsTr("Close")
                onClicked: {
                    root.close()
                    dfltRejected()
                    root.rejectAndClosed()
                }
            }
            AccessibleButton {
                Layout.alignment: Qt.AlignCenter
                text: qsTr("OK")
                onClicked: {
                    root.close()
                    dfltAccepted()
                    root.acceptAndClosed()
                }
            }
        }
    }
}
