/*
 * Copyright (C) 2014-2025 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

import QtQuick 2.7
import QtQuick.Layouts 1.3
import QtQuick.Controls 2.15
import cz.nic.mobileDatovka 1.0
import cz.nic.mobileDatovka.account 1.0
import cz.nic.mobileDatovka.qmlIdentifiers 1.0


/*
 * Provides a simple yes/no dialog.
 */
Popup {
    id: root
    anchors.centerIn: parent
    modal: true
    closePolicy: Popup.NoAutoClose

    property int preferredMinWidth: 260
    property string dIsdsAction
    property var dLoginMethod

    AcntId {
        id: actionAcntId
    }

     /* Public interface. */
    function question(isdsAction, loginMethod, acntId, title, text, detailText) {
        dIsdsAction = isdsAction
        dLoginMethod = loginMethod
        actionAcntId.username = acntId.username
        actionAcntId.testing = acntId.testing
        yesButton.visible = true
        noButton.visible = true
        titleText.text = title
        mainText.text = text
        dDetailText.text = detailText
        root.open()
    }

    /* Default code to be run when user accepts the dialogue. */
    function dfltAccepted() {
        isds.yesNoDialogResult(dIsdsAction, dLoginMethod, actionAcntId, true)
    }

    /* Default code to be run when user rejects the dialogue. */
    function dfltRejected() {
        isds.yesNoDialogResult(dIsdsAction, dLoginMethod, actionAcntId, false)
    }

    signal acceptAndClosed()
    signal rejectAndClosed()

    background: Rectangle {
        color: datovkaPalette.base
        border.color: datovkaPalette.text
        radius: defaultMargin
    }

    ColumnLayout {
        spacing: defaultMargin * 2
        AccessibleText {
            id: titleText
            font.bold: true
            Layout.alignment: Qt.AlignCenter
        }
        AccessibleText {
            id: mainText
            Layout.preferredWidth: root.preferredMinWidth
            horizontalAlignment : Text.Center
            wrapMode: Text.Wrap
        }
        AccessibleText {
            id: dDetailText
            Layout.preferredWidth: root.preferredMinWidth
            horizontalAlignment : Text.Center
            wrapMode: Text.Wrap
        }
        RowLayout {
            Layout.alignment: Qt.AlignCenter
            spacing: defaultMargin * 2
            AccessibleButton {
                id: yesButton
                Layout.alignment: Qt.AlignCenter
                text: qsTr("Yes")
                onClicked: {
                    dfltAccepted()
                    root.acceptAndClosed()
                    root.close()
                }
            }
            AccessibleButton {
                id: noButton
                Layout.alignment: Qt.AlignCenter
                text: qsTr("No")
                onClicked: {
                    dfltRejected()
                    root.rejectAndClosed()
                    root.close()
                }
            }
        }
    }
}
