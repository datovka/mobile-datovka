/*
 * Copyright (C) 2014-2025 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

import QtQuick 2.7
import QtQuick.Controls 2.15
import QtQuick.Layouts 1.2
import cz.nic.mobileDatovka 1.0
import cz.nic.mobileDatovka.account 1.0
import cz.nic.mobileDatovka.files 1.0
import cz.nic.mobileDatovka.messages 1.0
import cz.nic.mobileDatovka.models 1.0
import cz.nic.mobileDatovka.qmlIdentifiers 1.0

Popup {
    id: popup
    modal: true
    anchors.centerIn: Overlay.overlay
    padding: 0

    property string acntName
    property AcntId acntId: null
    property int msgType
    property string msgId
    property var messageModel: null
    property var attachmentModel: null /* The QVariant holds actually a pointer to the model. */
    property bool loadFromZfo: false
    property bool loadFromExternalZfo: false
    property bool canDelete: false

    function saveFile(iCloudUpload) {
        if (loadFromZfo || loadFromExternalZfo) {
            files.saveZfoFilesToDisk(acntId, msgId, msgType, iCloudUpload)
        } else {
            files.saveMsgFilesToDisk(acntId, msgId, msgType, MsgAttachFlag.MSG_ZFO, iCloudUpload)
        }
    }

    MessageBox {
        id: yesNoMsgBox
        onAcceptAndClosed: {
            messages.deleteMessageFromDbs(accountModel, messageModel, acntId, msgId)
            messages.fillMessageList(messageModel, acntId, msgType)
            pageView.pop()
        }
    }

    MessageBox {
        id: yesNoMsgBoxiOS
        onAcceptAndClosed: {
            saveFile(true)
            pageView.pop()
        }
        onRejectAndClosed: {
            saveFile(false)
            pageView.pop()
        }
    }

    ColumnLayout {
        anchors.fill: parent
        spacing: 0
        PopupItem {
            icon.source: "qrc:/ui/email-outline.svg"
            text: qsTr("View Message Envelope")
            onClicked: pageView.push(pageMessageEnvelope, {"pageView": pageView, "acntId": acntId, "msgType": msgType, "msgId": msgId, "loadFromZfo": loadFromZfo, "loadFromExternalZfo": loadFromExternalZfo, "isDeliveryInfo": false}, StackView.Immediate)
        }
        PopupItem {
            icon.source: "qrc:/ui/email-open-outline.svg"
            text: qsTr("View Acceptance Info")
            onClicked: pageView.push(pageMessageEnvelope, {"pageView": pageView, "acntId": acntId, "msgType": msgType, "msgId": msgId, "loadFromZfo": loadFromZfo, "loadFromExternalZfo": loadFromExternalZfo, "isDeliveryInfo": true}, StackView.Immediate)
        }
        PopupItem {
            icon.source: "qrc:/ui/datovka-email-sample.svg"
            text: qsTr("Use as Template")
            onClicked: pageView.push(pageSendMessage, {"pageView": pageView, "acntName" : acntName, "acntId": acntId, "msgId": msgId, "msgType": msgType, "action": "template"}, StackView.Immediate)
        }
        PopupItem {
            icon.source: "qrc:/ui/briefcase.svg"
            text: qsTr("Upload to Records Management")
            onClicked: pageView.push(pageRecordsManagementUpload, {"pageView": pageView, "acntName" : acntName, "acntId": acntId, "msgId": msgId, "msgType": msgType, "messageModel": messageModel}, StackView.Immediate)
        }
        PopupItem {
            icon.source: "qrc:/ui/email-zfo.svg"
            text: qsTr("Send Message ZFO by E-Mail")
            onClicked: {
                if (loadFromZfo) {
                    files.sendAttachmentsWithEmail(attachmentModel, acntId, msgId, msgType, MsgAttachFlag.MSG_ZFO)
                } else if (loadFromExternalZfo) {
                    files.sendAttachmentEmailZfo(attachmentModel, acntId, msgId, msgType, msgAnnotation, emailBody, MsgAttachFlag.MSG_ZFO)
                } else {
                    files.sendMsgFilesWithEmail(acntId, msgId, msgType, MsgAttachFlag.MSG_ZFO)
                }
            }
        }
        PopupItem {
            icon.source: "qrc:/ui/save-to-disk-zfo.svg"
            text: qsTr("Save Message ZFO")
            onClicked: {
                if (Qt.platform.os == "ios") {
                    yesNoMsgBoxiOS.question(qsTr("Save Message ZFO: %1").arg(msgId),
                    qsTr("You can export files into iCloud or into device local storage."),
                    qsTr("Do you want to export files to Datovka iCloud container?"))
                } else  {
                    saveFile(false)
                }
            }
        }
        PopupItem {
            visible: canDelete
            icon.source: "qrc:/ui/delete.svg"
            text: qsTr("Delete Message")
            onClicked: yesNoMsgBox.question(qsTr("Delete Message: %1").arg(msgId),
                qsTr("Do you want to delete the message '%1'?").arg(msgId),
                qsTr("Note: It will delete all attachments and message information from the local database."))
        }
    }
}
