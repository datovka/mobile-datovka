/*
 * Copyright (C) 2014-2025 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

import QtQuick 2.15
import QtQuick.Controls 2.15

Drawer {
    id: drawer

    width: Math.min(parent.width, parent.height) / 3 * 2
    height: parent.height
    interactive: pageView.depth === 1

    property bool isWelcomePage: false

    onClosed: {
        listView.positionViewAtIndex(0, ListView.Beginning)
    }

    function insertItem() {
        if (accountModel.rowCount() === 1) {
            var exists = false
            for (var i = 0; i < listModel.count; i++) {
                var item = listModel.get(i)
                if (item.type === 5) {
                    exists = true
                }
            }
            if (!exists) {
                listModel.insert(1, { "type": 5, "title": qsTr("Data-box properties"), "action": "manageDatabox", "icon": "qrc:/ui/databox-settings.svg" })
            }
        }
    }

    function removeItem() {
        if (accountModel.rowCount() !== 1) {
            for (var i = 0; i < listModel.count; i++) {
                var item = listModel.get(i)
                if (item.type === 5) {
                    listModel.remove(i)
                }
            }
        } else insertItem()
    }

    Component.onCompleted: {
        if (accountModel.rowCount() === 1) {
            listModel.insert(1, { "type": 5, "title": qsTr("Data-box properties"), "action": "manageDatabox", "icon": "qrc:/ui/databox-settings.svg" })
        }
    }

    /*
     * type 0 = Label without action
     * type 1 = Push page action
     * type 2 = Open external URL action
     * type 3 = Special page push
     * type 4 = Custom action
     */
    ListModel {
        id: listModel
        ListElement { type: 0; title: qsTr("Data Boxes") }
        ListElement { type: 1; title: qsTr("Add Data Box"); source: "qrc:/qml/pages/PageSettingsAccount.qml"; icon: "qrc:/ui/databox-add.svg" }
        ListElement { type: 0; title: qsTr("Messages") }
        ListElement { type: 4; title: qsTr("View Message"); action: "viewZfoMsg"; icon: "qrc:/ui/datovka-file-zfo.svg" }
        ListElement { type: 1; title: qsTr("Search Message"); source: "qrc:/qml/pages/PageMessageSearch.qml"; icon: "qrc:/ui/magnify.svg" }
        ListElement { type: 1; title: qsTr("Import Message"); source: "qrc:/qml/pages/PageImportMessage.qml"; icon: "qrc:/ui/file-import.svg" }
        ListElement { type: 0; title: qsTr("Settings") }
        ListElement { type: 1; title: qsTr("General"); source: "qrc:/qml/pages/PageSettingsGeneral.qml"; icon: "qrc:/ui/settings.svg" }
        ListElement { type: 1; title: qsTr("Synchronisation"); source: "qrc:/qml/pages/PageSettingsSync.qml"; icon: "qrc:/ui/sync-all.svg" }
        ListElement { type: 1; title: qsTr("Security && PIN"); source: "qrc:/qml/pages/PageSettingsPin.qml"; icon: "qrc:/ui/key-variant.svg" }
        ListElement { type: 1; title: qsTr("Records Management"); source: "qrc:/qml/pages/PageSettingsRecordsManagement.qml"; icon: "qrc:/ui/briefcase.svg" }
        ListElement { type: 1; title: qsTr("View All Settings"); source: "qrc:/qml/pages/PagePrefsEditor.qml"; icon: "qrc:/ui/app_preferences.svg" }
        ListElement { type: 0; title: qsTr("Back up && Restore") }
        ListElement { type: 1; title: qsTr("Transfer Data"); source: "qrc:/qml/pages/PageTransferData.qml"; icon: "qrc:/ui/mobile.svg" }
        ListElement { type: 1; title: qsTr("Back up Data"); source: "qrc:/qml/pages/PageBackupData.qml"; icon: "qrc:/ui/archive.svg" }
        ListElement { type: 1; title: qsTr("Restore Data"); source: "qrc:/qml/pages/PageRestoreData.qml"; icon: "qrc:/ui/database.svg" }
        ListElement { type: 0; title: qsTr("Help") }
        ListElement { type: 1; title: qsTr("Log Viewer"); source: "qrc:/qml/pages/PageLog.qml"; icon: "qrc:/ui/format-list-bulleted.svg" }
        ListElement { type: 2; title: qsTr("User Guide"); url: "https://datovka.nic.cz/redirect/mobile-manual-ver-2.html"; icon: "qrc:/ui/home.svg" }
        ListElement { type: 1; title: qsTr("About Datovka"); source: "qrc:/qml/pages/PageAboutApp.qml"; icon: "qrc:/ui/information.svg" }
    }

    ListModel {
        id: listModelShort
        ListElement { type: 0; title: qsTr("Data Boxes") }
        ListElement { type: 1; title: qsTr("Add Data Box"); source: "qrc:/qml/pages/PageSettingsAccount.qml"; icon: "qrc:/ui/databox-add.svg" }
        ListElement { type: 0; title: qsTr("Messages") }
        ListElement { type: 4; title: qsTr("View Message"); action: "viewZfoMsg"; icon: "qrc:/ui/datovka-file-zfo.svg" }
        ListElement { type: 0; title: qsTr("Settings") }
        ListElement { type: 1; title: qsTr("General"); source: "qrc:/qml/pages/PageSettingsGeneral.qml"; icon: "qrc:/ui/settings.svg" }
        ListElement { type: 1; title: qsTr("Synchronisation"); source: "qrc:/qml/pages/PageSettingsSync.qml"; icon: "qrc:/ui/sync-all.svg" }
        ListElement { type: 1; title: qsTr("Security && PIN"); source: "qrc:/qml/pages/PageSettingsPin.qml"; icon: "qrc:/ui/key-variant.svg" }
        ListElement { type: 1; title: qsTr("Records Management"); source: "qrc:/qml/pages/PageSettingsRecordsManagement.qml"; icon: "qrc:/ui/briefcase.svg" }
        ListElement { type: 1; title: qsTr("View All Settings"); source: "qrc:/qml/pages/PagePrefsEditor.qml"; icon: "qrc:/ui/app_preferences.svg" }
        ListElement { type: 0; title: qsTr("Back up && Restore") }
        ListElement { type: 1; title: qsTr("Restore Data"); source: "qrc:/qml/pages/PageRestoreData.qml"; icon: "qrc:/ui/database.svg" }
        ListElement { type: 0; title: qsTr("Help") }
        ListElement { type: 1; title: qsTr("Log Viewer"); source: "qrc:/qml/pages/PageLog.qml"; icon: "qrc:/ui/format-list-bulleted.svg" }
        ListElement { type: 2; title: qsTr("User Guide"); url: "https://datovka.nic.cz/redirect/mobile-manual-ver-2.html"; icon: "qrc:/ui/home.svg" }
        ListElement { type: 1; title: qsTr("About Datovka"); source: "qrc:/qml/pages/PageAboutApp.qml"; icon: "qrc:/ui/information.svg" }
    }

    ListView {
        id: listView

        focus: true
        anchors.fill: parent

        delegate: ItemDelegate {
            width: listView.width
            text: model.title
            enabled: (model.type !== 0)
            icon.source: (model.type !== 0) ? model.icon : ""
            icon.color: datovkaPalette.popupIconColor
            onClicked: {
                if (model.type === 1) {
                    pageView.push(model.source)
                } else if (model.type === 2) {
                    Qt.openUrlExternally(model.url)
                } else if (model.type === 4 && model.action === "viewZfoMsg") {
                    openFileDialog()
                } else if (model.type === 5 && model.action === "manageDatabox") {
                    showPopupAccountMenu(currentAcntName, currentAcntId)
                }
                drawer.close()
            }
        }

        model: isWelcomePage ? listModelShort : listModel

        ScrollIndicator.vertical: ScrollIndicator { }
    }
}
