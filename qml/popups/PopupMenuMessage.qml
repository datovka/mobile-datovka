/*
 * Copyright (C) 2014-2025 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

import QtQuick 2.7
import QtQuick.Controls 2.15
import QtQuick.Layouts 1.2
import cz.nic.mobileDatovka 1.0
import cz.nic.mobileDatovka.messages 1.0

Popup {
    id: popup
    modal: true
    anchors.centerIn: Overlay.overlay
    padding: 0

    property int msgType
    property string msgId
    property bool canDelete: false

    MessageBox {
        id: yesNoMsgBox
        onAcceptAndClosed: {
            messages.deleteMessageFromDbs(accountModel, messageModel, acntId, msgId)
            messages.fillMessageList(messageModel, acntId, msgType)
        }
    }

    ColumnLayout {
        anchors.fill: parent
        spacing: 0
        PopupItem {
            icon.source: "qrc:/ui/email-open-outline.svg"
            text: qsTr("Mark as Read")
            onClicked: messages.markMessageAsLocallyRead(messageModel, acntId, msgId, true)
        }
        PopupItem {
            icon.source: "qrc:/ui/email-outline.svg"
            text: qsTr("Mark as Unread")
            onClicked: messages.markMessageAsLocallyRead(messageModel, acntId, msgId, false)
        }
        PopupItem {
            visible: canDelete
            icon.source: "qrc:/ui/delete.svg"
            text: qsTr("Delete Message")
            onClicked: yesNoMsgBox.question(qsTr("Delete Message: %1").arg(msgId),
                qsTr("Do you want to delete the received message '%1'?").arg(msgId),
                qsTr("Note: It will delete all attachments and message information from the local database."))
        }
    }
}
