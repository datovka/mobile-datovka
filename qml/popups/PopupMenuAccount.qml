/*
 * Copyright (C) 2014-2025 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

import QtQuick 2.7
import QtQuick.Controls 2.15
import QtQuick.Layouts 1.2
import cz.nic.mobileDatovka 1.0
import cz.nic.mobileDatovka.account 1.0
import cz.nic.mobileDatovka.qmlIdentifiers 1.0

Popup {
    id: popup
    modal: true
    anchors.centerIn: Overlay.overlay
    padding: 0

    property string acntName
    property AcntId acntId: null

    MessageBox {
        id: yesNoMsgBox
        onAcceptAndClosed: {
            if (accounts.removeAccount(acntId)) {
                if (accountModel.rowCount() <= 0) {
                    pageView.replace("qrc:/qml/pages/PageWelcome.qml")
                } else if (accountModel.rowCount() === 1) {
                   pageView.replace("qrc:/qml/pages/PageMessageListSwipeView.qml")
                }
                /* INI settings are saved automatically. */
                drawerMenuDatovka.removeItem()
            }
        }
    }

    ColumnLayout {
        anchors.fill: parent
        spacing: 0
        PopupItem {
            icon.source: "qrc:/ui/databox-settings.svg"
            text: qsTr("Data-Box Settings")
            onClicked: pageView.push(pageSettingsAccount, {"pageView": pageView, "acntName": acntName, "acntId": acntId, "isNewAccount": false}, StackView.Immediate)
        }
        PopupItem {
            icon.source: "qrc:/ui/databox-info.svg"
            text: qsTr("View Data-Box info")
            onClicked: pageView.push(pageAccountDetail, {"pageView": pageView, "acntName": acntName, "acntId": acntId}, StackView.Immediate)
        }
        PopupItem {
            icon.source: "qrc:/ui/databox-settings.svg"
            text: qsTr("Data-Box Logo")
            onClicked: pageView.push(pageAccountLogo, {"acntName": acntName, "acntId": acntId}, StackView.Immediate)
        }
        PopupItem {
            icon.source: "qrc:/ui/send-gov-msg.svg"
            text: qsTr("Send E-Gov Request")
            onClicked: pageView.push(pageGovServiceList, {"pageView": pageView, "acntName": acntName, "acntId": acntId}, StackView.Immediate)
        }
        PopupItem {
            icon.source: "qrc:/ui/file-import.svg"
            text: qsTr("Import Message")
            onClicked: pageView.push(pageImportMessage, {"pageView": pageView, "acntName": acntName, "acntId": acntId}, StackView.Immediate)
        }
        PopupItem {
            icon.source: "qrc:/ui/account-search.svg"
            text: qsTr("Find Data Box")
            onClicked: pageView.push(pageDataboxSearch, {"pageView": pageView, "acntId": acntId}, StackView.Immediate)
        }
        PopupItem {
            icon.source: "qrc:/ui/archive.svg"
            text: qsTr("Back up Data")
            onClicked: pageView.push(pageBackupData, {"pageView": pageView, "acntId": acntId}, StackView.Immediate)
        }
        PopupItem {
            icon.source: "qrc:/ui/database.svg"
            text: qsTr("Repair Message Database")
            onClicked: pageView.push(pageRepairDatabase, {"pageView": pageView, "acntId": acntId}, StackView.Immediate)
        }
        PopupItem {
            id: changePwd
            icon.source: "qrc:/ui/account-key.svg"
            text: qsTr("Change Password")
            onClicked: pageView.push(pageChangePassword, {"pageView": pageView, "acntName": acntName, "acntId": acntId}, StackView.Immediate)
        }
        PopupItem {
            icon.source: "qrc:/ui/databox-remove.svg"
            text: qsTr("Delete Data Box")
            onClicked: yesNoMsgBox.question(qsTr("Remove Data Box: %1").arg(acntId.username),
                qsTr("Do you want to remove the data box '%1'?").arg(acntId.username),
                qsTr("Note: It will also remove all related local databases and data box information."))
        }
    }
}
