!contains(QT.network_private.enabled_features, openssl-linked) {
    CONFIG(release, debug|release): SSL_PATH = $$PWD/android_openssl
                              else: SSL_PATH = $$PWD/android_openssl/no-asm

    if (versionAtLeast(QT_VERSION, 6.5.0)) {
        SSL_PATH = $$SSL_PATH/ssl_3
    } else {
        SSL_PATH = $$SSL_PATH/ssl_1.1
    }

    INCLUDEPATH += \
        $$SSL_PATH/include/

    equals(ANDROID_TARGET_ARCH, armeabi) {
        LIBS = \
        $$SSL_PATH/armeabi-v7a/libcrypto.a \
        $$SSL_PATH/armeabi-v7a/libssl.a \
    }
    equals(ANDROID_TARGET_ARCH, armeabi-v7a) {
        LIBS = \
        $$SSL_PATH/armeabi-v7a/libcrypto.a \
        $$SSL_PATH/armeabi-v7a/libssl.a \
    }
    equals(ANDROID_TARGET_ARCH, arm64-v8a) {
        LIBS = \
        $$SSL_PATH/arm64-v8a/libcrypto.a \
        $$SSL_PATH/arm64-v8a/libssl.a \
    }
    equals(ANDROID_TARGET_ARCH, x86) {
        LIBS = \
        $$SSL_PATH/x86/libcrypto.a \
        $$SSL_PATH/x86/libssl.a \
    }
    equals(ANDROID_TARGET_ARCH, x86_64) {
        LIBS = \
        $$SSL_PATH/x86_64/libcrypto.a \
        $$SSL_PATH/x86_64/libssl.a
    }

    if (versionAtLeast(QT_VERSION, 6.5.0)) {
        ANDROID_EXTRA_LIBS += \
            $$SSL_PATH/arm64-v8a/libcrypto_3.so \
            $$SSL_PATH/arm64-v8a/libssl_3.so \
            $$SSL_PATH/armeabi-v7a/libcrypto_3.so \
            $$SSL_PATH/armeabi-v7a/libssl_3.so \
            $$SSL_PATH/x86/libcrypto_3.so \
            $$SSL_PATH/x86/libssl_3.so \
            $$SSL_PATH/x86_64/libcrypto_3.so \
            $$SSL_PATH/x86_64/libssl_3.so
    } else {
        ANDROID_EXTRA_LIBS += \
            $$SSL_PATH/arm64-v8a/libcrypto_1_1.so \
            $$SSL_PATH/arm64-v8a/libssl_1_1.so \
            $$SSL_PATH/armeabi-v7a/libcrypto_1_1.so \
            $$SSL_PATH/armeabi-v7a/libssl_1_1.so \
            $$SSL_PATH/x86/libcrypto_1_1.so \
            $$SSL_PATH/x86/libssl_1_1.so \
            $$SSL_PATH/x86_64/libcrypto_1_1.so \
            $$SSL_PATH/x86_64/libssl_1_1.so
    }
}
