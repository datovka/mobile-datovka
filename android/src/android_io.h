/*
 * Copyright (C) 2014-2025 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

#pragma once

#include <QAndroidActivityResultReceiver>

/*!
 * @brief Wraps Android intent retrieval.
 *
 */
class AndroidIO : public QAndroidActivityResultReceiver {

public:
	/*!
	 * @brief Constructor.
	 *
	 * @param[in] parent Parent object.
	 */
	explicit AndroidIO(void);

	/*!
	 * @brief Check if Android OS SDK Version is level 24 or newest.
	 *
	 * @return True if SDK Version is 24 or higher.
	 */
	static
	bool isSDKVersion24OrNewest(void);

	/*!
	 * @brief Check if Android OS SDK Version is level 30 or newest.
	 *
	 * @return True if SDK Version is 30 or higher.
	 */
	static
	bool isSDKVersion30OrNewest(void);

	/*!
	 * @brief Open file.
	 *
	 * @param[in] filePath File path.
	 * @return True if success.
	 */
	static
	bool openFile(const QString &filePath);

	/*!
	 * @brief Create email.
	 *
	 * @param[in] subject Email subject.
	 * @param[in] to Email recipient.
	 * @param[in] body Email body text.
	 * @param[in] filePaths Attachments paths.
	 * @return True if success.
	 */
	static
	bool createEmail(const QString &subject, const QString &to,
	    const QString &body, const QStringList &filePaths);

	/*!
	 * @brief Get real absolute file path from Java file provider.
	 *
	 * @param[in] filePath Virtual file path obtains from file manager.
	 * @return Full file path.
	 */
	static
	QString getRealFilePath(const QString &filePath);

	/*!
	 * @brief Open application rating page on the GooglePlay.
	 *
	 * @return True if success.
	 */
	static
	bool openAppRatingPage(void);

	/*!
	 * @brief Open application rating dialogue.
	 *
	 * @return True if success.
	 */
	static
	bool openAppRatingDlg(void);

private:
	/*!
	 * @brief Open file with Java file provider.
	 *
	 * @param[in] filePath File path.
	 * @return True if success.
	 */
	static
	bool openWithFileProvider(const QString &filePath);
};
