Mobile Datovka
--------------

Mobile Datovka provides access to the Data-Box Information System - a Czech
eGovernment service for delivering electronic documents. The application is
intended to be run on Android and iOS devices. It provides the same UI and
functionality for both platforms.

The application UI is based around the QtQuick/QML technology whereas the core
of the application is written in Qt/C++.
